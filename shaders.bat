@echo off
:: Copies shaders into release folder
echo Copying shaders...
copy /Y git\core\shaders\* release\game1\shaders

:: Copies properties into release folder
echo Copying core.game1.properties...
copy /Y git\core\core.game1.properties release\game1\
copy /Y git\core\core.game1.property-descriptions release\game1\
echo Copying toolkit.game1.properties...
copy /Y git\Toolkit\toolkit.game1.properties release\game1\
copy /Y git\Toolkit\toolkit.game1.property-descriptions release\game1\
