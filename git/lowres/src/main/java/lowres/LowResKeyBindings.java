package lowres;

import game1.core.annotations.Annotations;
import game1.core.input.KeyBindings;
import game1.core.input.KeyCommand;
import game1.core.input.Keys;
import game1.toolkit.commands.ExitCommand;

public class LowResKeyBindings extends KeyBindings {

  public LowResKeyBindings() {
    Annotations.process(this);
    register(KeyCommand.init().keys(Keys.KEY_ESCAPE).command(new ExitCommand()));
  }
}
