package lowres;

import java.util.List;

import game1.core.engine.Engine;
import game1.core.engine.TempActors;
import game1.core.engine.userstate.UserState;

public class LowResGameState extends UserState {

  @Override
  public void apply() {
    Engine.GAME_STATE.addActor(new ClickableBackground());
    Engine.GAME_STATE.addActor(TempActors.get("wall-left"));
    Engine.GAME_STATE.addActor(TempActors.get("wall-right"));
    Engine.GAME_STATE.addActor(TempActors.get("wall-top"));
    Engine.GAME_STATE.addActor(TempActors.get("wall-bottom"));
    Engine.GAME_STATE.addActor(TempActors.get("ball"));
    Engine.KEY_BINDINGS.activate("keys");
    Engine.MOUSE.setVisible(true);
  }

  @Override
  public List<String> listRequiredDatasets() {
    return List.of("LowResDataset");
  }
}
