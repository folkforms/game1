package lowres;

import game1.core.actors.Clickable;
import game1.core.engine.Engine;
import game1.core.engine.primitives.Rectangle;
import game1.core.engine.primitives.Shape;
import game1.core.graphics.Drawable;
import game1.toolkit.widgets.background.Background;
import game1.toolkit.widgets.label.Label;
import game1.toolkit.widgets.label.LabelBuilder;

public class ClickableBackground implements Clickable {

  private Label label;
  private int lastClickedX = -1;
  private int lastClickedY = -1;
  private Drawable background;
  private int z = 1;

  public ClickableBackground() {
    background = Background.create(0.2f, 0.7f, 0.44f);
    label = LabelBuilder.init("Clicked:").position(50, Engine.GAME_HEIGHT - 50, z).build();
    addChild(background);
    addChild(label);
  }

  @Override
  public Shape getShape() {
    return new Rectangle(0, 0, Engine.GAME_WIDTH, Engine.GAME_HEIGHT);
  }

  @Override
  public void onClick(int button) {
    lastClickedX = Engine.MOUSE.getX();
    lastClickedY = Engine.MOUSE.getY();
    label.setText(String.format("Clicked: %s,%s", lastClickedX, lastClickedY));
  }

  @Override
  public int getZ() {
    return z;
  }
}
