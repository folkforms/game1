package lowres;

import game1.core.actors.Actor;
import game1.core.actors.Tickable;
import game1.core.engine.Engine;
import game1.core.graphics.Drawable;

public class BouncingBall implements Actor, Tickable {

  private int size = 10, x = 100, y = 225, speedX = 10, speedY = 10;
  private Drawable drawable;

  public BouncingBall(Drawable drawable) {
    this.drawable = drawable;
    this.drawable.setPosition(x, y, 10);
    this.drawable.resize(size, size);
    addChild(this.drawable);
  }

  @Override
  public void onTick() {
    if (x <= 20 || x >= Engine.GAME_WIDTH - 20 - size) {
      speedX *= -1;
    }
    if (y <= 20 || y >= Engine.GAME_HEIGHT - 20 - size) {
      speedY *= -1;
    }

    x += speedX;
    y += speedY;

    drawable.setPosition(x, y);
  }
}
