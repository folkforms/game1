package lowres;

import java.io.IOException;

import game1.core.engine.Engine;
import game1.core.engine.Paths;

public class LowResMain {

  public static LowResGameState GAME_STATE;

  public static void main(String[] args) throws IOException {
    Paths.setGame1Version("0.0.11-SNAPSHOT");
    Paths.setLocalMavenRepo(System.getenv("MAVEN_REPO"));
    Paths.setGameName("low-res");

    Engine engine = new Engine();
    Engine.KEY_BINDINGS.register("keys", new LowResKeyBindings());
    Engine.DATASETS.register("LowResDataset", new LowResDataset());
    Engine.GAME_STATE.register("Main", new LowResGameState());
    engine.start("Main");
  }
}
