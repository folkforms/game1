package lowres;

import game1.core.engine.Engine;
import game1.core.engine.TempActors;
import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.pipelines.Pipeline;
import game1.core.resources.datasets.Dataset;
import game1.core.resources.loaders.ActorLoader;
import game1.core.resources.loaders.PrimitiveLoader;

class LowResDataset extends Dataset {
  LowResDataset() {
    int ballSize = 10;
    register("ball-drawable", PrimitiveLoader.init(Pipeline.SCENE_2D).rect2d(ballSize, ballSize));
    register("ball", ActorLoader.init(() -> {
      Drawable d = (Drawable) TempActors.get("ball-drawable");
      return new BouncingBall(d);
    }));

    int wallSize = 20;
    Colour wallColour = new Colour(1, 1, 0);
    register("wall-left", PrimitiveLoader.init(Pipeline.SCENE_2D)
        .rect2d(wallSize, Engine.GAME_HEIGHT).colour(wallColour).position(0, 0, 1));
    register("wall-right",
        PrimitiveLoader.init(Pipeline.SCENE_2D).rect2d(wallSize, Engine.GAME_HEIGHT)
            .colour(wallColour).position(Engine.GAME_WIDTH - wallSize, 0, 1));
    register("wall-top", PrimitiveLoader.init(Pipeline.SCENE_2D).rect2d(Engine.GAME_WIDTH, wallSize)
        .colour(wallColour).position(0, Engine.GAME_HEIGHT - wallSize, 1));
    register("wall-bottom", PrimitiveLoader.init(Pipeline.SCENE_2D)
        .rect2d(Engine.GAME_WIDTH, wallSize).colour(wallColour).position(0, 0, 1));
  }
}
