# LowRes

This is for testing out screen/window modes. To use, edit `low-res.game1.properties` and set `fullscreen` to one of the following:

- `off` (Windowed mode with initial size game.width x game.height)
- `borders` (Fullscreen and game is game.width x game.height inside the screen. Not stretched.)
- `stretch` (Fullscreen and game is stretched to fill the entire window. May distort the aspect ratio.)
- `soft_stretch` (Fullscreen and stretched but keeps aspect ratio by adding black bars.)
