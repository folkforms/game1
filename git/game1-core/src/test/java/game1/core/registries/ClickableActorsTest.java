package game1.core.registries;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import game1.actors.Clickable;
import game1.core.engine.viewport.DummyMouse;
import game1.core.input.MouseFactory;
import game1.core.registries.test_fixtures.DummyClickable;
import game1.debug.DebugScopes;
import game1.primitives.RectangleF;
import game1.resolutions.layers.ResolutionLayers;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.scaling.ScalingStrategies;

public class ClickableActorsTest {

  private DummyMouse mouse;

  @BeforeEach
  public void beforeEach() {
    DebugScopes.init();

    ClickableActors.clear();

    mouse = new DummyMouse();
    MouseFactory.setTestInstance(mouse);

    ResolutionLayersFactory.getInstance().init(1920, 1080);
  }

  @Test
  public void itSelectsTheCorrectClickable() {
    mouse.setWindowPos(new Vector2f(50, 50));
    DummyClickable inactiveClickable = new DummyClickable(new RectangleF(0, 0, 100, 100), false,
        5000);
    DummyClickable notUnderMouseClickable = new DummyClickable(new RectangleF(500, 500, 100, 100),
        true, 5000);
    DummyClickable lowerZIndexClickable = new DummyClickable(new RectangleF(0, 0, 100, 100), true,
        100);
    DummyClickable expectedClickable = new DummyClickable(new RectangleF(0, 0, 100, 100), true,
        110);
    ResolutionLayersFactory.getInstance().addActor(inactiveClickable,
        ResolutionLayers.WINDOW_LAYER);
    ResolutionLayersFactory.getInstance().addActor(notUnderMouseClickable,
        ResolutionLayers.WINDOW_LAYER);
    ResolutionLayersFactory.getInstance().addActor(lowerZIndexClickable,
        ResolutionLayers.WINDOW_LAYER);
    ResolutionLayersFactory.getInstance().addActor(expectedClickable,
        ResolutionLayers.WINDOW_LAYER);

    ClickableActors.register(inactiveClickable);
    ClickableActors.register(notUnderMouseClickable);
    ClickableActors.register(lowerZIndexClickable);
    ClickableActors.register(expectedClickable);

    Clickable actual = ClickableActors.getItemUnderMouse();

    assertEquals(expectedClickable, actual);
  }

  @Test
  public void itHandlesClickablesAtMultipleResolutions() {
    mouse.setWindowPos(new Vector2f(50, 50));
    String scaledLayer = "scaled-layer";
    ResolutionLayersFactory.getInstance().createStaticLayer(scaledLayer, new Vector2i(192, 108),
        ScalingStrategies.get(2));

    DummyClickable c1 = new DummyClickable(new RectangleF(0, 0, 10, 10), true, 400);
    DummyClickable c2 = new DummyClickable(new RectangleF(0, 0, 100, 100), true, 300);
    ResolutionLayersFactory.getInstance().addActor(c1, scaledLayer);
    ResolutionLayersFactory.getInstance().addActor(c2, ResolutionLayers.WINDOW_LAYER);

    ClickableActors.register(c1);
    ClickableActors.register(c2);

    Clickable actual = ClickableActors.getItemUnderMouse();

    assertEquals(c1, actual);
  }
}
