package game1.core.registries.test_fixtures;

import game1.actors.Clickable;
import game1.primitives.RectangleF;
import game1.primitives.Shape;

public class DummyClickable implements Clickable {

  private RectangleF shape;
  private boolean isActive;
  private int z;

  public DummyClickable(RectangleF shape, boolean isActive, int z) {
    this.shape = shape;
    this.isActive = isActive;
    this.z = z;
  }

  @Override
  public boolean isActive() {
    return isActive;
  }

  @Override
  public void setActive(boolean b) {
    this.isActive = b;
  }

  @Override
  public Shape<RectangleF> getShape() {
    return shape;
  }

  @Override
  public void onClick(int button, float windowX, float windowY) {
    // Do nothing
  }

  @Override
  public int getZ() {
    return z;
  }
}
