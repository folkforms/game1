package game1.core.engine.viewport;

import org.joml.Vector2f;

import game1.core.input.Mouse;

public class DummyMouse extends Mouse {

  private Vector2f windowPos;

  public DummyMouse() {
  }

  public void setWindowPos(Vector2f windowPos) {
    this.windowPos = windowPos;
  }

  @Override
  public Vector2f getWindowPos() {
    return windowPos;
  }
}
