package game1.core.engine.viewport;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector4f;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import game1.core.input.MouseFactory;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.scaling.ResolutionScalingStrategyV1;

public class MouseControlledSceneViewportTest {

  private Vector4f padding = new Vector4f(10, 10, 10, 10);
  private float scrollSpeed = 2.5f;
  private DummyMouse mouse;
  private MouseControlledSceneViewport viewport;
  private String resolutionLayerName = "test-layer";

  @BeforeEach
  public void beforeEach() {
    mouse = new DummyMouse();
    MouseFactory.setTestInstance(mouse);

    ResolutionLayersFactory.getInstance().init(1920, 1080);
    ResolutionLayersFactory.getInstance().createLayer(resolutionLayerName, new Vector2i(320, 180),
        new Vector2i(640, 360), new ResolutionScalingStrategyV1(), null);
  }

  @Test
  public void itMovesLeft() {
    viewport = new MouseControlledSceneViewport(resolutionLayerName, new Vector2f(320, 0), padding,
        scrollSpeed);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    mouse.setWindowPos(new Vector2f(0, 0)); // Left edge

    assertEquals(320, viewport.getX());
    assertEquals(0, viewport.getY());
    viewport.updateViewport();
    assertEquals(317.5f, viewport.getX());
    assertEquals(0, viewport.getY());
  }

  @Test
  public void itMovesRight() {
    viewport = new MouseControlledSceneViewport(resolutionLayerName, new Vector2f(0, 0), padding,
        scrollSpeed);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    mouse.setWindowPos(new Vector2f(1920, 0)); // Right edge

    assertEquals(0, viewport.getX());
    assertEquals(0, viewport.getY());
    viewport.updateViewport();
    assertEquals(2.5f, viewport.getX());
    assertEquals(0, viewport.getY());
  }

  @Test
  public void itMovesUp() {
    viewport = new MouseControlledSceneViewport(resolutionLayerName, new Vector2f(0, 0), padding,
        scrollSpeed);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    mouse.setWindowPos(new Vector2f(0, 1080)); // Top edge

    assertEquals(0, viewport.getX());
    assertEquals(0, viewport.getY());
    viewport.updateViewport();
    assertEquals(0, viewport.getX());
    assertEquals(2.5f, viewport.getY());
  }

  @Test
  public void itMovesDown() {
    viewport = new MouseControlledSceneViewport(resolutionLayerName, new Vector2f(0, 180), padding,
        scrollSpeed);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    mouse.setWindowPos(new Vector2f(0, 0)); // Bottom edge

    assertEquals(0, viewport.getX());
    assertEquals(180, viewport.getY());
    viewport.updateViewport();
    assertEquals(0, viewport.getX());
    assertEquals(177.5f, viewport.getY());
  }

  @Test
  public void itDoesNotMovePastLeftEdge() {
    viewport = new MouseControlledSceneViewport(resolutionLayerName, new Vector2f(1, 0), padding,
        scrollSpeed);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    mouse.setWindowPos(new Vector2f(0, 0)); // Left edge

    assertEquals(1, viewport.getX());
    assertEquals(0, viewport.getY());
    viewport.updateViewport();
    assertEquals(0, viewport.getX());
    assertEquals(0, viewport.getY());
    viewport.updateViewport();
    assertEquals(0, viewport.getX());
    assertEquals(0, viewport.getY());
  }

  @Test
  public void itDoesNotMovePastRightEdge() {
    viewport = new MouseControlledSceneViewport(resolutionLayerName, new Vector2f(319, 0), padding,
        scrollSpeed);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    mouse.setWindowPos(new Vector2f(1920, 0)); // Right edge

    assertEquals(319, viewport.getX());
    assertEquals(0, viewport.getY());
    viewport.updateViewport();
    assertEquals(320, viewport.getX());
    assertEquals(0, viewport.getY());
    viewport.updateViewport();
    assertEquals(320, viewport.getX());
    assertEquals(0, viewport.getY());
  }

  @Test
  public void itDoesNotMovePastTopEdge() {
    viewport = new MouseControlledSceneViewport(resolutionLayerName, new Vector2f(0, 179), padding,
        scrollSpeed);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    mouse.setWindowPos(new Vector2f(0, 1080)); // Top edge

    assertEquals(0, viewport.getX());
    assertEquals(179, viewport.getY());
    viewport.updateViewport();
    assertEquals(0, viewport.getX());
    assertEquals(180, viewport.getY());
    viewport.updateViewport();
    assertEquals(0, viewport.getX());
    assertEquals(180, viewport.getY());
  }

  @Test
  public void itDoesNotMovePastBottomEdge() {
    viewport = new MouseControlledSceneViewport(resolutionLayerName, new Vector2f(0, 1), padding,
        scrollSpeed);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    mouse.setWindowPos(new Vector2f(0, 0)); // Bottom edge

    assertEquals(0, viewport.getX());
    assertEquals(1, viewport.getY());
    viewport.updateViewport();
    assertEquals(0, viewport.getX());
    assertEquals(0, viewport.getY());
    viewport.updateViewport();
    assertEquals(0, viewport.getX());
    assertEquals(0, viewport.getY());
  }
}
