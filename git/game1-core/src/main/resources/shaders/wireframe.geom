#version 330

// Built-in variables:
//
// in gl_PerVertex {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// } gl_in[];
//
// in int gl_PrimitiveIDIn;
//
// out gl_PerVertex {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// };
//
// out int gl_PrimitiveID;
// out int gl_Layer;

layout (triangles) in;

layout(line_strip, max_vertices=6) out;// Three lines = 6 vertices

void createPrimitive(int startPoint, int endPoint) {
  gl_Position = gl_in[startPoint].gl_Position;
  EmitVertex();
  gl_Position = gl_in[endPoint].gl_Position;
  EmitVertex();
  EndPrimitive();
}

void main() {
  createPrimitive(0, 1);
  createPrimitive(1, 2);
  createPrimitive(2, 0);
}
