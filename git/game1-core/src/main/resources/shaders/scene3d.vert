#version 330

const int MAX_WEIGHTS = 4;
const int MAX_JOINTS = 200;

layout (location=0) in vec3 position;
layout (location=1) in vec2 texCoord;
layout (location=2) in vec3 vertexNormal;
layout (location=3) in vec4 jointWeights;
layout (location=4) in ivec4 jointIndices;
layout (location=5) in mat4 modelViewInstancedMatrix;
layout (location=9) in mat4 modelLightViewInstancedMatrix;
layout (location=13) in float selectedInstanced;

uniform int isInstanced;
uniform mat4 modelViewNonInstancedMatrix;
uniform mat4 jointsMatrix[MAX_JOINTS];
uniform mat4 projectionMatrix;
uniform mat4 modelLightViewNonInstancedMatrix;
uniform mat4 orthoProjectionMatrix;
uniform float selectedNonInstanced;

out DATA {
  vec2 texCoord;
  vec3 mvVertexNormal;
  vec3 mvVertexPos;
  vec4 mlightviewVertexPos;
  mat4 modelViewMatrix; // Either modelViewInstancedMatrix or modelViewNonInstancedMatrix
  float selected;
}vert_out;

void main() {
  vec4 initPos = vec4(0, 0, 0, 0);
  vec4 initNormal = vec4(0, 0, 0, 0);
  mat4 modelViewMatrix;
  mat4 lightViewMatrix;
  if (isInstanced > 0) {
    vert_out.selected = selectedInstanced;
    modelViewMatrix = modelViewInstancedMatrix;
    lightViewMatrix = modelLightViewInstancedMatrix;

    initPos = vec4(position, 1.0);
    initNormal = vec4(vertexNormal, 0.0);
  } else {
    vert_out.selected = selectedNonInstanced;
    modelViewMatrix = modelViewNonInstancedMatrix;
    lightViewMatrix = modelLightViewNonInstancedMatrix;

    int count = 0;
    for (int i = 0; i < MAX_WEIGHTS; i++) {
      float weight = jointWeights[i];
      if (weight > 0) {
        count++;
        int jointIndex = jointIndices[i];
        vec4 tmpPos = jointsMatrix[jointIndex] * vec4(position, 1.0);
        initPos += weight * tmpPos;

        vec4 tmpNormal = jointsMatrix[jointIndex] * vec4(vertexNormal, 0.0);
        initNormal += weight * tmpNormal;
      }
    }
    if (count == 0) {
      initPos = vec4(position, 1.0);
      initNormal = vec4(vertexNormal, 0.0);
    }
  }
  vec4 mvPos = modelViewMatrix * initPos;
  gl_Position = projectionMatrix * mvPos;

  vert_out.texCoord = texCoord;

  vert_out.mvVertexNormal = normalize(modelViewMatrix * initNormal).xyz;
  vert_out.mvVertexPos = mvPos.xyz;
  vert_out.mlightviewVertexPos = orthoProjectionMatrix * lightViewMatrix * initPos;
  vert_out.modelViewMatrix = modelViewMatrix;
}
