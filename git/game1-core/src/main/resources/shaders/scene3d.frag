#version 330

const int MAX_POINT_LIGHTS = 5;
const int MAX_SPOT_LIGHTS = 5;

struct Attenuation {
  float constant;
  float linear;
  float exponent;
};

struct PointLight {
  vec3 colour;
  // Light position is assumed to be in view coordinates
  vec3 position;
  float intensity;
  Attenuation att;
};

struct Spotlight {
  PointLight pl;
  vec3 conedir;
  float cutoff;
};

struct DirectionalLight {
  vec3 colour;
  vec3 direction;
  float intensity;
};

struct Material {
  vec4 ambient;
  vec4 diffuse;
  vec4 specular;
  int hasTexture;
  int hasNormalMap;
  float reflectance;
};

struct Fog {
  int isActive;
  vec3 colour;
  float density;
};

in DATA {
  vec2 texCoord;
  vec3 mvVertexNormal;
  vec3 mvVertexPos;
  vec4 mlightviewVertexPos;
  mat4 modelViewMatrix;
  float selected;
}frag_in;

uniform sampler2D texture_sampler;
uniform sampler2D normalMap;
uniform vec3 ambientLight;
uniform float specularPower;
uniform Material material;
uniform PointLight pointLights[MAX_POINT_LIGHTS];
uniform Spotlight spotlights[MAX_SPOT_LIGHTS];
uniform DirectionalLight directionalLight;
uniform Fog fog;
uniform sampler2D shadowMap;
uniform int renderShadow;
uniform int testMode;

out vec4 fragColor;

vec4 ambientC;
vec4 diffuseC;
vec4 speculrC;

void setupColours(Material material, vec2 textCoord) {
  if (material.hasTexture == 1 && testMode != 1) {
    ambientC = texture(texture_sampler, textCoord);
    diffuseC = ambientC;
    speculrC = ambientC;
  } else {
    ambientC = material.ambient;
    diffuseC = material.diffuse;
    speculrC = material.specular;
  }
}

vec4 calcLightColour(vec3 light_colour, float light_intensity, vec3 position, vec3 to_light_dir,
    vec3 normal) {
  vec4 diffuseColour = vec4(0, 0, 0, 0);
  vec4 specColour = vec4(0, 0, 0, 0);

  // Diffuse Light
  float diffuseFactor = max(dot(normal, to_light_dir), 0.0);
  diffuseColour = diffuseC * vec4(light_colour, 1.0) * light_intensity * diffuseFactor;

  // Specular Light
  vec3 camera_direction = normalize(-position);
  vec3 from_light_dir = -to_light_dir;
  vec3 reflected_light = normalize(reflect(from_light_dir, normal));
  float specularFactor = max(dot(camera_direction, reflected_light), 0.0);
  specularFactor = pow(specularFactor, specularPower);
  specColour = speculrC * light_intensity * specularFactor * material.reflectance
      * vec4(light_colour, 1.0);

  return (diffuseColour + specColour);
}

vec4 calcPointLight(PointLight light, vec3 position, vec3 normal) {
  vec3 light_direction = light.position - position;
  vec3 to_light_dir = normalize(light_direction);
  vec4 light_colour = calcLightColour(light.colour, light.intensity, position, to_light_dir,
      normal);

  // Apply Attenuation
  float distance = length(light_direction);
  float attenuationInv = light.att.constant + light.att.linear * distance
      + light.att.exponent * distance * distance;
  return light_colour / attenuationInv;
}

vec4 calcSpotlight(Spotlight light, vec3 position, vec3 normal) {
  vec3 light_direction = light.pl.position - position;
  vec3 to_light_dir = normalize(light_direction);
  vec3 from_light_dir = -to_light_dir;
  float spot_alfa = dot(from_light_dir, normalize(light.conedir));

  vec4 colour = vec4(0, 0, 0, 0);

  if (spot_alfa > light.cutoff) {
    colour = calcPointLight(light.pl, position, normal);
    colour *= (1.0 - (1.0 - spot_alfa) / (1.0 - light.cutoff));
  }
  return colour;
}

vec4 calcDirectionalLight(DirectionalLight light, vec3 position, vec3 normal) {
  return calcLightColour(light.colour, light.intensity, position, normalize(light.direction),
      normal);
}

vec4 calcFog(vec3 pos, vec4 colour, Fog fog, vec3 ambientLight, DirectionalLight dirLight) {
  vec3 fogColor = fog.colour * (ambientLight + dirLight.colour * dirLight.intensity);
  float distance = length(pos);
  float fogFactor = 1.0 / exp((distance * fog.density) * (distance * fog.density));
  fogFactor = clamp(fogFactor, 0.0, 1.0);

  vec3 resultColour = mix(fogColor, colour.xyz, fogFactor);
  return vec4(resultColour.xyz, colour.w);
}

vec3 calcNormal(Material material, vec3 normal, vec2 text_coord, mat4 modelViewMatrix) {
  vec3 newNormal = normal;
  if (material.hasNormalMap == 1) {
    newNormal = texture(normalMap, text_coord).rgb;
    newNormal = normalize(newNormal * 2 - 1);
    newNormal = normalize(modelViewMatrix * vec4(newNormal, 0.0)).xyz;
  }
  return newNormal;
}

float calcShadow(vec4 position) {
  if (renderShadow == 0) {
    return 1.0;
  }

  vec3 projCoords = position.xyz;
  // Transform from screen coordinates to texture coordinates
  projCoords = projCoords * 0.5 + 0.5;
  float bias = 0.05;

  float shadowFactor = 0.0;
  vec2 inc = 1.0 / textureSize(shadowMap, 0);

  for (int row = 0; row < 3; row++) {
    for (int col = 0; col < 3; col++) {
      float textDepth = texture(shadowMap, projCoords.xy + vec2(row, col) * inc).r;
      shadowFactor += projCoords.z - bias > textDepth ? 1.0 : 0.0;
    }
  }
  shadowFactor /= 9.0;

  if (projCoords.z > 1.0) {
    shadowFactor = 1.0;
  }

  return 1 - shadowFactor;
}

/**
 * Shows normals as blue pixels.
 */
void showNormals() {
  // Calculate per-pixel normal and light vector
  vec3 N = normalize(frag_in.mvVertexNormal);
  vec3 L = normalize(frag_in.mvVertexNormal - frag_in.mvVertexPos);
  // Simple N dot L diffuse lighting
  float tc = pow(max(0.0, dot(N, L)), 2);
  float g = (fragColor.x + fragColor.y + fragColor.z) / 3;
  fragColor.x = g * 0.2;
  fragColor.y = g * 0.2;
  fragColor.z = tc;
}

/**
 * Displays the image in grayscale.
 */
void toGrayscale() {
  float g = (fragColor.x + fragColor.y + fragColor.z) / 3;
  fragColor.x = g;
  fragColor.y = g;
  fragColor.z = g;
}

void main() {
  setupColours(material, frag_in.texCoord);

  vec3 currNomal = calcNormal(material, frag_in.mvVertexNormal, frag_in.texCoord,
      frag_in.modelViewMatrix);

  vec4 diffuseSpecularComp = calcDirectionalLight(directionalLight, frag_in.mvVertexPos, currNomal);

  for (int i = 0; i < MAX_POINT_LIGHTS; i++) {
    if (pointLights[i].intensity > 0) {
      diffuseSpecularComp += calcPointLight(pointLights[i], frag_in.mvVertexPos, currNomal);
    }
  }

  for (int i = 0; i < MAX_SPOT_LIGHTS; i++) {
    if (spotlights[i].pl.intensity > 0) {
      diffuseSpecularComp += calcSpotlight(spotlights[i], frag_in.mvVertexPos, currNomal);
    }
  }

  float shadow = calcShadow(frag_in.mlightviewVertexPos);
  fragColor = clamp(ambientC * vec4(ambientLight, 1) + diffuseSpecularComp * shadow, 0, 1);

  if (fog.isActive == 1) {
    fragColor = calcFog(frag_in.mvVertexPos, fragColor, fog, ambientLight, directionalLight);
  }

  if (frag_in.selected > 0) {
    fragColor = vec4(1, fragColor.y, fragColor.z, 1);
  }

  // testMode == 0 => Off
  // testMode == 1 => No textures (checked elsewhere)
  if (testMode == 2) {
    showNormals();
  } else if (testMode == 3) {
    toGrayscale();
  }
}
