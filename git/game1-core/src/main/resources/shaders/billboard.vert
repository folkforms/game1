#version 330

layout (location=0) in vec3 position;
layout (location=1) in vec2 texCoord;
layout (location=2) in vec3 vertexNormal;
layout (location=3) in mat4 modelViewMatrix;
layout (location=7) in float hasColor0;
layout (location=8) in vec4 particleColor0;

out vec2 outTexCoord;
out float hasColor;
out vec4 particleColor;

uniform mat4 projectionMatrix;

void main() {
  gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
  outTexCoord = texCoord;
  hasColor = hasColor0;
  particleColor = particleColor0;
}
