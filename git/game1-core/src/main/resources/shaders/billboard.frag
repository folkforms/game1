#version 330

in vec2 outTexCoord;
in float hasColor;
in vec4 particleColor;

out vec4 fragColor;

uniform sampler2D texture_sampler;

void main() {
  if (hasColor > 0) {
    fragColor = particleColor;
  } else {
    fragColor = texture(texture_sampler, outTexCoord);
  }
}
