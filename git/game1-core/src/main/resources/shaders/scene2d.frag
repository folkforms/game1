#version 330

struct Stencil {
  int type;
  vec4 area;
  vec4 colour;
};

in vec2 outTexCoord;
out vec4 fragColor;

uniform sampler2D texture_sampler;
uniform vec4 colour;
uniform int hasTexture;
uniform Stencil stencil;

void main() {

  if (hasTexture == 1) {
    fragColor = colour * texture(texture_sampler, outTexCoord);
  } else {
    fragColor = colour;
  }

  // Stencil
  float px = gl_FragCoord.x;
  float py = gl_FragCoord.y;

  float sx = stencil.area.x;
  float sy = stencil.area.y;
  float sw = stencil.area.z;
  float sh = stencil.area.w;
  bool isPixelInsideStencilArea = (px >= sx) && (px < sx + sw) && (py >= sy) && (py < sy + sh);
  if (stencil.type == 1) { // OUTSIDE
    if (!isPixelInsideStencilArea) {
      fragColor *= stencil.colour;
    }
  } else if (stencil.type == 2) { // INSIDE
    if (isPixelInsideStencilArea) {
      fragColor *= stencil.colour;
    }
  }
}
