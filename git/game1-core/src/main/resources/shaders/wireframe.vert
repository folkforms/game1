#version 330

layout (location=0) in vec3 position;
layout (location=1) in vec2 texCoord;
layout (location=2) in vec3 vertexNormal;
layout (location=3) in vec4 jointWeights;
layout (location=4) in ivec4 jointIndices;
layout (location=5) in mat4 modelViewInstancedMatrix;
layout (location=9) in mat4 modelLightViewInstancedMatrix;
layout (location=13) in float selectedInstanced;

uniform mat4 projectionMatrix;

void main() {
  vec4 initPos = vec4(position, 1.0);
  vec4 initNormal = vec4(vertexNormal, 0.0);
  mat4 modelViewMatrix = modelViewInstancedMatrix;

  vec4 mvPos = modelViewMatrix * initPos;
  gl_Position = projectionMatrix * mvPos;
}
