package game1.core.devtools;

import java.util.Map;
import java.util.stream.Collectors;

import org.joml.Vector2f;
import org.joml.Vector3f;

public class StringFormatUtils {

  public static String toString(Class<?> clazz, Map<String, Object> map) {
    String data = map.entrySet().stream()
        .map(e -> String.format("%s=%s", e.getKey(), convert(e.getValue())))
        .collect(Collectors.joining(","));
    return String.format("%s[%s]", clazz.getSimpleName(), data);
  }

  private static String convert(Object object) {
    if (object instanceof Vector3f) {
      return toString((Vector3f) object);
    } else if (object instanceof Vector2f) {
      return toString((Vector2f) object);
    } else {
      return object.toString();
    }
  }

  public static String toString(Vector3f v) {
    return String.format("%s,%s,%s", (int) v.x, (int) v.y, (int) v.z);
  }

  public static String toString(Vector2f v) {
    return String.format("%s,%s", (int) v.x, (int) v.y);
  }
}
