package game1.core.devtools;

import game1.actors.Actor;

/**
 * A label that will be displayed when the dev tools are visible.
 */
public interface DevLabel extends Actor {
}
