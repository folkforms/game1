package game1.core.devtools;

import game1.actors.Actor;
import game1.events.EventSubscriptionsFactory;
import game1.events.Game1Events;
import game1.events.ReceiveEvent;

public class HiddenDebugTool implements Actor {

  public static final float Z_INDEX = 90_000;

  public HiddenDebugTool() {
    EventSubscriptionsFactory.getInstance().subscribe(this);
  }

  @ReceiveEvent(Game1Events.DEV_TOOLS_VISIBILITY_TOGGLED)
  public void toggleVisibility(boolean isDevToolsVisible) {
    setVisible(isDevToolsVisible);
  }
}
