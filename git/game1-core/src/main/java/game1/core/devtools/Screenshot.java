package game1.core.devtools;

import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.ByteBuffer;
import java.util.Calendar;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

import folkforms.log.Log;
import game1.core.engine.Engine;
import game1.resolutions.layers.ResolutionLayers;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;

/**
 * Takes a screenshot.
 *
 * <p>
 * Screenshots are saved in the folder denoted by {@code game1.screenshots_folder}, which defaults
 * to 'screenshots'. The folder will be created if it does not exist.
 * </p>
 *
 * <p>
 * The easiest way to add screenshots to a game is with the following code:
 *
 * <pre>
  add(Keys.KEY_F12, KeyCommand.BLOCK, "screenshot");
  Engine.DEV_TOOLS.add(new DebugLabel("F12: Screenshot", 1600, 970));

  &#64;ReceiveEvent("screenshot")
  public void screenshot() {
    Screenshot.saveScreenshot();
  }
 * </pre>
 * </p>
 */
public class Screenshot {

  /**
   * Saves a screenshot. Filename is YYYYMMDDHHmmSS_i.png.
   */
  public static void saveScreenshot() {
    Calendar cal = Calendar.getInstance();
    String datetime = String.format("%s%s%s%s%s%s", cal.get(Calendar.YEAR),
        cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.HOUR),
        cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

    File screenshotsFolder = new File(Engine.PROPERTIES.getProperty("game1.screenshots_folder"));
    String filename = findAvailableIndex(screenshotsFolder, datetime);
    if (filename == null) {
      Log.error(
          "Could not save screenshot. Could not find available index for folder '%s', datetime '%s'",
          screenshotsFolder, datetime);
      return;
    }

    try {
      if (!screenshotsFolder.exists()) {
        if (!screenshotsFolder.mkdir()) {
          Log.error("The screenshot directory '%s'could not be created.", screenshotsFolder);
          return;
        }
      }
    } catch (SecurityException e) {
      Log.error("The screenshot directory '%s' could not be created.", screenshotsFolder);
      Log.error(e);
      return;
    }

    String filepath = screenshotsFolder + "/" + filename + ".png";
    File outputFile = new File(filepath);
    try {
      ImageIO.write(getImage(), "png", outputFile);
      Log.info("Saved screenshot to %s", outputFile.getAbsolutePath());
    } catch (Exception e) {
      Log.error("Could not save screenshot (path: '%s')", outputFile.getAbsolutePath());
      Log.error(e);
      return;
    }
  }

  private static String findAvailableIndex(File screenshotsFolder, String datetime) {
    int index = 0;
    while (index < 240) {
      String potentialFilename = datetime + "_" + index;
      File f = new File(screenshotsFolder + "/" + potentialFilename + ".png");
      if (!f.exists()) {
        return potentialFilename;
      }
      index++;
    }
    return null;
  }

  /**
   * Creates a buffered image from the OpenGL pixel buffer.
   *
   * @return a buffered image containing the pixel buffer data
   */
  private static BufferedImage getImage() {
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(ResolutionLayers.WINDOW_LAYER).getResolutionHints();
    int w = hints.getWindowWidth();
    int h = hints.getWindowHeight();

    BufferedImage bufferedImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
    ByteBuffer buffer = BufferUtils.createByteBuffer(w * h * 4);

    // Reads the data from OpenGL into the buffer
    GL11.glReadPixels(0, 0, w, h, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer);

    // Transfers the data from the buffer into the BufferedImage
    for (int x = bufferedImage.getWidth() - 1; x >= 0; x--) {
      for (int y = bufferedImage.getHeight() - 1; y >= 0; y--) {
        int i = (x + w * y) * 4;
        bufferedImage.setRGB(x, bufferedImage.getHeight() - 1 - y,
            (((buffer.get(i) & 0xFF) & 0x0ff) << 16) | (((buffer.get(i + 1) & 0xFF) & 0x0ff) << 8)
                | ((buffer.get(i + 2) & 0xFF) & 0x0ff));
      }
    }

    return bufferedImage;
  }
}
