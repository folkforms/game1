package game1.core.devtools;

import java.util.ArrayList;
import java.util.List;

import game1.core.engine.Engine;
import game1.core.engine.GameCamera;
import game1.core.engine.internal.Camera;
import game1.core.engine.internal.CameraFactory;
import game1.core.engine.internal.FrameProfiler;
import game1.core.input.KeyBindings;
import game1.core.input.KeyCommand;
import game1.core.input.KeyboardUtils;
import game1.core.input.Mouse;
import game1.core.input.MouseFactory;
import game1.core.pipelines.GraphicsPipeline;
import game1.core.pipelines.Scene3DPipeline;
import game1.debug.dump_data.DebugDataProviderRegistry;
import game1.events.Command;
import game1.events.EventsFactory;
import game1.events.Game1Events;
import game1.filesystem.FileSystemFactory;
import game1.filesystem.FileSystemImpl;
import game1.variables.VariablesFactory;

public class DevKeyBindings extends KeyBindings {

  public static List<String> DEV_KEY_BINDINGS_DISPLAY_TEXT = new ArrayList<>();

  public DevKeyBindings(FrameProfiler frameProfiler) {
    registerToggleDevToolsCommand("game1.keys.toggle_dev_tools_visible");
    registerToggleDevToolsMaskCommand("game1.keys.toggle_dev_tools_mask_opacity");
    registerPauseEngineCommand("game1.key.pause_engine");
    registerScreenshotCommand("game1.key.screenshot");
    registerDumpAllDataToFileCommand("game1.key.dump_all_data_to_file");
    registerDumpAllDataToConsoleCommand("game1.key.dump_all_data_to_console");
    registerCaptureFrameProfileCommand(frameProfiler, "game1.key.capture_frame_profile");
    registerChange3DShaderTestModeCommand("game1.key.change_shader_test_mode");
    registerReloadShadersCommand("game1.key.reload_shaders");
    registerPrintCameraDetailsCommand("game1.key.print_camera_details");
    registerPrintFileSystemDataCommand("game1.key.print_file_system_data");
    registerToggleCollisionMeshVisibilityCommand("game1.key.toggle_collision_mesh_visibility");
  }

  private void registerToggleDevToolsCommand(String keyboardKeyProperty) {
    Command showCommand = new Command() {
      @Override
      public void execute() {
        boolean devToolsVisible = VariablesFactory.getInstance().get("game1.dev.dev_tools_visible");
        devToolsVisible = !devToolsVisible;
        VariablesFactory.getInstance().set("game1.dev.dev_tools_visible", devToolsVisible);
        Mouse mouse = MouseFactory.getInstance();
        if (devToolsVisible) {
          mouse.stashMouseSettings();
          mouse.internal_forceVisible(true);
          mouse.internal_forceMouseCamera(false);
        } else {
          mouse.unstashMouseSettings();
        }
        EventsFactory.getInstance().raise(Game1Events.DEV_TOOLS_VISIBILITY_TOGGLED,
            devToolsVisible);
      }
    };
    register(KeyCommand.init().command(showCommand).keys(keyboardKeyProperty));
    DEV_KEY_BINDINGS_DISPLAY_TEXT.add(String.format("%s: Toggle dev tools",
        KeyboardUtils.convertForDisplay2(keyboardKeyProperty)));
  }

  private void registerToggleDevToolsMaskCommand(String keyboardKeyProperty) {
    Command command = new Command() {
      @Override
      public void execute() {
        List<String> colours = VariablesFactory.getInstance()
            .get("game1.dev.dev_tools_mask.colours");
        int currentColour = VariablesFactory.getInstance()
            .get("game1.dev.dev_tools_mask.current_colour");
        currentColour++;
        if (currentColour == colours.size()) {
          currentColour = 0;
        }
        VariablesFactory.getInstance().set("game1.dev.dev_tools_mask.current_colour",
            currentColour);
        EventsFactory.getInstance().raise(Game1Events.DEV_TOOLS_MASK_COLOUR_CHANGED, currentColour);
      }
    };
    register(KeyCommand.init().command(command).keys(keyboardKeyProperty));
    DEV_KEY_BINDINGS_DISPLAY_TEXT.add(String.format("%s: Toggle dev tools opacity",
        KeyboardUtils.convertForDisplay2(keyboardKeyProperty)));
  }

  private void registerPauseEngineCommand(String keyboardKeyProperty) {
    register(KeyCommand.init()
        .command(() -> Engine.INTERNAL_PROCESSING_PAUSED = !Engine.INTERNAL_PROCESSING_PAUSED)
        .property(keyboardKeyProperty));
    DEV_KEY_BINDINGS_DISPLAY_TEXT.add(
        String.format("%s: Pause engine", KeyboardUtils.convertForDisplay(keyboardKeyProperty)));
  }

  private void registerScreenshotCommand(String keyboardKeyProperty) {
    register(
        KeyCommand.init().command(() -> Screenshot.saveScreenshot()).property(keyboardKeyProperty));
    DEV_KEY_BINDINGS_DISPLAY_TEXT
        .add(String.format("%s: Screenshot", KeyboardUtils.convertForDisplay(keyboardKeyProperty)));
  }

  private void registerDumpAllDataToFileCommand(String keyboardKeyProperty) {
    Command dumpDataCommand = new Command() {
      @Override
      public void execute() {
        DebugDataProviderRegistry.dumpAllDataToFile();
      }
    };
    register(KeyCommand.init().command(dumpDataCommand).property(keyboardKeyProperty));
    DEV_KEY_BINDINGS_DISPLAY_TEXT.add(String.format("%s: Save pipeline data",
        KeyboardUtils.convertForDisplay(keyboardKeyProperty)));
  }

  private void registerDumpAllDataToConsoleCommand(String keyboardKeyProperty) {
    Command dumpDataCommand = new Command() {
      @Override
      public void execute() {
        DebugDataProviderRegistry.dumpAllDataToConsole();
      }
    };
    register(KeyCommand.init().command(dumpDataCommand).property(keyboardKeyProperty));
    DEV_KEY_BINDINGS_DISPLAY_TEXT.add(String.format("%s: Print pipeline data",
        KeyboardUtils.convertForDisplay(keyboardKeyProperty)));
  }

  private void registerCaptureFrameProfileCommand(FrameProfiler frameProfiler,
      String keyboardKeyProperty) {
    Command captureFrameProfileCommand = new Command() {
      @Override
      public void execute() {
        frameProfiler.turnOn();
      }
    };
    register(KeyCommand.init().command(captureFrameProfileCommand).property(keyboardKeyProperty));
    DEV_KEY_BINDINGS_DISPLAY_TEXT.add(String.format("%s: Capture frame profile",
        KeyboardUtils.convertForDisplay(keyboardKeyProperty)));
  }

  private void registerChange3DShaderTestModeCommand(String keyboardKeyProperty) {
    Command change3DShaderTestModeCommand = new Command() {
      @Override
      public void execute() {
        GraphicsPipeline.debug_testMode++;
        if (GraphicsPipeline.debug_testMode > Scene3DPipeline.TEST_MODES.size() - 1) {
          GraphicsPipeline.debug_testMode = 0;
        }
        GraphicsPipeline.debug_testModeName = Scene3DPipeline.TEST_MODES
            .get(GraphicsPipeline.debug_testMode);
      }
    };
    register(
        KeyCommand.init().command(change3DShaderTestModeCommand).property(keyboardKeyProperty));
    DEV_KEY_BINDINGS_DISPLAY_TEXT.add(String.format("%s: Change 3D shader test mode",
        KeyboardUtils.convertForDisplay(keyboardKeyProperty)));
  }

  private void registerReloadShadersCommand(String keyboardKeyProperty) {
    register(KeyCommand.init().command(() -> GraphicsPipeline.debug_shaderModified = true)
        .property(keyboardKeyProperty));
    DEV_KEY_BINDINGS_DISPLAY_TEXT.add(
        String.format("%s: Reload shaders", KeyboardUtils.convertForDisplay(keyboardKeyProperty)));
  }

  private void registerPrintCameraDetailsCommand(String keyboardKeyProperty) {
    Camera camera = CameraFactory.getInstance();
    register(KeyCommand.init().command(() -> ((GameCamera) camera).debug_printCameraDetails())
        .property(keyboardKeyProperty));
    DEV_KEY_BINDINGS_DISPLAY_TEXT.add(String.format("%s: Print camera details",
        KeyboardUtils.convertForDisplay(keyboardKeyProperty)));
  }

  private void registerPrintFileSystemDataCommand(String keyboardKeyProperty) {
    Command printFileSystemDataCommand = new Command() {
      @Override
      public void execute() {
        FileSystemImpl fileSystemImpl = (FileSystemImpl) FileSystemFactory.getInstance();
        fileSystemImpl.debug_printData();
        Engine.GAME_STATE.debug_printActors();
      }
    };
    register(KeyCommand.init().command(printFileSystemDataCommand).property(keyboardKeyProperty));
    DEV_KEY_BINDINGS_DISPLAY_TEXT.add(String.format("%s: Print file system data",
        KeyboardUtils.convertForDisplay(keyboardKeyProperty)));
  }

  private void registerToggleCollisionMeshVisibilityCommand(String keyboardKeyProperty) {
    register(KeyCommand.init()
        .command(() -> Engine.SHOW_COLLISION_MESHES = !Engine.SHOW_COLLISION_MESHES)
        .property(keyboardKeyProperty));
    DEV_KEY_BINDINGS_DISPLAY_TEXT.add(String.format("%s: Show collision meshes",
        KeyboardUtils.convertForDisplay(keyboardKeyProperty)));
  }
}
