package game1.core.devtools;

import java.util.List;

import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.pipelines.GraphicsPipelineName;
import game1.core.resources.builders.PrimitiveBuilder;
import game1.events.Game1Events;
import game1.events.ReceiveEvent;
import game1.resolutions.layers.ResolutionLayers;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;
import game1.variables.VariablesFactory;

public class DebugToolsMask extends HiddenDebugTool {

  private Drawable background;
  private List<Colour> colours;

  public DebugToolsMask() {
    List<String> colourStrings = VariablesFactory.getInstance()
        .get("game1.dev.dev_tools_mask.colours");
    colours = colourStrings.stream().map(Colour::fromHex).toList();

    Colour colour = getBackgroundColour(
        VariablesFactory.getInstance().get("game1.dev.dev_tools_mask.current_colour"));
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(ResolutionLayers.WINDOW_LAYER).getResolutionHints();
    background = PrimitiveBuilder.init(GraphicsPipelineName.SCENE_2D)
        .rect2d(hints.getWindowWidth(), hints.getWindowHeight()).position(0, 0, Z_INDEX - 1000)
        .colour(colour).build();
    background.setVisible(false);
    addChild(background);
  }

  @ReceiveEvent(Game1Events.DEV_TOOLS_MASK_COLOUR_CHANGED)
  public void toggleMaskColour(int index) {
    background.setColour(getBackgroundColour(index));
  }

  private Colour getBackgroundColour(int index) {
    return colours.get(index);
  }
}
