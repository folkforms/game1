package game1.core.states;

import java.util.List;

import game1.actors.Actor;
import game1.actors.Tickable;
import game1.core.engine.Engine;
import game1.core.engine.EngineInternal;
import game1.events.EventSubscriptionsFactory;

/**
 * {@link LoadingScreenFinishedChecker2} is added to state during a loading screen and moves to the
 * next state when all if the given datasets have been loaded.
 */
public class LoadingScreenFinishedChecker2 implements Actor, Tickable {

  private List<String> tagsToLoad;
  private String nextState;

  LoadingScreenFinishedChecker2(List<String> tagsToLoad, String nextState) {
    EventSubscriptionsFactory.getInstance().subscribe(this);
    this.tagsToLoad = tagsToLoad;
    this.nextState = nextState;
  }

  @Override
  public void onTick() {
    if (EngineInternal.GAME_FILES.internal_allTagsLoaded(tagsToLoad)) {
      Engine.GAME_STATE.moveToState(nextState);
    }
  }
}
