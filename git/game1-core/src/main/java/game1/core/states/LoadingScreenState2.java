package game1.core.states;

import java.io.IOException;
import java.util.List;

import game1.core.engine.Engine;
import game1.core.engine.EngineInternal;
import game1.core.engine.userstate.UserState;
import game1.core.engine.userstate.UserStateHelper;
import game1.core.engine.userstate.UserStateProperty;
import game1.core.input.Mouse;
import game1.resolutions.layers.ResolutionLayers;

abstract public class LoadingScreenState2 extends UserState {

  protected List<String> tagsToLoad;
  protected String nextState;
  protected LoadingScreenFinishedChecker2 isFinishedChecker;

  public LoadingScreenState2(List<String> tagsToLoad, String nextState) {
    this.tagsToLoad = tagsToLoad;
    this.nextState = nextState;
    setProperty(UserStateProperty.DO_NOT_UNLOAD_DATASETS, true);
  }

  @Override
  public void preApply() throws IOException {
    super.preApply();
    UserStateHelper.setMouse(Mouse.MOUSE_OFF);
    isFinishedChecker = new LoadingScreenFinishedChecker2(tagsToLoad, nextState);
    EngineInternal.GAME_FILES.internal_enqueueAll(tagsToLoad);
    Engine.GAME_STATE.addActor(isFinishedChecker, ResolutionLayers.WINDOW_LAYER);
  }

  protected LoadingScreenFinishedChecker2 getChecker() {
    return isFinishedChecker;
  }
}
