package game1.core.states;

import game1.actors.Actor;
import game1.actors.Clickable;
import game1.actors.Tickable;
import game1.core.engine.Engine;
import game1.core.input.KeyboardFactory;
import game1.primitives.Rectangle;
import game1.primitives.Shape;
import game1.resolutions.layers.ResolutionLayers;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;

/**
 * {@link SplashScreenChecker} is added to state during a splash screen and moves to the next state
 * when either a given amount of time has elapsed or the user clicks the mouse or presses any key.
 */
public class SplashScreenChecker implements Actor, Tickable, Clickable {

  private String nextState;
  private String soundRef = null;

  private boolean skip = false;
  private int delay = 5000;
  private long startTime = -1L;
  private Shape shape;
  private boolean active = true;

  /**
   * Creates a new {@link SplashScreenChecker} with the given next state and sound to play.
   *
   * @param nextState
   * @param soundRef
   */
  public SplashScreenChecker(String nextState, String soundRef) {
    this.nextState = nextState;
    this.soundRef = soundRef;
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(ResolutionLayers.WINDOW_LAYER).getResolutionHints();
    this.shape = new Rectangle(0, 0, hints.getWindowWidth(), hints.getWindowHeight());
  }

  public SplashScreenChecker(String nextState) {
    this(nextState, null);
  }

  @Override
  public void onTick() {
    if (startTime == -1) {
      startTime = System.currentTimeMillis();
    }

    if (KeyboardFactory.getInstance().anyKeyDown()) {
      skip = true;
    }

    boolean finishedPlaying = System.currentTimeMillis() - startTime > delay;
    if (finishedPlaying || skip) {
      if (soundRef != null) {
        Engine.SOUND_BOARD.stop(soundRef);
      }
      Engine.GAME_STATE.moveToState(nextState);
    }
  }

  @Override
  public int getZ() {
    return 0;
  }

  @Override
  public Shape getShape() {
    return shape;
  }

  @Override
  public void onClick(int button, float windowX, float windowY) {
    skip = true;
  }

  @Override
  public boolean isActive() {
    return active;
  }

  @Override
  public void setActive(boolean b) {
    active = b;
  }
}
