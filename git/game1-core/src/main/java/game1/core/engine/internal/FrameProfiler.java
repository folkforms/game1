package game1.core.engine.internal;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import folkforms.log.Log;
import folkforms.textio.TextIO;

public class FrameProfiler {

  private boolean turnedOn = false;
  private boolean isRunning = false;
  private List<String> events = new ArrayList<>();
  private int fileNum = 0;

  public void turnOn() {
    if (!turnedOn) {
      turnedOn = true;
    }
  }

  public void start() {
    if (!turnedOn) {
      return;
    }
    if (isRunning) {
      add("Frame profiler stopped");
      isRunning = false;
      turnedOn = false;
      writeData();
    } else {
      isRunning = true;
      add("Frame profiler started");
    }
  }

  public void add(String event, Object... args) {
    if (!turnedOn || !isRunning) {
      return;
    }
    String event2 = String.format(event, args);
    long timestamp = System.nanoTime();
    events.add(String.format("%s: %s", timestamp, event2));
  }

  public void writeData() {
    // FIXME Search for existing files and do not overwrite
    String outputFolder = "_dumps";
    new File(outputFolder).mkdir();
    String filename = String.format("%s/frame_profile_%s.txt", outputFolder, fileNum);
    fileNum++;
    long firstTimestamp = getFirstTimestamp();
    for (int i = 0; i < events.size(); i++) {
      String line = events.get(i);
      String[] tokens = line.split(": ", 2);
      String newLine = String.format("%s: %s", Long.parseLong(tokens[0]) - firstTimestamp,
          tokens[1]);
      events.set(i, newLine);
    }
    try {
      new TextIO().write(events, filename);
      Log.info("Wrote file %s", filename);
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }
    events.clear();
  }

  private long getFirstTimestamp() {
    String[] tokens = events.get(0).split(": ", 2);
    return Long.parseLong(tokens[0]);
  }
}
