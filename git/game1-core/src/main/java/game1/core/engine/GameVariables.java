package game1.core.engine;

import java.io.IOException;

import folkforms.yaml.YamlFile;
import game1.datasets.GameDataFactory;
import game1.variables.Variables;

/**
 * Wrapper around Variables to allow users to easily load a yaml file via filename. This is because
 * Variables has no concept of GAME_FILES or GAME_DATA.
 */
public class GameVariables extends Variables {

  public void apply(String filename) throws IOException {
    applyYamlFile(loadYamlFile(filename));
  }

  protected YamlFile loadYamlFile(String filename) throws IOException {
    EngineInternal.GAME_FILES.register(filename, "no-tag---from-engine-constructor");
    EngineInternal.GAME_FILES.internal_blockingLoadFile(filename);
    return GameDataFactory.getInstance().get(filename);
  }
}
