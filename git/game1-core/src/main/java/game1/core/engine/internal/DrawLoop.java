package game1.core.engine.internal;

import folkforms.log.Log;

abstract public class DrawLoop {

  private GraphicsPipelines pipelines;
  private Camera camera;
  private UpdateHandler updateHandler;
  private GarbageCollector garbageCollector;
  private FrameProfiler frameProfiler;

  public static final String FULLSCREEN_OFF = "off";
  public static final String FULLSCREEN_BORDERS = "borders";
  public static final String FULLSCREEN_STRETCH = "stretch";
  public static final String FULLSCREEN_SOFT_STRETCH = "soft_stretch";
  public static String FULLSCREEN_OPTION;
  public static int SCREEN_BORDER_X = 0;
  public static int SCREEN_BORDER_Y = 0;

  public static int DEBUG_AVG_FPS = 0;
  public static int DEBUG_AVG_UPS = 0;

  private double[] frameTimes = new double[100];
  private int frameTimesIndex = 0;
  public static double DEBUG_AVG_FRAME_TIME = 0;

  public DrawLoop(GraphicsPipelines pipelines, Camera camera, UpdateHandler updateHandler,
      GarbageCollector garbageCollector, FrameProfiler frameProfiler) {
    this.pipelines = pipelines;
    this.camera = camera;
    this.updateHandler = updateHandler;
    this.garbageCollector = garbageCollector;
    this.frameProfiler = frameProfiler;
  }

  public void start() {
    setClearColor();
    try {
      long lastTime = System.nanoTime();
      final double updatesPerSecond = 200;
      double updateInterval = 1_000_000_000 / updatesPerSecond;
      double pendingUpdates = 0;

      double time = 0;
      int frameCount = 0;
      int updateCount = 0;

      while (!shouldStop()) {
        frameProfiler.start();
        long now = System.nanoTime();

        // Update debug data
        time += now - lastTime;
        if (time >= 1_000_000_000) {
          time -= 1_000_000_000;
          DEBUG_AVG_FPS = frameCount;
          frameCount = 0;
          DEBUG_AVG_UPS = updateCount;
          updateCount = 0;
        }

        // Calculate how many pending updates we have
        pendingUpdates += (now - lastTime) / updateInterval;
        noteFrameTime(pendingUpdates);
        lastTime = now;

        // If enough time has elapsed, perform an update
        while (pendingUpdates >= 1) {
          updateHandler.run();
          garbageCollector.garbageCollect();
          pendingUpdates--;
          updateCount++;
        }

        // Draw a frame
        preDraw();
        draw();
        postDraw();
        frameCount++;
      }

    } catch (Exception ex) {
      Log.error(ex);
      System.exit(1);
    } finally {
      cleanup();
      System.exit(0);
    }
  }

  private void noteFrameTime(double frameTime) {
    frameTimes[frameTimesIndex++] = frameTime;
    if (frameTimesIndex == frameTimes.length) {
      updateAverageFrameTime();
      frameTimes = new double[frameTimes.length];
      frameTimesIndex = 0;
    }
  }

  private void updateAverageFrameTime() {
    double avg = 0;
    for (int i = 0; i < frameTimes.length; i++) {
      avg += frameTimes[i];
    }
    avg /= frameTimes.length;
    DEBUG_AVG_FRAME_TIME = avg;
  }

  /**
   * Sets the clear colour used by this draw loop.
   */
  abstract protected void setClearColor();

  /**
   * Checks whether the draw loop should stop or not.
   *
   * @return true if the draw loop should stop, false otherwise
   */
  abstract protected boolean shouldStop();

  /**
   * Perform any steps that need to happen before drawing an individual frame.
   */
  protected void preDraw() {
  }

  /**
   * Draws all the visible elements on screen.
   */
  protected void draw() {
    pipelines.renderAll(camera.getViewMatrix());
  }

  /**
   * Perform any steps that need to happen after drawing an individual frame.
   */
  protected void postDraw() {
  }

  /**
   * Cleans up after closing.
   */
  abstract protected void cleanup();
}
