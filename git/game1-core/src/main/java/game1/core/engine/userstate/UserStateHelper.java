package game1.core.engine.userstate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.joml.Vector3f;

import folkforms.log.Log;
import game1.core.engine.Engine;
import game1.core.engine.Game1RuntimeException;
import game1.core.graphics.Colour;
import game1.core.graphics.lights.DirectionalLight;
import game1.core.input.Mouse;
import game1.core.input.MouseFactory;
import game1.debug.DebugScopes;
import game1.resolutions.layers.ResolutionLayers;

public class UserStateHelper {

  public static final String MOUSE_VISIBLE = "mouse.visible";
  public static final String MOUSE_CAMERA = "mouse.camera";

  private static Map<String, Object> defaultSettings = Map.of( //
      MOUSE_VISIBLE, true, //
      MOUSE_CAMERA, false //
  );
  private static Map<String, Object> settings = new HashMap<>();

  public static void clear() {
    settings.clear();
  }

  public static boolean getBoolean(String name) {
    Object value = settings.get(name);
    if (value == null) {
      Object defaultValue = defaultSettings.get(name);
      if (defaultValue == null) {
        throw new Game1RuntimeException("No such user state setting '%s'", name);
      }
      return (Boolean) defaultValue;
    }
    return (Boolean) value;
  }

  public static void setKeyBindings(String... keyBindings) {
    Log.scoped(DebugScopes.STATE, "activating key bindings '%s'", Arrays.toString(keyBindings));
    Engine.KEY_BINDINGS.activate(keyBindings);
  }

  public static void setMouse(int mouseSetting) {
    MouseFactory.getInstance().clearStashedMouseSettings();
    if (mouseSetting == Mouse.MOUSE_OFF) {
      Log.scoped(DebugScopes.STATE, "UserStateHelper.setMouse (mouse off)");
      settings.put(MOUSE_VISIBLE, false);
      settings.put(MOUSE_CAMERA, false);
    } else if (mouseSetting == Mouse.MOUSE_CURSOR) {
      Log.scoped(DebugScopes.STATE, "UserStateHelper.setMouse (mouse cursor)");
      settings.put(MOUSE_VISIBLE, true);
      settings.put(MOUSE_CAMERA, false);
    } else if (mouseSetting == Mouse.MOUSE_CAMERA) {
      Log.scoped(DebugScopes.STATE, "UserStateHelper.setMouse (mouse camera)");
      settings.put(MOUSE_VISIBLE, false);
      settings.put(MOUSE_CAMERA, true);
    } else {
      Log.error("UserStateHelper.setMouse: Unknown value '%s' for mouse setting", mouseSetting);
    }
  }

  public static void setDirectionalLight(Vector3f directionalLightVector) {
    // e.g. [ 0, 1, 0 ]
    Log.scoped(DebugScopes.STATE, "directionalLightVector = %s", directionalLightVector);
    DirectionalLight directionalLight = DirectionalLight.create(Colour.fromHex("fff"),
        directionalLightVector, 1);
    directionalLight.setLightSourceDistance(10);
    directionalLight.setOrthoCoords(-10.0f, 10.0f, -10.0f, 10.0f, -1.0f, 20.0f);
    Engine.GAME_STATE.addActor(directionalLight, ResolutionLayers.LAYER_3D);
  }

  public static void setMainMenu(UserState userState, boolean isMainMenu) {
    userState.setProperty(UserStateProperty.IS_MAIN_MENU, isMainMenu);
  }

  public static void setDoNotUnloadDatasets(UserState userState, boolean doNotUnloadDatasets) {
    userState.setProperty(UserStateProperty.DO_NOT_UNLOAD_DATASETS, doNotUnloadDatasets);
  }
}
