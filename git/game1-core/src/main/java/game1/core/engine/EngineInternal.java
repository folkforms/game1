package game1.core.engine;

import game1.core.infrastructure.Infrastructure;
import game1.core.infrastructure.Texture;
import game1.core.infrastructure.TextureLoader;
import game1.datasets.GameFiles;

/**
 * Holds internal objects that game developers should not need to access.
 */
public class EngineInternal {
  /**
   * Class that provides infrastructure components, e.g. graphics, resource loaders, input handlers.
   */
  public static Infrastructure INFRASTRUCTURE;

  /**
   * Class for loading {@link Texture} objects.
   */
  public static TextureLoader TEXTURE_LOADER;

  public static GameFiles GAME_FILES;
}
