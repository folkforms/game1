package game1.core.engine.internal;

public interface GarbageCollector {

  void garbageCollect();
}
