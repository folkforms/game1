package game1.core.engine.userstate;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import folkforms.log.Log;
import game1.core.engine.Engine;
import game1.core.engine.EngineInternal;
import game1.core.input.Cursor;
import game1.core.input.Mouse;
import game1.core.input.MouseFactory;
import game1.debug.DebugScopes;
import game1.resolutions.layers.ResolutionLayers;

public abstract class UserState {

  private Map<UserStateProperty, Object> properties = new LinkedHashMap<>();

  public Object getProperty(UserStateProperty key) {
    return properties.get(key);
  }

  public boolean getBooleanProperty(UserStateProperty key) {
    Object o = properties.get(key);
    if (o == null) {
      return false;
    }
    return Boolean.parseBoolean(o.toString());
  }

  public void setProperty(UserStateProperty key, Object value) {
    properties.put(key, value);
  }

  public void preApply() throws IOException {
    UserStateHelper.clear();

    Log.scoped(DebugScopes.STATE, "%s: UserState.preApply start", getClass().getSimpleName());
    Engine.KEY_BINDINGS.clearAllActive();
    if (!getBooleanProperty(UserStateProperty.DO_NOT_UNLOAD_DATASETS)) {
      EngineInternal.GAME_FILES.internal_unloadAllExcept(listRequiredTags());
    }
    ensureRequiredTagsAreLoaded();

    Log.scoped(DebugScopes.STATE, "%s: UserState.preApply end", getClass().getSimpleName());
  }

  public void postApply() {
    Log.scoped(DebugScopes.STATE, "%s: UserState.postApply start", getClass().getSimpleName());
    addMouseCursor();
    Log.scoped(DebugScopes.STATE, "%s: UserState.postApply end", getClass().getSimpleName());
  }

  private void addMouseCursor() {
    Log.scoped(DebugScopes.STATE, "%s: Adding mouse cursor", getClass().getSimpleName());
    Mouse mouse = MouseFactory.getInstance();
    Cursor mouseCursor = mouse.internal_createCursor();
    Engine.GAME_STATE.addActor(mouseCursor, ResolutionLayers.WINDOW_LAYER);
    mouse.setVisible(UserStateHelper.getBoolean(UserStateHelper.MOUSE_VISIBLE));
    mouse.setMouseCamera(UserStateHelper.getBoolean(UserStateHelper.MOUSE_CAMERA));
  }

  /**
   * Loads the tags that the new state requires. Typically they will have already been loaded by a
   * previous loading screen state, and this method will do nothing.
   */
  protected void ensureRequiredTagsAreLoaded() throws IOException {
    Log.scoped(DebugScopes.STATE, "%s: UserState.ensureRequiredTagsAreLoaded (requiredTags = %s)",
        getClass().getSimpleName(), listRequiredTags());
    List<String> requiredTags = listRequiredTags();
    for (int i = 0; i < requiredTags.size(); i++) {
      String tag = requiredTags.get(i);
      EngineInternal.GAME_FILES.internal_blockingLoadTag(tag);
    }
  }

  abstract public void apply();

  abstract public List<String> listRequiredTags();
}
