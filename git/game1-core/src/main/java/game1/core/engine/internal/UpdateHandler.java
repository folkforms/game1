package game1.core.engine.internal;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import folkforms.log.Log;
import game1.actors.Tickable;
import game1.core.engine.Engine;
import game1.core.engine.EngineInternal;
import game1.core.graphics.Animation;
import game1.core.input.InputHandler;
import game1.core.input.KeyCommand;
import game1.core.input.MouseHandler;
import game1.core.registries.TickableActors;
import game1.events.EventsFactory;
import game1.resolutions.layers.ResolutionLayer;
import game1.resolutions.layers.ResolutionLayersFactory;

/**
 * The {@link UpdateHandler} handles {@link Tickable}s, {@link Animation}s, user input, events and
 * dataset loading. {@link Tickable}s are handled at a rate of 100 per second, everything else at a
 * rate of 200 per second.
 */
public class UpdateHandler {

  private Camera gameCamera;

  /**
   * Used to slow down the ticking of {@link Tickable}s and {@link Animation}s. Also used by
   * {@link InputHandler} to handle {@link KeyCommand}s that tick at a certain tickrate.
   */
  private static long tick = 0;
  private InputHandler inputHandler;
  private MouseHandler mouseHandler;

  private long lastLoadTime = 0L;
  private int sleepBetweenLoading = Engine.PROPERTIES
      .getIntProperty("game1.update_handler.sleep_time_between_loading_ms");
  private long framesSinceLastLoad = 0;
  private long minFramesBetweenLoading = Engine.PROPERTIES
      .getIntProperty("game1.update_handler.min_frames_between_loading");

  public UpdateHandler(Camera gameCamera) {
    this.gameCamera = gameCamera;
    inputHandler = new InputHandler();
    mouseHandler = new MouseHandler();
  }

  public static long getTick() {
    return tick;
  }

  public void run() {
    try {
      handleUserInput();
      handleEvents();
      if (tick % 2 == 0) {
        handleTicks();
        Engine.SOUND_MANAGER.updateListenerPosition(gameCamera);
      }
      Collection<ResolutionLayer> resolutionLayers = ResolutionLayersFactory.getInstance()
          .debug_getResolutionLayers().values();
      resolutionLayers.forEach(resolutionLayer -> resolutionLayer.getViewport().updateViewport());
      gameCamera.updateCamera();
      handleLoading();
    } catch (Exception ex) {
      Log.error(ex);
      System.exit(1);
    }
    tick++;
  }

  /**
   * Handle actions based on the input device state. InputHandler handles all input devices
   * (including the mouse). MouseHandler handles GUI concepts like Clickables, Focusables,
   * Draggables and Scrollables.
   */
  private void handleUserInput() {
    inputHandler.setStates();
    inputHandler.handleUserInput();
    mouseHandler.handleUserInput();
  }

  private void handleEvents() {
    EventsFactory.getInstance().processEvents();
  }

  private void handleTicks() {
    if (!Engine.INTERNAL_PROCESSING_PAUSED) {
      List<Tickable> tickables = Engine.GAME_PAUSED ? TickableActors.listTickEvenIfGamePaused()
        : TickableActors.listTickables();
      int size = tickables.size();
      for (int i = 0; i < size; i++) {
        if (Engine.GAME_STATE.pollSceneCleared()) {
          return;
        }
        Tickable t = tickables.get(i);
        t.onTick();
      }
      if (tick % 10 == 0) {
        List<Tickable> lowPriority = TickableActors.listLowPriority();
        int lowPrioritySize = lowPriority.size();
        for (int i = 0; i < lowPrioritySize; i++) {
          Tickable t = lowPriority.get(i);
          t.onTick();
        }
      }
    }
  }

  private void handleLoading() throws IOException {
    long currentTime = System.currentTimeMillis();
    if (currentTime - lastLoadTime > sleepBetweenLoading
        && framesSinceLastLoad >= minFramesBetweenLoading) {
      EngineInternal.GAME_FILES.internal_processLoadingQueue();
      lastLoadTime = System.currentTimeMillis();
      framesSinceLastLoad = 0;
    } else {
      framesSinceLastLoad++;
    }
  }
}
