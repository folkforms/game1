package game1.core.engine.viewport;

import org.joml.Vector2f;
import org.joml.Vector4f;

import game1.core.input.Mouse;
import game1.core.input.MouseFactory;
import game1.core.input.MouseUtils;
import game1.resolutions.layers.ResolutionLayer;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;
import game1.resolutions.viewports.SceneViewport;
import game1.resolutions.viewports.Viewport;

public class MouseControlledSceneViewport extends SceneViewport {

  private float scrollSpeed;

  public MouseControlledSceneViewport(String resolutionLayerName, Vector2f viewportCoords,
      Vector4f padding, float scrollSpeed) {
    super(resolutionLayerName, viewportCoords, padding);
    this.scrollSpeed = scrollSpeed;
  }

  @Override
  public float getX() {
    return viewportCoords.x;
  }

  @Override
  public float getY() {
    return viewportCoords.y;
  }

  @Override
  protected boolean checkXNegativeCondition() {
    ResolutionLayer resolutionLayer = ResolutionLayersFactory.getInstance()
        .getLayerByName(resolutionLayerName);
    Mouse mouse = MouseFactory.getInstance();
    Vector2f windowPos = mouse.getWindowPos();
    Vector2f scenePos = MouseUtils.translateToResolutionLayerPosition(windowPos,
        resolutionLayerName);
    Viewport viewport = resolutionLayer.getViewport();
    float leftPadding = padding.w;
    return scenePos.x - viewport.getX() < 0 + leftPadding;
  }

  @Override
  protected boolean checkXPositiveCondition() {
    ResolutionLayer resolutionLayer = ResolutionLayersFactory.getInstance()
        .getLayerByName(resolutionLayerName);
    ResolutionHints hints = resolutionLayer.getResolutionHints();
    Mouse mouse = MouseFactory.getInstance();
    Vector2f windowPos = mouse.getWindowPos();
    Vector2f scenePos = MouseUtils.translateToResolutionLayerPosition(windowPos,
        resolutionLayerName);
    Viewport viewport = resolutionLayer.getViewport();
    float rightPadding = padding.y;
    return scenePos.x - viewport.getX() > hints.getViewportWidth() - rightPadding;
  }

  @Override
  protected boolean checkYNegativeCondition() {
    ResolutionLayer resolutionLayer = ResolutionLayersFactory.getInstance()
        .getLayerByName(resolutionLayerName);
    Mouse mouse = MouseFactory.getInstance();
    Vector2f windowPos = mouse.getWindowPos();
    Vector2f scenePos = MouseUtils.translateToResolutionLayerPosition(windowPos,
        resolutionLayerName);
    Viewport viewport = resolutionLayer.getViewport();
    float bottomPadding = padding.z;
    return scenePos.y - viewport.getY() < 0 + bottomPadding;
  }

  @Override
  protected boolean checkYPositiveCondition() {
    ResolutionLayer resolutionLayer = ResolutionLayersFactory.getInstance()
        .getLayerByName(resolutionLayerName);
    ResolutionHints hints = resolutionLayer.getResolutionHints();
    Mouse mouse = MouseFactory.getInstance();
    Vector2f windowPos = mouse.getWindowPos();
    Vector2f scenePos = MouseUtils.translateToResolutionLayerPosition(windowPos,
        resolutionLayerName);
    Viewport viewport = resolutionLayer.getViewport();
    float topPadding = padding.x;
    return scenePos.y - viewport.getY() > hints.getViewportHeight() - topPadding;
  }

  @Override
  protected void moveVx(float dx) {
    viewportCoords.x += (dx * scrollSpeed);
  }

  @Override
  protected void moveVy(float dy) {
    viewportCoords.y += (dy * scrollSpeed);
  }
}
