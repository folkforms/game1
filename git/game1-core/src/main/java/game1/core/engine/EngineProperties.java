package game1.core.engine;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import folkforms.log.Log;
import game1.core.input.KeyboardUtils;
import game1.debug.DebugScopes;
import game1.filesystem.FileSystem;
import game1.filesystem.FileTransformer;
import game1.filesystem.Paths;

/**
 * Manages properties for the game and engine.
 */
public class EngineProperties {

  private FileSystem fileSystem;
  private Properties properties;
  private Properties descriptions;
  private URI gamePropFile;

  public EngineProperties(Paths paths, FileSystem fileSystem) {
    try {
      this.fileSystem = fileSystem;
      this.properties = new Properties();
      this.descriptions = new Properties();

      String corePropertyDescriptions = "properties/core.game1-property-descriptions";
      String coreProperties = "properties/core.game1-properties";
      String toolkitPropertyDescriptions = "properties/toolkit.game1-property-descriptions";
      String toolkitProperties = "properties/toolkit.game1-properties";
      String gamePropertyDescriptions = String.format(
          "src/main/resources/properties/%s.game1-property-descriptions", paths.getGameShortName());
      String gameDefaultProperties = String.format(
          "src/main/resources/properties/%s.game1-default-properties", paths.getGameShortName());
      String gameProperties = String.format("src/main/resources/properties/%s.game1-properties",
          paths.getGameShortName());

      loadDescriptions(fileSystem.load(corePropertyDescriptions), corePropertyDescriptions);
      load(fileSystem.load(coreProperties), coreProperties);
      loadDescriptions(fileSystem.load(toolkitPropertyDescriptions), toolkitPropertyDescriptions);
      load(fileSystem.load(toolkitProperties), toolkitProperties);
      loadDescriptions(fileSystem.load(gamePropertyDescriptions, false), gamePropertyDescriptions);
      load(fileSystem.load(gameDefaultProperties, false), gameDefaultProperties);
      load(fileSystem.load(gameProperties, false), gameProperties);

      debug_printCombinedProperties();
    } catch (IOException ex) {
      Log.error(ex);
      System.exit(1);
    }
  }

  private void loadDescriptions(byte[] bytes, String resourcePath) throws IOException {
    if (bytes != null) {
      InputStream inputStream = new ByteArrayInputStream(bytes);
      Properties props = new Properties();
      props.load(inputStream);
      inputStream.close();
      checkDescriptions(props, resourcePath);
      descriptions.putAll(props);
      Log.scoped(DebugScopes.PROPERTIES, "Loaded property file '%s': %s", resourcePath, props);
    }
  }

  private void checkDescriptions(Properties props, String resourcePath) {
    props.keySet().forEach(key -> {
      if (props.get(key) == null || props.get(key).toString().length() == 0) {
        Log.warn("Empty description for property '%s' in %s", key, resourcePath);
      }
    });
  }

  private void load(byte[] bytes, String resourcePath) throws IOException {
    if (bytes != null) {
      InputStream inputStream = new ByteArrayInputStream(bytes);
      Properties props = new Properties();
      props.load(inputStream);
      inputStream.close();
      checkProperties(props.keySet(), resourcePath);
      properties.putAll(props);
      Log.scoped(DebugScopes.PROPERTIES, "Loaded property file '%s': %s", resourcePath, props);
    }
  }

  private void checkProperties(Set<Object> keySet, String resourcePath) {
    keySet.forEach(key -> {
      if (!descriptions.keySet().contains(key)) {
        Log.warn(
            "No description for property '%s' in %s. If it is a custom property you should add a description to a '<game>.property-descriptions' file.",
            key, resourcePath);
      }
    });
  }

  /**
   * Gets the given property value as a string.
   *
   * @param key
   *          property key
   * @return property value
   */
  public String getProperty(String key) {
    final String value = properties.getProperty(key);
    if (value == null) {
      Log.error("Property '%s' not found", key);
      Thread.dumpStack();
      System.exit(1);
    }
    return value;
  }

  /**
   * Gets the given property value as an integer.
   *
   * @param key
   *          property key
   * @return property value
   */
  public int getIntProperty(String key) {
    int i = 0;
    final String value = getProperty(key);
    try {
      i = Integer.parseInt(value);
    } catch (NumberFormatException ex) {
      Log.error("Unable to parse property '%s' as int, value is '%s'!", key, value);
      Log.error(ex);
      System.exit(1);
    }
    return i;
  }

  /**
   * Gets the given property value as a float.
   *
   * @param key
   *          property key
   * @return property value
   */
  public float getFloatProperty(String key) {
    float f = 0;
    final String value = getProperty(key);
    try {
      f = Float.parseFloat(value);
    } catch (NumberFormatException ex) {
      Log.error("Unable to parse property '%s' as a float, value is '%s'!", key, value);
      Log.error(ex);
      System.exit(1);
    }
    return f;
  }

  /**
   * Gets the given property value as a boolean.
   *
   * @param key
   *          property key
   * @return property value
   */
  public boolean getBooleanProperty(String key) {
    final String value = getProperty(key);
    if (value.equals("false") || value.equals("true")) {
      return Boolean.parseBoolean(value);
    } else {
      Log.error("Unable to parse property '%s' as a boolean, value is '%s'!", key, value);
      Thread.dumpStack();
      System.exit(1);
      return false;
    }
  }

  /**
   * Gets the given property value as an integer array of key codes. The property should be a key
   * string like "A" or "KEY_A".
   *
   * @param keys
   *          the keys as a space-separated string
   * @return the integer codes for the given keys
   */
  public int[] getKeyboardKeysProperty(String keys) {
    String value = getProperty(keys);
    try {
      int[] keyCodes = KeyboardUtils.convert(value);
      return keyCodes;
    } catch (NullPointerException ex) {
      Log.error("Unable to parse property '%s=%s' as a keyboard key.", keys, value);
      Thread.dumpStack();
      System.exit(1);
      return null;
    }
  }

  /**
   * Saves the value of the given property to disk. Reloads the file, replaces the value, and saves
   * the file.
   *
   * @param key
   *          property key
   * @param value
   *          property value
   * @throws IOException
   *           if an error occurs loading or saving the properties file
   */
  // FIXME CORE: Why does it reload the file?
  public void saveProperty(final String key, final Object value) throws IOException {
    properties.setProperty(key, value.toString());

    if (gamePropFile != null) {
      boolean foundExisting = false;

      String resourcePath = new File(gamePropFile).getAbsolutePath();
      List<String> contents = FileTransformer.toList(fileSystem.load(resourcePath), resourcePath);

      String newLine = key + "=" + value;
      for (int i = 0; i < contents.size(); i++) {
        if (contents.get(i).startsWith(key + "=")) {
          contents.set(i, newLine);
          foundExisting = true;
          break;
        }
      }
      if (!foundExisting) {
        contents.add(newLine);
      }
      fileSystem.write(contents, resourcePath);
    }
  }

  private void debug_printCombinedProperties() {
    Log.scoped(DebugScopes.PROPERTIES, "Combined properties:");
    properties.keySet().stream().sorted().forEach(key -> {
      Log.scoped(DebugScopes.PROPERTIES, "    %s => %s", key,
          properties.getProperty(key.toString()));
    });
  }
}
