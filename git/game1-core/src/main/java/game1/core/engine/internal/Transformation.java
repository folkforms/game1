package game1.core.engine.internal;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;

import folkforms.log.Log;
import game1.core.graphics.Drawable;
import game1.resolutions.layers.ResolutionLayer;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;
import game1.resolutions.viewports.Viewport;

public class Transformation {

  private final Matrix4f modelMatrix;
  private final Matrix4f modelViewMatrix;
  private final Matrix4f modelLightViewMatrix;
  private final Matrix4f lightViewMatrix;
  private final Matrix4f orthoProjMatrix;
  private final Matrix4f ortho2DMatrix;
  private final Matrix4f orthoModelMatrix;

  public Transformation() {
    modelMatrix = new Matrix4f();
    modelViewMatrix = new Matrix4f();
    modelLightViewMatrix = new Matrix4f();
    orthoProjMatrix = new Matrix4f();
    ortho2DMatrix = new Matrix4f();
    orthoModelMatrix = new Matrix4f();
    lightViewMatrix = new Matrix4f();
  }

  public final Matrix4f getOrthoProjectionMatrix() {
    return orthoProjMatrix;
  }

  public Matrix4f updateOrthoProjectionMatrix(float left, float right, float bottom, float top,
      float zNear, float zFar) {
    return orthoProjMatrix.setOrtho(left, right, bottom, top, zNear, zFar);
  }

  public Matrix4f getLightViewMatrix() {
    return lightViewMatrix;
  }

  public void setLightViewMatrix(Matrix4f lightViewMatrix) {
    this.lightViewMatrix.set(lightViewMatrix);
  }

  public Matrix4f updateLightViewMatrix(Vector3f position, Vector3f rotation) {
    return updateGenericViewMatrix(position, rotation, lightViewMatrix);
  }

  public static Matrix4f updateGenericViewMatrix(Vector3f position, Vector3f rotation,
      Matrix4f matrix) {
    return matrix.rotationX((float) Math.toRadians(rotation.x))
        .rotateY((float) Math.toRadians(rotation.y))
        .translate(-position.x, -position.y, -position.z);
  }

  /**
   * Gets an orthographic projection for the given values, which are typically
   * <code>{0,windowWidth,0,windowHeight}</code>.
   *
   * @param left
   *          left side of the projection (typically 0)
   * @param right
   *          right side of the projection (typically screen width)
   * @param bottom
   *          bottom side of the projection (typically 0)
   * @param top
   *          top side of the projection (typically screen height)
   * @return orthographic projection
   */
  public final Matrix4f getOrtho2DProjectionMatrix(float left, float right, float bottom,
      float top) {
    return ortho2DMatrix.setOrtho2D(left, right, bottom, top);
  }

  public Matrix4f buildModelMatrix(Quaternionf rotation, Vector3f position, float scale) {
    return modelMatrix.translationRotateScale(position.x, position.y, position.z, rotation.x,
        rotation.y, rotation.z, rotation.w, scale, scale, scale);
  }

  public Matrix4f buildModelViewMatrix(Quaternionf rotation, Vector3f position, float scale,
      Matrix4f viewMatrix) {
    return buildModelViewMatrix(buildModelMatrix(rotation, position, scale), viewMatrix);
  }

  /**
   * Multiply {@code modelMatrix} by {@code viewMatrix}.
   *
   * @param modelMatrix
   *          model matrix
   * @param viewMatrix
   *          view matrix
   * @return the result of multiplying {@code modelMatrix} by {@code viewMatrix}
   */
  public Matrix4f buildModelViewMatrix(Matrix4f modelMatrix, Matrix4f viewMatrix) {
    return viewMatrix.mulAffine(modelMatrix, modelViewMatrix);
  }

  public Matrix4f buildModelLightViewMatrix(Matrix4f modelMatrix, Matrix4f lightViewMatrix) {
    return lightViewMatrix.mulAffine(modelMatrix, modelLightViewMatrix);
  }

  /**
   * Version 2: Optimised code from the book. Need to see if I can improve my version. This version
   * might be more efficient.
   *
   * @deprecated not used
   */
  @Deprecated
  public Matrix4f buildOrthoProjModelMatrix2(Quaternionf rotation, Vector3f position, float scale,
      Matrix4f orthoMatrix) {
    return orthoMatrix.mulOrthoAffine(buildModelMatrix(rotation, position, scale),
        orthoModelMatrix);
  }

  /**
   * Version 1: Original code from the book. Delete this eventually or add an option to my version
   * like "if ( game1.adjust_2D_z_values=false ) { Don't adjust z-values }". This version might be
   * more efficient.
   *
   * @deprecated not used
   */
  @Deprecated
  public Matrix4f buildOrthoProjModelMatrix1(Quaternionf rotation, Vector3f position, float scale,
      Matrix4f orthoMatrix) {
    modelMatrix.identity().translate(position).rotateX((float) Math.toRadians(-rotation.x))
        .rotateY((float) Math.toRadians(-rotation.y)).rotateZ((float) Math.toRadians(-rotation.z))
        .scale(scale);
    orthoModelMatrix.set(orthoMatrix);
    orthoModelMatrix.mul(modelMatrix);
    return orthoModelMatrix;
  }

  /**
   * Creates an orthographic projection for the given {@link Drawable}. The z-index of the Drawable
   * will be converted from a range of -100,000 to 100,000 to a range of -1 to 1. This is so that
   * users can specify z-index in whole numbers rather than messing around with fractions. Note that
   * {@code Integer.MAX_VALUE} should be used for always-on-top values, and
   * {@code Integer.MIN_VALUE} should be used for hidden items.
   *
   * @param drawable
   *          the drawable to project
   * @param orthoMatrix
   *          orthographic 2D projection matrix
   * @return
   */
  public Matrix4f buildOrthoProjModelMatrix(Drawable drawable, Matrix4f orthoMatrix) {
    float offsetX = drawable.getOffsetX();
    float offsetY = drawable.getOffsetY();

    float resolutionScale = 1;
    int resolutionOffsetX = 0;
    int resolutionOffsetY = 0;
    float viewportX = 0;
    float viewportY = 0;

    ResolutionLayer layer = ResolutionLayersFactory.getInstance().getLayerForActor(drawable);
    ResolutionHints hints = layer.getResolutionHints();
    resolutionOffsetX = (int) hints.getResolutionOffsetX();
    resolutionOffsetY = (int) hints.getResolutionOffsetY();
    resolutionScale = hints.getResolutionScale();
    Viewport viewport = layer.getViewport();
    viewportX = viewport.getX() * resolutionScale;
    viewportY = viewport.getY() * resolutionScale;

    Vector3f originalPosition = drawable.getPosition();
    Vector3f position = new Vector3f(
        (originalPosition.x + offsetX) * resolutionScale + resolutionOffsetX - viewportX,
        (originalPosition.y + offsetY) * resolutionScale + resolutionOffsetY - viewportY,
        originalPosition.z);

    // Recalculate z-index from integer in range -100,000 to 100,000 to float in range -1 to 1
    float newZ = (position.z) / 100_000;
    if (position.z == Integer.MAX_VALUE) {
      // ...Special case for Integer.MAX_VALUE used for debug tools that are always on top
      newZ = 0.99999f;
    } else if (position.z == Integer.MIN_VALUE) {
      // ...Special case for Integer.MIN_VALUE used for items that are hidden
      newZ = -1.5f;
    } else if (newZ <= -1 || newZ >= 1) {
      Log.warn("Z index of %s is %s. It must be between -100,000 and 100,000 (both exclusive.) "
          + "Note that the special value Integer.MAX_VALUE can be used for items always on top, and "
          + "Integer.MIN_VALUE can be used make items invisible.", drawable, position.z);
    }

    Quaternionf rotation = drawable.getRotation();
    modelMatrix.translation(position.x, position.y, newZ)
        .rotateX((float) Math.toRadians(-rotation.x)).rotateY((float) Math.toRadians(-rotation.y))
        .rotateZ((float) Math.toRadians(-rotation.z)).scale(drawable.getScale() * resolutionScale);
    orthoModelMatrix.set(orthoMatrix);
    orthoModelMatrix.mul(modelMatrix);
    return orthoModelMatrix;
  }
}
