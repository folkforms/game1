package game1.core.engine;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import folkforms.log.Log;
import game1.actors.Actor;
import game1.actors.Clickable;
import game1.actors.Draggable;
import game1.actors.Focusable;
import game1.actors.ParentChildRegistry;
import game1.actors.ParentChildRegistryFactory;
import game1.actors.Scrollable;
import game1.actors.Tickable;
import game1.actors.actorproperties.ActorPropertiesRegistry;
import game1.core.engine.userstate.UserState;
import game1.core.engine.userstate.UserStateProperty;
import game1.core.graphics.Drawable;
import game1.core.graphics.lights.AmbientLight;
import game1.core.graphics.lights.DirectionalLight;
import game1.core.graphics.lights.Light;
import game1.core.graphics.lights.PointLight;
import game1.core.graphics.lights.Spotlight;
import game1.core.registries.ClickableActors;
import game1.core.registries.CollisionMeshRegistry;
import game1.core.registries.DraggableActors;
import game1.core.registries.FocusableActors;
import game1.core.registries.Lights;
import game1.core.registries.ScrollableActors;
import game1.core.registries.TickableActors;
import game1.debug.DebugScopes;
import game1.debug.dump_data.DebugDataProvider;
import game1.debug.dump_data.DebugDataProviderRegistry;
import game1.events.Game1Events;
import game1.events.ReceiveEvent;
import game1.resolutions.layers.ResolutionLayer;
import game1.resolutions.layers.ResolutionLayers;
import game1.resolutions.layers.ResolutionLayersFactory;

/**
 * The GameState class is responsible for managing the current game state.
 *
 * @see {@link #register(String, UserState)}
 * @see {@link #moveToState(String)}
 * @see {@link UserState}
 */
public final class GameState implements DebugDataProvider {

  private enum StateMutationPolicy {
    ERROR_ON_CHANGE, WARN_ON_CHANGE, ALLOW_CHANGES;
  }

  private Map<String, UserState> userStates = new HashMap<>();
  private UserState currentUserState = null;
  // Needs CopyOnWriteArraySet so threads don't cause ConcurrentModificationExceptions
  private Set<Actor> actors = new CopyOnWriteArraySet<>();
  private boolean isStateSealed = true;
  private StateMutationPolicy stateMutationPolicy = StateMutationPolicy.ERROR_ON_CHANGE;
  private boolean sceneCleared = false;

  public GameState() {
    stateMutationPolicy = StateMutationPolicy
        .valueOf(Engine.PROPERTIES.getProperty("game1.state_mutation_policy"));
    Log.scoped(DebugScopes.STATE, "StateMutationPolicy is %s", stateMutationPolicy);
    DebugDataProviderRegistry.register(this, DebugDataProviderRegistry.Context.FILE);
  }

  public boolean temp_allowStateChanges() {
    return stateMutationPolicy != StateMutationPolicy.ERROR_ON_CHANGE;
  }

  public void register(String name, UserState userState) {
    userStates.put(name, userState);
  }

  @ReceiveEvent(Game1Events.MOVE_TO_STATE)
  public void moveToState(String name) {
    Log.scoped(DebugScopes.STATE, "Moving to state => %s", name);
    UserState userState = userStates.get(name);
    validateStateExists(userState, name);
    isStateSealed = false;
    clearActors();
    try {
      currentUserState = userState;
      userState.preApply();
      userState.apply();
      ensureAllChildrenAreAdded();
      ensureAllChildrenAreInAResolutionLayer();
      userState.postApply();
      isStateSealed = true;
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }
  }

  private static void validateStateExists(UserState userState, String name) {
    if (userState == null) {
      String message = String.format(
          "Unknown state: '%s'. Did you forget to register it? See GameState.registerUserState.",
          name, name);
      Log.error(message);
      throw new RuntimeException(message);
    }
  }

  private void clearActors() {
    for (Actor actor : actors) {
      removeActor(actor);
    }
    TickableActors.clear();
    FocusableActors.clear();
    ClickableActors.clear();
    ScrollableActors.clear();
    DraggableActors.clear();
    Lights.clear();
    ParentChildRegistryFactory.getInstance().clear();
    ActorPropertiesRegistry.clear();
    ResolutionLayersFactory.getInstance().clear();
    sceneCleared = true;
  }

  /**
   * Sometimes an object will be created and added to the state, and afterwards it will have a child
   * added to it. If this happens, the child will not be in the state unless it is explicitly added.
   * In order to avoid people having to remember this and/or forcing them to write their code in a
   * specific order, we just run this method at the last minute to ensure all children are in the
   * state.
   */
  private void ensureAllChildrenAreAdded() {
    ResolutionLayers resolutionLayers = ResolutionLayersFactory.getInstance();
    actors.forEach(
        actor -> ParentChildRegistryFactory.getInstance().getChildren(actor).forEach(child -> {
          ResolutionLayer resolutionLayer = resolutionLayers.getLayerForActor(actor);
          addActor(child, resolutionLayer.getName());
        }));
  }

  private void ensureAllChildrenAreInAResolutionLayer() {
    actors.forEach(actor -> {
      ParentChildRegistry pcr = ParentChildRegistryFactory.getInstance();
      pcr.getChildren(actor).forEach(child -> {
        Actor ancestor = pcr.getTopLevelAncestor(child);
        String layerName = ResolutionLayersFactory.getInstance().getLayerForActor(ancestor)
            .getName();
        if (layerName == null) {
          throw new RuntimeException(String.format(
              "ensureAllChildrenAreInAResolutionLayer: Actor ancestor %s is not in a layer, so I don't know what layer to add its children to",
              ancestor));
        }
        List<Actor> children = pcr.getAllChildren(ancestor);
        children.forEach(c -> ResolutionLayersFactory.getInstance().addActor(c, layerName));
      });
    });
  }

  public void add3dActor(Actor actor) {
    addActor(actor, ResolutionLayers.LAYER_3D);
  }

  public void addActor(Actor actor, String resolutionLayer) {
    checkStateMutationPolicy("addActor", actor);

    if (actors.contains(actor)) {
      return;
    }

    actors.add(actor);

    ResolutionLayersFactory.getInstance().addActor(actor, resolutionLayer);

    if (actor instanceof Tickable) {
      TickableActors.register((Tickable) actor);
    }
    if (actor instanceof Focusable) {
      FocusableActors.register((Focusable) actor);
    }
    if (actor instanceof Clickable) {
      ClickableActors.register((Clickable) actor);
    }
    if (actor instanceof Scrollable) {
      ScrollableActors.register((Scrollable) actor);
    }
    if (actor instanceof Draggable) {
      DraggableActors.register((Draggable) actor);
    }
    if (actor instanceof Drawable) {
      Drawable d = (Drawable) actor;
      Engine.PIPELINES.addDrawable(d);
      if (d.isCollisionMesh()) {
        CollisionMeshRegistry.add(d);
      }
    }
    if (actor instanceof Light) {
      if (actor instanceof Spotlight) {
        Lights.addSpotlight((Spotlight) actor);
      } else if (actor instanceof PointLight) {
        Lights.addPointLight((PointLight) actor);
      } else if (actor instanceof DirectionalLight) {
        Lights.setDirectionalLight((DirectionalLight) actor);
      } else if (actor instanceof AmbientLight) {
        Lights.setAmbientLight((AmbientLight) actor);
      } else {
        Log.warn(
            "Actor '%s' implements Light but is NOT one of Spotlight, Pointlight, DirectionalLight or AmbientLight.");
      }
    }

    String layerName = ResolutionLayersFactory.getInstance().getLayerForActor(actor).getName();
    ParentChildRegistryFactory.getInstance().getChildren(actor)
        .forEach(child -> addActor(child, layerName));
  }

  private void removeActor(Actor actor) {
    checkStateMutationPolicy("removeActor", actor);

    actors.remove(actor);

    if (actor instanceof Drawable) {
      Drawable d = (Drawable) actor;
      Engine.PIPELINES.removeDrawable(d);
      if (d.isCollisionMesh()) {
        CollisionMeshRegistry.remove(d);
      }
    }
  }

  public Set<Actor> internal_listActorsInState() {
    return actors;
  }

  /**
   * Checks if the scene has been cleared since the last poll, returns the value and resets it to
   * false. This is used to short-circuit the tickable code because if a tickable clears the scene
   * (for example, by calling {@link #moveToState(String)}) then the current list of Tickables is no
   * longer valid and we should abort processing and get a fresh list of Tickables next tick.
   */
  public boolean pollSceneCleared() {
    boolean value = sceneCleared;
    sceneCleared = false;
    return value;
  }

  public boolean debug_contains(Actor actor) {
    return actors.contains(actor);
  }

  public void debug_printActors() {
    Log.info("GAME_STATE:");
    actors.forEach(actor -> Log.info("    %s", actor));
  }

  @Override
  public String debug_getDumpFilename() {
    return "dump_game_state_actors.txt";
  }

  @Override
  public List<String> debug_listData() {
    return actors.stream().map(Actor::toString).toList();
  }

  public boolean isStateSealed() {
    return isStateSealed;
  }

  public boolean isMainMenuState() {
    return currentUserState.getBooleanProperty(UserStateProperty.IS_MAIN_MENU);
  }

  public void checkStateMutationPolicy(String description, Actor... actors) {
    if (!isStateSealed) {
      return;
    }
    String end = ".";
    if (actors.length > 0) {
      end = String.format(" with %s.", Arrays.toString(actors));
    }
    String message = String.format(
        "State was changed but game1.state_mutation_policy was set to %s. Tried to call '%s'%s",
        stateMutationPolicy, description, end);
    if (stateMutationPolicy == StateMutationPolicy.WARN_ON_CHANGE) {
      Log.warn(message);
    } else if (stateMutationPolicy == StateMutationPolicy.ERROR_ON_CHANGE) {
      throw new RuntimeException(message);
    }
  }
}
