package game1.core.engine.internal;

import game1.core.engine.Game1RuntimeException;

public class CameraFactory {

  private static Camera camera = null;

  public static Camera getInstance() {
    if (camera == null) {
      throw new Game1RuntimeException(
          "CameraFactory.getInstance() was invoked before CameraFactory.setInstance(Camera) was called.");
    }
    return camera;
  }

  public static void setInstance(Camera newCamera) {
    camera = newCamera;
  }
}
