package game1.core.engine;

import org.joml.Matrix4f;
import org.joml.Vector3f;

import folkforms.log.Log;
import game1.core.engine.internal.Camera;
import game1.core.engine.internal.Transformation;
import game1.core.graphics.Drawable;
import game1.core.pipelines.CameraBoxSelectionDetector;
import game1.events.EventSubscriptionsFactory;
import game1.events.Game1Events;
import game1.events.ReceiveEvent;

public class GameCamera implements Camera {

  private final Vector3f position;
  private final Vector3f rotation;
  private Matrix4f viewMatrix;
  private float mouseSensitivity = 0.02f;
  private CameraBoxSelectionDetector cameraBoxSelectionDetector;

  public GameCamera() {
    EventSubscriptionsFactory.getInstance().subscribe(this);
    position = new Vector3f(0, 0, 0);
    rotation = new Vector3f(0, 0, 0);
    viewMatrix = new Matrix4f();
  }

  @Override
  public Matrix4f getViewMatrix() {
    return viewMatrix;
  }

  @Override
  public Vector3f getPosition() {
    return position;
  }

  @ReceiveEvent(Game1Events.CAMERA_SET_POSITION_V3F)
  public void setPosition(Vector3f v) {
    setPosition(v.x, v.y, v.z);
  }

  @Override
  @ReceiveEvent(Game1Events.CAMERA_SET_POSITION)
  public void setPosition(float x, float y, float z) {
    position.x = x;
    position.y = y;
    position.z = z;
  }

  @ReceiveEvent(Game1Events.CAMERA_MOVE)
  public void movePosition(float offsetX, float offsetY, float offsetZ) {
    if (offsetZ != 0) {
      position.x += (float) Math.sin(Math.toRadians(rotation.y)) * -1.0f * offsetZ;
      position.z += (float) Math.cos(Math.toRadians(rotation.y)) * offsetZ;
    }
    if (offsetX != 0) {
      position.x += (float) Math.sin(Math.toRadians(rotation.y - 90)) * -1.0f * offsetX;
      position.z += (float) Math.cos(Math.toRadians(rotation.y - 90)) * offsetX;
    }
    position.y += offsetY;
  }

  @ReceiveEvent(Game1Events.CAMERA_ROTATE)
  public void moveRotation(float offsetX, float offsetY, float offsetZ) {
    rotation.x += offsetX;
    rotation.y += offsetY;
    rotation.z += offsetZ;
  }

  @Override
  public Vector3f getRotation() {
    return rotation;
  }

  @ReceiveEvent(Game1Events.CAMERA_SET_ROTATION_V3F)
  public void setRotation(Vector3f v) {
    setRotation(v.x, v.y, v.z);
  }

  @Override
  @ReceiveEvent(Game1Events.CAMERA_SET_ROTATION)
  public void setRotation(float x, float y, float z) {
    rotation.x = x;
    rotation.y = y;
    rotation.z = z;
  }

  @Override
  public void setSelectionDetector(CameraBoxSelectionDetector detector) {
    this.cameraBoxSelectionDetector = detector;
  }

  @Override
  public Drawable selectDrawable() {
    if (cameraBoxSelectionDetector != null) {
      return cameraBoxSelectionDetector.selectDrawable();
    }
    return null;
  }

  @Override
  public float getMouseSensitivity() {
    return mouseSensitivity;
  }

  public void debug_printCameraDetails() {
    Log.info("Camera position / rotation = %s, %s, %s, %s, %s, %s", position.x, position.y,
        position.z, rotation.x, rotation.y, rotation.z);
  }

  /**
   * Updates the camera after potential changes to its position, rotation or viewMatrix. This method
   * is automatically called every update.
   */
  @Override
  public void updateCamera() {
    Transformation.updateGenericViewMatrix(position, rotation, viewMatrix);
  }
}
