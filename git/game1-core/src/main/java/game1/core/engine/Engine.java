package game1.core.engine;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import folkforms.args.Args;
import folkforms.log.Log;
import game1.core.devtools.DevKeyBindings;
import game1.core.engine.internal.Camera;
import game1.core.engine.internal.CameraFactory;
import game1.core.engine.internal.DrawLoop;
import game1.core.engine.internal.FrameProfiler;
import game1.core.engine.internal.UpdateHandler;
import game1.core.graphics.Fog;
import game1.core.graphics.FontUtils;
import game1.core.infrastructure.lwjgl3.LWJGL3DrawLoop;
import game1.core.infrastructure.lwjgl3.LWJGL3Infrastructure;
import game1.core.infrastructure.lwjgl3.ShaderException;
import game1.core.input.KeyBindingsManager;
import game1.core.pipelines.BillboardPipeline;
import game1.core.pipelines.Scene2DPipeline;
import game1.core.pipelines.Scene3DPipeline;
import game1.core.pipelines.SkyboxPipeline;
import game1.core.pipelines.WireframePipeline;
import game1.core.registries.GraphicsPipelineRegistry;
import game1.core.registries.TickableAnnotations;
import game1.core.resources.Game1DefaultDataset;
import game1.core.resources.loaders2b.Drawable2DMultiLoader;
import game1.core.resources.loaders2b.SoundEffectMultiLoader;
import game1.core.resources.loaders2b.SpritesheetMultiLoader;
import game1.core.resources.loaders2b.YamlFileMultiLoader;
import game1.core.sound.Mixer;
import game1.core.sound.SoundBoard;
import game1.core.sound2.SoundManager;
import game1.datasets.GameDataFactory;
import game1.datasets.GameFiles;
import game1.datasets.GameFilesPublic;
import game1.datasets.hsqldb.GameFilesDao;
import game1.datasets.hsqldb.GameFilesDatabase;
import game1.debug.DebugScopes;
import game1.events.Events;
import game1.events.EventsFactory;
import game1.filesystem.FileSystem;
import game1.filesystem.FileSystemFactory;
import game1.filesystem.Paths;
import game1.variables.VariablesFactory;

/**
 * The core game engine. Handles setup, threads and common APIs.
 */
public class Engine {

  /**
   * Engine and game properties loaded from disk.
   */
  public static EngineProperties PROPERTIES;

  /**
   * Manages the sets of key bindings available to the application.
   */
  public static KeyBindingsManager KEY_BINDINGS = new KeyBindingsManager();

  public static GameFilesPublic GAME_FILES;

  /**
   * API for playing sound effects and music.
   */
  public static SoundBoard SOUND_BOARD = new SoundBoard();

  /**
   * API for controlling volume levels.
   */
  public static Mixer MIXER;

  /**
   * FIXME SOUND: This is the book's sound manager class. Will need to merge it with my code.
   */
  public static SoundManager SOUND_MANAGER = new SoundManager();

  /**
   * GameState is responsible for managing the current game state and moving between states.
   */
  public static GameState GAME_STATE;

  /**
   * Setting this value to true will halt processing within the engine, such as for ticks and
   * animations. It should not be used in production, but it can be useful in development when
   * debugging or hot-reloading data.
   *
   * <p>
   * If you want to stop the game from ticking (e.g. during a menu screen) you should set
   * {@link Engine#GAME_PAUSED} to <code>true</code>.
   * </p>
   */
  public static boolean INTERNAL_PROCESSING_PAUSED = false;

  /**
   * A flag to denote whether the game is paused or not. Typically used when opening the menu during
   * a game.
   *
   * @see TickableAnnotations.TickEvenIfGamePaused
   */
  public static boolean GAME_PAUSED = false;

  /**
   * Fog to use in 3D games.
   */
  public static Fog FOG = Fog.NO_FOG;

  /**
   * Registry of rendering pipelines used in the game.
   */
  public static GraphicsPipelineRegistry PIPELINES = new GraphicsPipelineRegistry();

  /**
   * Setting this value to true will render collision meshes as visible wireframes.
   */
  public static boolean SHOW_COLLISION_MESHES = false;

  private FrameProfiler frameProfiler = new FrameProfiler();
  private FileSystem fileSystem;

  /**
   * Creates a new Engine object and initialises the infrastructure.
   */
  public Engine(String... args) {
    try {
      initDebugMode(args);
      EventsFactory.setInstance(new Events());
      Paths paths = new Paths();
      initFileSystem(args, paths);
      initInfrastructure();
      initGameFilesDatabase();
      initVariables();

      PROPERTIES = new EngineProperties(paths, fileSystem);

      MIXER = new Mixer();
      GAME_STATE = new GameState();

      CameraFactory.setInstance(new GameCamera());

      SHOW_COLLISION_MESHES = PROPERTIES.getBooleanProperty("game1.show_collision_meshes");

      EngineInternal.INFRASTRUCTURE.init();
      EngineInternal.INFRASTRUCTURE.createWindow();
      EngineInternal.INFRASTRUCTURE.configureKeys();
      EngineInternal.INFRASTRUCTURE.configureInputDevices();

      registerDefaultPipelines();

      loadGame1DefaultDataset();
      initDefaultFont();

      SOUND_MANAGER.init();
      Engine.SOUND_BOARD.play("sounds/two_second_silence.ogg", "playing_two_second_silence");

      KEY_BINDINGS.register("dev", new DevKeyBindings(frameProfiler));
      KEY_BINDINGS.activate("dev");

    } catch (Throwable ex) {
      // If an error happens during engine initialisation but after window creation then for some
      // reason the error is silently discarded and the window freezes. Instead we catch it here.
      Log.error(ex);
      System.exit(1);
    }
  }

  private boolean containsArg(String arg, String[] args) {
    return Arrays.stream(args).anyMatch(a -> a.equals(arg));
  }

  private void initDebugMode(String[] args) throws IOException {
    boolean isProduction = new File("app/.production").exists();
    DebugScopes.init();

    String logFileStrMaybe = new Args(args).get("log-file");
    String logFileStr = logFileStrMaybe != null ? logFileStrMaybe : "game1.log";
    String logFilePath = String.format("%s/%s",
        isProduction ? System.getProperty("user.home") : ".", logFileStr);
    Log.setFile(logFilePath);

    String debugStr = new Args(args).get("debug");
    if (debugStr != null) {
      Log.setLevel(Log.LEVEL_DEBUG);
      DebugScopes.activate(debugStr);
    }
  }

  private void initFileSystem(String[] args, Paths paths) throws IOException {
    boolean warnOnExternalPaths = containsArg("--warn-on-external-paths", args);

    FileSystemFactory.init(paths, warnOnExternalPaths);
    this.fileSystem = FileSystemFactory.getInstance();
  }

  private void initInfrastructure() {
    EngineInternal.INFRASTRUCTURE = new LWJGL3Infrastructure();
    EngineInternal.TEXTURE_LOADER = EngineInternal.INFRASTRUCTURE.getTextureLoader();
  }

  private void initGameFilesDatabase() throws Exception {
    GameFilesDatabase gameFilesDatabase = new GameFilesDatabase();
    GameFilesDao gameFilesDao = new GameFilesDao(gameFilesDatabase);
    EngineInternal.GAME_FILES = new GameFiles(GameDataFactory.getInstance(), gameFilesDao);
    EngineInternal.GAME_FILES.addMultiLoader("spritesheet", SpritesheetMultiLoader.class);
    EngineInternal.GAME_FILES.addMultiLoader("ogg", SoundEffectMultiLoader.class);
    EngineInternal.GAME_FILES.addMultiLoader("obj", Drawable2DMultiLoader.class);
    EngineInternal.GAME_FILES.addMultiLoader("yaml", YamlFileMultiLoader.class);
    GAME_FILES = EngineInternal.GAME_FILES;
  }

  private void initVariables() throws IOException {
    GameVariables variables = new GameVariables();
    variables.apply("src/main/resources/properties/core.yaml");
    VariablesFactory.setInstance(variables);
  }

  public void applyVariables(String filename) throws IOException {
    GameVariables gameVariables = (GameVariables) VariablesFactory.getInstance();
    gameVariables.apply(filename);
  }

  /**
   * Starts the game and moves to <code>initialState</code>.
   *
   * @throws IOException
   *           if an error occurs when starting the engine, typically because a dataset failed to
   *           load
   */
  public void start(String initialState) throws IOException {
    EngineInternal.GAME_FILES.internal_init();
    Engine.PIPELINES.init();
    Engine.GAME_STATE.moveToState(initialState);
    Camera camera = CameraFactory.getInstance();
    DrawLoop drawLoop = new LWJGL3DrawLoop(
        Engine.PROPERTIES.getBooleanProperty("game1.vsync_enabled"), Engine.PIPELINES, camera,
        new UpdateHandler(camera), Engine.PIPELINES, frameProfiler);
    drawLoop.start();
  }

  /**
   * Loads a dataset that contains Game1 defaults such as font and cursor images.
   *
   * @throws IOException
   *           if an error occurs loading the data
   */
  private void loadGame1DefaultDataset() throws IOException {
    new Game1DefaultDataset();
    EngineInternal.GAME_FILES.internal_blockingLoadTag(Game1DefaultDataset.TAG);
  }

  /**
   * Initialises the default font. This font is expected to have been loaded by
   * {@link #loadGame1DefaultDataset()} earlier.
   */
  private void initDefaultFont() {
    FontUtils.DEFAULT_FONT_FACE = GameDataFactory.getInstance()
        .get(PROPERTIES.getProperty("game1.default_font.sprite"));
    FontUtils.DEFAULT_FONT_SIZE = PROPERTIES.getIntProperty("game1.default_font.scale");
  }

  private void registerDefaultPipelines() throws IOException, ShaderException {
    Engine.PIPELINES.register(new Scene3DPipeline());
    Engine.PIPELINES.register(new WireframePipeline());
    Engine.PIPELINES.register(new SkyboxPipeline());
    Engine.PIPELINES.register(new BillboardPipeline());
    Engine.PIPELINES.register(new Scene2DPipeline());
  }
}
