package game1.core.engine.internal;

import org.joml.Matrix4f;
import org.joml.Vector3f;

import game1.core.graphics.Drawable;
import game1.core.pipelines.CameraBoxSelectionDetector;

public interface Camera {

  Matrix4f getViewMatrix();

  Vector3f getPosition();

  void setPosition(float x, float y, float z);

  Vector3f getRotation();

  void setRotation(float x, float y, float z);

  void updateCamera();

  float getMouseSensitivity();

  void setSelectionDetector(CameraBoxSelectionDetector detector);

  Drawable selectDrawable();
}
