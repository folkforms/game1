package game1.core.engine;

public class Game1RuntimeException extends RuntimeException {

  public Game1RuntimeException(String format, Object... args) {
    super(String.format(format, args));
  }

  public Game1RuntimeException(Throwable cause, String format, Object... args) {
    super(String.format(format, args), cause);
  }
}
