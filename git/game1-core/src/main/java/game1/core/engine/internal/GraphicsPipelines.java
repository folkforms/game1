package game1.core.engine.internal;

import org.joml.Matrix4f;

public interface GraphicsPipelines {

  void renderAll(Matrix4f viewMatrix);
}
