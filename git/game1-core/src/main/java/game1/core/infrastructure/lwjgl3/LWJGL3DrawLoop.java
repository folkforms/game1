package game1.core.infrastructure.lwjgl3;

import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;

import game1.core.engine.Engine;
import game1.core.engine.internal.Camera;
import game1.core.engine.internal.DrawLoop;
import game1.core.engine.internal.FrameProfiler;
import game1.core.engine.internal.GarbageCollector;
import game1.core.engine.internal.GraphicsPipelines;
import game1.core.engine.internal.UpdateHandler;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3Window;
import game1.core.pipelines.GraphicsPipeline;

public class LWJGL3DrawLoop extends DrawLoop {

  public LWJGL3DrawLoop(boolean vSync, GraphicsPipelines pipelines, Camera camera,
      UpdateHandler updateHandler, GarbageCollector garbageCollector, FrameProfiler frameProfiler) {
    super(pipelines, camera, updateHandler, garbageCollector, frameProfiler);
    // FIXME WINDOWS: This should be called every time the window size or the near/far planes change
    LWJGL3Window.updateProjectionMatrix();
    if (vSync) {
      GLFW.glfwSwapInterval(1);
    }
  }

  @Override
  protected void setClearColor() {
    GL11.glClearColor(0, 0, 0, 0);
  }

  public void clearBuffer() {
    GL30.glClear(GL30.GL_COLOR_BUFFER_BIT | GL30.GL_DEPTH_BUFFER_BIT);
  }

  @Override
  protected boolean shouldStop() {
    return glfwWindowShouldClose(LWJGL3Window.windowHandle);
  }

  @Override
  protected void preDraw() {
    clearBuffer();

    debug_reloadShaders();
  }

  @Override
  protected void postDraw() {

    GLFW.glfwSwapBuffers(LWJGL3Window.windowHandle);

    GLFW.glfwPollEvents();
  }

  @Override
  protected void cleanup() {
    // Log.temp("FIXME CLEANUP: LWJGL3DrawLoop.cleanup not implemented yet");
    // // Mouse.destroy(); // FIXME Mouse.destroy() equivalent?
    // // Keyboard.destroy(); // FIXME Keyboard.destroy() equivalent?
    // // Display.destroy();
    // glfwDestroyWindow(LWJGL3Display.window);
    // glfwTerminate();

    // FIXME CLEANUP: SOUNDS: Should this be called here or in some other cleanup method... because
    // OpenAL is not specific to LWJGL3 I don't think?
    // Engine.SOUND_MANAGER.cleanup();
  }

  /**
   * Allows hot-reloading of shaders.
   */
  private void debug_reloadShaders() {
    if (GraphicsPipeline.debug_shaderModified) {
      Engine.PIPELINES.debug_reloadShaders();
    }
    GraphicsPipeline.debug_shaderModified = false;
  }
}
