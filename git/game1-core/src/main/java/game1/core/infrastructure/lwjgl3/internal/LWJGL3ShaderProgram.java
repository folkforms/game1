package game1.core.infrastructure.lwjgl3.internal;

import static org.lwjgl.opengl.GL20.GL_COMPILE_STATUS;
import static org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER;
import static org.lwjgl.opengl.GL20.GL_LINK_STATUS;
import static org.lwjgl.opengl.GL20.GL_VALIDATE_STATUS;
import static org.lwjgl.opengl.GL20.GL_VERTEX_SHADER;
import static org.lwjgl.opengl.GL20.glAttachShader;
import static org.lwjgl.opengl.GL20.glCompileShader;
import static org.lwjgl.opengl.GL20.glCreateProgram;
import static org.lwjgl.opengl.GL20.glCreateShader;
import static org.lwjgl.opengl.GL20.glDeleteProgram;
import static org.lwjgl.opengl.GL20.glDetachShader;
import static org.lwjgl.opengl.GL20.glGetProgramInfoLog;
import static org.lwjgl.opengl.GL20.glGetProgrami;
import static org.lwjgl.opengl.GL20.glGetShaderInfoLog;
import static org.lwjgl.opengl.GL20.glGetShaderi;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glLinkProgram;
import static org.lwjgl.opengl.GL20.glShaderSource;
import static org.lwjgl.opengl.GL20.glUniform1f;
import static org.lwjgl.opengl.GL20.glUniform1i;
import static org.lwjgl.opengl.GL20.glUniform2f;
import static org.lwjgl.opengl.GL20.glUniform3f;
import static org.lwjgl.opengl.GL20.glUniform4f;
import static org.lwjgl.opengl.GL20.glUniformMatrix4fv;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL20.glValidateProgram;
import static org.lwjgl.opengl.GL32.GL_GEOMETRY_SHADER;
import static org.lwjgl.opengl.GL41.glProgramUniform1f;
import static org.lwjgl.opengl.GL41.glProgramUniform3f;

import java.io.IOException;
import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.Map;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import org.lwjgl.system.MemoryStack;

import folkforms.log.Log;
import game1.core.engine.Engine;
import game1.core.graphics.Fog;
import game1.core.graphics.Material;
import game1.core.graphics.lights.DirectionalLight;
import game1.core.graphics.lights.PointLight;
import game1.core.graphics.lights.Spotlight;
import game1.core.infrastructure.lwjgl3.ShaderException;
import game1.core.registries.Lights;
import game1.filesystem.FileSystem;
import game1.filesystem.FileSystemFactory;

public class LWJGL3ShaderProgram {

  private final int programId;
  private int vertexShaderId;
  private int fragmentShaderId;
  // FIXME MISC_STUFF: What is geometryShaderId? It doesn't seem to be used...
  private int geometryShaderId;
  private final Map<String, Integer> uniforms;

  public LWJGL3ShaderProgram(String vertexShaderProperty, String fragmentShaderProperty,
      String geometryShaderProperty) throws ShaderException, IOException {
    programId = glCreateProgram();
    if (programId == 0) {
      throw new ShaderException("Could not create Shader");
    }
    uniforms = new HashMap<>();

    String vertexShaderPath = Engine.PROPERTIES.getProperty(vertexShaderProperty);
    String fragmentShaderPath = Engine.PROPERTIES.getProperty(fragmentShaderProperty);
    String geometryShaderPath = Engine.PROPERTIES.getProperty(geometryShaderProperty);
    FileSystem fileSystem = FileSystemFactory.getInstance();
    createVertexShader(new String(fileSystem.load(vertexShaderPath)));
    createFragmentShader(new String(fileSystem.load(fragmentShaderPath)));
    createGeometryShader(new String(fileSystem.load(geometryShaderPath)));
    link();
  }

  public LWJGL3ShaderProgram(String vertexShaderProperty, String fragmentShaderProperty)
      throws ShaderException, IOException {
    programId = glCreateProgram();
    if (programId == 0) {
      throw new ShaderException("Could not create Shader");
    }
    uniforms = new HashMap<>();

    String vertexShaderPath = Engine.PROPERTIES.getProperty(vertexShaderProperty);
    String fragmentShaderPath = Engine.PROPERTIES.getProperty(fragmentShaderProperty);
    FileSystem fileSystem = FileSystemFactory.getInstance();
    createVertexShader(new String(fileSystem.load(vertexShaderPath)));
    createFragmentShader(new String(fileSystem.load(fragmentShaderPath)));
    link();
  }

  protected void createUniform(String uniformName) throws ShaderException {
    int uniformLocation = glGetUniformLocation(programId, uniformName);
    if (uniformLocation < 0) {
      throw new ShaderException(String.format(
          "Could not find uniform: %s (Note that unused uniforms are optimised away.)",
          uniformName));
    }
    uniforms.put(uniformName, uniformLocation);
  }

  protected void createPointLightListUniform(String uniformName, int size) throws ShaderException {
    for (int i = 0; i < size; i++) {
      createPointLightUniform(uniformName + "[" + i + "]");
    }
  }

  protected void createPointLightUniform(String uniformName) throws ShaderException {
    createUniform(uniformName + ".colour");
    createUniform(uniformName + ".position");
    createUniform(uniformName + ".intensity");
    createUniform(uniformName + ".att.constant");
    createUniform(uniformName + ".att.linear");
    createUniform(uniformName + ".att.exponent");
  }

  protected void createSpotlightListUniform(String uniformName, int size) throws ShaderException {
    for (int i = 0; i < size; i++) {
      createSpotLightUniform(uniformName + "[" + i + "]");
    }
  }

  protected void createSpotLightUniform(String uniformName) throws ShaderException {
    createPointLightUniform(uniformName + ".pl");
    createUniform(uniformName + ".conedir");
    createUniform(uniformName + ".cutoff");
  }

  protected void createDirectionalLightUniform(String uniformName) throws ShaderException {
    createUniform(uniformName + ".colour");
    createUniform(uniformName + ".direction");
    createUniform(uniformName + ".intensity");
  }

  protected void createMaterialUniform(String uniformName) throws ShaderException {
    createUniform(uniformName + ".ambient");
    createUniform(uniformName + ".diffuse");
    createUniform(uniformName + ".specular");
    createUniform(uniformName + ".hasTexture");
    createUniform(uniformName + ".hasNormalMap");
    createUniform(uniformName + ".reflectance");
  }

  protected void createFogUniform(String uniformName) throws ShaderException {
    createUniform(uniformName + ".isActive");
    createUniform(uniformName + ".colour");
    createUniform(uniformName + ".density");
  }

  protected void createStencilUniform() throws ShaderException {
    createUniform("stencil.type");
    createUniform("stencil.area");
    createUniform("stencil.colour");
  }

  public void setUniform(String uniformName, Matrix4f value) {
    try (MemoryStack stack = MemoryStack.stackPush()) {
      // Dump the matrix into a float buffer
      FloatBuffer fb = stack.mallocFloat(16);
      value.get(fb);
      glUniformMatrix4fv(uniforms.get(uniformName), false, fb);
    }
  }

  public void setUniform(String uniformName, Matrix4f[] matrices) {
    try (MemoryStack stack = MemoryStack.stackPush()) {
      int length = matrices != null ? matrices.length : 0;
      FloatBuffer fb = stack.mallocFloat(16 * length);
      for (int i = 0; i < length; i++) {
        matrices[i].get(16 * i, fb);
      }
      glUniformMatrix4fv(uniforms.get(uniformName), false, fb);
    }
  }

  public void setUniform(String uniformName, int value) {
    glUniform1i(uniforms.get(uniformName), value);
  }

  public void setUniform(String uniformName, float value) {
    glUniform1f(uniforms.get(uniformName), value);
  }

  public void setUniform(String uniformName, Vector2f value) {
    glUniform2f(uniforms.get(uniformName), value.x, value.y);
  }

  public void setUniform(String uniformName, Vector3f value) {
    glUniform3f(uniforms.get(uniformName), value.x, value.y, value.z);
  }

  public void setUniform(String uniformName, Vector4f value) {
    glUniform4f(uniforms.get(uniformName), value.x, value.y, value.z, value.w);
  }

  public void setUniform(String uniformName, PointLight pointLight, int pos) {
    setUniform(uniformName + "[" + pos + "]", pointLight);
  }

  public void setUniform(String uniformName, PointLight pointLight) {
    setUniform(uniformName + ".colour", pointLight.getColour());
    setUniform(uniformName + ".position", pointLight.getPosition());
    setUniform(uniformName + ".intensity", pointLight.getIntensity());
    PointLight.Attenuation att = pointLight.getAttenuation();
    setUniform(uniformName + ".att.constant", att.getConstant());
    setUniform(uniformName + ".att.linear", att.getLinear());
    setUniform(uniformName + ".att.exponent", att.getExponent());
  }

  public void setUniform(String uniformName, Spotlight spotLight, int pos) {
    setUniform(uniformName + "[" + pos + "]", spotLight);
  }

  public void setUniform(String uniformName, Spotlight spotLight) {
    setUniform(uniformName + ".pl", spotLight.getPointLight());
    setUniform(uniformName + ".conedir", spotLight.getConeDirection());
    setUniform(uniformName + ".cutoff", spotLight.internal_getCutoff());
  }

  public void setUniform(String uniformName, DirectionalLight dirLight) {
    setUniform(uniformName + ".colour", dirLight.getColour());
    setUniform(uniformName + ".direction", dirLight.getDirection());
    setUniform(uniformName + ".intensity", dirLight.getIntensity());
  }

  public void setUniform(String uniformName, Material material) {
    setUniform(uniformName + ".ambient", material.getAmbientColour());
    setUniform(uniformName + ".diffuse", material.getDiffuseColour());
    setUniform(uniformName + ".specular", material.getSpecularColour());
    setUniform(uniformName + ".hasTexture", material.getTexture() != null ? 1 : 0);
    setUniform(uniformName + ".hasNormalMap", material.hasNormalMap() ? 1 : 0);
    setUniform(uniformName + ".reflectance", material.getReflectance());
  }

  public void setUniform(String uniformName, Fog fog) {
    setUniform(uniformName + ".isActive", fog.isActive() ? 1 : 0);
    setUniform(uniformName + ".colour", fog.getColour());
    setUniform(uniformName + ".density", fog.getDensity());
  }

  private void createVertexShader(String shaderCode) throws ShaderException {
    vertexShaderId = createShader(shaderCode, GL_VERTEX_SHADER);
  }

  private void createFragmentShader(String shaderCode) throws ShaderException {
    fragmentShaderId = createShader(shaderCode, GL_FRAGMENT_SHADER);
  }

  private void createGeometryShader(String shaderCode) throws ShaderException {
    geometryShaderId = createShader(shaderCode, GL_GEOMETRY_SHADER);
  }

  private int createShader(String shaderCode, int shaderType) throws ShaderException {
    int shaderId = glCreateShader(shaderType);
    if (shaderId == 0) {
      throw new ShaderException("Error creating shader. Type: " + shaderType);
    }

    glShaderSource(shaderId, shaderCode);
    glCompileShader(shaderId);

    if (glGetShaderi(shaderId, GL_COMPILE_STATUS) == 0) {
      throw new ShaderException(glGetShaderInfoLog(shaderId, 100_000));
    }

    glAttachShader(programId, shaderId);

    return shaderId;
  }

  private void link() throws ShaderException {
    glLinkProgram(programId);
    if (glGetProgrami(programId, GL_LINK_STATUS) == 0) {
      throw new ShaderException(
          "Error linking shader code: " + glGetProgramInfoLog(programId, 100_000));
    }

    if (vertexShaderId != 0) {
      glDetachShader(programId, vertexShaderId);
    }
    if (fragmentShaderId != 0) {
      glDetachShader(programId, fragmentShaderId);
    }
    if (geometryShaderId != 0) {
      glDetachShader(programId, geometryShaderId);
    }

    glValidateProgram(programId);
    if (glGetProgrami(programId, GL_VALIDATE_STATUS) == 0) {
      Log.warn(String.format("Warning validating Shader code: %s",
          glGetProgramInfoLog(programId, 100_000)));
    }
  }

  public void bind() {
    glUseProgram(programId);
  }

  public void unbind() {
    glUseProgram(0);
  }

  public void clearLightUniforms() {
    clearUniform3f("ambientLight");
    clearUniform1f("specularPower");
    clearPointLightUniforms();
    clearSpotlightUniforms();
    clearDirectionalLightUniform();
  }

  private void clearPointLightUniforms() {
    for (int i = 0; i < Lights.MAX_POINT_LIGHTS; i++) {
      String uniformName = "pointLights[" + i + "]";
      clearPointLightUniform(uniformName);
    }
  }

  private void clearPointLightUniform(String uniformName) {
    clearUniform3f(uniformName + ".colour");
    clearUniform3f(uniformName + ".position");
    clearUniform1f(uniformName + ".intensity");
    clearUniform1f(uniformName + ".att.constant");
    clearUniform1f(uniformName + ".att.linear");
    clearUniform1f(uniformName + ".att.exponent");
  }

  private void clearSpotlightUniforms() {
    for (int i = 0; i < Lights.MAX_SPOTLIGHTS; i++) {
      String uniformName = "spotlights[" + i + "]";
      clearSpotlightUniform(uniformName);
    }
  }

  private void clearSpotlightUniform(String uniformName) {
    clearPointLightUniform(uniformName + ".pl");
    clearUniform3f(uniformName + ".conedir");
    clearUniform1f(uniformName + ".cutoff");
  }

  private void clearDirectionalLightUniform() {
    String uniformName = "directionalLight";
    clearUniform3f(uniformName + ".colour");
    clearUniform3f(uniformName + ".direction");
    clearUniform1f(uniformName + ".intensity");
  }

  private void clearUniform3f(String uniformName) {
    glProgramUniform3f(programId, uniforms.get(uniformName), 0, 0, 0);
  }

  private void clearUniform1f(String uniformName) {
    glProgramUniform1f(programId, uniforms.get(uniformName), 0);
  }

  public void cleanup() {
    unbind();
    if (programId != 0) {
      glDeleteProgram(programId);
    }
  }
}
