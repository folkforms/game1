package game1.core.infrastructure.lwjgl3.internal;

import static org.lwjgl.glfw.GLFW.glfwGetMonitors;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.lwjgl.PointerBuffer;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GLCapabilities;

import folkforms.log.Log;
import game1.core.engine.Game1RuntimeException;
import game1.variables.VariablesFactory;

public class CheckGLCapabilities {

  private Map<String, Boolean> openGlVersions = new LinkedHashMap<>();

  public CheckGLCapabilities(GLCapabilities caps) {
    setSupportedopenGLVersions(caps);
    selectOpenGLVersion(caps);
    printSupportedOpenGLVersions(caps);
    printMonitorInformation();
  }

  public void setSupportedopenGLVersions(GLCapabilities caps) {
    openGlVersions.put("4.6", caps.OpenGL46);
    openGlVersions.put("4.5", caps.OpenGL45);
    openGlVersions.put("4.4", caps.OpenGL44);
    openGlVersions.put("4.3", caps.OpenGL43);
    openGlVersions.put("4.2", caps.OpenGL42);
    openGlVersions.put("4.1", caps.OpenGL41);
    openGlVersions.put("4.0", caps.OpenGL40);
    openGlVersions.put("3.3", caps.OpenGL33);
    openGlVersions.put("3.2", caps.OpenGL32);
    openGlVersions.put("3.1", caps.OpenGL31);
    openGlVersions.put("3.0", caps.OpenGL30);
    openGlVersions.put("2.1", caps.OpenGL21);
    openGlVersions.put("2.0", caps.OpenGL20);
    openGlVersions.put("1.5", caps.OpenGL15);
    openGlVersions.put("1.4", caps.OpenGL14);
    openGlVersions.put("1.3", caps.OpenGL13);
    openGlVersions.put("1.2", caps.OpenGL12);
    openGlVersions.put("1.1", caps.OpenGL11);
  }

  public void printSupportedOpenGLVersions(GLCapabilities caps) {
    Log.info("Supported OpenGL versions:");
    Log.info("    OpenGL 3.3 supported? %s", caps.OpenGL33);
    Log.info("    OpenGL 4.5 supported? %s", caps.OpenGL45);
    String highestSupportedVersion = openGlVersions.keySet().stream()
        .filter(key -> openGlVersions.get(key) == true).findFirst().orElse("Unknown");
    Log.info("    Highest OpenGL version supported: OpenGL %s", highestSupportedVersion);
    List<String> notSupported = openGlVersions.keySet().stream()
        .filter(key -> openGlVersions.get(key) == false).collect(Collectors.toList());
    List<String> supported = openGlVersions.keySet().stream()
        .filter(key -> openGlVersions.get(key) == true).collect(Collectors.toList());
    Log.info("    Not supported: %s", notSupported);
    Log.info("    Supported: %s", supported);
  }

  private void printMonitorInformation() {
    Log.info("Monitors:");
    long primaryMonitorId = glfwGetPrimaryMonitor();
    PointerBuffer monitors = glfwGetMonitors();
    while (monitors.hasRemaining()) {
      long monitorId = monitors.get();
      GLFWVidMode vidmode = glfwGetVideoMode(monitorId);
      Log.info("    %s x %s%s", vidmode.width(), vidmode.height(),
          monitorId == primaryMonitorId ? " (primary)" : "");
    }
  }

  private void selectOpenGLVersion(GLCapabilities caps) {
    String preferredOpenGLVersion = VariablesFactory.getInstance()
        .get("game1.graphics.preferred_open_gl_version");
    List<String> allowedOpenGLVersions = VariablesFactory.getInstance()
        .get("game1.graphics.allowed_open_gl_versions");
    if (openGlVersions.get(preferredOpenGLVersion)) {
      setOpenGLVersion(preferredOpenGLVersion);
    } else {
      Optional<String> highestAllowedVersionMaybe = allowedOpenGLVersions.stream()
          .filter(version -> openGlVersions.get(version) == true).findFirst();
      if (highestAllowedVersionMaybe.isPresent()) {
        String highestAllowedVersion = highestAllowedVersionMaybe.get();
        setOpenGLVersion(highestAllowedVersion);
      } else {
        throw new Game1RuntimeException(String.format(
            "No supported OpenGL versions found. OpenGL 4.5 or 3.3 are required. (openGlVersions = %s)",
            openGlVersions));
      }
    }
  }

  private void setOpenGLVersion(String openGLVersion) {
    VariablesFactory.getInstance().set("game1.graphics.open_gl_version", openGLVersion);
    Log.info("Using OpenGL %s", openGLVersion);
  }
}
