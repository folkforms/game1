package game1.core.infrastructure;

/**
 * Convenience class for working with Textures.
 */
public class Texture {

  protected int textureId, width, height;

  /**
   * Create a new Texture from the given textureId, width and height. The textureId is generated
   * when the texture is loaded using, for example,
   * {@link TextureLoader#loadTexture(java.awt.image.BufferedImage)}.
   *
   * @param textureId
   *          texture ID
   * @param width
   *          texture width
   * @param height
   *          texture height
   */
  public Texture(int textureId, int width, int height) {
    this.textureId = textureId;
    this.width = width;
    this.height = height;
  }

  /**
   * Gets the texture ID.
   *
   * @return the texture ID
   */
  public int getTextureId() {
    return textureId;
  }

  /**
   * Gets the texture width.
   *
   * @return the texture width
   */
  public int getWidth() {
    return width;
  }

  /**
   * Gets the texture height.
   *
   * @return the texture height
   */
  public int getHeight() {
    return height;
  }

  @Override
  public String toString() {
    return String.format("%s[id=%s,w=%s,h=%s]@%s", getClass().getSimpleName(), textureId, width,
        height, hashCode());
  }
}
