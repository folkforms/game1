package game1.core.infrastructure.lwjgl3.internal;

import static org.lwjgl.glfw.GLFW.GLFW_CONTEXT_VERSION_MAJOR;
import static org.lwjgl.glfw.GLFW.GLFW_CONTEXT_VERSION_MINOR;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_COMPAT_PROFILE;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_CORE_PROFILE;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_FORWARD_COMPAT;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_PROFILE;
import static org.lwjgl.glfw.GLFW.GLFW_SAMPLES;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_DEPTH_TEST;
import static org.lwjgl.opengl.GL11.GL_FRONT_AND_BACK;
import static org.lwjgl.opengl.GL11.GL_LINE;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glClearColor;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glPolygonMode;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.io.IOException;

import org.joml.Matrix4f;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GLCapabilities;
import org.lwjgl.system.Callback;

import folkforms.log.Log;
import game1.core.engine.Engine;
import game1.core.engine.internal.DrawLoop;
import game1.resolutions.layers.ResolutionLayersFactory;

public class LWJGL3Window {

  // Keep a strong reference to the callbacks to avoid garbage collection
  @SuppressWarnings("unused")
  private static GLFWErrorCallback errorCallback;
  @SuppressWarnings("unused")
  private static Callback debugMessageCallback;
  private static String title;
  private static int windowWidth, windowHeight;
  public static long windowHandle;
  private static boolean resized;
  private static boolean vSync;
  private static boolean isCompatibleProfile;
  private static final float FOV = (float) Math.toRadians(60.0f);
  private static final float Z_NEAR = 0.01f;
  private static final float Z_FAR = 1000.f;
  private static Matrix4f projectionMatrix = new Matrix4f();

  public static void init() throws IOException {
    // Initialize GLFW. Most GLFW functions will not work before doing this.
    if (!glfwInit()) {
      throw new IllegalStateException("Unable to initialize GLFW");
    }

    // Setup an error callback to print the error message to System.err.
    GLFWErrorCallback.createPrint(System.err).set();

    glfwDefaultWindowHints(); // optional, the current window hints are already the default
    // glfwWindowHint(GLFW_VISIBLE, GL_FALSE); // the window will stay hidden after creation
    // glfwWindowHint(GLFW_RESIZABLE, GL_FALSE); // the window will be resizable
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

    isCompatibleProfile = Engine.PROPERTIES.getBooleanProperty("game1.use_compatible_profile");
    if (isCompatibleProfile) {
      glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
    } else {
      glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
      glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    }

    glfwWindowHint(GLFW_SAMPLES, 4);

    // GLFW.glfwWindowHint(GLFW.GLFW_MAXIMIZED, GLFW.GLFW_FALSE);

    title = Engine.PROPERTIES.getProperty("window.title");
    long primaryMonitorId = glfwGetPrimaryMonitor();
    GLFWVidMode vidmode = glfwGetVideoMode(primaryMonitorId);
    windowWidth = vidmode.width();
    windowHeight = vidmode.height();
    windowHandle = glfwCreateWindow(windowWidth, windowHeight, title, primaryMonitorId, 0);
    if (windowHandle == NULL) {
      throw new RuntimeException("Failed to create the GLFW window");
    }

    ResolutionLayersFactory.getInstance().init(windowWidth, windowHeight);
    Log.info("Window size: %s x %s", windowWidth, windowHeight);

    // // Setup resize callback
    // glfwSetFramebufferSizeCallback(windowHandle, (window, width, height) -> {
    // this.width = width;
    // this.height = height;
    // this.setResized(true);
    // });

    // Get the resolution of the primary monitor
    // GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    // Position our window
    glfwSetWindowPos(windowHandle, 0, 0);

    // Make the OpenGL context current
    glfwMakeContextCurrent(windowHandle);

    if (vSync) {
      glfwSwapInterval(1);
    } else {
      glfwSwapInterval(0);
    }

    // Make the window visible
    glfwShowWindow(windowHandle);

    GLCapabilities caps = GL.createCapabilities();
    new CheckGLCapabilities(caps);

    // Turn on debug mode (very verbose)
    // debugMessageCallback = GLUtil.setupDebugMessageCallback();

    // Set the clear colour
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

    // Enable depth testing. Prevents hidden fragments from being drawn.
    glEnable(GL_DEPTH_TEST);

    // Show lines
    if (Engine.PROPERTIES.getBooleanProperty("game1.show_triangles")) {
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }

    // Support for transparencies
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // Enable face culling
    if (Engine.PROPERTIES.getBooleanProperty("game1.enable_face_culling")) {
      glEnable(GL_CULL_FACE);
    }

    // Load window icons
    IconLoader.load();
  }

  public static long getWindowHandle() {
    return windowHandle;
  }

  public static void setClearColour(float r, float g, float b, float alpha) {
    glClearColor(r, g, b, alpha);
  }

  public static int getWidth() {
    return windowWidth;
  }

  public static int getHeight() {
    return windowHeight;
  }

  public static boolean isResized() {
    return resized;
  }

  public static void setResized(boolean newValue) {
    resized = newValue;
  }

  public static boolean isvSync() {
    return vSync;
  }

  public static void setvSync(boolean newValue) {
    vSync = newValue;
  }

  public static void update() {
    glfwSwapBuffers(windowHandle);
    glfwPollEvents();
  }

  public static boolean isCompatibleProfile() {
    return isCompatibleProfile;
  }

  public static Matrix4f getProjectionMatrix() {
    return projectionMatrix;
  }

  public static Matrix4f updateProjectionMatrix() {
    float aspectRatio = (float) windowWidth / (float) windowHeight;
    return projectionMatrix.setPerspective(FOV, aspectRatio, Z_NEAR, Z_FAR);
  }

  /**
   * Closes the window and exits the game. This method sets <code>glfwSetWindowShouldClose</code>
   * which will be detected in {@link DrawLoop} which will begin the cleanup and shutdown process.
   */
  public static void close() {
    glfwSetWindowShouldClose(windowHandle, true);
  }
}
