package game1.core.infrastructure.lwjgl3;

import java.io.IOException;

import org.lwjgl.glfw.GLFW;

import game1.core.infrastructure.Infrastructure;
import game1.core.infrastructure.TextureLoader;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3Window;
import game1.core.input.InputDeviceRegistry;

public class LWJGL3Infrastructure implements Infrastructure {

  @Override
  public void init() throws Exception {
    // loadNativeDlls();
  }

  @Override
  public void createWindow() throws IOException {
    LWJGL3Window.init();
  }

  @Override
  public void configureKeys() {
    LWJGL3Keys.configureKeys();
  }

  @Override
  public void configureInputDevices() throws IOException {
    InputDeviceRegistry.register("keyboard", new LWJGL3KeyboardInputDevice());
    InputDeviceRegistry.register("mouse", new LWJGL3MouseInputDevice());
    int controllerId = checkForControllers();
    if (controllerId != -1) {
      InputDeviceRegistry.register("controller", new LWJGL3ControllerInputDevice(controllerId));
    }
  }

  // FIXME CONTROLLER: This should return a list of all controller IDs found
  private int checkForControllers() {
    return GLFW.glfwJoystickPresent(GLFW.GLFW_JOYSTICK_1) ? GLFW.GLFW_JOYSTICK_1 : -1;
  }

  // private static void loadNativeDlls() {
  // // FIXME Need to load Linux/Mac DLLs as well
  // Log.scoped("Loading native DLLs");
  // String path = Paths
  // .replaceGame1Folder(Engine.PROPERTIES.getProperty("org.lwjgl.librarypath.windows"));
  // File f = new File(path);
  // String cleanPath = f.getAbsolutePath();
  // try {
  // cleanPath = f.getCanonicalPath();
  // } catch (IOException ex) {
  // // Do nothing: If we cannot get the canonical path we will fall back to the absolute path
  // }
  // System.setProperty("org.lwjgl.librarypath", cleanPath);
  // Log.scoped(" OS is Windows: Set org.lwjgl.librarypath to '%s'", cleanPath);
  // }

  @Override
  public TextureLoader getTextureLoader() {
    return new LWJGL3TextureLoader();
  }
}
