package game1.core.infrastructure;

import java.io.IOException;

import game1.core.input.Keys;

/**
 * Provides the infrastructure for things like graphics, resources, keyboard, mouse, etc. that may
 * be specific to the graphics library used.
 */
public interface Infrastructure {

  /**
   * Initialises the infrastructure. Performs any tasks that need to be done up front, and/or any
   * miscellaneous tasks that can be done now.
   */
  public void init() throws Exception;

  /**
   * Creates the window.
   *
   * @throws IOException
   *           if an errors occurs setting up the creating the window
   */
  public void createWindow() throws IOException;

  /**
   * Configures the {@link Keys} object to use the key codes from the given infrastructure.
   */
  public void configureKeys();

  /**
   * Configures the input devices.
   *
   * @throws IOException
   *           if an error occurs configuring the input devices
   */
  public void configureInputDevices() throws IOException;

  /**
   * Gets the {@link TextureLoader} implementation used by the infrastructure.
   *
   * @return the {@link TextureLoader} implementation used by the infrastructure.
   */
  public TextureLoader getTextureLoader();
}
