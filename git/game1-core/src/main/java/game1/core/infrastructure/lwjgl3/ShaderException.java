package game1.core.infrastructure.lwjgl3;

/**
 * Thrown if there is an error parsing the shaders used in the application.
 */
@SuppressWarnings("serial")
public class ShaderException extends Exception {

  public ShaderException(String message) {
    super(message);
  }
}
