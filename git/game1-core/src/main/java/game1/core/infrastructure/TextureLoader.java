package game1.core.infrastructure;

import java.awt.image.BufferedImage;
import java.io.IOException;

import game1.core.pipelines.ShadowMap;
import game1.core.resources.utils.BufferedImageUtils;

abstract public class TextureLoader {

  /**
   * Converts an image into a texture.
   *
   * @param image
   *          the image to convert
   * @return a texture object
   */
  public Texture loadTexture(BufferedImage image) {
    return loadTexture(image, false);
  }

  /**
   * Converts an image into a texture.
   *
   * @param image
   *          the image to convert
   * @param tiled
   *          whether the texture is tiled or not
   * @return a texture object
   */
  abstract public Texture loadTexture(BufferedImage image, boolean tiled);

  /**
   * Creates a blank texture of the given size. Used for the {@link ShadowMap}.
   */
  abstract public Texture createBlankTexture(int width, int height, int pixelFormat);

  /**
   * Loads a texture from a file.
   *
   * @param filename
   *          file to load
   * @return a texture created from the file
   * @throws IOException
   *           if an error occurs loading the texture
   */
  public Texture loadTexture(String filename) throws IOException {
    return loadTexture(BufferedImageUtils.loadBufferedImage(filename));
  }

  /**
   * Loads a texture from a file and scales it by <code>scale</code>.
   *
   * @param filename
   *          file to load
   * @param scale
   *          value to scale the image by
   * @return a texture created from the file, scaled by the given value
   * @throws IOException
   *           if an error occurs loading the texture
   */
  public Texture loadTexture(String filename, int scale) throws IOException {
    return loadTexture(BufferedImageUtils.loadBufferedImage(filename, scale));
  }
}
