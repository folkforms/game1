package game1.core.infrastructure.lwjgl3;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import org.lwjgl.glfw.GLFW;

import game1.core.input.ControllerInputDevice;

/**
 * OpenGL class for querying controller input.
 */
class LWJGL3ControllerInputDevice extends ControllerInputDevice {

  private int id;

  protected LWJGL3ControllerInputDevice(int id) {
    super(20);
    this.id = id;
  }

  @Override
  public void setState() {
    ByteBuffer buttons = GLFW.glfwGetJoystickButtons(id);
    for (int i = 0; i < buttons.limit(); i++) {
      states[i] = buttons.get(i) != 0;
    }
    FloatBuffer axes = GLFW.glfwGetJoystickAxes(id);
    states[16] = axes.get(1) == -1;
    states[17] = axes.get(1) == 1;
    states[18] = axes.get(0) == -1;
    states[19] = axes.get(0) == 1;
  }
}
