package game1.core.infrastructure.lwjgl3;

import static org.lwjgl.glfw.GLFW.GLFW_PRESS;

import java.nio.DoubleBuffer;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallback;
import org.lwjgl.system.MemoryUtil;

import game1.core.infrastructure.lwjgl3.internal.LWJGL3Window;
import game1.core.input.MouseInputDevice;
import game1.resolutions.layers.ResolutionLayers;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;

/**
 * OpenGL class for querying mouse input.
 */
class LWJGL3MouseInputDevice extends MouseInputDevice {

  // Keep references to the callbacks to avoid garbage collection
  @SuppressWarnings("unused")
  private GLFWMouseButtonCallback mouseButtonCallback;
  private DoubleBuffer xdb, ydb;
  @SuppressWarnings("unused")
  private GLFWScrollCallback scrollWheelCallback;

  LWJGL3MouseInputDevice() {
    super(3);
    // FIXME MOUSE: LWJGL3MouseInputDevice: Can we get number of mouse buttons? Might have to query
    // it outside this constructor, but that's fine.

    xdb = MemoryUtil.memAllocDouble(1);
    ydb = MemoryUtil.memAllocDouble(1);

    GLFW.glfwSetScrollCallback(LWJGL3Window.windowHandle,
        scrollWheelCallback = new GLFWScrollCallback() {
          @Override
          public void invoke(long window, double xoffset, double yoffset) {
            wheelDown = false;
            wheelUp = false;
            if (yoffset < 0) {
              wheelDown = true;
            } else if (yoffset > 0) {
              wheelUp = true;
            }
          }
        });

    GLFW.glfwSetInputMode(LWJGL3Window.windowHandle, GLFW.GLFW_CURSOR, GLFW.GLFW_CURSOR_DISABLED);
  }

  /**
   * Gets the cursor position and updates the camera.
   */
  @Override
  protected void setCameraPosition() {
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(ResolutionLayers.WINDOW_LAYER).getResolutionHints();
    int windowWidth = hints.getWindowWidth();
    int windowHeight = hints.getWindowHeight();

    GLFW.glfwGetCursorPos(LWJGL3Window.windowHandle, xdb, ydb);
    double doubleX = xdb.get(0);
    double doubleY = ydb.get(0);
    double deltaX = doubleX - windowWidth / 2;
    double deltaY = doubleY - windowHeight / 2;
    GLFW.glfwSetCursorPos(LWJGL3Window.windowHandle, windowWidth / 2, windowHeight / 2);
    raiseMouseMoveEvent(deltaX, deltaY);
  }

  /**
   * Gets the cursor position and updates the mouse cursor.
   */
  @Override
  protected void setCursorPosition() {
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(ResolutionLayers.WINDOW_LAYER).getResolutionHints();
    int windowWidth = hints.getWindowWidth();
    int windowHeight = hints.getWindowHeight();

    GLFW.glfwGetCursorPos(LWJGL3Window.windowHandle, xdb, ydb);
    double doubleX = xdb.get(0);
    double doubleY = ydb.get(0);
    x = (int) Math.floor(doubleX);
    y = windowHeight - (int) Math.floor(doubleY);
    if (x >= windowWidth) {
      x = windowWidth - 1;
    } else if (x < 0) {
      x = 0;
    }
    if (y >= windowHeight) {
      y = windowHeight - 1;
    } else if (y < 0) {
      y = 0;
    }
    windowPos.x = x;
    windowPos.y = y;
    GLFW.glfwSetCursorPos(LWJGL3Window.windowHandle, x, windowHeight - y);
  }

  @Override
  protected void setScrollWheelState() {
    // Handled by callback
  }

  @Override
  protected void setButtonStates() {
    for (int i = 0; i < states.length; i++) {
      states[i] = GLFW.glfwGetMouseButton(LWJGL3Window.windowHandle, i) == GLFW_PRESS;
    }
  }

  @Override
  public void centerCursor() {
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(ResolutionLayers.WINDOW_LAYER).getResolutionHints();
    int windowWidth = hints.getWindowWidth();
    int windowHeight = hints.getWindowHeight();

    GLFW.glfwSetCursorPos(LWJGL3Window.windowHandle, windowWidth / 2, windowHeight / 2);
  }
}
