package game1.core.infrastructure.lwjgl3;

import static org.lwjgl.opengl.GL11.GL_DEPTH_COMPONENT;
import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_NEAREST;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_S;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_T;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;
import static org.lwjgl.opengl.GL12.GL_CLAMP_TO_EDGE;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import folkforms.log.Log;
import game1.core.infrastructure.Texture;
import game1.core.infrastructure.TextureLoader;
import game1.debug.DebugScopes;

public class LWJGL3TextureLoader extends TextureLoader {

  /**
   * {@inheritDoc}
   *
   * <p>
   * For LWJGL3, this method will ensure that the texture is loaded on the main thread.
   * </p>
   */
  @Override
  public Texture loadTexture(BufferedImage image, boolean tiled) {
    String threadName = Thread.currentThread().getName();
    Log.scoped(DebugScopes.TEXTURE_LOADER,
        "LWJGL3TextureLoader.loadTexture: BufferedImage@%s (thread = %s)", image.hashCode(),
        threadName);
    Log.scoped(DebugScopes.MAIN_THREAD_TICKETS,
        "LWJGL3TextureLoader.loadTexture CommandWithResult.execute: Create texture from BufferedImage@%s (thread = %s)",
        image.hashCode(), threadName);
    return internal_loadTexture(image, tiled);
  }

  /**
   * Converts an image into a texture.
   *
   * @param image
   *          the image to convert
   * @param tiled
   *          whether the texture is tiled or not
   * @return a texture object
   */
  private Texture internal_loadTexture(BufferedImage image, boolean tiled) {
    int[] pixels = new int[image.getWidth() * image.getHeight()];
    image.getRGB(0, 0, image.getWidth(), image.getHeight(), pixels, 0, image.getWidth());

    int BYTES_PER_PIXEL = 4; // 4 for RGBA, 3 for RGB
    ByteBuffer buffer = BufferUtils
        .createByteBuffer(image.getWidth() * image.getHeight() * BYTES_PER_PIXEL);

    for (int y = 0; y < image.getHeight(); y++) {
      for (int x = 0; x < image.getWidth(); x++) {
        int pixel = pixels[y * image.getWidth() + x];
        buffer.put((byte) ((pixel >> 16) & 0xFF)); // Red component
        buffer.put((byte) ((pixel >> 8) & 0xFF)); // Green component
        buffer.put((byte) (pixel & 0xFF)); // Blue component
        buffer.put((byte) ((pixel >> 24) & 0xFF)); // Alpha component
      }
    }

    buffer.flip(); // FOR THE LOVE OF GOD DO NOT FORGET THIS

    // You now have a ByteBuffer filled with the colour data of each pixel.
    // Now just create a texture ID and bind it. Then you can load it using
    // whatever OpenGL method you want, for example:

    int textureID = GL11.glGenTextures();
    GL11.glBindTexture(GL11.GL_TEXTURE_2D, textureID);

    // Setup wrap mode
    if (!tiled) {
      GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
      GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
    } else {
      GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
      GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
    }

    // Setup texture scaling filtering
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);

    // Send texel data to OpenGL
    GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA8, image.getWidth(), image.getHeight(), 0,
        GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer);

    Texture texture = new Texture(textureID, image.getWidth(), image.getHeight());
    return texture;
  }

  /**
   * Creates a blank texture. Used for shadow mapping.
   *
   * @param width
   *          texture width
   * @param height
   *          texture height
   * @param pixelFormat
   *          texture pixel format FIXME What is this?
   * @return a blank texture
   */
  @Override
  public Texture createBlankTexture(int width, int height, int pixelFormat) {
    int id = glGenTextures();
    glBindTexture(GL_TEXTURE_2D, id);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, width, height, 0, pixelFormat, GL_FLOAT,
        (ByteBuffer) null);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    return new Texture(id, width, height);
  }
}
