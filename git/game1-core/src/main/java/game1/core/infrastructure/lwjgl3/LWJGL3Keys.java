package game1.core.infrastructure.lwjgl3;

import org.lwjgl.glfw.GLFW;

import game1.core.input.Keys;

public class LWJGL3Keys {
  public static void configureKeys() {
    Keys.KEY_0 = GLFW.GLFW_KEY_0;
    Keys.KEY_1 = GLFW.GLFW_KEY_1;
    Keys.KEY_2 = GLFW.GLFW_KEY_2;
    Keys.KEY_3 = GLFW.GLFW_KEY_3;
    Keys.KEY_4 = GLFW.GLFW_KEY_4;
    Keys.KEY_5 = GLFW.GLFW_KEY_5;
    Keys.KEY_6 = GLFW.GLFW_KEY_6;
    Keys.KEY_7 = GLFW.GLFW_KEY_7;
    Keys.KEY_8 = GLFW.GLFW_KEY_8;
    Keys.KEY_9 = GLFW.GLFW_KEY_9;
    Keys.KEY_A = GLFW.GLFW_KEY_A;
    Keys.KEY_ADD = GLFW.GLFW_KEY_KP_ADD;
    Keys.KEY_APOSTROPHE = GLFW.GLFW_KEY_APOSTROPHE;
    Keys.KEY_APPS = GLFW.GLFW_KEY_MENU;
    Keys.KEY_B = GLFW.GLFW_KEY_B;
    Keys.KEY_BACKSPACE = GLFW.GLFW_KEY_BACKSPACE;
    Keys.KEY_BACKSLASH = GLFW.GLFW_KEY_BACKSLASH;
    Keys.KEY_C = GLFW.GLFW_KEY_C;
    Keys.KEY_CAPSLOCK = GLFW.GLFW_KEY_CAPS_LOCK;
    Keys.KEY_COMMA = GLFW.GLFW_KEY_COMMA;
    Keys.KEY_D = GLFW.GLFW_KEY_D;
    Keys.KEY_DELETE = GLFW.GLFW_KEY_DELETE;
    Keys.KEY_DIVIDE = GLFW.GLFW_KEY_KP_DIVIDE;
    Keys.KEY_DOWN_ARROW = GLFW.GLFW_KEY_DOWN;
    Keys.KEY_E = GLFW.GLFW_KEY_E;
    Keys.KEY_END = GLFW.GLFW_KEY_END;
    Keys.KEY_ENTER = GLFW.GLFW_KEY_ENTER;
    Keys.KEY_EQUALS = GLFW.GLFW_KEY_EQUAL;
    Keys.KEY_ESCAPE = GLFW.GLFW_KEY_ESCAPE;
    Keys.KEY_F = GLFW.GLFW_KEY_F;
    Keys.KEY_F1 = GLFW.GLFW_KEY_F1;
    Keys.KEY_F2 = GLFW.GLFW_KEY_F2;
    Keys.KEY_F3 = GLFW.GLFW_KEY_F3;
    Keys.KEY_F4 = GLFW.GLFW_KEY_F4;
    Keys.KEY_F5 = GLFW.GLFW_KEY_F5;
    Keys.KEY_F6 = GLFW.GLFW_KEY_F6;
    Keys.KEY_F7 = GLFW.GLFW_KEY_F7;
    Keys.KEY_F8 = GLFW.GLFW_KEY_F8;
    Keys.KEY_F9 = GLFW.GLFW_KEY_F9;
    Keys.KEY_F10 = GLFW.GLFW_KEY_F10;
    Keys.KEY_F11 = GLFW.GLFW_KEY_F11;
    Keys.KEY_F12 = GLFW.GLFW_KEY_F12;
    Keys.KEY_F13 = GLFW.GLFW_KEY_F13;
    Keys.KEY_F14 = GLFW.GLFW_KEY_F14;
    Keys.KEY_F15 = GLFW.GLFW_KEY_F15;
    Keys.KEY_F16 = GLFW.GLFW_KEY_F16;
    Keys.KEY_F17 = GLFW.GLFW_KEY_F17;
    Keys.KEY_F18 = GLFW.GLFW_KEY_F18;
    Keys.KEY_F19 = GLFW.GLFW_KEY_F19;
    Keys.KEY_FORWARD_SLASH = GLFW.GLFW_KEY_SLASH;
    Keys.KEY_G = GLFW.GLFW_KEY_G;
    Keys.KEY_H = GLFW.GLFW_KEY_H;
    Keys.KEY_HOME = GLFW.GLFW_KEY_HOME;
    Keys.KEY_I = GLFW.GLFW_KEY_I;
    Keys.KEY_INSERT = GLFW.GLFW_KEY_INSERT;
    Keys.KEY_J = GLFW.GLFW_KEY_J;
    Keys.KEY_K = GLFW.GLFW_KEY_K;
    Keys.KEY_L = GLFW.GLFW_KEY_L;
    Keys.KEY_LEFT_BRACKET = GLFW.GLFW_KEY_LEFT_BRACKET;
    Keys.KEY_LEFT_CTRL = GLFW.GLFW_KEY_LEFT_CONTROL;
    Keys.KEY_LEFT_ARROW = GLFW.GLFW_KEY_LEFT;
    Keys.KEY_LEFT_ALT = GLFW.GLFW_KEY_LEFT_ALT;
    Keys.KEY_LEFT_SHIFT = GLFW.GLFW_KEY_LEFT_SHIFT;
    Keys.KEY_M = GLFW.GLFW_KEY_M;
    Keys.KEY_MINUS = GLFW.GLFW_KEY_MINUS;
    Keys.KEY_MULTIPLY = GLFW.GLFW_KEY_KP_MULTIPLY;
    Keys.KEY_N = GLFW.GLFW_KEY_N;
    Keys.KEY_NUMLOCK = GLFW.GLFW_KEY_NUM_LOCK;
    Keys.KEY_NUMPAD_0 = GLFW.GLFW_KEY_KP_0;
    Keys.KEY_NUMPAD_1 = GLFW.GLFW_KEY_KP_1;
    Keys.KEY_NUMPAD_2 = GLFW.GLFW_KEY_KP_2;
    Keys.KEY_NUMPAD_3 = GLFW.GLFW_KEY_KP_3;
    Keys.KEY_NUMPAD_4 = GLFW.GLFW_KEY_KP_4;
    Keys.KEY_NUMPAD_5 = GLFW.GLFW_KEY_KP_5;
    Keys.KEY_NUMPAD_6 = GLFW.GLFW_KEY_KP_6;
    Keys.KEY_NUMPAD_7 = GLFW.GLFW_KEY_KP_7;
    Keys.KEY_NUMPAD_8 = GLFW.GLFW_KEY_KP_8;
    Keys.KEY_NUMPAD_9 = GLFW.GLFW_KEY_KP_9;
    Keys.KEY_NUMPAD_DECIMAL = GLFW.GLFW_KEY_KP_DECIMAL;
    Keys.KEY_NUMPAD_ENTER = GLFW.GLFW_KEY_KP_ENTER;
    Keys.KEY_NUMPAD_EQUALS = GLFW.GLFW_KEY_KP_EQUAL;
    Keys.KEY_O = GLFW.GLFW_KEY_O;
    Keys.KEY_P = GLFW.GLFW_KEY_P;
    Keys.KEY_PAUSE = GLFW.GLFW_KEY_PAUSE;
    Keys.KEY_PERIOD = GLFW.GLFW_KEY_PERIOD;
    Keys.KEY_PAGE_DOWN = GLFW.GLFW_KEY_PAGE_DOWN;
    Keys.KEY_PAGE_UP = GLFW.GLFW_KEY_PAGE_UP;
    Keys.KEY_PRINT_SCREEN = GLFW.GLFW_KEY_PRINT_SCREEN;
    Keys.KEY_Q = GLFW.GLFW_KEY_Q;
    Keys.KEY_R = GLFW.GLFW_KEY_R;
    Keys.KEY_RIGHT_BRACKET = GLFW.GLFW_KEY_RIGHT_BRACKET;
    Keys.KEY_RIGHT_CTRL = GLFW.GLFW_KEY_RIGHT_CONTROL;
    Keys.KEY_RIGHT_ARROW = GLFW.GLFW_KEY_RIGHT;
    Keys.KEY_RIGHT_ALT = GLFW.GLFW_KEY_RIGHT_ALT;
    Keys.KEY_RIGHT_SHIFT = GLFW.GLFW_KEY_RIGHT_SHIFT;
    Keys.KEY_S = GLFW.GLFW_KEY_S;
    Keys.KEY_SCROLL_LOCK = GLFW.GLFW_KEY_SCROLL_LOCK;
    Keys.KEY_SEMICOLON = GLFW.GLFW_KEY_SEMICOLON;
    Keys.KEY_SPACE = GLFW.GLFW_KEY_SPACE;
    Keys.KEY_SUBTRACT = GLFW.GLFW_KEY_MINUS;
    Keys.KEY_T = GLFW.GLFW_KEY_T;
    Keys.KEY_TAB = GLFW.GLFW_KEY_TAB;
    Keys.KEY_U = GLFW.GLFW_KEY_U;
    Keys.KEY_UP_ARROW = GLFW.GLFW_KEY_UP;
    Keys.KEY_V = GLFW.GLFW_KEY_V;
    Keys.KEY_W = GLFW.GLFW_KEY_W;
    Keys.KEY_X = GLFW.GLFW_KEY_X;
    Keys.KEY_Y = GLFW.GLFW_KEY_Y;
    Keys.KEY_Z = GLFW.GLFW_KEY_Z;
  }
}
