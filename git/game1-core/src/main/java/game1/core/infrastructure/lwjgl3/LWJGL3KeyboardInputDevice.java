package game1.core.infrastructure.lwjgl3;

import static org.lwjgl.glfw.GLFW.glfwGetKey;

import game1.core.infrastructure.lwjgl3.internal.LWJGL3Window;
import game1.core.input.KeyboardInputDevice;

/**
 * OpenGL class for querying keyboard input.
 */
class LWJGL3KeyboardInputDevice extends KeyboardInputDevice {

  @Override
  public int getState(int key) {
    return glfwGetKey(LWJGL3Window.windowHandle, key);
  }
}
