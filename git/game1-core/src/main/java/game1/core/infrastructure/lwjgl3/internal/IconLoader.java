package game1.core.infrastructure.lwjgl3.internal;

import static org.lwjgl.BufferUtils.createByteBuffer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWImage;
import org.lwjgl.stb.STBImage;
import org.lwjgl.system.MemoryStack;

import game1.core.engine.Engine;
import game1.core.resources.utils.ByteBufferUtils;
import game1.filesystem.FileSystem;
import game1.filesystem.FileSystemFactory;

public class IconLoader {

  public static void load() throws IOException {

    GLFWImage[] glfwImages = null;
    String operatingSystemName = System.getProperty("os.name").toLowerCase();
    if (operatingSystemName.contains("win")) {
      glfwImages = new GLFWImage[2];
      glfwImages[0] = load("icon.16x16.path");
      glfwImages[1] = load("icon.32x32.path");
    } else if (operatingSystemName.contains("mac")) {
      glfwImages = new GLFWImage[1];
      glfwImages[0] = load("icon.128x128.path");
    } else { // Linux
      glfwImages = new GLFWImage[1];
      glfwImages[0] = load("icon.32x32.path");
    }

    GLFWImage.Buffer imagebf = GLFWImage.malloc(glfwImages.length);
    for (int i = 0; i < glfwImages.length; i++) {
      imagebf.put(i, glfwImages[i]);
    }
    GLFW.glfwSetWindowIcon(LWJGL3Window.windowHandle, imagebf);
  }

  private static GLFWImage load(String property) throws IOException {
    String path = Engine.PROPERTIES.getProperty(property);
    FileSystem fileSystem = FileSystemFactory.getInstance();
    byte[] bytes = fileSystem.load(path);

    ByteBuffer image;
    int width, height;
    try (MemoryStack stack = MemoryStack.stackPush()) {
      IntBuffer comp = stack.mallocInt(1);
      IntBuffer w = stack.mallocInt(1);
      IntBuffer h = stack.mallocInt(1);

      ByteBuffer byteBuffer = createByteBuffer(bytes.length);
      byteBuffer.put(bytes);
      byteBuffer = ByteBufferUtils.resizeBuffer(byteBuffer, byteBuffer.capacity() * 2);
      byteBuffer.flip();

      image = STBImage.stbi_load_from_memory(byteBuffer, w, h, comp, 4);
      if (image == null) {
        String message = String.format("Could not load window icon '%s' => '%s' (%s)", property,
            path, STBImage.stbi_failure_reason());
        throw new IOException(message);
      }
      width = w.get();
      height = h.get();
    }

    GLFWImage glfwImage = GLFWImage.malloc();
    glfwImage.set(width, height, image);

    return glfwImage;
  }
}
