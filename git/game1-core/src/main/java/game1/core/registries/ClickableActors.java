package game1.core.registries;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.joml.Vector2f;

import folkforms.log.Log;
import game1.actors.Clickable;
import game1.core.input.MouseFactory;
import game1.core.input.MouseUtils;
import game1.debug.DebugScopes;

/**
 * Registry of Actors that implement the {@link Clickable} interface, as well as methods to find the
 * item that was clicked on.
 */
public class ClickableActors {

  private static List<Clickable> items = new CopyOnWriteArrayList<>();
  private static Clickable mouseDownItem = null;
  private static int mouseDownItemButton = -1;

  private ClickableActors() {
  }

  public static void register(Clickable item) {
    items.add(item);
  }

  public static void unregister(Clickable item) {
    if (mouseDownItem == item) {
      mouseDownItem = null;
    }
    items.remove(item);
  }

  public static void setMouseDownItem(int button) {
    mouseDownItemButton = button;
    mouseDownItem = getItemUnderMouse();
  }

  public static void clearMouseDownItem() {
    mouseDownItem = null;
  }

  public static Clickable getMouseDownItem() {
    return mouseDownItem;
  }

  public static int getMouseDownItemButton() {
    return mouseDownItemButton;
  }

  public static Clickable getItemUnderMouse() {
    List<Clickable> clickablesUnderMouse = new ArrayList<>();
    for (Clickable c : items) {
      Vector2f xy = MouseUtils
          .translateToResolutionLayerPosition(MouseFactory.getInstance().getWindowPos(), c);
      if (c.getShape() != null && c.getShape().contains(xy) && c.isActive()) {
        clickablesUnderMouse.add(c);
      }
    }

    if (clickablesUnderMouse.isEmpty()) {
      Log.scoped(DebugScopes.MOUSE, "No clickables under mouse, returning null");
      return null;
    }

    Log.scoped(DebugScopes.MOUSE, "Found %s clickables under mouse. Finding top item...",
        clickablesUnderMouse.size());
    int highestZIndex = clickablesUnderMouse.get(0).getZ();
    int highestZIndexItemIndex = 0;
    for (int i = 1; i < clickablesUnderMouse.size(); i++) {
      int zIndex = clickablesUnderMouse.get(i).getZ();
      if (zIndex > highestZIndex) {
        highestZIndex = zIndex;
        highestZIndexItemIndex = i;
      }
    }
    Clickable topItem = clickablesUnderMouse.get(highestZIndexItemIndex);
    Log.scoped(DebugScopes.MOUSE, "Returning %s", topItem);
    return topItem;
  }

  public static void clear() {
    items.clear();
    mouseDownItem = null;
    mouseDownItemButton = -1;
  }
}
