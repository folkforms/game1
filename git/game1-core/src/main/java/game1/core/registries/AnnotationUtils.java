package game1.core.registries;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import folkforms.log.Log;

/**
 * Utility methods for processing and checking annotations.
 */
public class AnnotationUtils {

  public static boolean methodHasAnnotation(Object obj, String methodSignature,
      Class<? extends Annotation> annotationClass) {
    try {
      String methodName;
      int parameterCount = 0;
      if (methodSignature.contains("(")) {
        methodName = methodSignature.substring(0, methodSignature.indexOf('('));
        String methodArgs = methodSignature.substring(methodSignature.indexOf('(') + 1,
            methodSignature.indexOf(')'));
        if (methodArgs.length() > 0) {
          parameterCount++;
        }
        for (int i = 0; i < methodArgs.length(); i++) {
          if (methodArgs.charAt(i) == ',') {
            parameterCount++;
          }
        }
      } else {
        methodName = methodSignature;
      }

      Class<?> forName = Class.forName(obj.getClass().getName());
      Method[] methods = forName.getMethods();
      for (Method m : methods) {
        if (m.getName().equals(methodName) && m.getParameterCount() == parameterCount
            && m.isAnnotationPresent(annotationClass)) {
          return true;
        }
      }
      return false;

    } catch (ClassNotFoundException ex) {
      Log.error(ex);
      return false;
    }
  }

  public static boolean classHasAnnotation(Object obj,
      Class<? extends Annotation> annotationClass) {
    try {
      Class<?> forName = Class.forName(obj.getClass().getName());
      return forName.isAnnotationPresent(annotationClass);
    } catch (ClassNotFoundException ex) {
      Log.error(ex);
      return false;
    }
  }
}
