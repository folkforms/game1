package game1.core.registries;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import game1.actors.Draggable;

/**
 * Registry of actors that implement the {@link Draggable} interface, whose
 * {@link Draggable#onDrag(float, float)} methods will be called when they are dragged, and whose
 * {@link Draggable#dragFinished(float, float)} methods will be called when they are no longer being
 * dragged.
 */
public class DraggableActors {

  private static List<Draggable> items = new CopyOnWriteArrayList<>();
  private static Draggable mouseDownItem = null;

  private DraggableActors() {
  }

  public static void register(Draggable item) {
    items.add(item);
  }

  public static void unregister(Draggable item) {
    if (mouseDownItem == item) {
      mouseDownItem = null;
    }
    items.remove(item);
  }

  public static void setMouseDownItem(float windowX, float windowY) {
    mouseDownItem = getDraggableAt(windowX, windowY);
  }

  public static void clearMouseDownItem() {
    mouseDownItem = null;
  }

  public static Draggable getMouseDownItem() {
    return mouseDownItem;
  }

  public static Draggable getDraggableAt(float x, float y) {
    Draggable selected = null;
    for (Draggable d : items) {
      if (d.getShape().contains(x, y)) {
        if (selected == null || d.getZ() > selected.getZ()) {
          selected = d;
        }
      }
    }
    return selected;
  }

  public static void clear() {
    items.clear();
    mouseDownItem = null;
  }
}
