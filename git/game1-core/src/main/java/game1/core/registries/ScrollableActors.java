package game1.core.registries;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.joml.Vector2f;

import game1.actors.Scrollable;
import game1.core.input.MouseFactory;
import game1.core.input.MouseUtils;

/**
 * Registry of actors that implement the {@link Scrollable} interface, whose
 * {@link Scrollable#onMouseWheelUp() onMouseWheelUp()} or {@link Scrollable#onMouseWheelDown()
 * onMouseWheelDown()} methods will be called when the mouse cursor is over the actor and the mouse
 * wheel is used.
 */
public class ScrollableActors {

  private static List<Scrollable> items = new CopyOnWriteArrayList<>();

  private ScrollableActors() {
  }

  public static void register(Scrollable item) {
    items.add(item);
  }

  public static void unregister(Scrollable item) {
    items.remove(item);
  }

  public static Scrollable getItemAt(float x, float y) {
    Scrollable possible = null;
    for (Scrollable s : items) {
      Vector2f xy = MouseUtils
          .translateToResolutionLayerPosition(MouseFactory.getInstance().getWindowPos(), s);
      if (s.getShape() != null && s.getShape().contains(xy)) {
        if (possible == null || possible.getZ() < s.getZ()) {
          possible = s;
        }
      }
    }
    return possible;
  }

  public static void clear() {
    items.clear();
  }
}
