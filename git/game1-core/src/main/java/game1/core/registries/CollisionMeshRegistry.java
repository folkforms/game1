package game1.core.registries;

import java.util.ArrayList;
import java.util.List;

import org.joml.Intersectionf;
import org.joml.Matrix3f;
import org.joml.Vector3f;

import game1.core.graphics.CollisionMeshUtils;
import game1.core.graphics.Drawable;

/**
 * Registry of Drawables that make up a collision mesh. Projected player movement can be tested
 * again these items.
 */
public class CollisionMeshRegistry {

  private static List<Drawable> drawables = new ArrayList<>();

  public static void add(Drawable drawable) {
    drawables.add(drawable);
  }

  public static void remove(Drawable drawable) {
    drawables.remove(drawable);
  }

  /**
   * Checks if the projected movement will cross any boundary lines of any registered collision
   * meshes. Returns the intersection point if there will be a collision, otherwise returns null.
   *
   * @param currentPosition
   *          current position
   * @param projectedPosition
   *          projected position
   *
   * @return the intersection point if there will be a collision, false otherwise
   */
  public static Vector3f checkForCollision(Vector3f currentPosition, Vector3f projectedPosition) {
    int size = drawables.size();
    for (int i = 0; i < size; i++) {
      Matrix3f[] triangles = CollisionMeshUtils.getCollisionMesh(drawables.get(i));
      Vector3f check = checkForCollision(currentPosition, projectedPosition, triangles);
      if (check != null) {
        return check;
      }
    }
    return null;
  }

  /**
   * Checks if there is a collision when moving from <code>p0</code> to <code>p1</code>, given a
   * collision mesh of <code>triangles</code>.
   *
   * @param p0
   *          start position
   * @param p1
   *          end position
   * @param triangles
   *          collision mesh
   * @return the intersection point if there is a collision, <code>null</code> otherwise
   */
  private static Vector3f checkForCollision(Vector3f p0, Vector3f p1, Matrix3f[] triangles) {
    Vector3f v0 = new Vector3f();
    Vector3f v1 = new Vector3f();
    Vector3f v2 = new Vector3f();
    int size = triangles.length;
    for (int i = 0; i < size; i++) {
      Matrix3f t = triangles[i];
      v0 = t.getColumn(0, v0);
      v1 = t.getColumn(1, v1);
      v2 = t.getColumn(2, v2);
      Vector3f intersectionPoint = new Vector3f();
      boolean intersection = Intersectionf.intersectLineSegmentTriangle(p0, p1, v0, v1, v2, 0,
          intersectionPoint);
      if (intersection) {
        return intersectionPoint;
      }
    }
    return null;
  }
}
