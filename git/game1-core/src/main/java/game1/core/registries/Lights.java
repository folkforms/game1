package game1.core.registries;

import java.util.ArrayList;
import java.util.List;

import org.joml.Vector3f;

import folkforms.log.Log;
import game1.core.engine.Engine;
import game1.core.graphics.lights.AmbientLight;
import game1.core.graphics.lights.DirectionalLight;
import game1.core.graphics.lights.PointLight;
import game1.core.graphics.lights.Spotlight;

/**
 * Represents the lights added to a scene.
 */
public class Lights {

  public static final int MAX_POINT_LIGHTS = 5;
  public static final int MAX_SPOTLIGHTS = 5;

  // We need to set a default ambient light otherwise the game is black
  private static final AmbientLight defaultAmbientLight = new AmbientLight(
      new Vector3f(0.3f, 0.3f, 0.3f));
  private static AmbientLight ambientLight = defaultAmbientLight;
  private static DirectionalLight directionalLight = null;
  private static PointLight[] pointLightArray = new PointLight[0];
  private static List<PointLight> pointLightList = new ArrayList<>();
  private static Spotlight[] spotlightArray = new Spotlight[0];
  private static List<Spotlight> spotlightList = new ArrayList<>();

  public static Vector3f getAmbientLightColor() {
    return ambientLight.getColour();
  }

  public static void setAmbientLight(AmbientLight newAmbientLight) {
    if (newAmbientLight != null) {
      ambientLight = newAmbientLight;
    } else {
      // When the lights are cleared from the game state reset this back to the default so we always
      // have some ambient light, just in case the user forgets to set it.
      ambientLight = defaultAmbientLight;
    }
  }

  public static PointLight[] getPointLights() {
    return pointLightArray;
  }

  public static void addPointLight(PointLight pointLight) {
    if (pointLightList.size() < MAX_POINT_LIGHTS) {
      pointLightList.add(pointLight);
      createPointLightArray();
    } else {
      Log.warn("Tried to add Point Light '%s' but we have already reached max point lights.");
    }
  }

  private static void createPointLightArray() {
    PointLight[] temp = new PointLight[pointLightList.size()];
    for (int i = 0; i < pointLightList.size(); i++) {
      temp[i] = pointLightList.get(i);
    }
    pointLightArray = temp;
  }

  public static void removePointLight(PointLight pointLight) {
    pointLightList.remove(pointLight);
    createPointLightArray();
  }

  public static Spotlight[] getSpotlights() {
    return spotlightArray;
  }

  public static void addSpotlight(Spotlight newSpotlight) {
    if (spotlightList.size() < MAX_SPOTLIGHTS) {
      spotlightList.add(newSpotlight);
      createSpotlightArray();
    } else {
      Log.warn("Tried to add Spotlight '%s' but we have already reached max spotlights.");
    }
  }

  private static void createSpotlightArray() {
    Spotlight[] temp = new Spotlight[spotlightList.size()];
    for (int i = 0; i < spotlightList.size(); i++) {
      temp[i] = spotlightList.get(i);
    }
    spotlightArray = temp;
  }

  public static void removeSpotlight(Spotlight spotlight) {
    spotlightList.remove(spotlight);
    createSpotlightArray();
  }

  public static DirectionalLight getDirectionalLight() {
    return directionalLight;
  }

  public static void setDirectionalLight(DirectionalLight newDirectionalLight) {
    directionalLight = newDirectionalLight;
  }

  public static void clear() {
    ambientLight = defaultAmbientLight;
    directionalLight = null;
    pointLightArray = new PointLight[0];
    pointLightList = new ArrayList<>();
    spotlightArray = new Spotlight[0];
    spotlightList = new ArrayList<>();
    Engine.PIPELINES.clearLightUniforms();
  }
}
