package game1.core.registries;

import java.util.HashMap;
import java.util.Map;

import game1.actors.Actor;

/**
 * Links an actor to a debug string. This way we can say
 * <code>if(DebugActorRegistry.contains(actor, targetId) { ... }</code>. Useful in situations where
 * a method is called extremely frequently and/or called against many actors but we only want to
 * print debug data for a single actor.
 */
public class DebugActorRegistry {

  private static Map<Actor, String> actorToDebugIdMap = new HashMap<>();

  private DebugActorRegistry() {
  }

  public static boolean contains(Actor actor, String targetId) {
    // Log.temp("#### DebugActorRegistry: actor = %s, targetId = %s, actorToDebugIdMap = %s", actor,
    // targetId, actorToDebugIdMap);
    boolean found = actorToDebugIdMap.getOrDefault(actor, "").equals(targetId);
    // Log.temp("# returning %s", found);
    return found;
  }

  public static void put(Actor actor, String debugId) {
    actorToDebugIdMap.put(actor, debugId);
  }

  public static void clear() {
    actorToDebugIdMap.clear();
  }
}
