package game1.core.registries;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joml.Matrix4f;

import game1.core.engine.internal.GarbageCollector;
import game1.core.engine.internal.GraphicsPipelines;
import game1.core.graphics.Drawable;
import game1.core.pipelines.GraphicsPipeline;
import game1.core.pipelines.GraphicsPipelineName;

public class GraphicsPipelineRegistry implements GraphicsPipelines, GarbageCollector {

  private Map<GraphicsPipelineName, GraphicsPipeline> graphicsPipelineNames = new HashMap<>();
  private List<GraphicsPipeline> graphicsPipelineList = new ArrayList<>();
  private GraphicsPipeline[] graphicsPipelines;

  public void register(GraphicsPipeline p) {
    graphicsPipelineNames.put(p.getName(), p);
    graphicsPipelineList.add(p);
  }

  public void init() {
    graphicsPipelines = new GraphicsPipeline[graphicsPipelineNames.size()];
    for (int i = 0; i < graphicsPipelines.length; i++) {
      graphicsPipelines[i] = graphicsPipelineList.get(i);
    }
  }

  @Override
  public void renderAll(Matrix4f viewMatrix) {
    for (int i = 0; i < graphicsPipelines.length; i++) {
      graphicsPipelines[i].render(viewMatrix);
    }
  }

  public void addDrawable(Drawable drawable) {
    GraphicsPipeline p = graphicsPipelineNames.get(drawable.getPipeline());
    p.add(drawable);
  }

  public void removeDrawable(Drawable drawable) {
    GraphicsPipeline p = graphicsPipelineNames.get(drawable.getPipeline());
    p.remove(drawable);
  }

  public void clearLightUniforms() {
    if (graphicsPipelines != null) {
      for (int i = 0; i < graphicsPipelines.length; i++) {
        graphicsPipelines[i].clearLightUniforms();
      }
    }
  }

  @Override
  public void garbageCollect() {
    for (int i = 0; i < graphicsPipelines.length; i++) {
      graphicsPipelines[i].garbageCollect();
    }
  }

  // FIXME CLEANUP: Need to call this when exiting...
  public void cleanup() {
    for (int i = 0; i < graphicsPipelines.length; i++) {
      graphicsPipelines[i].cleanup();
    }
  }

  public GraphicsPipeline[] debug_listPipelines() {
    return graphicsPipelines;
  }

  public void debug_reloadShaders() {
    for (int i = 0; i < graphicsPipelines.length; i++) {
      graphicsPipelines[i].debug_reloadShaders();
    }
  }
}
