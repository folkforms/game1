package game1.core.registries;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A set of annotations that can be applied to {@link Tickable}s.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface TickableAnnotations {

  /**
   * If a {@link Tickable#onTick()} method has this annotation it will tick even when
   * <code>Engine.GAME_PAUSED</code> is true.
   */
  @Retention(RetentionPolicy.RUNTIME)
  @Target({ ElementType.METHOD })
  public @interface TickEvenIfGamePaused {
  }

  /**
   * If a {@link Tickable#onTick()} method has this annotation it will tick at 1/10th the normal
   * tick rate.
   */
  @Retention(RetentionPolicy.RUNTIME)
  @Target({ ElementType.METHOD })
  public @interface TickSlowly {
  }
}
