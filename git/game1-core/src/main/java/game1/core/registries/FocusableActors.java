package game1.core.registries;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import game1.actors.Focusable;

/**
 * Registry of actors that implement the {@link Focusable} interface. Keeps track of the currently
 * selected item so that only 0-1 items can be selected at a time.
 */
public class FocusableActors {

  private static List<Focusable> items = new CopyOnWriteArrayList<>();
  private static Focusable selectedItem = null;
  private static Focusable stashedItem = null;

  private FocusableActors() {
  }

  public static void register(Focusable item) {
    items.add(item);
  }

  public static void unregister(Focusable item) {
    if (selectedItem == item) {
      selectedItem.onBlur();
      selectedItem = null;
    }
    items.remove(item);
  }

  public static void setSelected(Focusable item) {
    if (item != selectedItem) {
      if (selectedItem != null) {
        selectedItem.onBlur();
      }
      selectedItem = item;
      if (item != null) {
        item.onFocus();
      }
    }
  }

  /**
   * Stashes the currently-focused item for later retrieval.
   */
  public static void stashSave() {
    if (selectedItem != null) {
      stashedItem = selectedItem;
    }
  }

  /**
   * Restores the previously stashed item. Used for e.g. modal dialogs that are disappearing and
   * want to transfer focus back to whatever was focused before they appeared.
   */
  public static void stashPop() {
    if (stashedItem != null) {
      setSelected(stashedItem);
      stashedItem = null;
    }
  }

  /**
   * Gets the currently focused item, or <code>null</code> if nothing has focus.
   *
   * @return the currently focused item, or <code>null</code> if nothing has focus
   */
  public static Focusable getSelected() {
    return selectedItem;
  }

  /**
   * Moves to the next focusable item. Typically called by Commands assigned using
   * TextField.addTabKeyCommand.
   */
  public static void moveToNextItem() {
    if (items.size() < 2) {
      return;
    }

    int selectedIndex = -1;
    for (int i = 0; i < items.size(); i++) {
      if (items.get(i) == selectedItem) {
        selectedIndex = i;
        break;
      }
    }

    int nextIndex = selectedIndex + 1;
    if (nextIndex >= items.size()) {
      nextIndex = 0;
    }

    setSelected(items.get(nextIndex));
  }

  public static void clear() {
    items.clear();
    selectedItem = null;
    stashedItem = null;
  }
}
