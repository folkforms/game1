package game1.core.registries;

import java.util.Comparator;

import game1.core.graphics.Drawable;

/**
 * Compares two Drawables based on their Z values. If their Z values are the same it compares their
 * Y values.
 */
public class DrawableComparator implements Comparator<Drawable> {

  @Override
  public int compare(Drawable d1, Drawable d2) {
    float zValue = d1.getPosition().z - d2.getPosition().z;
    if (zValue < 0) {
      return -1;
    } else if (zValue > 0) {
      return 1;
    } else {
      float yValue = d1.getPosition().y - d2.getPosition().y;
      if (yValue < 0) {
        return -1;
      } else if (yValue > 0) {
        return 1;
      } else {
        return 0;
      }
    }
  }
}
