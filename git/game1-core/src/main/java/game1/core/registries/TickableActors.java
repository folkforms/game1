package game1.core.registries;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import game1.actors.Tickable;

/**
 * Registry of actors that implement the {@link Tickable} interface, whose
 * {@link Tickable#onTick(long)} methods will be called every tick.
 */
public class TickableActors {

  private static List<Tickable> items = new CopyOnWriteArrayList<>();

  /**
   * These are the items annotated with {@link TickableAnnotations.TickEvenIfGamePaused}.
   */
  private static List<Tickable> tickIfGamePaused = new CopyOnWriteArrayList<>();

  /**
   * These are the items annotated with {@link TickableAnnotations.TickSlowly}.
   */
  private static List<Tickable> tickSlowly = new CopyOnWriteArrayList<>();

  private TickableActors() {
  }

  public static List<Tickable> listTickables() {
    return List.copyOf(items);
  }

  public static void register(Tickable item) {
    if (AnnotationUtils.methodHasAnnotation(item, "onTick()",
        TickableAnnotations.TickSlowly.class)) {
      tickSlowly.add(item);
    } else {
      items.add(item);
      if (AnnotationUtils.methodHasAnnotation(item, "onTick()",
          TickableAnnotations.TickEvenIfGamePaused.class)) {
        tickIfGamePaused.add(item);
      }
    }
  }

  public static void unregister(Tickable item) {
    items.remove(item);
    tickIfGamePaused.remove(item);
    tickSlowly.remove(item);
  }

  public static List<Tickable> listTickEvenIfGamePaused() {
    return tickIfGamePaused;
  }

  public static List<Tickable> listLowPriority() {
    return tickSlowly;
  }

  public static void clear() {
    items.clear();
    tickIfGamePaused.clear();
    tickSlowly.clear();
  }
}
