package game1.core.resources.loaders.assimp;

import java.util.List;

class ListUtils {

  static float[] floatListToArray(List<Float> list) {
    int size = list != null ? list.size() : 0;
    float[] floatArr = new float[size];
    for (int i = 0; i < size; i++) {
      floatArr[i] = list.get(i);
    }
    return floatArr;
  }

  static int[] integerListToArray(List<Integer> input) {
    int[] output = new int[input.size()];
    for (int i = 0; i < input.size(); i++) {
      output[i] = input.get(i);
    }
    return output;
  }
}
