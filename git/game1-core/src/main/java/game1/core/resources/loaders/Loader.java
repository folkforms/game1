package game1.core.resources.loaders;

import java.io.IOException;

import game1.actors.Actor;
import game1.core.engine.Game1RuntimeException;
import game1.filesystem.FileSystem;
import game1.filesystem.FileSystemFactory;

public interface Loader {

  Actor load(FileSystem fileSystem) throws IOException;

  default void unload(FileSystem fileSystem) {
  };

  /**
   * Validates that the given file exists.
   *
   * @param path
   *          file path to validate
   */
  default void validateFileExists(String path) {
    FileSystem fileSystem = FileSystemFactory.getInstance();
    try {
      if (!fileSystem.exists(path)) {
        throw new Game1RuntimeException("File %s does not exist", path);
      }
    } catch (IOException e) {
      throw new Game1RuntimeException(e, "Error trying to validate if file %s exists", path);
    }
  }
}
