package game1.core.resources.loaders.assimp;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import game1.core.engine.EngineInternal;
import game1.core.infrastructure.Texture;

class TextureCache {

  private static TextureCache INSTANCE;
  private Map<String, Texture> texturesMap;

  private TextureCache() {
    texturesMap = new HashMap<>();
  }

  public static synchronized TextureCache getInstance() {
    if (INSTANCE == null) {
      INSTANCE = new TextureCache();
    }
    return INSTANCE;
  }

  /**
   * Gets the given texture from the texture cache. If it does not exist it will be loaded.
   *
   * @param path
   * @return
   * @throws IOException
   */
  public Texture getTexture(String path) throws IOException {
    Texture texture = texturesMap.get(path);
    if (texture == null) {
      texture = EngineInternal.TEXTURE_LOADER.loadTexture(path);
      if (texture == null) {
        throw new IOException(String.format("Error loading %s", path));
      }
      texturesMap.put(path, texture);
    }
    return texture;
  }
}
