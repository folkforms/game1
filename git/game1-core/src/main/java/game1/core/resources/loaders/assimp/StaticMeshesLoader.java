package game1.core.resources.loaders.assimp;

import static org.lwjgl.assimp.Assimp.AI_MATKEY_COLOR_AMBIENT;
import static org.lwjgl.assimp.Assimp.AI_MATKEY_COLOR_DIFFUSE;
import static org.lwjgl.assimp.Assimp.AI_MATKEY_COLOR_SPECULAR;
import static org.lwjgl.assimp.Assimp.aiGetErrorString;
import static org.lwjgl.assimp.Assimp.aiGetMaterialColor;
import static org.lwjgl.assimp.Assimp.aiImportFileEx;
import static org.lwjgl.assimp.Assimp.aiTextureType_DIFFUSE;
import static org.lwjgl.assimp.Assimp.aiTextureType_NONE;
import static org.lwjgl.system.MemoryUtil.memAddress;
import static org.lwjgl.system.MemoryUtil.memCopy;
import static org.lwjgl.system.MemoryUtil.memUTF8;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.joml.Vector4f;
import org.lwjgl.PointerBuffer;
import org.lwjgl.assimp.AIColor4D;
import org.lwjgl.assimp.AIFace;
import org.lwjgl.assimp.AIFile;
import org.lwjgl.assimp.AIFileIO;
import org.lwjgl.assimp.AIMaterial;
import org.lwjgl.assimp.AIMesh;
import org.lwjgl.assimp.AIScene;
import org.lwjgl.assimp.AIString;
import org.lwjgl.assimp.AIVector3D;
import org.lwjgl.assimp.Assimp;

import game1.core.graphics.Material;
import game1.core.graphics.meshes.Mesh;
import game1.core.graphics.meshes.MeshUtils;
import game1.core.infrastructure.Texture;
import game1.core.resources.utils.ByteBufferUtils;
import game1.filesystem.FileSystem;
import game1.filesystem.FileSystemFactory;

public class StaticMeshesLoader {

  private static AIFileIO AI_FILE_IO = getAIFileIO();

  private static AIFileIO getAIFileIO() {
    AIFileIO fileIo = AIFileIO.create().OpenProc((pFileIO, fileName, openMode) -> {
      ByteBuffer data;
      String fileNameUtf8 = memUTF8(fileName);
      try {
        // data = ioResourceToByteBuffer(fileNameUtf8, 8192);
        FileSystem fileSystem = FileSystemFactory.getInstance();
        byte[] bytes = fileSystem.load(fileNameUtf8);
        data = ByteBufferUtils.convertBytesToByteBuffer(bytes);
      } catch (IOException ex) {
        throw new RuntimeException(String.format("Error loading resource: %s", fileNameUtf8), ex);
      }

      return AIFile.create().ReadProc((pFile, pBuffer, size, count) -> {
        long max = Math.min(data.remaining(), size * count);
        memCopy(memAddress(data) + data.position(), pBuffer, max);
        return max;
      }).SeekProc((pFile, offset, origin) -> {
        if (origin == Assimp.aiOrigin_CUR) {
          data.position(data.position() + (int) offset);
        } else if (origin == Assimp.aiOrigin_SET) {
          data.position((int) offset);
        } else if (origin == Assimp.aiOrigin_END) {
          data.position(data.limit() + (int) offset);
        }
        return 0;
      }).FileSizeProc(pFile -> data.limit()).address();
    }).CloseProc((pFileIO, pFile) -> {
      AIFile aiFile = AIFile.create(pFile);
      aiFile.ReadProc().free();
      aiFile.SeekProc().free();
      aiFile.FileSizeProc().free();
    });
    return fileIo;
  }

  public static Mesh[] load(String resourcePath, String texturesDir, int instances, int flags)
      throws IOException {
    AIScene aiScene = aiImportFileEx(resourcePath, flags, AI_FILE_IO);

    // FIXME FILE_SYSTEM FIXME CLEANUP Should probably add these two calls to a cleanup method?
    // AI_FILE_IO.OpenProc().free();
    // AI_FILE_IO.CloseProc().free();

    if (aiScene == null) {
      throw new IOException(String.format(
          "Error loading mesh: %s (resourcePath = '%s', texturesDir = '%s', flags = %s)",
          aiGetErrorString(), resourcePath, texturesDir, flags));
    }

    int numMaterials = aiScene.mNumMaterials();
    PointerBuffer aiMaterials = aiScene.mMaterials();
    List<Material> materials = new ArrayList<>();
    for (int i = 0; i < numMaterials; i++) {
      AIMaterial aiMaterial = AIMaterial.create(aiMaterials.get(i));
      processMaterial(aiMaterial, materials, texturesDir);
    }

    int numMeshes = aiScene.mNumMeshes();
    PointerBuffer aiMeshes = aiScene.mMeshes();
    Mesh[] meshes = new Mesh[numMeshes];
    for (int i = 0; i < numMeshes; i++) {
      AIMesh aiMesh = AIMesh.create(aiMeshes.get(i));
      Mesh mesh = processMesh(aiMesh, materials, instances, resourcePath, i);
      meshes[i] = mesh;
    }

    return meshes;
  }

  protected static void processIndices(AIMesh aiMesh, List<Integer> indices) {
    int numFaces = aiMesh.mNumFaces();
    AIFace.Buffer aiFaces = aiMesh.mFaces();
    for (int i = 0; i < numFaces; i++) {
      AIFace aiFace = aiFaces.get(i);
      IntBuffer buffer = aiFace.mIndices();
      while (buffer.remaining() > 0) {
        indices.add(buffer.get());
      }
    }
  }

  protected static void processMaterial(AIMaterial aiMaterial, List<Material> materials,
      String texturesDir) throws IOException {
    AIColor4D colour = AIColor4D.create();

    Texture texture = null;
    if (texturesDir != null) {
      AIString path = AIString.calloc();
      Assimp.aiGetMaterialTexture(aiMaterial, aiTextureType_DIFFUSE, 0, path, (IntBuffer) null,
          null, null, null, null, null);
      String textPath = path.dataString();
      if (textPath != null && textPath.length() > 0) {
        TextureCache textCache = TextureCache.getInstance();
        String textureFile = texturesDir + "/" + textPath;
        textureFile = textureFile.replace("//", "/");
        texture = textCache.getTexture(textureFile);
      }
    }

    Vector4f ambient = Material.DEFAULT_COLOUR;
    int result = aiGetMaterialColor(aiMaterial, AI_MATKEY_COLOR_AMBIENT, aiTextureType_NONE, 0,
        colour);
    if (result == 0) {
      ambient = new Vector4f(colour.r(), colour.g(), colour.b(), colour.a());
    }

    Vector4f diffuse = Material.DEFAULT_COLOUR;
    result = aiGetMaterialColor(aiMaterial, AI_MATKEY_COLOR_DIFFUSE, aiTextureType_NONE, 0, colour);
    if (result == 0) {
      diffuse = new Vector4f(colour.r(), colour.g(), colour.b(), colour.a());
    }

    Vector4f specular = Material.DEFAULT_COLOUR;
    result = aiGetMaterialColor(aiMaterial, AI_MATKEY_COLOR_SPECULAR, aiTextureType_NONE, 0,
        colour);
    if (result == 0) {
      specular = new Vector4f(colour.r(), colour.g(), colour.b(), colour.a());
    }

    Material material = new Material(ambient, diffuse, specular, texture, 1.0f);
    materials.add(material);
  }

  private static Mesh processMesh(AIMesh aiMesh, List<Material> materials, int instances,
      String resourcePath, int index) {
    List<Float> vertices = new ArrayList<>();
    List<Float> textures = new ArrayList<>();
    List<Float> normals = new ArrayList<>();
    List<Integer> indices = new ArrayList<>();

    processVertices(aiMesh, vertices);
    processNormals(aiMesh, normals);
    processTextCoords(aiMesh, textures);
    processIndices(aiMesh, indices);

    String filename = String.format("%s/%s",
        resourcePath.substring(resourcePath.lastIndexOf('/') + 1), index);
    Mesh mesh = MeshUtils.internal_createMesh(ListUtils.floatListToArray(vertices),
        ListUtils.floatListToArray(textures), ListUtils.floatListToArray(normals),
        ListUtils.integerListToArray(indices), null, null, instances, null, filename);
    Material material;
    int materialIdx = aiMesh.mMaterialIndex();
    if (materialIdx >= 0 && materialIdx < materials.size()) {
      material = materials.get(materialIdx);
    } else {
      material = new Material();
    }
    mesh.setMaterial(material);

    return mesh;
  }

  // private static void processVertices(AIMesh aiMesh, List<Float> vertices) {
  // AIVector3D.Buffer aiVertices = aiMesh.mVertices();
  // while (aiVertices.remaining() > 0) {
  // AIVector3D aiVertex = aiVertices.get();
  // vertices.add(aiVertex.x());
  // vertices.add(aiVertex.y());
  // vertices.add(aiVertex.z());
  // }
  // }

  protected static void processNormals(AIMesh aiMesh, List<Float> normals) {
    AIVector3D.Buffer aiNormals = aiMesh.mNormals();
    while (aiNormals != null && aiNormals.remaining() > 0) {
      AIVector3D aiNormal = aiNormals.get();
      normals.add(aiNormal.x());
      normals.add(aiNormal.y());
      normals.add(aiNormal.z());
    }
  }

  protected static void processTextCoords(AIMesh aiMesh, List<Float> textures) {
    AIVector3D.Buffer textCoords = aiMesh.mTextureCoords(0);
    int numTextCoords = textCoords != null ? textCoords.remaining() : 0;
    for (int i = 0; i < numTextCoords; i++) {
      AIVector3D textCoord = textCoords.get();
      textures.add(textCoord.x());
      textures.add(1 - textCoord.y());
    }
  }

  // private static void processIndices(AIMesh aiMesh, List<Integer> indices) {
  // int numFaces = aiMesh.mNumFaces();
  // AIFace.Buffer aiFaces = aiMesh.mFaces();
  // for (int i = 0; i < numFaces; i++) {
  // AIFace aiFace = aiFaces.get(i);
  // IntBuffer buffer = aiFace.mIndices();
  // while (buffer.remaining() > 0) {
  // indices.add(buffer.get());
  // }
  // }
  // }

  protected static void processVertices(AIMesh aiMesh, List<Float> vertices) {
    AIVector3D.Buffer aiVertices = aiMesh.mVertices();
    while (aiVertices.remaining() > 0) {
      AIVector3D aiVertex = aiVertices.get();
      vertices.add(aiVertex.x());
      vertices.add(aiVertex.y());
      vertices.add(aiVertex.z());
    }
  }
}
