package game1.core.resources.spritesheets;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.joml.Vector3f;

public class SpritesheetTweaks {

  private Map<String, Object> data;

  public SpritesheetTweaks(Map<String, Object> data) {
    this.data = data;
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  private Optional<Object> get(String parent, String child) {
    List<LinkedHashMap<String, Object>> section = (ArrayList) data.get(parent);
    if (section == null) {
      return Optional.empty();
    }
    Optional<LinkedHashMap<String, Object>> findFirstMaybe = section.stream()
        .filter(k -> k.containsKey(child)).findFirst();
    if (findFirstMaybe.isEmpty()) {
      return Optional.empty();
    }
    Object object = findFirstMaybe.get().get(child);
    return Optional.of(object);
  }

  public Optional<Float> getAsFloat(String parent, String child) {
    Optional<Object> objectMaybe = get(parent, child);
    if (objectMaybe.isEmpty()) {
      return Optional.ofNullable(null);
    }
    Object object = objectMaybe.get();
    if (object instanceof Double) {
      return Optional
          .of(Float.valueOf(BigDecimal.valueOf((Double) objectMaybe.get()).floatValue()));
    } else if (object instanceof Integer) {
      return Optional
          .of(Float.valueOf(BigDecimal.valueOf((Integer) objectMaybe.get()).floatValue()));
    } else {
      throw new RuntimeException(
          String.format("Unable to convert value of type %s to float", object.getClass()));
    }
  }

  public Optional<Integer> getAsInteger(String parent, String child) {
    Optional<Object> objectMaybe = get(parent, child);
    if (objectMaybe.isEmpty()) {
      return Optional.ofNullable(null);
    }
    return Optional.of((Integer) objectMaybe.get());
  }

  public Optional<Boolean> getAsBoolean(String parent, String child) {
    Optional<Object> objectMaybe = get(parent, child);
    if (objectMaybe.isEmpty()) {
      return Optional.ofNullable(null);
    }
    return Optional.of(Boolean.parseBoolean(objectMaybe.get().toString()));
  }

  public Optional<String> getAsString(String parent, String child) {
    Optional<Object> objectMaybe = get(parent, child);
    if (objectMaybe.isEmpty()) {
      return Optional.ofNullable(null);
    }
    return Optional.of(objectMaybe.get().toString());
  }

  public void applyGeneralTweaks(Map<String, SpriteMetadata> spriteMetadataMap) {
    spriteMetadataMap.keySet().forEach(sprite -> {
      SpriteMetadata spriteMetadata = spriteMetadataMap.get(sprite);
      applyScale("ALL", spriteMetadata);
      applyVisible("ALL", spriteMetadata);
    });
  }

  public void applyIndividualTweaks(Map<String, SpriteMetadata> spriteMetadataMap) {
    spriteMetadataMap.keySet().forEach(sprite -> {
      SpriteMetadata spriteMetadata = spriteMetadataMap.get(sprite);
      applyScale(sprite, spriteMetadata);
      applyVisible(sprite, spriteMetadata);
      applyTickRate(sprite, spriteMetadata);
      applyPosition(sprite, spriteMetadata);
    });
  }

  private void applyScale(String sprite, SpriteMetadata spriteMetadata) {
    getAsFloat(sprite, "scale").ifPresent(scale -> spriteMetadata.scale = scale);
  }

  private void applyVisible(String sprite, SpriteMetadata spriteMetadata) {
    getAsBoolean(sprite, "visible").ifPresent(visible -> spriteMetadata.visible = visible);
  }

  private void applyTickRate(String sprite, SpriteMetadata spriteMetadata) {
    getAsInteger(sprite, "tickRate")
        .ifPresent(tickRate -> spriteMetadata.tickRate = new int[] { tickRate });
  }

  private void applyPosition(String sprite, SpriteMetadata spriteMetadata) {
    getAsString(sprite, "position").ifPresent(position -> {
      String[] tokens = position.split(",\\s*|\\s+");
      float x = Float.parseFloat(tokens[0]);
      float y = Float.parseFloat(tokens[1]);
      float z = Float.parseFloat(tokens[2]);
      spriteMetadata.position = new Vector3f(x, y, z);
    });
  }
}
