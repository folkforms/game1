package game1.core.resources.loaders2b;

import java.io.IOException;
import java.util.Map;
import java.util.function.Supplier;

import game1.core.sound.SoundEffect;
import game1.datasets.MultiLoader;

public class SoundEffectMultiLoader extends MultiLoader {

  @Override
  public Map<String, Supplier<?>> load(String path) throws IOException {
    SoundEffect soundEffect = new SoundEffect(path, fileSystem.load(path));
    soundEffect.setChannel("master");
    soundEffect.setVolume(1);
    return Map.of(path, () -> soundEffect);
  }
}
