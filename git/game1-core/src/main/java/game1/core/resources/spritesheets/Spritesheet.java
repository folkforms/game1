package game1.core.resources.spritesheets;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import folkforms.fileutils.FileUtils;
import folkforms.log.Log;
import game1.core.graphics.Animation;
import game1.core.graphics.Image;
import game1.core.graphics.RenderedFont;
import game1.core.infrastructure.Texture;
import game1.debug.DebugScopes;
import game1.primitives.Rectangle;

public class Spritesheet {

  private String masterFile;
  private Map<String, SpriteMetadata> spriteMetadataMap = new HashMap<>();
  private Map<String, List<String>> fontData;

  public Spritesheet(List<Texture> textureList, List<List<String>> metadataList,
      SpritesheetTweaks tweaks, Map<String, List<String>> fontData, String masterFile) {
    this.fontData = fontData;
    this.masterFile = masterFile;

    List<SpriteMetadata> spriteMetadata = new ArrayList<>();
    for (int i = 0; i < textureList.size(); i++) {
      Texture texture = textureList.get(i);
      List<String> metadata = metadataList.get(i);
      spriteMetadata.addAll(loadPartialSpriteSheet(texture, metadata));
    }
    spriteMetadata.forEach(metadata -> spriteMetadataMap.put(metadata.spriteName, metadata));

    if (tweaks != null) {
      tweaks.applyGeneralTweaks(spriteMetadataMap);
      tweaks.applyIndividualTweaks(spriteMetadataMap);
    }
  }

  @Override
  public String toString() {
    return String.format("Spritesheet[%s]", masterFile);
  }

  private List<SpriteMetadata> loadPartialSpriteSheet(Texture texture, List<String> metadata) {
    List<SpriteMetadata> spriteMetadata = new ArrayList<>();
    for (int i = 0; i < metadata.size(); i++) {
      if (metadata.get(i).startsWith("animation:") || metadata.get(i).startsWith("image:")
          || metadata.get(i).startsWith("font:")) {
        SpriteMetadata extractMetadata = extractMetadata(metadata, i);
        extractMetadata.texture = texture;
        spriteMetadata.add(extractMetadata);
      }
    }
    return spriteMetadata;
  }

  // animation: abcd.png
  // ....frame: 0,0,64,64
  // ....frame: 64,0,64,64
  // ....frame: 128,0,64,64
  // ....frame: 192,0,64,64
  // ....timings: 100,100,100,100
  // image: click_10x16.png
  // ....frame: 1158,0,100,160
  private SpriteMetadata extractMetadata(List<String> metadata, int index) {
    SpriteMetadata spriteMetadata = new SpriteMetadata();
    // Parse first line
    String firstLineOfItem = metadata.get(index);
    if (metadata.get(index).startsWith("animation:")) {
      spriteMetadata.spriteName = firstLineOfItem.substring("animation: ".length());
      spriteMetadata.type = SpriteMetadata.ANIMATION;
    } else if (metadata.get(index).startsWith("font:")) {
      spriteMetadata.spriteName = firstLineOfItem.substring("font: ".length());
      spriteMetadata.type = SpriteMetadata.FONT;
      spriteMetadata.fontData = fontData
          .get(new FileUtils().removeExtension(spriteMetadata.spriteName));
    } else {
      spriteMetadata.spriteName = firstLineOfItem.substring("image: ".length());
      spriteMetadata.type = SpriteMetadata.IMAGE;
    }
    // Parse rest of content
    spriteMetadata.rectangles = new ArrayList<>();
    for (int i = index + 1; i < metadata.size(); i++) {
      String line = metadata.get(i);
      if (line.startsWith("animation:") || line.startsWith("image: ")
          || line.startsWith("font: ")) {
        break;
      } else if (line.startsWith("    frame: ")) {
        spriteMetadata.rectangles
            .add(Rectangle.parseString(line.substring("    frame: ".length())));
      } else if (line.startsWith("    timings: ")) {
        spriteMetadata.tickRate = parseTimings(line.substring("    timings: ".length()));
      }
    }
    return spriteMetadata;
  }

  /**
   * Parses a string of comma-separated digits into an <code>int[]</code>.
   *
   * @param string
   *          input string
   * @return integer array
   */
  private static int[] parseTimings(String string) {
    String[] tokens = string.split(",");
    int[] timings = new int[tokens.length];
    for (int i = 0; i < tokens.length; i++) {
      // Divide by 10 since Aseprite timings are in milliseconds but Game1 ticks are 100 per second
      timings[i] = Integer.parseInt(tokens[i]) / 10;
    }
    return timings;
  }

  public Map<String, Supplier<?>> createSuppliers() {
    Map<String, Supplier<?>> suppliers = new HashMap<>();
    Iterator<String> iterator = spriteMetadataMap.keySet().iterator();
    while (iterator.hasNext()) {
      String spriteName = iterator.next();
      SpriteMetadata spriteMetadata = spriteMetadataMap.get(spriteName);
      if (spriteMetadata.type.equals(SpriteMetadata.ANIMATION)) {
        suppliers.put(spriteName, createAnimationSupplier(spriteMetadata, false));
        suppliers.put(spriteName + "-mirrored", createAnimationSupplier(spriteMetadata, true));
      } else if (spriteMetadata.type.equals(SpriteMetadata.IMAGE)) {
        suppliers.put(spriteName, createImageSupplier(spriteMetadata, false));
        suppliers.put(spriteName + "-mirrored", createImageSupplier(spriteMetadata, true));
      } else if (spriteMetadata.type.equals(SpriteMetadata.FONT)) {
        suppliers.put(spriteName, createFontSupplier(spriteMetadata));
      } else {
        throw new RuntimeException(
            String.format("Sprite '%s' had unknown type '%s'", spriteName, spriteMetadata.type));
      }
    }
    Log.scoped(DebugScopes.GAME_CACHE, "Spritesheet '%s' adding suppliers: %s", masterFile,
        suppliers.keySet());
    return suppliers;
  }

  private Supplier<Animation> createAnimationSupplier(SpriteMetadata spriteMetadata,
      boolean mirrored) {
    Animation animation = Animation.internal_create(spriteMetadata.texture,
        spriteMetadata.rectangles, spriteMetadata.tickRate, mirrored);
    animation.setName(spriteMetadata.spriteName);
    animation.setScale(spriteMetadata.scale);
    animation.setVisible(spriteMetadata.visible);
    if (spriteMetadata.position != null) {
      animation.setPosition(spriteMetadata.position);
    }
    return () -> animation.getClone();
  }

  private Supplier<Image> createImageSupplier(SpriteMetadata spriteMetadata, boolean mirrored) {
    Image image = new Image(spriteMetadata.texture, spriteMetadata.rectangles.get(0), mirrored);
    image.setName(spriteMetadata.spriteName);
    image.setScale(spriteMetadata.scale);
    image.setVisible(spriteMetadata.visible);
    return () -> image.getClone();
  }

  private Supplier<RenderedFont> createFontSupplier(SpriteMetadata spriteMetadata) {
    RenderedFont renderedFont = RenderedFont.newInstance(spriteMetadata.texture,
        spriteMetadata.rectangles.get(0), spriteMetadata.fontData);
    return () -> renderedFont;
  }
}
