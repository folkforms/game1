package game1.core.resources.utils;

import static org.lwjgl.BufferUtils.createByteBuffer;

import java.nio.ByteBuffer;

import org.lwjgl.BufferUtils;

public class ByteBufferUtils {

  public static ByteBuffer convertBytesToByteBuffer(byte[] bytes) {
    ByteBuffer byteBuffer = createByteBuffer(bytes.length);
    byteBuffer.put(bytes);
    byteBuffer = resizeBuffer(byteBuffer, byteBuffer.capacity() * 2);
    byteBuffer.flip();
    return byteBuffer;
  }

  public static ByteBuffer resizeBuffer(ByteBuffer buffer, int newCapacity) {
    ByteBuffer newBuffer = BufferUtils.createByteBuffer(newCapacity);
    buffer.flip();
    newBuffer.put(buffer);
    return newBuffer;
  }
}
