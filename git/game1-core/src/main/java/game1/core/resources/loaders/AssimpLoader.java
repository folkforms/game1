package game1.core.resources.loaders;

import static org.lwjgl.assimp.Assimp.aiProcess_FixInfacingNormals;
import static org.lwjgl.assimp.Assimp.aiProcess_GenSmoothNormals;
import static org.lwjgl.assimp.Assimp.aiProcess_JoinIdenticalVertices;
import static org.lwjgl.assimp.Assimp.aiProcess_LimitBoneWeights;
import static org.lwjgl.assimp.Assimp.aiProcess_Triangulate;

import java.io.IOException;

import game1.core.engine.EngineInternal;
import game1.core.graphics.Animation3D;
import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.graphics.Material;
import game1.core.graphics.meshes.InstancedMesh;
import game1.core.graphics.meshes.Mesh;
import game1.core.infrastructure.Texture;
import game1.core.pipelines.GraphicsPipelineName;
import game1.core.resources.loaders.assimp.AnimatedMeshesData;
import game1.core.resources.loaders.assimp.AnimatedMeshesLoader;
import game1.core.resources.loaders.assimp.StaticMeshesLoader;

// FIXME ASSIMP: You can probably use loadStatic for images based on the resource file extension,
// and make loadImage private (i.e. load -> if ends with "png/jpg/bmp/gif" it's an image, otherwise
// assume it's a model.

// FIXME ASSIMP: When loading a texture we should put the texture into Assets (use filename as asset
// name) to avoid reloading the same texture twice

// FIXME ASSIMP:
// (x) modelPath(path)           [just for brevity]
// (x) sameTexturesDir()         [throws error if model file not set]
// (x) texturesDir(path)         [for models with textures in their directory]
// (x) useTexture(path)          [load and use this texture for all meshes]
// (x) useNormal(path)           [load and use this normal mapping for all meshes]
// ( ) resize(w,h)               [for loading images] (???)
// (x) scale(scale)              [scales the mesh/image]
// (x) instances(num)            [number of instances]
// ( ) setSprite(x,y,w,h)        [for loading images]
// ( ) position(x,y,z)           [sets position]
// ( ) text(String)              [for creating text]
// ( ) text(String, face)        [for creating text]
// ( ) text(String, scale)       [for creating text]
// ( ) text(String, face, scale) [for creating text]
// (x) loadImage(Image)          [for loading images from spritesheets]

/**
 * Loader class for creating Drawables. Starts with a call to {@link #init()} and ends with a call
 * to {@link #load()} or {@link #loadAnimated()}.
 *
 * <p>
 * Example:
 * </p>
 * <p>
 * <code>Drawable drawable = AssimpLoader.init(Pipeline.SCENE_3D).modelPath("./foo/foo.obj").sameTexturesDir().scale(5).instances(10000).load();</code>
 * </p>
 */
public class AssimpLoader {

  private static final int defaultFlagsStatic = aiProcess_GenSmoothNormals
      | aiProcess_JoinIdenticalVertices | aiProcess_Triangulate | aiProcess_FixInfacingNormals;
  private static final int defaultFlagsAnimated = aiProcess_GenSmoothNormals
      | aiProcess_JoinIdenticalVertices | aiProcess_Triangulate | aiProcess_FixInfacingNormals
      | aiProcess_LimitBoneWeights;

  private String modelPath;
  private String texturesDir;
  private String textureFile;
  private String normalMappingFile;
  private Colour colour;
  private int instances = 1;
  private int flags = -1;
  private static GraphicsPipelineName defaultPipeline;
  private GraphicsPipelineName pipeline;
  private float scale = 1;

  private AssimpLoader(GraphicsPipelineName pipeline) {
    this.pipeline = pipeline;
  }

  private AssimpLoader() {
    this(defaultPipeline);
  }

  /**
   * Initialises a new {@link AssimpLoader} object with the given pipeline.
   *
   * @return the {@link AssimpLoader} object
   */
  public static AssimpLoader init(GraphicsPipelineName pipeline) {
    return new AssimpLoader(pipeline);
  }

  /**
   * Initialises a new {@link AssimpLoader} object with the default pipeline (which can be set using
   * {@link AssimpLoader#setDefaultPipeline} or {@link AssimpLoader#init(String)}.
   *
   * @return the {@link AssimpLoader} object
   * @see #setDefaultPipeline(String)
   */
  public static AssimpLoader init() {
    if (defaultPipeline == null) {
      throw new RuntimeException(
          "AssimpLoader.init() was called but no default pipeline was set. Either call AssimpLoader.setDefaultPipeline or use AssimpLoader.init(String pipeline).");
    }
    return new AssimpLoader(defaultPipeline);
  }

  /**
   * Sets the default pipeline. You can now use {@link #init()} rather than specifying the pipeline
   * every time.
   *
   * @param pipeline
   *          pipeline to set as default
   */
  public static void setDefaultPipeline(GraphicsPipelineName pipeline) {
    defaultPipeline = pipeline;
  }

  /**
   * Sets the scale of the Drawable to the given value.
   *
   * @param scale
   *          scale value
   * @return the {@link AssimpLoader} object
   */
  public AssimpLoader scale(float scale) {
    this.scale = scale;
    return this;
  }

  /**
   * Sets the path to the model file.
   *
   * @param modelPath
   *          the path to the model file
   * @return the {@link AssimpLoader} object
   */
  public AssimpLoader modelPath(String modelPath) {
    this.modelPath = modelPath;
    return this;
  }

  /**
   * Loads the given texture and applies it to all meshes in the model.
   *
   * @param textureFile
   *          texture file to load
   * @return the {@link AssimpLoader} object
   * @see #sameTexturesDir()
   * @see #texturesDir(String)
   */
  public AssimpLoader useTexture(String textureFile) {
    if (texturesDir != null) {
      throw new RuntimeException(String.format(
          "Cannot call setTexture if texturesDir or sameTexturesDir have been called. (Called useTexture('%s'), modelPath='%s', texturesDir='%s'.)",
          textureFile, modelPath, texturesDir));
    }
    this.textureFile = textureFile;
    return this;
  }

  /**
   * Loads the given normal mapping and applies it to all meshes in the model.
   *
   * @param normalMappingFile
   *          normal mapping file file to load
   * @return the {@link AssimpLoader} object
   * @see #useTexture(String)
   * @see #sameTexturesDir()
   * @see #texturesDir(String)
   */
  public AssimpLoader useNormalMap(String normalMappingFile) {
    if (texturesDir != null) {
      throw new RuntimeException(String.format(
          "Cannot call useNormal if texturesDir or sameTexturesDir have been called. (Called useNormalMap('%s'), modelPath='%s', texturesDir='%s'.)",
          normalMappingFile, modelPath, texturesDir));
    }
    if (textureFile == null) {
      throw new RuntimeException(String.format(
          "useNormalMap can only be called after useTexture has been called. (Called useTexture('%s'), modelPath='%s'.)",
          normalMappingFile, modelPath));
    }
    this.normalMappingFile = normalMappingFile;
    return this;
  }

  /**
   * Create a material of the given {@link Colour} and apply it to all meshes in the model.
   *
   * <p>
   * Note that very bright values will not reflect light well.
   * </p>
   *
   * @param colour
   *          colour to use
   * @return the {@link AssimpLoader} object
   */
  public AssimpLoader colour(Colour colour) {
    if (texturesDir != null) {
      throw new RuntimeException(String.format(
          "Cannot call colour if texturesDir or sameTexturesDir have been called. (Called colour('%s'), modelPath='%s', texturesDir='%s'.)",
          colour, modelPath, texturesDir));
    }
    if (textureFile != null) {
      throw new RuntimeException(String.format(
          "Cannot call colour if useTexture has been called. (Called colour('%s'), modelPath='%s', textureFile='%s'.)",
          colour, modelPath, textureFile));
    }
    this.colour = colour;
    return this;
  }

  /**
   * Sets the textures directory to the given path.
   *
   * @param texturesDir
   *          the textures directory
   * @return the {@link AssimpLoader} object
   * @see #sameTexturesDir()
   * @see #useTexture(String)
   */
  public AssimpLoader texturesDir(String texturesDir) {
    if (textureFile != null) {
      throw new RuntimeException(String.format(
          "Cannot call texturesDir if setTexture has been called. (Called texturesDir('%s'), modelPath='%s', textureFile='%s'.)",
          texturesDir, modelPath, textureFile));
    }
    this.texturesDir = texturesDir;
    return this;
  }

  /**
   * Sets the textures directory to be the same as the model directory.
   *
   * @return the {@link AssimpLoader} object
   * @see #texturesDir(String)
   * @see #useTexture(String)
   */
  public AssimpLoader sameTexturesDir() {
    if (textureFile != null) {
      throw new RuntimeException(String.format(
          "Cannot call sameTexturesDir if setTexture has already been called. (Called sameTexturesDir(), modelPath='%s', textureFile='%s'.)",
          modelPath, textureFile));
    }
    if (modelPath == null) {
      throw new RuntimeException(String.format(
          "sameTexturesDir can only be called after calling modelPath. (Called sameTexturesDir(), modelPath='%s'.)"));
    }
    texturesDir = modelPath.substring(0, modelPath.lastIndexOf('/'));
    return this;
  }

  /**
   * Sets the number of instances to use for the meshes that will be loaded.
   *
   * <p>
   * Note that the loader will still use an {@link InstancedMesh} even if you specify one instance.
   * This is fine. But if for some reason you wish to use a non-instanced {@link Mesh} you can set
   * the number of instances to '0'.
   * </p>
   *
   * @param instances
   *          number of instances
   * @return the {@link AssimpLoader} object
   */
  public AssimpLoader instances(int instances) {
    this.instances = instances;
    return this;
  }

  /**
   * Sets the flags to be used when loading this object.
   *
   * @param flags
   *          flags to set
   * @return the {@link AssimpLoader} object
   */
  public AssimpLoader setFlags(int flags) {
    this.flags = flags;
    return this;
  }

  // /**
  // * Loads a {@link Drawable} from an {@link Image} using the options that were chosen in previous
  // * methods.
  // *
  // * @param image
  // * image to create the {@link Drawable} from
  // * @return a {@link Drawable} created from an {@link Image}.
  // * @see #scale(int)
  // */
  // public Drawable load(Image image) {
  // if (texturesDir != null) {
  // throw new RuntimeException(
  // "Cannot use sameTexturesDir or texturesDir when calling loadImage.");
  // }
  // if (textureFile != null) {
  // throw new RuntimeException("Cannot use useTexture when calling loadImage.");
  // }
  // if (modelPath != null) {
  // throw new RuntimeException("Cannot use modelPath when calling loadImage.");
  // }
  //
  // Mesh mesh = MeshUtils.internal_createImageMesh(image);
  // Drawable d = new Drawable(pipeline, mesh);
  // d.setScale(scale);
  // return d;
  // }

  // /**
  // * Loads a {@link Mesh} using the options that were chosen in previous methods. This is used in
  // * rare situations where we want to stretch an existing mesh rather than scale it.
  // *
  // * @return a {@link Drawable} object loaded using the chosen options
  // * @throws IOException
  // * if an error occurs loading the data
  // */
  // public Mesh loadMesh(Image image) {
  // return MeshUtils.internal_createImageMesh(image);
  // }

  /**
   * Loads a {@link Drawable} using the options that were chosen in previous methods.
   *
   * @return a {@link Drawable} object loaded using the chosen options
   * @throws IOException
   *           if an error occurs loading the data
   */
  public Drawable load() throws IOException {
    if (flags == -1) {
      flags = defaultFlagsStatic;
    }
    Mesh[] meshes = StaticMeshesLoader.load(modelPath, texturesDir, instances, flags);
    applyTextures(meshes);

    Drawable drawable = new Drawable(pipeline, meshes);
    drawable.setScale(scale);
    return drawable;
  }

  /**
   * Loads an {@link Animated3D} using the options that were chosen in previous methods.
   *
   * @return a {@link Animated3D} object loaded using the chosen options
   * @throws IOException
   *           if an error occurs loading the data
   */
  public Animation3D loadAnimated() throws IOException {
    if (flags == -1) {
      flags = defaultFlagsAnimated;
    }
    AnimatedMeshesData animatedMeshesData = AnimatedMeshesLoader.loadAnimated(modelPath,
        texturesDir, 0, flags);
    Animation3D animation3D = new Animation3D(pipeline, animatedMeshesData);
    animation3D.setScale(scale);
    return animation3D;
  }

  private void applyTextures(Mesh[] meshes) throws IOException {
    boolean apply = false;
    Material material = new Material();
    if (textureFile != null) {
      Texture texture = EngineInternal.TEXTURE_LOADER.loadTexture(textureFile);
      material.setTexture(texture);
      if (normalMappingFile != null) {
        Texture normalMapping = EngineInternal.TEXTURE_LOADER.loadTexture(normalMappingFile);
        material.setNormalMap(normalMapping);
      }
      apply = true;
    } else if (colour != null) {
      material.setAmbientColour(colour);
      material.setDiffuseColour(colour);
      material.setSpecularColour(colour);
      material.setReflectance(0.5f);
      apply = true;
    }
    if (apply) {
      for (Mesh mesh : meshes) {
        mesh.setMaterial(material);
      }
    }
  }
}
