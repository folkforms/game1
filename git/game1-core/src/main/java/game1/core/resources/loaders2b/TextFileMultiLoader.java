package game1.core.resources.loaders2b;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import game1.datasets.MultiLoader;

public class TextFileMultiLoader extends MultiLoader {

  @Override
  public Map<String, Supplier<?>> load(String path) throws IOException {
    List<String> list = fileSystem.loadList(path);
    return Map.of(path, () -> list);
  }
}
