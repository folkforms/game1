package game1.core.resources.loaders;

import java.io.IOException;

import org.joml.Vector3f;

import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.pipelines.GraphicsPipelineName;
import game1.filesystem.FileSystem;

public class DrawableLoader implements Loader {

  private GraphicsPipelineName pipeline = GraphicsPipelineName.SCENE_2D;
  private String name;
  private int instances = 1;
  private String modelPath;
  private String texturePath;
  private boolean sameTexturesDir = false;
  private String normalMap;
  private Vector3f position = new Vector3f();
  private int rotateX, rotateY, rotateZ;
  private float scale = 1;
  private Colour colour = null;
  private boolean visible = true;
  private boolean animated = false;

  public static DrawableLoader init(GraphicsPipelineName pipeline) {
    DrawableLoader drawableLoader = new DrawableLoader();
    drawableLoader.pipeline = pipeline;
    return drawableLoader;
  }

  public DrawableLoader name(String name) {
    this.name = name;
    return this;
  }

  public DrawableLoader instances(int instances) {
    this.instances = instances;
    return this;
  }

  public DrawableLoader model(String modelPath) {
    validateFileExists(modelPath);
    this.modelPath = modelPath;
    return this;
  }

  public DrawableLoader texture(String texturePath) {
    validateFileExists(texturePath);
    this.texturePath = texturePath;
    return this;
  }

  public DrawableLoader sameTexturesDir() {
    sameTexturesDir = true;
    return this;
  }

  public DrawableLoader normalMap(String normalMap) {
    validateFileExists(normalMap);
    this.normalMap = normalMap;
    return this;
  }

  public DrawableLoader scale(float scale) {
    this.scale = scale;
    return this;
  }

  public DrawableLoader position(float x, float y) {
    position.x = x;
    position.y = y;
    return this;
  }

  public DrawableLoader position(float x, float y, float z) {
    position.x = x;
    position.y = y;
    position.z = z;
    return this;
  }

  public DrawableLoader rotation(int rx, int ry, int rz) {
    this.rotateX = rx;
    this.rotateY = ry;
    this.rotateZ = rz;
    return this;
  }

  public DrawableLoader colour(Colour colour) {
    this.colour = colour;
    return this;
  }

  public DrawableLoader colour(float r, float g, float b, float a) {
    return this.colour(new Colour(r, g, b, a));
  }

  public DrawableLoader colour(float r, float g, float b) {
    return this.colour(r, g, b, 1);
  }

  public DrawableLoader visible(boolean visible) {
    this.visible = visible;
    return this;
  }

  public DrawableLoader animated(boolean animated) {
    this.animated = animated;
    return this;
  }

  @Override
  // FIXME DATASETS: Need to clean up the AssimpLoader code since DrawableLoader is basically
  // duplicating it here.
  public Drawable load(FileSystem fileSystem) throws IOException {
    if (modelPath != null) {
      AssimpLoader assimpLoader = AssimpLoader.init(pipeline).instances(instances)
          .modelPath(modelPath).scale(scale);
      if (texturePath != null) {
        assimpLoader = assimpLoader.useTexture(texturePath);
      }
      if (sameTexturesDir) {
        assimpLoader = assimpLoader.sameTexturesDir();
      }
      if (normalMap != null) {
        assimpLoader = assimpLoader.useNormalMap(normalMap);
      }
      if (colour != null) {
        assimpLoader = assimpLoader.colour(colour);
      }
      Drawable drawable;
      if (animated) {
        drawable = assimpLoader.loadAnimated();
      } else {
        drawable = assimpLoader.load();
      }
      drawable.setVisible(visible);
      drawable.setPosition(position);
      drawable.rotate(rotateX, rotateY, rotateZ);
      if (name != null) {
        drawable.setName(name);
      }
      return drawable;
    } else {
      // FIXME FILE_SYSTEM: Implement a validate method so that we can't have conflicting settings.
      // Also override DrawableLoader.toString() [Not 100% sure what the former is referring to]
      throw new RuntimeException(String.format(
          "Could not figure out how to construct Drawable from DrawableLoader data: %s", this));
    }
  }

  @Override
  public void unload(FileSystem fileSystem) {
    // FIXME FILE_SYSTEM: Implement this later. What we unload will depend on the type of Drawable.
    // For example, spritesheets will call Engine.FILE_SYSTEM.unload(spritesheetMasterFile), which
    // will trigger unloading of the sub-files and textures.
  }
}
