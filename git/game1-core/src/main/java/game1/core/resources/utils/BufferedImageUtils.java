package game1.core.resources.utils;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import game1.filesystem.FileSystemFactory;

/**
 * Image utilities.
 */
public class BufferedImageUtils {

  /**
   * Loads a single image from a file and scales it by the given amount.
   *
   * @param path
   *          path to the file
   * @param scale
   *          amount to scale the loaded image by
   * @return a BufferedImage containing the image data
   * @throws IOException
   *           if an error occurs loading the image
   */
  public static BufferedImage loadBufferedImage(String path, int scale) throws IOException {
    try {
      return loadBufferedImage(new ByteArrayInputStream(FileSystemFactory.getInstance().load(path)),
          scale);
    } catch (IOException ex) {
      throw new IOException(String.format("Could not load image '%s'.", path), ex);
    }
  }

  public static BufferedImage loadBufferedImage(String path) throws IOException {
    return loadBufferedImage(path, 1);
  }

  private static BufferedImage loadBufferedImage(InputStream inputStream, int scale)
      throws IOException {
    BufferedImage before = ImageIO.read(inputStream);

    if (before.getType() != BufferedImage.TYPE_4BYTE_ABGR) {
      before = convertToFourByteABGR(before);
    }

    BufferedImage after = new BufferedImage(before.getWidth() * scale, before.getHeight() * scale,
        BufferedImage.TYPE_4BYTE_ABGR);
    AffineTransform at = new AffineTransform();
    at.scale(scale, scale);
    AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
    after = scaleOp.filter(before, after);
    inputStream.close();
    return after;
  }

  private static BufferedImage convertToFourByteABGR(BufferedImage image) {
    BufferedImage after = new BufferedImage(image.getWidth(), (image.getHeight()),
        BufferedImage.TYPE_4BYTE_ABGR);
    after.getGraphics().drawImage(image, 0, 0, image.getWidth(), image.getHeight(), null);
    return after;
  }
}
