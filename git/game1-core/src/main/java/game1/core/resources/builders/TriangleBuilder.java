package game1.core.resources.builders;

import org.joml.Vector3f;

import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.graphics.meshes.Mesh;
import game1.core.graphics.meshes.TriangleMeshData;
import game1.core.pipelines.GraphicsPipelineName;

public class TriangleBuilder {

  private GraphicsPipelineName pipeline;
  private Colour colour = new Colour(1, 1, 1);
  private Vector3f p1, p2, p3;
  private Vector3f position = new Vector3f(0, 0, 0);

  public static TriangleBuilder init(GraphicsPipelineName pipeline) {
    TriangleBuilder triangleBuilder = new TriangleBuilder();
    triangleBuilder.pipeline = pipeline;
    return triangleBuilder;
  }

  // Points are the first triangle in a lower-case 'n'-shaped quad i.e. 0/1/2 or 0,0 -> 0,1 -> 1,1
  public TriangleBuilder points(Vector3f p1, Vector3f p2, Vector3f p3) {
    this.p1 = p1;
    this.p2 = p2;
    this.p3 = p3;
    return this;
  }

  public TriangleBuilder position(Vector3f position) {
    this.position = position;
    return this;
  }

  public TriangleBuilder colour(Colour colour) {
    this.colour = colour;
    return this;
  }

  public TriangleBuilder colour(String colourStr) {
    this.colour = Colour.fromHex(colourStr);
    return this;
  }

  public Drawable build() {
    Mesh mesh = new TriangleMeshData(p1, p2, p3).create();
    Drawable drawable = new Drawable(pipeline, mesh);
    drawable.setPosition(position);
    drawable.setColour(colour);
    return drawable;
  }
}
