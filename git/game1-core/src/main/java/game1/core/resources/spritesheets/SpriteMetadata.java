package game1.core.resources.spritesheets;

import java.util.Arrays;
import java.util.List;

import org.joml.Vector3f;

import game1.core.infrastructure.Texture;
import game1.primitives.Rectangle;

class SpriteMetadata {
  final static String ANIMATION = "animation";
  final static String IMAGE = "image";
  final static String FONT = "font";

  String spriteName;
  String type;
  Texture texture;
  float scale = 1;
  List<Rectangle> rectangles;
  int[] tickRate;
  List<String> fontData;
  Vector3f position;
  boolean visible = true;

  @Override
  public String toString() {
    return String.format(
        "SpriteMetadata[spriteName=%s,type=%s,texture=%s,scale=%s,rectangles=%s,tickRate=%s,fontData=%s,position=%s,visible=%s]",
        spriteName, type, texture, scale, rectangles, Arrays.toString(tickRate), fontData, position,
        visible);
  }
}
