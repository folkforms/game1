package game1.core.resources.loaders.assimp;

import java.util.Map;

import game1.core.graphics.Animation3D;
import game1.core.graphics.internal.Animation3DFrames;
import game1.core.graphics.meshes.Mesh;

/**
 * Convenience class so we can return both a <code>Mesh[]</code> and a
 * <code>Map&lt;String, Animation3D&gt;</code> from <code>AssimpLoader.loadAnimated</code>. This is
 * designed to be passed to the {@link Animation3D} constructor.
 */
public class AnimatedMeshesData {
  private Mesh[] meshes;
  private Map<String, Animation3DFrames> animations;

  public AnimatedMeshesData(Mesh[] meshes, Map<String, Animation3DFrames> animations,
      String resourcePath) {
    this.meshes = meshes;
    this.animations = animations;
  }

  public Mesh[] getMeshes() {
    return meshes;
  }

  public Map<String, Animation3DFrames> getAnimations() {
    return animations;
  }

  @Override
  public String toString() {
    return String.format("AnimatedMeshesData@%s[%s meshes, %s animations %s]", hashCode(),
        meshes.length, animations.keySet().size(), animations.keySet());
  }
}
