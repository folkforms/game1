package game1.core.resources.loaders2b;

import java.io.IOException;
import java.util.Map;
import java.util.function.Supplier;

import org.joml.Vector3f;

import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.pipelines.GraphicsPipelineName;
import game1.core.resources.loaders.AssimpLoader;
import game1.datasets.MultiLoader;

public class Drawable2DMultiLoader extends MultiLoader {

  @Override
  public Map<String, Supplier<?>> load(String path) throws IOException {
    int instances = options.getOrDefault("instances", 1);
    float scale = options.getOrDefault("scale", 1f);

    AssimpLoader assimpLoader = AssimpLoader.init(GraphicsPipelineName.SCENE_2D).instances(instances)
        .modelPath(path).scale(scale);
    // FIXME DATASETS: Add these other options as well...
    // if (texturePath != null) {
    // assimpLoader = assimpLoader.useTexture(texturePath);
    // }
    // if (sameTexturesDir) {
    // assimpLoader = assimpLoader.sameTexturesDir();
    // }
    // if (normalMap != null) {
    // assimpLoader = assimpLoader.useNormalMap(normalMap);
    // }
    String colourStr = options.get("colour");
    Colour colour = colourStr != null ? Colour.fromHex(colourStr) : null;
    if (colour != null) {
      assimpLoader = assimpLoader.colour(colour);
    }

    Drawable drawable = assimpLoader.load();

    Object positionInt = options.get("positionInt");
    if (positionInt != null) {
      Vector3f pos = parsePosition(positionInt.toString());
      drawable.setPosition(pos);
    }

    Object visible = options.getOrDefault("visible", true);
    boolean visible2 = Boolean.parseBoolean(visible.toString());
    drawable.setVisible(visible2);
    // FIXME DATASETS: Add these other options as well...
    // drawable.rotate(rotateX, rotateY, rotateZ);
    // if (name != null) {
    // drawable.setName(name);
    // }
    return Map.of(path, () -> drawable.getClone());
  }

  private Vector3f parsePosition(String posStr) {
    posStr = posStr.replaceAll("\\(", "").replaceAll("\\)", "").trim();
    String[] tokens = posStr.split("\\s+");
    Vector3f pos = new Vector3f();
    pos.x = Float.parseFloat(tokens[0]);
    pos.y = Float.parseFloat(tokens[1]);
    pos.z = Float.parseFloat(tokens[2]);
    return pos;
  }
}
