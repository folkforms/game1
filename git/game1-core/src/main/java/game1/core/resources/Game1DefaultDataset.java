package game1.core.resources;

import game1.core.engine.Engine;

public class Game1DefaultDataset {

  public static final String TAG = "Game1DefaultDatasetTag";

  // private final String defaultSpritesheetPath = Engine.PROPERTIES
  // .getProperty("game1.core_spritesheet");
  // private final SpritesheetLoader2 spritesheetLoader;

  public Game1DefaultDataset() {
    Engine.GAME_FILES.register(Engine.PROPERTIES.getProperty("game1.core_spritesheet"), TAG);
    Engine.GAME_FILES.setPersistent(TAG);

    // FIXME DATASETS: Remove old code...
    // spritesheetLoader = new SpritesheetLoader2("game1-default-spritesheet",
    // defaultSpritesheetPath,
    // 1);
    // loadDefaultFont();
    // loadDefaultCursor();
    loadTwoSecondSilence();
  }

  // private void loadDefaultFont() {
  // String defaultFontSpriteName = Engine.PROPERTIES.getProperty("game1.default_font.sprite");
  // String defaultFontDataPath =
  // Engine.PROPERTIES.getProperty("game1.default_font.font_data_path");
  // // register("game1.default_font",
  // // FontLoader.init(defaultSpritesheetPath, defaultFontSpriteName, defaultFontDataPath));
  // FontLoader2 fontLoader = new FontLoader2("game1.default_font", spritesheetLoader,
  // defaultFontSpriteName, defaultFontDataPath, 1);
  // Engine.ACTOR_LOADERS.register(fontLoader, "Game1DefaultTag", true);
  // }

  // private void loadDefaultCursor() {
  // String cursorImageName = Engine.PROPERTIES.getProperty("game1.default_cursor.sprite");
  // // register("game1.default_cursor",
  // // SpritesheetLoader.init(defaultSpritesheetPath).image(cursorImageName));
  // ImageLoader imageLoader = new ImageLoader("game1.default_cursor", cursorImageName,
  // spritesheetLoader);
  // Engine.ACTOR_LOADERS.register(imageLoader, "Game1DefaultTag", true);
  // }

  private void loadTwoSecondSilence() {
    Engine.GAME_FILES.register("sounds/two_second_silence.ogg", TAG);
  }
}
