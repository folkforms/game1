package game1.core.resources.loaders2b;

import java.io.IOException;
import java.util.Map;
import java.util.function.Supplier;

import folkforms.yaml.YamlFile;
import game1.datasets.MultiLoader;

public class YamlFileMultiLoader extends MultiLoader {

  @Override
  public Map<String, Supplier<?>> load(String path) throws IOException {
    String collect = fileSystem.loadString(path);
    YamlFile yamlFile = new YamlFile(collect);
    return Map.of(path, () -> yamlFile);
  }
}
