package game1.core.resources.loaders.assimp;

import static org.lwjgl.assimp.Assimp.aiGetErrorString;
import static org.lwjgl.assimp.Assimp.aiImportFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.lwjgl.PointerBuffer;
import org.lwjgl.assimp.AIAnimation;
import org.lwjgl.assimp.AIBone;
import org.lwjgl.assimp.AIMaterial;
import org.lwjgl.assimp.AIMatrix4x4;
import org.lwjgl.assimp.AIMesh;
import org.lwjgl.assimp.AINode;
import org.lwjgl.assimp.AINodeAnim;
import org.lwjgl.assimp.AIQuatKey;
import org.lwjgl.assimp.AIQuaternion;
import org.lwjgl.assimp.AIScene;
import org.lwjgl.assimp.AIVector3D;
import org.lwjgl.assimp.AIVectorKey;
import org.lwjgl.assimp.AIVertexWeight;

import game1.core.graphics.Material;
import game1.core.graphics.internal.Animation3DFrame;
import game1.core.graphics.internal.Animation3DFrames;
import game1.core.graphics.meshes.Mesh;
import game1.core.graphics.meshes.MeshUtils;

public class AnimatedMeshesLoader extends StaticMeshesLoader {

  public static AnimatedMeshesData loadAnimated(String resourcePath, String texturesDir,
      int instances, int flags) throws IOException {
    AIScene aiScene = aiImportFile(resourcePath, flags);
    if (aiScene == null) {
      String errorString = aiGetErrorString();
      throw new IOException(String.format(
          "Error loading model (resourcePath = '%s', texturesDir = '%s', flags = %s): %s",
          resourcePath, texturesDir, flags, errorString));
    }

    int numMaterials = aiScene.mNumMaterials();
    PointerBuffer aiMaterials = aiScene.mMaterials();
    List<Material> materials = new ArrayList<>();
    for (int i = 0; i < numMaterials; i++) {
      AIMaterial aiMaterial = AIMaterial.create(aiMaterials.get(i));
      processMaterial(aiMaterial, materials, texturesDir);
    }

    List<Bone> boneList = new ArrayList<>();
    int numMeshes = aiScene.mNumMeshes();
    PointerBuffer aiMeshes = aiScene.mMeshes();
    Mesh[] meshes = new Mesh[numMeshes];
    for (int i = 0; i < numMeshes; i++) {
      AIMesh aiMesh = AIMesh.create(aiMeshes.get(i));
      Mesh mesh = processMesh(aiMesh, materials, boneList, instances, resourcePath, i);
      meshes[i] = mesh;
    }

    AINode aiRootNode = aiScene.mRootNode();
    Matrix4f rootTransformation = AnimatedMeshesLoader.toMatrix(aiRootNode.mTransformation());
    Node rootNode = processNodesHierarchy(aiRootNode, null);
    Map<String, Animation3DFrames> animations = processAnimations(aiScene, boneList, rootNode,
        rootTransformation);

    AnimatedMeshesData animatedMeshesData = new AnimatedMeshesData(meshes, animations,
        resourcePath);
    return animatedMeshesData;
  }

  private static void buildTransformationMatrices(AINodeAnim aiNodeAnim, Node node) {
    int numFrames = aiNodeAnim.mNumPositionKeys();
    AIVectorKey.Buffer positionKeys = aiNodeAnim.mPositionKeys();
    AIVectorKey.Buffer scalingKeys = aiNodeAnim.mScalingKeys();
    AIQuatKey.Buffer rotationKeys = aiNodeAnim.mRotationKeys();

    for (int i = 0; i < numFrames; i++) {
      AIVectorKey aiVecKey = positionKeys.get(i);
      AIVector3D vec = aiVecKey.mValue();

      Matrix4f transfMat = new Matrix4f().translate(vec.x(), vec.y(), vec.z());

      AIQuatKey quatKey = rotationKeys.get(i);
      AIQuaternion aiQuat = quatKey.mValue();
      Quaternionf quat = new Quaternionf(aiQuat.x(), aiQuat.y(), aiQuat.z(), aiQuat.w());
      transfMat.rotate(quat);

      if (i < aiNodeAnim.mNumScalingKeys()) {
        aiVecKey = scalingKeys.get(i);
        vec = aiVecKey.mValue();
        transfMat.scale(vec.x(), vec.y(), vec.z());
      }

      node.addTransformation(transfMat);
    }
  }

  private static List<Animation3DFrame> buildAnimationFrames(List<Bone> boneList, Node rootNode,
      Matrix4f rootTransformation) {

    int numFrames = rootNode.getAnimationFrames();
    List<Animation3DFrame> frameList = new ArrayList<>();
    for (int i = 0; i < numFrames; i++) {
      Animation3DFrame frame = new Animation3DFrame();
      frameList.add(frame);

      int numBones = boneList.size();
      for (int j = 0; j < numBones; j++) {
        Bone bone = boneList.get(j);
        Node node = rootNode.findByName(bone.getBoneName());
        Matrix4f boneMatrix = Node.getParentTransforms(node, i);
        boneMatrix.mul(bone.getOffsetMatrix());
        boneMatrix = new Matrix4f(rootTransformation).mul(boneMatrix);
        frame.setMatrix(j, boneMatrix);
      }
    }

    return frameList;
  }

  private static Map<String, Animation3DFrames> processAnimations(AIScene aiScene,
      List<Bone> boneList, Node rootNode, Matrix4f rootTransformation) {
    Map<String, Animation3DFrames> animations = new HashMap<>();

    // Process all animations
    int numAnimations = aiScene.mNumAnimations();
    PointerBuffer aiAnimations = aiScene.mAnimations();
    int frameMarker = 0;
    for (int i = 0; i < numAnimations; i++) {
      AIAnimation aiAnimation = AIAnimation.create(aiAnimations.get(i));

      // Calculate transformation matrices for each node
      int numChanels = aiAnimation.mNumChannels();
      PointerBuffer aiChannels = aiAnimation.mChannels();
      for (int j = 0; j < numChanels; j++) {
        AINodeAnim aiNodeAnim = AINodeAnim.create(aiChannels.get(j));
        String nodeName = aiNodeAnim.mNodeName().dataString();
        Node node = rootNode.findByName(nodeName);
        buildTransformationMatrices(aiNodeAnim, node);
      }

      List<Animation3DFrame> frames = buildAnimationFrames(boneList, rootNode, rootTransformation);
      // The frames are one long list so we need to cut it up into sections
      int previousSize = frames.size();
      frames = stripPreviousFrames(frames, frameMarker);
      frameMarker = previousSize;

      Animation3DFrames animation = new Animation3DFrames(aiAnimation.mName().dataString(), frames,
          aiAnimation.mDuration());
      animations.put(animation.getName(), animation);
      // Log.temp("Animation: %s, %s frames, %s duration", animation.getName(),
      // animation.debug_getTotalFrames(), animation.getDuration());
    }

    return animations;
  }

  private static List<Animation3DFrame> stripPreviousFrames(List<Animation3DFrame> frames,
      int numPreviousFrames) {
    List<Animation3DFrame> newData = new ArrayList<>();
    for (int i = numPreviousFrames; i < frames.size(); i++) {
      newData.add(frames.get(i));
    }
    return newData;
  }

  private static void processBones(AIMesh aiMesh, List<Bone> boneList, List<Integer> boneIds,
      List<Float> weights) {
    Map<Integer, List<VertexWeight>> weightSet = new HashMap<>();
    int numBones = aiMesh.mNumBones();
    PointerBuffer aiBones = aiMesh.mBones();
    for (int i = 0; i < numBones; i++) {
      AIBone aiBone = AIBone.create(aiBones.get(i));
      int id = boneList.size();
      Bone bone = new Bone(id, aiBone.mName().dataString(), toMatrix(aiBone.mOffsetMatrix()));
      boneList.add(bone);
      int numWeights = aiBone.mNumWeights();
      AIVertexWeight.Buffer aiWeights = aiBone.mWeights();
      for (int j = 0; j < numWeights; j++) {
        AIVertexWeight aiWeight = aiWeights.get(j);
        VertexWeight vw = new VertexWeight(bone.getBoneId(), aiWeight.mVertexId(),
            aiWeight.mWeight());
        List<VertexWeight> vertexWeightList = weightSet.get(vw.getVertexId());
        if (vertexWeightList == null) {
          vertexWeightList = new ArrayList<>();
          weightSet.put(vw.getVertexId(), vertexWeightList);
        }
        vertexWeightList.add(vw);
      }
    }

    int numVertices = aiMesh.mNumVertices();
    for (int i = 0; i < numVertices; i++) {
      List<VertexWeight> vertexWeightList = weightSet.get(i);
      int size = vertexWeightList != null ? vertexWeightList.size() : 0;
      for (int j = 0; j < Mesh.MAX_WEIGHTS; j++) {
        if (j < size) {
          VertexWeight vw = vertexWeightList.get(j);
          weights.add(vw.getWeight());
          boneIds.add(vw.getBoneId());
        } else {
          weights.add(0.0f);
          boneIds.add(0);
        }
      }
    }
  }

  private static Mesh processMesh(AIMesh aiMesh, List<Material> materials, List<Bone> boneList,
      int instances, String resourcePath, int index) {
    List<Float> vertices = new ArrayList<>();
    List<Float> textures = new ArrayList<>();
    List<Float> normals = new ArrayList<>();
    List<Integer> indices = new ArrayList<>();
    List<Integer> boneIds = new ArrayList<>();
    List<Float> weights = new ArrayList<>();

    processVertices(aiMesh, vertices);
    processNormals(aiMesh, normals);
    processTextCoords(aiMesh, textures);
    processIndices(aiMesh, indices);
    processBones(aiMesh, boneList, boneIds, weights);

    String filename = String.format("%s/%s",
        resourcePath.substring(resourcePath.lastIndexOf('/') + 1), index);
    Mesh mesh = MeshUtils.internal_createMesh(ListUtils.floatListToArray(vertices),
        ListUtils.floatListToArray(textures), ListUtils.floatListToArray(normals),
        ListUtils.integerListToArray(indices), ListUtils.integerListToArray(boneIds),
        ListUtils.floatListToArray(weights), instances, null, filename);

    Material material;
    int materialIdx = aiMesh.mMaterialIndex();
    if (materialIdx >= 0 && materialIdx < materials.size()) {
      material = materials.get(materialIdx);
    } else {
      material = new Material();
    }
    mesh.setMaterial(material);

    return mesh;
  }

  private static Node processNodesHierarchy(AINode aiNode, Node parentNode) {
    String nodeName = aiNode.mName().dataString();
    Node node = new Node(nodeName, parentNode);

    int numChildren = aiNode.mNumChildren();
    PointerBuffer aiChildren = aiNode.mChildren();
    for (int i = 0; i < numChildren; i++) {
      AINode aiChildNode = AINode.create(aiChildren.get(i));
      Node childNode = processNodesHierarchy(aiChildNode, node);
      node.addChild(childNode);
    }

    return node;
  }

  private static Matrix4f toMatrix(AIMatrix4x4 aiMatrix4x4) {
    Matrix4f result = new Matrix4f();
    result.m00(aiMatrix4x4.a1());
    result.m10(aiMatrix4x4.a2());
    result.m20(aiMatrix4x4.a3());
    result.m30(aiMatrix4x4.a4());
    result.m01(aiMatrix4x4.b1());
    result.m11(aiMatrix4x4.b2());
    result.m21(aiMatrix4x4.b3());
    result.m31(aiMatrix4x4.b4());
    result.m02(aiMatrix4x4.c1());
    result.m12(aiMatrix4x4.c2());
    result.m22(aiMatrix4x4.c3());
    result.m32(aiMatrix4x4.c4());
    result.m03(aiMatrix4x4.d1());
    result.m13(aiMatrix4x4.d2());
    result.m23(aiMatrix4x4.d3());
    result.m33(aiMatrix4x4.d4());
    return result;
  }
}
