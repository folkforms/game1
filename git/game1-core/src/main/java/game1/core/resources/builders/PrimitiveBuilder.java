package game1.core.resources.builders;

import org.joml.Vector3f;

import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.graphics.meshes.Mesh;
import game1.core.graphics.meshes.PlaneUtils;
import game1.core.pipelines.GraphicsPipelineName;
import game1.primitives.RectangleF;

public class PrimitiveBuilder {

  private GraphicsPipelineName pipeline;
  private float x, y, z, w, h;
  private Colour colour = new Colour(1, 1, 1);
  private Vector3f position = new Vector3f(0, 0, 0);
  private boolean collisionMesh = false;

  public static PrimitiveBuilder init(GraphicsPipelineName pipeline) {
    PrimitiveBuilder primitive2DLoader = new PrimitiveBuilder();
    primitive2DLoader.pipeline = pipeline;
    return primitive2DLoader;
  }

  /**
   * Create a 2d rectangle of the given width and height.
   *
   * @param w
   *          width
   * @param h
   *          height
   * @return a rectangle created from the given inputs
   */
  public PrimitiveBuilder rect2d(float w, float h) {
    this.w = w;
    this.h = h;
    return this;
  }

  /**
   * Creates a plane that runs from 0,0,0 to x,y,z.
   *
   * @param x
   *          x coordinate
   * @param y
   *          y coordinate
   * @param z
   *          z coordinate
   * @return a plane created from the given inputs
   */
  public PrimitiveBuilder rect3d(float x, float y, float z) {
    this.x = x;
    this.y = y;
    this.z = z;
    return this;
  }

  public PrimitiveBuilder colour(Colour colour) {
    this.colour = colour;
    return this;
  }

  public PrimitiveBuilder colour(String colourStr) {
    this.colour = Colour.fromHex(colourStr);
    return this;
  }

  public PrimitiveBuilder colour(float r, float g, float b, float a) {
    return this.colour(new Colour(r, g, b, a));
  }

  public PrimitiveBuilder colour(float r, float g, float b) {
    return this.colour(r, g, b, 1);
  }

  public PrimitiveBuilder position(float x, float y) {
    position.x = x;
    position.y = y;
    return this;
  }

  public PrimitiveBuilder position(float x, float y, float z) {
    position.x = x;
    position.y = y;
    position.z = z;
    return this;
  }

  public PrimitiveBuilder setZ(float z) {
    position.z = z;
    return this;
  }

  public PrimitiveBuilder collisionMesh(boolean collisionMesh) {
    this.collisionMesh = collisionMesh;
    return this;
  }

  public PrimitiveBuilder from(RectangleF rect) {
    position.x = rect.x;
    position.y = rect.y;
    this.w = rect.w;
    this.h = rect.h;
    return this;
  }

  public Drawable build() {
    Mesh mesh;
    if (pipeline == GraphicsPipelineName.SCENE_2D) {
      mesh = PlaneUtils.createPlane2D(w, h, colour);
    } else {
      mesh = PlaneUtils.createPlane3D(x, y, z, colour);
    }
    Drawable drawable = new Drawable(pipeline, mesh);
    drawable.setPosition(position);
    drawable.setColour(colour);
    drawable.setCollisionMesh(collisionMesh);
    return drawable;
  }
}
