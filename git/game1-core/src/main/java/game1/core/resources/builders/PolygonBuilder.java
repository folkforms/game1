package game1.core.resources.builders;

import java.util.ArrayList;
import java.util.List;

import org.joml.Vector2f;
import org.joml.Vector3f;

import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.graphics.meshes.Mesh;
import game1.core.graphics.meshes.PolygonMeshData;
import game1.core.pipelines.GraphicsPipelineName;

// FIXME VIEWPORT: Split this into 2D and 3D versions
public class PolygonBuilder {

  private GraphicsPipelineName pipeline;
  private Colour colour = new Colour(1, 1, 1);
  private List<Vector3f> points = new ArrayList<>();
  private Vector3f position = new Vector3f(0, 0, 0);

  public static PolygonBuilder init(GraphicsPipelineName pipeline) {
    PolygonBuilder quadBuilder = new PolygonBuilder();
    quadBuilder.pipeline = pipeline;
    return quadBuilder;
  }

  // Points are n-shaped i.e. 0/1/2 or 0,0 -> 0,1 -> 1,1
  public PolygonBuilder points2d(List<Vector2f> pointsList) {
    List<Vector3f> points3d = pointsList.stream().map(p -> new Vector3f(p.x, p.y, 0)).toList();
    points.addAll(points3d);
    return this;
  }

  public PolygonBuilder points3d(List<Vector3f> pointsList) {
    points.addAll(pointsList);
    return this;
  }

  public PolygonBuilder position(Vector3f position) {
    this.position = position;
    return this;
  }

  public PolygonBuilder z(float z) {
    this.position.z = z;
    return this;
  }

  public PolygonBuilder colour(Colour colour) {
    this.colour = colour;
    return this;
  }

  public PolygonBuilder colour(String colourStr) {
    this.colour = Colour.fromHex(colourStr);
    return this;
  }

  public Drawable build() {
    Mesh mesh = new PolygonMeshData(points).create();
    Drawable drawable = new Drawable(pipeline, mesh);
    drawable.setPosition(position);
    drawable.setColour(colour);
    return drawable;
  }
}
