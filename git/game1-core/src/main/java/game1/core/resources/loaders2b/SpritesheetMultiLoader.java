package game1.core.resources.loaders2b;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import org.yaml.snakeyaml.Yaml;

import folkforms.fileutils.FileUtils;
import game1.core.engine.EngineInternal;
import game1.core.infrastructure.Texture;
import game1.core.resources.spritesheets.Spritesheet;
import game1.core.resources.spritesheets.SpritesheetTweaks;
import game1.datasets.MultiLoader;
import game1.filesystem.FileSystem;

public class SpritesheetMultiLoader extends MultiLoader {

  @Override
  public Map<String, Supplier<?>> load(String masterFile) throws IOException {
    Spritesheet spritesheet = createSpritesheet(masterFile);
    return spritesheet.createSuppliers();
  }

  protected Spritesheet createSpritesheet(String masterFile) throws IOException {
    List<String> masterFileContents = fileSystem.loadList(masterFile);

    SpritesheetTweaks tweaks = loadSpritesheetTweaks(masterFile, fileSystem);

    String path = masterFile.substring(0, masterFile.lastIndexOf('/'));
    List<Texture> textureList = new ArrayList<>();
    List<List<String>> metadataList = new ArrayList<>();
    Map<String, List<String>> fontData = new HashMap<>();
    for (int i = 0; i < masterFileContents.size(); i++) {
      String subFilename = masterFileContents.get(i);

      if (subFilename.endsWith(".fontdata")) {
        FileUtils fileUtils = new FileUtils();
        fontData.put(fileUtils.removeExtension(fileUtils.filenameOnly(subFilename)),
            fileSystem.loadList(subFilename));
      } else {
        String textureFile = String.format("%s/%s.png", path, subFilename);
        String metadataFile = String.format("%s/%s.txt", path, subFilename);
        Texture texture = EngineInternal.TEXTURE_LOADER.loadTexture(textureFile);
        List<String> metadata = fileSystem.loadList(metadataFile);
        textureList.add(texture);
        metadataList.add(metadata);
      }
    }
    Spritesheet spritesheet = new Spritesheet(textureList, metadataList, tweaks, fontData,
        masterFile);
    return spritesheet;
  }

  private SpritesheetTweaks loadSpritesheetTweaks(String masterFile, FileSystem fileSystem)
      throws IOException {
    String tweaksFile = masterFile + "-tweaks";
    if (fileSystem.exists(tweaksFile)) {
      Map<String, Object> yamlData = new Yaml().load(fileSystem.loadString(tweaksFile));
      return new SpritesheetTweaks(yamlData);
    }
    return null;
  }
}
