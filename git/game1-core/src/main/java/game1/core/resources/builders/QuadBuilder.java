package game1.core.resources.builders;

import org.joml.Vector3f;

import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.graphics.meshes.Mesh;
import game1.core.graphics.meshes.QuadMeshData;
import game1.core.pipelines.GraphicsPipelineName;
import game1.primitives.RectangleF;

public class QuadBuilder {

  private GraphicsPipelineName pipeline;
  private Colour colour = new Colour(1, 1, 1);
  private Vector3f p1, p2, p3, p4;
  private Vector3f position = new Vector3f(0, 0, 0);

  public static QuadBuilder init(GraphicsPipelineName pipeline) {
    QuadBuilder quadBuilder = new QuadBuilder();
    quadBuilder.pipeline = pipeline;
    return quadBuilder;
  }

  // Points are lower-case 'n'-shaped i.e. 0/1/2/3 or 0,0 -> 0,1 -> 1,1 -> 1,0
  public QuadBuilder points(Vector3f p1, Vector3f p2, Vector3f p3, Vector3f p4) {
    this.p1 = p1;
    this.p2 = p2;
    this.p3 = p3;
    this.p4 = p4;
    return this;
  }

  public QuadBuilder rectangle(RectangleF rectangle) {
    RectangleF r1 = rectangle.translateCopy(-rectangle.x, -rectangle.y, 0);
    p1 = new Vector3f(0, 0, 0);
    p2 = new Vector3f(0, r1.h, 0);
    p3 = new Vector3f(r1.w, r1.h, 0);
    p4 = new Vector3f(r1.w, 0, 0);
    position.x = rectangle.x;
    position.y = rectangle.y;
    return this;
  }

  public QuadBuilder position(Vector3f position) {
    this.position = position;
    return this;
  }

  public QuadBuilder z(float z) {
    position.z = z;
    return this;
  }

  public QuadBuilder colour(Colour colour) {
    this.colour = colour;
    return this;
  }

  public QuadBuilder colour(String colourStr) {
    this.colour = Colour.fromHex(colourStr);
    return this;
  }

  public Drawable build() {
    Mesh mesh = new QuadMeshData(p1, p2, p3, p4).create();
    Drawable drawable = new Drawable(pipeline, mesh);
    drawable.setPosition(position);
    drawable.setColour(colour);
    return drawable;
  }
}
