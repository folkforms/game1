package game1.core.pipelines;

import java.io.IOException;

import game1.core.infrastructure.lwjgl3.ShaderException;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3ShaderProgram;

public class Scene3DDepthShader extends LWJGL3ShaderProgram {

  public Scene3DDepthShader() throws IOException, ShaderException {
    super("game1.shaders.depth_vertex_shader", "game1.shaders.depth_fragment_shader");

    createUniform("isInstanced");
    createUniform("jointsMatrix");
    createUniform("modelLightViewNonInstancedMatrix");
    createUniform("orthoProjectionMatrix");
  }
}
