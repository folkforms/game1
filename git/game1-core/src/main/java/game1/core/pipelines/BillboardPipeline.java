package game1.core.pipelines;

import static org.lwjgl.opengl.GL11.glDepthMask;

import java.io.IOException;

import org.joml.Matrix4f;

import folkforms.log.Log;
import game1.core.graphics.meshes.InstancedMesh;
import game1.core.infrastructure.lwjgl3.ShaderException;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3ShaderProgram;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3Window;

public class BillboardPipeline extends GraphicsPipeline {

  private LWJGL3ShaderProgram billboardShaderProgram;

  public BillboardPipeline() throws IOException, ShaderException {
    super(GraphicsPipelineName.BILLBOARD, 10, 0);
    billboardShaderProgram = new BillboardShader();
  }

  @Override
  public void render(Matrix4f viewMatrix) {
    renderBillboards(viewMatrix);
  }

  private void renderBillboards(Matrix4f viewMatrix) {
    billboardShaderProgram.bind();

    billboardShaderProgram.setUniform("texture_sampler", 0);
    Matrix4f projectionMatrix = LWJGL3Window.getProjectionMatrix();
    billboardShaderProgram.setUniform("projectionMatrix", projectionMatrix);

    glDepthMask(false);

    for (int i = 0; i < instancedIndex; i++) {
      InstancedMesh instancedMesh = instanced[i];
      instancedMesh.renderBillboardListInstanced(transformation, viewMatrix, null);
    }

    glDepthMask(true);

    billboardShaderProgram.unbind();
  }

  @Override
  public void cleanup() {
    if (billboardShaderProgram != null) {
      billboardShaderProgram.cleanup();
    }
  }

  @Override
  public void debug_reloadShaders() {
    try {
      billboardShaderProgram = new BillboardShader();
    } catch (IOException | ShaderException ex) {
      Log.error("Failed to reload BillboardPipeline shader");
      Log.error(ex);
    }
  }
}
