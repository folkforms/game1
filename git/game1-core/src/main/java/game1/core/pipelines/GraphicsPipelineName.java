package game1.core.pipelines;

public enum GraphicsPipelineName {
  SCENE_3D, SCENE_2D, SKYBOX, BILLBOARD, WIREFRAME
}
