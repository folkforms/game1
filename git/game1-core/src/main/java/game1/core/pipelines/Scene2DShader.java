package game1.core.pipelines;

import java.io.IOException;

import game1.core.infrastructure.lwjgl3.ShaderException;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3ShaderProgram;

public class Scene2DShader extends LWJGL3ShaderProgram {

  public Scene2DShader() throws ShaderException, IOException {
    super("game1.shaders.scene_2d_vertex_shader", "game1.shaders.scene_2d_fragment_shader");

    // Create uniforms for orthographic-model projection matrix and base colour
    createUniform("projModelMatrix");
    createUniform("colour");
    createUniform("hasTexture");
    createUniform("sprite");
    createStencilUniform();
  }
}
