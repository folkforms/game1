package game1.core.pipelines;

import java.util.Comparator;

/**
 * Comparator for sorting arrays which pushes all null items to the right. Non-null items are
 * considered equal.
 */
class ArrayWithNullsLastComparator<T> implements Comparator<T> {

  @Override
  public int compare(T a, T b) {
    return Comparator.nullsLast(new Comparator<T>() {
      @Override
      public int compare(T a, T b) {
        return 0;
      }
    }).compare(a, b);
  }
}
