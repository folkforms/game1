package game1.core.pipelines;

import java.util.Arrays;
import java.util.List;

import org.joml.Matrix4f;

import game1.core.engine.internal.Transformation;
import game1.core.graphics.Drawable;
import game1.core.graphics.meshes.InstancedMesh;
import game1.core.graphics.meshes.Mesh;
import game1.debug.dump_data.DebugDataProvider;
import game1.debug.dump_data.DebugDataProviderRegistry;

abstract public class GraphicsPipeline implements DebugDataProvider {

  /**
   * Shared transformation object to be used by pipelines.
   */
  protected final Transformation transformation = new Transformation();

  protected boolean debug_dataChanged = false;
  protected String debug_data;
  public static boolean debug_shaderModified = false;
  public static int debug_testMode = 0;
  public static String debug_testModeName = "Off";

  protected GraphicsPipelineName name;
  protected InstancedMesh[] instanced;
  protected int instancedIndex = 0;
  protected Mesh[] nonInstanced;
  protected int nonInstancedIndex = 0;
  protected boolean garbageCollectionRequired = false;
  private int dataDumpCount = 0;

  public GraphicsPipeline(GraphicsPipelineName name, int instancedSize, int nonInstancedSize) {
    this.name = name;
    instanced = new InstancedMesh[instancedSize];
    nonInstanced = new Mesh[nonInstancedSize];
    DebugDataProviderRegistry.register(this, DebugDataProviderRegistry.Context.FILE);
  }

  public GraphicsPipelineName getName() {
    return name;
  }

  /**
   * Adds the given {@link Drawable} and its meshes to the scene.
   *
   * @param drawable
   *          the Drawable to add to the scene
   */
  public void add(Drawable drawable) {
    Mesh[] meshes = drawable.internal_getMeshes();
    for (Mesh mesh : meshes) {
      int foundInstancedIndex = -1;
      int foundNonInstancedIndex = -1;
      for (int i = 0; i < instancedIndex; i++) {
        if (instanced[i] == mesh) {
          foundInstancedIndex = i;
          break;
        }
      }
      for (int i = 0; i < nonInstancedIndex; i++) {
        if (nonInstanced[i] == mesh) {
          foundNonInstancedIndex = i;
          break;
        }
      }

      if (foundInstancedIndex == -1 && mesh instanceof InstancedMesh) {
        instanced[instancedIndex++] = (InstancedMesh) mesh;
      }
      if (foundNonInstancedIndex == -1 && mesh instanceof InstancedMesh == false) {
        nonInstanced[nonInstancedIndex++] = mesh;
      }
      mesh.link(drawable);
    }

    debug_dataChanged = true;
  }

  /**
   * Removes the given {@link Drawable} from the scene. If its {@link Mesh}es are not linked to any
   * other {@link Drawable}s they will be removed as well.
   *
   * @param drawable
   *          the Drawable to flag for removal
   */
  public void remove(Drawable drawable) {
    Mesh[] meshes = drawable.internal_getMeshes();
    for (Mesh mesh : meshes) {
      mesh.unlink(drawable);
      if (mesh.listDrawables().size() == 0) {
        garbageCollectionRequired = true;
      }
    }
    debug_dataChanged = true;
  }

  /**
   * Garbage collect any items that have been flagged for removal. This method must be called from
   * the {@link #render()} method in order to avoid concurrent modification exceptions. It does not
   * have to be called every time, although it can be.
   *
   * @see {@link #remove(Drawable)}
   */
  public void garbageCollect() {
    if (garbageCollectionRequired) {
      instancedIndex = removeUnlinkedMeshes(instanced, instancedIndex);
      nonInstancedIndex = removeUnlinkedMeshes(nonInstanced, nonInstancedIndex);
      garbageCollectionRequired = false;
    }
  }

  /**
   * Removes any unlinked meshes from the given array. Sets them to <code>null</code> then sorts the
   * array with nulls last.
   *
   * @param meshes
   *          array of meshes to clean
   * @param maxIndex
   *          the max index to search in the array
   * @return new max index value
   */
  private int removeUnlinkedMeshes(Mesh[] meshes, int maxIndex) {
    for (int i = maxIndex - 1; i >= 0; i--) {
      if (meshes[i].listDrawables().size() == 0) {
        meshes[i] = null;
        maxIndex--;
      }
    }
    Arrays.sort(meshes, new ArrayWithNullsLastComparator<Mesh>());
    return maxIndex;
  }

  @Override
  public String debug_getDumpFilename() {
    return String.format("dump_pipeline_%s.txt", name);
  }

  @Override
  public List<String> debug_listData() {
    return PipelineDebugUtils.dumpData(name, instanced, instancedIndex, nonInstanced,
        nonInstancedIndex);
  }

  public String debug_getOnScreenData() {
    if (debug_dataChanged) {
      debug_dataChanged = false;
      debug_data = String.format("  %s: %s, %s", name, instancedIndex, nonInstancedIndex);
    }
    return debug_data;
  }

  abstract public void render(Matrix4f viewMatrix);

  abstract public void cleanup();

  /**
   * Subclasses can override this method to clear any light uniforms (or other lighting data) when
   * the scene lights change.
   */
  public void clearLightUniforms() {
  }

  /**
   * Reloads all shaders used by this pipeline. If the shaders fail to load we should continue to
   * use the previous versions.
   */
  abstract public void debug_reloadShaders();
}
