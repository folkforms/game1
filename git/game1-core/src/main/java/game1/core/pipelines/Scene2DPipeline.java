package game1.core.pipelines;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.joml.Matrix4f;
import org.joml.Vector4f;

import folkforms.log.Log;
import game1.core.engine.Engine;
import game1.core.graphics.Drawable;
import game1.core.graphics.meshes.Mesh;
import game1.core.infrastructure.lwjgl3.ShaderException;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3ShaderProgram;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3Window;
import game1.core.registries.DrawableComparator;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;

public class Scene2DPipeline extends GraphicsPipeline {

  private static final int MAX_INSTANCED = 5000;

  private LWJGL3ShaderProgram scene2DShaderProgram;
  private Drawable[] drawables = new Drawable[MAX_INSTANCED];
  private int drawablesIndex = 0;

  public Scene2DPipeline() throws IOException, ShaderException {
    super(GraphicsPipelineName.SCENE_2D, MAX_INSTANCED, 0);
    scene2DShaderProgram = new Scene2DShader();
  }

  @Override
  public void add(Drawable d) {
    super.add(d);
  }

  @Override
  public void remove(Drawable drawable) {
    super.remove(drawable);
  }

  private void createDrawablesArray() {
    Set<Drawable> seen = new HashSet<>();
    drawables = new Drawable[drawables.length];
    drawablesIndex = 0;
    for (int i = 0; i < instancedIndex; i++) {
      Mesh mesh = instanced[i];
      List<Drawable> list = mesh.listDrawables();
      for (Drawable d : list) {
        if (!seen.contains(d)) {
          drawables[drawablesIndex++] = d;
          seen.add(d);
        }
      }
    }
    Arrays.sort(drawables, 0, drawablesIndex, new DrawableComparator());
  }

  @Override
  public void render(Matrix4f viewMatrix) {
    createDrawablesArray();
    renderScene2D();
  }

  // Although 2D rendering will respect z-index of the drawables, we still need to render
  // back-to-front so that transparent textures can blend with the textures behind them.
  private void renderScene2D() {
    scene2DShaderProgram.bind();
    Matrix4f ortho = transformation.getOrtho2DProjectionMatrix(0, LWJGL3Window.getWidth(), 0,
        LWJGL3Window.getHeight());

    for (int i = 0; i < drawablesIndex; i++) {
      Drawable drawable = drawables[i];
      if (drawable.isVisible() == false) {
        continue;
      }

      scene2DShaderProgram.setUniform("colour", drawable.getColour());

      Stencil2d stencil = drawable.getStencil();
      if (stencil != null) {
        Vector4f stencilArea = new Vector4f(stencil.area);
        ResolutionHints hints = ResolutionLayersFactory.getInstance().getLayerForActor(drawable)
            .getResolutionHints();
        float resolutionOffsetX = hints.getResolutionOffsetX();
        float resolutionOffsetY = hints.getResolutionOffsetY();
        float scale = hints.getResolutionScale();
        stencilArea.x = stencilArea.x * scale + resolutionOffsetX;
        stencilArea.y = stencilArea.y * scale + resolutionOffsetY;
        stencilArea.z = stencilArea.z * scale;
        stencilArea.w = stencilArea.w * scale;
        scene2DShaderProgram.setUniform("stencil.area", stencilArea);
        scene2DShaderProgram.setUniform("stencil.type", stencil.type);
        boolean debugStencils = Engine.PROPERTIES
            .getBooleanProperty("game1.dev_mode.show_stencils");
        scene2DShaderProgram.setUniform("stencil.colour",
            debugStencils ? Stencil2d.DEBUG_COLOUR : stencil.colour);
      } else {
        scene2DShaderProgram.setUniform("stencil.type", 0);
      }

      Mesh[] meshes = drawable.internal_getMeshes();
      for (int j = 0; j < meshes.length; j++) {
        Mesh mesh = meshes[j];
        Matrix4f projModelMatrix = transformation.buildOrthoProjModelMatrix(drawable, ortho);
        scene2DShaderProgram.setUniform("projModelMatrix", projModelMatrix);
        scene2DShaderProgram.setUniform("hasTexture",
            mesh.getMaterial().getTexture() != null ? 1 : 0);
        scene2DShaderProgram.setUniform("sprite", mesh.getMaterial().getSprite());
        mesh.render(GraphicsPipelineName.SCENE_2D);
      }
    }
    scene2DShaderProgram.unbind();
  }

  @Override
  public void cleanup() {
    if (scene2DShaderProgram != null) {
      scene2DShaderProgram.cleanup();
    }
  }

  @Override
  public String debug_getOnScreenData() {
    if (debug_dataChanged) {
      debug_dataChanged = false;
      debug_data = String.format("  %s: %s, %s, %s", name, instancedIndex, nonInstancedIndex,
          drawablesIndex);
    }
    return debug_data;
  }

  @Override
  public void debug_reloadShaders() {
    try {
      scene2DShaderProgram = new Scene2DShader();
    } catch (IOException | ShaderException ex) {
      Log.error("Failed to reload Scene2DPipeline shader");
      Log.error(ex);
    }
  }

  @Override
  public List<String> debug_listData() {
    return Scene2DPipelineDebug.debug_listData(name, instanced, instancedIndex, nonInstanced,
        nonInstancedIndex, drawables, drawablesIndex);
  }
}
