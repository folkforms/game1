package game1.core.pipelines;

import java.io.IOException;

import game1.core.infrastructure.lwjgl3.ShaderException;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3ShaderProgram;

public class WireframeShader extends LWJGL3ShaderProgram {

  public WireframeShader() throws ShaderException, IOException {
    super("game1.shaders.wireframe_vertex_shader", "game1.shaders.wireframe_fragment_shader",
        "game1.shaders.wireframe_geometry_shader");

    createUniform("projectionMatrix");
  }
}
