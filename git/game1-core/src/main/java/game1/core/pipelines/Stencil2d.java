package game1.core.pipelines;

import org.joml.Vector4f;

import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;

/**
 * A stencil can be used to mask certain areas of the screen from drawing, either to only draw
 * {@link Drawable}s where they are overlap the stencil area or to only draw them when they are
 * outside the stencil area.
 *
 * @see {@link Drawable#setStencil(Stencil2d)}
 */
public class Stencil2d {

  /**
   * DEBUG_COLOUR used when property `game1.dev_mode.show_stencils` is true.
   */
  public static Colour DEBUG_COLOUR = new Colour(1, 1, 1, 0.3f);

  /**
   * Pixels outside the stencil area will be multiplied by the stencil colour.
   */
  public static final int OUTSIDE = 1;

  /**
   * Pixels inside the stencil area will be multiplied by the stencil colour.
   */
  public static final int INSIDE = 2;

  /**
   * The stencil type, either {@link #OUTSIDE} or {@link #INSIDE}.
   */
  public int type;

  /**
   * Stencil shape (x,y,w,h).
   */
  public Vector4f area;

  /**
   * Stencil colour.
   */
  public Vector4f colour;

  /**
   * Creates a stencil with the given values.
   *
   * @param type
   *          stencil type, either {@link #OUTSIDE} or {@link #INSIDE}.
   * @param area
   *          stencil area
   * @param colour
   *          stencil colour to multiply affected pixels by
   */
  public Stencil2d(int type, Vector4f area, Vector4f colour) {
    this.type = type;
    this.area = area;
    this.colour = colour;
  }

  /**
   * Creates a stencil that only shows pixels inside the given area. Everything outside will be
   * removed.
   *
   * <p>
   * This is equivalent to
   * {@code new Stencil2d(OUTSIDE, new Vector4f(x,y,w,h), new Vector4f(0,0,0,0))}.
   * </p>
   *
   * @param x
   *          stencil x coordinate
   * @param y
   *          stencil y coordinate
   * @param w
   *          stencil width
   * @param h
   *          stencil height
   */
  public Stencil2d(float x, float y, float w, float h) {
    this(OUTSIDE, new Vector4f(x, y, w, h), new Vector4f(0, 0, 0, 0));
  }

  @Override
  public String toString() {
    return String.format("Stencil[area=%s,type=%s,colour=%s]", area, type, colour);
  }
}
