package game1.core.pipelines;

import static org.lwjgl.opengl.GL11.GL_DEPTH_COMPONENT;
import static org.lwjgl.opengl.GL11.GL_NONE;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_S;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_T;
import static org.lwjgl.opengl.GL11.glDrawBuffer;
import static org.lwjgl.opengl.GL11.glReadBuffer;
import static org.lwjgl.opengl.GL11.glTexParameteri;
import static org.lwjgl.opengl.GL13.GL_CLAMP_TO_BORDER;
import static org.lwjgl.opengl.GL30.GL_DEPTH_ATTACHMENT;
import static org.lwjgl.opengl.GL30.GL_FRAMEBUFFER;
import static org.lwjgl.opengl.GL30.GL_FRAMEBUFFER_COMPLETE;
import static org.lwjgl.opengl.GL30.glBindFramebuffer;
import static org.lwjgl.opengl.GL30.glCheckFramebufferStatus;
import static org.lwjgl.opengl.GL30.glDeleteFramebuffers;
import static org.lwjgl.opengl.GL30.glFramebufferTexture2D;
import static org.lwjgl.opengl.GL30.glGenFramebuffers;

import game1.core.engine.Engine;
import game1.core.engine.EngineInternal;
import game1.core.infrastructure.Texture;
import game1.core.infrastructure.lwjgl3.ShaderException;
import game1.resolutions.layers.ResolutionLayers;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;

public class ShadowMap {
  private int[] SHADOW_MAP_WIDTHS;
  private int[] SHADOW_MAP_HEIGHTS;
  public static int SHADOW_MAP_WIDTH;
  public static int SHADOW_MAP_HEIGHT;
  private int depthMapFBO;
  private Texture depthMap;
  private int shadowQuality;

  public ShadowMap() throws ShaderException {
    populateShadowMapSizes();

    shadowQuality = Engine.PROPERTIES.getIntProperty("game1.shadow_quality");
    if (shadowQuality > 0) {
      SHADOW_MAP_WIDTH = SHADOW_MAP_WIDTHS[shadowQuality - 1];
      SHADOW_MAP_HEIGHT = SHADOW_MAP_HEIGHTS[shadowQuality - 1];

      // Create a FBO to render the depth map
      depthMapFBO = glGenFramebuffers();
      // Create the depth map texture
      depthMap = EngineInternal.TEXTURE_LOADER.createBlankTexture(SHADOW_MAP_WIDTH,
          SHADOW_MAP_HEIGHT, GL_DEPTH_COMPONENT);
      // Attach the the depth map texture to the FBO
      glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
      glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
          depthMap.getTextureId(), 0);
      // Set only depth
      glDrawBuffer(GL_NONE);
      glReadBuffer(GL_NONE);
      // Use clamp to border otherwise shadows at the edge of directional light frustum will stretch
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

      if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        throw new ShaderException("ShadowMap: Could not create Framebuffer.");
      }
      // Unbind
      glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
  }

  private void populateShadowMapSizes() {
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(ResolutionLayers.WINDOW_LAYER).getResolutionHints();
    int width = hints.getWindowWidth();
    int height = hints.getWindowHeight();
    SHADOW_MAP_WIDTHS = new int[] { //
        width / 16, //
        width / 8, //
        width / 4, //
        width / 2, //
        width //
    };
    SHADOW_MAP_HEIGHTS = new int[] { //
        height / 16, //
        height / 8, //
        height / 4, //
        height / 2, //
        height //
    };
  }

  public Texture getDepthMapTexture() {
    return depthMap;
  }

  public int getDepthMapFBO() {
    return depthMapFBO;
  }

  public int getShadowQuality() {
    return shadowQuality;
  }

  public void cleanup() {
    glDeleteFramebuffers(depthMapFBO);
    // Log.temp("ShadowMap: cleanup method calling Texture.cleanup. Do we need to clean things
    // up?");
    // depthMap.cleanup();
  }
}