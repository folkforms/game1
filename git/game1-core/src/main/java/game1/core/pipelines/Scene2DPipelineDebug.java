package game1.core.pipelines;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.joml.Vector3f;

import game1.core.graphics.Drawable;
import game1.core.graphics.meshes.InstancedMesh;
import game1.core.graphics.meshes.Mesh;
import game1.primitives.RectangleF;

public class Scene2DPipelineDebug {

  public static List<String> debug_listData(GraphicsPipelineName name, InstancedMesh[] instanced,
      int instancedIndex, Mesh[] nonInstanced, int nonInstancedIndex, Drawable[] drawables,
      int drawablesIndex) {
    List<String> output = new ArrayList<>();
    output.addAll(PipelineDebugUtils.dumpData(name, instanced, instancedIndex, nonInstanced,
        nonInstancedIndex));
    output.addAll(debug_listDrawablesByZIndex(drawables, drawablesIndex));
    output.addAll(debug_listRenderingCollisions(drawables, drawablesIndex));
    return output;
  }

  private static List<String> debug_listDrawablesByZIndex(Drawable[] drawables,
      int drawablesIndex) {
    List<String> data = new ArrayList<>();
    data.add("## Drawables by z-index");
    data.add("");
    for (int i = 0; i < drawablesIndex; i++) {
      Drawable drawable = drawables[i];
      data.add(String.format("%s: %s", (int) drawable.getPosition().z, drawable));
    }
    data.add("");
    return data;
  }

  private static List<String> debug_listRenderingCollisions(Drawable[] drawables,
      int drawablesIndex) {
    Map<Float, List<Drawable>> map = new HashMap<>();
    for (int i = 0; i < drawablesIndex; i++) {
      Drawable drawable = drawables[i];
      float z = drawable.getPosition().z;
      List<Drawable> drawblesForZIndex = map.getOrDefault(z, new ArrayList<>());
      drawblesForZIndex.add(drawable);
      map.put(z, drawblesForZIndex);
    }

    Map<Drawable, List<Drawable>> collisions = new LinkedHashMap<>();

    map.entrySet().forEach(e -> {
      List<Drawable> list = e.getValue();
      for (int i = 0; i < list.size(); i++) {
        Drawable d1 = list.get(i);
        if (!d1.isVisible()) {
          continue;
        }
        Vector3f pos1 = d1.getPosition();
        RectangleF shape1 = new RectangleF(pos1.x, pos1.y, d1.getWidth(), d1.getHeight());
        for (int j = i + 1; j < list.size(); j++) {
          Drawable d2 = list.get(j);
          if (!d2.isVisible()) {
            continue;
          }
          Vector3f pos2 = d2.getPosition();
          RectangleF shape2 = new RectangleF(pos2.x, pos2.y, d2.getWidth(), d2.getHeight());
          if (shape2.intersects(shape1)) {
            List<Drawable> collisionList = collisions.getOrDefault(d1, new ArrayList<>());
            collisionList.add(d2);
            collisions.put(d1, collisionList);
          }
        }
      }
    });

    List<String> data = new ArrayList<>();
    data.add("## Rendering collisions");
    data.add("");
    data.add(
        "Rendering collisions happen when two items share the same space on screen and have the same z-index.");
    data.add(
        "One of them will effectively not be rendered at all, regardless of whether the other one is transparent or not.");
    data.add("");
    if (collisions.size() > 0) {
      collisions.entrySet().forEach(e -> {
        data.add(e.getKey().toString());
        e.getValue().forEach(v -> data.add(String.format("    => %s", v)));
      });
    } else {
      data.add("<No rendering collisions found>");
    }
    data.add("");
    return data;
  }
}
