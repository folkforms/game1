package game1.core.pipelines;

import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glViewport;
import static org.lwjgl.opengl.GL13.GL_TEXTURE2;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL30.GL_FRAMEBUFFER;
import static org.lwjgl.opengl.GL30.glBindFramebuffer;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.joml.Vector4f;

import folkforms.log.Log;
import game1.core.engine.Engine;
import game1.core.engine.internal.CameraFactory;
import game1.core.graphics.Drawable;
import game1.core.graphics.lights.DirectionalLight;
import game1.core.graphics.lights.PointLight;
import game1.core.graphics.lights.Spotlight;
import game1.core.graphics.meshes.InstancedMesh;
import game1.core.graphics.meshes.Mesh;
import game1.core.infrastructure.lwjgl3.ShaderException;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3ShaderProgram;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3Window;
import game1.core.registries.Lights;

// FIXME PIPELINES: FrustumCullingFilter needs to be updated to use this pipeline and not MeshTable
public class Scene3DPipeline extends GraphicsPipeline {

  private ShadowMap shadowMap;
  private LWJGL3ShaderProgram depthShaderProgram;
  private LWJGL3ShaderProgram scene3DShaderProgram;
  // FIXME MISC_STUFF: What is specularPower?
  private float specularPower;
  private boolean frustumCullingEnabled;
  private final FrustumCullingFilter frustumFilter;
  private Drawable[] drawablesInScene;
  private int drawablesInSceneIndex = 0;
  private boolean sceneChanged = false;
  private CameraBoxSelectionDetector detector;

  public static Map<Integer, String> TEST_MODES = new HashMap<>();
  static {
    TEST_MODES.put(0, "Off");
    TEST_MODES.put(1, "No textures");
    TEST_MODES.put(2, "Show normals");
    TEST_MODES.put(3, "Grayscale");
  }

  // FIXME PIPELINES: I will need a property to set these array sizes so that you know the number
  // and can set it later. Maybe recreate the array with a warning when it goes over.
  public Scene3DPipeline() throws IOException, ShaderException {
    super(GraphicsPipelineName.SCENE_3D, 11000, 100);
    shadowMap = new ShadowMap();
    depthShaderProgram = new Scene3DDepthShader();
    scene3DShaderProgram = new Scene3DShader();
    specularPower = 10f;
    frustumFilter = new FrustumCullingFilter(this);
    drawablesInScene = new Drawable[11000];
    detector = new CameraBoxSelectionDetector(this);
    CameraFactory.getInstance().setSelectionDetector(detector);
    frustumCullingEnabled = Engine.PROPERTIES.getBooleanProperty("game1.enable_frustum_culling");
  }

  @Override
  public void add(Drawable d) {
    super.add(d);
    sceneChanged = true;
  }

  @Override
  public void remove(Drawable d) {
    super.remove(d);
    sceneChanged = true;
  }

  /**
   * Creates an array of all Drawables in the scene from the Meshes in the scene. This list is used
   * for frustum culling and camera selection.
   */
  // FIXME PIPELINES: I should frustum cull non-instanced meshes as well
  // FIXME PIPELINES: The code below is wrong - we won't know the array length because each mesh
  // could have multiple drawables, so we need to keep track of drawables similar to Scene2DPipeline
  private void createDrawablesInSceneArray() {
    if (sceneChanged) {
      drawablesInScene = new Drawable[drawablesInScene.length];
      drawablesInSceneIndex = 0;
      for (int i = 0; i < instancedIndex; i++) {
        Mesh mesh = instanced[i];
        List<Drawable> list = mesh.listDrawables();
        for (Drawable d : list) {
          drawablesInScene[drawablesInSceneIndex++] = d;
        }
      }
      sceneChanged = false;
    }
  }

  /**
   * Clear the frustum culling filter right before rendering the depth map so that off-screen items
   * still cast shadows.
   */
  private void clearFrustumCulling() {
    if (frustumCullingEnabled) {
      frustumFilter.reset();
    }
  }

  private void applyFrustumCulling() {
    if (frustumCullingEnabled) {
      frustumFilter.updateFrustum();
      frustumFilter.filter();
    }
  }

  Drawable[] listDrawablesInScene() {
    return drawablesInScene;
  }

  int countDrawablesInScene() {
    return drawablesInSceneIndex;
  }

  @Override
  public void render(Matrix4f viewMatrix) {
    clearFrustumCulling();
    createDrawablesInSceneArray();
    renderDepthMap();
    applyFrustumCulling();
    renderScene3D(viewMatrix);
  }

  private void renderDepthMap() {
    if (shadowMap.getShadowQuality() > 0) {
      DirectionalLight light = Lights.getDirectionalLight();
      if (light != null) {
        // Setup viewport to match the texture size
        glBindFramebuffer(GL_FRAMEBUFFER, shadowMap.getDepthMapFBO());
        glViewport(0, 0, ShadowMap.SHADOW_MAP_WIDTH, ShadowMap.SHADOW_MAP_HEIGHT);
        glClear(GL_DEPTH_BUFFER_BIT);

        depthShaderProgram.bind();

        Vector3f lightDirection = light.getDirection();
        float lightAngleX = (float) Math.toDegrees(Math.acos(lightDirection.z));
        float lightAngleY = (float) Math.toDegrees(Math.asin(lightDirection.x));
        float lightAngleZ = 0;
        Matrix4f lightViewMatrix = transformation.updateLightViewMatrix(
            new Vector3f(lightDirection).mul(light.getLightSourceDistance()),
            new Vector3f(lightAngleX, lightAngleY, lightAngleZ));
        DirectionalLight.OrthoCoords orthCoords = light.getOrthoCoords();
        Matrix4f orthoProjMatrix = transformation.updateOrthoProjectionMatrix(orthCoords.left,
            orthCoords.right, orthCoords.bottom, orthCoords.top, orthCoords.near, orthCoords.far);
        depthShaderProgram.setUniform("orthoProjectionMatrix", orthoProjMatrix);

        renderNonInstancedMeshes(depthShaderProgram, null, lightViewMatrix);
        renderInstancedMeshes(depthShaderProgram, null, lightViewMatrix);

        // Unbind
        depthShaderProgram.unbind();
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        // Reset viewport to match the window size
        glViewport(0, 0, LWJGL3Window.getWidth(), LWJGL3Window.getHeight());
      }
    }
  }

  private void renderScene3D(Matrix4f viewMatrix) {
    scene3DShaderProgram.bind();

    Matrix4f projectionMatrix = LWJGL3Window.getProjectionMatrix();
    scene3DShaderProgram.setUniform("projectionMatrix", projectionMatrix);
    Matrix4f orthoProjMatrix = transformation.getOrthoProjectionMatrix();
    scene3DShaderProgram.setUniform("orthoProjectionMatrix", orthoProjMatrix);
    Matrix4f lightViewMatrix = transformation.getLightViewMatrix();

    setLightUniforms(viewMatrix);

    scene3DShaderProgram.setUniform("fog", Engine.FOG);
    scene3DShaderProgram.setUniform("texture_sampler", 0);
    scene3DShaderProgram.setUniform("normalMap", 1);
    scene3DShaderProgram.setUniform("shadowMap", 2);
    scene3DShaderProgram.setUniform("renderShadow", shadowMap.getShadowQuality() > 0 ? 1 : 0);
    scene3DShaderProgram.setUniform("testMode", GraphicsPipeline.debug_testMode);
    renderNonInstancedMeshes(scene3DShaderProgram, viewMatrix, lightViewMatrix);
    renderInstancedMeshes(scene3DShaderProgram, viewMatrix, lightViewMatrix);
    scene3DShaderProgram.unbind();
  }

  private void renderNonInstancedMeshes(LWJGL3ShaderProgram shader, Matrix4f viewMatrix,
      Matrix4f lightViewMatrix) {
    shader.setUniform("isInstanced", 0);

    // Non-instanced meshes
    int currentNumNonInstanced = nonInstancedIndex;
    for (int i = 0; i < currentNumNonInstanced; i++) {
      Mesh mesh = nonInstanced[i];
      // Sort out viewMatrix
      if (viewMatrix != null) {
        shader.setUniform("material", mesh.getMaterial());
        if (shadowMap.getShadowQuality() > 0) {
          glActiveTexture(GL_TEXTURE2);
          glBindTexture(GL_TEXTURE_2D, shadowMap.getDepthMapTexture().getTextureId());
        }
      }

      mesh.renderList(shader, scene3DShaderProgram, transformation, viewMatrix, lightViewMatrix);
    }
  }

  private void renderInstancedMeshes(LWJGL3ShaderProgram shader, Matrix4f viewMatrix,
      Matrix4f lightViewMatrix) {
    shader.setUniform("isInstanced", 1);

    // Instanced meshes
    int currentNumInstanced = instancedIndex;
    for (int i = 0; i < currentNumInstanced; i++) {
      InstancedMesh instancedMesh = instanced[i];
      // Sort out viewMatrix
      if (viewMatrix != null) {
        shader.setUniform("material", instancedMesh.getMaterial());
        if (shadowMap.getShadowQuality() > 0) {
          glActiveTexture(GL_TEXTURE2);
          glBindTexture(GL_TEXTURE_2D, shadowMap.getDepthMapTexture().getTextureId());
        }
      }

      instancedMesh.renderListInstanced(transformation, viewMatrix, lightViewMatrix);
    }
  }

  private void setLightUniforms(Matrix4f viewMatrix) {
    scene3DShaderProgram.setUniform("ambientLight", Lights.getAmbientLightColor());
    scene3DShaderProgram.setUniform("specularPower", specularPower);

    // Process point lights
    PointLight[] pointLightList = Lights.getPointLights();
    for (int i = 0; i < pointLightList.length; i++) {
      // Copy the point light and transform its position to view coordinates
      PointLight currPointLight = new PointLight(pointLightList[i]);
      Vector3f lightPos = currPointLight.getPosition();
      Vector4f aux = new Vector4f(lightPos, 1);
      aux.mul(viewMatrix);
      lightPos.x = aux.x;
      lightPos.y = aux.y;
      lightPos.z = aux.z;
      scene3DShaderProgram.setUniform("pointLights", currPointLight, i);
    }

    // Process spotlights
    Spotlight[] spotlightList = Lights.getSpotlights();
    for (int i = 0; i < spotlightList.length; i++) {
      // Copy the spotlight and transform its position and cone direction to view coordinates
      Spotlight currSpotlight = new Spotlight(spotlightList[i]);
      Vector4f dir = new Vector4f(currSpotlight.getConeDirection(), 0);
      dir.mul(viewMatrix);
      currSpotlight.setConeDirection(new Vector3f(dir.x, dir.y, dir.z));

      Vector3f lightPos = currSpotlight.getPointLight().getPosition();
      Vector4f aux = new Vector4f(lightPos, 1);
      aux.mul(viewMatrix);
      lightPos.x = aux.x;
      lightPos.y = aux.y;
      lightPos.z = aux.z;

      scene3DShaderProgram.setUniform("spotlights", currSpotlight, i);
    }

    // Get a copy of the directional light object and transform its position to view coordinates
    DirectionalLight directionalLight = Lights.getDirectionalLight();
    if (directionalLight != null) {
      DirectionalLight currDirLight = new DirectionalLight(directionalLight);
      Vector4f dir = new Vector4f(currDirLight.getDirection(), 0);
      dir.mul(viewMatrix);
      currDirLight.setDirection(new Vector3f(dir.x, dir.y, dir.z));
      scene3DShaderProgram.setUniform("directionalLight", currDirLight);
    }
  }

  @Override
  public void clearLightUniforms() {
    scene3DShaderProgram.clearLightUniforms();
  }

  @Override
  public void cleanup() {
    if (shadowMap != null) {
      shadowMap.cleanup();
    }
    if (depthShaderProgram != null) {
      depthShaderProgram.cleanup();
    }
    if (scene3DShaderProgram != null) {
      scene3DShaderProgram.cleanup();
    }
  }

  @Override
  public void debug_reloadShaders() {
    try {
      Scene3DDepthShader newDepthShaderProgram = new Scene3DDepthShader();
      Scene3DShader newScene3DShaderProgram = new Scene3DShader();
      depthShaderProgram = newDepthShaderProgram;
      scene3DShaderProgram = newScene3DShaderProgram;
    } catch (IOException | ShaderException ex) {
      Log.error("Failed to reload Scene3DPipeline shader");
      Log.error(ex);
    }
  }
}
