package game1.core.pipelines;

import java.io.IOException;

import game1.core.infrastructure.lwjgl3.ShaderException;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3ShaderProgram;
import game1.core.registries.Lights;

public class Scene3DShader extends LWJGL3ShaderProgram {

  public Scene3DShader() throws ShaderException, IOException {
    super("game1.shaders.scene_3d_vertex_shader", "game1.shaders.scene_3d_fragment_shader");

    // Create uniforms for modelView and projection matrices and texture
    createUniform("projectionMatrix");
    createUniform("modelViewNonInstancedMatrix");
    createUniform("texture_sampler");
    createUniform("normalMap");

    // Create uniform for material
    createMaterialUniform("material");

    // Create lighting related uniforms
    createUniform("specularPower");
    createUniform("ambientLight");
    createPointLightListUniform("pointLights", Lights.MAX_POINT_LIGHTS);
    createSpotlightListUniform("spotlights", Lights.MAX_SPOTLIGHTS);
    createDirectionalLightUniform("directionalLight");
    createFogUniform("fog");

    // Create uniforms for shadow mapping
    createUniform("shadowMap");
    createUniform("orthoProjectionMatrix");
    createUniform("modelLightViewNonInstancedMatrix");
    createUniform("renderShadow");

    // Create uniform for joint matrices
    createUniform("jointsMatrix");

    // Create uniform for whether we are rendering an instanced mesh or not
    createUniform("isInstanced");

    // Create uniform for whether the object is selected or not
    createUniform("selectedNonInstanced");

    // Enable test mode so certain test functions inside the shader will be run
    createUniform("testMode");
  }
}
