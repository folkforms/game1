package game1.core.pipelines;

import java.util.ArrayList;
import java.util.List;

import game1.core.graphics.Drawable;
import game1.core.graphics.meshes.Mesh;

public class PipelineDebugUtils {

  public static List<String> dumpData(GraphicsPipelineName name, Mesh[] instanced,
      int instancedIndex, Mesh[] nonInstanced, int nonInstancedIndex) {
    List<String> data = new ArrayList<>();
    getHeader(name.name(), data);
    getData(instanced, instancedIndex, nonInstanced, nonInstancedIndex, data);
    return data;
    // Engine.DUMP_DATA_REGISTRY.debug_dumpDataToFile(filename, data);
  }

  // public static List<String> dumpDataScene2D(PipelineName name, Mesh[] instanced,
  // int instancedIndex, Mesh[] nonInstanced, int nonInstancedIndex, Drawable[] drawables,
  // int drawablesIndex) {
  // List<String> data = new ArrayList<>();
  // getHeader(name.name(), data);
  // getDataScene2D(drawables, drawablesIndex, data);
  // getData(instanced, instancedIndex, nonInstanced, nonInstancedIndex, data);
  // return data;
  // // Engine.DUMP_DATA_REGISTRY.debug_dumpDataToFile(filename, data);
  // }

  private static void getHeader(String name, List<String> data) {
    data.add(String.format("# %s", name));
    data.add("");
  }

  // private static void getDataScene2D(Drawable[] drawables, int drawablesIndex, List<String> data)
  // {
  // data.add("## Drawables");
  // data.add("");
  // for (int i = 0; i < drawablesIndex; i++) {
  // data.add(drawables[i].toString());
  // Mesh[] meshes = drawables[i].internal_getMeshes();
  // for (Mesh mesh : meshes) {
  // data.add(String.format(" %s", mesh));
  // }
  // }
  // data.add("");
  // }

  private static void getData(Mesh[] instanced, int instancedIndex, Mesh[] nonInstanced,
      int nonInstancedIndex, List<String> data) {
    // instanced
    data.add("## Instanced");
    data.add("");
    for (int i = 0; i < instancedIndex; i++) {
      data.add(instanced[i].toString());
      List<Drawable> drawables = instanced[i].listDrawables();
      for (Drawable d : drawables) {
        data.add(String.format("        %s", d));
      }
    }
    data.add("");

    // instanced
    data.add("## Non-Instanced");
    data.add("");
    for (int i = 0; i < nonInstancedIndex; i++) {
      data.add(nonInstanced[i].toString());
      List<Drawable> drawables = nonInstanced[i].listDrawables();
      for (Drawable d : drawables) {
        data.add(String.format("        %s", d));
      }
    }
    data.add("");
  }
}
