package game1.core.pipelines;

import java.io.IOException;
import java.util.List;

import org.joml.Matrix4f;

import folkforms.log.Log;
import game1.core.graphics.Drawable;
import game1.core.graphics.meshes.InstancedMesh;
import game1.core.infrastructure.lwjgl3.ShaderException;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3ShaderProgram;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3Window;
import game1.core.registries.Lights;

/**
 * JAVADOC
 *
 * Note: This MUST come after the Scene3D pipeline. Otherwise the keyboard keys stop working.
 */
public class SkyboxPipeline extends GraphicsPipeline {

  private LWJGL3ShaderProgram skyboxShaderProgram;

  public SkyboxPipeline() throws IOException, ShaderException {
    super(GraphicsPipelineName.SKYBOX, 1, 0);
    skyboxShaderProgram = new SkyboxShader();
  }

  @Override
  public void render(Matrix4f viewMatrix) {
    renderSkybox(viewMatrix);
  }

  private void renderSkybox(Matrix4f viewMatrix) {
    InstancedMesh mesh = instanced[0];
    if (mesh == null) {
      return;
    }

    List<Drawable> drawables = mesh.listDrawables();

    Drawable skyboxDrawable = drawables.get(0);

    skyboxShaderProgram.bind();
    skyboxShaderProgram.setUniform("texture_sampler", 0);

    Matrix4f projectionMatrix = LWJGL3Window.getProjectionMatrix();
    skyboxShaderProgram.setUniform("projectionMatrix", projectionMatrix);
    float m30 = viewMatrix.m30();
    float m31 = viewMatrix.m31();
    float m32 = viewMatrix.m32();
    viewMatrix.m30(0);
    viewMatrix.m31(0);
    viewMatrix.m32(0);
    Matrix4f modelViewMatrix = transformation.buildModelViewMatrix(skyboxDrawable.getRotation(),
        skyboxDrawable.getPosition(), skyboxDrawable.getScale(), viewMatrix);

    skyboxShaderProgram.setUniform("modelViewMatrix", modelViewMatrix);
    skyboxShaderProgram.setUniform("ambientLight", Lights.getAmbientLightColor());
    skyboxShaderProgram.setUniform("colour", mesh.getMaterial().getAmbientColour());
    skyboxShaderProgram.setUniform("hasTexture", mesh.getMaterial().getTexture() != null ? 1 : 0);
    mesh.render(GraphicsPipelineName.SKYBOX);

    viewMatrix.m30(m30);
    viewMatrix.m31(m31);
    viewMatrix.m32(m32);
    skyboxShaderProgram.unbind();
  }

  @Override
  public void cleanup() {
    if (skyboxShaderProgram != null) {
      skyboxShaderProgram.cleanup();
    }
  }

  @Override
  public void debug_reloadShaders() {
    try {
      skyboxShaderProgram = new SkyboxShader();
    } catch (IOException | ShaderException ex) {
      Log.error("Failed to reload SkyboxPipeline shader");
      Log.error(ex);
    }
  }
}
