package game1.core.pipelines;

import java.io.IOException;

import game1.core.infrastructure.lwjgl3.ShaderException;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3ShaderProgram;

public class BillboardShader extends LWJGL3ShaderProgram {

  public BillboardShader() throws ShaderException, IOException {
    super("game1.shaders.billboard_vertex_shader", "game1.shaders.billboard_fragment_shader");

    createUniform("projectionMatrix");
    createUniform("texture_sampler");
  }
}
