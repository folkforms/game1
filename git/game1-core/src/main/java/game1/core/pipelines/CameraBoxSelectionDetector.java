package game1.core.pipelines;

import org.joml.Intersectionf;
import org.joml.Matrix4f;
import org.joml.Vector2d;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;

import game1.core.engine.internal.Camera;
import game1.core.engine.internal.CameraFactory;
import game1.core.graphics.Drawable;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3Window;
import game1.core.input.Mouse;
import game1.core.input.MouseFactory;
import game1.resolutions.layers.ResolutionLayers;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;

public class CameraBoxSelectionDetector {

  private Scene3DPipeline scene3DPipeline;
  private final Vector3f max;
  private final Vector3f min;
  private final Vector2f nearFar;
  private Vector3f dir;

  public CameraBoxSelectionDetector(Scene3DPipeline scene3DPipeline) {
    this.scene3DPipeline = scene3DPipeline;
    dir = new Vector3f();
    min = new Vector3f();
    max = new Vector3f();
    nearFar = new Vector2f();
  }

  // FIXME PICKING: Change the boolean to a state property or something.
  // FIXME PICKING: Consider adding an example of a Clickable 3D item.
  public Drawable selectDrawable() {
    Camera camera = CameraFactory.getInstance();
    boolean cameraPicking = true;
    if (cameraPicking) {
      dir = camera.getViewMatrix().positiveZ(dir).negate();
    } else {
      // Mouse picking
      dir = getMouseDir();
    }

    Drawable[] drawables = scene3DPipeline.listDrawablesInScene();
    int drawablesIndex = scene3DPipeline.countDrawablesInScene();
    Drawable selectedDrawable = null;
    float closestDistance = Float.POSITIVE_INFINITY;
    for (int i = 0; i < drawablesIndex; i++) {
      Drawable drawable = drawables[i];
      drawables[i].setSelected(false);

      min.set(drawable.getAABMin());
      max.set(drawable.getAABMax());
      min.mul(drawable.getScale(), min);
      max.mul(drawable.getScale(), max);
      min.add(drawable.getPosition());
      max.add(drawable.getPosition());
      Vector3f center = camera.getPosition();

      // This check works fine for a cube
      if (Intersectionf.intersectRayAab(center, dir, min, max, nearFar)
          && nearFar.x < closestDistance) {
        closestDistance = nearFar.x;
        selectedDrawable = drawable;
      }

      // This check works ok for weird shapes, but we need to adjust the center position of the
      // sphere to match the shape. That or the shape could have its 0,0,0 in the center so we
      // don't have to adjust anything. We should probably add a bounding box for each shape that
      // might need one and add the ability to draw it wireframe or else make it semi-transparent.

      // float radiusSquared = 0.1f;
      // Vector2f result = new Vector2f();
      // if (Intersectionf.intersectRaySphere(Engine.CAMERA.getPosition(), dir,
      // drawable.getPosition(), radiusSquared, result)) {
      // closestDistance = nearFar.x;
      // selectedDrawable = drawable;
      // }
    }

    if (selectedDrawable != null) {
      selectedDrawable.setSelected(true);
    }

    return selectedDrawable;
  }

  // Transform mouse coordinates into normalised space [-1, 1]
  private Vector3f getMouseDir() {
    Mouse mouse = MouseFactory.getInstance();
    Vector2d mousePos = new Vector2d(mouse.getWindowPos().x, mouse.getWindowPos().y);

    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(ResolutionLayers.WINDOW_LAYER).getResolutionHints();
    int windowWidth = hints.getWindowWidth();
    int windowHeight = hints.getWindowHeight();

    float x = (float) (2 * mousePos.x) / windowWidth - 1.0f;
    float y = (float) (2 * mousePos.y) / windowHeight - 1.0f;
    float z = -1.0f;

    Matrix4f invProjectionMatrix = new Matrix4f();
    Matrix4f invViewMatrix = new Matrix4f();
    Vector3f mouseDir = new Vector3f();
    Vector4f tmpVec = new Vector4f();

    invProjectionMatrix.set(LWJGL3Window.getProjectionMatrix());
    invProjectionMatrix.invert();

    tmpVec.set(x, y, z, 1.0f);
    tmpVec.mul(invProjectionMatrix);
    tmpVec.z = -1.0f;
    tmpVec.w = 0.0f;

    Matrix4f viewMatrix = CameraFactory.getInstance().getViewMatrix();
    invViewMatrix.set(viewMatrix);
    invViewMatrix.invert();
    tmpVec.mul(invViewMatrix);

    mouseDir.set(tmpVec.x, tmpVec.y, tmpVec.z);

    return mouseDir;
  }
}
