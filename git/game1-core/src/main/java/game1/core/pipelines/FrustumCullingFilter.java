package game1.core.pipelines;

import org.joml.FrustumIntersection;
import org.joml.Matrix4f;
import org.joml.Vector3f;

import game1.core.engine.internal.CameraFactory;
import game1.core.graphics.Drawable;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3Window;

public class FrustumCullingFilter {

  // Debug data
  public static int CULLED = -1;
  public static int NOT_CULLED = -1;
  public static int INELIGIBLE = -1;
  public static int TOTAL = -1;

  private Scene3DPipeline scene3DPipeline;
  private final Matrix4f prjViewMatrix;
  private FrustumIntersection frustumIntersection;

  public FrustumCullingFilter(Scene3DPipeline scene3DPipeline) {
    this.scene3DPipeline = scene3DPipeline;
    prjViewMatrix = new Matrix4f();
    frustumIntersection = new FrustumIntersection();
  }

  public void updateFrustum() {
    // Calculate projection view matrix
    prjViewMatrix.set(LWJGL3Window.getProjectionMatrix());
    prjViewMatrix.mul(CameraFactory.getInstance().getViewMatrix());
    // Update frustum intersection class
    frustumIntersection.set(prjViewMatrix);
  }

  public void filter() {
    CULLED = 0;
    NOT_CULLED = 0;
    INELIGIBLE = 0;
    TOTAL = 0;

    Drawable[] drawables = scene3DPipeline.listDrawablesInScene();
    int drawablesIndex = scene3DPipeline.countDrawablesInScene();
    for (int i = 0; i < drawablesIndex; i++) {
      TOTAL++;
      Drawable d = drawables[i];
      if (!d.isFrustumCullingEnabled()) {
        INELIGIBLE++;
        continue;
      }

      boolean culled = cullWithSphereMethod(d);
      // boolean culled = cullWithCollisionMeshMethod(d);
      // boolean culled = cullWithAABMethod(d);
      if (culled) {
        CULLED++;
      } else {
        NOT_CULLED++;
      }
    }
  }

  // FIXME FRUSTUM_CULLING: Drawable needs a bounding radius value since it can be many meshes. Also
  // displacement of the sphere.
  private boolean cullWithSphereMethod(Drawable drawable) {
    float meshBoundingRadius = 2;
    float scale = drawable.getScale();
    if (scale < 1) {
      scale = 1;
    }
    float boundingRadius = scale * meshBoundingRadius;
    Vector3f pos = drawable.getPosition();
    boolean insideFrustum = frustumIntersection.testSphere(pos.x, pos.y, pos.z, boundingRadius);
    drawable.setInsideFrustum(insideFrustum);
    return !insideFrustum;
  }

  // This has problems with what order to apply the scaling/translation, and also it is culling
  // on-screen items that are just at the edge of the screen.
  private boolean cullWithAABMethod(Drawable drawable) {
    Vector3f pos = drawable.getPosition();
    float scale = drawable.getScale();
    Vector3f min = drawable.getAABMin();
    Vector3f max = drawable.getAABMax();
    // The scale should be done first but for some reason that doesn't work and I need to reverse it
    // for the 10000 instanced cubes. But for the animated model it works fine.
    Vector3f min2 = min.add(pos).mul(scale);
    Vector3f max2 = max.add(pos).mul(scale);
    // intersectAab will return the plane (0-5) that culled the frustum
    // boolean insideFrustum = frustumIntersection.intersectAab(min2, max2) < 0;
    boolean insideFrustum = frustumIntersection.testAab(min2, max2);
    drawable.setInsideFrustum(insideFrustum);
    return !insideFrustum;
  }

  /**
   * We need to enable all items so the depth map can render shadows correctly.
   */
  public void reset() {
    Drawable[] drawables = scene3DPipeline.listDrawablesInScene();
    int drawablesIndex = scene3DPipeline.countDrawablesInScene();
    for (int i = 0; i < drawablesIndex; i++) {
      drawables[i].setInsideFrustum(true);
    }
  }
}
