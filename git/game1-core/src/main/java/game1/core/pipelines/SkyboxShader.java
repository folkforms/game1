package game1.core.pipelines;

import java.io.IOException;

import game1.core.infrastructure.lwjgl3.ShaderException;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3ShaderProgram;

public class SkyboxShader extends LWJGL3ShaderProgram {

  public SkyboxShader() throws IOException, ShaderException {
    super("game1.shaders.skybox_vertex_shader", "game1.shaders.skybox_fragment_shader");

    // Create uniforms for projection matrix
    createUniform("projectionMatrix");
    createUniform("modelViewMatrix");
    createUniform("texture_sampler");
    createUniform("ambientLight");
    createUniform("colour");
    createUniform("hasTexture");
  }
}
