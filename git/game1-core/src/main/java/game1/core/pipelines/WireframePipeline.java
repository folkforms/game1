package game1.core.pipelines;

import java.io.IOException;

import org.joml.Matrix4f;

import folkforms.log.Log;
import game1.core.graphics.Drawable;
import game1.core.graphics.meshes.InstancedMesh;
import game1.core.infrastructure.lwjgl3.ShaderException;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3ShaderProgram;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3Window;

public class WireframePipeline extends GraphicsPipeline {

  private LWJGL3ShaderProgram wireframeShaderProgram;

  // FIXME PIPELINES: I will need a property to set these array sizes so that you know the number
  // and can set it later. Maybe recreate the array with a warning when it goes over.
  public WireframePipeline() throws IOException, ShaderException {
    super(GraphicsPipelineName.WIREFRAME, 11000, 100);
    wireframeShaderProgram = new WireframeShader();
  }

  @Override
  public void add(Drawable d) {
    super.add(d);
  }

  @Override
  public void remove(Drawable d) {
    super.remove(d);
  }

  @Override
  public void render(Matrix4f viewMatrix) {
    renderScene(viewMatrix);
  }

  private void renderScene(Matrix4f viewMatrix) {
    wireframeShaderProgram.bind();

    Matrix4f projectionMatrix = LWJGL3Window.getProjectionMatrix();
    wireframeShaderProgram.setUniform("projectionMatrix", projectionMatrix);
    Matrix4f lightViewMatrix = transformation.getLightViewMatrix();

    renderInstancedMeshes(wireframeShaderProgram, viewMatrix, lightViewMatrix);
    wireframeShaderProgram.unbind();
  }

  private void renderInstancedMeshes(LWJGL3ShaderProgram shader, Matrix4f viewMatrix,
      Matrix4f lightViewMatrix) {
    int currentNumInstanced = instancedIndex;
    for (int i = 0; i < currentNumInstanced; i++) {
      InstancedMesh instancedMesh = instanced[i];
      instancedMesh.renderListInstanced(transformation, viewMatrix, lightViewMatrix);
    }
  }

  @Override
  public void cleanup() {
    if (wireframeShaderProgram != null) {
      wireframeShaderProgram.cleanup();
    }
  }

  @Override
  public void debug_reloadShaders() {
    try {
      WireframeShader newWireframeShaderProgram = new WireframeShader();
      wireframeShaderProgram = newWireframeShaderProgram;
    } catch (IOException | ShaderException ex) {
      Log.error("Failed to reload WireframePipeline shader");
      Log.error(ex);
    }
  }
}
