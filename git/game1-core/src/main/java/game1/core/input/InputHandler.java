package game1.core.input;

import java.util.List;

import game1.core.engine.Engine;
import game1.core.engine.internal.UpdateHandler;
import game1.events.Command;

/**
 * Class for querying {@link InputDevice}s for user input and processing the relevant
 * {@link KeyCommands}.
 */
public class InputHandler {

  private List<InputDevice> devices = InputDeviceRegistry.listInputDevices();

  /**
   * The delay before a pressed input repeats, in ticks.
   */
  private static final long DELAY_BEFORE_REPEAT_IN_TICKS = 100;

  /**
   * The frequency with which a pressed input repeats, in ticks.
   */
  private static final long REPEAT_SPEED_IN_TICKS = 20;

  /**
   * Queries each {@link InputDevice} and sets its state.
   */
  public void setStates() {
    int size = devices.size();
    for (int i = 0; i < size; i++) {
      InputDevice device = devices.get(i);
      device.clearConsumed();
      device.backupState();
      device.setState();
    }
  }

  /**
   * Execute actions based on what keys are currently pressed. If a key is no longer pressed it will
   * be unblocked.
   */
  public void handleUserInput() {
    long currentTick = UpdateHandler.getTick();
    List<KeyCommand> keyCommands = Engine.KEY_BINDINGS.listActiveKeyCommands();
    int size = keyCommands.size();
    for (int i = 0; i < size; i++) {
      // If Engine.GAME_STATE.moveToState was called a KeyCommand then the keybindings list may no
      // longer be valid. In that case we abort processing and get the new list next update.
      if (Engine.KEY_BINDINGS.pollKeyBindingsChanged()) {
        return;
      }
      KeyCommand keyCommand = keyCommands.get(i);
      int[] keyCodes = keyCommand.getKeyCodes();
      int[] mouseCodes = keyCommand.getMouseCodes();
      int[] controllerCodes = keyCommand.getControllerCodes();
      String blockType = keyCommand.getBlockType();
      List<Command> commands = keyCommand.getCommands();
      int tickRate = keyCommand.getTickRate();

      // Check enough time has elapsed between ticks
      if (currentTick - keyCommand.getLastTickTime() < tickRate) {
        continue;
      }

      // Check all keys are down
      if (!allKeysAreDown(keyCodes, mouseCodes, controllerCodes)) {
        keyCommand.resetRepeatNumber();
        continue;
      }

      // Check keys have not already been consumed
      if (keyWasAlreadyConsumed(keyCodes, mouseCodes, controllerCodes)) {
        keyCommand.resetRepeatNumber();
        continue;
      }

      // If it's a blocking key check there has been a change since the last state
      if (blockType.equals(KeyCommand.BLOCK)
          && noChangeInKeyStates(keyCodes, mouseCodes, controllerCodes)) {
        continue;
      }

      // If it's a delay repeat and we've actioned it once, wait before actioning it again
      if (blockType.equals(KeyCommand.DELAY_REPEAT) && keyCommand.getRepeatNumber() == 1
          && currentTick - keyCommand.getLastTickTime() < DELAY_BEFORE_REPEAT_IN_TICKS) {
        continue;
      }

      // If it's a delay repeat and we've actioned it more than once, wait before actioning it again
      if (blockType.equals(KeyCommand.DELAY_REPEAT) && keyCommand.getRepeatNumber() > 1
          && currentTick - keyCommand.getLastTickTime() < REPEAT_SPEED_IN_TICKS) {
        continue;
      }

      // ================================

      // If we have got this far then everything is ok, let's execute the commands
      for (int c = 0; c < commands.size(); c++) {
        Command command = commands.get(c);
        command.execute();
      }

      // Update the last tick time and repeat number
      keyCommand.setLastTickTime(currentTick);
      keyCommand.incrementRepeatNumber();

      // Update consumed keys
      updateConsumedKeys(keyCodes, mouseCodes, controllerCodes);
    }

    // Clear state
    int numDevices = devices.size();
    for (int i = 0; i < numDevices; i++) {
      InputDevice device = devices.get(i);
      device.clearState();
    }
  }

  /**
   * Checks that all keys in the given key command are pressed.
   *
   * @param keyCodes
   *          keyCodes from the KeyCommand
   * @param mouseCodes
   *          mouseCodes from the KeyCommand
   * @return true if all keys in the given key command are pressed, false otherwise
   */
  private boolean allKeysAreDown(int[] keyCodes, int[] mouseCodes, int[] controllerCodes) {
    return allKeysAreDown(keyCodes, InputDeviceRegistry.getDevice("keyboard"))
        && allKeysAreDown(mouseCodes, InputDeviceRegistry.getDevice("mouse"))
        && allKeysAreDown(controllerCodes, InputDeviceRegistry.getDevice("controller"));
  }

  private boolean allKeysAreDown(int[] codes, InputDevice device) {
    if (device == null) {
      return true;
    }
    for (int i = 0; i < codes.length; i++) {
      if (!device.isActive(codes[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks that there has been no change in the states of the keys of the given key command, i.e.
   * all the keys are still held down.
   *
   * @param keyCodes
   *          keyCodes from the KeyCommand
   * @param mouseCodes
   *          mouseCodes from the KeyCommand
   * @return true if there has been no change in the states of the keys of the given key command,
   *         false otherwise
   */
  private boolean noChangeInKeyStates(int[] keyCodes, int[] mouseCodes, int[] controllerCodes) {
    return noChangeInKeyStates(keyCodes, InputDeviceRegistry.getDevice("keyboard"))
        && noChangeInKeyStates(mouseCodes, InputDeviceRegistry.getDevice("mouse"))
        && noChangeInKeyStates(controllerCodes, InputDeviceRegistry.getDevice("controller"));
  }

  private boolean noChangeInKeyStates(int[] codes, InputDevice device) {
    if (device == null) {
      return true;
    }
    for (int i = 0; i < codes.length; i++) {
      if (device.isActive(codes[i]) != device.wasActive(codes[i])) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks if any of the key codes from the given key command have already been consumed this
   * update.
   *
   * @param keyCodes
   *          keyCodes from the KeyCommand
   * @param mouseCodes
   *          mouseCodes from the KeyCommand
   * @return true if any of the key codes from the given key command have already been consumed this
   *         update, false otherwise
   */
  private boolean keyWasAlreadyConsumed(int[] keyCodes, int[] mouseCodes, int[] controllerCodes) {
    return keyWasAlreadyConsumed(keyCodes, InputDeviceRegistry.getDevice("keyboard"))
        || keyWasAlreadyConsumed(mouseCodes, InputDeviceRegistry.getDevice("mouse"))
        || keyWasAlreadyConsumed(controllerCodes, InputDeviceRegistry.getDevice("controller"));
  }

  private boolean keyWasAlreadyConsumed(int[] codes, InputDevice device) {
    if (device == null) {
      return false;
    }
    for (int i = 0; i < codes.length; i++) {
      if (device.isConsumed(codes[i])) {
        return true;
      }
    }
    return false;
  }

  /**
   * Marks the key codes from the given key command as having been consumed this update, so that
   * they will not affect other key commands.
   *
   * @param keyCodes
   *          keyCodes from the KeyCommand
   * @param mouseCodes
   *          mouseCodes from the KeyCommand
   */
  private void updateConsumedKeys(int[] keyCodes, int[] mouseCodes, int[] controllerCodes) {
    updateConsumedKeys(keyCodes, InputDeviceRegistry.getDevice("keyboard"));
    updateConsumedKeys(mouseCodes, InputDeviceRegistry.getDevice("mouse"));
    updateConsumedKeys(controllerCodes, InputDeviceRegistry.getDevice("controller"));
  }

  private void updateConsumedKeys(int[] codes, InputDevice device) {
    if (device == null) {
      return;
    }
    for (int i = 0; i < codes.length; i++) {
      device.setConsumed(codes[i]);
    }
  }
}
