package game1.core.input;

import org.joml.Vector2f;

import game1.actors.Actor;
import game1.actors.Clickable;
import game1.resolutions.layers.ResolutionLayer;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;
import game1.resolutions.viewports.SceneViewport;
import game1.resolutions.viewports.Viewport;

public class MouseUtils {

  public static Vector2f translateToResolutionLayerPosition(Vector2f windowMousePos,
      String resolutionLayerName) {
    ResolutionLayer layer = ResolutionLayersFactory.getInstance()
        .getLayerByName(resolutionLayerName);
    return getScenePos(windowMousePos, layer.getResolutionHints(), layer.getViewport());
  }

  public static Vector2f translateToResolutionLayerPosition(Vector2f windowMousePos, Actor actor) {
    ResolutionLayer layer = ResolutionLayersFactory.getInstance().getLayerForActor(actor);
    return getScenePos(windowMousePos, layer.getResolutionHints(), layer.getViewport());
  }

  public static Vector2f translateToResolutionLayerPosition(Vector2f windowMousePos,
      ResolutionLayer layer) {
    return getScenePos(windowMousePos, layer.getResolutionHints(), layer.getViewport());
  }

  private static Vector2f getScenePos(Vector2f windowMousePos, ResolutionHints hints,
      Viewport viewport) {
    float scale = hints.getResolutionScale();
    float mx = windowMousePos.x;
    float my = windowMousePos.y;
    float sceneX;
    float sceneY;
    float vx = viewport.getX();
    float vy = viewport.getY();
    float ox = hints.getResolutionOffsetX();
    float oy = hints.getResolutionOffsetY();
    if (viewport instanceof SceneViewport) {
      sceneX = mx / scale + vx - ox / scale;
      sceneY = my / scale + vy - oy / scale;
    } else {
      sceneX = (mx + vx) / scale - ox / scale;
      sceneY = (my + vy) / scale - oy / scale;
    }
    return new Vector2f(sceneX, sceneY);
  }

  public static boolean hover(Clickable clickable) {
    Vector2f translate = MouseUtils
        .translateToResolutionLayerPosition(MouseFactory.getInstance().getWindowPos(), clickable);
    return clickable.getShape().contains(translate);
  }
}
