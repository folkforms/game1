package game1.core.input;

import java.util.Arrays;

import org.joml.Vector2f;

import game1.core.engine.internal.CameraFactory;
import game1.events.EventsFactory;
import game1.events.Game1Events;

/**
 * Represents a mouse attached to the system.
 */
abstract public class MouseInputDevice extends InputDevice {

  protected int x, y;
  protected Vector2f windowPos = new Vector2f(0, 0);
  protected boolean wheelUp, wheelDown;
  protected boolean previousWheelUp, previousWheelDown;
  protected boolean consumedWheelUp, consumedWheelDown;
  private boolean firstCameraDeltaCheck = true;
  private Float[] eventData = new Float[3];

  protected MouseInputDevice(int numButtons) {
    super(numButtons);
    Arrays.fill(eventData, 0f);
  }

  /**
   * Raises a {@link Game1Events#MOUSE_MOVE} event. This might, for example, be consumed by a player
   * object which is attached to the camera.
   *
   * @param deltaX
   *          x delta
   * @param deltaY
   *          y delta
   */
  public void raiseMouseMoveEvent(double deltaX, double deltaY) {
    if (MouseFactory.getInstance().isMouseCameraOn()) {
      if (firstCameraDeltaCheck) {
        firstCameraDeltaCheck = false;
      } else {
        float mouseSensitivity = CameraFactory.getInstance().getMouseSensitivity();
        float dx = (float) deltaX;
        float dy = (float) deltaY;
        // These are backwards because rotation around an axis is perpendicular to that axis. For
        // example the x-axis line goes left to right on the screen, so rotating around that line
        // would tilt the player's head up or down
        eventData[0] = dy * mouseSensitivity;
        eventData[1] = dx * mouseSensitivity;
        EventsFactory.getInstance().raise(Game1Events.MOUSE_MOVE, eventData[0], eventData[1],
            eventData[2]);
      }
    }
  }

  @Override
  public void setState() {
    Mouse mouse = MouseFactory.getInstance();
    if (mouse.isMouseCameraOn()) {
      setCameraPosition();
    } else {
      setCursorPosition();
      mouse.getCursor().setPosition(windowPos);
    }
    setScrollWheelState();
    setButtonStates();
  }

  @Override
  void clearState() {
    wheelUp = false;
    wheelDown = false;
  }

  @Override
  protected boolean isActive(int item) {
    if (item == Mouse.SCROLL_WHEEL_UP) {
      return wheelUp;
    } else if (item == Mouse.SCROLL_WHEEL_DOWN) {
      return wheelDown;
    } else {
      return states[item];
    }
  }

  @Override
  boolean wasActive(int item) {
    if (item == Mouse.SCROLL_WHEEL_UP) {
      return previousWheelUp;
    } else if (item == Mouse.SCROLL_WHEEL_DOWN) {
      return previousWheelDown;
    } else {
      return previousStates[item];
    }
  }

  @Override
  boolean isConsumed(int item) {
    if (item == Mouse.SCROLL_WHEEL_UP) {
      return consumedWheelUp;
    } else if (item == Mouse.SCROLL_WHEEL_DOWN) {
      return consumedWheelDown;
    } else {
      return super.isConsumed(item);
    }
  }

  @Override
  void setConsumed(int item) {
    if (item == Mouse.SCROLL_WHEEL_UP) {
      consumedWheelUp = true;
    } else if (item == Mouse.SCROLL_WHEEL_DOWN) {
      consumedWheelDown = true;
    } else {
      super.setConsumed(item);
    }
  }

  @Override
  void clearConsumed() {
    super.clearConsumed();
    consumedWheelUp = false;
    consumedWheelDown = false;
  }

  /**
   * Sets the cursor position.
   */
  protected abstract void setCursorPosition();

  /**
   * Sets the camera position.
   */
  protected abstract void setCameraPosition();

  /**
   * Sets the scroll wheel state.
   */
  protected abstract void setScrollWheelState();

  /**
   * Sets the button states.
   */
  protected abstract void setButtonStates();

  public Vector2f getWindowPos() {
    return windowPos;
  }

  abstract public void centerCursor();
}
