package game1.core.input;

import java.util.Comparator;

/**
 * Sorts two {@link KeyCommand}s such that the one with more inputs is first. This is so that a
 * 'shift+x' {@link KeyCommand} will always be checked before an 'x' {@link KeyCommand} regardless
 * of the order they were defined or added in. This comparison is applied across the entire set of
 * active {@link KeyCommand}s.
 *
 * <p>
 * If two {@link KeyCommand}s have the same number of inputs then the order will be unchanged i.e.
 * the order they are checked will depend on the order they were defined within the
 * {@link KeyBindings} (if both defined in the same class) or the order their {@link KeyBindings}
 * classes were activated (if defined in separate classes.)
 * </p>
 */
public class KeyCommandComparator implements Comparator<KeyCommand> {

  @Override
  public int compare(KeyCommand kc1, KeyCommand kc2) {
    int count1 = kc1.getKeyCodes().length + kc1.getMouseCodes().length;
    int count2 = kc2.getKeyCodes().length + kc2.getMouseCodes().length;
    if (count1 > count2) {
      return -1;
    } else if (count1 < count2) {
      return 1;
    } else {
      return 0;
    }
  }
}
