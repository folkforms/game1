package game1.core.input;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Stream;

import game1.core.engine.Engine;
import game1.variables.VariablesFactory;

/**
 * Utilities to convert keyboard keys between human-readable strings and integer key codes.
 */
public class KeyboardUtils {
  /**
   * This map is used to convert between strings and integers (e.g. "player.jump=KEY_A" => 65).
   */
  private static Map<String, Integer> stringToIntegerMap = new HashMap<>();

  static {
    stringToIntegerMap.put("KEY_0", Keys.KEY_0);
    stringToIntegerMap.put("KEY_1", Keys.KEY_1);
    stringToIntegerMap.put("KEY_2", Keys.KEY_2);
    stringToIntegerMap.put("KEY_3", Keys.KEY_3);
    stringToIntegerMap.put("KEY_4", Keys.KEY_4);
    stringToIntegerMap.put("KEY_5", Keys.KEY_5);
    stringToIntegerMap.put("KEY_6", Keys.KEY_6);
    stringToIntegerMap.put("KEY_7", Keys.KEY_7);
    stringToIntegerMap.put("KEY_8", Keys.KEY_8);
    stringToIntegerMap.put("KEY_9", Keys.KEY_9);
    stringToIntegerMap.put("KEY_A", Keys.KEY_A);
    stringToIntegerMap.put("KEY_ADD", Keys.KEY_ADD);
    stringToIntegerMap.put("KEY_APOSTROPHE", Keys.KEY_APOSTROPHE);
    stringToIntegerMap.put("KEY_APPS", Keys.KEY_APPS);
    stringToIntegerMap.put("KEY_B", Keys.KEY_B);
    stringToIntegerMap.put("KEY_BACKSPACE", Keys.KEY_BACKSPACE);
    stringToIntegerMap.put("KEY_BACKSLASH", Keys.KEY_BACKSLASH);
    stringToIntegerMap.put("KEY_C", Keys.KEY_C);
    stringToIntegerMap.put("KEY_CAPSLOCK", Keys.KEY_CAPSLOCK);
    stringToIntegerMap.put("KEY_COMMA", Keys.KEY_COMMA);
    stringToIntegerMap.put("KEY_D", Keys.KEY_D);
    stringToIntegerMap.put("KEY_DELETE", Keys.KEY_DELETE);
    stringToIntegerMap.put("KEY_DIVIDE", Keys.KEY_DIVIDE);
    stringToIntegerMap.put("KEY_DOWN_ARROW", Keys.KEY_DOWN_ARROW);
    stringToIntegerMap.put("KEY_E", Keys.KEY_E);
    stringToIntegerMap.put("KEY_END", Keys.KEY_END);
    stringToIntegerMap.put("KEY_ENTER", Keys.KEY_ENTER);
    stringToIntegerMap.put("KEY_EQUALS", Keys.KEY_EQUALS);
    stringToIntegerMap.put("KEY_ESCAPE", Keys.KEY_ESCAPE);
    stringToIntegerMap.put("KEY_F", Keys.KEY_F);
    stringToIntegerMap.put("KEY_F1", Keys.KEY_F1);
    stringToIntegerMap.put("KEY_F2", Keys.KEY_F2);
    stringToIntegerMap.put("KEY_F3", Keys.KEY_F3);
    stringToIntegerMap.put("KEY_F4", Keys.KEY_F4);
    stringToIntegerMap.put("KEY_F5", Keys.KEY_F5);
    stringToIntegerMap.put("KEY_F6", Keys.KEY_F6);
    stringToIntegerMap.put("KEY_F7", Keys.KEY_F7);
    stringToIntegerMap.put("KEY_F8", Keys.KEY_F8);
    stringToIntegerMap.put("KEY_F9", Keys.KEY_F9);
    stringToIntegerMap.put("KEY_F10", Keys.KEY_F10);
    stringToIntegerMap.put("KEY_F11", Keys.KEY_F11);
    stringToIntegerMap.put("KEY_F12", Keys.KEY_F12);
    stringToIntegerMap.put("KEY_F13", Keys.KEY_F13);
    stringToIntegerMap.put("KEY_F14", Keys.KEY_F14);
    stringToIntegerMap.put("KEY_F15", Keys.KEY_F15);
    stringToIntegerMap.put("KEY_F16", Keys.KEY_F16);
    stringToIntegerMap.put("KEY_F17", Keys.KEY_F17);
    stringToIntegerMap.put("KEY_F18", Keys.KEY_F18);
    stringToIntegerMap.put("KEY_F19", Keys.KEY_F19);
    stringToIntegerMap.put("KEY_FORWARD_SLASH", Keys.KEY_FORWARD_SLASH);
    stringToIntegerMap.put("KEY_G", Keys.KEY_G);
    stringToIntegerMap.put("KEY_H", Keys.KEY_H);
    stringToIntegerMap.put("KEY_HOME", Keys.KEY_HOME);
    stringToIntegerMap.put("KEY_I", Keys.KEY_I);
    stringToIntegerMap.put("KEY_INSERT", Keys.KEY_INSERT);
    stringToIntegerMap.put("KEY_J", Keys.KEY_J);
    stringToIntegerMap.put("KEY_K", Keys.KEY_K);
    stringToIntegerMap.put("KEY_L", Keys.KEY_L);
    stringToIntegerMap.put("KEY_LEFT_BRACKET", Keys.KEY_LEFT_BRACKET);
    stringToIntegerMap.put("KEY_LEFT_CTRL", Keys.KEY_LEFT_CTRL);
    stringToIntegerMap.put("KEY_LEFT_ARROW", Keys.KEY_LEFT_ARROW);
    stringToIntegerMap.put("KEY_LEFT_ALT", Keys.KEY_LEFT_ALT);
    stringToIntegerMap.put("KEY_LEFT_SHIFT", Keys.KEY_LEFT_SHIFT);
    stringToIntegerMap.put("KEY_M", Keys.KEY_M);
    stringToIntegerMap.put("KEY_MINUS", Keys.KEY_MINUS);
    stringToIntegerMap.put("KEY_MULTIPLY", Keys.KEY_MULTIPLY);
    stringToIntegerMap.put("KEY_N", Keys.KEY_N);
    stringToIntegerMap.put("KEY_NUMLOCK", Keys.KEY_NUMLOCK);
    stringToIntegerMap.put("KEY_NUMPAD0", Keys.KEY_NUMPAD_0);
    stringToIntegerMap.put("KEY_NUMPAD1", Keys.KEY_NUMPAD_1);
    stringToIntegerMap.put("KEY_NUMPAD2", Keys.KEY_NUMPAD_2);
    stringToIntegerMap.put("KEY_NUMPAD3", Keys.KEY_NUMPAD_3);
    stringToIntegerMap.put("KEY_NUMPAD4", Keys.KEY_NUMPAD_4);
    stringToIntegerMap.put("KEY_NUMPAD5", Keys.KEY_NUMPAD_5);
    stringToIntegerMap.put("KEY_NUMPAD6", Keys.KEY_NUMPAD_6);
    stringToIntegerMap.put("KEY_NUMPAD7", Keys.KEY_NUMPAD_7);
    stringToIntegerMap.put("KEY_NUMPAD8", Keys.KEY_NUMPAD_8);
    stringToIntegerMap.put("KEY_NUMPAD9", Keys.KEY_NUMPAD_9);
    stringToIntegerMap.put("KEY_NUMPAD_DECIMAL", Keys.KEY_NUMPAD_DECIMAL);
    stringToIntegerMap.put("KEY_NUMPAD_ENTER", Keys.KEY_NUMPAD_ENTER);
    stringToIntegerMap.put("KEY_NUMPAD_EQUALS", Keys.KEY_NUMPAD_EQUALS);
    stringToIntegerMap.put("KEY_O", Keys.KEY_O);
    stringToIntegerMap.put("KEY_P", Keys.KEY_P);
    stringToIntegerMap.put("KEY_PAUSE", Keys.KEY_PAUSE);
    stringToIntegerMap.put("KEY_PERIOD", Keys.KEY_PERIOD);
    stringToIntegerMap.put("KEY_PAGE_DOWN", Keys.KEY_PAGE_DOWN);
    stringToIntegerMap.put("KEY_PAGE_UP", Keys.KEY_PAGE_UP);
    stringToIntegerMap.put("KEY_Q", Keys.KEY_Q);
    stringToIntegerMap.put("KEY_R", Keys.KEY_R);
    stringToIntegerMap.put("KEY_RIGHT_BRACKET", Keys.KEY_RIGHT_BRACKET);
    stringToIntegerMap.put("KEY_RIGHT_CTRL", Keys.KEY_RIGHT_CTRL);
    stringToIntegerMap.put("KEY_RIGHT_ARROW", Keys.KEY_RIGHT_ARROW);
    stringToIntegerMap.put("KEY_RIGHT_ALT", Keys.KEY_RIGHT_ALT);
    stringToIntegerMap.put("KEY_RIGHT_SHIFT", Keys.KEY_RIGHT_SHIFT);
    stringToIntegerMap.put("KEY_S", Keys.KEY_S);
    stringToIntegerMap.put("KEY_SCROLL_LOCK", Keys.KEY_SCROLL_LOCK);
    stringToIntegerMap.put("KEY_SEMICOLON", Keys.KEY_SEMICOLON);
    stringToIntegerMap.put("KEY_SPACE", Keys.KEY_SPACE);
    stringToIntegerMap.put("KEY_SUBTRACT", Keys.KEY_SUBTRACT);
    stringToIntegerMap.put("KEY_T", Keys.KEY_T);
    stringToIntegerMap.put("KEY_TAB", Keys.KEY_TAB);
    stringToIntegerMap.put("KEY_U", Keys.KEY_U);
    stringToIntegerMap.put("KEY_UP_ARROW", Keys.KEY_UP_ARROW);
    stringToIntegerMap.put("KEY_V", Keys.KEY_V);
    stringToIntegerMap.put("KEY_W", Keys.KEY_W);
    stringToIntegerMap.put("KEY_X", Keys.KEY_X);
    stringToIntegerMap.put("KEY_Y", Keys.KEY_Y);
    stringToIntegerMap.put("KEY_Z", Keys.KEY_Z);
  }

  /**
   * Converts a string of space-separated keys to key codes. Will prefix "KEY_" to the value if not
   * already present and will convert to uppercase. For example: "a, b" => "KEY_A, KEY_B" => { 65,
   * 66 }.
   *
   * @param keys
   *          a string of space-separated keys
   * @return an array of integers
   */
  public static int[] convert(String keys) {
    String[] tokens = keys.split(" ");
    int[] keyCodes = new int[tokens.length];
    for (int i = 0; i < tokens.length; i++) {
      String key = tokens[i];
      String token = key.toUpperCase();
      if (!token.startsWith("KEY_")) {
        token = "KEY_" + token;
      }
      Integer keyCode = stringToIntegerMap.get(token);
      if (keyCode == null) {
        throw new NullPointerException(String.format(
            "Could not find a mapping for key '%s' ('%s') in KeyboardUtils class.", key, token));
      }
      keyCodes[i] = keyCode;
    }
    return keyCodes;
  }

  /**
   * Converts an array of keycodes into an array of strings representing the keycode names.
   *
   * @param keyCodes
   *          keycodes to convert
   * @return an array of strings
   */
  public static String[] convert(int... keyCodes) {
    String[] keyNames = new String[keyCodes.length];
    for (int i = 0; i < keyCodes.length; i++) {
      Iterator<String> iterator = stringToIntegerMap.keySet().iterator();
      while (iterator.hasNext()) {
        String key = iterator.next();
        if (stringToIntegerMap.get(key) == keyCodes[i]) {
          keyNames[i] = key;
          break;
        }
      }
    }
    return keyNames;
  }

  /**
   * Converts a property representing a key combination into a display string, for example
   * "Shift+F11".
   */
  public static String convertForDisplay(String keyboardKeysProperty) {
    int[] keyCodes = Engine.PROPERTIES.getKeyboardKeysProperty(keyboardKeysProperty);
    return convertForDisplay(keyCodes);
  }

  /**
   * Converts a property representing a key combination into a display string, for example
   * "Shift+F11".
   */
  public static String convertForDisplay2(String keyboardKeysProperty) {
    String value = VariablesFactory.getInstance().get(keyboardKeysProperty);
    int[] keyCodes = KeyboardUtils.convert(value);
    return convertForDisplay(keyCodes);
  }

  /**
   * Converts a property representing a key combination into a display string, for example
   * "Shift+F11".
   */
  private static String convertForDisplay(int[] keyCodes) {
    String[] keyStrings = convert(keyCodes);
    keyStrings = Stream.of(keyStrings).map(x -> x.replace("KEY_", "")).toArray(String[]::new);
    StringBuffer sb = new StringBuffer(keyStrings[0]);
    for (int i = 1; i < keyStrings.length; i++) {
      sb.append("+").append(keyStrings[i]);
    }
    return sb.toString();
  }
}
