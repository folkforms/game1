package game1.core.input;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import game1.core.engine.Engine;

/**
 * Defines a list of key bindings that can be set for your application. Your application can change
 * the current key bindings using {@link Engine#KEY_BINDINGS_MANAGER.activate(String)}.
 *
 * @see {@link Engine#KEY_BINDINGS_MANAGER.activate(String)}
 */
public class KeyBindings {

  private List<KeyCommand> keyCommands = new ArrayList<>();

  /**
   * Registers a {@link KeyCommand} with this {@link KeyBindings}.
   *
   * @param keyCommand
   *          key command to add
   */
  public void register(KeyCommand keyCommand) {
    keyCommands.add(keyCommand);
  }

  /**
   * Lists the {@link KeyCommand}s in this set.
   *
   * @return a list of the {@link KeyCommand}s in this set
   */
  List<KeyCommand> listKeyCommands() {
    return keyCommands;
  }

  /**
   * Removes all existing key commands that use exactly the same key codes as the replacement key
   * command, and then adds the replacement key command. Used for overriding key bindings in
   * specific circumstances. For example, TextArea maps the enter key to "\n" and TextField needs to
   * replace that with a custom "submit" action. Replacing the key command is a lot simpler than
   * using a new set of key bindings.
   *
   * @param replacement
   *          the key command to use as a replacement
   */
  public void internal_replace(KeyCommand replacement) {
    remove(replacement.getKeyCodes());
    register(replacement);
  }

  private void remove(int... keyCodes) {
    keyCommands = keyCommands.stream().filter(kc -> {
      return !keyCodesMatch(kc.getKeyCodes(), keyCodes);
    }).collect(Collectors.toList());
  }

  private boolean keyCodesMatch(int[] keyCodes, int[] keyCodes2) {
    if (keyCodes.length != keyCodes2.length) {
      return false;
    }
    Arrays.sort(keyCodes);
    Arrays.sort(keyCodes2);
    for (int i = 0; i < keyCodes.length; i++) {
      if (keyCodes[i] != keyCodes2[i]) {
        return false;
      }
    }
    return true;
  }
}
