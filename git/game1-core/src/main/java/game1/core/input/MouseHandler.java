package game1.core.input;

import org.joml.Vector2f;

import folkforms.log.Log;
import game1.actors.Clickable;
import game1.actors.Draggable;
import game1.actors.Focusable;
import game1.actors.Scrollable;
import game1.core.registries.ClickableActors;
import game1.core.registries.DraggableActors;
import game1.core.registries.FocusableActors;
import game1.core.registries.ScrollableActors;
import game1.debug.DebugScopes;
import game1.events.EventsFactory;
import game1.events.Game1Events;

/**
 * Class for handling GUI mouse events such as Clickables, Focusables, Draggables and Scrollables.
 *
 * <p>
 * Standard mouse input (cursor position, clicks, scroll wheel, etc.) is handled by
 * {@link MouseInputDevice}.
 */
public class MouseHandler {

  private InputDevice mouseInputDevice;

  public MouseHandler() {
    mouseInputDevice = InputDeviceRegistry.getDevice("mouse");
  }

  private int MOUSE_DOWN_EVENT = 1;
  private int MOUSE_UP_EVENT = 2;

  public void handleUserInput() {
    Mouse mouse = MouseFactory.getInstance();
    float windowX = mouse.getWindowPos().x;
    float windowY = mouse.getWindowPos().y;

    // Compare new button states and previous button states and update events array as appropriate
    int[] events = new int[mouseInputDevice.getNumStates()];
    for (int i = 0; i < events.length; i++) {
      if (mouseInputDevice.isActive(i) != mouseInputDevice.wasActive(i)) {
        if (mouseInputDevice.isActive(i)) {
          events[i] = MOUSE_DOWN_EVENT;
        } else {
          events[i] = MOUSE_UP_EVENT;
        }
      }
    }

    for (int i = 0; i < events.length; i++) {

      // Handle clicking...
      // ...Handle mouse down
      if (events[i] == MOUSE_DOWN_EVENT) {
        ClickableActors.setMouseDownItem(i);
      }

      // ...Handle mouse up
      if (events[i] == MOUSE_UP_EVENT) {
        Clickable mouseDownItem = ClickableActors.getMouseDownItem();
        Clickable mouseUpItem = ClickableActors.getItemUnderMouse();

        // ...If we mouse up'ed on an item other than the focused item, the clear the focus
        Focusable selected = FocusableActors.getSelected();
        if (selected != mouseUpItem) {
          FocusableActors.setSelected(null);
        }

        boolean eventConsumed = false;
        // ...If there was a mouse down item, and we mouse up'ed on it, then call its onClick
        if (mouseDownItem != null) {
          if (mouseDownItem != null && mouseUpItem == mouseDownItem) {
            mouseDownItem.onClick(i, windowX, windowY);
            eventConsumed = true;
          }
          ClickableActors.clearMouseDownItem();
        }

        // Only raise a mouse event if the click was not already consumed. Otherwise you can trigger
        // overlapping buttons if one is clickable-driven and one is event-driven.
        if (!eventConsumed) {
          Log.scoped(DebugScopes.MOUSE,
              "Raising event name = '%s', button = %s, window pos = %s,%s", Game1Events.MOUSE_CLICK,
              i, (int) windowX, (int) windowY);
          EventsFactory.getInstance().raise(Game1Events.MOUSE_CLICK, i,
              new Vector2f(windowX, windowY));
        }
      }

      // Handle dragging...
      // ...If we mouse down on a Draggable then start dragging it
      if (events[i] == MOUSE_DOWN_EVENT) {
        DraggableActors.setMouseDownItem(windowX, windowY);
      }

      // ...If we are currently dragging an item, then call its onDrag method
      Draggable draggableMouseDownItem = DraggableActors.getMouseDownItem();
      if (draggableMouseDownItem != null) {
        draggableMouseDownItem.onDrag(windowX, windowY);
      }

      // ...If we stopped dragging an item, then call its dragFinished method
      if (events[i] == MOUSE_UP_EVENT) {
        if (draggableMouseDownItem != null) {
          draggableMouseDownItem.dragFinished(windowX, windowY);
          DraggableActors.clearMouseDownItem();
        }
      }

      // Handle mouse wheel
      Scrollable scrollable = ScrollableActors.getItemAt(windowX, windowY);
      if (scrollable != null) {
        if (mouse.isWheelUp()) {
          scrollable.onMouseWheelUp();
        } else if (mouse.isWheelDown()) {
          scrollable.onMouseWheelDown();
        }
      }
    }
  }
}
