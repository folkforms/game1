package game1.core.input;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents a keyboard attached to the system.
 */
abstract public class KeyboardInputDevice extends InputDevice {

  private static Map<Integer, Integer> arrayIndexToKeyCodes = new HashMap<>();
  private static Map<Integer, Integer> keyCodesToArrayIndex = new HashMap<>();
  private static int numKeys;
  static {
    populate();
  }

  protected KeyboardInputDevice() {
    super(numKeys);
  }

  /**
   * Populate the maps with the array indexes and the corresponding key code values.
   */
  private static void populate() {
    int index = 0;
    populateMaps(index++, Keys.KEY_0);
    populateMaps(index++, Keys.KEY_1);
    populateMaps(index++, Keys.KEY_2);
    populateMaps(index++, Keys.KEY_3);
    populateMaps(index++, Keys.KEY_4);
    populateMaps(index++, Keys.KEY_5);
    populateMaps(index++, Keys.KEY_6);
    populateMaps(index++, Keys.KEY_7);
    populateMaps(index++, Keys.KEY_8);
    populateMaps(index++, Keys.KEY_9);
    populateMaps(index++, Keys.KEY_A);
    populateMaps(index++, Keys.KEY_ADD);
    populateMaps(index++, Keys.KEY_APOSTROPHE);
    populateMaps(index++, Keys.KEY_APPS);
    populateMaps(index++, Keys.KEY_B);
    populateMaps(index++, Keys.KEY_BACKSPACE);
    populateMaps(index++, Keys.KEY_BACKSLASH);
    populateMaps(index++, Keys.KEY_C);
    populateMaps(index++, Keys.KEY_CAPSLOCK);
    populateMaps(index++, Keys.KEY_COMMA);
    populateMaps(index++, Keys.KEY_D);
    populateMaps(index++, Keys.KEY_DELETE);
    populateMaps(index++, Keys.KEY_DIVIDE);
    populateMaps(index++, Keys.KEY_DOWN_ARROW);
    populateMaps(index++, Keys.KEY_E);
    populateMaps(index++, Keys.KEY_END);
    populateMaps(index++, Keys.KEY_ENTER);
    populateMaps(index++, Keys.KEY_EQUALS);
    populateMaps(index++, Keys.KEY_ESCAPE);
    populateMaps(index++, Keys.KEY_F);
    populateMaps(index++, Keys.KEY_F1);
    populateMaps(index++, Keys.KEY_F2);
    populateMaps(index++, Keys.KEY_F3);
    populateMaps(index++, Keys.KEY_F4);
    populateMaps(index++, Keys.KEY_F5);
    populateMaps(index++, Keys.KEY_F6);
    populateMaps(index++, Keys.KEY_F7);
    populateMaps(index++, Keys.KEY_F8);
    populateMaps(index++, Keys.KEY_F9);
    populateMaps(index++, Keys.KEY_F10);
    populateMaps(index++, Keys.KEY_F11);
    populateMaps(index++, Keys.KEY_F12);
    populateMaps(index++, Keys.KEY_F13);
    populateMaps(index++, Keys.KEY_F14);
    populateMaps(index++, Keys.KEY_F15);
    populateMaps(index++, Keys.KEY_F16);
    populateMaps(index++, Keys.KEY_F17);
    populateMaps(index++, Keys.KEY_F18);
    populateMaps(index++, Keys.KEY_F19);
    populateMaps(index++, Keys.KEY_FORWARD_SLASH);
    populateMaps(index++, Keys.KEY_G);
    populateMaps(index++, Keys.KEY_H);
    populateMaps(index++, Keys.KEY_HOME);
    populateMaps(index++, Keys.KEY_I);
    populateMaps(index++, Keys.KEY_INSERT);
    populateMaps(index++, Keys.KEY_J);
    populateMaps(index++, Keys.KEY_K);
    populateMaps(index++, Keys.KEY_L);
    populateMaps(index++, Keys.KEY_LEFT_BRACKET);
    populateMaps(index++, Keys.KEY_LEFT_CTRL);
    populateMaps(index++, Keys.KEY_LEFT_ARROW);
    populateMaps(index++, Keys.KEY_LEFT_ALT);
    populateMaps(index++, Keys.KEY_LEFT_SHIFT);
    populateMaps(index++, Keys.KEY_M);
    populateMaps(index++, Keys.KEY_MINUS);
    populateMaps(index++, Keys.KEY_MULTIPLY);
    populateMaps(index++, Keys.KEY_N);
    populateMaps(index++, Keys.KEY_NUMLOCK);
    populateMaps(index++, Keys.KEY_NUMPAD_0);
    populateMaps(index++, Keys.KEY_NUMPAD_1);
    populateMaps(index++, Keys.KEY_NUMPAD_2);
    populateMaps(index++, Keys.KEY_NUMPAD_3);
    populateMaps(index++, Keys.KEY_NUMPAD_4);
    populateMaps(index++, Keys.KEY_NUMPAD_5);
    populateMaps(index++, Keys.KEY_NUMPAD_6);
    populateMaps(index++, Keys.KEY_NUMPAD_7);
    populateMaps(index++, Keys.KEY_NUMPAD_8);
    populateMaps(index++, Keys.KEY_NUMPAD_9);
    populateMaps(index++, Keys.KEY_NUMPAD_DECIMAL);
    populateMaps(index++, Keys.KEY_NUMPAD_ENTER);
    populateMaps(index++, Keys.KEY_NUMPAD_EQUALS);
    populateMaps(index++, Keys.KEY_O);
    populateMaps(index++, Keys.KEY_P);
    populateMaps(index++, Keys.KEY_PAUSE);
    populateMaps(index++, Keys.KEY_PERIOD);
    populateMaps(index++, Keys.KEY_PAGE_UP);
    populateMaps(index++, Keys.KEY_PAGE_DOWN);
    populateMaps(index++, Keys.KEY_PRINT_SCREEN);
    populateMaps(index++, Keys.KEY_Q);
    populateMaps(index++, Keys.KEY_R);
    populateMaps(index++, Keys.KEY_RIGHT_BRACKET);
    populateMaps(index++, Keys.KEY_RIGHT_CTRL);
    populateMaps(index++, Keys.KEY_RIGHT_ARROW);
    populateMaps(index++, Keys.KEY_RIGHT_ALT);
    populateMaps(index++, Keys.KEY_RIGHT_SHIFT);
    populateMaps(index++, Keys.KEY_S);
    populateMaps(index++, Keys.KEY_SCROLL_LOCK);
    populateMaps(index++, Keys.KEY_SEMICOLON);
    populateMaps(index++, Keys.KEY_SPACE);
    populateMaps(index++, Keys.KEY_SUBTRACT);
    populateMaps(index++, Keys.KEY_T);
    populateMaps(index++, Keys.KEY_TAB);
    populateMaps(index++, Keys.KEY_U);
    populateMaps(index++, Keys.KEY_UP_ARROW);
    populateMaps(index++, Keys.KEY_V);
    populateMaps(index++, Keys.KEY_W);
    populateMaps(index++, Keys.KEY_X);
    populateMaps(index++, Keys.KEY_Y);
    populateMaps(index++, Keys.KEY_Z);
    numKeys = index;
  }

  /**
   * Adds an entry to the maps used to convert between array indexes and key codes.
   *
   * @param arrayIndex
   *          array index to map to key code
   * @param keyCode
   *          key code to map to array index
   */
  private static void populateMaps(int arrayIndex, int keyCode) {
    arrayIndexToKeyCodes.put(arrayIndex, keyCode);
    keyCodesToArrayIndex.put(keyCode, arrayIndex);
  }

  @Override
  boolean isActive(int item) {
    return states[keyCodesToArrayIndex.get(item)];
  };

  /**
   * Checks if any key is pressed.
   *
   * @return true if any key is pressed, false otherwise
   */
  public boolean isAnyKeyActive() {
    for (int i = 0; i < states.length; i++) {
      if (states[i]) {
        return true;
      }
    }
    return false;
  }

  @Override
  boolean wasActive(int item) {
    return previousStates[keyCodesToArrayIndex.get(item)];
  }

  @Override
  boolean isConsumed(int item) {
    return consumed[keyCodesToArrayIndex.get(item)];
  }

  @Override
  void setConsumed(int item) {
    consumed[keyCodesToArrayIndex.get(item)] = true;
  }

  @Override
  public void setState() {
    for (int i = 0; i < states.length; i++) {
      states[i] = getState(arrayIndexToKeyCodes.get(i)) == 1; // GLFW_PRESS
    }
  }

  abstract public int getState(int key);
}
