package game1.core.input;

public class KeyboardFactory {

  private static Keyboard keyboard = null;

  public static Keyboard getInstance() {
    if (keyboard == null) {
      keyboard = new Keyboard();
    }
    return keyboard;
  }
}
