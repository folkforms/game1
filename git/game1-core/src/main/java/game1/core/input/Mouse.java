package game1.core.input;

import java.util.Stack;
import java.util.function.Supplier;

import org.joml.Vector2f;

import game1.variables.VariablesFactory;

/**
 * Interface for the mouse.
 *
 * <p>
 * To create a custom cursor call {@code setCursorSupplier(Supplier<Cursor> cursorSupplier)} in
 * {@code UserState#apply()}.
 * </p>
 */
public class Mouse {

  // FIXME MOUSE I think we can just use cursor visible/invisible, where invisible == mouse camera,
  // and it just depends on whether someone is processing Game1.MOUSE_MOVE events.

  // How should the mouse be used at this particular time?
  public static final int MOUSE_OFF = 0;
  public static final int MOUSE_CURSOR = 1;
  public static final int MOUSE_CAMERA = 2;
  private boolean isMouseCameraOn = false;

  public static final int SCROLL_WHEEL_UP = -1;
  public static final int SCROLL_WHEEL_DOWN = -2;
  public static final int LEFT_BUTTON = 0;
  public static final int RIGHT_BUTTON = 1;
  public static final int MIDDLE_BUTTON = 2;
  public static final int BUTTON_1 = 0;
  public static final int BUTTON_2 = 1;
  public static final int BUTTON_3 = 2;
  public static final int BUTTON_4 = 3;
  public static final int BUTTON_5 = 4;
  public static final int BUTTON_6 = 5;
  public static final int BUTTON_7 = 6;
  public static final int BUTTON_8 = 7;

  private MouseInputDevice inputDevice;
  private Cursor cursor;
  private Supplier<Cursor> cursorSupplier = () -> Cursor.createMouseCursor();
  private boolean visible = true;
  private Stack<Boolean> previousCursorVisibility = new Stack<>();
  private Stack<Boolean> previousMouseCamera = new Stack<>();

  public Mouse() {
    this.inputDevice = (MouseInputDevice) InputDeviceRegistry.getDevice("mouse");
  }

  // Get the mouse position in the window. Ignores resolution and viewport.
  public Vector2f getWindowPos() {
    return inputDevice.getWindowPos();
  }

  /**
   * Gets whether the given button is currently down or not.
   *
   * @param button
   *          the button to check
   * @return <code>true</code> of the button is down, <code>false</code> otherwise
   */
  public boolean isButtonDown(int button) {
    return inputDevice.isActive(button);
  }

  public boolean isWheelUp() {
    return inputDevice.isActive(SCROLL_WHEEL_UP);
  }

  public boolean isWheelDown() {
    return inputDevice.isActive(SCROLL_WHEEL_DOWN);
  }

  public void stashMouseSettings() {
    previousCursorVisibility.push(cursor.isVisible());
    previousMouseCamera.push(isMouseCameraOn);
  }

  public void unstashMouseSettings() {
    // We can sometimes unstash on an empty stash when changing states
    if (hasStashedMouseSettings()) {
      setVisible(previousCursorVisibility.pop());
      setMouseCamera(previousMouseCamera.pop());
    }
  }

  public boolean hasStashedMouseSettings() {
    return previousCursorVisibility.size() > 0;
  }

  public void clearStashedMouseSettings() {
    previousCursorVisibility.clear();
    previousMouseCamera.clear();
  }

  Cursor getCursor() {
    return cursor;
  }

  public void setCursorSupplier(Supplier<Cursor> cursorSupplier) {
    this.cursorSupplier = cursorSupplier;
  }

  public void centerCursor() {
    inputDevice.centerCursor();
  }

  public void setVisible(boolean newValue) {
    visible = newValue;
    if (!VariablesFactory.getInstance().getAsBoolean("game1.dev.dev_tools_visible")) {
      cursor.setVisible(newValue);
    } else {
      previousCursorVisibility.push(newValue);
    }
  }

  public void setMouseCamera(boolean newValue) {
    if (!VariablesFactory.getInstance().getAsBoolean("game1.dev.dev_tools_visible")) {
      isMouseCameraOn = newValue;
    } else {
      previousMouseCamera.push(newValue);
    }
  }

  public void internal_forceVisible(boolean newValue) {
    cursor.setVisible(newValue);
  }

  public void internal_forceMouseCamera(boolean newValue) {
    isMouseCameraOn = newValue;
  }

  public boolean isMouseCameraOn() {
    return isMouseCameraOn;
  }

  public Cursor internal_createCursor() {
    cursor = cursorSupplier.get();
    centerCursor();
    cursor.setVisible(visible);
    return cursor;
  }

  public boolean debug_isCursorVisible() {
    return cursor.isVisible();
  }

  public Cursor debug_getCursor() {
    return cursor;
  }
}
