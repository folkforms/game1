package game1.core.input;

public class MouseFactory {

  private static Mouse mouse = null;

  public static Mouse getInstance() {
    if (mouse == null) {
      mouse = new Mouse();
    }
    return mouse;
  }

  public static void setTestInstance(Mouse testMouse) {
    mouse = testMouse;
  }
}
