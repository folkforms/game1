package game1.core.input;

import org.joml.Vector2f;
import org.joml.Vector3f;

import game1.actors.Actor;
import game1.core.engine.Engine;
import game1.core.graphics.Drawable;
import game1.datasets.GameDataFactory;

/**
 * The cursor image. For custom cursors see {@code Mouse} javadoc.
 */
public class Cursor implements Actor {

  private final int z = Integer.MAX_VALUE;
  private Drawable drawable;
  private boolean visible = true;
  private Vector2f drawableOffset;
  private Vector2f styleOffset = new Vector2f(0, 0);
  private Vector2f sceneOffset;

  public Cursor(Drawable drawable) {
    this.drawable = drawable;
    sceneOffset = new Vector2f(0, 0);
    setStyle(Style.TOP_LEFT);
    drawable.setName("MouseCursor");
    addChild(drawable);
  }

  public Vector2f getPosition() {
    Vector3f p = drawable.getPosition();
    return new Vector2f(p.x, p.y);
  }

  public void setPosition(Vector2f windowPos) {
    drawable.setPosition(windowPos.x + drawableOffset.x + styleOffset.x + sceneOffset.x,
        windowPos.y + drawableOffset.y + styleOffset.y + sceneOffset.y, z);
  }

  @Override
  public void setVisible(boolean newValue) {
    visible = newValue;
    drawable.setVisible(visible);
  }

  public boolean isVisible() {
    return visible;
  }

  public Drawable debug_getDrawable() {
    return drawable;
  }

  public void setStyle(Style style) {
    if (style == Style.TOP_LEFT) {
      drawableOffset = new Vector2f(0, -drawable.getHeight() * drawable.getScale());
      styleOffset = new Vector2f(0, 0);
    } else {
      drawableOffset = new Vector2f(0, 0);
      styleOffset = new Vector2f(-drawable.getWidth() / 2 * drawable.getScale(),
          -drawable.getHeight() / 2 * drawable.getScale());
    }
  }

  public enum Style {
    TOP_LEFT, CENTERED
  }

  public static Cursor createMouseCursor() {
    Drawable cursorDrawable = GameDataFactory.getInstance()
        .get(Engine.PROPERTIES.getProperty("game1.default_cursor.sprite"));
    return new Cursor(cursorDrawable);
  }
}
