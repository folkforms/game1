package game1.core.input;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import folkforms.log.Log;
import game1.core.engine.Engine;
import game1.debug.DebugScopes;

/**
 * Manages user-defined sets of key bindings. These key bindings can be named, e.g. "main menu",
 * "level 1". Multiple sets of key bindings can be active at the same time.
 */
public class KeyBindingsManager {

  private Map<String, KeyBindings> keyBindingsMap = new HashMap<>();
  private List<KeyCommand> active = new CopyOnWriteArrayList<>();
  private List<String> activeNames = new CopyOnWriteArrayList<>();
  private List<String> stash = new ArrayList<>();
  private boolean keyBindingsChanged = false;

  /**
   * Register a set of key bindings for later activation.
   *
   * @param name
   *          name of the set of key bindings
   * @param keyBindings
   *          key bindings object
   */
  public void register(String name, KeyBindings keyBindings) {
    KeyBindings previous = keyBindingsMap.put(name, keyBindings);
    if (previous != null) {
      String message = String.format(
          "Keybindings named '%s' already in use for '%s', cannot re-use for '%s'", name, previous,
          keyBindings);
      Log.error(message);
      throw new RuntimeException(message);
    }
  }

  /**
   * Activates a set of key bindings. Note that multiple sets of key bindings can be active at the
   * same time.
   *
   * @param name
   *          the name of the set of key bindings to activate
   */
  public void activate(String... names) {
    Log.scoped(DebugScopes.KEYBINDINGS, "Activating keybindings '%s'", Arrays.toString(names));
    for (String name : names) {
      if (activeNames.contains(name)) {
        continue;
      }
      KeyBindings keyBindings = keyBindingsMap.get(name);
      if (keyBindings == null) {
        throw new RuntimeException(String
            .format("Error: KeyBindings '%s' not found. Did you forget to register it?", name));
      }
      List<KeyCommand> keyCommands = keyBindings.listKeyCommands();
      active.addAll(keyCommands);
      active.sort(new KeyCommandComparator());
      activeNames.add(name);
    }

    keyBindingsChanged = true;
  }

  /**
   * Deactivates a set of key bindings.
   *
   * @param name
   *          the name of the set of key bindings to deactivate
   */
  public void deactivate(String... names) {
    for (String name : names) {
      KeyBindings keyBindings = keyBindingsMap.get(name);
      if (keyBindings == null) {
        throw new RuntimeException(String
            .format("Error: KeyBindings '%s' not found. Did you forget to register it?", name));
      }
      List<KeyCommand> keyCommands = keyBindings.listKeyCommands();
      active.removeAll(keyCommands);
      activeNames.remove(name);
      activate("dev");
    }

    keyBindingsChanged = true;
  }

  /**
   * Deactivates all active key bindings.
   */
  public void clearAllActive() {
    active.clear();
    activeNames.clear();
    activate("dev");
    keyBindingsChanged = true;
  }

  /**
   * Lists the full set of active key bindings.
   *
   * @return all active key bindings
   */
  List<KeyCommand> listActiveKeyCommands() {
    return active;
  }

  /**
   * Stashes the current set of active key bindings and clears everything.
   *
   * @see #stashPop()
   */
  public void stashSave() {
    stash = new ArrayList<>(activeNames);
    clearAllActive();
  }

  /**
   * Restores the stashed set of key bindings and clears the stash.
   *
   * @see #stashSave()
   */
  public void stashPop() {
    clearAllActive();
    for (String s : stash) {
      activate(s);
    }
    stash.clear();
  }

  /**
   * Checks if the active KeyBindings have changed since the last poll, returns the value and resets
   * it to false.
   *
   * <p>
   * If {@link #activate(String...)}, {@link #clearAllActive()} or {@link #deactivate(String...)}
   * were called by one of the input devices then the list may no longer be valid. In that case we
   * abort processing of the KeyBindings. We will get a fresh list next update.
   * </p>
   *
   * @return <code>true</code> if the active KeyBindings have changed since the last poll,
   *         <code>false</code> otherwise
   */
  public boolean pollKeyBindingsChanged() {
    boolean value = keyBindingsChanged;
    keyBindingsChanged = false;
    return value;
  }

  /**
   * Debug method to get the current stash.
   *
   * @return the current stash
   */
  public List<String> debug_listStash() {
    return stash;
  }

  /**
   * Debug method to list all active names.
   *
   * @return list of all active names
   */
  public List<String> debug_listActiveNames() {
    return activeNames;
  }

  /**
   * Debug method to get a given key command.
   *
   * @param keyCodes
   *          key codes to search for
   * @return key command, or null if not found
   */
  public KeyCommand debug_getKeyCommand(int[] keyCodes) {
    outer: for (int i = 0; i < active.size(); i++) {
      KeyCommand tempKeyCommand = active.get(i);
      int[] tempKeyCodes = tempKeyCommand.getKeyCodes();
      if (tempKeyCodes.length != keyCodes.length) {
        continue outer;
      }
      for (int j = 0; j < tempKeyCodes.length; j++) {
        if (tempKeyCodes[j] != keyCodes[j]) {
          continue outer;
        }
      }
      return tempKeyCommand;
    }
    return null;
  }

  /**
   * Debug method to get a given key command.
   *
   * @param key
   *          key name from the game properties
   * @return key command, or null if not found
   */
  public KeyCommand debug_getKeyCommand(String key) {
    return debug_getKeyCommand(Engine.PROPERTIES.getKeyboardKeysProperty(key));
  }
}
