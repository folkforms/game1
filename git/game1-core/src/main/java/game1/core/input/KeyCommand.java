package game1.core.input;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import folkforms.log.Log;
import game1.core.engine.Engine;
import game1.events.Command;
import game1.events.RaiseEventCommand;
import game1.variables.VariablesFactory;

/**
 * KeyCommand links a set of key codes to a command or event which will be executed when the given
 * keys are pressed.
 */
public class KeyCommand {

  /**
   * Key commands set to NO_BLOCK will execute every tick. They can be slowed down by setting
   * {@link #tickRate(int)}.
   */
  public static final String NO_BLOCK = "NO_BLOCK";

  /**
   * Key commands set to BLOCK will only execute once per key press.
   */
  public static final String BLOCK = "BLOCK";

  /**
   * Key commands set to DELAY_REPEAT will execute once, then pause for a short while, then start
   * repeating as a given rate.
   */
  public static final String DELAY_REPEAT = "DELAY_REPEAT";

  private int[] keyCodes = new int[0];
  private int[] mouseCodes = new int[0];
  private int[] controllerCodes = new int[0];
  private List<Command> commands = new ArrayList<>();
  private String blockType;
  private int tickRate = 1;
  private long lastTick = 0;
  private long repeatNumber = 0;
  private String name = "UNKNOWN";

  /**
   * Private constructor used by {@link #init()}.
   */
  private KeyCommand() {
    this.blockType = BLOCK;
  }

  /**
   * Creates a new {@link KeyCommand} object for building. Defaults to {@link #BLOCK}.
   *
   * @return the KeyCommand object
   */
  public static KeyCommand init() {
    return new KeyCommand();
  }

  /**
   * Creates a {@link KeyCommand} object with the given key codes. Use {@link #block()},
   * {@link #noBlock()}, {@link #delayRepeat()}, and {@link #event(String, Object...)} or
   * {@link #command(Command command)} to set the other properties.
   *
   * @param keyCodes
   *          key codes to use for this key command
   * @return the KeyCommand object
   */
  public KeyCommand keys(int... keyCodes) {
    this.keyCodes = keyCodes;
    return this;
  }

  /**
   * Creates a {@link KeyCommand} object with the keys taken from the given property. The property
   * should be a key string like "A" or "KEY_A".
   *
   * @param property
   *          property containing keys to use for this key command
   * @return the KeyCommand object
   * @see {@link KeyboardUtils#convert(String)}
   */
  public KeyCommand property(String property) {
    this.keyCodes = Engine.PROPERTIES.getKeyboardKeysProperty(property);
    return this;
  }

  /**
   * Creates a {@link KeyCommand} object with the keys taken from the given variable. The property
   * should be a key string like "f1" or "KEY_A".
   *
   * @param property
   *          property containing keys to use for this key command
   * @return the KeyCommand object
   * @see {@link KeyboardUtils#convert(String)}
   */
  public KeyCommand keys(String property) {
    String value = VariablesFactory.getInstance().get(property);
    try {
      int[] keyCodes = KeyboardUtils.convert(value);
      this.keyCodes = keyCodes;
      return this;
    } catch (NullPointerException ex) {
      Log.error("Unable to parse variable '%s' with value '%s' as keyboard keys.", property, value);
      Thread.dumpStack();
      System.exit(1);
      return null;
    }
  }

  /**
   * Creates a {@link KeyCommand} object with the given mouse codes. Use {@link #block()},
   * {@link #noBlock()}, {@link #delayRepeat()}, and {@link #event(String, Object...)} or
   * {@link #command(Command command)} to set the other properties.
   *
   * @param mouseCodes
   *          mouse codes to use for this key command
   * @return the KeyCommand object
   */
  public KeyCommand mouse(int... mouseCodes) {
    this.mouseCodes = mouseCodes;
    return this;
  }

  /**
   * Creates a {@link KeyCommand} object with the given controller codes. Use {@link #block()},
   * {@link #noBlock()}, {@link #delayRepeat()}, and {@link #event(String, Object...)} or
   * {@link #command(Command command)} to set the other properties.
   *
   * @param controllerCodes
   *          controller codes to use for this key command
   * @return the KeyCommand object
   */
  public KeyCommand controller(int... controllerCodes) {
    this.controllerCodes = controllerCodes;
    return this;
  }

  /**
   * Sets the block type of this key command to {@link #BLOCK}.
   *
   * @return the KeyCommand object
   */
  public KeyCommand block() {
    this.blockType = KeyCommand.BLOCK;
    return this;
  }

  /**
   * Sets the block type of this key command to {@link #NO_BLOCK}.
   *
   * @return the KeyCommand object
   */
  public KeyCommand noBlock() {
    this.blockType = KeyCommand.NO_BLOCK;
    return this;
  }

  /**
   * Sets the block type of this key command to {@link #DELAY_REPEAT}.
   *
   * @return the KeyCommand object
   */
  public KeyCommand delayRepeat() {
    this.blockType = KeyCommand.DELAY_REPEAT;
    return this;
  }

  /**
   * Sets the tickRate of this key command and sets the blockType to {@link #NO_BLOCK}
   *
   * @return the KeyCommand object
   */
  public KeyCommand tickRate(int tickRate) {
    this.tickRate = tickRate;
    this.blockType = KeyCommand.NO_BLOCK;
    return this;
  }

  /**
   * Sets the command of this key command object to raise an event of the given type with the given
   * data.
   *
   * @return the KeyCommand object
   */

  public KeyCommand event(String event, Object... data) {
    commands.add(new RaiseEventCommand(event, data));
    return this;
  }

  /**
   * Sets the command of this key command object to the given command.
   *
   * @return the KeyCommand object
   */
  public KeyCommand command(Command command) {
    commands.add(command);
    return this;
  }

  /**
   * Sets the name of this key command object to the given string. Used for debugging.
   *
   * @return the KeyCommand object
   */
  public KeyCommand name(String name) {
    this.name = name;
    return this;
  }

  @Override
  public String toString() {
    return String.format(
        "KeyCommand[name='%s',blockType=%s,keyCodes=%s (%s),mouseCodes=%s,controllerCodes=%s,tickRate=%s,lastTick=%s,repeatNumber=%s,command=%s]",
        name, blockType, Arrays.toString(keyCodes),
        Arrays.toString(KeyboardUtils.convert(keyCodes)), Arrays.toString(mouseCodes),
        Arrays.toString(controllerCodes), tickRate, lastTick, repeatNumber, commands);
  }

  /**
   * Gets the key codes that make up this KeyCommand.
   *
   * @return the key codes that make up this KeyCommand
   */
  int[] getKeyCodes() {
    return keyCodes;
  }

  /**
   * Gets the mouse codes that make up this KeyCommand.
   *
   * @return the mouse codes that make up this KeyCommand
   */
  int[] getMouseCodes() {
    return mouseCodes;
  }

  /**
   * Gets the controller codes that make up this KeyCommand.
   *
   * @return the controller codes that make up this KeyCommand
   */
  int[] getControllerCodes() {
    return controllerCodes;
  }

  /**
   * Gets the type of blocking for this KeyCommand.
   *
   * @return the type of blocking for this KeyCommand
   */
  String getBlockType() {
    return blockType;
  }

  /**
   * Gets the Command that this KeyCommand should execute.
   *
   * @return the Command that this KeyCommand should execute
   */
  List<Command> getCommands() {
    return commands;
  }

  /**
   * Gets the tickRate for this KeyCommand.
   *
   * @return the tickRate for this KeyCommand
   */
  int getTickRate() {
    return tickRate;
  }

  /**
   * Gets the last tick that this KeyCommand ticked on.
   *
   * @return the last tick that this KeyCommand ticked on
   */
  long getLastTickTime() {
    return lastTick;
  }

  /**
   * Sets the last tick that this KeyCommand ticked on.
   *
   * @param newTickTime
   *          the last tick that this KeyCommand ticked on
   */
  void setLastTickTime(long newTickTime) {
    lastTick = newTickTime;
  }

  /**
   * Gets the number of times this KeyCommand has repeated. Used for DELAY_REPEAT KeyCommands.
   *
   * @return the number of times this KeyCommand has repeated
   */
  long getRepeatNumber() {
    return repeatNumber;
  }

  /**
   * Debug version of {@link #getRepeatNumber()}.
   *
   * @return the number of times this KeyCommand has repeated
   */
  public long debug_getRepeatNumber() {
    return repeatNumber;
  }

  /**
   * Increments the number of times this KeyCommand has repeated
   */
  void incrementRepeatNumber() {
    repeatNumber++;
  }

  /**
   * Resets to zero the number of times this KeyCommand has repeated.
   */
  void resetRepeatNumber() {
    repeatNumber = 0;
  }
}
