package game1.core.input;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class InputDeviceRegistry {

  private static Map<String, InputDevice> inputDevices = new LinkedHashMap<>();
  private static List<InputDevice> inputDevicesAsList = new ArrayList<>();

  public static void register(String name, InputDevice device) {
    inputDevices.put(name, device);
    inputDevicesAsList.add(device);
  }

  public static List<InputDevice> listInputDevices() {
    return inputDevicesAsList;
  }

  public static InputDevice getDevice(String name) {
    return inputDevices.get(name);
  }
}
