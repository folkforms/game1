package game1.core.input;

/**
 * Represents an input device attached to the system: mouse, keyboard, controller, joystick, etc.
 *
 * <p>
 * The input device has a given number of states. These represent input controls on the device e.g.
 * buttons.
 * </p>
 */
abstract class InputDevice {

  protected int numStates = 0;
  protected boolean[] states;
  protected boolean[] previousStates;
  protected boolean[] consumed;

  /**
   * Create a new {@link InputDevice} with the given number of states.
   *
   * @param numStates
   */
  InputDevice(int numStates) {
    states = new boolean[numStates];
    previousStates = new boolean[numStates];
    consumed = new boolean[numStates];
  }

  boolean isActive(int item) {
    return states[item];
  }

  boolean wasActive(int item) {
    return previousStates[item];
  }

  boolean isConsumed(int item) {
    return consumed[item];
  }

  void setConsumed(int item) {
    consumed[item] = true;
  }

  void clearConsumed() {
    for (int i = 0; i < consumed.length; i++) {
      consumed[i] = false;
    }
  }

  /**
   * Copies <code>states</code> into <code>previousStates</code> so that we can compare the two
   * after setting the new state.
   */
  void backupState() {
    for (int i = 0; i < states.length; i++) {
      previousStates[i] = states[i];
    }
  }

  /**
   * Sets the new state of the input device.
   */
  abstract public void setState();

  /**
   * Empty method. Can be overridden to clear the state between updates if required.
   */
  void clearState() {
  }

  /**
   * Gets the number of states (i.e. input controls) on this device.
   *
   * @return the number of states on this device
   */
  int getNumStates() {
    return states.length;
  }
}
