package game1.core.input;

/**
 * Represents a controller attached to the system.
 */
abstract public class ControllerInputDevice extends InputDevice {

  protected ControllerInputDevice(int numStates) {
    super(numStates);
  }
}
