package game1.core.input;

/**
 * Interface for the keyboard.
 */
public class Keyboard {

  private KeyboardInputDevice inputDevice;

  public Keyboard() {
    this.inputDevice = (KeyboardInputDevice) InputDeviceRegistry.getDevice("keyboard");
  }

  /**
   * Gets whether the given key is currently pressed or not.
   *
   * @param key
   *          key code
   * @return <code>true</code> if the the given key is currently pressed, <code>false</code>
   *         otherwise
   */
  public boolean isKeyDown(int key) {
    return inputDevice.isActive(key);
  }

  /**
   * Gets whether any key is currently pressed.
   *
   * @param key
   *          key code
   * @return <code>true</code> if any key is currently pressed, <code>false</code> otherwise
   */
  public boolean anyKeyDown() {
    return inputDevice.isAnyKeyActive();
  }
}
