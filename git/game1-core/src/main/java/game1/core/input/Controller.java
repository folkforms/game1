package game1.core.input;

/**
 * Controller buttons.
 */
public class Controller {

  public static final int BUTTON_1 = 0; // PS2 Triangle
  public static final int BUTTON_2 = 1; // PS2 Circle
  public static final int BUTTON_3 = 2; // PS2 X
  public static final int BUTTON_4 = 3; // PS2 Square
  public static final int BUTTON_5 = 4; // Left trigger
  public static final int BUTTON_6 = 5; // Right trigger
  public static final int BUTTON_7 = 6; // Left bumper
  public static final int BUTTON_8 = 7; // Right bumper
  public static final int BUTTON_9 = 8; // Start
  public static final int BUTTON_10 = 9; // Select
  public static final int BUTTON_11 = 10;
  public static final int BUTTON_12 = 11;
  public static final int BUTTON_13 = 12;
  public static final int BUTTON_14 = 13;
  public static final int BUTTON_15 = 14;
  public static final int BUTTON_16 = 15;
  public static final int DPAD_UP = 16;
  public static final int DPAD_DOWN = 17;
  public static final int DPAD_LEFT = 18;
  public static final int DPAD_RIGHT = 19;

  // Alternate names for buttons
  public static final int PS2_TRIANGLE = BUTTON_1;
  public static final int PS2_CIRCLE = BUTTON_2;
  public static final int PS2_X = BUTTON_3;
  public static final int PS2_SQUARE = BUTTON_4;
  public static final int PS2_LTRIGGER = BUTTON_5;
  public static final int PS2_RTRIGGER = BUTTON_6;
  public static final int PS2_LBUMPER = BUTTON_7;
  public static final int PS2_RBUMPER = BUTTON_8;
  public static final int PS2_START = BUTTON_9;
  public static final int PS2_SELECT = BUTTON_10;

  // Alternate names for buttons
  public static final int SNES_X = BUTTON_1;
  public static final int SNES_A = BUTTON_2;
  public static final int SNES_B = BUTTON_3;
  public static final int SNES_Y = BUTTON_4;
  public static final int SNES_L = BUTTON_7;
  public static final int SNES_R = BUTTON_8;
  public static final int SNES_SELECT = BUTTON_5;
  public static final int SNES_START = BUTTON_6;
}
