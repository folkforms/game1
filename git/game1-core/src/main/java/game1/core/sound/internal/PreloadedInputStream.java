package game1.core.sound.internal;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Arrays;

/**
 * An input stream where all of the data is loaded from disk at the start.
 */
public class PreloadedInputStream extends InputStream {

  private byte[] buffer;
  private int index = 0;
  private int mark = 0;
  private String debug_path;

  public PreloadedInputStream(String path) throws IOException {
    buffer = Files.readAllBytes(new File(path).toPath());
    index = 0;
    mark = 0;
    debug_path = path;
  }

  public PreloadedInputStream(byte[] buffer, String debug_path) {
    this.buffer = Arrays.copyOf(buffer, buffer.length);
    this.index = 0;
    this.mark = 0;
    this.debug_path = debug_path;
  }

  @Override
  public PreloadedInputStream clone() {
    return new PreloadedInputStream(buffer, debug_path);
  }

  @Override
  public boolean markSupported() {
    return true;
  }

  @Override
  public synchronized void mark(int readlimit) {
    mark = index;
  }

  @Override
  public synchronized void reset() throws IOException {
    index = mark;
  }

  public void resetData() {
    index = 0;
    mark = 0;
  }

  @Override
  public int read() throws IOException {
    if (index == buffer.length) {
      return -1;
    } else {
      return buffer[index++] & 0xFF;
    }
  }

  public byte[] getAllData() {
    return buffer;
  }

  public String debug_getPath() {
    return debug_path;
  }
}
