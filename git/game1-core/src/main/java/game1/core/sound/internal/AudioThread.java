package game1.core.sound.internal;

/**
 * All audio files are played on separate threads. This class gives them common methods so we can
 * modify the sound in flight, e.g. change the volume or pause playback.
 */
public interface AudioThread extends MixerChannelListener {

  public void setVolume(float newVolume);

  public float getUnadjustedVolume();

  public void stopPlayback();

  public void pausePlayback();

  public void unpausePlayback();

  @Override
  default public void notifyVolumeChanged() {
    setVolume(getUnadjustedVolume());
  }
}
