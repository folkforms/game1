package game1.core.sound.internal;

public class OggThread extends Thread implements AudioThread {

  private OggPlayer oggPlayer;

  public void setOggPlayer(OggPlayer oggPlayer) {
    this.oggPlayer = oggPlayer;
  }

  @Override
  public void setVolume(float newVolume) {
    oggPlayer.setVolume(newVolume);
  }

  @Override
  public float getUnadjustedVolume() {
    return oggPlayer.getUnadjustedVolume();
  }

  @Override
  public void stopPlayback() {
    oggPlayer.setShouldStop(true);
  }

  @Override
  public void pausePlayback() {
    oggPlayer.setShouldPause(true);
  }

  @Override
  public void unpausePlayback() {
    oggPlayer.setShouldPause(false);
  }
}
