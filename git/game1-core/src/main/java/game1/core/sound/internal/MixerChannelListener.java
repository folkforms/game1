package game1.core.sound.internal;

/**
 * Classes implementing {@link MixerChannelListener} are notified of changes to a
 * {@link MixerChannel}'s volume and muted state.
 */
public interface MixerChannelListener {

  /**
   * Updates the volume of the current item based on the mixer's current volume.
   */
  public void notifyVolumeChanged();
}
