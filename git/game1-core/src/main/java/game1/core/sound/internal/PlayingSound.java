package game1.core.sound.internal;

import java.io.InputStream;

import folkforms.log.Log;
import game1.actors.Actor;
import game1.actors.Tickable;
import game1.core.sound.SoundBoard;

/**
 * Represents a sound that is currently playing. Sounds Methods such as {@link #play(float)} are
 * accessed through the {@link SoundBoard} class.
 *
 * @see {@link SoundBoard}
 */
public class PlayingSound implements Actor, Tickable {

  protected PreloadedInputStream inputStream;

  private float defaultVolume = 0;
  private float volume = 0;

  private int state = 0;
  private static final int PLAY_ONCE = 0;
  private static final int LOOP = 1;
  private static final int STOP = 2;
  private boolean isPlaying = false;
  private boolean isFading = false;
  private long fadeNumSteps = 0;
  private float fadeVolumeStepSize = 0;
  private String ref = null;

  private AudioThread thread;
  private String channel;

  public PlayingSound(PreloadedInputStream inputStream, String channel, float defaultVolume) {
    this.inputStream = inputStream;
    this.channel = channel;
    this.defaultVolume = defaultVolume;
  }

  public void play(String ref) {
    play(ref, defaultVolume);
  }

  public void play(String ref, float volume) {
    this.volume = volume;
    state = PLAY_ONCE;
    internal_play();
    this.ref = ref;
  }

  /**
   * Separates out the actual playing of the file, so that {@link #loop(float)} can call it without
   * setting state to PLAY_ONCE.
   */
  private void internal_play() {
    isPlaying = true;
    OggPlayer oggPlayer = new OggPlayer();
    thread = oggPlayer.play(this, volume);
  }

  public void loop(String ref) {
    loop(ref, defaultVolume);
  }

  public void loop(String ref, float volume) {
    this.volume = volume;
    state = LOOP;
    isPlaying = true;
    OggPlayer oggPlayer = new OggPlayer();
    oggPlayer.setLooped(true);
    thread = oggPlayer.play(this, volume);
    this.ref = ref;
  }

  public void pause() {
    thread.pausePlayback();
  }

  public void unpause() {
    thread.unpausePlayback();
  }

  public void stop() {
    state = STOP;
    isPlaying = false;
    thread.stopPlayback();
    if (ref != null) {
      Log.warn(
          "PlayingSound would remove '%s' from TempActors here but TempActors does not exist any more",
          ref);
    }
    // Engine.GAME_STATE.removeActor(this);
  }

  public void fade(long fadeTimeMs) {
    isFading = true;
    fadeNumSteps = fadeTimeMs / 10; // Divide by 10 because 100 ticks/second
    fadeVolumeStepSize = volume / fadeNumSteps;
    // Log.temp("fadeTimeMs = %s, fadeNumSteps = %s, fadeVolumeStepSize = %s", fadeTimeMs,
    // fadeNumSteps, fadeVolumeStepSize);
  }

  @Override
  public void onTick() {
    if (state == PLAY_ONCE && !isPlaying) {
      stop();
    }
    if (isFading) {
      if (fadeNumSteps > 0) {
        volume -= fadeVolumeStepSize;
        thread.setVolume(volume);
        fadeNumSteps--;
      } else {
        thread.setVolume(0);
        isFading = false;
        stop();
      }
    }
  }

  InputStream getInputStream() {
    return inputStream;
  }

  void setPlaying(boolean b) {
    this.isPlaying = b;
  }

  String getChannel() {
    return channel;
  }

  String debug_getPath() {
    return inputStream.debug_getPath();
  }
}
