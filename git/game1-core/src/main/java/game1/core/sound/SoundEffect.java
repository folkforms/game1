package game1.core.sound;

import java.io.IOException;

import folkforms.log.Log;
import game1.actors.Actor;
import game1.core.engine.Engine;
import game1.core.sound.internal.MixerChannel;
import game1.core.sound.internal.PlayingSound;
import game1.core.sound.internal.PreloadedInputStream;
import game1.debug.DebugScopes;

/**
 * A sound effect.
 */
public class SoundEffect implements Actor {

  protected String path;
  protected PreloadedInputStream inputStream;
  protected String channel;
  protected float volume;

  /**
   * Loads a sound effect from disk, associates it with a {@link MixerChannel} and sets the volume.
   *
   * @param path
   *          path to file
   * @param channel
   *          mixer channel the sound should be played on
   * @param volume
   *          volume of this sound
   * @return {@link SoundEffect} object
   * @throws IOException
   *           if an error occurs loading the file
   * @see MixerChannel
   */
  private SoundEffect(String path, byte[] bytes, String channel, float volume) throws IOException {
    if (!path.toLowerCase().endsWith(".ogg")) {
      throw new IOException(String.format(
          "Unsupported file format: '%s' for file '%s'. Only ogg files are currently supported.",
          path.substring(path.lastIndexOf('.')), path));
    }
    Log.scoped(DebugScopes.SOUND, "SoundEffect.<constructor>");
    Log.scoped(DebugScopes.SOUND, "    path = %s", path);
    this.path = path;
    if (bytes != null) {
      this.inputStream = new PreloadedInputStream(bytes, path);
    } else {
      // FIXME Old
      this.inputStream = new PreloadedInputStream(path);
    }
    this.channel = channel;
    this.volume = volume;
  }

  /**
   * Loads a sound effect from disk. It will be associated with 'master' channel and have a volume
   * of 1 (100%).
   *
   * @param path
   *          path to file
   * @return {@link SoundEffect} object
   * @throws IOException
   *           if an error occurs loading the file
   */
  // FIXME Old
  public SoundEffect(String path) throws IOException {
    this(path, null, "master", 1);
  }

  public SoundEffect(String path, byte[] bytes) throws IOException {
    this(path, bytes, "master", 1);
  }

  private void validateChannel() {
    MixerChannel c = Engine.MIXER.getChannel(channel);
    if (c == null) {
      throw new RuntimeException(String.format(
          "Tried to link SoundEffect '%s' to channel '%s' but no such channel exists. Did you forget to register the channel?",
          path, channel));
    }
  }

  public void setChannel(String channel) {
    this.channel = channel;
    validateChannel();
  }

  public void setVolume(float volume) {
    this.volume = volume;
  }

  /**
   * Creates an instance of {@link PlayingSound} for playing.
   *
   * @return a {@link PlayingSound} to play
   */
  PlayingSound createInstanceForPlaying() {
    return new PlayingSound(inputStream.clone(), channel, volume);
  }

  /**
   * Debug method to get the sound effect's volume.
   */
  public float debug_getVolume() {
    return volume;
  }
}
