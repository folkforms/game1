package game1.core.sound.internal;

import java.util.ArrayList;
import java.util.List;

/**
 * {@link MixerChannel}s (like "effects" or "music") each have their own volume and muted state
 * values.
 *
 * @see {@link game1.core.sound.Mixer Mixer}
 */
public class MixerChannel {

  /** Channel volume */
  private float volume = 1;

  /** Channel muted state */
  private boolean isMuted = false;

  /** A list of listeners who wish to be notified if this channel's volume or muted state changes */
  private List<MixerChannelListener> channelListeners = new ArrayList<>();

  /** Property name for loading/saving volume value */
  private String volumePropertyName;

  /** Property name for loading/saving muted state */
  private String mutedPropertyName;

  public MixerChannel(String volumePropertyName, String mutedPropertyName) {
    this.volumePropertyName = volumePropertyName;
    this.mutedPropertyName = mutedPropertyName;
  }

  /**
   * Gets the volume of the channel.
   *
   * @return channel volume
   */
  public float getVolume() {
    return volume;
  }

  /**
   * Sets the volume of the channel. Notifies any listeners that the volume has changed.
   *
   * @param newVolume
   *          new volume value
   */
  public void setVolume(float newVolume) {
    this.volume = newVolume;
    notifyListeners();
  }

  /**
   * Gets the muted state of this channel.
   *
   * @return <code>true</code> if this channel is muted, <code>false</code> otherwise
   */
  public boolean getMuted() {
    return isMuted;
  }

  /**
   * Sets the muted state of this channel.
   *
   * @param b
   *          new muted state
   */
  public void setMuted(boolean b) {
    isMuted = b;
    notifyListeners();
  }

  /**
   * Notifies all listers that this channels volume has changed. May also be called when the master
   * volume changes.
   */
  public void notifyListeners() {
    for (MixerChannelListener listener : channelListeners) {
      listener.notifyVolumeChanged();
    }
  }

  /**
   * Gets the volume property name of this channel.
   *
   * @return volume property name
   */
  public String getVolumePropertyName() {
    return volumePropertyName;
  }

  /**
   * Gets the muted state property name of this channel.
   *
   * @return muted state property name
   */
  public String getMutedPropertyName() {
    return mutedPropertyName;
  }

  public void internal_registerListener(MixerChannelListener listener) {
    channelListeners.add(listener);
  }

  public void internal_unregisterListener(MixerChannelListener listener) {
    channelListeners.remove(listener);
  }
}
