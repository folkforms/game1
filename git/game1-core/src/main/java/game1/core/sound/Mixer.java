package game1.core.sound;

import java.util.HashMap;
import java.util.Map;

import game1.core.engine.Engine;
import game1.core.sound.internal.MixerChannel;
import game1.core.sound.internal.MixerChannelListener;

/**
 * The mixer controls the volume of sound effects, music, etc. Players will typically interact with
 * the mixer through the game options.
 */
public class Mixer {

  private static Map<String, MixerChannel> channels = new HashMap<>();
  static {
    channels.put("master", new MixerChannel("master.volume", "master.muted"));
  }

  public Mixer() {
    for (MixerChannel channel : channels.values()) {
      channel.setVolume(Engine.PROPERTIES.getFloatProperty(channel.getVolumePropertyName()));
      channel.setMuted(Engine.PROPERTIES.getBooleanProperty(channel.getMutedPropertyName()));
    }
  }

  private void registerChannel(String channel, String volumeProperty, String mutedProperty) {
    MixerChannel previous = channels.put(channel, new MixerChannel(volumeProperty, mutedProperty));
    if (previous != null) {
      throw new RuntimeException(String.format(
          "Attempted to register mixer channel '%s' but it was already registered.", channel));
    }
    // Validate that required properties exist
    Engine.PROPERTIES.getFloatProperty(volumeProperty);
    Engine.PROPERTIES.getBooleanProperty(mutedProperty);
  }

  /**
   * Registers a new sound channel which provides independent volume and muted state values. The
   * channel will use the properties <code>&lt;channel&gt;.volume</code> and
   * <code>&lt;channel&gt;.muted</code> to get its initial state.
   *
   * @param channel
   *          the channel name
   */
  public void registerChannel(String channel) {
    String volumeProperty = String.format("%s.volume", channel);
    String mutedProperty = String.format("%s.muted", channel);
    registerChannel(channel, volumeProperty, mutedProperty);
  }

  /**
   * Gets the volume of the given channel.
   *
   * @param channel
   *          the channel whose volume we want
   * @return the volume of the given channel
   */
  public float getVolume(String channel) {
    return channels.get(channel).getVolume();
  }

  /**
   * Sets the volume of the given channel. Must be between 0 and 1.
   *
   * @param channel
   *          the channel whose volume we want to set
   * @param newVolume
   *          the new volume of the channel
   */
  public void setVolume(String channel, float newVolume) {
    if (newVolume < 0 || newVolume > 1) {
      throw new RuntimeException(
          String.format("Invalid volume '%s'. Volume must be between 0 and 1.", newVolume));
    }
    channels.get(channel).setVolume(newVolume);
    if (channel.equals("master")) {
      for (MixerChannel c : channels.values()) {
        c.notifyListeners();
      }
    }
  }

  /**
   * Gets a channel's muted state.
   *
   * @param channel
   *          channel name
   * @return the muted state of the channel
   */
  public boolean getMuted(String channel) {
    return channels.get(channel).getMuted();
  }

  /**
   * Sets a channel's muted state
   *
   * @param channel
   *          channel name
   * @param newMuted
   *          new muted state of the channel
   */
  public void setMuted(String channel, boolean newMuted) {
    channels.get(channel).setMuted(newMuted);
    if (channel.equals("master")) {
      for (MixerChannel c : channels.values()) {
        c.notifyListeners();
      }
    }
  }

  public MixerChannel getChannel(String channel) {
    return channels.get(channel);
  }

  public void internal_registerChannelListener(String channel, MixerChannelListener listener) {
    MixerChannel c = channels.get(channel);
    c.internal_registerListener(listener);
  }

  public void internal_unregisterChannelListener(String channel, MixerChannelListener listener) {
    MixerChannel c = channels.get(channel);
    c.internal_unregisterListener(listener);
  }
}
