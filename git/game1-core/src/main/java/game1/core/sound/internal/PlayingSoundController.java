package game1.core.sound.internal;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Controls all currently-playing AudioThreads. Can make an AudioThread (or all AudioThreads)
 * pause/unpause/stop/fade/etc.
 */
public class PlayingSoundController {

  private static List<AudioThread> audioThreads = new CopyOnWriteArrayList<>();

  /**
   * Registers an AudioThread.
   *
   * @param thread
   *          AudioThread to register
   */
  public static void registerAudioThread(AudioThread thread) {
    audioThreads.add(thread);
  }

  /**
   * Unregisters an AudioThread.
   *
   * @param thread
   *          AudioThread to unregister
   */
  public static void unregisterAudioThread(AudioThread thread) {
    audioThreads.remove(thread);
  }

  /**
   * Pauses all currently-playing AudioThreads.
   */
  public static void pauseAll() {
    for (AudioThread a : audioThreads) {
      a.pausePlayback();
    }
  }

  /**
   * Unpauses all currently-playing AudioThreads.
   */
  public static void unpauseAll() {
    for (AudioThread a : audioThreads) {
      a.unpausePlayback();
    }
  }

  /**
   * Stops all currently-playing AudioThreads.
   */
  public static void stopAll() {
    for (AudioThread a : audioThreads) {
      a.stopPlayback();
    }
  }

  /**
   * Fades out all currently-playing AudioThreads over the given time.
   *
   * @param fadeTimeMs
   *          fade time in milliseconds
   */
  public static void fadeAll(long fadeTimeMs) {
    // Log.temp("FIXME PlayingSoundController.fadeAll not implemented yet");
  }
}
