package game1.core.sound;

import java.util.HashMap;
import java.util.Map;

import folkforms.log.Log;
import game1.core.engine.Engine;
import game1.core.sound.internal.PlayingSound;
import game1.core.sound.internal.PlayingSoundController;
import game1.datasets.GameDataFactory;
import game1.resolutions.layers.ResolutionLayers;

/**
 * SoundBoard handles playing, looping, pausing, unpausing, fading and stopping sounds. This is the
 * default way a game should play audio.
 *
 * <p>
 * Playing a sound creates an instance of {@link PlayingSound}. This can be given a reference should
 * you wish to interact with the sound later, e.g. to stop the sound.
 * <p>
 *
 * FIXME Not 100% sure SoundBoard should handle stop/fade/pause/unpause. If it does then it should
 * handle volume as well. Sort-of... Mixer should handle volume.
 *
 * FIXME Also I think methods involving references like play(asset, ref, volume), stop(ref), etc.
 * should be marked as internal... only cutscenes use them (and sound test buttons, but that's a
 * special case.) A game is going to want to use play (non-ref versions), loop (ref version),
 * fadeAll, pauseAll (maybe)... what else?
 */
public class SoundBoard {

  private Map<String, PlayingSound> playingSounds = new HashMap<>();

  /**
   * Plays the given sound asset at the given volume and creates a reference <code>ref</code> to the
   * {@link PlayingSound} object that is created.
   *
   * @param asset
   *          asset name
   * @param ref
   *          reference to use
   * @param volume
   *          volume to use
   */
  public void play(String asset, String ref, float volume) {
    SoundEffect sound = GameDataFactory.getInstance().get(asset);
    if (sound != null) {
      PlayingSound clone = sound.createInstanceForPlaying();
      if (volume == -1) {
        clone.play(ref);
      } else {
        clone.play(ref, volume);
      }
      Log.warn("SoundBoard.play called but SoundBoard is still adding actors to the state");
      if (Engine.GAME_STATE.temp_allowStateChanges()) {
        Engine.GAME_STATE.addActor(clone, ResolutionLayers.WINDOW_LAYER);
      }
      playingSounds.put(asset, clone);
    }
  }

  /**
   * Plays the given sound asset at its default volume and creates a reference <code>ref</code> to
   * the {@link PlayingSound} object that is created.
   *
   * @param asset
   *          asset name
   * @param ref
   *          reference to use
   */
  public void play(String asset, String ref) {
    play(asset, ref, -1);
  }

  /**
   * Plays the given asset at its default volume.
   *
   * @param asset
   *          asset name
   */
  public void play(String asset) {
    play(asset, null);
  }

  /**
   * Plays the given asset at the given volume.
   *
   * @param asset
   *          asset name
   * @param volume
   *          volume to use
   */
  public void play(String asset, float volume) {
    play(asset, null, volume);
  }

  /**
   * Loops the given sound asset at the given volume. Also creates a reference <code>ref</code> to
   * the {@link PlayingSound} object that is created.
   *
   * @param asset
   *          asset name
   * @param ref
   *          reference to use
   * @param volume
   *          volume to use
   */
  public void loop(String asset, String ref, float volume) {
    SoundEffect sound = GameDataFactory.getInstance().get(asset);
    if (sound != null) {
      PlayingSound clone = sound.createInstanceForPlaying();
      if (volume == -1) {
        clone.loop(ref);
      } else {
        clone.loop(ref, volume);
      }
      Engine.GAME_STATE.addActor(clone, ResolutionLayers.WINDOW_LAYER);
      playingSounds.put(ref, clone);
    }
  }

  /**
   * Loops the given sound asset at its volume. Also creates a reference <code>ref</code> to the
   * {@link PlayingSound} object that is created.
   *
   * @param asset
   *          asset name
   * @param ref
   *          reference to use
   */
  public void loop(String asset, String ref) {
    loop(asset, ref, -1);
  }

  /**
   * Stops the sound with reference <code>ref</code>. It cannot be restarted.
   *
   * @param ref
   *          object reference
   * @see #pause(String)
   * @see #pauseAll()
   */
  public void stop(String ref) {
    PlayingSound playingSound = playingSounds.get(ref);
    if (playingSound != null) {
      playingSound.stop();
      // Engine.GAME_STATE.removeActor(playingSound);
    }
  }

  /**
   * Fades the given sound out over time.
   *
   * @param ref
   *          object reference
   * @param fadeTimeMs
   *          fade time in milliseconds
   */
  public void fade(String ref, long fadeTimeMs) {
    // FIXME DATASETS: This won't work, this will get a different copy of the sound we want to fade
    PlayingSound clone = (PlayingSound) GameDataFactory.getInstance().get(ref);
    if (clone != null) {
      clone.fade(fadeTimeMs);
    }
  }

  /**
   * Pauses all currently-playing sounds.
   */
  public void pauseAll() {
    PlayingSoundController.pauseAll();
  }

  /**
   * Unpauses all currently-playing sounds.
   */
  public void unpauseAll() {
    PlayingSoundController.unpauseAll();
  }

  /**
   * Stops all currently-playing sounds.
   */
  public void stopAll() {
    PlayingSoundController.stopAll();
  }

  /**
   * Fades out all currently-playing sounds.
   *
   * @param fadeTimeMs
   *          fade time in milliseconds
   */
  public void fadeAll(long fadeTimeMs) {
    PlayingSoundController.fadeAll(fadeTimeMs);
  }
}
