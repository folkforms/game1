package game1.core.sound.internal;

import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import com.jcraft.jogg.Packet;
import com.jcraft.jogg.Page;
import com.jcraft.jogg.StreamState;
import com.jcraft.jogg.SyncState;
import com.jcraft.jorbis.Block;
import com.jcraft.jorbis.Comment;
import com.jcraft.jorbis.DspState;
import com.jcraft.jorbis.Info;

import folkforms.log.Log;
import game1.core.engine.Engine;
import game1.debug.DebugScopes;

/*
 * Copyright &copy; Jon Kristensen, 2008.
 * All rights reserved.
 *
 * This is version 1.0 of this source code, made to work with JOrbis 1.x. The
 * last time this file was updated was the 15th of March, 2008.
 *
 * Version history:
 *
 * 1.0: Initial release.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *   * Neither the name of jonkri.com nor the names of its contributors may be
 *     used to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/**
 * Plays an Ogg Vorbis file.
 *
 * <p>
 * Adapted from John Kristensen's original code.
 * </p>
 */
public class OggPlayer {

  private PlayingSound sound;
  private float unadjustedVolume = 0;
  private boolean shouldStop = false;
  private boolean shouldPause = false;
  private boolean isLooped = false;

  private InputStream inputStream = null;
  byte[] buffer = null;
  int bufferSize = 2048;
  private SyncState joggSyncState = new SyncState();
  int count = 0;
  int index = 0;

  byte[] convertedBuffer;
  int convertedBufferSize;
  private SourceDataLine outputLine = null;
  private float[][][] pcmInfo;
  private int[] pcmIndex;
  private Packet joggPacket = new Packet();
  private Page joggPage = new Page();
  private StreamState joggStreamState = new StreamState();
  private DspState jorbisDspState = new DspState();
  private Block jorbisBlock = new Block(jorbisDspState);
  private Comment jorbisComment = new Comment();
  private Info jorbisInfo = new Info();

  public AudioThread play(PlayingSound sound, float volume) {
    this.sound = sound;
    this.inputStream = sound.getInputStream();
    this.unadjustedVolume = volume;
    OggThread thread = new OggThread() {
      @Override
      public void run() {
        sound.setPlaying(true);
        shouldStop = false;
        try {
          do {
            clearVariables();
            initializeJOrbis();
            if (readHeader()) {
              if (initializeSound()) {
                readBody();
              }
            }
            cleanUp();
            // Wait for sound to finish playing
            outputLine.drain();
          } while (isLooped && !shouldStop);
        } catch (Exception ex) {
          Log.scoped(DebugScopes.SOUND, "OggPlayer");
          Log.scoped(DebugScopes.SOUND, "    sound.debug_getPath() = %s", sound.debug_getPath());
          throw ex;
        }
        Engine.MIXER.internal_unregisterChannelListener(sound.getChannel(), this);
        PlayingSoundController.unregisterAudioThread(this);
        outputLine.close();
        sound.setPlaying(false);
      }
    };
    thread.setOggPlayer(this);
    Engine.MIXER.internal_registerChannelListener(sound.getChannel(), thread);
    PlayingSoundController.registerAudioThread(thread);
    thread.start();
    return thread;
  }

  private void clearVariables() {
    buffer = null;
    bufferSize = 2048;
    joggSyncState = new SyncState();
    count = 0;
    index = 0;

    convertedBuffer = null;
    convertedBufferSize = 0;
    outputLine = null;
    pcmInfo = null;
    pcmIndex = null;

    joggPacket = new Packet();
    joggPage = new Page();
    joggStreamState = new StreamState();
    jorbisDspState = new DspState();
    jorbisBlock = new Block(jorbisDspState);
    jorbisComment = new Comment();
    jorbisInfo = new Info();
  }

  private void initializeJOrbis() {
    long t0 = System.nanoTime();
    joggSyncState.init();
    joggSyncState.buffer(bufferSize);
    buffer = joggSyncState.data;
    long t1 = System.nanoTime();
    Log.scoped(DebugScopes.SOUND, "OggPlayer: initializeJOrbis took %s ms", (t1 - t0) / 1_000_000);
  }

  private boolean readHeader() {
    long t0 = System.nanoTime();
    boolean needMoreData = true;
    int packet = 1;
    while (needMoreData) {
      try {
        count = inputStream.read(buffer, index, bufferSize);
      } catch (IOException ex) {
        Log.error("Could not read from the input stream.");
        Log.error(ex);
      }
      joggSyncState.wrote(count);
      switch (packet) {
      case 1: {
        switch (joggSyncState.pageout(joggPage)) {
        case -1: {
          Log.error("There is a hole in the first packet data.");
          return false;
        }
        case 0: {
          break;
        }
        case 1: {
          joggStreamState.init(joggPage.serialno());
          joggStreamState.reset();
          jorbisInfo.init();
          jorbisComment.init();
          if (joggStreamState.pagein(joggPage) == -1) {
            Log.error("Error reading the first header page.");
            return false;
          }
          if (joggStreamState.packetout(joggPacket) != 1) {
            Log.error("Error reading the first header packet.");
            return false;
          }
          if (jorbisInfo.synthesis_headerin(jorbisComment, joggPacket) < 0) {
            Log.error("Error interpreting the first packet. Apparently it's not Vorbis data.");
            return false;
          }
          packet++;
          break;
        }
        }
        if (packet == 1) {
          break;
        }
      }
      // The code for the second and third packets
      case 2:
      case 3: {
        switch (joggSyncState.pageout(joggPage)) {
        case -1: {
          Log.error("There is a hole in the second or third packet data.");
          return false;
        }
        case 0: {
          break;
        }
        case 1: {
          joggStreamState.pagein(joggPage);
          switch (joggStreamState.packetout(joggPacket)) {
          case -1: {
            Log.error("There is a hole in the first packet data.");
            return false;
          }
          case 0: {
            break;
          }
          case 1: {
            jorbisInfo.synthesis_headerin(jorbisComment, joggPacket);
            packet++;
            if (packet == 4) {
              needMoreData = false;
            }
            break;
          }
          }
          break;
        }
        }
        break;
      }
      }
      index = joggSyncState.buffer(bufferSize);
      buffer = joggSyncState.data;
      if (count == 0 && needMoreData) {
        Log.error("Not enough header data was supplied.");
        return false;
      }
      if (count == -1) {
        Log.scoped(DebugScopes.SOUND, "OggPlayer: count was -1 so stopped reading header data");
        return true;
      }
    }
    long t1 = System.nanoTime();
    Log.scoped(DebugScopes.SOUND, "OggPlayer: readHeader took %s ms", (t1 - t0) / 1_000_000);
    return true;
  }

  private boolean initializeSound() {
    long t0 = System.nanoTime();
    convertedBufferSize = bufferSize * 2;
    convertedBuffer = new byte[convertedBufferSize];
    jorbisDspState.synthesis_init(jorbisInfo);
    long t1 = System.nanoTime();
    jorbisBlock.init(jorbisDspState);
    long t2 = System.nanoTime();
    int channels = jorbisInfo.channels;
    int rate = jorbisInfo.rate;
    AudioFormat audioFormat = new AudioFormat(rate, 16, channels, true, false);
    long t3 = System.nanoTime();
    DataLine.Info datalineInfo = new DataLine.Info(SourceDataLine.class, audioFormat,
        AudioSystem.NOT_SPECIFIED);
    long t4 = System.nanoTime();
    // FIXME Clean up all these errors: Use Log plus better error messages.
    if (!AudioSystem.isLineSupported(datalineInfo)) {
      Log.error("Audio output line is not supported.");
      return false;
    }
    long t5, t6, t7;
    try {
      t5 = System.nanoTime();
      outputLine = (SourceDataLine) AudioSystem.getLine(datalineInfo);
      t6 = System.nanoTime();
      outputLine.open(audioFormat);
      t7 = System.nanoTime();
    } catch (LineUnavailableException ex) {
      Log.error("The audio output line could not be opened due to resource restrictions.");
      Log.error(ex);
      return false;
    } catch (IllegalStateException ex) {
      Log.error("The audio output line is already open.");
      Log.error(ex);
      return false;
    } catch (SecurityException ex) {
      Log.error("The audio output line could not be opened due to security restrictions.");
      Log.error(ex);
      return false;
    }
    setVolume(unadjustedVolume);
    long t8 = System.nanoTime();
    outputLine.start();
    long t9 = System.nanoTime();
    pcmInfo = new float[1][][];
    pcmIndex = new int[jorbisInfo.channels];
    Log.scoped(DebugScopes.SOUND, "OggPlayer: initializeSound", hashCode());
    Log.scoped(DebugScopes.SOUND, "    t0 -> t1 = %s ms (jorbisDspState.synthesis_init)",
        (t1 - t0) / 1_000_000);
    Log.scoped(DebugScopes.SOUND, "    t1 -> t2 = %s ms (jorbisBlock.init)", (t2 - t1) / 1_000_000);
    Log.scoped(DebugScopes.SOUND, "    t2 -> t3 = %s ms (new AudioFormat)", (t3 - t2) / 1_000_000);
    Log.scoped(DebugScopes.SOUND, "    t3 -> t4 = %s ms (new DataLine.Info)",
        (t4 - t3) / 1_000_000);
    Log.scoped(DebugScopes.SOUND, "    t4 -> t5 = %s ms (AudioSystem.isLineSupported)",
        (t5 - t4) / 1_000_000);
    Log.scoped(DebugScopes.SOUND, "    t5 -> t6 = %s ms (AudioSystem.getLine)",
        (t6 - t5) / 1_000_000);
    Log.scoped(DebugScopes.SOUND, "    t6 -> t7 = %s ms (outputLine.open)", (t7 - t6) / 1_000_000);
    Log.scoped(DebugScopes.SOUND, "    t7 -> t8 = %s ms (outputLine.setVolume)",
        (t8 - t7) / 1_000_000);
    Log.scoped(DebugScopes.SOUND, "    t8 -> t9 = %s ms (outputLine.start)", (t9 - t8) / 1_000_000);
    return true;
  }

  private void readBody() {
    // Log.temp("readBody");
    boolean needMoreData = true;
    // Log.temp(" shouldStop = %s", shouldStop);
    while (needMoreData && !shouldStop) {
      switch (joggSyncState.pageout(joggPage)) {
      case -1: {
        // Log.temp(" case -1");
        // Ignore holes in the data
      }
      case 0: {
        // Log.temp(" case 0");
        break;
      }
      case 1: {
        // Log.temp(" case 1");
        joggStreamState.pagein(joggPage);
        if (joggPage.granulepos() == 0) {
          // Log.temp(" joggPage.granulepos() == 0");
          needMoreData = false;
          break;
        }
        processPackets: while (true) {
          // Log.temp("Sound: %s looping processPackets", sound.hashCode());
          checkForPause();
          if (shouldStop) {
            return;
          }
          switch (joggStreamState.packetout(joggPacket)) {
          case -1: {
            // Ignore holes in the data
          }
          case 0: {
            break processPackets;
          }
          case 1: {
            decodeCurrentPacket();
          }
          }
        }
        if (joggPage.eos() != 0) {
          // Log.temp(" joggPage.eos() != 0");
          needMoreData = false;
        }
      }
      }

      if (needMoreData) {
        // Log.temp(" needMoreData");
        index = joggSyncState.buffer(bufferSize);
        buffer = joggSyncState.data;
        // Log.temp(" index = %s", index);
        if (index == -1) {
          count = 0;
        } else {
          try {
            count = inputStream.read(buffer, index, bufferSize);
          } catch (Exception ex) {
            Log.error(ex);
            return;
          }
        }
        joggSyncState.wrote(count);
        // Log.temp(" joggSyncState.wrote(%s)", count);
        if (count == 0) {
          needMoreData = false;
        }
      }
    }
    // Log.temp("----");
  }

  private void decodeCurrentPacket() {
    int samples;
    if (jorbisBlock.synthesis(joggPacket) == 0) {
      jorbisDspState.synthesis_blockin(jorbisBlock);
    }
    int range;
    while ((samples = jorbisDspState.synthesis_pcmout(pcmInfo, pcmIndex)) > 0) {
      if (samples < convertedBufferSize) {
        range = samples;
      } else {
        range = convertedBufferSize;
      }
      for (int i = 0; i < jorbisInfo.channels; i++) {
        int sampleIndex = i * 2;
        for (int j = 0; j < range; j++) {
          int value = (int) (pcmInfo[0][i][pcmIndex[i] + j] * 32767);
          if (value > 32767) {
            value = 32767;
          }
          if (value < -32768) {
            value = -32768;
          }
          if (value < 0) {
            value = value | 32768;
          }
          convertedBuffer[sampleIndex] = (byte) (value);
          convertedBuffer[sampleIndex + 1] = (byte) (value >>> 8);
          sampleIndex += 2 * (jorbisInfo.channels);
        }
      }
      outputLine.write(convertedBuffer, 0, 2 * jorbisInfo.channels * range);
      jorbisDspState.synthesis_read(range);
    }
  }

  private void cleanUp() {
    joggStreamState.clear();
    jorbisBlock.clear();
    jorbisDspState.clear();
    jorbisInfo.clear();
    joggSyncState.clear();
    shouldStop = false;
    ((PreloadedInputStream) inputStream).resetData();
  }

  float getUnadjustedVolume() {
    return unadjustedVolume;
  }

  void setShouldStop(boolean shouldStop) {
    this.shouldStop = shouldStop;
    if (shouldStop) {
      this.isLooped = false;
    }

    if (outputLine != null) {
      // Set volume to zero immediately
      FloatControl gainControl = (FloatControl) outputLine
          .getControl(FloatControl.Type.MASTER_GAIN);
      gainControl.setValue(gainControl.getMinimum());
    }
  }

  void setVolume(float newUnadjustedVolume) {
    if (!shouldStop) {
      unadjustedVolume = newUnadjustedVolume;
      float master = Engine.MIXER.getVolume("master");
      String channelName = sound.getChannel();
      float channel = channelName.equals("master") ? 1 : Engine.MIXER.getVolume(channelName);
      float adjustedVolume = newUnadjustedVolume * channel * master;
      setAudibleVolume(adjustedVolume);
    }
  }

  private void setAudibleVolume(float volume) {
    try {
      FloatControl gainControl = (FloatControl) outputLine
          .getControl(FloatControl.Type.MASTER_GAIN);
      boolean isMuted = Engine.MIXER.getMuted("master")
          || Engine.MIXER.getMuted(sound.getChannel());
      float minimum = gainControl.getMinimum();
      if (isMuted) {
        gainControl.setValue(minimum);
      } else {
        float converted = convertPercentToDB(volume);
        if (converted < minimum) {
          converted = minimum;
        }
        gainControl.setValue(converted);
      }
    } catch (NullPointerException ex) {
      // Do nothing. If you change volume of a looped sound just as it has ended and is cleaning up
      // you will have the outputLine == null.
    }
  }

  private static float convertPercentToDB(float percent) {
    // Option 1. Use 6.2f and +18f
    // Option 2. Use 0.0f and +17f
    float MAX_LEVEL_DB = 0f;
    // dBVal = fMaxLevelDB + 20 * log(linVal)
    float db = MAX_LEVEL_DB + 17f * (float) Math.log(percent);
    return db;
  }

  void setShouldPause(boolean shouldPause) {
    this.shouldPause = shouldPause;
    if (shouldPause) {
      // Set volume to zero immediately
      FloatControl gainControl = (FloatControl) outputLine
          .getControl(FloatControl.Type.MASTER_GAIN);
      gainControl.setValue(gainControl.getMinimum());
    } else {
      setVolume(unadjustedVolume);
    }
  }

  private void checkForPause() {
    while (shouldPause) {
      try {
        Thread.sleep(10);
      } catch (InterruptedException ex) {
        // Do nothing
      }
    }
  }

  public void setLooped(boolean b) {
    this.isLooped = b;
  }
}
