package game1.core.sound2;

import static org.lwjgl.openal.AL10.alDistanceModel;
import static org.lwjgl.openal.ALC10.alcCloseDevice;
import static org.lwjgl.openal.ALC10.alcCreateContext;
import static org.lwjgl.openal.ALC10.alcDestroyContext;
import static org.lwjgl.openal.ALC10.alcMakeContextCurrent;
import static org.lwjgl.openal.ALC10.alcOpenDevice;
import static org.lwjgl.system.MemoryUtil.NULL;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.openal.AL;
import org.lwjgl.openal.AL11;
import org.lwjgl.openal.ALC;
import org.lwjgl.openal.ALCCapabilities;

import game1.core.engine.internal.Camera;
import game1.core.engine.internal.Transformation;

public class SoundManager {

  private long device;
  private long context;
  private SoundListener listener;
  private final List<SoundBuffer> soundBufferList;
  private final Map<String, SoundSource> soundSourceMap;
  private final Matrix4f cameraMatrix;

  public SoundManager() {
    soundBufferList = new ArrayList<>();
    soundSourceMap = new HashMap<>();
    cameraMatrix = new Matrix4f();
  }

  // FIXME SOUND: Throw a more specific exception type
  public void init() throws Exception {
    this.device = alcOpenDevice((ByteBuffer) null);
    if (device == NULL) {
      throw new IllegalStateException("Failed to open the default OpenAL device.");
    }
    ALCCapabilities deviceCaps = ALC.createCapabilities(device);
    this.context = alcCreateContext(device, (IntBuffer) null);
    if (context == NULL) {
      throw new IllegalStateException("Failed to create OpenAL context.");
    }
    alcMakeContextCurrent(context);
    AL.createCapabilities(deviceCaps);
    setAttenuationModel(AL11.AL_EXPONENT_DISTANCE);
  }

  public void addSoundSource(String name, SoundSource soundSource) {
    this.soundSourceMap.put(name, soundSource);
  }

  public SoundSource getSoundSource(String name) {
    return this.soundSourceMap.get(name);
  }

  public void playSoundSource(String name) {
    SoundSource soundSource = this.soundSourceMap.get(name);
    if (soundSource != null && !soundSource.isPlaying()) {
      soundSource.play();
    }
  }

  public void removeSoundSource(String name) {
    this.soundSourceMap.remove(name);
  }

  public void addSoundBuffer(SoundBuffer soundBuffer) {
    this.soundBufferList.add(soundBuffer);
  }

  public SoundListener getListener() {
    return this.listener;
  }

  public void setListener(SoundListener listener) {
    this.listener = listener;
  }

  public void updateListenerPosition(Camera gameCamera) {
    // FIXME SOUND: Need to properly arrange the sequence of create camera, create sound manager,
    // check for updates, etc.
    if (listener == null) {
      return;
    }

    // Update camera matrix with camera data
    Transformation.updateGenericViewMatrix(gameCamera.getPosition(), gameCamera.getRotation(),
        cameraMatrix);

    listener.setPosition(gameCamera.getPosition());
    Vector3f at = new Vector3f();
    cameraMatrix.positiveZ(at).negate();
    Vector3f up = new Vector3f();
    cameraMatrix.positiveY(up);
    listener.setOrientation(at, up);
  }

  public void setAttenuationModel(int model) {
    alDistanceModel(model);
  }

  public void cleanup() {
    for (SoundSource soundSource : soundSourceMap.values()) {
      soundSource.cleanup();
    }
    soundSourceMap.clear();
    for (SoundBuffer soundBuffer : soundBufferList) {
      soundBuffer.cleanup();
    }
    soundBufferList.clear();
    if (context != NULL) {
      alcDestroyContext(context);
    }
    if (device != NULL) {
      alcCloseDevice(device);
    }
  }
}
