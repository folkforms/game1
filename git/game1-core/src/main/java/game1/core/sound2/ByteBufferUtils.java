package game1.core.sound2;

import static org.lwjgl.BufferUtils.createByteBuffer;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.lwjgl.BufferUtils;

// FIXME SOUND: Do I have these utility methods already or similar ones?
// FIXME SOUND: Are all of these methods actually used?
public class ByteBufferUtils {

  // public static String loadResource(String fileName) throws Exception {
  // String result;
  // try (InputStream in = Class.forName(SoundUtils.class.getName()).getResourceAsStream(fileName);
  // Scanner scanner = new Scanner(in, "UTF-8")) {
  // result = scanner.useDelimiter("\\A").next();
  // }
  // return result;
  // }

  // public static List<String> readAllLines(String fileName) throws Exception {
  // List<String> list = new ArrayList<>();
  // try (BufferedReader br = new BufferedReader(new InputStreamReader(
  // Class.forName(SoundUtils.class.getName()).getResourceAsStream(fileName)))) {
  // String line;
  // while ((line = br.readLine()) != null) {
  // list.add(line);
  // }
  // }
  // return list;
  // }

  // public static boolean existsResourceFile(String fileName) {
  // boolean result;
  // try (InputStream is = SoundUtils.class.getResourceAsStream(fileName)) {
  // result = is != null;
  // } catch (Exception excp) {
  // result = false;
  // }
  // return result;
  // }

  // FIXME FILE_SYSTEM This is only used by the SoundBuffer from the book's 3D sound example, which
  // is not actually used at the minute. Either way, I will have to change the SoundBuffer to use
  // the new file system and thus this code will likely not be used.

  // FIXME Compare this to the version in
  // https://github.com/LWJGL/lwjgl3-demos/blob/8d47965d29b477c5b80f81da8ab3668f6c4b07c3/src/org/lwjgl/demo/util/IOUtils.java#L25
  public static ByteBuffer ioResourceToByteBuffer(String resource, int bufferSize)
      throws IOException {
    ByteBuffer buffer;

    // FIXME SOUND: If the file fails to load nothing happens but you later get a
    // NullPointerException. Need to check and throw a proper error. Maybe check that input stream
    // is not null?

    Path path = Paths.get(resource);
    if (Files.isReadable(path)) {
      try (SeekableByteChannel fc = Files.newByteChannel(path)) {
        buffer = BufferUtils.createByteBuffer((int) fc.size() + 1);
        while (fc.read(buffer) != -1) {
          // Wait...
          // FIXME SOUND: Empty loop, ugh. Maybe do it like they do just below? Or is there a
          // 'readAll' equivalent?
        }
      }
    } else {
      try (InputStream source = ByteBufferUtils.class.getResourceAsStream(resource);
          ReadableByteChannel rbc = Channels.newChannel(source)) {
        buffer = createByteBuffer(bufferSize);
        while (true) {
          int bytes = rbc.read(buffer);
          if (bytes == -1) {
            break;
          }
          if (buffer.remaining() == 0) {
            buffer = resizeBuffer(buffer, buffer.capacity() * 2);
          }
        }
      }
    }

    buffer.flip();
    return buffer;
  }

  private static ByteBuffer resizeBuffer(ByteBuffer buffer, int newCapacity) {
    ByteBuffer newBuffer = BufferUtils.createByteBuffer(newCapacity);
    buffer.flip();
    newBuffer.put(buffer);
    return newBuffer;
  }
}
