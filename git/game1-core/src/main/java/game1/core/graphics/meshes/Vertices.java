package game1.core.graphics.meshes;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL15.GL_DYNAMIC_DRAW;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL45C.glCreateBuffers;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL45;

/**
 * Utility methods for VAOs and VBOs.
 */
public class Vertices {

  private static Map<String, Integer> sizes = new HashMap<>();
  static {
    sizes.put("float", 1);
    sizes.put("vec2", 2);
    sizes.put("vec3", 3);
    sizes.put("vec4", 4);
    sizes.put("ivec4", 4);
    sizes.put("mat4", 16);
  }

  private static Map<Integer, Integer> numAttribs = new HashMap<>();

  /**
   * Attaches a VBO to the given VAO at <code>attribIndex</code>.
   *
   * @param vao
   *          vao to use
   * @param attribIndex
   *          attribute index
   * @param data
   *          array of float data
   * @param type
   *          one of: float, vec2, vec3, vec4, ivec4, mat4
   */
  public static void attachFloatBuffer(int vao, int attribIndex, float[] data, String type) {
    attachFloatBuffer(vao, attribIndex, data, sizes.get(type), 0L, sizes.get(type) * 4);
  }

  private static void attachFloatBuffer(int vao, int attribIndex, float[] data, int size,
      long offsetBytes, int strideBytes) {
    ByteBuffer byteBuffer = toByteBuffer(data);
    createVBO(vao, attribIndex, byteBuffer, size, offsetBytes, strideBytes);
  }

  /**
   * Attaches a VBO to the given VAO at <code>attribIndex</code>.
   *
   * @param vao
   *          vao to use
   * @param attribIndex
   *          attribute index
   * @param data
   *          array of float data
   * @param type
   *          one of: float, vec2, vec3, vec4, ivec4, mat4
   */
  public static void attachIntBuffer(int vao, int attribIndex, int[] data, String type) {
    attachIntBuffer(vao, attribIndex, data, sizes.get(type), 0L, sizes.get(type) * 4);
  }

  private static void attachIntBuffer(int vao, int attribIndex, int[] data, int size,
      long offsetBytes, int strideBytes) {
    ByteBuffer byteBuffer = toByteBuffer(data);
    createIntVBO(vao, attribIndex, byteBuffer, size, offsetBytes, strideBytes);
  }

  private static ByteBuffer toByteBuffer(float[] data) {
    ByteBuffer byteBuffer = BufferUtils.createByteBuffer(data.length * 4);
    FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
    floatBuffer.put(data).flip();
    return byteBuffer;
  }

  /**
   * Converts an int[] to a ByteBuffer.
   *
   * @param data
   *          int[] to covert
   * @return a ByteBuffer containing the input data
   */
  public static ByteBuffer toByteBuffer(int[] data) {
    ByteBuffer byteBuffer = BufferUtils.createByteBuffer(data.length * 4);
    IntBuffer intBuffer = byteBuffer.asIntBuffer();
    intBuffer.put(data).flip();
    return byteBuffer;
  }

  private static void createVBO(int vao, int attribIndex, ByteBuffer bb, int size, long offsetBytes,
      int strideBytes) {
    int bindingIndex = attribIndex; // These are the same here
    int vbo = glCreateBuffers();
    GL45.glNamedBufferData(vbo, bb, GL_STATIC_DRAW);
    GL45.glVertexArrayVertexBuffer(vao, bindingIndex, vbo, offsetBytes, strideBytes);
    GL45.glVertexArrayAttribBinding(vao, attribIndex, bindingIndex);
    GL45.glVertexArrayAttribFormat(vao, attribIndex, size, GL_FLOAT, false, 0);
    GL45.glEnableVertexArrayAttrib(vao, attribIndex);

    Integer tempNumAttribs = numAttribs.get(vao);
    numAttribs.put(vao, tempNumAttribs == null ? 1 : tempNumAttribs + 1);
  }

  private static void createIntVBO(int vao, int attribIndex, ByteBuffer bb, int size,
      long offsetBytes, int strideBytes) {
    int bindingIndex = attribIndex; // These are the same here
    int vbo = glCreateBuffers();
    GL45.glNamedBufferData(vbo, bb, GL_STATIC_DRAW);
    GL45.glVertexArrayVertexBuffer(vao, bindingIndex, vbo, offsetBytes, strideBytes);
    GL45.glVertexArrayAttribBinding(vao, attribIndex, bindingIndex);
    GL45.glVertexArrayAttribIFormat(vao, attribIndex, size, GL_UNSIGNED_INT, 0);
    GL45.glEnableVertexArrayAttrib(vao, attribIndex);

    Integer tempNumAttribs = numAttribs.get(vao);
    numAttribs.put(vao, tempNumAttribs == null ? 1 : tempNumAttribs + 1);
  }

  /**
   * Creates and attaches a FloatBuffer to a vbo. The vbo will have its data formatted based on the
   * types provided, and these types will map to locations starting at <code>startLocation</code>.
   * The FloatBuffer will have a size equal to
   * <code>numInstances * (data size of combined types in floats)</code>.
   *
   * @param vao
   *          vao to use
   * @param startLocation
   *          the starting location within the vao locations
   * @param vbo
   *          vbo to use
   * @param numInstances
   *          the number of instances that this buffer will hold
   * @param types
   *          a list containing any mixture of: float, vec2, vec3, vec4, ivec4, mat4
   * @return a FloatBuffer of the appropriate size that has been linked to the vbo
   */
  public static FloatBuffer attachInstancedDataFloatBuffer(int vao, int startLocation, int vbo,
      int numInstances, String... types) {
    // Calculate instance size
    int instanceSizeFloats = 0;
    for (int i = 0; i < types.length; i++) {
      instanceSizeFloats += sizes.get(types[i]);
    }

    // Create float buffer and bind to the vbo
    ByteBuffer byteBuffer = BufferUtils.createByteBuffer(numInstances * instanceSizeFloats * 4);
    FloatBuffer floatBuffer = byteBuffer.asFloatBuffer();
    int bindingIndex = startLocation;
    GL45.glNamedBufferData(vbo, byteBuffer, GL_DYNAMIC_DRAW);

    // Bind the vbo to vao
    GL45.glVertexArrayVertexBuffer(vao, bindingIndex, vbo, 0L, instanceSizeFloats * 4);
    GL45.glVertexArrayBindingDivisor(vao, bindingIndex, 1);
    int location = startLocation;
    int relativeOffset = 0;

    // Create the bindings based on the given types
    for (int i = 0; i < types.length; i++) {
      if (types[i].equals("mat4")) {
        for (int j = 0; j < 4; j++) {
          GL45.glVertexArrayAttribBinding(vao, location + j, bindingIndex);
          GL45.glVertexArrayAttribFormat(vao, location + j, 4, GL_FLOAT, false, relativeOffset * 4);
          GL45.glEnableVertexArrayAttrib(vao, location + j);
          relativeOffset += 4;
        }
        location += 4;
      } else if (types[i].equals("float")) {
        GL45.glVertexArrayAttribBinding(vao, location, bindingIndex);
        GL45.glVertexArrayAttribFormat(vao, location, 1, GL_FLOAT, false, relativeOffset * 4);
        GL45.glEnableVertexArrayAttrib(vao, location);
        relativeOffset += 1;
        location += 1;
      } else if (types[i].equals("vec4")) {
        GL45.glEnableVertexArrayAttrib(vao, location);
        GL45.glVertexArrayAttribFormat(vao, location, 4, GL_FLOAT, false, relativeOffset * 4);
        GL45.glVertexArrayAttribBinding(vao, location, bindingIndex);
        relativeOffset += 4;
        location += 1;
      }
    }

    return floatBuffer;
  }

  /**
   * Gets the number of locations required to fit a list of types. Normally this is one location per
   * type, except for mat4s, which occupy 4 locations.
   *
   * @param types
   *          list of types
   * @return the number of locations required to fit the list of types
   */
  public static int countLocations(String[] types) {
    int total = 0;
    for (int i = 0; i < types.length; i++) {
      total += types[i].equals("mat4") ? 4 : 1;
    }
    return total;
  }

  /**
   * Gets the number of floats required to fit a list of types. Used for instanced rendering.
   *
   * @param types
   *          list of types
   * @return the number of floats required to fit the list of types
   */
  public static int countFloats(String[] types) {
    int total = 0;
    for (int i = 0; i < types.length; i++) {
      total = total + sizes.get(types[i]);
    }
    return total;
  }

  /**
   * Gets the number of floats required to fit a list of types. Used for instanced rendering. This
   * is equal to <code>{@link #countFloats(String[])} * 4</code>.
   *
   * @param types
   *          list of types
   * @return the number of bytes required to fit the list of types
   */
  public static int countBytes(String[] types) {
    return countFloats(types) * 4;
  }

  /**
   * Gets the number of vertex array attributes associated with a VAO. This is so we know how many
   * attributes to enable when rendering.
   *
   * @param vao
   *          the vao whose attribute count we want
   * @return the number of vertex array attributes associated with a VAO
   */
  public static int getNumAttribs(int vao) {
    return numAttribs.get(vao);
  }
}
