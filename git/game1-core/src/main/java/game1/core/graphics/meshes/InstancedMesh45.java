package game1.core.graphics.meshes;

import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL15.GL_DYNAMIC_DRAW;
import static org.lwjgl.opengl.GL31.glDrawElementsInstanced;
import static org.lwjgl.opengl.GL45C.glCreateBuffers;

import java.nio.FloatBuffer;

import org.joml.Matrix4f;
import org.joml.Vector4f;
import org.lwjgl.opengl.GL45;

import game1.core.engine.internal.Transformation;
import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.graphics.Material;
import game1.core.infrastructure.Texture;

public class InstancedMesh45 extends Mesh45 implements InstancedMesh {

  private static final int MAT4_SIZE_FLOATS = 16;
  private final int numInstances;
  private int scene3DInstancedVbo, billboardInstancedVbo;
  private FloatBuffer scene3DInstancedDataBuffer, billboardInstancedDataBuffer;
  private int scene3DInstanceSizeFloats, billboardInstanceSizeFloats;

  public InstancedMesh45(float[] positions, float[] textCoords, float[] normals, int[] indices,
      int[] jointIndices, float[] weights, int numInstances, String filename, Material material) {
    super(positions, textCoords, normals, indices, jointIndices, weights, filename, material);

    // Create a FloatBuffer attached to the VAO with locations created for the given data types
    this.numInstances = numInstances;
    createScene3DVBOs("mat4", "mat4", "float");
    createBillboardVBOs("mat4", "float", "vec4");
  }

  private void createScene3DVBOs(String... scene3DLayoutTypes) {
    int scene3DStartLocation = 5;
    scene3DInstancedVbo = glCreateBuffers();
    scene3DInstancedDataBuffer = Vertices.attachInstancedDataFloatBuffer(scene3DVao,
        scene3DStartLocation, scene3DInstancedVbo, this.numInstances, scene3DLayoutTypes);
    scene3DInstanceSizeFloats = Vertices.countFloats(scene3DLayoutTypes);
    scene3DVAAttribs += Vertices.countLocations(scene3DLayoutTypes);
  }

  private void createBillboardVBOs(String... billboardLayoutTypes) {
    int billboardStartLocation = 3;
    billboardInstancedVbo = glCreateBuffers();
    billboardInstancedDataBuffer = Vertices.attachInstancedDataFloatBuffer(billboardVao,
        billboardStartLocation, billboardInstancedVbo, this.numInstances, billboardLayoutTypes);
    billboardInstanceSizeFloats = Vertices.countFloats(billboardLayoutTypes);
    billboardVAAttribs += Vertices.countLocations(billboardLayoutTypes);
  }

  @Override
  public void cleanup() {
    super.cleanup();
    // FIXME CLEANUP
    // if (instanceDataBuffer != null) {
    // MemoryUtil.memFree(instanceDataBuffer);
    // instanceDataBuffer = null;
    // }
  }

  /**
   * Renders instanced meshes in a 3D scene
   *
   * @param transformation
   * @param viewMatrix
   * @param lightViewMatrix
   */
  @Override
  public void renderListInstanced(Transformation transformation, Matrix4f viewMatrix,
      Matrix4f lightViewMatrix) {
    initRender(scene3DVao, scene3DVAAttribs);

    int chunkSize = numInstances;
    int length = drawables.size();
    for (int i = 0; i < length; i += chunkSize) {
      int end = Math.min(length, i + chunkSize);
      renderChunkInstanced(i, end, transformation, viewMatrix, lightViewMatrix);
    }

    endRender(scene3DVao, scene3DVAAttribs);
  }

  private void renderChunkInstanced(int start, int end, Transformation transformation,
      Matrix4f viewMatrix, Matrix4f lightViewMatrix) {
    scene3DInstancedDataBuffer.clear();

    int numToRender = 0;
    for (int k = start; k < end; k++) {
      Drawable drawable = drawables.get(k);
      if (!drawable.isVisible() || !drawable.isInsideFrustum()) {
        continue;
      }

      // modelViewInstancedMatrix
      Matrix4f modelMatrix = transformation.buildModelMatrix(drawable.getRotation(),
          drawable.getPosition(), drawable.getScale());
      if (viewMatrix != null) {
        Matrix4f modelViewMatrix = transformation.buildModelViewMatrix(modelMatrix, viewMatrix);
        modelViewMatrix.get(scene3DInstanceSizeFloats * numToRender, scene3DInstancedDataBuffer);
      }
      // modelLightViewInstancedMatrix
      if (lightViewMatrix != null) {
        Matrix4f modelLightViewMatrix = transformation.buildModelLightViewMatrix(modelMatrix,
            lightViewMatrix);
        modelLightViewMatrix.get(scene3DInstanceSizeFloats * numToRender + MAT4_SIZE_FLOATS,
            scene3DInstancedDataBuffer);
      }
      // selectedInstanced
      int buffPos = scene3DInstanceSizeFloats * numToRender + MAT4_SIZE_FLOATS + MAT4_SIZE_FLOATS;
      scene3DInstancedDataBuffer.put(buffPos, drawable.isSelected() ? 1 : 0);

      numToRender++;
    }

    GL45.glNamedBufferData(scene3DInstancedVbo, scene3DInstancedDataBuffer, GL_DYNAMIC_DRAW);
    glDrawElementsInstanced(GL_TRIANGLES, vertexCount, GL_UNSIGNED_INT, 0, numToRender);
  }

  /**
   * Renders instanced billboards in a 3D scene.
   *
   * @param transformation
   * @param viewMatrix
   * @param lightViewMatrix
   */
  @Override
  public void renderBillboardListInstanced(Transformation transformation, Matrix4f viewMatrix,
      Matrix4f lightViewMatrix) {
    initRender(billboardVao, billboardVAAttribs);

    int chunkSize = numInstances;
    int length = drawables.size();
    for (int i = 0; i < length; i += chunkSize) {
      int end = Math.min(length, i + chunkSize);
      renderBillboardChunkInstanced(i, end, transformation, viewMatrix, lightViewMatrix);
    }

    endRender(billboardVao, billboardVAAttribs);
  }

  private void renderBillboardChunkInstanced(int start, int end, Transformation transformation,
      Matrix4f viewMatrix, Matrix4f lightViewMatrix) {
    billboardInstancedDataBuffer.clear();

    int numToRender = 0;
    for (int k = start; k < end; k++) {
      Drawable drawable = drawables.get(k);
      if (!drawable.isVisible()) {
        continue;
      }

      Matrix4f modelMatrix = transformation.buildModelMatrix(drawable.getRotation(),
          drawable.getPosition(), drawable.getScale());
      // modelViewInstancedMatrix
      if (viewMatrix != null) {
        viewMatrix.transpose3x3(modelMatrix);
        Matrix4f modelViewMatrix = transformation.buildModelViewMatrix(modelMatrix, viewMatrix);
        modelViewMatrix.scale(drawable.getScale());
        modelViewMatrix.get(billboardInstanceSizeFloats * numToRender,
            billboardInstancedDataBuffer);
      }

      // hasColour
      // modelLightViewInstancedMatrix is not used so we skip over a mat4-sized hole
      int buffPos = billboardInstanceSizeFloats * numToRender + MAT4_SIZE_FLOATS;
      Material m = drawable.internal_getMeshes()[0].getMaterial();
      float hasColor = m != null && m.getTexture() != null ? 0 : 1;
      billboardInstancedDataBuffer.put(buffPos, hasColor);

      // color
      if (hasColor > 0) {
        buffPos += 1;
        Colour colour = drawable.getColour();
        Vector4f v = colour != null ? colour : new Vector4f(1, 1, 1, 1);
        v.get(buffPos, billboardInstancedDataBuffer);
      }

      numToRender++;
    }

    GL45.glNamedBufferData(billboardInstancedVbo, billboardInstancedDataBuffer, GL_DYNAMIC_DRAW);
    glDrawElementsInstanced(GL_TRIANGLES, vertexCount, GL_UNSIGNED_INT, 0, numToRender);
  }

  @Override
  public InstancedMesh clone() {
    Texture texture = this.getMaterial().getTexture();
    InstancedMesh mesh = new InstancedMesh45(positions, textCoords, normals, indices, jointIndices,
        weights, numInstances, name, new Material(texture));
    return mesh;
  }

  @Override
  public boolean equals(Object other) {
    if (other == null) {
      return false;
    } else if (other instanceof InstancedMesh45) {
      InstancedMesh45 otherMesh = (InstancedMesh45) other;
      return id.equals(otherMesh.getId());
    } else {
      return false;
    }
  }
}
