package game1.core.graphics.meshes;

import java.util.List;

import org.joml.Matrix4f;

import game1.core.engine.internal.Transformation;
import game1.core.graphics.Drawable;
import game1.core.graphics.Material;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3ShaderProgram;
import game1.core.pipelines.GraphicsPipelineName;

public interface Mesh {

  public static final int MAX_WEIGHTS = 4;

  void render(GraphicsPipelineName pipeline);

  void resize(float width, float height);

  public Material getMaterial();

  public void setMaterial(Material material);

  public String getName();

  public void setName(String name);

  public float getWidth();

  public float getHeight();

  public void renderList(LWJGL3ShaderProgram shader, LWJGL3ShaderProgram sceneShaderProgram,
      Transformation transformation, Matrix4f viewMatrix, Matrix4f lightViewMatrix);

  public void cleanup();

  public void deleteBuffers();

  public Mesh clone();

  public void debug_dumpData();

  public void link(Drawable d);

  public void unlink(Drawable d);

  public List<Drawable> listDrawables();

  public float[] getPositions();

  public int[] getIndices();

  String getId();

  int getHumanReadableId();
}
