package game1.core.graphics.meshes;

import game1.core.engine.Game1RuntimeException;
import game1.core.graphics.Material;
import game1.core.infrastructure.Texture;
import game1.core.resources.builders.PrimitiveBuilder;
import game1.primitives.Rectangle;
import game1.variables.VariablesFactory;

/**
 * Utility methods for creating meshes. Used by {@link PrimitiveBuilder} to create 2D rectangles and
 * planes.
 *
 * <p>
 * Note: From 'Learn OpenGL Graphics Programming': "By default, triangles defined with
 * counter-clockwise vertices are processed as front-facing triangles." For this reason I've chosen
 * 'n'-shaped positions and indices of 0,2,1,0,3,2. This makes them counter-clockwise. Plus it's
 * important to have a standard for consistency, and it should match the OpenGL defaults so I don't
 * have to muck around with settings.
 * </p>
 *
 * <p>
 * 'n'-shaped positions, drawn as triangles 0,2,1 and 0,3,2.
 *
 * <pre>
* 1---2
* | / |
* 0---3
 * </pre>
 *
 * Note that textures are applied in a 'u'-shape. This is likely because their coordinate space has
 * an inverted y-axis.
 * </p>
 */
public class MeshUtils {

  private static MeshCache meshCache = new MeshCache();

  // 'n'-shaped positions, drawn as triangles 0,2,1 and 0,3,2
  // 1---2
  // | / |
  // 0---3
  public static final int[] INDICES = new int[] { 0, 2, 1, 0, 3, 2 };

  public static Mesh internal_createMesh(float[] positions, float[] texCoords, float[] normals,
      int[] indices, int[] jointIndices, float[] weights, int instances, Material material,
      String name) {
    return internal_createMesh(positions, texCoords, normals, indices, jointIndices, weights,
        instances, material, name, true);
  }

  public static Mesh internal_createMesh(float[] positions, float[] texCoords, float[] normals,
      int[] indices, int[] jointIndices, float[] weights, int instances, Material material,
      String name, boolean useMeshCache) {

    if (useMeshCache) {
      String id = MeshId.generate(MeshId.generateClassName(instances), positions, texCoords,
          normals, indices, jointIndices, weights, material);
      Mesh existingMeshMaybe = meshCache.get(id);
      if (existingMeshMaybe != null) {
        return existingMeshMaybe;
      }
    }

    int[] indices2 = indices != null ? indices : INDICES;
    Mesh mesh;
    // We can pass 0 to get a non-instanced mesh

    String openGLVersion = VariablesFactory.getInstance().get("game1.graphics.open_gl_version");
    if (openGLVersion.equals("3.3")) {
      if (instances > 0) {
        mesh = new InstancedMesh33(positions, texCoords, normals, indices2, jointIndices, weights,
            instances, name, material);
      } else {
        mesh = new Mesh33(positions, texCoords, normals, indices2, jointIndices, weights, name,
            material);
      }
    } else if (openGLVersion.equals("4.5")) {
      if (instances > 0) {
        mesh = new InstancedMesh45(positions, texCoords, normals, indices2, jointIndices, weights,
            instances, name, material);
      } else {
        mesh = new Mesh45(positions, texCoords, normals, indices2, jointIndices, weights, name,
            material);
      }
    } else {
      throw new Game1RuntimeException(
          "Unsupported OpenGL version %s. Only versions 4.5 and 3.3 are supported.", openGLVersion);
    }
    if (useMeshCache) {
      meshCache.add(mesh);
    }
    return mesh;
  }

  public static Mesh internal_createMesh(Texture texture, Rectangle sprite, boolean mirrored) {
    float spriteWidth = sprite.w;
    float spriteHeight = sprite.h;

    float[] positions = new float[] { //
        0, 0, 0, // 0
        0, spriteHeight, 0, // 1
        spriteWidth, spriteHeight, 0, // 2
        spriteWidth, 0, 0 // 3
    };
    float[] texCoords = !mirrored ? new float[] { 0, 1, 0, 0, 1, 0, 1, 1 }
      : new float[] { 1, 1, 1, 0, 0, 0, 0, 1 };

    float[] normals = new float[] { 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1 };
    Material material = new Material(texture);
    material.setSprite(sprite);

    String description = String.format(
        "MeshUtils.createMesh(textureId=%s,sprite=[%s,%s,%s,%s],mirrored=%s)",
        texture.getTextureId(), sprite.x, sprite.y, sprite.w, sprite.h, mirrored);
    return internal_createMesh(positions, texCoords, normals, INDICES, null, null, 1, material,
        description);
  }
}
