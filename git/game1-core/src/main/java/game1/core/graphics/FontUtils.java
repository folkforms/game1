package game1.core.graphics;

/**
 * Font utilities.
 */
public class FontUtils {

  public static RenderedFont DEFAULT_FONT_FACE;
  public static float DEFAULT_FONT_SIZE;

  /**
   * Gets the width of the string <code>text</code> rendered in <code>font</code> at a scale of
   * <code>scale</code>.
   *
   * @param text
   *          the text to measure
   * @param font
   *          font to use
   * @param scale
   *          font scale
   * @return the width of the string <code>text</code> rendered in <code>font</code> at a scale of
   *         <code>scale</code>
   */
  public static int getStringWidth(String text, RenderedFont font, float scale) {
    if (text.length() == 0) {
      return 0;
    }

    int width = 0;
    char[] charArray = text.toCharArray();
    for (int i = 0; i < charArray.length - 1; i++) {
      char c = charArray[i];
      if (c == ' ') {
        width += font.getSpaceWidth() * scale + font.getCharSpacing() * scale;
      } else {
        width += font.getRectangle(c).w * scale + font.getCharSpacing() * scale;
      }
    }
    width += font.getRectangle(charArray[charArray.length - 1]).w * scale;
    return width;
  }

  /**
   * Gets the width of the widest string in the given array when rendered in <code>font</code> at a
   * scale of <code>scale</code>.
   *
   * @param lines
   *          array of strings to check
   * @param font
   *          font to use
   * @param scale
   *          font scale
   * @return
   */
  public static int getMultiLineStringWidth(String[] lines, RenderedFont font, float scale) {
    int maxWidth = 0;
    for (String s : lines) {
      int lineWidth = getStringWidth(s, font, scale);
      if (lineWidth > maxWidth) {
        maxWidth = lineWidth;
      }
    }
    return maxWidth;
  }

  public static int getMultiLineStringHeight(String[] lines, RenderedFont font, float scale,
      int gap) {
    int lineHeight = getStringHeight(font, scale);
    return lines.length * lineHeight + lines.length * gap;
  }

  /**
   * Gets the height of the tallest character in <code>font</code> at a scale of <code>scale</code>.
   *
   * @param font
   *          the font to measure
   * @param scale
   *          the font scale
   * @return Gets the height of the tallest character of <code>font</code> when rendered at a scale
   *         of <code>scale</code>.
   */
  public static int getStringHeight(RenderedFont font, float scale) {
    return (int) ((font.getRectangle('M').h + font.getCharSpacing()) * scale);
  }
}
