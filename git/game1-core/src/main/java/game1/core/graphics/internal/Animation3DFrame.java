package game1.core.graphics.internal;

import java.util.Arrays;

import org.joml.Matrix4f;

/**
 * A single animation frame for a 3D mesh animation.
 */
public class Animation3DFrame {

  private static final Matrix4f IDENTITY_MATRIX = new Matrix4f();
  public static final int MAX_JOINTS = 200;
  private final Matrix4f[] jointMatrices;

  public Animation3DFrame() {
    jointMatrices = new Matrix4f[MAX_JOINTS];
    Arrays.fill(jointMatrices, IDENTITY_MATRIX);
  }

  public Matrix4f[] getJointMatrices() {
    return jointMatrices;
  }

  public void setMatrix(int pos, Matrix4f jointMatrix) {
    jointMatrices[pos] = jointMatrix;
  }
}
