package game1.core.graphics.meshes;

import folkforms.log.Log;
import game1.core.graphics.Colour;
import game1.core.graphics.Material;
import game1.core.resources.builders.PrimitiveBuilder;

public class PlaneUtils {

  /**
   * Creates a rectangular vertical mesh of the given size and colour.
   *
   * <p>
   * If you want a quick rectangle for prototyping something you should use {@link PrimitiveBuilder}
   * which wraps this class.
   * </p>
   *
   * @param width
   *          width of the mesh
   * @param height
   *          height of the mesh
   * @param colour
   *          colour of the mesh
   * @return a rectangular mesh of the given size and colour
   */
  public static Mesh createPlane2D(float width, float height, Colour colour) {

    if (width == 0 || height == 0) {
      Log.warn("Mesh.createMesh called with width %s and/or height %s of zero.", width, height);
    }

    float[] positions = new float[] { //
        0, 0, 0, // 0
        0, height, 0, // 1
        width, height, 0, // 2
        width, 0, 0 }; // 3
    float[] texCoords = new float[] { 0, 1, 0, 0, 1, 0, 1, 1 };
    float[] normals = new float[] { 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1 };

    Material material = new Material(colour, 1);

    String description = String.format("MeshUtils.createMesh(%s,%s,%s)", width, height, colour);
    return MeshUtils.internal_createMesh(positions, texCoords, normals, MeshUtils.INDICES, null,
        null, 1, material, description);
  }

  /**
   * Creates a plane that runs from 0,0,0 to x,y,z. Effectively x and y determine the size and z
   * determines the tilt. We are assuming that the line 0,0,0 to x,y,z bisects a rectangle, but this
   * means that the rectangle must be created on the x-y plane, i.e. you cannot create a rectangle
   * perpendicular to the X axis. If you want a rectangle perpendicular to the X axis you must
   * create one perpendicular to the Z axis and rotate it.
   *
   * @param x
   *          top-right x coordinate
   * @param y
   *          top-right y coordinate
   * @param z
   *          top-right z coordinate
   * @param colour
   *          colour of the mesh
   * @return a rectangular mesh
   */
  public static Mesh createPlane3D(float x, float y, float z, Colour colour) {

    float[] positions = new float[] { //
        0, 0, 0, // 0
        0, y, z, // 1
        x, y, z, // 2
        x, 0, 0 }; // 3
    float[] texCoords = new float[] { 0, 1, 0, 0, 1, 0, 1, 1 };
    float[] normals = new float[] { 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1 };

    Material material = new Material(colour, 1);

    String description = String.format("MeshUtils.createPlane(%s,%s,%s,%s)", x, y, z, colour);
    return MeshUtils.internal_createMesh(positions, texCoords, normals, MeshUtils.INDICES, null,
        null, 1, material, description);
  }
}
