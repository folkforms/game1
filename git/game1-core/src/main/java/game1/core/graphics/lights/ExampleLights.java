package game1.core.graphics.lights;

import org.joml.Vector3f;

import game1.core.registries.Lights;

public class ExampleLights {

  private static AmbientLight ambientLight;
  private static DirectionalLight directionalLight;

  public static void setLights() {
    // Ambient light
    ambientLight = new AmbientLight(new Vector3f(0.3f, 0.3f, 0.3f));

    // Point light
    Vector3f lightColour = new Vector3f(1, 1, 1);
    Vector3f lightPosition = new Vector3f(0, 0, 1);
    float lightIntensity = 1.0f;
    PointLight pointLight = new PointLight(lightColour, lightPosition, lightIntensity);
    PointLight.Attenuation att = new PointLight.Attenuation(0.0f, 0.0f, 1.0f);
    pointLight.setAttenuation(att);

    // Spot Light
    lightPosition = new Vector3f(0, 0.0f, 10f);
    pointLight = new PointLight(new Vector3f(1, 1, 1), lightPosition, lightIntensity);
    att = new PointLight.Attenuation(0.0f, 0.0f, 0.02f);
    pointLight.setAttenuation(att);
    Vector3f coneDir = new Vector3f(0, 0, -1);
    float cutoff = (float) Math.cos(Math.toRadians(140));
    Spotlight spotLight = new Spotlight(pointLight, coneDir, cutoff);

    // Directional light
    lightPosition = new Vector3f(-1, 0, 0);
    lightColour = new Vector3f(1, 1, 1);
    directionalLight = new DirectionalLight(lightColour, lightPosition, lightIntensity);

    // Set up Lights object
    Lights.setAmbientLight(ambientLight);
    Lights.setDirectionalLight(directionalLight);
    Lights.addPointLight(pointLight);
    Lights.addSpotlight(spotLight);
  }
}
