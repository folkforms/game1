package game1.core.graphics.lights;

import org.joml.Vector3f;

import game1.actors.Actor;

/**
 * Ambient light is a constant light in a scene that comes from all directions at once. Examples of
 * this are the moon and distant lights. This lighting constant always gives objects some colour.
 */
public class AmbientLight implements Actor, Light {

  private Vector3f colour;

  /**
   * Creates a new ambient light with the given colour.
   *
   * @param colour
   *          colour of the light
   */
  public AmbientLight(Vector3f colour) {
    this.colour = colour;
  }

  // /**
  // * Creates a new ambient light with the given colour.
  // *
  // * @param colour
  // * colour of the light
  // * @return a new ambient light with the given colour
  // */
  // public static AmbientLight create(Color colour) {
  // return new AmbientLight(colour.toVector3f());
  // }

  public AmbientLight(AmbientLight light) {
    this(new Vector3f(light.getColour()));
  }

  public Vector3f getColour() {
    return colour;
  }

  public void setColor(Vector3f colour) {
    this.colour = colour;
  }
}
