package game1.core.graphics;

import org.joml.Matrix3f;
import org.joml.Quaternionf;
import org.joml.Vector3f;

import game1.core.graphics.meshes.Mesh;
import game1.core.registries.CollisionMeshRegistry;

public class CollisionMeshUtils {

  /**
   * Draws a line from {@code startPosition} to a point {@targetPosition} that equals
   * {@code new Vector3f(startPosition.x + offsetX, startPosition.y + offsetY, startPosition.z + offsetZ)}.
   * If the line intersects with a collision mesh then it returns the point of intersection,
   * otherwise it returns the targetPosition.
   *
   * @param startPosition
   *          starting position
   * @param rotation
   *          rotation of the object
   * @param offsetX
   *          x movement
   * @param offsetY
   *          y movement
   * @param offsetZ
   *          z movement
   */
  public static Vector3f getAllowedMove(Vector3f startPosition, Vector3f rotation, float offsetX,
      float offsetY, float offsetZ) {
    Vector3f targetPosition = new Vector3f(startPosition.x, startPosition.y, startPosition.z);
    if (offsetZ != 0) {
      targetPosition.x += (float) Math.sin(Math.toRadians(rotation.y)) * -1.0f * offsetZ;
      targetPosition.z += (float) Math.cos(Math.toRadians(rotation.y)) * offsetZ;
    }
    if (offsetX != 0) {
      targetPosition.x += (float) Math.sin(Math.toRadians(rotation.y - 90)) * -1.0f * offsetX;
      targetPosition.z += (float) Math.cos(Math.toRadians(rotation.y - 90)) * offsetX;
    }
    targetPosition.y += offsetY;
    return checkForCollisionWhenMoving(startPosition, targetPosition);
  }

  /**
   * Checks if there would be a collision when moving from <code>position</code> to
   * <code>targetPosition</code>. Returns the maximum allowed position that can be moved to by
   * applying each direction of movement (x,y,z) separately. This will result in the object
   * "sliding" against a wall, as expected.
   *
   * @param position
   *          starting position
   * @param targetPosition
   *          target position
   * @return the maximum allowed position that can be moved to
   */
  private static Vector3f checkForCollisionWhenMoving(Vector3f position, Vector3f targetPosition) {

    float diffX = targetPosition.x - position.x;
    float diffY = targetPosition.y - position.y;
    float diffZ = targetPosition.z - position.z;

    Vector3f collisionX = checkForCollisionSingleAxis(position, diffX, 0, 0);
    Vector3f collisionY = checkForCollisionSingleAxis(position, 0, diffY, 0);
    Vector3f collisionZ = checkForCollisionSingleAxis(position, 0, 0, diffZ);

    float newX = position.x + (collisionX != null ? 0 : diffX);
    float newY = position.y + (collisionY != null ? 0 : diffY);
    float newZ = position.z + (collisionZ != null ? 0 : diffZ);

    return new Vector3f(newX, newY, newZ);
  }

  /**
   * Checks for a collision between <code>position</code> and
   * <code>new Vector3f(position.x + diffX, position.y + diffY, position.z + diffZ)</code>.
   *
   * @param position
   *          start position
   * @param diffX
   * @param diffY
   * @param diffZ
   * @return the intersection point if there is a collision, null otherwise
   */
  public static Vector3f checkForCollisionSingleAxis(Vector3f position, float diffX, float diffY,
      float diffZ) {
    Vector3f projected = new Vector3f(position.x + diffX, position.y + diffY, position.z + diffZ);
    return CollisionMeshRegistry.checkForCollision(position, projected);
  }

  public static Matrix3f[] convertToTriangles(Mesh mesh) {
    int[] indices = mesh.getIndices();
    Matrix3f[] m = new Matrix3f[indices.length / 3];
    float[] positions = mesh.getPositions();
    int count = 0;
    for (int i = 0; i < indices.length; i += 3) {
      int indexA = indices[i];
      int indexB = indices[i + 1];
      int indexC = indices[i + 2];

      float v0x = positions[indexA * 3];
      float v0y = positions[indexA * 3 + 1];
      float v0z = positions[indexA * 3 + 2];
      Vector3f v0 = new Vector3f(v0x, v0y, v0z);

      float v1x = positions[indexB * 3];
      float v1y = positions[indexB * 3 + 1];
      float v1z = positions[indexB * 3 + 2];
      Vector3f v1 = new Vector3f(v1x, v1y, v1z);

      float v2x = positions[indexC * 3];
      float v2y = positions[indexC * 3 + 1];
      float v2z = positions[indexC * 3 + 2];
      Vector3f v2 = new Vector3f(v2x, v2y, v2z);

      Matrix3f t = new Matrix3f(v0, v1, v2);
      m[count++] = t;
    }
    return m;
  }

  public static Matrix3f[] translateRotateScale(Matrix3f[] triangles, Vector3f position,
      Quaternionf rotation, float scale) {
    Matrix3f[] output = new Matrix3f[triangles.length];
    int size = triangles.length;
    Vector3f dest = new Vector3f();

    for (int i = 0; i < size; i++) {
      Matrix3f t1 = triangles[i];
      Matrix3f t2 = new Matrix3f(
          translateRotateScale(t1.getColumn(0, dest), position, rotation, scale),
          translateRotateScale(t1.getColumn(1, dest), position, rotation, scale),
          translateRotateScale(t1.getColumn(2, dest), position, rotation, scale));
      output[i] = t2;
    }
    return output;
  }

  private static Vector3f translateRotateScale(Vector3f original, Vector3f pos, Quaternionf rot,
      float scale) {
    Vector3f v = new Vector3f(original);
    v = v.mul(scale);
    v.rotate(rot);
    v.x += pos.x;
    v.y += pos.y;
    v.z += pos.z;
    return v;
  }

  public static Matrix3f[] getCollisionMesh(Drawable drawable) {
    Mesh mesh = drawable.internal_getMeshes()[0];
    Matrix3f[] collisionMeshTriangles = CollisionMeshUtils.convertToTriangles(mesh);
    Matrix3f[] updatedCopy = CollisionMeshUtils.translateRotateScale(collisionMeshTriangles,
        drawable.getPosition(), drawable.getRotation(), drawable.getScale());
    return updatedCopy;
  }
}
