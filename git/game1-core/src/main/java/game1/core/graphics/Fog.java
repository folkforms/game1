package game1.core.graphics;

import org.joml.Vector3f;

public class Fog {

  private boolean isActive;
  private Vector3f colour;
  private float density;
  public static Fog NO_FOG = new Fog();

  public Fog() {
	isActive = false;
    this.colour = new Vector3f(0, 0, 0);
    this.density = 0;
  }

  public Fog(boolean isActive, Vector3f colour, float density) {
    this.colour = colour;
    this.density = density;
    this.isActive = isActive;
  }

  public boolean isActive() {
    return isActive;
  }

  public void setActive(boolean isActive) {
    this.isActive = isActive;
  }

  public Vector3f getColour() {
    return colour;
  }

  public void setColour(Vector3f colour) {
    this.colour = colour;
  }

  public float getDensity() {
    return density;
  }

  public void setDensity(float density) {
    this.density = density;
  }
}
