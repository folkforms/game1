package game1.core.graphics.meshes;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector3i;

public class TriangleMeshData extends SimpleMeshData {

  public TriangleMeshData(Vector3f p1, Vector3f p2, Vector3f p3) {
    description = String.format("%s[%s,%s,%s]", getClass().getSimpleName(), p1, p2, p3);
    addTriangle(p1, p2, p3);
  }

  private void addTriangle(Vector3f v1, Vector3f v2, Vector3f v3) {
    Vector3f pos0 = new Vector3f(v1.x, v1.y, v1.z);
    Vector3f pos1 = new Vector3f(v2.x, v2.y, v2.z);
    Vector3f pos2 = new Vector3f(v3.x, v3.y, v3.z);
    positions.add(pos0);
    positions.add(pos1);
    positions.add(pos2);

    Vector3i index0 = new Vector3i(0, 2, 1);
    indices.add(index0);

    float tx = 0;
    float ty = 0;
    float txx = 1;
    float tyy = 1;
    Vector2f t0 = new Vector2f(tx, tyy);
    Vector2f t1 = new Vector2f(tx, ty);
    Vector2f t2 = new Vector2f(txx, ty);
    Vector2f t3 = new Vector2f(txx, tyy);
    texCoords.add(t0);
    texCoords.add(t1);
    texCoords.add(t2);
    texCoords.add(t3);

    Vector3f normal = new Vector3f(0, 0, 1);
    normals.add(normal);
    normals.add(normal);
    normals.add(normal);
  }
}
