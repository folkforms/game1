package game1.core.graphics;

import org.joml.Vector4f;

import folkforms.log.Log;
import game1.core.infrastructure.Texture;
import game1.primitives.Rectangle;

public class Material {

  public static final Vector4f DEFAULT_COLOUR = new Vector4f(1.0f, 1.0f, 1.0f, 1.0f);

  // Ambient colour defines what colour the surface reflects under ambient lighting
  private Vector4f ambientColour;
  // The diffuse material vector defines the colour of the surface under diffuse lighting
  private Vector4f diffuseColour;
  // Specular colour defines the percentage of light that will be reflected
  private Vector4f specularColour;
  // Reflectance impacts the scattering/radius of the specular highlight
  private float reflectance;
  private Texture texture;
  private Texture normalMap;
  private Vector4f sprite = new Vector4f(0, 0, 1, 1);

  public Material() {
    this.ambientColour = DEFAULT_COLOUR;
    this.diffuseColour = DEFAULT_COLOUR;
    this.specularColour = DEFAULT_COLOUR;
    this.texture = null;
    this.reflectance = 0;
  }

  public Material(Vector4f colour, float reflectance) {
    this(colour, colour, colour, null, reflectance);
  }

  public Material(Texture texture) {
    this(DEFAULT_COLOUR, DEFAULT_COLOUR, DEFAULT_COLOUR, texture, 0);
  }

  public Material(Texture texture, float reflectance) {
    this(DEFAULT_COLOUR, DEFAULT_COLOUR, DEFAULT_COLOUR, texture, reflectance);
  }

  public Material(Vector4f ambientColour, Vector4f diffuseColour, Vector4f specularColour,
      Texture texture, float reflectance) {
    this.ambientColour = ambientColour;
    this.diffuseColour = diffuseColour;
    this.specularColour = specularColour;
    this.texture = texture;
    this.reflectance = reflectance;
  }

  public Material(Vector4f ambientColour, Vector4f diffuseColour, Vector4f specularColour,
      float reflectance) {
    this.ambientColour = ambientColour;
    this.diffuseColour = diffuseColour;
    this.specularColour = specularColour;
    this.texture = null;
    this.reflectance = reflectance;
  }

  public Vector4f getAmbientColour() {
    return ambientColour;
  }

  public void setAmbientColour(Vector4f ambientColour) {
    this.ambientColour = ambientColour;
  }

  public Vector4f getDiffuseColour() {
    return diffuseColour;
  }

  public void setDiffuseColour(Vector4f diffuseColour) {
    this.diffuseColour = diffuseColour;
  }

  public Vector4f getSpecularColour() {
    return specularColour;
  }

  public void setSpecularColour(Vector4f specularColour) {
    this.specularColour = specularColour;
  }

  public float getReflectance() {
    return reflectance;
  }

  public void setReflectance(float reflectance) {
    this.reflectance = reflectance;
  }

  /**
   * Gets the texture of this material, or <code>null</code> if not texture set.
   *
   * @return the texture of this material
   */
  public Texture getTexture() {
    return texture;
  }

  /**
   * Sets the texture of this material.
   *
   * @param texture
   *          texture to use
   */
  public void setTexture(Texture texture) {
    this.texture = texture;
  }

  /**
   * Gets the sprite within the texture of this material that should be applied to the mesh.
   *
   * @see #setSprite(Rectangle)
   */
  public Vector4f getSprite() {
    return sprite;
  }

  /**
   * Sets the sprite within the texture of this material that should be applied to the mesh.
   *
   * <p>
   * For example the texture might be an entire spritesheet and the sprite represents a small
   * portion that is used for this mesh.
   * </p>
   *
   * @param r
   *          sprite rectangle
   */
  public void setSprite(Rectangle r) {
    sprite = convertToZeroOneCoordinateSpace(r, texture);
  }

  /**
   * Converts a rectangle representing a set of points on a texture into zero-one coordinate space.
   *
   * @param r
   *          rectangle
   * @param t
   *          texture
   * @return a Vector4f of the rectangles position in the texture in zero-one coordinate space
   */
  private static Vector4f convertToZeroOneCoordinateSpace(Rectangle r, Texture t) {
    float tw = t.getWidth();
    float th = t.getHeight();
    float x1 = r.x / tw;
    float y1 = r.y / th;
    float x2 = (r.x + r.w) / tw;
    float y2 = (r.y + r.h) / th;
    Vector4f v = new Vector4f(x1, y1, x2, y2);
    return v;
  }

  public boolean hasNormalMap() {
    return this.normalMap != null;
  }

  public Texture getNormalMap() {
    return normalMap;
  }

  public void setNormalMap(Texture normalMap) {
    this.normalMap = normalMap;
  }

  public String getId() {
    return toString();
  }

  @Override
  public String toString() {
    return String.format("%s[textureId=%s,sprite=%s]", getClass().getSimpleName(),
        texture != null ? texture.getTextureId() : texture, sprite);
  }

  public void debug_dumpData() {
    Log.info("Material: %s", toString());
    Log.info("ambientColour: %s", ambientColour);
    Log.info("diffuseColour: %s", diffuseColour);
    Log.info("specularColour: %s", specularColour);
    Log.info("reflectance: %s", reflectance);
    Log.info("texture: %s", texture);
    Log.info("normalMap: %s", normalMap);
    Log.info("sprite: %s", sprite);
  }

  public static Material createTestMaterial() {
    return new Material(new Vector4f(0.3f, 0.3f, 0.3f, 1), new Vector4f(0.3f, 0.3f, 0.3f, 1),
        new Vector4f(0.3f, 0.3f, 0.3f, 1), 0.5f);
  }
}
