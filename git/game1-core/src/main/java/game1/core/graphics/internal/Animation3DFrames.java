package game1.core.graphics.internal;

import java.util.List;

/**
 * The entire set of frames for a 3D mesh animation.
 */
public class Animation3DFrames {

  private int currentFrame;
  private List<Animation3DFrame> frames;
  private String name;
  private double duration;
  private long numTicks = 0;
  private int tickRate = 2;

  public Animation3DFrames(String name, List<Animation3DFrame> frames, double duration) {
    this.name = name;
    this.frames = frames;
    currentFrame = 0;
    this.duration = duration;
  }

  public Animation3DFrame getCurrentFrame() {
    return this.frames.get(currentFrame);
  }

  public double getDuration() {
    return this.duration;
  }

  public List<Animation3DFrame> getFrames() {
    return frames;
  }

  public String getName() {
    return name;
  }

  public void setTickRate(int tickRate) {
    this.tickRate = tickRate;
  }

  public void advance() {
    if (numTicks % tickRate == 0) {
      int nextFrame = currentFrame + 1;
      if (nextFrame > frames.size() - 1) {
        currentFrame = 0;
      } else {
        currentFrame = nextFrame;
      }
    }
    numTicks++;
  }

  @Override
  public String toString() {
    return String.format("Animation3DFrames@%s[name=%s, numFrames=%s]", hashCode(), name,
        frames.size());
  }

  public int debug_getCurrentFrameNumber() {
    return currentFrame;
  }

  public int debug_getTotalFrames() {
    return frames.size();
  }
}
