package game1.core.graphics;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import game1.actors.Actor;
import game1.core.infrastructure.Texture;
import game1.primitives.Rectangle;

/**
 * A RenderedFont is constructed from a texture, rectangle and font metadata. Font metadata lists
 * the location of each character within the image.
 */
public class RenderedFont implements Actor {

  private int SPACE_WIDTH = 5;
  private int SPACING = 1;

  private Texture texture;
  private Map<Character, Rectangle> characterSpriteMap = new HashMap<>();

  public static RenderedFont newInstance(Texture texture, Rectangle rectangle,
      List<String> fontData) {
    return new RenderedFont(texture, rectangle, fontData);
  }

  /**
   * Creates a new {@link RenderedFont} object from the given image and font metadata.
   *
   * @param image
   *          image file
   * @param fontDataStream
   *          input stream to read font metadata from
   * @throws IOException
   *           if an error occurs reading the font metadata
   */
  private RenderedFont(Texture texture, Rectangle sprite, List<String> characterPositions) {
    this.texture = texture;
    createCharacterSpriteMap(sprite, characterPositions);
    Rectangle c = characterSpriteMap.get('c');
    if (c == null) {
      c = characterSpriteMap.get('C');
    }
    SPACE_WIDTH = c.w;
  }

  private void createCharacterSpriteMap(Rectangle sprite, List<String> characterPositions) {
    for (String line : characterPositions) {
      if (line.length() > 0) {
        char c = line.charAt(0);
        line = line.substring(2);
        String[] tokens = line.split(",");
        int x = Integer.parseInt(tokens[0]) + sprite.x;
        int y = Integer.parseInt(tokens[1]) + sprite.y;
        int w = Integer.parseInt(tokens[2]);
        int h = Integer.parseInt(tokens[3]);
        Rectangle r = new Rectangle(x, y, w, h);
        characterSpriteMap.put(c, r);
      }
    }
    // If no lower case letters included then make them point to the upper case letters
    if (characterSpriteMap.get('a') == null) {
      for (int i = 0; i < 26; i++) {
        char c = (char) (i + 97);
        char sub = (char) (i + 97 - 32);
        characterSpriteMap.put(c, characterSpriteMap.get(sub));
      }
    }
  }

  /**
   * Gets the rectangle that contains the given character in the image.
   *
   * @param c
   *          character whose rectangle we want
   * @return the rectangle that contains the given character in the image
   */
  public Rectangle getRectangle(char c) {
    Rectangle r = characterSpriteMap.get(c);
    if (r == null) {
      r = characterSpriteMap.get('?');
      if (r == null) {
        throw new NullPointerException(
            String.format("Character '%s' not found and replacement '?' also not found!", c));
      }
    }
    return r;
  }

  /**
   * Gets the width of the given character.
   *
   * @param c
   *          input character
   * @return the width of the given character, in pixels
   */
  public int getCharWidth(char c) {
    return getRectangle(c).w;
  }

  /**
   * Gets the height of the given character.
   *
   * @param c
   *          input character
   * @return the height of the given character, in pixels
   */
  public int getCharHeight(char c) {
    return getRectangle(c).h;
  }

  /**
   * Gets the width of a space character.
   *
   * @return the width of a space character
   */
  public int getSpaceWidth() {
    return SPACE_WIDTH;
  }

  /**
   * Gets the width between each character.
   *
   * @return the width between each character
   */
  public int getCharSpacing() {
    return SPACING;
  }

  /**
   * Gets the texture that backs the image used in this font.
   *
   * @return Gets the texture that backs the image used in this font
   */
  public Texture getTexture() {
    return texture;
  }
}
