package game1.core.graphics.meshes;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector3i;

import game1.core.devtools.StringFormatUtils;

public class QuadMeshData extends SimpleMeshData {

  public QuadMeshData(Vector3f p1, Vector3f p2, Vector3f p3, Vector3f p4) {
    String p1Str = StringFormatUtils.toString(p1);
    String p2Str = StringFormatUtils.toString(p2);
    String p3Str = StringFormatUtils.toString(p3);
    String p4Str = StringFormatUtils.toString(p4);
    description = String.format("%s[(%s),(%s),(%s),(%s)]", getClass().getSimpleName(), p1Str, p2Str,
        p3Str, p4Str);
    addQuad(p1, p2, p3, p4);
  }

  private void addQuad(Vector3f v1, Vector3f v2, Vector3f v3, Vector3f v4) {
    Vector3f pos0 = new Vector3f(v1.x, v1.y, v1.z);
    Vector3f pos1 = new Vector3f(v2.x, v2.y, v2.z);
    Vector3f pos2 = new Vector3f(v3.x, v3.y, v3.z);
    Vector3f pos3 = new Vector3f(v4.x, v4.y, v4.z);
    positions.add(pos0);
    positions.add(pos1);
    positions.add(pos2);
    positions.add(pos3);

    Vector3i index0 = new Vector3i(0, 2, 1);
    Vector3i index1 = new Vector3i(0, 3, 2);
    indices.add(index0);
    indices.add(index1);

    float tx = 0;
    float ty = 0;
    float txx = 1;
    float tyy = 1;
    Vector2f t0 = new Vector2f(tx, tyy);
    Vector2f t1 = new Vector2f(tx, ty);
    Vector2f t2 = new Vector2f(txx, ty);
    Vector2f t3 = new Vector2f(txx, tyy);
    texCoords.add(t0);
    texCoords.add(t1);
    texCoords.add(t2);
    texCoords.add(t3);

    Vector3f normal = new Vector3f(0, 0, 1);
    normals.add(normal);
    normals.add(normal);
    normals.add(normal);
    normals.add(normal);
  }
}
