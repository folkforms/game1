package game1.core.graphics.meshes;

import java.util.HashMap;
import java.util.Map;

class MeshCache {

  private Map<String, Mesh> meshes = new HashMap<>();

  public void add(Mesh mesh) {
    meshes.put(mesh.getId(), mesh);
  }

  public Mesh get(String id) {
    return meshes.get(id);
  }
}
