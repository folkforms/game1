package game1.core.graphics.meshes;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_DYNAMIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL31.glDrawElementsInstanced;
import static org.lwjgl.opengl.GL33.glVertexAttribDivisor;

import java.nio.FloatBuffer;
import java.util.Arrays;

import org.joml.Matrix4f;
import org.joml.Vector4f;
import org.lwjgl.system.MemoryUtil;

import folkforms.log.Log;
import game1.core.engine.internal.Transformation;
import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.graphics.Material;
import game1.core.infrastructure.Texture;

public class InstancedMesh33 extends Mesh33 implements InstancedMesh {

  private static final int FLOAT_SIZE_BYTES = 4;
  private static final int VECTOR4F_SIZE_BYTES = FLOAT_SIZE_BYTES * 4;
  private static final int MATRIX_SIZE_FLOATS = 4 * 4;
  private static final int MATRIX_SIZE_BYTES = MATRIX_SIZE_FLOATS * FLOAT_SIZE_BYTES;
  private static final int INSTANCE_SIZE_BYTES = MATRIX_SIZE_BYTES * 2 + FLOAT_SIZE_BYTES
      + FLOAT_SIZE_BYTES + VECTOR4F_SIZE_BYTES;
  private static final int INSTANCE_SIZE_FLOATS = MATRIX_SIZE_FLOATS * 2 + 1 + 1 + 4;

  private final int numInstances;
  private final int instanceDataVBO;
  private FloatBuffer instanceDataBuffer;
  protected boolean isInstanced = true;
  private int vertexAttributePointers = 0;

  public InstancedMesh33(float[] positions, float[] textCoords, float[] normals, int[] indices,
      int[] jointIndices, float[] weights, int numInstances, String filename, Material material) {
    super(positions, textCoords, normals, indices, jointIndices, weights, filename, material);

    this.numInstances = numInstances;
    instanceDataBuffer = MemoryUtil.memAllocFloat(numInstances * INSTANCE_SIZE_FLOATS);

    glBindVertexArray(vaoId);
    instanceDataVBO = glGenBuffers();
    vboIdList.add(instanceDataVBO);
    glBindBuffer(GL_ARRAY_BUFFER, instanceDataVBO);

    // This data represents locations 5-13 in scene_3d_vertex.vs => 9 locations (mat4, mat4, float)
    int startLocation = 5;
    int strideStart = 0;

    // modelViewInstancedMatrix (= mat4 = vec4 * 4)
    for (int i = 0; i < 4; i++) {
      glVertexAttribPointer(startLocation, 4, GL_FLOAT, false, INSTANCE_SIZE_BYTES, strideStart);
      glVertexAttribDivisor(startLocation, 1);
      startLocation++;
      strideStart += VECTOR4F_SIZE_BYTES;
      vertexAttributePointers += 1;
    }

    // modelLightViewInstancedMatrix (= mat4 = vec4 * 4)
    for (int i = 0; i < 4; i++) {
      glVertexAttribPointer(startLocation, 4, GL_FLOAT, false, INSTANCE_SIZE_BYTES, strideStart);
      glVertexAttribDivisor(startLocation, 1);
      startLocation++;
      strideStart += VECTOR4F_SIZE_BYTES;
      vertexAttributePointers += 1;
    }

    // selectedInstanced (= float)
    glVertexAttribPointer(startLocation, 1, GL_FLOAT, false, INSTANCE_SIZE_BYTES, strideStart);
    glVertexAttribDivisor(startLocation, 1);
    startLocation++;
    strideStart += FLOAT_SIZE_BYTES;
    vertexAttributePointers += 1;

    // hasColor (= float)
    glVertexAttribPointer(startLocation, 1, GL_FLOAT, false, INSTANCE_SIZE_BYTES, strideStart);
    glVertexAttribDivisor(startLocation, 1);
    startLocation++;
    strideStart += FLOAT_SIZE_BYTES;
    vertexAttributePointers += 1;

    // particleColor (= vec4)
    glVertexAttribPointer(startLocation, 4, GL_FLOAT, false, INSTANCE_SIZE_BYTES, strideStart);
    glVertexAttribDivisor(startLocation, 1);
    startLocation++;
    strideStart += VECTOR4F_SIZE_BYTES;
    vertexAttributePointers += 1;

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
  }

  @Override
  public void cleanup() {
    super.cleanup();
    if (instanceDataBuffer != null) {
      MemoryUtil.memFree(instanceDataBuffer);
      instanceDataBuffer = null;
    }
  }

  @Override
  protected void initRender() {
    super.initRender();

    int start = 5;
    for (int i = 0; i < vertexAttributePointers; i++) {
      glEnableVertexAttribArray(start + i);
    }
  }

  @Override
  protected void endRender() {
    int start = 5;
    for (int i = 0; i < vertexAttributePointers; i++) {
      glDisableVertexAttribArray(start + i);
    }

    super.endRender();
  }

  @Override
  public void renderListInstanced(Transformation transformation, Matrix4f viewMatrix,
      Matrix4f lightViewMatrix) {
    initRender();

    int chunkSize = numInstances;
    int length = drawables.size();
    for (int i = 0; i < length; i += chunkSize) {
      int end = Math.min(length, i + chunkSize);
      renderChunkInstanced(i, end, transformation, viewMatrix, lightViewMatrix);
    }

    endRender();
  }

  private void renderChunkInstanced(int start, int end, Transformation transformation,
      Matrix4f viewMatrix, Matrix4f lightViewMatrix) {
    instanceDataBuffer.clear();

    int i = 0;
    int numToRender = 0;
    for (int k = start; k < end; k++) {
      Drawable drawable = drawables.get(k);
      if (!drawable.isVisible() || !drawable.isInsideFrustum()) {
        continue;
      }
      numToRender++;

      Matrix4f modelMatrix = transformation.buildModelMatrix(drawable.getRotation(),
          drawable.getPosition(), drawable.getScale());
      if (viewMatrix != null) {
        Matrix4f modelViewMatrix = transformation.buildModelViewMatrix(modelMatrix, viewMatrix);
        modelViewMatrix.get(INSTANCE_SIZE_FLOATS * i, instanceDataBuffer);
      }
      if (lightViewMatrix != null) {
        Matrix4f modelLightViewMatrix = transformation.buildModelLightViewMatrix(modelMatrix,
            lightViewMatrix);
        modelLightViewMatrix.get(INSTANCE_SIZE_FLOATS * i + MATRIX_SIZE_FLOATS, instanceDataBuffer);
      }
      // Selected data
      int buffPos = INSTANCE_SIZE_FLOATS * i + MATRIX_SIZE_FLOATS + MATRIX_SIZE_FLOATS;
      instanceDataBuffer.put(buffPos, drawable.isSelected() ? 1 : 0);

      i++;
    }

    glBindBuffer(GL_ARRAY_BUFFER, instanceDataVBO);
    glBufferData(GL_ARRAY_BUFFER, instanceDataBuffer, GL_DYNAMIC_DRAW);

    glDrawElementsInstanced(GL_TRIANGLES, getVertexCount(), GL_UNSIGNED_INT, 0, numToRender);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
  }

  @Override
  public void renderBillboardListInstanced(Transformation transformation, Matrix4f viewMatrix,
      Matrix4f lightViewMatrix) {
    initRender();

    int chunkSize = numInstances;
    int length = drawables.size();
    for (int i = 0; i < length; i += chunkSize) {
      int end = Math.min(length, i + chunkSize);
      renderBillboardChunkInstanced(i, end, transformation, viewMatrix, lightViewMatrix);
    }

    endRender();
  }

  private void renderBillboardChunkInstanced(int start, int end, Transformation transformation,
      Matrix4f viewMatrix, Matrix4f lightViewMatrix) {
    instanceDataBuffer.clear();

    int numToRender = 0;
    int i = 0;
    for (int k = start; k < end; k++) {
      Drawable drawable = drawables.get(k);
      if (!drawable.isVisible()) {
        continue;
      }
      numToRender++;

      Matrix4f modelMatrix = transformation.buildModelMatrix(drawable.getRotation(),
          drawable.getPosition(), drawable.getScale());
      if (viewMatrix != null) {
        viewMatrix.transpose3x3(modelMatrix);
        Matrix4f modelViewMatrix = transformation.buildModelViewMatrix(modelMatrix, viewMatrix);
        modelViewMatrix.scale(drawable.getScale());
        modelViewMatrix.get(INSTANCE_SIZE_FLOATS * i, instanceDataBuffer);
      }
      if (lightViewMatrix != null) {
        Matrix4f modelLightViewMatrix = transformation.buildModelLightViewMatrix(modelMatrix,
            lightViewMatrix);
        modelLightViewMatrix.get(INSTANCE_SIZE_FLOATS * i + MATRIX_SIZE_FLOATS, instanceDataBuffer);
      }

      // // FIXME BILLBOARDS: Not used
      // // Selected data
      int buffPos = INSTANCE_SIZE_FLOATS * i + MATRIX_SIZE_FLOATS + MATRIX_SIZE_FLOATS;
      // instanceDataBuffer.put(buffPos, drawable.isSelected() ? 1 : 0);

      // ======== FIXME Start ========

      buffPos += 1;
      Material m = drawable.internal_getMeshes()[0].getMaterial();
      float hasColor = m != null && m.getTexture() != null ? 0 : 1;
      instanceDataBuffer.put(buffPos, hasColor);

      if (hasColor > 0) {
        buffPos += 1;
        Colour colour = drawable.getColour();
        Vector4f v = colour != null ? colour : new Vector4f(1, 1, 1, 1);
        v.get(buffPos, instanceDataBuffer);
      }

      // ======== FIXME End ========

      i++;
    }

    // debug_dumpInstanceDataBuffer();

    glBindBuffer(GL_ARRAY_BUFFER, instanceDataVBO);
    glBufferData(GL_ARRAY_BUFFER, instanceDataBuffer, GL_DYNAMIC_DRAW);

    glDrawElementsInstanced(GL_TRIANGLES, getVertexCount(), GL_UNSIGNED_INT, 0, numToRender);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
  }

  private void debug_dumpInstanceDataBuffer() {
    int cap = instanceDataBuffer.capacity();
    float[] f = new float[cap];
    for (int i = 0; i < cap; i++) {
      f[i] = instanceDataBuffer.get(i);
    }
    Log.temp("instanceDataBuffer (%s) = %s", cap, Arrays.toString(f));
  }

  @Override
  public InstancedMesh clone() {
    Texture texture = this.getMaterial().getTexture();
    InstancedMesh mesh = new InstancedMesh33(positions, textCoords, normals, indices, jointIndices,
        weights, numInstances, name, new Material(texture));
    return mesh;
  }

  @Override
  public boolean equals(Object other) {
    if (other == null) {
      return false;
    } else if (other instanceof InstancedMesh33) {
      InstancedMesh33 otherMesh = (InstancedMesh33) other;
      return id.equals(otherMesh.getId());
    } else {
      return false;
    }
  }
}
