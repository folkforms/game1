package game1.core.graphics.meshes;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.GL_TEXTURE1;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

import java.math.BigInteger;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.joml.Matrix4f;
import org.lwjgl.system.MemoryUtil;

import folkforms.log.Log;
import game1.core.engine.internal.Transformation;
import game1.core.graphics.Animation3D;
import game1.core.graphics.ArrayUtils;
import game1.core.graphics.Drawable;
import game1.core.graphics.Material;
import game1.core.graphics.internal.Animation3DFrame;
import game1.core.infrastructure.Texture;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3ShaderProgram;
import game1.core.pipelines.GraphicsPipelineName;

/**
 * A Mesh is a set of triangles that make up a 3D object.
 *
 * <p>
 * Note: See {@link MeshUtils} javadoc for an explanation of the ordering of vertices and indices.
 * </p>
 */
public class Mesh33 implements Mesh {

  protected final int vaoId;
  protected final List<Integer> vboIdList;
  private final int vertexCount;
  private Material material;
  private float boundingRadius = 1;
  protected boolean isInstanced = false;
  protected String name;
  /**
   * Used for resizing the mesh on the fly
   */
  private int positionsVboId;
  private float width, height;
  protected float[] positions, textCoords, normals, weights;
  protected int[] indices, jointIndices;
  protected List<Drawable> drawables = new CopyOnWriteArrayList<>();
  protected String id;
  protected int humanReadableId;

  public Mesh33(float[] positions, float[] textCoords, float[] normals, int[] indices,
      int[] jointIndices, float[] weights, String name, Material material) {

    id = MeshId.generate(getClass().getSimpleName(), positions, textCoords, normals, indices,
        jointIndices, weights, material);
    humanReadableId = MeshId.generateHumanReadableId();

    this.positions = positions;
    this.textCoords = textCoords;
    this.normals = normals;
    this.indices = indices;
    this.jointIndices = jointIndices;
    this.weights = weights;
    this.name = name;
    this.material = material;

    if (jointIndices == null) {
      jointIndices = ArrayUtils.createEmptyIntArray(MAX_WEIGHTS * positions.length / 3, 0);
    }
    if (weights == null) {
      weights = ArrayUtils.createEmptyFloatArray(MAX_WEIGHTS * positions.length / 3, 0);
    }

    FloatBuffer posBuffer = null;
    FloatBuffer textCoordsBuffer = null;
    FloatBuffer vecNormalsBuffer = null;
    IntBuffer indicesBuffer = null;
    FloatBuffer weightsBuffer = null;
    IntBuffer jointIndicesBuffer = null;

    try {
      vertexCount = indices.length;
      vboIdList = new ArrayList<>();

      vaoId = glGenVertexArrays();
      glBindVertexArray(vaoId);

      // Position VBO
      if (positions.length >= 2) {
        width = positions[0];
        height = positions[1];
      } else {
        width = 0;
        height = 0;
      }
      int vboId = positionsVboId = glGenBuffers();
      vboIdList.add(vboId);
      posBuffer = MemoryUtil.memAllocFloat(positions.length);
      posBuffer.put(positions).flip();
      glBindBuffer(GL_ARRAY_BUFFER, vboId);
      glBufferData(GL_ARRAY_BUFFER, posBuffer, GL_STATIC_DRAW);
      glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);

      // Texture coordinates VBO
      vboId = glGenBuffers();
      vboIdList.add(vboId);
      textCoordsBuffer = MemoryUtil.memAllocFloat(textCoords.length);
      textCoordsBuffer.put(textCoords).flip();
      glBindBuffer(GL_ARRAY_BUFFER, vboId);
      glBufferData(GL_ARRAY_BUFFER, textCoordsBuffer, GL_STATIC_DRAW);
      glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);

      // Vertex normals VBO
      vboId = glGenBuffers();
      vboIdList.add(vboId);
      vecNormalsBuffer = MemoryUtil.memAllocFloat(normals.length);
      vecNormalsBuffer.put(normals).flip();
      glBindBuffer(GL_ARRAY_BUFFER, vboId);
      glBufferData(GL_ARRAY_BUFFER, vecNormalsBuffer, GL_STATIC_DRAW);
      glVertexAttribPointer(2, 3, GL_FLOAT, false, 0, 0);

      // Weights
      vboId = glGenBuffers();
      vboIdList.add(vboId);
      weightsBuffer = MemoryUtil.memAllocFloat(weights.length);
      weightsBuffer.put(weights).flip();
      glBindBuffer(GL_ARRAY_BUFFER, vboId);
      glBufferData(GL_ARRAY_BUFFER, weightsBuffer, GL_STATIC_DRAW);
      glVertexAttribPointer(3, 4, GL_FLOAT, false, 0, 0);

      // Joint indices
      vboId = glGenBuffers();
      vboIdList.add(vboId);
      jointIndicesBuffer = MemoryUtil.memAllocInt(jointIndices.length);
      jointIndicesBuffer.put(jointIndices).flip();
      glBindBuffer(GL_ARRAY_BUFFER, vboId);
      glBufferData(GL_ARRAY_BUFFER, jointIndicesBuffer, GL_STATIC_DRAW);
      glVertexAttribPointer(4, 4, GL_FLOAT, false, 0, 0);

      // Index VBO
      vboId = glGenBuffers();
      vboIdList.add(vboId);
      indicesBuffer = MemoryUtil.memAllocInt(indices.length);
      indicesBuffer.put(indices).flip();
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboId);
      glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_STATIC_DRAW);

      glBindBuffer(GL_ARRAY_BUFFER, 0);
      glBindVertexArray(0);
    } finally {
      if (posBuffer != null) {
        MemoryUtil.memFree(posBuffer);
      }
      if (textCoordsBuffer != null) {
        MemoryUtil.memFree(textCoordsBuffer);
      }
      if (vecNormalsBuffer != null) {
        MemoryUtil.memFree(vecNormalsBuffer);
      }
      if (weightsBuffer != null) {
        MemoryUtil.memFree(weightsBuffer);
      }
      if (jointIndicesBuffer != null) {
        MemoryUtil.memFree(jointIndicesBuffer);
      }
      if (indicesBuffer != null) {
        MemoryUtil.memFree(indicesBuffer);
      }
    }

    this.name = name;
  }

  public Mesh33(float[] positions, float[] textCoords, float[] normals, int[] indices, String name,
      Material material) {
    this(positions, textCoords, normals, indices,
        ArrayUtils.createEmptyIntArray(MAX_WEIGHTS * positions.length / 3, 0),
        ArrayUtils.createEmptyFloatArray(MAX_WEIGHTS * positions.length / 3, 0), name, material);
  }

  @Override
  public void resize(float width, float height) {
    float[] positions = new float[] { //
        0, 0, 0, // 0
        0, height, 0, // 1
        width, height, 0, // 2
        width, 0, 0 // 3
    };
    FloatBuffer posBuffer = null;
    posBuffer = MemoryUtil.memAllocFloat(positions.length);
    posBuffer.put(positions).flip();
    glBindBuffer(GL_ARRAY_BUFFER, positionsVboId);
    glBufferData(GL_ARRAY_BUFFER, posBuffer, GL_STATIC_DRAW);
    this.width = width;
    this.height = height;
  }

  @Override
  public Material getMaterial() {
    return material;
  }

  @Override
  public void setMaterial(Material material) {
    this.material = material;
  }

  private int getVaoId() {
    return vaoId;
  }

  protected int getVertexCount() {
    return vertexCount;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public float getWidth() {
    return width;
  }

  @Override
  public float getHeight() {
    return height;
  }

  protected void initRender() {
    Texture texture = material.getTexture();
    if (texture != null) {
      // Activate first texture bank
      glActiveTexture(GL_TEXTURE0);
      // Bind the texture
      glBindTexture(GL_TEXTURE_2D, texture.getTextureId());
    }
    Texture normalMap = material.getNormalMap();
    if (normalMap != null) {
      glActiveTexture(GL_TEXTURE1);
      glBindTexture(GL_TEXTURE_2D, normalMap.getTextureId());
    }
    // Draw the mesh
    glBindVertexArray(getVaoId());
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);
    glEnableVertexAttribArray(3);
    glEnableVertexAttribArray(4);
  }

  protected void endRender() {
    // Restore state
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(3);
    glDisableVertexAttribArray(4);
    glBindVertexArray(0);

    glBindTexture(GL_TEXTURE_2D, 0);
  }

  // Used for rendering simple objects like 2D and skybox. Projection has already been applied.
  @Override
  public void render(GraphicsPipelineName pipeline) {
    initRender();
    glDrawElements(GL_TRIANGLES, getVertexCount(), GL_UNSIGNED_INT, 0);
    endRender();
  }

  @Override
  public void renderList(LWJGL3ShaderProgram shader, LWJGL3ShaderProgram sceneShaderProgram,
      Transformation transformation, Matrix4f viewMatrix, Matrix4f lightViewMatrix) {
    initRender();

    for (Drawable drawable : drawables) {
      if (!drawable.isVisible() || !drawable.isInsideFrustum()) {
        continue;
      }

      // Set up data required by the drawable
      sceneShaderProgram.setUniform("selectedNonInstanced", drawable.isSelected() ? 1 : 0);
      Matrix4f modelMatrix = transformation.buildModelMatrix(drawable.getRotation(),
          drawable.getPosition(), drawable.getScale());
      if (viewMatrix != null) {
        Matrix4f modelViewMatrix = transformation.buildModelViewMatrix(modelMatrix, viewMatrix);
        sceneShaderProgram.setUniform("modelViewNonInstancedMatrix", modelViewMatrix);
      }
      Matrix4f modelLightViewMatrix = transformation.buildModelLightViewMatrix(modelMatrix,
          lightViewMatrix);
      shader.setUniform("modelLightViewNonInstancedMatrix", modelLightViewMatrix);

      if (drawable instanceof Animation3D) {
        Animation3D animated = (Animation3D) drawable;
        Animation3DFrame frame = animated.getCurrentAnimation().getCurrentFrame();
        shader.setUniform("jointsMatrix", frame.getJointMatrices());
      }

      // Render the drawable
      glDrawElements(GL_TRIANGLES, getVertexCount(), GL_UNSIGNED_INT, 0);
    }
    endRender();
  }

  @Override
  public void cleanup() {
    glDisableVertexAttribArray(0);

    // Delete the VBOs
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    for (int vboId : vboIdList) {
      glDeleteBuffers(vboId);
    }

    // Delete the texture
    // Log.temp("Mesh.cleanup: Do we need to delete textures? Why did I remove Texture.cleanup()?");
    // Log.temp("Mesh.cleanup: Should we be calling normalMap.cleanup() as well?");
    // Texture texture = material.getTexture();
    // if (texture != null) {
    // texture.cleanup();
    // GL30.glDeleteTextures(textureId);
    // }

    // Delete the VAO
    glBindVertexArray(0);
    glDeleteVertexArrays(vaoId);
  }

  @Override
  public void deleteBuffers() {
    glDisableVertexAttribArray(0);

    // Delete the VBOs
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    for (int vboId : vboIdList) {
      glDeleteBuffers(vboId);
    }

    // Delete the VAO
    glBindVertexArray(0);
    glDeleteVertexArrays(vaoId);
  }

  /**
   * Do not use this unless you need to change materials or stretch (not scale) a mesh. It is
   * considered a different instance and will slow rendering. If you want to copy an object you can
   * use {@link Drawable#clone} which will re-use the same instanced mesh.
   */
  @Override
  public Mesh clone() {
    Texture texture = this.getMaterial().getTexture();
    Mesh mesh = new Mesh33(positions, textCoords, normals, indices, jointIndices, weights, name,
        new Material(texture));
    return mesh;
  }

  @Override
  public String toString() {
    return String.format("Mesh[id=%s,name=%s,material=%s]@%s", humanReadableId, name, material,
        hashCode());
  }

  @Override
  public void debug_dumpData() {
    Log.info("Mesh: %s", toString());
    Log.info("positions: %s", Arrays.toString(positions));
    Log.info("textCoords: %s", Arrays.toString(textCoords));
    Log.info("normals: %s", Arrays.toString(normals));
    Log.info("indices: %s", Arrays.toString(indices));
    if (material == null) {
      Log.info("Material: null");
    } else {
      material.debug_dumpData();
    }
  }

  @Override
  public void link(Drawable d) {
    drawables.add(d);
  }

  @Override
  public void unlink(Drawable d) {
    drawables.remove(d);
  }

  @Override
  public List<Drawable> listDrawables() {
    return drawables;
  }

  @Override
  public float[] getPositions() {
    return positions;
  }

  @Override
  public int[] getIndices() {
    return indices;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public int getHumanReadableId() {
    return humanReadableId;
  }

  @Override
  public boolean equals(Object other) {
    if (other == null) {
      return false;
    } else if (other instanceof Mesh33) {
      Mesh33 otherMesh = (Mesh33) other;
      return id.equals(otherMesh.getId());
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    BigInteger bigInt = new BigInteger(getId().getBytes());
    return bigInt.intValue();
  }
}
