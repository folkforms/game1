package game1.core.graphics;

import org.joml.Vector3f;
import org.joml.Vector4f;

/**
 * Convenience class that allows users to define colours for later use.
 */
public class Colour extends Vector4f {

  /**
   * Create a new colour object.
   *
   * @param r
   *          red value
   * @param g
   *          green value
   * @param b
   *          blue value
   * @param a
   *          alpha value
   */
  public Colour(float r, float g, float b, float a) {
    super(r, g, b, a);
    if (r < 0 || r > 1 || g < 0 || g > 1 || b < 0 || b > 1 || a < 0 || a > 1) {
      throw new IllegalArgumentException(String.format(
          "Colour values must be between 0 and 1, but they were r=%s,g=%s,b=%s,a=%s", r, g, b, a));
    }
  }

  /**
   * Create a new colour object. Its alpha will be 1 (100% opaque).
   *
   * @param r
   *          red value
   * @param g
   *          green value
   * @param b
   *          blue value
   */
  public Colour(float r, float g, float b) {
    this(r, g, b, 1);
  }

  /**
   * Gets the red value of this colour.
   *
   * @return the red value of this colour
   */
  public float getRed() {
    return x;
  }

  /**
   * Sets the red value of this colour.
   *
   * @param newR
   *          the new red value
   */
  public void setRed(float newR) {
    this.x = newR;
  }

  /**
   * Gets the green value of this colour.
   *
   * @return the green value of this colour
   */
  public float getGreen() {
    return y;
  }

  /**
   * Sets the green value of this colour.
   *
   * @param newG
   *          the new green value
   */
  public void setGreen(float newG) {
    this.y = newG;
  }

  /**
   * Gets the blue value of this colour.
   *
   * @return the blue value of this colour
   */
  public float getBlue() {
    return z;
  }

  /**
   * Sets the blue value of this colour.
   *
   * @param newB
   *          the new blue value
   */
  public void setBlue(float newB) {
    this.z = newB;
  }

  /**
   * Gets the alpha value of this colour.
   *
   * @return the alpha value of this colour
   */
  public float getAlpha() {
    return w;
  }

  /**
   * Sets the alpha value of this colour.
   *
   * @param newA
   *          the new alpha value
   */
  public void setAlpha(float newA) {
    this.w = newA;
  }

  /**
   * Converts integer RGBA values (from 0-255) into a {@link Colour} object.
   *
   * @param r
   *          red value
   * @param g
   *          green value
   * @param b
   *          blue value
   * @param a
   *          alpha value
   * @return a Colour object based on the given RGBA values
   */
  public static Colour fromRGB(int r, int g, int b, int a) {
    if (r < 0 || r > 255 || g < 0 || g > 255 || b < 0 || b > 255 || a < 0 || a > 255) {
      throw new IllegalArgumentException(String.format(
          "Colour values must be between 0 and 255, but they were r=%s,g=%s,b=%s,a=%s", r, g, b,
          a));
    }
    return new Colour(r / 255f, g / 255f, b / 255f, a / 255f);
  }

  /**
   * Converts integer RGB values (from 0-255) into a {@link Colour} object.
   *
   * @param r
   *          red value
   * @param g
   *          green value
   * @param b
   *          blue value
   * @return a Colour object based on the given RGB values
   */
  public static Colour fromRGB(int r, int g, int b) {
    return Colour.fromRGB(r, g, b, 255);
  }

  /**
   * Converts a hex string like #3c2940 or #4CF into a {@link Colour} object.
   *
   * @param hex
   *          hex string (note the '#' is optional and the case is irrelevant)
   * @return a Colour object based on the given hex string
   */
  public static Colour fromHex(String hex) {
    String originalHex = hex;
    if (hex.charAt(0) == '#') {
      hex = hex.substring(1);
    }

    // If length 8 do nothing
    // If length 6 add 'ff' for alpha at the end
    if (hex.length() == 6) {
      hex += "ff";
    }
    // If length 3 add 'f' for alpha at the end
    if (hex.length() == 3) {
      hex += "f";
    }

    // Convert 3-character colours into 6-character colours, e.g. '123' -> '112233'
    if (hex.length() == 4) {
      StringBuffer sb = new StringBuffer();
      sb.append(hex.charAt(0)).append(hex.charAt(0));
      sb.append(hex.charAt(1)).append(hex.charAt(1));
      sb.append(hex.charAt(2)).append(hex.charAt(2));
      sb.append(hex.charAt(3)).append(hex.charAt(3));
      hex = sb.toString();
    }

    if (hex.length() != 8) {
      throw new RuntimeException(String.format(
          "Invalid value for hex: %s. Should be 3, 4, 6 or 8 characters long (excluding the '#' if present).",
          originalHex));
    }

    String redStr = hex.substring(0, 2);
    String greenStr = hex.substring(2, 4);
    String blueStr = hex.substring(4, 6);
    String alphaStr = hex.substring(6, 8);
    int redInt = Integer.parseInt(redStr, 16);
    int greenInt = Integer.parseInt(greenStr, 16);
    int blueInt = Integer.parseInt(blueStr, 16);
    int alphaInt = Integer.parseInt(alphaStr, 16);
    return Colour.fromRGB(redInt, greenInt, blueInt, alphaInt);
  }

  /**
   * Converts a Vector3f into a {@link Colour} object.
   *
   * @param vec3
   *          Vector3f object
   * @return a Colour object based on the given Vector3f
   */
  public static Colour fromVector3f(Vector3f vec3) {
    return new Colour(vec3.x, vec3.y, vec3.z);
  }

  /**
   * Converts a Vector4f into a {@link Colour} object.
   *
   * @param vec4
   *          Vector4f object
   * @return a Colour object based on the given Vector4f
   */
  public static Colour fromVector4f(Vector4f vec4) {
    return new Colour(vec4.x, vec4.y, vec4.z, vec4.w);
  }

  @Override
  public Colour clone() {
    return new Colour(x, y, z, w);
  }

  public Vector3f toVector3f() {
    return new Vector3f(x, y, z);
  }

  public String toHexString() {
    String c1 = Integer.toHexString((int) (x * 255));
    String c2 = Integer.toHexString((int) (y * 255));
    String c3 = Integer.toHexString((int) (z * 255));
    String c4 = Integer.toHexString((int) (w * 255));
    if (c1.length() == 1) {
      c1 = "0" + c1;
    }
    if (c2.length() == 1) {
      c2 = "0" + c2;
    }
    if (c3.length() == 1) {
      c3 = "0" + c3;
    }
    if (c4.length() == 1) {
      c4 = "0" + c4;
    }
    return String.format("#%s%s%s%s", c1, c2, c3, c4);
  }

  @Override
  public String toString() {
    String c1 = Integer.toHexString((int) (x * 255));
    String c2 = Integer.toHexString((int) (y * 255));
    String c3 = Integer.toHexString((int) (z * 255));
    String c4 = Integer.toHexString((int) (w * 255));
    if (c1.length() == 1) {
      c1 = "0" + c1;
    }
    if (c2.length() == 1) {
      c2 = "0" + c2;
    }
    if (c3.length() == 1) {
      c3 = "0" + c3;
    }
    if (c4.length() == 1) {
      c4 = "0" + c4;
    }
    return String.format("Colour[%s%s%s/%s]", c1, c2, c3, c4);
  }
}
