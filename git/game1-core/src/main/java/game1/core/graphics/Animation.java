package game1.core.graphics;

import java.util.Arrays;
import java.util.List;

import org.joml.Quaternionf;
import org.joml.Vector3f;

import game1.actors.Tickable;
import game1.core.devtools.StringFormatUtils;
import game1.core.engine.Engine;
import game1.core.graphics.meshes.Mesh;
import game1.core.graphics.meshes.MeshUtils;
import game1.core.infrastructure.Texture;
import game1.core.pipelines.GraphicsPipelineName;
import game1.primitives.Rectangle;

/**
 * An animation consists of a 2D sequence of frames taken from a texture and a tick rate to
 * determine how fast it plays.
 */
public class Animation extends Drawable implements Tickable {

  /**
   * Texture to use.
   */
  protected Texture texture;

  /**
   * Animation frames within the texture.
   */
  protected List<Rectangle> sprites;

  /**
   * Whether this animation is mirrored or not.
   */
  protected boolean mirrored;

  /**
   * Internal tick counter.
   */
  private int numTicks = 0;

  /**
   * The number of ticks it takes to advance the animation by one frame.
   */
  protected int[] tickRate;
  private int tickRateIndex = 0;

  /**
   * The current animation frame.
   */
  private int currentFrame = 0;

  /**
   * Whether the animation should play or not. Used to pause animations.
   */
  private boolean playing = true;

  /**
   * Whether the animation loops or not.
   */
  private boolean looping = true;

  /**
   * Whether the animation is finished or not.
   */
  private boolean finishedPlaying = false;

  /**
   * Total number of ticks from start to finish. Used in cutscene timings.
   */
  private int totalTicks = 0;

  /**
   * The mesh this animation is drawn onto.
   */
  protected Mesh mesh;

  /**
   * Creates a new {@link Animation} from the given data. This method is internal to Game1 and
   * should not be used. If you need to copy an animation use {@link #getClone()}.
   *
   * @param texture
   *          texture
   * @param sprites
   *          sprites within the texture
   * @param tickRate
   *          tickrate for the sprites
   * @param mirrored
   *          if the animation is mirrored
   *
   * @return {@link Animation} created from the given data
   */
  public static Animation internal_create(Texture texture, List<Rectangle> sprites, int[] tickRate,
      boolean mirrored) {
    Rectangle sprite = sprites.get(0);
    Mesh mesh = MeshUtils.internal_createMesh(texture, sprite, mirrored);
    Animation animation = new Animation(GraphicsPipelineName.SCENE_2D, mesh, texture, sprites,
        tickRate, mirrored);
    return animation;
  }

  /**
   * Creates a new {@link Animation} from this animation but mirrored. This method is internal to
   * Game1 and should not be used. If you need to copy an animation use {@link #getClone()}.
   *
   * @return {@link Animation} a mirrored version of this animation
   */
  public Animation internal_createMirror() {
    return internal_create(texture, sprites, tickRate, !mirrored);
  }

  /**
   * Constructs a new repeating animation from the given texture and sprites with the given
   * tickRate.
   *
   * @param texture
   *          a texture object of the spritesheet image that contains the animation data
   * @param sprites
   *          a list of rectangles representing the individual sprites on the texture
   * @param tickRate
   *          array containing how long each animation frame lasts
   * @param mirrored
   *          whether the animation should be mirrored or not
   * @param meshes
   *          meshes to use, or null to create new meshes
   * @see #setOneShot(boolean)
   */
  protected Animation(GraphicsPipelineName pipeline, Mesh mesh, Texture texture,
      List<Rectangle> sprites, int[] tickRate, boolean mirrored) {
    super(pipeline, mesh);
    this.pipeline = pipeline;
    this.mesh = mesh;
    this.texture = texture;
    this.sprites = sprites;
    this.tickRate = tickRate;
    this.mirrored = mirrored;

    if (texture == null) {
      throw new RuntimeException("Texture cannot be null.");
    }
    if (sprites == null || sprites.size() == 0) {
      throw new RuntimeException("Sprites cannot be null or empty.");
    }
    if (tickRate.length != 1 && sprites.size() != tickRate.length) {
      throw new RuntimeException(String.format(
          "Different number of frames and tickRate values (%s vs %s.) Must be equal if non-constant tick rate.",
          sprites.size(), tickRate.length));
    }
    if (tickRate.length == 0) {
      throw new RuntimeException("tickRate must have at least one value.");
    }
    for (int i = 0; i < tickRate.length; i++) {
      if (tickRate[i] <= 0) {
        throw new RuntimeException(
            String.format("tickRate values must be > 0 (tickRate[%s] = %s.)", i, tickRate[i]));
      }
    }
    for (int i = 0; i < tickRate.length; i++) {
      totalTicks += tickRate[i];
    }
  }

  public void setTickRate(int tickRate) {
    setTickRate(new int[] { tickRate });
  }

  public void setTickRate(int[] tickRate) {
    this.tickRate = tickRate;
  }

  public Rectangle getCurrentSprite() {
    return sprites.get(currentFrame);
  }

  /**
   * Sets whether this animation loops or not.
   *
   * @param looping
   *          <code>true</code> if this animation should loop, <code>false</code> otherwise
   */
  public void setLooping(boolean looping) {
    this.looping = looping;
  }

  /**
   * Starts the animation. Note that animations are started by default, so this method is only
   * required if the animation was previously stopped using {@link #stop()}.
   *
   * @see #stop()
   */
  public void start() {
    this.playing = true;
  }

  /**
   * Stops the animation.
   *
   * @see #start()
   */
  public void stop() {
    this.playing = false;
  }

  /**
   * Resets the animation back to the start.
   */
  public void reset() {
    this.numTicks = 0;
    this.tickRateIndex = 0;
    this.currentFrame = 0;
    this.finishedPlaying = false;
    mesh.getMaterial().setSprite(getCurrentSprite());
  }

  /**
   * Advances the animation if enough ticks have passed since the last advance. Does nothing if not
   * enough ticks have elapsed or if the animation is stopped.
   */
  protected void advance() {
    if (playing) {
      numTicks++;
      int time = tickRate[tickRateIndex];
      if (numTicks >= time) {
        numTicks = 0;
        internalAdvance();
        tickRateIndex = tickRateIndex >= tickRate.length - 1 ? 0 : tickRateIndex + 1;
      }
    }
  }

  /**
   * Advances the animation by one frame. If the animation has finished it will loop around, unless
   * looping is false in which case it will stop at the final frame.
   */
  private void internalAdvance() {
    if (looping) {
      if (currentFrame == sprites.size() - 1) {
        currentFrame = 0;
      } else {
        currentFrame++;
      }
    } else {
      // If this is not a looped animation then we will stop on the last frame
      if (currentFrame < sprites.size() - 1) {
        currentFrame++;
      }
      if (currentFrame == sprites.size() - 1) {
        finishedPlaying = true;
      }
    }
    mesh.getMaterial().setSprite(getCurrentSprite());
  }

  /**
   * Gets the current frame number of this animation.
   *
   * @return the current frame number
   */
  protected int getCurrentFrame() {
    return currentFrame;
  }

  /**
   * Gets the total number of frames in this animation.
   *
   * @return the total number of frames in this animation
   */
  protected int getNumFrames() {
    return sprites.size();
  }

  /**
   * Gets the width of the current frame of this animation.
   *
   * @return the width of the current frame of this animation
   */
  @Override
  public float getWidth() {
    return this.getCurrentSprite().w * scale;
  }

  /**
   * Gets the height of the current frame of this animation.
   *
   * @return the height of the current frame of this animation
   */
  @Override
  public float getHeight() {
    return this.getCurrentSprite().h * scale;
  }

  /**
   * Gets the total number of ticks from start to finish. Used in cutscene timings.
   *
   * @return the total number of ticks required to play this animation from start to finish
   */
  public int getTotalTicks() {
    return totalTicks;
  }

  @Override
  public void onTick() {
    if (!Engine.GAME_PAUSED && isVisible) {
      advance();
    }
  }

  @Override
  public String toString() {
    return String.format(
        "Animation[name=%s,position=%s,stencil=%s,textureId=%s,sprites=%s,mirrored=%s,numTicks=%s,tickRate=%s,tickRateIndex=%s,currentFrame=%s,playing=%s,looping=%s,finishedPlaying=%s,totalTicks=%s,offsetX=%s,offsetY=%s,isVisible=%s,mesh=%s]",
        name, StringFormatUtils.toString(position), stencil, texture.getTextureId(), sprites,
        mirrored, numTicks, Arrays.toString(tickRate), tickRateIndex, currentFrame, playing,
        looping, finishedPlaying, totalTicks, offsetX, offsetY, isVisible, mesh.getName());
  }

  @Override
  public Animation getClone() {
    Animation clone = Animation.internal_create(texture, sprites, tickRate, mirrored);
    clone.setOffset(offsetX, offsetY);
    clone.playing = playing;
    if (!playing) {
      clone.stop();
    }
    clone.looping = looping;
    clone.finishedPlaying = finishedPlaying;

    // Copied from Drawable.clone():
    clone.position = new Vector3f(0, 0, 0);
    clone.position.set(position);
    clone.rotation = new Quaternionf();
    clone.rotation.set(rotation);
    clone.scale = scale;
    clone.name = name;
    clone.selected = selected;
    clone.frustumCullingEnabled = frustumCullingEnabled;
    clone.insideFrustum = insideFrustum;
    clone.stencil = stencil;
    clone.isVisible = isVisible;
    return clone;
  }
}
