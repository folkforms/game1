package game1.core.graphics;

import java.util.Arrays;

public class ArrayUtils {

  public static float[] createEmptyFloatArray(int length, float defaultValue) {
    float[] result = new float[length];
    Arrays.fill(result, defaultValue);
    return result;
  }

  public static int[] createEmptyIntArray(int length, int defaultValue) {
    int[] result = new int[length];
    Arrays.fill(result, defaultValue);
    return result;
  }
}
