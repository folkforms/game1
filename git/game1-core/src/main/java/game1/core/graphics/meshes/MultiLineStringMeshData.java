package game1.core.graphics.meshes;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector3i;

import game1.core.graphics.RenderedFont;
import game1.core.infrastructure.Texture;
import game1.primitives.Rectangle;

public class MultiLineStringMeshData extends SimpleMeshData {

  private String text;
  private RenderedFont font;
  private float fontSize;
  private float x, y, lineHeight;
  private int numChars;

  public MultiLineStringMeshData(String text, RenderedFont font, float fontSize) {
    this.text = text;
    this.font = font;
    this.fontSize = fontSize;
    this.texture = font.getTexture();
    setup();
  }

  private void setup() {
    int numLines = 1;
    for (int i = 0; i < text.length(); i++) {
      if (text.charAt(i) == '\n') {
        numLines++;
      }
    }
    lineHeight = (font.getCharHeight('M') + font.getCharSpacing());
    y = (numLines - 1) * lineHeight;
    x = 0;

    for (int i = 0; i < text.length(); i++) {
      if (text.charAt(i) == '\n') {
        nextLine();
      } else if (text.charAt(i) == ' ') {
        x = x + (font.getSpaceWidth() + font.getCharSpacing());
      } else {
        Rectangle r = font.getRectangle(text.charAt(i));
        addSprite(r);
        x = x + (r.w + font.getCharSpacing());
        numChars++;
      }
    }

    description = String.format("%s[text='%s',font=%s]", getClass().getSimpleName(), text, font);
  }

  private void nextLine() {
    x = 0;
    y -= lineHeight;
  }

  private void addSprite(Rectangle r) {
    Vector3f pos0 = new Vector3f(0, 0, 0);
    Vector3f pos1 = new Vector3f(0, r.h * fontSize, 0);
    Vector3f pos2 = new Vector3f(r.w * fontSize, r.h * fontSize, 0);
    Vector3f pos3 = new Vector3f(r.w * fontSize, 0, 0);
    pos0.x += x * fontSize;
    pos1.x += x * fontSize;
    pos2.x += x * fontSize;
    pos3.x += x * fontSize;
    pos0.y += y * fontSize;
    pos1.y += y * fontSize;
    pos2.y += y * fontSize;
    pos3.y += y * fontSize;
    positions.add(pos0);
    positions.add(pos1);
    positions.add(pos2);
    positions.add(pos3);

    Vector3i index0 = new Vector3i(0, 2, 1);
    Vector3i index1 = new Vector3i(0, 3, 2);
    index0.x = index0.x + numChars * 4;
    index0.y = index0.y + numChars * 4;
    index0.z = index0.z + numChars * 4;
    index1.x = index1.x + numChars * 4;
    index1.y = index1.y + numChars * 4;
    index1.z = index1.z + numChars * 4;
    indices.add(index0);
    indices.add(index1);

    Texture texture = font.getTexture();
    float sx = r.x;
    float sy = r.y;
    float sw = r.w;
    float sh = r.h;
    float tx = sx / texture.getWidth();
    float ty = sy / texture.getHeight();
    float txx = (sx + sw) / texture.getWidth();
    float tyy = (sy + sh) / texture.getHeight();
    Vector2f t0 = new Vector2f(tx, tyy);
    Vector2f t1 = new Vector2f(tx, ty);
    Vector2f t2 = new Vector2f(txx, ty);
    Vector2f t3 = new Vector2f(txx, tyy);
    texCoords.add(t0);
    texCoords.add(t1);
    texCoords.add(t2);
    texCoords.add(t3);

    Vector3f normal = new Vector3f(0, 0, 1);
    normals.add(normal);
    normals.add(normal);
    normals.add(normal);
    normals.add(normal);
  }
}
