package game1.core.graphics;

import org.joml.Quaternionf;
import org.joml.Vector3f;

import game1.core.devtools.StringFormatUtils;
import game1.core.graphics.meshes.Mesh;
import game1.core.graphics.meshes.MeshUtils;
import game1.core.infrastructure.Texture;
import game1.core.pipelines.GraphicsPipelineName;
import game1.primitives.Rectangle;

/**
 * Represents a single sprite from a spritesheet.
 */
public class Image extends Drawable {

  /** The image data as texture references */
  protected Texture texture;
  protected Rectangle rectangle;
  protected boolean mirrored;

  /**
   * Constructs a new image from the given texture and rectangle. The image will typically only
   * occupy a portion of the texture.
   *
   * @param texture
   *          a texture object containing the spritesheet data
   * @param sprite
   *          the area within the texture that contains the image data
   * @param mirrored
   *          whether the image should be mirrored or not
   */
  public Image(Texture texture, Rectangle rectangle, boolean mirrored) {
    super(GraphicsPipelineName.SCENE_2D,
        MeshUtils.internal_createMesh(texture, rectangle, mirrored));
    this.texture = texture;
    this.rectangle = rectangle;
    this.mirrored = mirrored;
  }

  @Override
  public String toString() {
    return String.format(
        "core.graphics.Image[name=%s,texture=%s,rectangle=%s,mirrored=%s,position=%s,visible=%s,debugId=%s]@%s",
        name, texture, rectangle, mirrored, StringFormatUtils.toString(position), isVisible,
        debugId, hashCode());
  }

  /**
   * Protected constructor for test fixtures.
   */
  protected Image() {
    super(GraphicsPipelineName.SCENE_2D, new Mesh[0]);
  }

  /**
   * Private constructor for cloning.
   *
   * @param mesh
   *          image mesh
   */
  private Image(Mesh meshes) {
    super(GraphicsPipelineName.SCENE_2D, meshes);
  }

  @Override
  public Image getClone() {
    Mesh mesh = super.internal_getMeshes()[0];
    Image clone = new Image(mesh);
    clone.texture = texture;
    clone.rectangle = rectangle;
    clone.mirrored = mirrored;

    // Copied from Drawable.clone():
    clone.position = new Vector3f(0, 0, 0);
    clone.position.set(position);
    clone.rotation = new Quaternionf();
    clone.rotation.set(rotation);
    clone.scale = scale;
    clone.name = name;
    clone.selected = selected;
    clone.frustumCullingEnabled = frustumCullingEnabled;
    clone.insideFrustum = insideFrustum;
    clone.stencil = stencil;
    clone.isVisible = isVisible;
    return clone;
  }
}
