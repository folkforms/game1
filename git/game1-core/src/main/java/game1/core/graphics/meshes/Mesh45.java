package game1.core.graphics.meshes;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.GL_TEXTURE1;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;
import static org.lwjgl.opengl.GL45C.glCreateBuffers;
import static org.lwjgl.opengl.GL45C.glCreateVertexArrays;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.joml.Matrix4f;
import org.lwjgl.opengl.GL45;

import folkforms.log.Log;
import game1.core.engine.internal.Transformation;
import game1.core.graphics.Animation3D;
import game1.core.graphics.ArrayUtils;
import game1.core.graphics.Drawable;
import game1.core.graphics.Material;
import game1.core.graphics.internal.Animation3DFrame;
import game1.core.infrastructure.Texture;
import game1.core.infrastructure.lwjgl3.internal.LWJGL3ShaderProgram;
import game1.core.pipelines.GraphicsPipelineName;

/**
 * A Mesh is a set of triangles that make up a 3D object.
 *
 * <p>
 * Note: See {@link MeshUtils} javadoc for an explanation of the ordering of vertices and indices.
 * </p>
 */
public class Mesh45 implements Mesh {

  protected final int scene3DVao, scene2DVao, skyboxVao, billboardVao;
  protected final int indicesVbo;
  protected int scene3DVAAttribs = 0, scene2DVAAttribs = 0, skyboxVAAttribs = 0,
      billboardVAAttribs = 0;
  protected final int vertexCount;
  private Material material;
  private float boundingRadius = 1;
  protected String name;
  private float width, height;
  protected float[] positions, textCoords, normals, weights;
  protected int[] indices, jointIndices;
  protected List<Drawable> drawables = new CopyOnWriteArrayList<>();
  protected String id;
  protected int humanReadableId;

  public Mesh45(float[] positions, float[] textCoords, float[] normals, int[] indices,
      int[] jointIndices, float[] weights, String name, Material material) {

    id = MeshId.generate(getClass().getSimpleName(), positions, textCoords, normals, indices,
        jointIndices, weights, material);
    humanReadableId = MeshId.generateHumanReadableId();

    this.positions = positions;
    this.textCoords = textCoords;
    this.normals = normals;
    this.indices = indices;
    this.vertexCount = indices.length;
    if (jointIndices == null) {
      jointIndices = ArrayUtils.createEmptyIntArray(MAX_WEIGHTS * positions.length / 3, 0);
    }
    if (weights == null) {
      weights = ArrayUtils.createEmptyFloatArray(MAX_WEIGHTS * positions.length / 3, 0);
    }
    this.jointIndices = jointIndices;
    this.weights = weights;
    this.name = name;
    this.material = material;

    scene3DVao = glCreateVertexArrays();
    Vertices.attachFloatBuffer(scene3DVao, 0, positions, "vec3");
    Vertices.attachFloatBuffer(scene3DVao, 1, textCoords, "vec2");
    Vertices.attachFloatBuffer(scene3DVao, 2, normals, "vec3");
    Vertices.attachFloatBuffer(scene3DVao, 3, weights, "vec4");
    Vertices.attachIntBuffer(scene3DVao, 4, jointIndices, "vec4");

    scene2DVao = glCreateVertexArrays();
    Vertices.attachFloatBuffer(scene2DVao, 0, positions, "vec3");
    Vertices.attachFloatBuffer(scene2DVao, 1, textCoords, "vec2");
    Vertices.attachFloatBuffer(scene2DVao, 2, normals, "vec3");

    skyboxVao = glCreateVertexArrays();
    Vertices.attachFloatBuffer(skyboxVao, 0, positions, "vec3");
    Vertices.attachFloatBuffer(skyboxVao, 1, textCoords, "vec2");
    Vertices.attachFloatBuffer(skyboxVao, 2, normals, "vec3");

    billboardVao = glCreateVertexArrays();
    Vertices.attachFloatBuffer(billboardVao, 0, positions, "vec3");
    Vertices.attachFloatBuffer(billboardVao, 1, textCoords, "vec2");
    Vertices.attachFloatBuffer(billboardVao, 2, normals, "vec3");

    // Take note of the number of attributes for each VAO
    scene3DVAAttribs = Vertices.getNumAttribs(scene3DVao);
    scene2DVAAttribs = Vertices.getNumAttribs(scene2DVao);
    skyboxVAAttribs = Vertices.getNumAttribs(skyboxVao);
    billboardVAAttribs = Vertices.getNumAttribs(billboardVao);

    // Indices will be bound at render time as an "element array buffer"
    ByteBuffer indicesByteBuffer = Vertices.toByteBuffer(indices);
    indicesVbo = glCreateBuffers();
    GL45.glNamedBufferData(indicesVbo, indicesByteBuffer, GL_STATIC_DRAW);
  }

  // Used by Scene2D pipeline
  @Override
  public void resize(float width, float height) {
    float[] positions = new float[] { //
        0, 0, 0, // 0
        0, height, 0, // 1
        width, height, 0, // 2
        width, 0, 0 // 3
    };
    Vertices.attachFloatBuffer(scene2DVao, 0, positions, "vec3");
    this.width = width;
    this.height = height;
  }

  @Override
  public Material getMaterial() {
    return material;
  }

  @Override
  public void setMaterial(Material material) {
    this.material = material;
  }

  // FIXME FRUSTUM_CULLING: This is never used.
  public float getBoundingRadius() {
    return boundingRadius;
  }

  // FIXME FRUSTUM_CULLING: This is never used.
  public void setBoundingRadius(float boundingRadius) {
    this.boundingRadius = boundingRadius;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public float getWidth() {
    return width;
  }

  @Override
  public float getHeight() {
    return height;
  }

  // Set up state
  protected void initRender(int vao, int numAttribs) {
    GL45.glBindVertexArray(vao);
    for (int i = 0; i < numAttribs; i++) {
      GL45.glEnableVertexArrayAttrib(vao, i);
    }
    GL45.glVertexArrayElementBuffer(vao, indicesVbo);

    Texture texture = material.getTexture();
    if (texture != null) {
      glActiveTexture(GL_TEXTURE0);
      glBindTexture(GL_TEXTURE_2D, texture.getTextureId());
    }
    Texture normalMap = material.getNormalMap();
    if (normalMap != null) {
      glActiveTexture(GL_TEXTURE1);
      glBindTexture(GL_TEXTURE_2D, normalMap.getTextureId());
    }
  }

  // Restore state
  protected void endRender(int vao, int numAttribs) {
    GL45.glBindVertexArray(0);
    for (int i = 0; i < numAttribs; i++) {
      GL45.glDisableVertexArrayAttrib(vao, i);
    }
    GL45.glVertexArrayElementBuffer(vao, 0);

    glBindTexture(GL_TEXTURE_2D, 0);
  }

  // Used for rendering simple objects like 2D and skybox. Projection has already been applied.
  @Override
  public void render(GraphicsPipelineName pipeline) {
    int vao = 0;
    int numAttribs = 0;
    if (pipeline == GraphicsPipelineName.SCENE_2D) {
      vao = scene2DVao;
      numAttribs = scene2DVAAttribs;
    } else {
      vao = skyboxVao;
      numAttribs = skyboxVAAttribs;
    }

    initRender(vao, numAttribs);
    glDrawElements(GL_TRIANGLES, vertexCount, GL_UNSIGNED_INT, 0);
    endRender(vao, numAttribs);
  }

  // Used for rendering non-instanced meshes in a 3D scene
  @Override
  public void renderList(LWJGL3ShaderProgram shader, LWJGL3ShaderProgram sceneShaderProgram,
      Transformation transformation, Matrix4f viewMatrix, Matrix4f lightViewMatrix) {
    initRender(scene3DVao, scene3DVAAttribs);

    for (Drawable drawable : drawables) {
      if (!drawable.isVisible() || !drawable.isInsideFrustum()) {
        continue;
      }

      // Set up data required by the drawable
      sceneShaderProgram.setUniform("selectedNonInstanced", drawable.isSelected() ? 1 : 0);
      Matrix4f modelMatrix = transformation.buildModelMatrix(drawable.getRotation(),
          drawable.getPosition(), drawable.getScale());
      if (viewMatrix != null) {
        Matrix4f modelViewMatrix = transformation.buildModelViewMatrix(modelMatrix, viewMatrix);
        sceneShaderProgram.setUniform("modelViewNonInstancedMatrix", modelViewMatrix);
      }
      Matrix4f modelLightViewMatrix = transformation.buildModelLightViewMatrix(modelMatrix,
          lightViewMatrix);
      shader.setUniform("modelLightViewNonInstancedMatrix", modelLightViewMatrix);

      if (drawable instanceof Animation3D) {
        Animation3D animated = (Animation3D) drawable;
        Animation3DFrame frame = animated.getCurrentAnimation().getCurrentFrame();
        shader.setUniform("jointsMatrix", frame.getJointMatrices());
      }

      // Render the drawable
      glDrawElements(GL_TRIANGLES, vertexCount, GL_UNSIGNED_INT, 0);
    }
    endRender(scene3DVao, scene3DVAAttribs);
  }

  @Override
  public void cleanup() {
    glDisableVertexAttribArray(0);

    // Delete the VBOs
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // for (int vboId : vboIdList) {
    // glDeleteBuffers(vboId);
    // }

    // Delete the texture
    // Log.temp("Mesh.cleanup: Do we need to delete textures? Why did I remove Texture.cleanup()?");
    // Log.temp("Mesh.cleanup: Should we be calling normalMap.cleanup() as well?");
    // Texture texture = material.getTexture();
    // if (texture != null) {
    // texture.cleanup();
    // GL30.glDeleteTextures(textureId);
    // }

    // Delete the VAO
    glBindVertexArray(0);
    glDeleteVertexArrays(-1);
  }

  // FIXME MESHES: Not used...
  @Override
  public void deleteBuffers() {
    glDisableVertexAttribArray(0);

    // Delete the VBOs
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    // for (int vboId : vboIdList) {
    // glDeleteBuffers(vboId);
    // }

    // Delete the VAO
    glBindVertexArray(0);
    glDeleteVertexArrays(-1);
  }

  /**
   * Do not use this unless you need to change materials or stretch (not scale) a mesh. It is
   * considered a different instance and will slow rendering. If you want to copy an object you can
   * use {@link Drawable#clone} which will re-use the same instanced mesh.
   */
  @Override
  public Mesh clone() {
    Texture texture = this.getMaterial().getTexture();
    Mesh mesh = new Mesh45(positions, textCoords, normals, indices, jointIndices, weights, name,
        new Material(texture));
    return mesh;
  }

  @Override
  public String toString() {
    return String.format("Mesh[id=%s,name=%s,material=%s]@%s", humanReadableId, name, material,
        hashCode());
  }

  @Override
  public void debug_dumpData() {
    Log.info("Mesh: %s", toString());
    Log.info("positions: %s", Arrays.toString(positions));
    Log.info("textCoords: %s", Arrays.toString(textCoords));
    Log.info("normals: %s", Arrays.toString(normals));
    Log.info("indices: %s", Arrays.toString(indices));
    if (material == null) {
      Log.info("Material: null");
    } else {
      material.debug_dumpData();
    }
  }

  @Override
  public void link(Drawable d) {
    drawables.add(d);
  }

  @Override
  public void unlink(Drawable d) {
    drawables.remove(d);
  }

  @Override
  public List<Drawable> listDrawables() {
    return drawables;
  }

  @Override
  public float[] getPositions() {
    return positions;
  }

  @Override
  public int[] getIndices() {
    return indices;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public int getHumanReadableId() {
    return humanReadableId;
  }

  @Override
  public boolean equals(Object other) {
    if (other == null) {
      return false;
    } else if (other instanceof Mesh45) {
      Mesh45 otherMesh = (Mesh45) other;
      return id.equals(otherMesh.getId());
    } else {
      return false;
    }
  }

  @Override
  public int hashCode() {
    BigInteger bigInt = new BigInteger(getId().getBytes());
    return bigInt.intValue();
  }
}
