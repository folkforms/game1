package game1.core.graphics;

import java.util.Map;
import java.util.Optional;

import game1.actors.Tickable;
import game1.core.engine.Engine;
import game1.core.graphics.internal.Animation3DFrames;
import game1.core.pipelines.GraphicsPipelineName;
import game1.core.resources.loaders.assimp.AnimatedMeshesData;

// FIXME ANIMATIONS: This needs to include a speed (tickRate) as well. I don't know if Blender will
// provide one. If not then we can pick a default and print a warning, and we can check the folder
// when loading for a properties file with a value.
public class Animation3D extends Drawable implements Tickable {

  private Map<String, Animation3DFrames> animations;
  private Animation3DFrames currentAnimation;

  public Animation3D(GraphicsPipelineName pipeline, AnimatedMeshesData data) {
    super(pipeline, data.getMeshes());
    this.animations = data.getAnimations();
    Optional<Map.Entry<String, Animation3DFrames>> entry = animations.entrySet().stream()
        .findFirst();
    currentAnimation = entry.isPresent() ? entry.get().getValue() : null;
  }

  public void setCurrentAnimation(String name) {
    this.currentAnimation = animations.get(name);
  }

  public Animation3DFrames getCurrentAnimation() {
    return currentAnimation;
  }

  @Override
  public void onTick() {
    if (!Engine.GAME_PAUSED) {
      currentAnimation.advance();
    }
  }

  public int debug_getFrameNumber() {
    return currentAnimation.debug_getCurrentFrameNumber();
  }

  public int debug_getTotalFrames() {
    return currentAnimation.debug_getTotalFrames();
  }
}
