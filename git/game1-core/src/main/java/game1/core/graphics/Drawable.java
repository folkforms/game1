package game1.core.graphics;

import java.awt.Container;

import org.joml.Quaternionf;
import org.joml.Vector3f;

import game1.actors.Actor;
import game1.actors.HasScale;
import game1.actors.HasSize;
import game1.actors.Moveable;
import game1.core.devtools.StringFormatUtils;
import game1.core.engine.Engine;
import game1.core.graphics.meshes.Mesh;
import game1.core.pipelines.GraphicsPipelineName;
import game1.core.pipelines.Stencil2d;

/**
 * A Drawable contains the data to draw something on the screen. It can use any pipeline so can be
 * 3D, 2D, billboard, etc. It contains a single position, rotation and scale, plus a list of meshes.
 */
public class Drawable implements Actor, Moveable, HasSize, HasScale {

  protected Vector3f position = new Vector3f(0, 0, 0);
  protected Quaternionf rotation = new Quaternionf();
  protected float scale = 1;
  protected boolean isVisible = true;

  /**
   * X offset. Used to line up related animations or adjust images inside containers.
   */
  protected float offsetX = 0;

  /**
   * Y offset. Used to line up related animations or adjust images inside containers.
   */
  protected float offsetY = 0;

  // Name field so we can more easily identify it in lists
  protected String name = "unknown";
  // When the Drawables associated with an actor change we need to know the parent so that the
  // GameState can go through the scene, removing all of the old actors and adding the new ones.
  protected boolean selected;

  // Frustum culling
  protected boolean frustumCullingEnabled = true;
  protected boolean insideFrustum = true;
  protected Vector3f aabMin, aabMax;

  protected GraphicsPipelineName pipeline;
  protected Mesh[] meshes = new Mesh[0];
  protected Colour colour = new Colour(1, 1, 1);

  // Stencil
  protected Stencil2d stencil = null;

  protected boolean isCollisionMesh = false;

  protected float width, height;

  protected String debugId = null;

  public Drawable(GraphicsPipelineName pipeline, Mesh... meshes) {
    this.pipeline = pipeline;
    this.meshes = meshes;
    // Drawables should always have meshes, but sometimes tests pass an empty mesh array
    boolean hasMeshes = meshes != null && meshes.length > 0;
    name = "Drawable with no meshes";
    if (hasMeshes) {
      name = "meshes[0]: " + meshes[0].getName();
      setAAB();
      setWidthAndHeightFromAAB();
    }
  }

  private void setWidthAndHeightFromAAB() {
    width = (aabMax.x - aabMin.x) * scale;
    height = (aabMax.y - aabMin.y) * scale;
  }

  /**
   * Resize a 2D Drawable. This has no effect on 3D Drawables.
   *
   * @param width
   *          new width
   * @param height
   *          new height
   */
  public void resize(float width, float height) {
    for (Mesh m : meshes) {
      m.resize(width, height);
    }
    setWidthAndHeightFromMesh();
  }

  private void setWidthAndHeightFromMesh() {
    width = meshes[0].getWidth() * scale;
    height = meshes[0].getHeight() * scale;
  }

  public GraphicsPipelineName getPipeline() {
    return pipeline;
  }

  public Mesh[] internal_getMeshes() {
    return meshes;
  }

  @Override
  public Vector3f getPosition() {
    return position;
  }

  @Override
  public void setPosition(float x, float y, float z) {
    position.x = x;
    position.y = y;
    position.z = z;
  }

  @Override
  public float getScale() {
    return scale;
  }

  public void setScale(float scale) {
    this.scale = scale;
  }

  public Quaternionf getRotation() {
    return rotation;
  }

  public void setRotation(Quaternionf newRotation) {
    this.rotation.set(newRotation);
  }

  /**
   * Rotates this Drawable about the local X, Y and Z axes. Positive values rotate clockwise.
   *
   * @param xDegrees
   *          the number of degrees to rotate about the local X axis
   * @param yDegrees
   *          the number of degrees to rotate about the local Y axis
   * @param zDegrees
   *          the number of degrees to rotate about the local Z axis
   */
  public void rotate(int xDegrees, int yDegrees, int zDegrees) {
    rotation.rotateLocalX((float) Math.toRadians(xDegrees));
    rotation.rotateLocalY((float) Math.toRadians(yDegrees));
    rotation.rotateLocalZ((float) Math.toRadians(zDegrees));
  }

  public void setCollisionMesh(boolean isCollisionMesh) {
    this.isCollisionMesh = isCollisionMesh;
  }

  public boolean isCollisionMesh() {
    return isCollisionMesh;
  }

  public void setName(String format, Object... args) {
    this.name = String.format(format, args);
  }

  @Override
  public String toString() {
    return String.format("Drawable[name='%s',pos=%s,visible=%s,debugId='%s']", name,
        StringFormatUtils.toString(position), isVisible, debugId);
  }

  public boolean isSelected() {
    return selected;
  }

  public void setSelected(boolean newValue) {
    selected = newValue;
  }

  public boolean isFrustumCullingEnabled() {
    return frustumCullingEnabled;
  }

  public void setFrustumCullingEnabled(boolean newValue) {
    this.frustumCullingEnabled = newValue;
  }

  public boolean isInsideFrustum() {
    return insideFrustum;
  }

  public void setInsideFrustum(boolean newValue) {
    this.insideFrustum = newValue;
  }

  public boolean isVisible() {
    if (!isCollisionMesh) {
      return isVisible;
    } else {
      return Engine.SHOW_COLLISION_MESHES;
    }
  }

  @Override
  public void setVisible(boolean newValue) {
    isVisible = newValue;
  }

  /**
   * Gets the {@link Colour} for this {@link Drawable}. Items in the 2D pipeline will have their
   * {@link Mesh} colour multiplied by this colour.
   *
   * @return the {@link Colour} for this {@link Drawable}
   */
  public Colour getColour() {
    return colour;
  }

  /**
   * Sets the colour for this {@link Drawable}. Items in the 2D pipeline will have their
   * {@link Mesh} colour multiplied by this colour.
   *
   * @param colour
   *          new colour to set
   */
  public void setColour(Colour colour) {
    this.colour = colour;
  }

  /**
   * Gets the width of this Drawable.
   *
   * <p>
   * Note: This method only works with 2D Drawables.
   * </p>
   *
   * @return the width of this Drawable
   */
  @Override
  public float getWidth() {
    return width;
  }

  /**
   * Gets the height of this Drawable.
   *
   * <p>
   * Note: This method only works with 2D Drawables.
   * </p>
   *
   * @return the height of this Drawable
   */
  @Override
  public float getHeight() {
    return height;
  }

  public void cleanup() {
    // Log.temp("FIXME Drawable.cleanup not implemented yet!");
    // FIXME CLEANUP
    // for (Mesh mesh : meshes) {
    // mesh.cleanup();
    // }
  }

  /**
   * Clones the Drawable.
   *
   * @return a clone of the Drawable
   */
  @Override
  public Drawable getClone() {
    return internal_clone(false);
  }

  /**
   * Clones the Drawable.
   *
   * @return a clone of the Drawable
   * @deprecated Use {@link #getClone()} instead
   */
  @Override
  @Deprecated
  public Drawable clone() {
    return internal_clone(false);
  }

  /**
   * Clones the Drawable, and also clones its meshes. You should use {@link #clone()} instead of
   * this method wherever possible. This method should be used with caution as it can tank the
   * framerate.
   *
   * @return a clone of the Drawable
   */
  public Drawable deepClone() {
    return internal_clone(true);
  }

  private Drawable internal_clone(boolean cloneMeshes) {
    /*
     * Generally speaking you shouldn't clone the meshes as that will eliminate any gains from
     * instancing and possibly tank the framerate. If you need to resize one Drawable's meshes
     * without resizing others you should create a copy earlier in the process, e.g. register the
     * same image from the spritesheet twice with different references.
     *
     * The option to clone the meshes exists for a handful of scenarios where the above is not
     * practical advice. For example: When using two SpeechBubbles at the same time a problem arises
     * as they are sharing the same speechbubble graphics. Your options are to either clone the
     * meshes, or else register multiple copies of the speechbubble graphics and add some sort of
     * object to keep track of free instances of the speechbubble graphics. The latter is more code
     * to maintain so I'm not going to do it unless framerate becomes a problem.
     */
    Mesh[] clonedMeshes = meshes;
    if (cloneMeshes) {
      clonedMeshes = new Mesh[meshes.length];
      for (int i = 0; i < meshes.length; i++) {
        clonedMeshes[i] = meshes[i].clone();
      }
    }
    Drawable clone = new Drawable(pipeline, clonedMeshes);
    clone.position = new Vector3f(0, 0, 0);
    clone.position.set(position);
    clone.rotation = new Quaternionf();
    clone.rotation.set(rotation);
    clone.scale = scale;
    clone.name = name;
    clone.selected = selected;
    clone.frustumCullingEnabled = frustumCullingEnabled;
    clone.insideFrustum = insideFrustum;
    clone.colour = colour;
    clone.stencil = stencil;
    return clone;
  }

  private void setAAB() {
    float minX = Float.MAX_VALUE;
    float minY = Float.MAX_VALUE;
    float minZ = Float.MAX_VALUE;
    float maxX = Float.MIN_VALUE;
    float maxY = Float.MIN_VALUE;
    float maxZ = Float.MIN_VALUE;
    for (int i = 0; i < meshes.length; i++) {
      int size = meshes[i].getPositions().length;
      for (int j = 0; j < size; j += 3) {
        float px = meshes[i].getPositions()[j];
        float py = meshes[i].getPositions()[j + 1];
        float pz = meshes[i].getPositions()[j + 2];
        if (px < minX) {
          minX = px;
        }
        if (py < minY) {
          minY = py;
        }
        if (pz < minZ) {
          minZ = pz;
        }
        if (px > maxX) {
          maxX = px;
        }
        if (py > maxY) {
          maxY = py;
        }
        if (pz > maxZ) {
          maxZ = pz;
        }
      }
    }
    aabMin = new Vector3f(minX, minY, minZ);
    aabMax = new Vector3f(maxX, maxY, maxZ);
  }

  public Vector3f getAABMin() {
    return aabMin;
  }

  public Vector3f getAABMax() {
    return aabMax;
  }

  /**
   * Gets the {@link Stencil2d} associated with this Drawable, or {@code null} if there is no
   * stencil.
   *
   * @return a {@link Stencil2d} or {@code null}
   */
  public Stencil2d getStencil() {
    return stencil;
  }

  /**
   * Sets the {@link Stencil2d} to use for this Drawable, or {@code null} to use no stencil.
   *
   * @param newStencil
   *          {@link Stencil2d} or {@code null}
   */
  public void setStencil(Stencil2d newStencil) {
    stencil = newStencil;
  }

  public float getOffsetX() {
    return offsetX;
  }

  public float getOffsetY() {
    return offsetY;
  }

  /**
   * Offset the position of the drawable. This is typically used to line it up with other
   * animations. For example, if multiple animations are children of a player avatar they should
   * each set their offsets so they all line up relative to the avatar's {@code x,y} position. The
   * avatar is responsible for updating their positions. It can also be used to easily offset images
   * within a {@link Container}.
   *
   * @see Drawable#setPosition(float, float)
   */
  public void setOffset(float newOffsetX, float newOffsetY) {
    offsetX = newOffsetX;
    offsetY = newOffsetY;
  }

  public String getDebugId() {
    return debugId;
  }

  public void setDebugId(String debugId) {
    this.debugId = debugId;
  }

  public String debug_getName() {
    return name;
  }
}
