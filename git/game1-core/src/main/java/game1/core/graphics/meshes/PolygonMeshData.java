package game1.core.graphics.meshes;

import java.util.List;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector3i;

public class PolygonMeshData extends SimpleMeshData {

  private int indexCount = 0;

  public PolygonMeshData(List<Vector3f> points) {
    description = String.format("%s[%s]", getClass().getSimpleName(), points);
    splitIntoTriangles(points);
  }

  private void splitIntoTriangles(List<Vector3f> points) {
    while (points.size() > 3) {
      // Find right-most x value
      int rightMostXIndex = findRightMostXPoint(points);
      // Find the two points that it connects to
      int p1Index = -1;
      int p2Index = -1;
      if (rightMostXIndex == points.size() - 1) {
        p1Index = points.size() - 2;
        p2Index = 0;
      } else {
        p1Index = rightMostXIndex - 1;
        p2Index = rightMostXIndex + 1;
      }

      addTriangle(points.get(p1Index), points.get(rightMostXIndex), points.get(p2Index));
      points.remove(rightMostXIndex);
    }
    Vector3f f1 = points.get(0);
    Vector3f f2 = points.get(1);
    Vector3f f3 = points.get(2);
    addTriangle(f1, f2, f3);
  }

  private int findRightMostXPoint(List<Vector3f> points) {
    float maxX = -1_000_000;
    int maxXIndex = -1;
    for (int i = 0; i < points.size(); i++) {
      if (points.get(i).x > maxX) {
        maxX = points.get(i).x;
        maxXIndex = i;
      }
    }
    return maxXIndex;
  }

  private void addTriangle(Vector3f v1, Vector3f v2, Vector3f v3) {
    Vector3f pos0 = new Vector3f(v1.x, v1.y, v1.z);
    Vector3f pos1 = new Vector3f(v2.x, v2.y, v2.z);
    Vector3f pos2 = new Vector3f(v3.x, v3.y, v3.z);
    positions.add(pos0);
    positions.add(pos1);
    positions.add(pos2);

    Vector3i index0 = new Vector3i(0 + indexCount, 2 + indexCount, 1 + indexCount);
    indices.add(index0);
    indexCount += 3;

    float tx = 0;
    float ty = 0;
    float txx = 1;
    float tyy = 1;
    Vector2f t0 = new Vector2f(tx, tyy);
    Vector2f t1 = new Vector2f(tx, ty);
    Vector2f t2 = new Vector2f(txx, ty);
    Vector2f t3 = new Vector2f(txx, tyy);
    texCoords.add(t0);
    texCoords.add(t1);
    texCoords.add(t2);
    texCoords.add(t3);

    Vector3f normal = new Vector3f(0, 0, 1);
    normals.add(normal);
    normals.add(normal);
    normals.add(normal);
  }
}
