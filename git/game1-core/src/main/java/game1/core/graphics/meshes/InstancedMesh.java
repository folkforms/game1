package game1.core.graphics.meshes;

import org.joml.Matrix4f;

import game1.core.engine.internal.Transformation;

public interface InstancedMesh extends Mesh {

  void renderListInstanced(Transformation transformation, Matrix4f viewMatrix,
      Matrix4f lightViewMatrix);

  void renderBillboardListInstanced(Transformation transformation, Matrix4f viewMatrix,
      Matrix4f lightViewMatrix);
}
