package game1.core.graphics.lights;

import org.joml.Vector3f;

import game1.actors.Actor;
import game1.core.graphics.Colour;

/**
 * Directional lights cast light in parallel lines from very far away. The sun is one such example.
 */
public class DirectionalLight implements Actor, Light {

  private Vector3f colour;
  private Vector3f direction;
  private float intensity;
  /**
   * Represents the boundaries within which the light and shadows will be calculated.
   */
  private OrthoCoords orthoCoords;
  /**
   * Represents the distance of the directional light (e.g. the sun) from the scene.
   */
  private float lightSourceDistance;
  private float lightAngle1, lightAngle2;
  private float lightAngleDelta = 1;

  /**
   * Creates a new directional light with the given colour, direction and intensity. Direction is
   * expressed in world coordinates using the right-hand rule, e.g.
   * <code>new Vector3f(-1,0,0)</code> would result in a light coming from the left,
   * <code>new Vector3f(0,1,0)</code> would result in a light coming from above,
   * <code>new Vector3f(-1,1,0)</code> would result in a light coming from the top-left, etc.
   *
   * @param colour
   *          colour of the light
   * @param direction
   *          direction of the light
   * @param intensity
   *          intensity of the light
   */
  public DirectionalLight(Vector3f colour, Vector3f direction, float intensity) {
    this.colour = colour;
    this.direction = direction;
    this.intensity = intensity;
    this.orthoCoords = new OrthoCoords();
    this.lightSourceDistance = 1;
    this.lightAngle1 = (float) Math.toDegrees(Math.acos(direction.x));
    this.lightAngle2 = (float) Math.toDegrees(Math.acos(direction.y));
  }

  /**
   * Creates a new directional light with the given colour, direction and intensity. Direction is
   * expressed in world coordinates using the right-hand rule, e.g.
   * <code>new Vector3f(-1,0,0)</code> would result in a light coming from the left,
   * <code>new Vector3f(0,1,0)</code> would result in a light coming from above,
   * <code>new Vector3f(-1,1,0)</code> would result in a light coming from the top-left, etc.
   *
   * @param colour
   *          colour of the light
   * @param direction
   *          direction of the light
   * @param intensity
   *          intensity of the light
   * @return a new directional light with the given colour, direction and intensity
   */
  public static DirectionalLight create(Colour colour, Vector3f direction, float intensity) {
    return new DirectionalLight(colour.toVector3f(), direction, intensity);
  }

  public DirectionalLight(DirectionalLight light) {
    this(new Vector3f(light.getColour()), new Vector3f(light.getDirection()), light.getIntensity());
  }

  public Vector3f getColour() {
    return colour;
  }

  public void setColour(Vector3f colour) {
    this.colour = colour;
  }

  public Vector3f getDirection() {
    return direction;
  }

  public void setDirection(Vector3f direction) {
    this.direction = direction;
  }

  public float getIntensity() {
    return intensity;
  }

  public void setIntensity(float intensity) {
    this.intensity = intensity;
  }

  public OrthoCoords getOrthoCoords() {
    return orthoCoords;
  }

  public void setOrthoCoords(float left, float right, float bottom, float top, float near,
      float far) {
    orthoCoords.left = left;
    orthoCoords.right = right;
    orthoCoords.bottom = bottom;
    orthoCoords.top = top;
    orthoCoords.near = near;
    orthoCoords.far = far;
  }

  public float getLightSourceDistance() {
    return lightSourceDistance;
  }

  public void setLightSourceDistance(float newLightSourceDistance) {
    this.lightSourceDistance = newLightSourceDistance;
  }

  public static class OrthoCoords {
    public float left;
    public float right;
    public float bottom;
    public float top;
    public float near;
    public float far;
  }

  public void increaseLightAngle1() {
    modifyLightAngle1(lightAngleDelta);
  }

  public void decreaseLightAngle1() {
    modifyLightAngle1(-lightAngleDelta);
  }

  private void modifyLightAngle1(float delta) {
    lightAngle1 += delta;
    if (lightAngle1 < 0) {
      lightAngle1 = 0;
    } else if (lightAngle1 > 180) {
      lightAngle1 = 180;
    }
    direction.x = direction.x;
    direction.y = (float) Math.sin(Math.toRadians(lightAngle1));
    direction.z = (float) Math.cos(Math.toRadians(lightAngle1));
    direction.normalize();
    lightAngle1 = (float) Math.toDegrees(Math.acos(direction.z));
  }

  public void increaseLightAngle2() {
    modifyLightAngle2(lightAngleDelta);
  }

  public void decreaseLightAngle2() {
    modifyLightAngle2(-lightAngleDelta);
  }

  private void modifyLightAngle2(float delta) {
    lightAngle2 += delta;
    if (lightAngle2 < 0) {
      lightAngle2 = 0;
    } else if (lightAngle2 > 180) {
      lightAngle2 = 180;
    }
    direction.x = (float) Math.cos(Math.toRadians(lightAngle2));
    direction.y = (float) Math.sin(Math.toRadians(lightAngle2));
    direction.z = direction.z;
    direction.normalize();
    lightAngle2 = (float) Math.toDegrees(Math.acos(direction.x));
  }

  /**
   * Creates a typical directional light.
   *
   * @return a directional light
   */
  public static Actor createDefault() {
    DirectionalLight directionalLight = DirectionalLight.create(Colour.fromHex("#fff"),
        new Vector3f(0.5f, 0.5f, 0.5f), 1);
    directionalLight.setLightSourceDistance(10);
    directionalLight.setOrthoCoords(-25.0f, 25.0f, -25.0f, 8.0f, -1.0f, 20.0f);
    return directionalLight;
  }
}
