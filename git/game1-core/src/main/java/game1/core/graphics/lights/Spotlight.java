package game1.core.graphics.lights;

import org.joml.Vector3f;

import game1.actors.Actor;

/**
 * A spotlight is a light source that emits light from a point in space in a cone.
 */
public class Spotlight implements Actor, Light {

  private PointLight pointLight;
  private Vector3f coneDirection;
  private float coneDegrees;
  private float cutoff;

  /**
   * Creates a new spotlight using the position, colour, intensity and attenuation of the given
   * {@link PointLight}, that shines a cone of light with a direction of <code>coneDirection</code>
   * and a size of <code>coneDegrees</code>. Note that <code>coneDirection</code> is the direction
   * it is shining towards, i.e. 0,-1,0 means it is shining straight down.
   *
   * @param pointLight
   *          point light to use
   * @param coneDirection
   *          direction of the spotlight cone
   * @param coneDegrees
   *          size of the spotlight cone, in degrees
   */
  public Spotlight(PointLight pointLight, Vector3f coneDirection, float coneDegrees) {
    this.pointLight = pointLight;
    this.coneDirection = coneDirection;
    this.coneDegrees = coneDegrees;
    calculateCutOff();
  }

  /**
   * Creates a new spotlight from the given spotlight's data. This is used when rendering lights.
   *
   * @param spotlight
   *          spotlight to copy
   */
  public Spotlight(Spotlight spotlight) {
    this(new PointLight(spotlight.getPointLight()), new Vector3f(spotlight.getConeDirection()),
        spotlight.getConeDegrees());
  }

  private void calculateCutOff() {
    this.cutoff = (float) Math.cos(Math.toRadians(coneDegrees / 2));
  }

  public PointLight getPointLight() {
    return pointLight;
  }

  public void setPointLight(PointLight pointLight) {
    this.pointLight = pointLight;
  }

  public Vector3f getConeDirection() {
    return coneDirection;
  }

  public void setConeDirection(Vector3f coneDirection) {
    this.coneDirection = coneDirection;
  }

  public float getConeDegrees() {
    return coneDegrees;
  }

  /**
   * Sets the angle of the spotlight cone, in degrees.
   *
   * @param coneDegrees
   *          the angle of the spotlight cone, in degrees
   */
  public void setConeDegrees(float coneDegrees) {
    this.coneDegrees = coneDegrees;
    calculateCutOff();
  }

  public float getIntensity() {
    return pointLight.intensity;
  }

  public void setIntensity(float intensity) {
    pointLight.intensity = intensity;
  }

  /**
   * Gets the cutoff value of the spotlight cone, i.e. cos(cone degrees / 2 in radians). Used by the
   * rendering pipeline.
   *
   * @return the cutoff value of the spotlight cone
   */
  public float internal_getCutoff() {
    return cutoff;
  }
}
