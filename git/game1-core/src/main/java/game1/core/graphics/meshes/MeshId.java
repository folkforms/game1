package game1.core.graphics.meshes;

import java.util.Arrays;

import game1.core.engine.Game1RuntimeException;
import game1.core.graphics.Material;
import game1.variables.VariablesFactory;

public class MeshId {

  private static int humanReadableId = 0;

  public static int generateHumanReadableId() {
    return humanReadableId++;
  }

  public static String generate(String className, float[] positions, float[] texCoords,
      float[] normals, int[] indices, int[] jointIndices, float[] weights, Material material) {
    return String.format(
        "%s[positions=%s,texCoords=%s,normals=%s,indices=%s,joinIndices=%s,weights=%s,material=%s]",
        className, Arrays.toString(positions), Arrays.toString(texCoords), Arrays.toString(normals),
        Arrays.toString(indices), Arrays.toString(jointIndices), Arrays.toString(weights),
        material);
  }

  public static String generateClassName(int instances) {
    String openGLVersion = VariablesFactory.getInstance().get("game1.graphics.open_gl_version");
    if (openGLVersion.equals("3.3")) {
      if (instances > 0) {
        return InstancedMesh33.class.getSimpleName();
      } else {
        return Mesh33.class.getSimpleName();
      }
    } else if (openGLVersion.equals("4.5")) {
      if (instances > 0) {
        return InstancedMesh45.class.getSimpleName();
      } else {
        return Mesh45.class.getSimpleName();
      }
    } else {
      throw new Game1RuntimeException(
          "Unsupported OpenGL version %s. Only versions 4.5 and 3.3 are supported.", openGLVersion);
    }
  }
}
