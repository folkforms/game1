package game1.core.graphics.meshes;

import java.util.ArrayList;
import java.util.List;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector3i;

import game1.core.graphics.Material;
import game1.core.infrastructure.Texture;

public abstract class SimpleMeshData {

  protected String description;
  protected List<Vector3f> positions = new ArrayList<>();
  protected List<Vector2f> texCoords = new ArrayList<>();
  protected List<Vector3i> indices = new ArrayList<>();
  protected List<Vector3f> normals = new ArrayList<>();
  protected Texture texture = null;

  public Mesh create() {
    return create(true);
  }

  public Mesh create(boolean useMeshCache) {
    Material material;
    if (texture != null) {
      material = new Material(texture);
    } else {
      material = new Material();
    }
    float[] positionsArray = convertVector3f(positions);
    float[] texCoordsArray = convertVector2f(texCoords);
    float[] normalsArray = convertVector3f(normals);
    int[] indicesArray = convertVector3i(indices);
    return MeshUtils.internal_createMesh(positionsArray, texCoordsArray, normalsArray, indicesArray,
        null, null, 1, material, description, useMeshCache);
  }

  private float[] convertVector3f(List<Vector3f> list) {
    float[] floats = new float[list.size() * 3];
    for (int i = 0; i < list.size(); i++) {
      Vector3f v = list.get(i);
      floats[i * 3 + 0] = v.x;
      floats[i * 3 + 1] = v.y;
      floats[i * 3 + 2] = v.z;
    }
    return floats;
  }

  private float[] convertVector2f(List<Vector2f> list) {
    float[] floats = new float[list.size() * 2];
    for (int i = 0; i < list.size(); i++) {
      Vector2f v = list.get(i);
      floats[i * 2 + 0] = v.x;
      floats[i * 2 + 1] = v.y;
    }
    return floats;
  }

  private int[] convertVector3i(List<Vector3i> list) {
    int[] ints = new int[list.size() * 3];
    for (int i = 0; i < list.size(); i++) {
      Vector3i v = list.get(i);
      ints[i * 3 + 0] = v.x;
      ints[i * 3 + 1] = v.y;
      ints[i * 3 + 2] = v.z;
    }
    return ints;
  }
}
