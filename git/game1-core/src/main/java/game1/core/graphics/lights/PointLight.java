package game1.core.graphics.lights;

import org.joml.Vector3f;

import game1.actors.Actor;
import game1.core.graphics.Colour;

/**
 * A point light is a light source that emits light uniformly from a point in space in all
 * directions.
 */
public class PointLight implements Actor, Light {

  private Vector3f colour;
  private Vector3f position;
  protected float intensity;
  private Attenuation attenuation;

  /**
   * Creates a new point light with the given colour, position, intensity and attenuation.
   *
   * @param colour
   *          colour of the light
   * @param position
   *          position of the light
   * @param intensity
   *          intensity of the light
   * @param attenuation
   *          attenuation, or falloff, of the light, which can be expressed as a
   *          constant/linear/exponent value
   * @see {@link Attenuation}
   */
  public PointLight(Vector3f colour, Vector3f position, float intensity, Attenuation attenuation) {
    this.colour = colour;
    this.position = position;
    this.intensity = intensity;
    this.attenuation = attenuation;
  }

  /**
   * Creates a new point light with the given colour, position, intensity, and an attenuation type
   * of constant with a value of 1.
   *
   * @param colour
   *          colour of the light
   * @param position
   *          position of the light
   * @param intensity
   *          intensity of the light
   */
  public PointLight(Vector3f colour, Vector3f position, float intensity) {
    this(colour, position, intensity, new Attenuation(1, 0, 0));
  }

  /**
   * Creates a new point light with the given colour, position, intensity and attenuation.
   *
   * @param colour
   *          colour of the light
   * @param position
   *          position of the light
   * @param intensity
   *          intensity of the light
   * @param attenuation
   *          attenuation, or falloff, of the light, which can be expressed as a
   *          constant/linear/exponent value
   * @see {@link Attenuation}
   * @return a new point light with the given colour, position, intensity and attenuation
   */
  public PointLight(Colour colour, Vector3f position, float intensity, Attenuation attenuation) {
    this(colour.toVector3f(), position, intensity, attenuation);
  }

  /**
   * Creates a new point light with the given colour, position, intensity, and an attenuation of
   * constant 1.
   *
   * @param colour
   *          colour of the light
   * @param position
   *          position of the light
   * @param intensity
   *          intensity of the light
   * @return a new point light with the given colour, position, intensity, and an attenuation type
   *         of constant with a value of 1
   */
  public PointLight(Colour colour, Vector3f position, float intensity) {
    this(colour.toVector3f(), position, intensity, new Attenuation(1, 0, 0));
  }

  /**
   * Creates a new point light from the given point light's data. This is used when rendering
   * lights.
   *
   * @param pointLight
   *          point light to copy
   */
  public PointLight(PointLight pointLight) {
    this(new Vector3f(pointLight.getColour()), new Vector3f(pointLight.getPosition()),
        pointLight.getIntensity(), pointLight.getAttenuation());
  }

  public Vector3f getColour() {
    return colour;
  }

  public void setColour(Vector3f colour) {
    this.colour = colour;
  }

  public Vector3f getPosition() {
    return position;
  }

  public void setPosition(Vector3f position) {
    this.position = position;
  }

  public float getIntensity() {
    return intensity;
  }

  public void setIntensity(float intensity) {
    this.intensity = intensity;
  }

  public Attenuation getAttenuation() {
    return attenuation;
  }

  public void setAttenuation(Attenuation attenuation) {
    this.attenuation = attenuation;
  }

  public static class Attenuation {

    private float constant;
    private float linear;
    private float exponent;

    public Attenuation(float constant, float linear, float exponent) {
      this.constant = constant;
      this.linear = linear;
      this.exponent = exponent;
    }

    public float getConstant() {
      return constant;
    }

    public void setConstant(float constant) {
      this.constant = constant;
    }

    public float getLinear() {
      return linear;
    }

    public void setLinear(float linear) {
      this.linear = linear;
    }

    public float getExponent() {
      return exponent;
    }

    public void setExponent(float exponent) {
      this.exponent = exponent;
    }
  }
}
