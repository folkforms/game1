package game1.core.graphics;

import game1.events.Command;

/**
 * An animation that executes a command when the animation finishes.
 *
 * @see {@link Animation#setLooping(boolean)}
 */
public class AnimationWithCommand extends Animation {

  /**
   * Command to execute when the animation finishes.
   */
  private Command command;

  /**
   * Whether the command has been executed or not.
   */
  private boolean executed = false;

  /**
   * Creates a new AnimationWithCommand object.
   *
   * @param animation
   *          animation data
   * @param command
   *          command to execute
   */
  public AnimationWithCommand(Animation animation, Command command) {
    super(animation.pipeline, animation.mesh, animation.texture, animation.sprites,
        animation.tickRate, animation.mirrored);
    this.command = command;
  }

  @Override
  public void advance() {
    super.advance();
    if (!executed && getCurrentFrame() == getNumFrames() - 1) {
      command.execute();
      executed = true;
    }
  }
}
