package instanceoftest;

import folkforms.log.Log;

/**
 * Tests how long it takes to check:
 * <ul>
 * <li><code>instanceof</code> InstancedMesh</li>
 * <li>A public <code>isInstanced</code> field in the variable</li>
 * <li>Calling <code>isInstanced()</code></li>
 * <li>A map with an "instanceOf" property</li>
 * </ul>
 *
 * Note that a more complicated class seems to affect the timings. When I added the Map it more than
 * doubled the time for all tests.
 */
public class InstanceOfTest {

  public static void main(String[] args) {
    int numItems = 1_000_000;
    Object1[] arr = new Object1[numItems];
    for (int i = 0; i < numItems; i++) {
      if (Math.random() < 0.5) {
        arr[i] = new Object1();
      } else {
        arr[i] = new Object2();
      }
    }

    testMap(numItems, arr);
    testInstanceOf(numItems, arr);
    testMethod(numItems, arr);
    testField(numItems, arr);
  }

  private static void testInstanceOf(int numItems, Object1[] arr) {
    long startTime = System.nanoTime();
    int o2Count = 0;
    for (int i = 0; i < numItems; i++) {
      if (arr[i] instanceof Object2) {
        o2Count++;
      }
    }
    long endTime = System.nanoTime();
    Log.info("instanceof time = %s (%s true)", endTime - startTime, o2Count);
  }

  private static void testMethod(int numItems, Object1[] arr) {
    long startTime = System.nanoTime();
    int o2Count = 0;
    for (int i = 0; i < numItems; i++) {
      if (arr[i].isInstanced()) {
        o2Count++;
      }
    }
    long endTime = System.nanoTime();
    Log.info("method time     = %s (%s true)", endTime - startTime, o2Count);
  }

  private static void testField(int numItems, Object1[] arr) {
    long startTime = System.nanoTime();
    int o2Count = 0;
    for (int i = 0; i < numItems; i++) {
      if (arr[i].isInstanced) {
        o2Count++;
      }
    }
    long endTime = System.nanoTime();
    Log.info("field time      = %s (%s true)", endTime - startTime, o2Count);
  }

  private static void testMap(int numItems, Object1[] arr) {
    long startTime = System.nanoTime();
    int o2Count = 0;
    for (int i = 0; i < numItems; i++) {
      if (arr[i].getProperty("isInstanced").equals("true")) {
        o2Count++;
      }
    }
    long endTime = System.nanoTime();
    Log.info("map time        = %s (%s true)", endTime - startTime, o2Count);
  }
}
