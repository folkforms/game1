package instanceoftest;

public class Object2 extends Object1 {
  public Object2() {
    isInstanced = true;
    properties.put("isInstanced", "true");
  }
}
