package instanceoftest;

import java.util.HashMap;
import java.util.Map;

public class Object1 {
  public boolean isInstanced = false;

  protected Map<String, String> properties = new HashMap<>();

  public Object1() {
    properties.put("isInstanced", "false");
  }

  public Object getProperty(String key) {
    return properties.get(key);
  }

  public boolean isInstanced() {
    return isInstanced;
  }
}
