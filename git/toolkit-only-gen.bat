@echo off
cls

cd toolkit
call mvn clean install
if %ERRORLEVEL% neq 0 exit /b %ERRORLEVEL%
cd ..
