package game1.utils.images.iconcreator;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 * GUI for IconCreator.
 */
@SuppressWarnings("serial")
public class IconCreatorGui extends JFrame implements ActionListener {

  private JTextField inputFile;
  private JButton convertButton;

  public static void main(String[] args) {
    IconCreatorGui gui = new IconCreatorGui();
    gui.setVisible(true);
  }

  public IconCreatorGui() {
    setTitle("Icon Creator");
    setSize(550, 100);
    setLocation(100, 100);
    setDefaultCloseOperation(EXIT_ON_CLOSE);

    setLayout(new BorderLayout(4, 4));
    addInputArea();
    addButtons();
  }

  private void addInputArea() {
    inputFile = new JTextField("");
    add(inputFile, BorderLayout.CENTER);
  }

  private void addButtons() {
    convertButton = new JButton("Convert");
    convertButton.addActionListener(this);
    add(convertButton, BorderLayout.SOUTH);
  }

  @Override
  public void actionPerformed(ActionEvent ae) {
    if (ae.getSource() == convertButton) {
      convert();
    }
  }

  private void convert() {
    String filename = inputFile.getText();
    try {
      IconCreator.main(new String[] { filename });
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }
}
