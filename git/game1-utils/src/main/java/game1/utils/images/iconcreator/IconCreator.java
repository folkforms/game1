package game1.utils.images.iconcreator;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;

import folkforms.log.Log;
import net.sf.image4j.codec.ico.ICOEncoder;

/**
 * Takes a square input file of any size and generates six output files: 5 pngs of size 16, 32, 48,
 * 128 and 256, respectively; plus a .ico file containing the previous 5 pngs. Note that a 16x16 png
 * is the suggested input format.
 */
public class IconCreator {

  private static String INPUT_FILE;
  private static String INPUT_FILENAME_NO_EXTENSION;
  private static String OUTPUT_FOLDER;
  private static final int[] OUTPUT_SIZES = { 16, 32, 48, 128, 256 };

  public static void main(String[] args) throws IOException {
    // Log.setLevel(Log.LEVEL_DEBUG);
    checkArgs(args);
    createIcons();
  }

  private static void checkArgs(String[] args) {
    Log.debug("checkArgs");

    if (args.length == 0) {
      Log.error("Missing argument: input file.");
      System.exit(1);
    }

    INPUT_FILE = args[0].replace('\\', '/');
    INPUT_FILE = removeQuotes(INPUT_FILE);
    OUTPUT_FOLDER = INPUT_FILE.substring(0, INPUT_FILE.lastIndexOf('/'));
    INPUT_FILENAME_NO_EXTENSION = INPUT_FILE.substring(0, INPUT_FILE.lastIndexOf('.'));
    if (INPUT_FILENAME_NO_EXTENSION.contains("/")) {
      INPUT_FILENAME_NO_EXTENSION = INPUT_FILENAME_NO_EXTENSION
          .substring(INPUT_FILENAME_NO_EXTENSION.lastIndexOf('/') + 1);
    }
    Log.debug("    INPUT_FILE = %s", INPUT_FILE);
    Log.debug("    INPUT_FILENAME_ONLY_NO_EXT = %s", INPUT_FILENAME_NO_EXTENSION);
    Log.debug("    OUTPUT_FOLDER = %s", OUTPUT_FOLDER);
  }

  /**
   * Remove quotes around the filename.
   *
   * @param filename
   *          filename to fix
   * @return the filename minus the surrounding quotes, if any
   */
  private static String removeQuotes(String filename) {
    if (filename.startsWith("\"") && filename.endsWith("\"")) {
      filename = filename.substring(1, filename.length() - 1);
    }
    return filename;
  }

  private static void createIcons() throws IOException {
    BufferedImage inputImage = ImageIO.read(new File(INPUT_FILE));
    validateInputImageSize(inputImage);
    for (int size : OUTPUT_SIZES) {
      createResizedImage(inputImage, size);
    }
    createIcoFile();
  }

  private static void validateInputImageSize(BufferedImage inputImage) {
    // Check input image is a square
    if (inputImage.getWidth() != inputImage.getHeight()) {
      Log.error("Input image was not a square (was %s x %s)", inputImage.getWidth(),
          inputImage.getHeight());
      System.exit(1);
    }
    // Check input image is a valid size
    boolean found = false;
    for (int size : OUTPUT_SIZES) {
      if (inputImage.getWidth() == size) {
        found = true;
        break;
      }
    }
    if (!found) {
      Log.error("Input image must be a square, with one of the following sizes: %s",
          Arrays.toString(OUTPUT_SIZES));
      System.exit(1);
    }
  }

  private static void createResizedImage(BufferedImage inputImage, int size)
      throws FileNotFoundException, IOException {
    Log.debug("createResizedImage");

    Image scaled = inputImage.getScaledInstance(size, size, Image.SCALE_DEFAULT);
    BufferedImage outputImage = new BufferedImage(size, size, inputImage.getType());
    Graphics g = outputImage.getGraphics();
    g.drawImage(scaled, 0, 0, null);

    String outputFile = String.format("%s/%s_%s.png", OUTPUT_FOLDER, INPUT_FILENAME_NO_EXTENSION,
        size);
    Log.debug("    outputFile = %s", outputFile);
    ImageIO.write(outputImage, "png", new FileOutputStream(new File(outputFile)));
  }

  private static void createIcoFile() throws IOException {
    Log.debug("createIcoFile");
    List<BufferedImage> images = new ArrayList<>();
    for (int size : OUTPUT_SIZES) {
      String inputImage = String.format("%s/%s_%s.png", OUTPUT_FOLDER, INPUT_FILENAME_NO_EXTENSION,
          size);
      Log.debug("    inputImage [%s] = %s", size, inputImage);
      BufferedImage bi = ImageIO.read(new File(inputImage));
      images.add(bi);
    }
    String outputFile = OUTPUT_FOLDER + "/" + INPUT_FILENAME_NO_EXTENSION + ".ico";
    Log.debug("    outputFile = %s", outputFile);
    ICOEncoder.write(images, new File(outputFile));
  }
}
