package game1.utils.images.skybox;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

import folkforms.fileutils.FileUtils;

/**
 * Converts 6 images into a cubemap for a skybox. Used to turn the output from
 * <a href="https://jaxry.github.io/panorama-to-cubemap/">panorama-to-cubemap</a> into a usable
 * form.
 */
public class StitchSkybox {

  private static boolean ADD_NAMES = false;

  public static void main(String[] args) throws IOException {
    // FIXME Add proper args...
    String argInputFiles = "/path/to/files/*.png";
    String argOutputFile = "skybox-s1.png";

    // Expects 6 images: nx.png, px.png, etc.
    List<String> glob = new FileUtils().glob(argInputFiles);
    String nxFile = getFile(glob, "nx.png");
    String nyFile = getFile(glob, "ny.png");
    String nzFile = getFile(glob, "nz.png");
    String pxFile = getFile(glob, "px.png");
    String pyFile = getFile(glob, "py.png");
    String pzFile = getFile(glob, "pz.png");

    BufferedImage nx = ImageIO.read(new File(nxFile));
    BufferedImage ny = ImageIO.read(new File(nyFile));
    BufferedImage nz = ImageIO.read(new File(nzFile));
    BufferedImage px = ImageIO.read(new File(pxFile));
    BufferedImage py = ImageIO.read(new File(pyFile));
    BufferedImage pz = ImageIO.read(new File(pzFile));

    int blockWidth = nx.getWidth();
    int blockHeight = nx.getHeight();
    BufferedImage output = new BufferedImage(blockWidth * 3, blockHeight * 2,
        BufferedImage.TYPE_4BYTE_ABGR);
    Graphics g = output.getGraphics();
    // In reading order the images go: nx,pz,px / nz,py,ny
    drawImage(g, nx, 0, 0, "nx");
    drawImage(g, pz, blockWidth, 0, "pz");
    drawImage(g, px, blockWidth * 2, 0, "px");
    drawImage(g, nz, 0, blockHeight, "nz");
    drawImage(g, rotate180(rotateCounterClockwise90(py)), blockWidth, blockHeight, "py");
    drawImage(g, ny, blockWidth * 2, blockHeight, "ny");

    ImageIO.write(output, "png", new File(argOutputFile));
  }

  private static void drawImage(Graphics g, BufferedImage image, int x, int y, String name) {
    g.drawImage(image, x, y, null);

    if (ADD_NAMES) {
      g.setColor(Color.GREEN);
      g.setFont(Font.decode("Verdana-Plain-100"));
      g.drawString(name, x + image.getWidth() / 2, y + image.getWidth() / 2);
    }
  }

  private static String getFile(List<String> glob, String name) throws IOException {
    for (String s : glob) {
      if (s.endsWith(name)) {
        return s;
      }
    }
    throw new IOException(String.format("No file named %s could be found.", name));
  }

  public static BufferedImage rotateCounterClockwise90(BufferedImage src) {
    int width = src.getWidth();
    int height = src.getHeight();

    BufferedImage dest = new BufferedImage(height, width, src.getType());

    Graphics2D graphics2D = dest.createGraphics();
    graphics2D.translate((height - width) / 2, (height - width) / 2);
    graphics2D.rotate(-Math.PI / 2, height / 2, width / 2);
    graphics2D.drawRenderedImage(src, null);

    return dest;
  }

  public static BufferedImage rotate180(BufferedImage src) {
    int width = src.getWidth();
    int height = src.getHeight();

    BufferedImage dest = new BufferedImage(height, width, src.getType());

    Graphics2D graphics2D = dest.createGraphics();
    graphics2D.translate((height - width) / 2, (height - width) / 2);
    graphics2D.rotate(Math.PI, height / 2, width / 2);
    graphics2D.drawRenderedImage(src, null);

    return dest;
  }
}
