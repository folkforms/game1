package game1.utils.fontgenerator;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.joml.Vector3i;

import folkforms.fileutils.FileUtils;

/**
 * Takes a png font file and a char map (e.g. "ABCD\nEFGH\nIJKL") as input, and generates a
 * "fontdata" file. The "fontdata" file is used by spritesheets to map sprites to letters in the
 * font.
 *
 * Expects an image filename of the form /path/to/image/name_WxHxB.png, where W and H are width and
 * height of the rectangle that can fit the largest character, and B is the bleed distance between
 * characters. The bleed distance is added to stop texture bleed when scaling spritesheets.
 */
public class GenRenderedFont {

  GenRenderedFont(String imageFilename, String charMapFilename, String outputFolder)
      throws IOException {
    BufferedImage image = ImageIO.read(new File(imageFilename));
    FileUtils fileUtils = new FileUtils();
    String filenameOnlyNoExt = fileUtils.removeExtension(fileUtils.filenameOnly(imageFilename));
    String outputFile = filenameOnlyNoExt + ".fontdata";

    Vector3i gridSizes = getGridSizes(imageFilename);
    int gridW = gridSizes.x;
    int gridH = gridSizes.y;
    int bleedBuffer = gridSizes.z;
    String charMap = loadCharMap(charMapFilename);
    List<Rectangle> rectangles = new ArrayList<>();
    int x = 0;
    int y = 0;
    int maxX = image.getWidth() / (gridW + bleedBuffer);
    for (int i = 0; i < charMap.toCharArray().length; i++) {
      Rectangle r = new Rectangle(x * (gridW + bleedBuffer), y * (gridH + bleedBuffer), gridW,
          gridH);
      // Trim the rectangle to exclude all wholly-transparent columns on the RHS
      int xpos = x * (gridW + bleedBuffer);
      int ypos = y * (gridH + bleedBuffer);
      r = getTrimmedRectangle(image.getSubimage(xpos, ypos, gridW, gridH), r);
      if (r != null) {
        rectangles.add(r);
      } else {
        rectangles.add(new Rectangle(0, 0, gridW, gridH));
      }
      x++;
      if (x >= maxX) {
        x = 0;
        y++;
      }
    }

    writeFontMapData(charMap, rectangles, outputFile, outputFolder);
    copyImageFile(imageFilename, outputFolder);
  }

  private static Vector3i getGridSizes(String imageFilename) {
    String s = imageFilename;
    int underscore = s.lastIndexOf('_');
    s = s.substring(underscore + 1);
    s = s.substring(0, s.length() - 4);
    String[] tokens = s.split("x");
    int gridW = Integer.parseInt(tokens[0]);
    int gridH = Integer.parseInt(tokens[1]);
    int bleedBuffer = Integer.parseInt(tokens[2]);
    return new Vector3i(gridW, gridH, bleedBuffer);
  }

  private static Rectangle getTrimmedRectangle(BufferedImage subimage, Rectangle r) {

    // Trim right
    boolean allTransparent = true;
    do {
      for (int y = 0; y < subimage.getHeight(); y++) {
        int rgb = subimage.getRGB((int) r.getWidth() - 1, y);
        if ((rgb >> 24) != 0x00) {
          allTransparent = false;
          break;
        }
      }
      if (allTransparent) {
        r.width--;
        if (r.width == 0) {
          // Entire sub-image is transparent!
          return null;
        }
      }
    } while (allTransparent);

    // Trim left
    allTransparent = true;
    int x = 0;
    do {
      for (int y = 0; y < subimage.getHeight(); y++) {
        int rgb = subimage.getRGB(x, y);
        if ((rgb >> 24) != 0x00) {
          allTransparent = false;
          break;
        }
      }
      if (allTransparent) {
        r.x++;
        r.width--;
        x++;
      }
    } while (allTransparent);

    return r;
  }

  private static String loadCharMap(String inputCharMapFilename) throws IOException {
    BufferedReader in = new BufferedReader(new FileReader(inputCharMapFilename));
    StringBuffer sb = new StringBuffer();
    String s = "";
    while ((s = in.readLine()) != null) {
      sb.append(s);
    }
    in.close();
    return sb.toString();
  }

  private static void writeFontMapData(String charMap, List<Rectangle> rectangles,
      String outputFile, String outputFolder) throws IOException {
    List<String> outputData = new ArrayList<>();
    char[] charArray = charMap.toCharArray();
    for (int i = 0; i < charArray.length; i++) {
      Rectangle r = rectangles.get(i);
      String c = Character.toString(charArray[i]);
      String s = c + " " + r.x + "," + r.y + "," + r.width + "," + r.height;
      outputData.add(s);
    }
    writeFontMapDataToFile(outputData, outputFile, outputFolder);
  }

  private static void writeFontMapDataToFile(List<String> output, String outputFile,
      String outputFolder) throws IOException {
    outputFile = outputFile.replace('\\', '/');
    outputFile = outputFile.substring(outputFile.lastIndexOf('/') + 1);
    String outputFullPath = outputFolder + "/" + outputFile;
    PrintWriter p = new PrintWriter(outputFullPath);
    for (String s : output) {
      p.println(s);
    }
    p.close();
    System.out.printf("Wrote file %s%n", outputFullPath);
  }

  private static void copyImageFile(String imagePath, String outputFolder) throws IOException {
    String outputFile = imagePath.replace('\\', '/');
    outputFile = outputFile.substring(outputFile.lastIndexOf('/') + 1);
    String outputFullPath = outputFolder + "/" + outputFile;
    Files.copy(new File(imagePath).toPath(), new File(outputFullPath).toPath());
    System.out.printf("Wrote file %s%n", outputFullPath);
  }
}
