package game1.utils.fontgenerator;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import folkforms.log.Log;
import folkforms.textio.TextIO;

public class GenerateFontGui extends JFrame implements ActionListener {

  private JTextField inputImageField, inputCharMapField, outputFontDataFolder, scaleField;
  private JButton generateButton;
  private JTextArea charMapField;
  private JButton loadButton, editImageButton, editCharMapButton;

  private String inputImageFile, inputCharMapFile;
  private BufferedImage image;
  private double scale;

  public GenerateFontGui() throws IOException {
    this("../game1-core/src/main/resources/images/fonts/SmallFont_5x7x1.png",
        "../game1-core/src/main/resources/images/fonts/SmallFont_5x7x1_CharMap.txt");
  }

  public GenerateFontGui(String inputImagePath, String inputCharMapPath) throws IOException {
    setTitle("Generate Font");
    setSize(400, 400);
    setLocation(100, 100);
    setDefaultCloseOperation(EXIT_ON_CLOSE);

    setLayout(new BorderLayout(2, 2));
    getContentPane().add(createInputFields(inputImagePath, inputCharMapPath), BorderLayout.NORTH);
    getContentPane().add(createPreviewFields(), BorderLayout.CENTER);
    getContentPane().add(createButtons(), BorderLayout.SOUTH);

    setVisible(true);
  }

  private Component createInputFields(String inputImagePath, String inputCharMapPath) {
    JPanel panel = new JPanel(new BorderLayout(2, 2));
    JPanel labelsPanel = new JPanel(new GridLayout(0, 1, 2, 2));
    labelsPanel.add(new JLabel("Input image "));
    labelsPanel.add(new JLabel("Input char map "));
    labelsPanel.add(new JLabel("Output folder "));
    labelsPanel.add(new JLabel("Scale "));
    panel.add(labelsPanel, BorderLayout.WEST);
    JPanel fieldsPanel = new JPanel(new GridLayout(0, 1, 2, 2));
    fieldsPanel.add(inputImageField = new JTextField(inputImagePath));
    fieldsPanel.add(inputCharMapField = new JTextField(inputCharMapPath));
    fieldsPanel.add(outputFontDataFolder = new JTextField("./output"));
    fieldsPanel.add(scaleField = new JTextField("1"));
    panel.add(fieldsPanel, BorderLayout.CENTER);
    loadButton = new JButton("Load");
    loadButton.addActionListener(this);
    panel.add(loadButton, BorderLayout.SOUTH);
    return panel;
  }

  private Component createPreviewFields() {
    JPanel p = new JPanel(new BorderLayout(2, 2));
    p.add(createImagePanel(), BorderLayout.CENTER);
    p.add(createCharMapPanel(), BorderLayout.EAST);
    return p;
  }

  private Component createImagePanel() {
    JPanel outerPanel = new JPanel(new BorderLayout(2, 2));
    JPanel p = new JPanel() {
      @Override
      public void paint(Graphics g) {
        g.setColor(Color.gray);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        g.setColor(Color.darkGray);
        for (int i = 0; i < this.getWidth(); i += 20) {
          for (int j = 0; j < this.getHeight(); j += 10) {
            if (j % 20 == 0) {
              g.fillRect(i + 10, j, 10, 10);
            } else {
              g.fillRect(i, j, 10, 10);
            }
          }
        }
        if (image != null) {
          g.drawImage(image, 0, 0, (int) (image.getWidth() * scale),
              (int) (image.getHeight() * scale), null);
        }
      }
    };
    outerPanel.add(p, BorderLayout.CENTER);

    editImageButton = new JButton("Edit in Gimp");
    editImageButton.addActionListener(this);
    editImageButton.setEnabled(false);
    outerPanel.add(editImageButton, BorderLayout.SOUTH);

    return outerPanel;
  }

  private Component createCharMapPanel() {
    JPanel outerPanel = new JPanel(new BorderLayout(2, 2));
    charMapField = new JTextArea("");
    charMapField.setFont(Font.decode("Courier New-Plain-18"));
    charMapField.setEditable(false);
    outerPanel.add(charMapField, BorderLayout.CENTER);
    editCharMapButton = new JButton("Edit in Notepad");
    editCharMapButton.addActionListener(this);
    editCharMapButton.setEnabled(false);
    outerPanel.add(editCharMapButton, BorderLayout.SOUTH);
    return outerPanel;
  }

  private Component createButtons() {
    JPanel p = new JPanel(new GridLayout(1, 0, 2, 2));
    generateButton = new JButton("Generate Font Data File");
    generateButton.addActionListener(this);
    generateButton.setEnabled(false);
    p.add(generateButton);
    return p;
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    final String inputImageFilename = inputImageField.getText();
    final String inputCharMapFilename = inputCharMapField.getText();
    final String outputFolder = outputFontDataFolder.getText();
    try {
      scale = Double.parseDouble(scaleField.getText());
    } catch (NumberFormatException ex) {
      Log.showPopup(JOptionPane.ERROR_MESSAGE, "Invalid value", "Invalid value for scale: '%s'",
          scaleField.getText());
      return;
    }

    try {

      if (event.getSource() == loadButton) {

        this.inputImageFile = inputImageFilename;
        this.inputCharMapFile = inputCharMapFilename;
        reloadImageAndCharMap();
        editImageButton.setEnabled(true);
        editCharMapButton.setEnabled(true);
        generateButton.setEnabled(true);

      } else if (event.getSource() == generateButton) {

        new GenRenderedFont(inputImageFilename, inputCharMapFilename, outputFolder);

      } else if (event.getSource() == editImageButton) {

        final String[] cmdArray = new String[] { "C:\\Program Files\\GIMP 2\\bin\\gimp-2.8.exe",
            inputImageFilename };
        Process p = Runtime.getRuntime().exec(cmdArray);
        p.waitFor();
        reloadImageAndCharMap();

      } else if (event.getSource() == editCharMapButton) {

        final String[] cmdArray = new String[] { "notepad.exe", inputCharMapFilename };
        Process p = Runtime.getRuntime().exec(cmdArray);
        p.waitFor();
        reloadImageAndCharMap();
      }

    } catch (IOException | InterruptedException ex) {
      Log.error(ex);
      System.exit(1);
    }
  }

  private void reloadImageAndCharMap() throws IOException {
    try {
      image = ImageIO.read(new File(inputImageFile));
    } catch (IOException ex) {
      Log.showPopup(JOptionPane.ERROR_MESSAGE, "Error loading file",
          "Could not load image file: %s (dir = %s)", inputImageFile,
          System.getProperty("user.dir"));
    }

    try {
      String charMap = new TextIO().readAsString(inputCharMapFile, "\n");
      charMapField.setText(charMap);
    } catch (IOException ex) {
      Log.showPopup(JOptionPane.ERROR_MESSAGE, "Error loading file",
          "Could not load char map file: %s (dir = %s)", inputCharMapFile,
          System.getProperty("user.dir"));
    }

    revalidate();
    repaint();
  }

  public static void main(String[] args) throws IOException {
    new GenerateFontGui();
  }
}
