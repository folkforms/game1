package game1.utils.fontgenerator;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import folkforms.log.Log;

/**
 * Converts a GameMaker font image into my own font image format.
 */
public class ConvertGameMakerFont {

  ConvertGameMakerFont(String inputFile, String outputFile) throws IOException {

    // Load image
    BufferedImage inputImage = ImageIO.read(new File(inputFile));

    // Char height is input image height - 1
    int CHAR_HEIGHT = inputImage.getHeight() - 1;

    // Go through the drawing, find all marker pixels
    List<Integer> markers = new ArrayList<>();
    for (int x = 0; x < inputImage.getWidth(); x++) {
      Log.temp("inputImage.getRGB(%s, 0) = %s", x, inputImage.getRGB(x, 0));
      // If pixel is white
      if (inputImage.getRGB(x, 0) == -1) {
        markers.add(x);
      }
    }
    markers.add(inputImage.getWidth());

    // Find widest char
    int CHAR_WIDTH = 0;
    for (int i = 0; i < markers.size() - 1; i++) {
      int tempCharWidth = markers.get(i + 1) - markers.get(i);
      if (CHAR_WIDTH < tempCharWidth) {
        CHAR_WIDTH = tempCharWidth;
      }
    }
    Log.temp("markers.size() = %s", markers.size());
    Log.temp("Char width: %s", CHAR_WIDTH);
    Log.temp("Char height: %s", CHAR_HEIGHT);

    // Create a new output image with a 10x10 grid
    BufferedImage outputImage = new BufferedImage(CHAR_WIDTH * 10, CHAR_HEIGHT * 10,
        BufferedImage.TYPE_4BYTE_ABGR);

    // Redraw each character onto the grid
    int numChars = 0;
    Graphics g = outputImage.getGraphics();
    int outputX = 0;
    int outputY = 0;
    for (int i = 0; i < markers.size() - 1; i++) {
      int inputX1 = markers.get(i) + 1;
      int inputX2 = markers.get(i + 1);
      int inputY1 = 1;
      int inputY2 = inputImage.getHeight();

      if (inputX2 - inputX1 != 0) {
        BufferedImage subimage = inputImage.getSubimage(inputX1, inputY1, inputX2 - inputX1,
            inputY2 - inputY1);
        g.drawImage(subimage, outputX, outputY, null);

        outputX += CHAR_WIDTH;
        numChars += 1;
        if (numChars % 10 == 0) {
          outputX = 0;
          outputY += CHAR_HEIGHT;
        }
      }
    }

    // Adjust filename
    outputFile = outputFile.substring(0, outputFile.length() - 4);
    outputFile += "_" + CHAR_WIDTH + "x" + CHAR_HEIGHT + ".png";

    // Save the image
    ImageIO.write(outputImage, "png", new File(outputFile));
  }

  public static void main(String[] args) throws IOException {
    if (args.length != 2) {
      System.out.println("Usage:");
      System.out.println("    java ConvertGameMakerFont <input file> <output file>");
      System.exit(1);
    }
    String inputFile = args[0];
    String outputFile = args[1];

    new ConvertGameMakerFont(inputFile, outputFile);
  }
}
