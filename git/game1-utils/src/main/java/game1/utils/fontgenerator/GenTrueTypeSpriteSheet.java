package game1.utils.fontgenerator;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.imageio.ImageIO;

/**
 * Generates a spritesheet containing all characters of a font. Expects input of "FontName
 * FontSize".
 */
public class GenTrueTypeSpriteSheet {

  private final String OUTPUT_FOLDER = "./output";
  private final String CHAR_MAP = "DefaultCharMap.txt";

  private String fontName, fontSize;
  private int offsetX, offsetY;
  private boolean debug;

  private int gridW, gridH;
  private BufferedImage image;

  public GenTrueTypeSpriteSheet(String fontName, String fontSize, String xOffsetStr,
      String yOffsetStr, boolean debug) throws IOException {
    this.fontName = fontName;
    this.fontSize = fontSize;
    offsetX = Integer.parseInt(xOffsetStr);
    offsetY = Integer.parseInt(yOffsetStr);
    this.debug = debug;
  }

  public BufferedImage generate() throws IOException {
    checkFontExists();

    if (!new File(OUTPUT_FOLDER).exists()) {
      new File(OUTPUT_FOLDER).mkdir();
    }

    Font font = Font.decode(fontName + "-" + fontSize);
    adjustFontName();

    int cols = 10;
    int rows = 10;
    String charMap = loadCharMap();

    // Calculate the maximum size of a character at this font size
    BufferedImage tempImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_ARGB);
    Graphics g = tempImage.getGraphics();
    g.setFont(font);
    FontMetrics fm = g.getFontMetrics();
    int maxCharWidth = -1;
    for (char c : charMap.toCharArray()) {
      maxCharWidth = Math.max(maxCharWidth, fm.charWidth(c));
    }
    // Add a buffer of 10 as otherwise the offsetX will push the widest chars
    // off the right-hand edge of the boxes
    gridW = maxCharWidth + 10;
    gridH = fm.getHeight();

    // Draw one of each letter onto the image at regular intervals
    image = new BufferedImage(gridW * cols, gridH * rows, BufferedImage.TYPE_INT_ARGB);
    g = image.getGraphics();
    g.setFont(font);
    g.setColor(debug ? Color.red : Color.white);
    int x = 0;
    int y = 0;
    int debugColour = 0;
    Color[] debugColours = { Color.yellow, Color.gray, Color.green };
    for (char c : charMap.toCharArray()) {
      if (debug) {
        g.setColor(debugColours[debugColour]);
        g.fillRect(x * gridW, y * gridH, gridW, gridH);
        debugColour++;
        if (debugColour >= debugColours.length) {
          debugColour = 0;
        }
        g.setColor(Color.red);
      }

      g.drawString(Character.toString(c), x * gridW + offsetX, y * gridH + gridH + offsetY);

      x++;
      if (x >= cols) {
        x = 0;
        y++;
      }
    }
    return image;
  }

  public String save() throws IOException {
    final String filename = fontName + "_" + fontSize + "Point_" + gridW + "x" + gridH + ".png";

    // Write the image and the java file
    String outputPath = OUTPUT_FOLDER + "/" + filename;
    ImageIO.write(image, "png", new File(outputPath));
    return outputPath;
  }

  private void checkFontExists() throws IOException {
    GraphicsEnvironment g = GraphicsEnvironment.getLocalGraphicsEnvironment();
    String[] fonts = g.getAvailableFontFamilyNames();
    for (int i = 0; i < fonts.length; i++) {
      if (fonts[i].equals(fontName)) {
        return;
      }
    }
    throw new IOException("Font '" + fontName + "' does not exist!");
  }

  /**
   * Changes the font name to be a valid java file identifier
   */
  private void adjustFontName() {
    String modified = "";
    for (int i = fontName.length() - 1; i >= 0; i--) {
      String s = fontName.substring(i, i + 1);
      // FIXME Change to discarding all non-alphanumeric
      if (!s.equals(" ")) {
        modified = s + modified;
      }
    }
    fontName = modified;
  }

  private String loadCharMap() throws IOException {
    BufferedReader in = new BufferedReader(new FileReader(new File("./fontgenerator/" + CHAR_MAP)));
    StringBuffer sb = new StringBuffer();
    String s = "";
    while ((s = in.readLine()) != null) {
      sb.append(s);
    }
    in.close();
    return sb.toString();
  }

}
