package game1.utils.fontgenerator;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import folkforms.log.Log;

public class GenTrueTypeSpriteSheetGui extends JFrame implements ActionListener {

  private JComboBox<String> fontDropdown;
  private JTextField fontSizeField, xOffsetField, yOffsetField, outputFolderField, scaleField;
  private JButton generateButton, saveButton, openGenerateFontGuiButton;
  private JCheckBox debugCheckbox;
  private BufferedImage image;
  private GenTrueTypeSpriteSheet genTrueTypeSpriteSheet;
  private double scale = 0.5;
  private String savedFilename;

  public GenTrueTypeSpriteSheetGui() {
    setTitle("Generate TrueType Sprite Sheet");
    setSize(400, 400);
    setLocation(100, 100);
    setDefaultCloseOperation(EXIT_ON_CLOSE);

    setLayout(new BorderLayout(2, 2));
    getContentPane().add(createInputFields(), BorderLayout.NORTH);
    getContentPane().add(createOutputPanel(), BorderLayout.CENTER);
    getContentPane().add(createButtons(), BorderLayout.SOUTH);

    setVisible(true);
  }

  private Component createInputFields() {
    JPanel p = new JPanel(new BorderLayout(2, 2));

    p.add(createFontList(), BorderLayout.NORTH);

    JPanel labelsPanel = new JPanel(new GridLayout(0, 1, 2, 2));
    JPanel fieldsPanel = new JPanel(new GridLayout(0, 1, 2, 2));
    labelsPanel.add(new JLabel("Font size "));
    fontSizeField = new JTextField("72");
    fieldsPanel.add(fontSizeField);
    labelsPanel.add(new JLabel("X offset "));
    xOffsetField = new JTextField("0");
    fieldsPanel.add(xOffsetField);
    labelsPanel.add(new JLabel("Y offset "));
    yOffsetField = new JTextField("0");
    fieldsPanel.add(yOffsetField);
    labelsPanel.add(new JLabel("Output folder "));
    outputFolderField = new JTextField("output");
    fieldsPanel.add(outputFolderField);
    labelsPanel.add(new JLabel("Scale "));
    scaleField = new JTextField("1");
    fieldsPanel.add(scaleField);
    p.add(labelsPanel, BorderLayout.WEST);
    p.add(fieldsPanel, BorderLayout.CENTER);

    JPanel p2 = new JPanel(new GridLayout(0, 1, 2, 2));
    debugCheckbox = new JCheckBox("Debug");
    p2.add(debugCheckbox);
    generateButton = new JButton("Generate Image");
    generateButton.addActionListener(this);
    p2.add(generateButton);
    p.add(p2, BorderLayout.SOUTH);

    return p;
  }

  private Component createFontList() {
    GraphicsEnvironment g = GraphicsEnvironment.getLocalGraphicsEnvironment();
    String[] fonts = g.getAvailableFontFamilyNames();
    fontDropdown = new JComboBox<>(fonts);
    return fontDropdown;
  }

  private Component createOutputPanel() {
    JPanel p = new JPanel() {
      @Override
      public void paint(Graphics g) {
        g.setColor(Color.gray);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        g.setColor(Color.darkGray);
        for (int i = 0; i < this.getWidth(); i += 20) {
          for (int j = 0; j < this.getHeight(); j += 10) {
            if (j % 20 == 0) {
              g.fillRect(i + 10, j, 10, 10);
            } else {
              g.fillRect(i, j, 10, 10);
            }
          }
        }
        if (image != null) {
          g.drawImage(image, 0, 0, (int) (image.getWidth() * scale),
              (int) (image.getHeight() * scale), null);
        }
      }
    };
    return new JScrollPane(p, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
        JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
  }

  private Component createButtons() {
    JPanel p = new JPanel(new GridLayout(1, 0, 2, 2));
    saveButton = new JButton("Save");
    saveButton.addActionListener(this);
    saveButton.setEnabled(false);
    p.add(saveButton);
    openGenerateFontGuiButton = new JButton("Open GenerateFontGui");
    openGenerateFontGuiButton.addActionListener(this);
    openGenerateFontGuiButton.setEnabled(false);
    p.add(openGenerateFontGuiButton);
    return p;
  }

  public static void main(String[] args) throws IOException {
    GenTrueTypeSpriteSheetGui gui = new GenTrueTypeSpriteSheetGui();
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    if (event.getSource() == generateButton) {
      String fontName = fontDropdown.getSelectedItem().toString();
      String fontSize = fontSizeField.getText();
      String xOffset = xOffsetField.getText();
      String yOffset = yOffsetField.getText();
      boolean debug = debugCheckbox.isSelected();
      scale = Double.parseDouble(scaleField.getText());

      try {
        genTrueTypeSpriteSheet = new GenTrueTypeSpriteSheet(fontName, fontSize, xOffset, yOffset,
            debug);
        image = genTrueTypeSpriteSheet.generate();
      } catch (Exception ex) {
        Log.error(ex);
      }

      revalidate();
      repaint();
      saveButton.setEnabled(true);

    } else if (event.getSource() == saveButton) {

      try {
        this.savedFilename = genTrueTypeSpriteSheet.save();
        openGenerateFontGuiButton.setEnabled(true);
      } catch (IOException ex) {
        Log.error(ex);
      }

    } else if (event.getSource() == openGenerateFontGuiButton) {

      String filename = new File(savedFilename).getAbsolutePath();
      String charMap = new File("./fontgenerator/DefaultCharMap.txt").getAbsolutePath();

      try {
        GenerateFontGui gui = new GenerateFontGui(filename, charMap);
      } catch (IOException ex) {
        Log.error(ex);
      }
    }
  }
}
