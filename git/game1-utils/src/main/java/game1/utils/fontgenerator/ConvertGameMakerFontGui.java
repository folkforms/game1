package game1.utils.fontgenerator;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import folkforms.log.Log;

/**
 * Gui for {@link ConvertGameMakerFont}.
 */
public class ConvertGameMakerFontGui extends JFrame implements ActionListener {

  private JTextField inputImageField, outputImageField;
  private JButton convertButton;

  public ConvertGameMakerFontGui() throws IOException {
    this("E:/dev/assets/fonts/original_grafx_fonts/_edit2/Paris.png",
        "E:/dev/assets/fonts/original_grafx_fonts/_edit2/Paris_converted.png");
  }

  public ConvertGameMakerFontGui(String inputImagePath, String outputImagePath) throws IOException {
    setTitle("Convert GameMaker Font");
    setSize(500, 100);
    setLocation(100, 100);
    setDefaultCloseOperation(EXIT_ON_CLOSE);

    setLayout(new BorderLayout(2, 2));
    getContentPane().add(createInputFields(inputImagePath, outputImagePath), BorderLayout.CENTER);
    getContentPane().add(createButtons(), BorderLayout.EAST);

    setVisible(true);
  }

  private Component createInputFields(String inputImagePath, String outputImagePath) {
    JPanel panel = new JPanel(new GridLayout(0, 1, 5, 5));
    panel.add(inputImageField = new JTextField(inputImagePath));
    panel.add(outputImageField = new JTextField(outputImagePath));
    return panel;
  }

  private Component createButtons() {
    convertButton = new JButton("Convert");
    convertButton.addActionListener(this);
    return convertButton;
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    if (event.getSource() == convertButton) {

      String inputFile = inputImageField.getText();
      String outputFile = outputImageField.getText();

      try {
        new ConvertGameMakerFont(inputFile, outputFile);
      } catch (IOException ex) {
        Log.showPopup(JOptionPane.ERROR_MESSAGE, "Error", ex.getClass() + ": " + ex.getMessage());
      }
    }
  }

  public static void main(String[] args) throws IOException {
    new ConvertGameMakerFontGui();
  }
}