# Tags are a feature of Aseprite. Say you have an animation that is 20 frames, with 4 walking,
# 4 jumping, 7 attacking, 5 dying. You can give each portion of the 20-frame animation a "tag"
# which just marks frames 1-4 as "walking", frames 5-8 as "jumping", etc.
#
# This script will load "filename" and split the aseprite file into separate aseprite files.
# It will then iterate over the new aseprite files and save them as png and json files.
#

if ARGV.length != 1 then
  puts "Splits aseprite animations into separate spritesheets by tag."
  puts ""
  puts "Usage: split-aseprite-by-tag.rb <filename>"
  exit 1
end

filename = ARGV[0]
filename_no_ext = filename.gsub(/\.aseprite/, '')

ok = system "aseprite.exe --batch #{filename} --split-tags --sheet-type horizontal --save-as #{filename_no_ext}_{tag}.aseprite"
exit 1 if !ok

files_with_tags = Dir.glob "#{filename_no_ext}_*.aseprite"
files_with_tags.map! { |file_with_tag|
  file_with_tag_no_ext = file_with_tag.gsub(/\.aseprite/, '')
  system "aseprite.exe --batch #{file_with_tag} --format json-array --sheet-type horizontal --sheet #{file_with_tag_no_ext}.png --data #{file_with_tag_no_ext}.json"
  system "del #{file_with_tag}"
}
