@echo off
cls

echo ======== Building game1-spritesheets ========
echo.

cd game1-spritesheets
call mvn clean install
if %ERRORLEVEL% neq 0 exit /b %ERRORLEVEL%
cd ..

echo.
echo ======== Building game1 ========
echo.

call mvn clean install
if %ERRORLEVEL% neq 0 exit /b %ERRORLEVEL%

echo.
echo ======== Building game1-utils ========
echo.

cd game1-utils
call mvn clean package
if %ERRORLEVEL% neq 0 exit /b %ERRORLEVEL%
cd ..

echo.
echo ======== Building test-game ========
echo.

cd test-game
call mvn clean package
if %ERRORLEVEL% neq 0 exit /b %ERRORLEVEL%
cd ..
