package game1.sound.mixer;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * The mixer controls the volume of the channels.
 */
public class Mixer {

  private static Map<String, MixerChannel> channels = new HashMap<>();
  static {
    channels.put("master", new MixerChannel("master"));
  }

  public Mixer() {
  }

  public void init(Set<String> channels) {
    // FIXME SOUND: When the game starts we will need to set the channel volumes to the values
    // contained in the properties file i.e. whatever the game was set to when it was last played.

    // channels.forEach(channel -> {
    // . channel.setVolume(...);
    // . channel.setMuted(...);
    // }
  }

  public void registerChannel(String channel) {
    MixerChannel previous = channels.put(channel, new MixerChannel(channel));
    if (previous != null) {
      throw new RuntimeException(String.format(
          "Attempted to register mixer channel '%s' but it was already registered.", channel));
    }
  }

  // FIXME SOUND: I have no idea if the below methods are going to be used or not

  public float getVolume(String channel) {
    return channels.get(channel).getVolume();
  }

  public void setVolume(String channel, float newVolume) {
    if (newVolume < 0 || newVolume > 1) {
      throw new RuntimeException(
          String.format("Invalid volume '%s'. Volume must be between 0 and 1.", newVolume));
    }
    channels.get(channel).setVolume(newVolume);
    if (channel.equals("master")) {
      for (MixerChannel c : channels.values()) {
        c.notifyListeners();
      }
    }
  }

  public boolean getMuted(String channel) {
    return channels.get(channel).getMuted();
  }

  public void setMuted(String channel, boolean newMuted) {
    channels.get(channel).setMuted(newMuted);
    if (channel.equals("master")) {
      for (MixerChannel c : channels.values()) {
        c.notifyListeners();
      }
    }
  }

  public MixerChannel getChannel(String channel) {
    return channels.get(channel);
  }

  public void internal_registerChannelListener(String channel, MixerChannelListener listener) {
    MixerChannel c = channels.get(channel);
    c.internal_registerListener(listener);
  }

  public void internal_unregisterChannelListener(String channel, MixerChannelListener listener) {
    MixerChannel c = channels.get(channel);
    c.internal_unregisterListener(listener);
  }

  public Map<String, Float> getData() {
    // FIXME SOUND: Get current channel volume and muted data for saving to the properties file
    return null;
  }
}
