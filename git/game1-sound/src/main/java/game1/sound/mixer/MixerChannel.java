package game1.sound.mixer;

import java.util.HashSet;
import java.util.Set;

public class MixerChannel {

  private float volume = 1;
  private boolean isMuted = false;
  private Set<MixerChannelListener> channelListeners = new HashSet<>();
  private String volumePropertyName;
  private String mutedPropertyName;

  public MixerChannel(String channel) {
    this.volumePropertyName = String.format("%s.volume", channel);
    this.mutedPropertyName = String.format("%s.muted", channel);
  }

  public float getVolume() {
    return volume;
  }

  public void setVolume(float newVolume) {
    this.volume = newVolume;
    notifyListeners();
  }

  public boolean getMuted() {
    return isMuted;
  }

  public void setMuted(boolean b) {
    isMuted = b;
    notifyListeners();
  }

  // FIXME SOUND: Could this be private?
  public void notifyListeners() {
    for (MixerChannelListener listener : channelListeners) {
      listener.notifyVolumeChanged();
    }
  }

  public String getVolumePropertyName() {
    return volumePropertyName;
  }

  public String getMutedPropertyName() {
    return mutedPropertyName;
  }

  public void internal_registerListener(MixerChannelListener listener) {
    channelListeners.add(listener);
  }

  public void internal_unregisterListener(MixerChannelListener listener) {
    channelListeners.remove(listener);
  }
}
