package game1.sound.sound2d;

public interface SoundBoard2d {

  void loop(String name);

  void play(String name);

  void pause(String name);

  void stop(String name);

  void fadeOut(String name);

  void pauseAll();

  void stopAll();

  void fadeOutAll();
}
