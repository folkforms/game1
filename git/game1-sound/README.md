# game1-sound

## 2D sound Goal

1. Games should be able to swap out sound apps
2. It should be able to convert files into generic sound data (e.g. wav, ogg)
3. Sound channels are available for separate volume control and muting
4. Games should be able to call `Engine.SOUND.play(name)` and it just works

### Interface

SoundBoard {
  void play(String name, String group);
  void loop(String name, String group);

  void mute(String group);
  void pause(String group);
  void stop(String group);

  void muteAll(String name);
  void pauseAll(String name)
  void stopAll(String name);
}

### Notes

- `Mixer` is probably good as-is. It just provides volume levels/mute status for a channel.
- Let's just start with wav files, and move on to ogg files later.
