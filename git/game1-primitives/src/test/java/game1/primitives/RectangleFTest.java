package game1.primitives;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class RectangleFTest {

  @Test
  public void itCorrectlyComparesEquality() {
    RectangleF r = new RectangleF(1, 2, 5, 7);
    RectangleF r2 = new RectangleF(1, 2, 5, 7);
    RectangleF r3 = new RectangleF(3, 4, 5, 6);
    Object o = new Object();

    assertTrue(r.equals(r2));
    assertFalse(r.equals(r3));
    assertFalse(r.equals(o));
  }

  @Test
  public void itCorrectlyTranslates() {
    RectangleF r = new RectangleF(10, 20, 30, 40);
    r.translate(1, 2, 0);

    assertEquals(r.x, 11);
    assertEquals(r.y, 22);
    assertEquals(r.w, r.w);
    assertEquals(r.h, r.h);
  }

  @Test
  public void itCorrectlyTranslatesCopies() {
    RectangleF r = new RectangleF(10, 20, 30, 40);
    RectangleF r2 = r.translateCopy(5, 6, 0);

    assertEquals(r2.x, r.x + 5);
    assertEquals(r2.y, r.y + 6);
    assertEquals(r2.w, r.w);
    assertEquals(r2.h, r.h);
  }
}
