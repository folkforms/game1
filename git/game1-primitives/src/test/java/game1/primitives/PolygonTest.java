package game1.primitives;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.joml.Vector2f;
import org.junit.jupiter.api.Test;

class PolygonTest {

  @Test
  public void itCorrectlyComparesEquality() {
    Polygon p = new Polygon(
        List.of(new Vector2f(1, 2), new Vector2f(3, 4), new Vector2f(5, 6), new Vector2f(7, 8)));
    Polygon p2 = new Polygon(
        List.of(new Vector2f(1, 2), new Vector2f(3, 4), new Vector2f(5, 6), new Vector2f(7, 8)));
    Polygon p3 = new Polygon(
        List.of(new Vector2f(5, 6), new Vector2f(7, 8), new Vector2f(1, 2), new Vector2f(3, 4)));
    Polygon p4 = new Polygon(
        List.of(new Vector2f(0, 2), new Vector2f(3, 4), new Vector2f(5, 6), new Vector2f(7, 8)));
    Object o = new Object();

    assertTrue(p.equals(p2));
    assertTrue(p.equals(p3));
    assertFalse(p.equals(p4));
    assertFalse(p.equals(o));
  }

  @Test
  public void itCorrectlySetsTheFirstPoint() {
    Polygon p1 = new Polygon(
        List.of(new Vector2f(1, 2), new Vector2f(3, 4), new Vector2f(5, 6), new Vector2f(7, 8)));
    Polygon p2 = new Polygon(
        List.of(new Vector2f(5, 6), new Vector2f(7, 8), new Vector2f(1, 2), new Vector2f(3, 4)));

    assertEquals(new Vector2f(1, 2), p1.getFirstPoint());
    assertEquals(new Vector2f(1, 2), p2.getFirstPoint());
  }

  @Test
  public void itCorrectlyTranslates() {
    Polygon p = new Polygon(List.of(new Vector2f(0, 0), new Vector2f(0, 10), new Vector2f(10, 10),
        new Vector2f(10, 0)));
    assertFalse(p.contains(10, 10)); // Bounding box is inclusive, exclusive
    assertTrue(p.contains(9.999999f, 9.999999f));

    p.translate(1, 2, 0);

    assertTrue(p.contains(10, 11));
    assertFalse(p.contains(0, 1));
  }

  @Test
  public void itCorrectlyTranslatesCopies() {
    Polygon p = new Polygon(List.of(new Vector2f(0, 0), new Vector2f(0, 10), new Vector2f(10, 10),
        new Vector2f(10, 0)));
    assertFalse(p.contains(10, 10)); // Bounding box is inclusive, exclusive
    assertTrue(p.contains(9.999999f, 9.999999f));

    Polygon p2 = p.translateCopy(1, 2, 0);

    assertTrue(p2.contains(10, 11));
    assertFalse(p2.contains(0, 1));
  }
}
