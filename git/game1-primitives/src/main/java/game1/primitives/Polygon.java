package game1.primitives;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.joml.Vector2f;

import folkforms.log.Log;

public class Polygon implements Shape<Polygon> {

  private int[] xPoints;
  private int[] yPoints;
  private int numPoints;
  private Rectangle bounds;

  public Polygon(List<Vector2f> points) {
    points = rotatePoints(points);

    xPoints = new int[points.size()];
    yPoints = new int[points.size()];
    for (int i = 0; i < points.size(); i++) {
      xPoints[i] = (int) points.get(i).x;
      yPoints[i] = (int) points.get(i).y;
    }
    this.numPoints = points.size();
    bounds = createBoundingBox();
  }

  /**
   * Rotate the list of points leftwards until the lowest x,y value is in position 0.
   */
  private List<Vector2f> rotatePoints(List<Vector2f> points) {
    Vector2f lowestX = points.stream().min((p1, p2) -> Float.compare(p1.x, p2.x)).orElseThrow();
    List<Vector2f> pointsWithLowestX = points.stream().filter(p -> p.x == lowestX.x).toList();
    Vector2f pointWithLowestXAndY = pointsWithLowestX.stream()
        .min((p1, p2) -> Float.compare(p1.y, p2.y)).orElseThrow();
    int index;
    for (index = 0; index < points.size(); index++) {
      Vector2f point = points.get(index);
      if (point.equals(pointWithLowestXAndY)) {
        break;
      }
    }
    return rotatePoints(points, index);
  }

  private List<Vector2f> rotatePoints(List<Vector2f> points, int numRotations) {
    List<Vector2f> mutablePoints = new ArrayList<>();
    mutablePoints.addAll(points);
    for (int i = 0; i < numRotations; i++) {
      mutablePoints.add(mutablePoints.remove(0));
    }
    return mutablePoints;
  }

  public Vector2f getFirstPoint() {
    return new Vector2f(xPoints[0], yPoints[0]);
  }

  @Override
  public List<Vector2f> listPoints() {
    List<Vector2f> newPoints = new ArrayList<>();
    for (int i = 0; i < numPoints; i++) {
      newPoints.add(new Vector2f(xPoints[i], yPoints[i]));
    }
    return newPoints;
  }

  @Override
  public boolean contains(float x, float y) {
    if (numPoints <= 2 || !bounds.contains(x, y)) {
      return false;
    }
    int hits = 0;

    int lastx = xPoints[numPoints - 1];
    int lasty = yPoints[numPoints - 1];
    int curx, cury;

    // Walk the edges of the polygon
    for (int i = 0; i < numPoints; lastx = curx, lasty = cury, i++) {
      curx = xPoints[i];
      cury = yPoints[i];

      if (cury == lasty) {
        continue;
      }

      int leftx;
      if (curx < lastx) {
        if (x >= lastx) {
          continue;
        }
        leftx = curx;
      } else {
        if (x >= curx) {
          continue;
        }
        leftx = lastx;
      }

      double test1, test2;
      if (cury < lasty) {
        if (y < cury || y >= lasty) {
          continue;
        }
        if (x < leftx) {
          hits++;
          continue;
        }
        test1 = x - curx;
        test2 = y - cury;
      } else {
        if (y < lasty || y >= cury) {
          continue;
        }
        if (x < leftx) {
          hits++;
          continue;
        }
        test1 = x - lastx;
        test2 = y - lasty;
      }

      if (test1 < (test2 / (lasty - cury) * (lastx - curx))) {
        hits++;
      }
    }
    return ((hits & 1) != 0);
  }

  @Override
  public void scale(float scale) {
    // throw new RuntimeException(
    // "Scaling a polygon is not implemented yet. Because it's really difficult to do.");

    int leftMostPointIndex = -1;
    int leftMostPoint = Integer.MAX_VALUE;
    for (int i = 0; i < xPoints.length; i++) {
      if (xPoints[i] < leftMostPoint) {
        leftMostPoint = xPoints[i];
        leftMostPointIndex = i;
      }
    }

    float tx = -1 * xPoints[leftMostPointIndex];
    float ty = -1 * yPoints[leftMostPointIndex];

    for (int i = 0; i < xPoints.length; i++) {
      xPoints[i] += tx;
      yPoints[i] += ty;
    }

    for (int i = 0; i < xPoints.length; i++) {
      xPoints[i] *= scale;
      yPoints[i] *= scale;
    }

    float ty2 = ty * scale;
    for (int i = 0; i < xPoints.length; i++) {
      xPoints[i] -= tx;
      yPoints[i] -= ty2;
    }

    bounds = createBoundingBox();
  }

  private Rectangle createBoundingBox() {
    if (numPoints == 0) {
      return new Rectangle(0, 0, 0, 0);
    }
    calculateBounds(xPoints, yPoints, numPoints);
    return bounds;
  }

  void calculateBounds(int[] xpoints, int[] ypoints, int npoints) {
    int boundsMinX = Integer.MAX_VALUE;
    int boundsMinY = Integer.MAX_VALUE;
    int boundsMaxX = Integer.MIN_VALUE;
    int boundsMaxY = Integer.MIN_VALUE;

    for (int i = 0; i < npoints; i++) {
      int x = xpoints[i];
      boundsMinX = Math.min(boundsMinX, x);
      boundsMaxX = Math.max(boundsMaxX, x);
      int y = ypoints[i];
      boundsMinY = Math.min(boundsMinY, y);
      boundsMaxY = Math.max(boundsMaxY, y);
    }
    bounds = new Rectangle(boundsMinX, boundsMinY, boundsMaxX - boundsMinX,
        boundsMaxY - boundsMinY);
  }

  @Override
  public void translate(float dx, float dy, float dz) {
    for (int i = 0; i < numPoints; i++) {
      xPoints[i] += dx;
      yPoints[i] += dy;
    }
    bounds = createBoundingBox();
  }

  @Override
  public Polygon translateCopy(float dx, float dy, float dz) {
    List<Vector2f> newPoints = new ArrayList<>();
    for (int i = 0; i < numPoints; i++) {
      newPoints.add(new Vector2f(xPoints[i] + dx, yPoints[i] + dy));
    }
    return new Polygon(newPoints);
  }

  public void debug_printData() {
    Log.info("Polygon: ");
    for (int i = 0; i < xPoints.length; i++) {
      Log.info("  %s,%s", xPoints[i], yPoints[i]);
    }
  }

  @Override
  public String toString() {
    List<String> p = new ArrayList<>();
    for (int i = 0; i < xPoints.length; i++) {
      p.add(String.format("%s,%s", xPoints[i], yPoints[i]));
    }
    return String.format("%s[%s]", getClass().getSimpleName(), p);
  }

  @Override
  public int hashCode() {
    return Objects.hash(xPoints, yPoints, numPoints, bounds);
  }

  @Override
  public boolean equals(Object other) {
    if (other instanceof Polygon) {
      Polygon p2 = (Polygon) other;
      return Arrays.compare(xPoints, p2.xPoints) == 0 && Arrays.compare(yPoints, p2.yPoints) == 0
          && numPoints == p2.numPoints && bounds.equals(p2.bounds);
    }
    return false;
  }
}
