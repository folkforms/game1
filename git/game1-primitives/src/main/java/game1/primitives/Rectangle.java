package game1.primitives;

import java.util.List;

import org.joml.Vector2f;

public class Rectangle implements Shape<Rectangle> {

  public int x, y, w, h;

  public Rectangle(int x, int y, int w, int h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }

  @Override
  public String toString() {
    return String.format("Rectangle[%s,%s,%s,%s]", x, y, w, h);
  }

  /**
   * Determines whether or not this {@code Rectangle} and the specified {@code Rectangle} intersect.
   * Two rectangles intersect if their intersection is nonempty.
   *
   * @param r
   *          the specified {@code Rectangle}
   * @return {@code true} if the specified {@code Rectangle} and this {@code Rectangle} intersect;
   *         {@code false} otherwise.
   */
  public boolean intersects(Rectangle r) {
    int tw = this.w;
    int th = this.h;
    int rw = r.w;
    int rh = r.h;
    if (rw <= 0 || rh <= 0 || tw <= 0 || th <= 0) {
      return false;
    }
    int tx = this.x;
    int ty = this.y;
    int rx = r.x;
    int ry = r.y;
    rw += rx;
    rh += ry;
    tw += tx;
    th += ty;
    // overflow || intersect
    return ((rw < rx || rw > tx) && (rh < ry || rh > ty) && (tw < tx || tw > rx)
        && (th < ty || th > ry));
  }

  /**
   * Checks whether or not this {@code Rectangle} contains the point {@code (x2,y2)}.
   *
   * @param x2
   *          the specified x coordinate
   * @param y2
   *          the specified y coordinate
   * @return {@code true} if the point {@code (x2,y2)} is inside this {@code Rectangle};
   *         {@code false} otherwise.
   */
  @Override
  public boolean contains(float x2, float y2) {
    int w = this.w;
    int h = this.h;
    if ((w | h) < 0) {
      return false;
    }
    int x = this.x;
    int y = this.y;
    if (x2 < x || y2 < y) {
      return false;
    }
    w += x;
    h += y;
    // overflow || intersect
    return ((w < x || w > x2) && (h < y || h > y2));
  }

  @Override
  public void scale(float scale) {
    w = (int) (w * scale);
    h = (int) (h * scale);
  }

  @Override
  public void translate(float dx, float dy, float dz) {
    x += dx;
    y += dx;
  }

  @Override
  public Rectangle translateCopy(float dx, float dy, float dz) {
    return new Rectangle(x + (int) dx, y + (int) dy, w, h);
  }

  @Override
  public List<Vector2f> listPoints() {
    return List.of(new Vector2f(x, y), new Vector2f(x, y + h), new Vector2f(x + w, y + h),
        new Vector2f(x + w, y));
  }

  /**
   * Parses a {@link Rectangle} from an input string of the form "x,y,w,h".
   *
   * @param substring
   * @return
   */
  public static Rectangle parseString(String string) {
    String[] tokens = string.split(",");
    int x = Integer.parseInt(tokens[0]);
    int y = Integer.parseInt(tokens[1]);
    int w = Integer.parseInt(tokens[2]);
    int h = Integer.parseInt(tokens[3]);
    return new Rectangle(x, y, w, h);
  }

  @Override
  public boolean equals(Object other) {
    if (other == null || other instanceof Rectangle == false) {
      return false;
    }
    Rectangle otherRectangle = (Rectangle) other;
    return x == otherRectangle.x && y == otherRectangle.y && w == otherRectangle.w
        && h == otherRectangle.h;
  }

  @Override
  public int hashCode() {
    return (x + y + w + h) * 31;
  }
}
