package game1.primitives;

import java.util.List;
import java.util.Objects;

import org.joml.Vector2f;

public class RectangleF implements Shape<RectangleF> {

  public float x, y, w, h;

  public RectangleF(float x, float y, float w, float h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }

  @Override
  public String toString() {
    return String.format("%s[%s,%s,%s,%s]", getClass().getSimpleName(), x, y, w, h);
  }

  /**
   * Determines whether or not this {@code RectangleF} and the specified {@code RectangleF}
   * intersect. Two rectangles intersect if their intersection is nonempty.
   *
   * @param r
   *          the specified {@code Rectangle}
   * @return {@code true} if the specified {@code Rectangle} and this {@code Rectangle} intersect;
   *         {@code false} otherwise.
   */
  public boolean intersects(RectangleF r) {
    float tw = this.w;
    float th = this.h;
    float rw = r.w;
    float rh = r.h;
    if (rw <= 0 || rh <= 0 || tw <= 0 || th <= 0) {
      return false;
    }
    float tx = this.x;
    float ty = this.y;
    float rx = r.x;
    float ry = r.y;
    rw += rx;
    rh += ry;
    tw += tx;
    th += ty;
    // overflow || intersect
    return ((rw < rx || rw > tx) && (rh < ry || rh > ty) && (tw < tx || tw > rx)
        && (th < ty || th > ry));
  }

  /**
   * Checks whether or not this {@code Rectangle} contains the point {@code (x2,y2)}.
   *
   * @param x2
   *          the specified x coordinate
   * @param y2
   *          the specified y coordinate
   * @return {@code true} if the point {@code (x2,y2)} is inside this {@code Rectangle};
   *         {@code false} otherwise.
   */
  @Override
  public boolean contains(float x2, float y2) {
    float w = this.w;
    float h = this.h;
    if (w < 0 || h < 0) {
      return false;
    }
    float x = this.x;
    float y = this.y;
    if (x2 < x || y2 < y) {
      return false;
    }
    w += x;
    h += y;
    // overflow || intersect
    return ((w < x || w > x2) && (h < y || h > y2));
  }

  @Override
  public void scale(float scale) {
    w *= scale;
    h *= scale;
  }

  @Override
  public void translate(float dx, float dy, float dz) {
    x += dx;
    y += dy;
  }

  @Override
  public RectangleF translateCopy(float dx, float dy, float dz) {
    return new RectangleF(x + dx, y + dy, w, h);
  }

  @Override
  public List<Vector2f> listPoints() {
    return List.of(new Vector2f(x, y), new Vector2f(x, y + h), new Vector2f(x + w, y + h),
        new Vector2f(x + w, y));
  }

  /**
   * Parses a {@link RectangleF} from an input string of the form "x,y,w,h".
   *
   * @param substring
   * @return
   */
  public static RectangleF parseString(String string) {
    String[] tokens = string.split(",");
    float x = Float.parseFloat(tokens[0]);
    float y = Float.parseFloat(tokens[1]);
    float w = Float.parseFloat(tokens[2]);
    float h = Float.parseFloat(tokens[3]);
    return new RectangleF(x, y, w, h);
  }

  @Override
  public int hashCode() {
    return Objects.hash(x, y, w, h);
  }

  @Override
  public boolean equals(Object other) {
    if (other instanceof RectangleF) {
      RectangleF r2 = (RectangleF) other;
      return x == r2.x && y == r2.y && w == r2.w && h == r2.h;
    }
    return false;
  }
}
