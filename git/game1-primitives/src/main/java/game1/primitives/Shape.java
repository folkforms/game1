package game1.primitives;

import java.util.List;

import org.joml.Vector2f;

public interface Shape<T> {

  public boolean contains(float x, float y);

  default boolean contains(Vector2f point) {
    return contains(point.x, point.y);
  }

  public void scale(float scale);

  public void translate(float dx, float dy, float dz);

  public T translateCopy(float dx, float dy, float dz);

  public List<Vector2f> listPoints();
}
