package game1.resolutions.layers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joml.Vector2i;

import folkforms.log.Log;
import game1.actors.Actor;
import game1.actors.ParentChildRegistry;
import game1.actors.ParentChildRegistryFactory;
import game1.debug.dump_data.DebugDataProvider;
import game1.debug.dump_data.DebugDataProviderRegistry;
import game1.resolutions.scaling.ResolutionScalingStrategyV0;
import game1.resolutions.scaling.ResolutionScalingStrategyV1;
import game1.resolutions.scaling.ScalingStrategies;
import game1.resolutions.scaling.ScalingStrategy;
import game1.resolutions.viewports.StaticViewport;
import game1.resolutions.viewports.Viewport;

/**
 * Keeps track of resolution layers, and also which actors are in which resolution layers.
 */
public class ResolutionLayers implements DebugDataProvider {

  public static final String WINDOW_LAYER = "window-layer";
  public static final String LAYER_3D = "3d-layer";

  private Vector2i windowSize;
  private ResolutionLayer windowLayer;
  private ResolutionLayer threeDeeLayer;
  private Map<String, ResolutionLayer> resolutionLayers = new HashMap<>();
  private Map<Actor, String> actorToResolutionLayerMap = new HashMap<>();

  public ResolutionLayers() {
    DebugDataProviderRegistry.register(this, DebugDataProviderRegistry.Context.FILE);
  }

  // FIXME RESOLUTIONS: Make this internal somehow
  public void init(int windowWidth, int windowHeight) {
    windowSize = new Vector2i(windowWidth, windowHeight);
    windowLayer = new ResolutionLayer(WINDOW_LAYER, windowSize, windowSize, windowSize,
        new ResolutionScalingStrategyV1(), new StaticViewport());
    resolutionLayers.put(WINDOW_LAYER, windowLayer);

    threeDeeLayer = new ResolutionLayer(LAYER_3D, windowSize, new Vector2i(99, 99),
        new Vector2i(99, 99), new ResolutionScalingStrategyV0(), new StaticViewport());
    resolutionLayers.put(LAYER_3D, threeDeeLayer);
  }

  public ResolutionLayer createLayer(String name, Vector2i resolution, Vector2i levelSize,
      ScalingStrategy scalingStrategy, Viewport viewport) {
    ResolutionLayer resolutionLayer = new ResolutionLayer(name, windowSize, resolution, levelSize,
        scalingStrategy, viewport);
    resolutionLayers.put(name, resolutionLayer);
    return resolutionLayer;
  }

  public ResolutionLayer createStaticLayer(String name, Vector2i resolution,
      ScalingStrategy scalingStrategy) {
    ResolutionLayer resolutionLayer = new ResolutionLayer(name, windowSize, resolution, resolution,
        scalingStrategy, new StaticViewport());
    resolutionLayers.put(name, resolutionLayer);
    return resolutionLayer;
  }

  /**
   * 3D layers are effectively a no-op. All items need a layer, but only 2D items are adjusted. For
   * now, it's just easier this way as it allows us to validate that everything is in a layer.
   */
  public ResolutionLayer create3DLayer(String name) {
    ResolutionLayer resolutionLayer = new ResolutionLayer(name, windowSize, new Vector2i(99, 99),
        new Vector2i(99, 99), ScalingStrategies.get(1), new StaticViewport());
    resolutionLayers.put(name, resolutionLayer);
    return resolutionLayer;
  }

  public void addActor(Actor actor, String layerName) {
    // FIXME Validate layer exists
    if (!resolutionLayers.containsKey(layerName)) {
      throw new RuntimeException(String.format("No such layer '%s'", layerName));
    }

    ParentChildRegistry instance = ParentChildRegistryFactory.getInstance();
    actorToResolutionLayerMap.put(actor, layerName);
    List<Actor> children = instance.getAllChildren(actor);
    children.forEach(child -> actorToResolutionLayerMap.put(child, layerName));
  }

  public ResolutionLayer getLayerForActor(Actor actor) {
    String layerName = actorToResolutionLayerMap.get(actor);
    if (layerName == null) {
      throw new RuntimeException(String.format("Could not find layer for actor %s", actor));
    }
    return resolutionLayers.get(layerName);
  }

  public ResolutionLayer getLayerByName(String name) {
    // FIXME RESOLUTIONS: Validate layer exists
    return resolutionLayers.get(name);
  }

  public Map<String, ResolutionLayer> debug_getResolutionLayers() {
    return resolutionLayers;
  }

  public void clear() {
    resolutionLayers.clear();
    actorToResolutionLayerMap.clear();
    resolutionLayers.put(WINDOW_LAYER, windowLayer);
    resolutionLayers.put(LAYER_3D, windowLayer);
  }

  public void debug_printData() {
    Log.info("#### ResolutionLayer");
    Log.info("# resolutionLayers = %s", resolutionLayers);
    Log.info("# actorToResolutionLayerMap = %s", actorToResolutionLayerMap);
  }

  @Override
  public String debug_getDumpFilename() {
    return "dump_resolution_layer_map.txt";
  }

  @Override
  public List<String> debug_listData() {
    List<String> list1 = resolutionLayers.values().stream().map(r -> r.toString()).toList();
    List<String> list2 = actorToResolutionLayerMap.entrySet().stream()
        .map(entry -> String.format("%s => %s", entry.getKey(), entry.getValue())).toList();
    List<String> output = new ArrayList<>();
    output.addAll(List.of("ResolutionLayers:", ""));
    output.addAll(list1);
    output.addAll(List.of("Actor to ResolutionLayer map:", ""));
    output.addAll(list2);
    return output;
  }
}
