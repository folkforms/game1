package game1.resolutions.layers;

public class ResolutionLayersFactory {

  private static ResolutionLayers resolutionLayers;

  public static ResolutionLayers getInstance() {
    if (resolutionLayers == null) {
      resolutionLayers = new ResolutionLayers();
    }
    return resolutionLayers;
  }
}
