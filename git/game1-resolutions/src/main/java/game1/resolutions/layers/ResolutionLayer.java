package game1.resolutions.layers;

import org.joml.Vector2f;
import org.joml.Vector2i;

import game1.resolutions.scaling.ScalingStrategy;
import game1.resolutions.shared.ResolutionHints;
import game1.resolutions.shared.ResolutionHintsImpl;
import game1.resolutions.viewports.Viewport;

public class ResolutionLayer {

  private String name;
  private ResolutionHints resolutionHints;
  private Viewport viewport;

  public ResolutionLayer(String name, Vector2i windowSize, Vector2i resolution, Vector2i levelSize,
      ScalingStrategy scalingStrategy, Viewport viewport) {
    this.name = name;
    this.viewport = viewport;
    resolutionHints = new ResolutionHintsImpl(scalingStrategy);
    resolutionHints.setWindowSize(windowSize.x, windowSize.y);
    resolutionHints.setResolution(resolution.x, resolution.y);
    resolutionHints.setLevelSize(levelSize.x, levelSize.y);
  }

  public String getName() {
    return name;
  }

  public Vector2f getResolution() {
    return new Vector2f(resolutionHints.getResolutionWidth(),
        resolutionHints.getResolutionHeight());
  }

  public Vector2f getLevelSize() {
    return new Vector2f(resolutionHints.getLevelWidth(), resolutionHints.getLevelHeight());
  }

  public Viewport getViewport() {
    return viewport;
  }

  public ResolutionHints getResolutionHints() {
    return resolutionHints;
  }

  public void setViewport(Viewport viewport) {
    this.viewport = viewport;
  }

  public void setResolution(Vector2i newResolution) {
    resolutionHints.setResolution(newResolution.x, newResolution.y);
  }

  public void setLevelSize(Vector2i newLevelSize) {
    resolutionHints.setLevelSize(newLevelSize.x, newLevelSize.y);
  }

  @Override
  public String toString() {
    return String.format("%s[\n    name=%s,\n    resolutionHints=%s,\n    viewport=%s\n]",
        getClass().getSimpleName(), name, resolutionHints, viewport);
  }
}
