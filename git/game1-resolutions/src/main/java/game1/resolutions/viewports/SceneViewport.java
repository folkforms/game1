package game1.resolutions.viewports;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector4f;

import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;

/**
 * SceneViewports are scaled to the scene.
 */
public abstract class SceneViewport implements Viewport {

  protected String resolutionLayerName;
  protected Vector2f viewportCoords;
  protected Vector4f padding;
  protected Vector2i levelSize;
  protected float vxMax;
  protected float vyMax;

  public SceneViewport(String resolutionLayerName, Vector2f viewportCoords, Vector4f padding) {
    this.resolutionLayerName = resolutionLayerName;
    this.viewportCoords = viewportCoords;
    this.padding = padding;
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(resolutionLayerName).getResolutionHints();
    this.vxMax = hints.getLevelWidth() - hints.getViewportWidth();
    this.vyMax = hints.getLevelHeight() - hints.getViewportHeight();
    if (vxMax < 0) {
      vxMax = 0;
    }
    if (vyMax < 0) {
      vyMax = 0;
    }
    clamp();
  }

  private void clamp() {
    if (viewportCoords.x < 0) {
      viewportCoords.x = 0;
    }
    if (viewportCoords.y < 0) {
      viewportCoords.y = 0;
    }
    if (viewportCoords.x > vxMax) {
      viewportCoords.x = vxMax;
    }
    if (viewportCoords.y > vyMax) {
      viewportCoords.y = vyMax;
    }
  }

  @Override
  public float getX() {
    return viewportCoords.x;
  }

  @Override
  public float getY() {
    return viewportCoords.y;
  }

  @Override
  public void updateViewport() {
    if (checkXNegativeCondition()) {
      moveVx(-1);
    }
    if (checkXPositiveCondition()) {
      moveVx(1);
    }
    if (checkYNegativeCondition()) {
      moveVy(-1);
    }
    if (checkYPositiveCondition()) {
      moveVy(1);
    }
    clamp();
  }

  protected abstract boolean checkXNegativeCondition();

  protected abstract boolean checkXPositiveCondition();

  protected abstract boolean checkYNegativeCondition();

  protected abstract boolean checkYPositiveCondition();

  protected abstract void moveVx(float dx);

  protected abstract void moveVy(float dy);
}
