package game1.resolutions.viewports;

public interface Viewport {

  public float getX();

  public float getY();

  public void updateViewport();
}
