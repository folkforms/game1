package game1.resolutions.viewports;

public class StaticViewport implements Viewport {

  @Override
  public float getX() {
    return 0;
  }

  @Override
  public float getY() {
    return 0;
  }

  @Override
  public void updateViewport() {
  }

  @Override
  public String toString() {
    return getClass().getSimpleName();
  }
}
