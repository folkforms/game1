package game1.resolutions.viewports;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;

import game1.actors.Mobile;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;

/**
 * A {@link Mobile} scaled to the scene size controls the scene viewport by pushing against its
 * edges.
 */
public abstract class MobileControlledSceneViewport extends SceneViewport {

  protected Mobile mobile;

  public MobileControlledSceneViewport(Mobile mobile, String resolutionLayerName,
      Vector2f viewportCoords, Vector4f padding) {
    super(resolutionLayerName, viewportCoords, padding);
    this.mobile = mobile;
  }

  public abstract Vector3f getMobileFocusPoint();

  @Override
  protected boolean checkXNegativeCondition() {
    Vector3f mobileScenePos = getMobileFocusPoint();
    float leftPaddingXBoundary = viewportCoords.x + padding.w;
    if (mobileScenePos.x < leftPaddingXBoundary) {
      return true;
    }
    return false;
  }

  @Override
  protected boolean checkXPositiveCondition() {
    Vector3f mobileScenePos = getMobileFocusPoint();
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(resolutionLayerName).getResolutionHints();
    float rightPaddingXBoundary = viewportCoords.x + hints.getViewportWidth() - padding.y;
    if (mobileScenePos.x > rightPaddingXBoundary) {
      return true;
    }
    return false;
  }

  @Override
  protected boolean checkYNegativeCondition() {
    Vector3f mobileScenePos = getMobileFocusPoint();
    float bottomPaddingYBoundary = viewportCoords.y + padding.z;
    if (mobileScenePos.y < bottomPaddingYBoundary) {
      return true;
    }
    return false;
  }

  @Override
  protected boolean checkYPositiveCondition() {
    Vector3f mobileScenePos = getMobileFocusPoint();
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(resolutionLayerName).getResolutionHints();
    float topPaddingYBoundary = viewportCoords.y + hints.getViewportHeight() - padding.x;
    if (mobileScenePos.y > topPaddingYBoundary) {
      return true;
    }
    return false;
  }
}
