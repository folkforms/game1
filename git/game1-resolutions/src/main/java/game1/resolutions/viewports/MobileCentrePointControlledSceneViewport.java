package game1.resolutions.viewports;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;

import game1.actors.Mobile;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;

public class MobileCentrePointControlledSceneViewport extends MobileControlledSceneViewport {

  public MobileCentrePointControlledSceneViewport(Mobile mobile, String resolutionLayerName,
      Vector2f viewportCoords, Vector4f padding) {
    super(mobile, resolutionLayerName, viewportCoords, padding);
  }

  @Override
  public Vector3f getMobileFocusPoint() {
    Vector3f pos = mobile.getPosition();
    float x = pos.x;
    float y = pos.y;
    float z = pos.z;
    float w = mobile.getWidth();
    float h = mobile.getHeight();
    return new Vector3f(x + w / 2, y + h / 2, z);
  }

  public void centerOnMobile() {
    Vector3f mobileFocusPoint = getMobileFocusPoint();
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(resolutionLayerName).getResolutionHints();
    viewportCoords.x = mobileFocusPoint.x - hints.getViewportWidth() / 2;
    viewportCoords.y = mobileFocusPoint.y - hints.getViewportHeight() / 2;
  }

  @Override
  protected void moveVx(float dx) {
    Vector3f mobileFocusPoint = getMobileFocusPoint();
    if (dx < 0) {
      float newVx = mobileFocusPoint.x - padding.w;
      viewportCoords.x = newVx;
    }
    if (dx > 0) {
      ResolutionHints hints = ResolutionLayersFactory.getInstance()
          .getLayerByName(resolutionLayerName).getResolutionHints();
      float newVx = mobileFocusPoint.x - hints.getViewportWidth() + padding.y;
      viewportCoords.x = newVx;
    }
  }

  @Override
  protected void moveVy(float dy) {
    Vector3f mobileFocusPoint = getMobileFocusPoint();
    if (dy < 0) {
      float newVy = mobileFocusPoint.y - padding.z;
      viewportCoords.y = newVy;
    }
    if (dy > 0) {
      ResolutionHints hints = ResolutionLayersFactory.getInstance()
          .getLayerByName(resolutionLayerName).getResolutionHints();
      float newVy = mobileFocusPoint.y - hints.getViewportHeight() + padding.x;
      viewportCoords.y = newVy;
    }
  }

  @Override
  public String toString() {
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(resolutionLayerName).getResolutionHints();
    return String.format("%s[viewportCoords=%s,%s,padding=%.0f,%.0f,%.0f,%.0f,wxh=%sx%s]",
        getClass().getSimpleName(), (int) viewportCoords.x, (int) viewportCoords.y, padding.x,
        padding.y, padding.z, padding.w, (int) hints.getViewportWidth(),
        (int) hints.getViewportHeight());
  }
}
