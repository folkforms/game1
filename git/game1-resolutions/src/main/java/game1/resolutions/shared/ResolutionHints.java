package game1.resolutions.shared;

public interface ResolutionHints {

  public void setWindowSize(int windowWidth, int windowHeight);

  public void setResolution(int newResolutionWidth, int newResolutionHeight);

  public void setLevelSize(int newLevelWidth, int newLevelHeight);

  /* Used when the scaled resolution is smaller than the window */
  public float getResolutionOffsetX();

  /* Used when the scaled resolution is smaller than the window */
  public float getResolutionOffsetY();

  public float getResolutionScale();

  public float getResolutionWidth();

  public float getResolutionHeight();

  public float getLevelWidth();

  public float getLevelHeight();

  public int getWindowWidth();

  public int getWindowHeight();

  public float getViewportWidth();

  public float getViewportHeight();
}
