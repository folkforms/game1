package game1.resolutions.shared;

public class ResolutionCalculations {

  private float offsetX;
  private float offsetY;
  private float scale;
  private float viewportWidth;
  private float viewportHeight;

  public ResolutionCalculations(float offsetX, float offsetY, float scale, float viewportWidth,
      float viewportHeight) {
    this.offsetX = offsetX;
    this.offsetY = offsetY;
    this.scale = scale;
    this.viewportWidth = viewportWidth;
    this.viewportHeight = viewportHeight;
  }

  public float getOffsetX() {
    return offsetX;
  }

  public float getOffsetY() {
    return offsetY;
  }

  public float getScale() {
    return scale;
  }

  public float getViewportWidth() {
    return viewportWidth;
  }

  public float getViewportHeight() {
    return viewportHeight;
  }
}
