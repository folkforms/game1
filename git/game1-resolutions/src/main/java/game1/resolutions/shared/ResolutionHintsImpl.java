package game1.resolutions.shared;

import org.joml.Vector2i;

import game1.resolutions.scaling.ScalingStrategy;

public class ResolutionHintsImpl implements ResolutionHints {

  private ScalingStrategy scalingStrategy;
  private Vector2i resolutionSize;
  private Vector2i levelSize;
  private Vector2i windowSize;
  private float resolutionScale;
  private float resolutionOffsetX;
  private float resolutionOffsetY;
  private float viewportWidth;
  private float viewportHeight;

  public ResolutionHintsImpl(ScalingStrategy scalingStrategy) {
    this.scalingStrategy = scalingStrategy;
  }

  public void setScalingStrategy(ScalingStrategy scalingStrategy) {
    this.scalingStrategy = scalingStrategy;
    recalculate();
  }

  public String debug_getScalingStrategy() {
    return scalingStrategy.getClass().getSimpleName();
  }

  @Override
  public void setWindowSize(int windowWidth, int windowHeight) {
    windowSize = new Vector2i(windowWidth, windowHeight);
    recalculate();
  }

  @Override
  public void setResolution(int newResolutionWidth, int newResolutionHeight) {
    resolutionSize = new Vector2i(newResolutionWidth, newResolutionHeight);
    recalculate();
  }

  @Override
  public void setLevelSize(int newLevelWidth, int newLevelHeight) {
    levelSize = new Vector2i(newLevelWidth, newLevelHeight);
    recalculate();
  }

  // FIXME RESOLUTIONS: Is there a correct place to call this as opposed to triggering it after
  // every 'set' method?

  private void recalculate() {
    if (resolutionSize == null) {
      return;
    }
    if (levelSize == null) {
      return;
    }
    if (windowSize == null) {
      return;
    }

    ResolutionCalculations rc = scalingStrategy.calculate(resolutionSize.x, resolutionSize.y,
        levelSize.x, levelSize.y, windowSize.x, windowSize.y);
    resolutionScale = rc.getScale();
    resolutionOffsetX = rc.getOffsetX();
    resolutionOffsetY = rc.getOffsetY();
    viewportWidth = rc.getViewportWidth();
    viewportHeight = rc.getViewportHeight();
  }

  @Override
  public float getResolutionOffsetX() {
    return resolutionOffsetX;
  }

  @Override
  public float getResolutionOffsetY() {
    return resolutionOffsetY;
  }

  @Override
  public float getResolutionScale() {
    return resolutionScale;
  }

  @Override
  public float getResolutionWidth() {
    return resolutionSize.x;
  }

  @Override
  public float getResolutionHeight() {
    return resolutionSize.y;
  }

  @Override
  public float getLevelWidth() {
    return levelSize.x;
  }

  @Override
  public float getLevelHeight() {
    return levelSize.y;
  }

  @Override
  public int getWindowWidth() {
    return windowSize.x;
  }

  @Override
  public int getWindowHeight() {
    return windowSize.y;
  }

  @Override
  public float getViewportWidth() {
    return viewportWidth;
  }

  @Override
  public float getViewportHeight() {
    return viewportHeight;
  }

  @Override
  public String toString() {
    return String.format(
        "%s[scale=%s,offsetX=%s,offsetY=%s,resolutionSize=%sx%s,levelSize=%sx%s,windowSize=%sx%s,scalingStrategy=%s]",
        getClass().getSimpleName(), resolutionScale, resolutionOffsetX, resolutionOffsetY,
        resolutionSize.x, resolutionSize.y, levelSize.x, levelSize.y, windowSize.x, windowSize.y,
        scalingStrategy.getClass().getSimpleName());
  }
}
