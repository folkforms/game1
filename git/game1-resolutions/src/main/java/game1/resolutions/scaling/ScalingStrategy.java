package game1.resolutions.scaling;

import game1.resolutions.shared.ResolutionCalculations;

public interface ScalingStrategy {

  ResolutionCalculations calculate(int resolutionWidth, int resolutionHeight, int levelWidth,
      int levelHeight, int windowWidth, int windowHeight);
}
