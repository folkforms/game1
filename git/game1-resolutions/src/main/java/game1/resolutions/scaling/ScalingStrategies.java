package game1.resolutions.scaling;

import java.util.List;

public class ScalingStrategies {

  private static List<ScalingStrategy> scalingStrategies = List.of(
      new ResolutionScalingStrategyV0(), new ResolutionScalingStrategyV1(),
      new ResolutionScalingStrategyV2());

  public static ScalingStrategy get(int scalingStrategy) {
    return scalingStrategies.get(scalingStrategy);
  }
}
