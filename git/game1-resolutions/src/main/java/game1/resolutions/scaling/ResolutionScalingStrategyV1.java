package game1.resolutions.scaling;

import game1.resolutions.shared.ResolutionCalculations;

/**
 * Uses non-integer scaling. A horizontally-scrolling level should have no black bars.
 */
public class ResolutionScalingStrategyV1 implements ScalingStrategy {

  @Override
  public ResolutionCalculations calculate(int resolutionWidth, int resolutionHeight, int levelWidth,
      int levelHeight, int windowWidth, int windowHeight) {

    float resolutionWidthF = resolutionWidth;
    float resolutionHeightF = resolutionHeight;
    float windowWidthF = windowWidth;
    float windowHeightF = windowHeight;
    float scale = Math.min(windowHeightF / resolutionHeightF, windowWidthF / resolutionWidthF);
    scale = round(scale, 3);

    int scaledLevelWidth = (int) (levelWidth * scale);
    int scaledLevelHeight = (int) (levelHeight * scale);

    float offsetX = scaledLevelWidth > windowWidth ? 0 : (windowWidth - scaledLevelWidth) / 2;
    float offsetY = scaledLevelHeight > windowHeight ? 0 : (windowHeight - scaledLevelHeight) / 2;

    float viewportWidth = scaledLevelWidth >= windowWidth ? windowWidth / scale : resolutionWidth;
    float viewportHeight = scaledLevelHeight >= windowHeight ? windowHeight / scale : resolutionHeight;

    return new ResolutionCalculations(offsetX, offsetY, scale, round(viewportWidth, 1),
        round(viewportHeight, 1));
  }

  private float round(float f, int numDecimalPlaces) {
    int mul = (int) Math.pow(10, numDecimalPlaces);
    return ((float) Math.round(f * mul)) / mul;
  }
}
