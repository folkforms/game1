package game1.resolutions.scaling;

import java.util.Stack;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import game1.resolutions.shared.ResolutionCalculations;

/**
 * Scales scene in integer intervals to find the largest size that will fit in the window.
 *
 * For horizontally-scrolling levels it tends to have black bars on the left and right.
 */
public class ResolutionScalingStrategyV0 implements ScalingStrategy {

  private Vector3f findBestFit(int resolutionWidth, int resolutionHeight, int windowWidth, int windowHeight) {
    Vector2i monitor = new Vector2i(windowWidth, windowHeight);
    Vector2i game = new Vector2i(resolutionWidth, resolutionHeight);
    Stack<Vector2i> stack = new Stack<>();
    int scale = 0;
    boolean fits = false;
    do {
      scale++;
      Vector2i nextScaleUp = new Vector2i();
      game.mul(scale, nextScaleUp);
      fits = fits(nextScaleUp, monitor);
      if (fits) {
        stack.push(nextScaleUp);
      }
    } while (fits);
    scale--;

    if (stack.size() == 0) {
      throw new RuntimeException(
          String.format("Game is too big for window (game %sx%s vs window %sx%s)", resolutionWidth,
              resolutionHeight, windowWidth, windowHeight));
    }

    Vector2i bestScale = stack.pop();
    int scaledResolutionWidth = bestScale.x;
    int scaledResolutionHeight = bestScale.y;

    int offsetX = (windowWidth - scaledResolutionWidth) / 2;
    int offsetY = (windowHeight - scaledResolutionHeight) / 2;

    return new Vector3f(offsetX, offsetY, scale);
  }

  private boolean fits(Vector2i game, Vector2i monitor) {
    return game.x <= monitor.x && game.y <= monitor.y;
  }

  private Vector2f findViewportSize(int resolutionWidth, int resolutionHeight, int windowWidth,
      int windowHeight) {
    return new Vector2f(resolutionWidth, resolutionHeight);
  }

  @Override
  public ResolutionCalculations calculate(int resolutionWidth, int resolutionHeight, int levelWidth,
      int levelHeight, int windowWidth, int windowHeight) {
    Vector3f bestFit = findBestFit(resolutionWidth, resolutionHeight, windowWidth, windowHeight);
    Vector2f viewportSize = findViewportSize(resolutionWidth, resolutionHeight, windowWidth, windowHeight);
    return new ResolutionCalculations(bestFit.x, bestFit.y, bestFit.z, viewportSize.x,
        viewportSize.y);
  }
}
