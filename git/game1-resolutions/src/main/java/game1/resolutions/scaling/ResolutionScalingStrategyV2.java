package game1.resolutions.scaling;

import game1.resolutions.shared.ResolutionCalculations;

/**
 * Uses integer scaling. A horizontally-scrolling level may have black bars at the top and bottom,
 * but will not have black bars on the left and right.
 */
public class ResolutionScalingStrategyV2 implements ScalingStrategy {

  @Override
  public ResolutionCalculations calculate(int resolutionWidth, int resolutionHeight, int levelWidth,
      int levelHeight, int windowWidth, int windowHeight) {

    float resolutionWidthF = resolutionWidth;
    float resolutionHeightF = resolutionHeight;
    float windowWidthF = windowWidth;
    float windowHeightF = windowHeight;
    float scale = Math.min(windowHeightF / resolutionHeightF, windowWidthF / resolutionWidthF);
    scale = (float) Math.floor(scale);

    int scaledLevelWidth = (int) (levelWidth * scale);
    int scaledLevelHeight = (int) (levelHeight * scale);

    float offsetX = scaledLevelWidth >= windowWidth ? 0 : (windowWidth - scaledLevelWidth) / 2;
    float offsetY = scaledLevelHeight >= windowHeight ? 0 : (windowHeight - scaledLevelHeight) / 2;
    float viewportWidth = scaledLevelWidth >= windowWidth ? windowWidth / scale : resolutionWidth;
    float viewportHeight = scaledLevelHeight >= windowHeight ? windowHeight / scale : resolutionHeight;

    return new ResolutionCalculations(offsetX, offsetY, scale, viewportWidth, viewportHeight);
  }
}
