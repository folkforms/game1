package game1.resolutions.viewports;

import org.joml.Vector2f;
import org.joml.Vector4f;

public class DummySceneViewport extends SceneViewport {

  public DummySceneViewport(String resolutionLayerName, Vector2f viewportCoords, Vector4f padding) {
    super(resolutionLayerName, viewportCoords, padding);
  }

  private boolean triggered = false;

  @Override
  protected boolean checkXNegativeCondition() {
    return false;
  }

  @Override
  protected boolean checkXPositiveCondition() {
    if (triggered) {
      return false;
    } else {
      triggered = true;
      return true;
    }
  }

  @Override
  protected boolean checkYNegativeCondition() {
    return false;
  }

  @Override
  protected boolean checkYPositiveCondition() {
    return false;
  }

  @Override
  protected void moveVx(float dx) {
    viewportCoords.x += dx;
  }

  @Override
  protected void moveVy(float dy) {
    viewportCoords.y += dy;
  }
}
