package game1.resolutions.viewports;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector4f;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.scaling.ResolutionScalingStrategyV1;

public class SceneViewportTest {

  private String resolutionLayerName = "test-layer";

  @BeforeEach
  public void beforeEach() {
    ResolutionLayersFactory.getInstance().init(1920, 1080);
    ResolutionLayersFactory.getInstance().createLayer(resolutionLayerName, new Vector2i(480, 270),
        new Vector2i(961, 541), new ResolutionScalingStrategyV1(), null);
  }

  @Test
  public void itUpdatesXAndYBasedOnConditions() {
    DummySceneViewport v = new DummySceneViewport(resolutionLayerName, new Vector2f(0, 0),
        new Vector4f(10, 10, 10, 10));
    assertEquals(0, v.getX());
    assertEquals(0, v.getY());
    v.updateViewport();
    assertEquals(1, v.getX());
    assertEquals(0, v.getY());
    v.updateViewport();
    assertEquals(1, v.getX());
    assertEquals(0, v.getY());
  }

  @Test
  public void itCorrectsTheViewportPositionIfItStartsOutOfBounds() {
    DummySceneViewport outOfBoundsTop = new DummySceneViewport(resolutionLayerName,
        new Vector2f(0, 1000), new Vector4f(10, 10, 10, 10));
    assertEquals(0, outOfBoundsTop.getX());
    assertEquals(271, outOfBoundsTop.getY());

    DummySceneViewport outOfBoundsRight = new DummySceneViewport(resolutionLayerName,
        new Vector2f(2000, 0), new Vector4f(10, 10, 10, 10));
    assertEquals(481, outOfBoundsRight.getX());
    assertEquals(0, outOfBoundsRight.getY());

    DummySceneViewport outOfBoundsBottom = new DummySceneViewport(resolutionLayerName,
        new Vector2f(0, -1000), new Vector4f(10, 10, 10, 10));
    assertEquals(0, outOfBoundsBottom.getX());
    assertEquals(0, outOfBoundsBottom.getY());

    DummySceneViewport outOfBoundsTopLeft = new DummySceneViewport(resolutionLayerName,
        new Vector2f(-1000, 0), new Vector4f(10, 10, 10, 10));
    assertEquals(0, outOfBoundsTopLeft.getX());
    assertEquals(0, outOfBoundsTopLeft.getY());
  }
}
