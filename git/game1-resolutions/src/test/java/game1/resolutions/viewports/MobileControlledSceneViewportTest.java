package game1.resolutions.viewports;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;
import org.joml.Vector4f;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import game1.actors.Mobile;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.scaling.ResolutionScalingStrategyV1;
import game1.resolutions.scaling.ResolutionScalingStrategyV2;

public class MobileControlledSceneViewportTest {

  private float mobZ = 10_000;
  private Mobile mobile = new DummySceneMobile(10, 10);
  private Vector4f tenPxPadding = new Vector4f(10, 10, 10, 10);
  private String resolutionLayerName = "test-layer";

  @BeforeEach
  public void beforeEach() {
    ResolutionLayersFactory.getInstance().init(1920, 1080);
    ResolutionLayersFactory.getInstance().createLayer(resolutionLayerName, new Vector2i(320, 180),
        new Vector2i(640, 360), new ResolutionScalingStrategyV1(), null);
  }

  @Test
  public void itMovesRight() {
    SceneViewport viewport = new MobileCentrePointControlledSceneViewport(mobile,
        resolutionLayerName, new Vector2f(0, 0), tenPxPadding);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    mobile.setPosition(new Vector3f(320, 0, mobZ));

    viewport.updateViewport();
    assertEquals(15, viewport.getX());
    assertEquals(0, viewport.getY());
  }

  @Test
  public void itMovesLeft() {
    SceneViewport viewport = new MobileCentrePointControlledSceneViewport(mobile,
        resolutionLayerName, new Vector2f(320, 0), tenPxPadding);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    mobile.setPosition(new Vector3f(320, 0, mobZ));

    viewport.updateViewport();
    assertEquals(315, viewport.getX());
    assertEquals(0, viewport.getY());
    viewport.updateViewport();
    assertEquals(315, viewport.getX());
    assertEquals(0, viewport.getY());
  }

  @Test
  public void itMovesUp() {
    SceneViewport viewport = new MobileCentrePointControlledSceneViewport(mobile,
        resolutionLayerName, new Vector2f(0, 0), tenPxPadding);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    mobile.setPosition(new Vector3f(0, 180, mobZ));

    viewport.updateViewport();
    assertEquals(0, viewport.getX());
    assertEquals(15, viewport.getY());
    viewport.updateViewport();
    assertEquals(0, viewport.getX());
    assertEquals(15, viewport.getY());
  }

  @Test
  public void itMovesDown() {
    SceneViewport viewport = new MobileCentrePointControlledSceneViewport(mobile,
        resolutionLayerName, new Vector2f(0, 100), tenPxPadding);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    mobile.setPosition(new Vector3f(0, 100, mobZ));

    viewport.updateViewport();
    assertEquals(0, viewport.getX());
    assertEquals(95, viewport.getY());
    viewport.updateViewport();
    assertEquals(0, viewport.getX());
    assertEquals(95, viewport.getY());
  }

  @Test
  public void itDoesNotMovePastRightEdge() {
    SceneViewport viewport = new MobileCentrePointControlledSceneViewport(mobile,
        resolutionLayerName, new Vector2f(319, 0), tenPxPadding);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    mobile.setPosition(new Vector3f(640, 0, mobZ));

    assertEquals(319, viewport.getX());
    assertEquals(0, viewport.getY());
    viewport.updateViewport();
    assertEquals(320, viewport.getX());
    assertEquals(0, viewport.getY());
    viewport.updateViewport();
    assertEquals(320, viewport.getX());
    assertEquals(0, viewport.getY());
  }

  @Test
  public void itDoesNotMovePastLeftEdge() {
    SceneViewport viewport = new MobileCentrePointControlledSceneViewport(mobile,
        resolutionLayerName, new Vector2f(1, 0), tenPxPadding);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    mobile.setPosition(new Vector3f(0, 75, mobZ));

    assertEquals(1, viewport.getX());
    assertEquals(0, viewport.getY());
    viewport.updateViewport();
    assertEquals(0, viewport.getX());
    assertEquals(0, viewport.getY());
    viewport.updateViewport();
    assertEquals(0, viewport.getX());
    assertEquals(0, viewport.getY());
  }

  @Test
  public void itDoesNotMovePastTopEdge() {
    SceneViewport viewport = new MobileCentrePointControlledSceneViewport(mobile,
        resolutionLayerName, new Vector2f(0, 179), tenPxPadding);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    mobile.setPosition(new Vector3f(100, 360, mobZ));

    assertEquals(0, viewport.getX());
    assertEquals(179, viewport.getY());
    viewport.updateViewport();
    assertEquals(0, viewport.getX());
    assertEquals(180, viewport.getY());
    viewport.updateViewport();
    assertEquals(0, viewport.getX());
    assertEquals(180, viewport.getY());
  }

  @Test
  public void itDoesNotMovePastBottomEdge() {
    SceneViewport viewport = new MobileCentrePointControlledSceneViewport(mobile,
        resolutionLayerName, new Vector2f(0, 1), tenPxPadding);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    mobile.setPosition(new Vector3f(0, 0, mobZ));

    assertEquals(0, viewport.getX());
    assertEquals(1, viewport.getY());
    viewport.updateViewport();
    assertEquals(0, viewport.getX());
    assertEquals(0, viewport.getY());
    viewport.updateViewport();
    assertEquals(0, viewport.getX());
    assertEquals(0, viewport.getY());
  }

  @Test
  public void itMovesUpAndRight() {
    SceneViewport viewport = new MobileCentrePointControlledSceneViewport(mobile,
        resolutionLayerName, new Vector2f(0, 0), tenPxPadding);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    mobile.setPosition(new Vector3f(320, 180, mobZ));

    viewport.updateViewport();
    assertEquals(15, viewport.getX());
    assertEquals(15, viewport.getY());
  }

  @Test
  public void itMovesDownAndRight() {
    SceneViewport viewport = new MobileCentrePointControlledSceneViewport(mobile,
        resolutionLayerName, new Vector2f(0, 100), tenPxPadding);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    mobile.setPosition(new Vector3f(320, 100, mobZ));

    viewport.updateViewport();
    assertEquals(15, viewport.getX());
    assertEquals(95, viewport.getY());
  }

  @Test
  public void itMovesDownAndLeft() {
    SceneViewport viewport = new MobileCentrePointControlledSceneViewport(mobile,
        resolutionLayerName, new Vector2f(100, 100), tenPxPadding);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    mobile.setPosition(new Vector3f(100, 100, mobZ));

    viewport.updateViewport();
    assertEquals(95, viewport.getX());
    assertEquals(95, viewport.getY());
  }

  @Test
  public void itMovesUpAndLeft() {
    SceneViewport viewport = new MobileCentrePointControlledSceneViewport(mobile,
        resolutionLayerName, new Vector2f(100, 0), tenPxPadding);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    mobile.setPosition(new Vector3f(100, 180, mobZ));

    viewport.updateViewport();
    assertEquals(95, viewport.getX());
    assertEquals(15, viewport.getY());
  }

  /**
   * ResolutionV2ScalingStrategy will use a viewport size of 640x320 despite the scene size of
   * 569x320. We need to check that the viewport only moves when the mobile is at x=640, not x=569
   * (padding notwithstanding).
   */
  @Test
  public void itCorrectlyHandlesViewports() {
    ResolutionLayersFactory.getInstance().init(2560, 1440);
    ResolutionLayersFactory.getInstance().createLayer(resolutionLayerName, new Vector2i(569, 320),
        new Vector2i(900, 320), new ResolutionScalingStrategyV2(), null);

    // Scene viewport will be 640x320
    SceneViewport viewport = new MobileCentrePointControlledSceneViewport(mobile,
        resolutionLayerName, new Vector2f(0, 0), tenPxPadding);
    ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayerName).setViewport(viewport);

    // When mobile moves to position in scene 564,75 (focus point 569,80) the viewport does not move
    mobile.setPosition(new Vector3f(564, 75, mobZ));

    viewport.updateViewport();
    assertEquals(0, viewport.getX());
    assertEquals(0, viewport.getY());

    // When mobile moves to position in scene 635,75 (focus point 640,80) the viewport moves
    mobile.setPosition(new Vector3f(635, 75, mobZ));

    viewport.updateViewport();
    assertEquals(10, viewport.getX());
    assertEquals(0, viewport.getY());
  }
}
