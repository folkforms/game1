package game1.resolutions.viewports;

import org.joml.Vector3f;

import game1.actors.Mobile;

public class DummySceneMobile implements Mobile {

  private Vector3f position;
  private float mobileWidthInScene;
  private float mobileHeightInScene;

  public DummySceneMobile(float mobileWidthInScene, float mobileHeightInScene) {
    this.mobileWidthInScene = mobileWidthInScene;
    this.mobileHeightInScene = mobileHeightInScene;
  }

  @Override
  public float getWidth() {
    return mobileWidthInScene;
  }

  @Override
  public float getHeight() {
    return mobileHeightInScene;
  }

  @Override
  public Vector3f getPosition() {
    return position;
  }

  @Override
  public void setPosition(Vector3f newPosition) {
    position = newPosition;
  }

  @Override
  public void setPosition(float x, float y, float z) {
    position = new Vector3f(x, y, z);
  }
}
