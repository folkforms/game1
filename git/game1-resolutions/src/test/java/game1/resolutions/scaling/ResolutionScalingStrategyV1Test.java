package game1.resolutions.scaling;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import game1.resolutions.shared.ResolutionCalculations;

class ResolutionScalingStrategyV1Test {

  private ResolutionScalingStrategyV1 ss = new ResolutionScalingStrategyV1();

  @Test
  public void itCalculatesCorrectly1() {
    ResolutionCalculations rc = ss.calculate(569, 320, 569, 320, 2560, 1440);
    assertEquals(4.5f, rc.getScale(), 0.01f);
    assertEquals(0, rc.getOffsetX());
    assertEquals(0, rc.getOffsetY());
    assertEquals(569, rc.getViewportWidth());
    assertEquals(320, rc.getViewportHeight());
  }

  @Test
  public void itCalculatesCorrectly2() {
    ResolutionCalculations rc = ss.calculate(569, 320, 900, 320, 2560, 1440);
    assertEquals(4.5f, rc.getScale(), 0.01f);
    assertEquals(0, rc.getOffsetX());
    assertEquals(0, rc.getOffsetY());
    assertEquals(569, rc.getViewportWidth());
    assertEquals(320, rc.getViewportHeight());
  }

  @Test
  public void itCalculatesCorrectly3() {
    ResolutionCalculations rc = ss.calculate(569, 320, 569, 800, 2560, 1440);
    assertEquals(4.5f, rc.getScale(), 0.01f);
    assertEquals(0, rc.getOffsetX());
    assertEquals(0, rc.getOffsetY());
    assertEquals(569, rc.getViewportWidth());
    assertEquals(320.1f, rc.getViewportHeight());
  }

  /**
   * Letterbox level that horizontally scales to the same size as the window.
   */
  @Test
  public void itCalculatesCorrectly4() {
    ResolutionCalculations rc = ss.calculate(569, 200, 569, 200, 2560, 1440);
    assertEquals(4.5f, rc.getScale(), 0.01f);
    assertEquals(0, rc.getOffsetX());
    assertEquals(270, rc.getOffsetY());
    assertEquals(569, rc.getViewportWidth());
    assertEquals(200, rc.getViewportHeight());
  }

  /**
   * Letterbox level that is wider than the window.
   */
  @Test
  public void itCalculatesCorrectly5() {
    ResolutionCalculations rc = ss.calculate(569, 200, 900, 200, 2560, 1440);
    assertEquals(4.5f, rc.getScale(), 0.01f);
    assertEquals(0, rc.getOffsetX());
    assertEquals(270, rc.getOffsetY());
    assertEquals(569, rc.getViewportWidth());
    assertEquals(200, rc.getViewportHeight());
  }

  /**
   * Small 16:9 level in a 16:10 window.
   */
  @Test
  public void itCalculatesCorrectly6() {
    ResolutionCalculations rc = ss.calculate(569, 320, 569, 320, 2560, 1600);
    assertEquals(4.5f, rc.getScale(), 0.01f);
    assertEquals(0, rc.getOffsetX());
    assertEquals(80, rc.getOffsetY());
    assertEquals(569, rc.getViewportWidth());
    assertEquals(320, rc.getViewportHeight());
  }

  /**
   * Letterbox 16:9 level in a 16:10 window.
   */
  @Test
  public void itCalculatesCorrectly7() {
    ResolutionCalculations rc = ss.calculate(569, 320, 900, 320, 2560, 1600);
    assertEquals(4.5f, rc.getScale(), 0.01f);
    assertEquals(0, rc.getOffsetX());
    assertEquals(80, rc.getOffsetY());
    assertEquals(569, rc.getViewportWidth());
    assertEquals(320, rc.getViewportHeight());
  }

  /**
   * Pillarbox 16:9 level in a 16:10 window.
   */
  @Test
  public void itCalculatesCorrectly8() {
    ResolutionCalculations rc = ss.calculate(569, 320, 569, 800, 2560, 1600);
    assertEquals(4.5f, rc.getScale(), 0.01f);
    assertEquals(0, rc.getOffsetX());
    assertEquals(0, rc.getOffsetY());
    assertEquals(569, rc.getViewportWidth());
    assertEquals(355.6f, rc.getViewportHeight());
  }

  @Test
  public void itCalculatesCorrectly9() {
    ResolutionCalculations rc = ss.calculate(100, 50, 200, 50, 2560, 1440);
    assertEquals(25.6f, rc.getScale());
    assertEquals(0, rc.getOffsetX());
    assertEquals(80, rc.getOffsetY());
    assertEquals(100, rc.getViewportWidth());
    assertEquals(50, rc.getViewportHeight());
  }

  @Test
  public void itCalculatesCorrectly10() {
    ResolutionCalculations rc = ss.calculate(1920, 1080, 1920, 1080, 2560, 1440);
    assertEquals(1.333f, rc.getScale());
    assertEquals(0, rc.getOffsetX());
    assertEquals(0, rc.getOffsetY());
    assertEquals(1920, rc.getViewportWidth());
    assertEquals(1080, rc.getViewportHeight());
  }

  @Test
  public void itCalculatesCorrectly11() {
    ResolutionCalculations rc = ss.calculate(320, 180, 640, 180, 3840, 1080);
    assertEquals(6f, rc.getScale());
    assertEquals(0, rc.getOffsetX());
    assertEquals(0, rc.getOffsetY());
    assertEquals(640, rc.getViewportWidth());
    assertEquals(180, rc.getViewportHeight());
  }
}
