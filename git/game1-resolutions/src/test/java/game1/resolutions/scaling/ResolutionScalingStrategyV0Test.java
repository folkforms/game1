package game1.resolutions.scaling;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.joml.Vector2i;
import org.junit.jupiter.api.Test;

import game1.resolutions.shared.ResolutionCalculations;

class ResolutionScalingStrategyV0Test {

  private ResolutionScalingStrategyV0 scalingStrategy = new ResolutionScalingStrategyV0();

  void test(Vector2i sceneSize, Vector2i levelSize, Vector2i windowSize, float expectedScale,
      float expectedOffsetX, float expectedOffsetY, float expectedViewportWidth,
      float expectedViewportHeight) {

    ResolutionCalculations rc = scalingStrategy.calculate(sceneSize.x, sceneSize.y, levelSize.x,
        levelSize.y, windowSize.x, windowSize.y);
    assertEquals(expectedScale, rc.getScale());
    assertEquals(expectedOffsetX, rc.getOffsetX());
    assertEquals(expectedOffsetY, rc.getOffsetY());
    assertEquals(expectedViewportWidth, rc.getViewportWidth());
    assertEquals(expectedViewportHeight, rc.getViewportHeight());
  }

  @Test
  void itFitsWhenGameIsExactlyWindowSize() {
    Vector2i sceneSize = new Vector2i(1920, 1080);
    Vector2i levelSize = new Vector2i(1920, 1080);
    Vector2i windowSize = new Vector2i(1920, 1080);
    test(sceneSize, levelSize, windowSize, 1, 0, 0, 1920, 1080);
  }

  @Test
  void itAddsBlackBarsTopAndBottom() {
    Vector2i sceneSize = new Vector2i(1920, 1080);
    Vector2i levelSize = new Vector2i(1920, 1080);
    Vector2i windowSize = new Vector2i(1920, 1200);
    test(sceneSize, levelSize, windowSize, 1, 0, 60, 1920, 1080);
  }

  @Test
  void itAddsBlackBarsLeftAndRight() {
    Vector2i sceneSize = new Vector2i(1920, 1080);
    Vector2i levelSize = new Vector2i(1920, 1080);
    Vector2i windowSize = new Vector2i(2156, 1080);
    test(sceneSize, levelSize, windowSize, 1, 118, 0, 1920, 1080);
  }

  @Test
  void itAddsBlackBarsAllAround() {
    Vector2i sceneSize = new Vector2i(1920, 1080);
    Vector2i levelSize = new Vector2i(1920, 1080);
    Vector2i windowSize = new Vector2i(2156, 1200);
    test(sceneSize, levelSize, windowSize, 1, 118, 60, 1920, 1080);
  }

  @Test
  void itAddsBlackBarsAllAround2() {
    Vector2i sceneSize = new Vector2i(1920, 1080);
    Vector2i levelSize = new Vector2i(1920, 1080);
    Vector2i windowSize = new Vector2i(2560, 1440);
    test(sceneSize, levelSize, windowSize, 1, 320, 180, 1920, 1080);
  }

  @Test
  void itAddsBlackBarsAllAround3() {
    Vector2i sceneSize = new Vector2i(569, 320);
    Vector2i levelSize = new Vector2i(569, 320);
    Vector2i windowSize = new Vector2i(2560, 1440);
    test(sceneSize, levelSize, windowSize, 4, 142, 80, 569, 320);
  }

  @Test
  void itScalesTo1920x1080v1() {
    Vector2i sceneSize = new Vector2i(600, 400);
    Vector2i levelSize = new Vector2i(600, 400);
    Vector2i windowSize = new Vector2i(1920, 1080);
    test(sceneSize, levelSize, windowSize, 2, 360, 140, 600, 400);
  }

  @Test
  void itScalesTo1920x1080v2() {
    Vector2i sceneSize = new Vector2i(400, 256);
    Vector2i levelSize = new Vector2i(400, 256);
    Vector2i windowSize = new Vector2i(1920, 1080);
    test(sceneSize, levelSize, windowSize, 4, 160, 28, 400, 256);
  }

  @Test
  void itScalesTo1280x720v1() {
    Vector2i sceneSize = new Vector2i(640, 360);
    Vector2i levelSize = new Vector2i(640, 360);
    Vector2i windowSize = new Vector2i(1280, 720);
    test(sceneSize, levelSize, windowSize, 2, 0, 0, 640, 360);
  }

  @Test
  void itScalesTo1280x720v2() {
    Vector2i sceneSize = new Vector2i(480, 270);
    Vector2i levelSize = new Vector2i(480, 270);
    Vector2i windowSize = new Vector2i(1280, 720);
    test(sceneSize, levelSize, windowSize, 2, 160, 90, 480, 270);
  }

  @Test
  void itScalesTo1920x1200v1() {
    Vector2i sceneSize = new Vector2i(600, 400);
    Vector2i levelSize = new Vector2i(600, 400);
    Vector2i windowSize = new Vector2i(1920, 1200);
    test(sceneSize, levelSize, windowSize, 3, 60, 0, 600, 400);
  }

  @Test
  void itScalesTo1920x1200v2() {
    Vector2i sceneSize = new Vector2i(400, 256);
    Vector2i levelSize = new Vector2i(400, 256);
    Vector2i windowSize = new Vector2i(1920, 1200);
    test(sceneSize, levelSize, windowSize, 4, 160, 88, 400, 256);
  }

  @Test
  void itScalesTo3840x2160v1() {
    Vector2i sceneSize = new Vector2i(600, 400);
    Vector2i levelSize = new Vector2i(600, 400);
    Vector2i windowSize = new Vector2i(3840, 2160);
    test(sceneSize, levelSize, windowSize, 5, 420, 80, 600, 400);
  }

  @Test
  void itScalesTo3840x2160v2() {
    Vector2i sceneSize = new Vector2i(400, 256);
    Vector2i levelSize = new Vector2i(400, 256);
    Vector2i windowSize = new Vector2i(3840, 2160);
    test(sceneSize, levelSize, windowSize, 8, 320, 56, 400, 256);
  }

  @Test
  void itScalesTo1600x1200() {
    Vector2i sceneSize = new Vector2i(480, 270);
    Vector2i levelSize = new Vector2i(480, 270);
    Vector2i windowSize = new Vector2i(1600, 1200);
    test(sceneSize, levelSize, windowSize, 3, 80, 195, 480, 270);
  }

  @Test
  void itScalesTo1366x768() {
    Vector2i sceneSize = new Vector2i(480, 270);
    Vector2i levelSize = new Vector2i(480, 270);
    Vector2i windowSize = new Vector2i(1366, 768);
    test(sceneSize, levelSize, windowSize, 2, 203, 114, 480, 270);
  }

  // @Test
  // void itThrowsIfGameCannotFitInWindow() {
  // RuntimeException ex = assertThrows(RuntimeException.class,
  // () -> scalingStrategy.findBestFit(1920, 1080, 1600, 1200));
  // String expectedMessage = "Game is too big for window (game 1920x1080 vs window 1600x1200)";
  // assertEquals(expectedMessage, ex.getMessage());
  // }
  //
  // @Test
  // void ifThrowsIfGameIsTooTallForWindow() {
  // RuntimeException ex = assertThrows(RuntimeException.class,
  // () -> scalingStrategy.findBestFit(1920, 1080, 1920, 1000));
  // String expectedMessage = "Game is too big for window (game 1920x1080 vs window 1920x1000)";
  // assertEquals(expectedMessage, ex.getMessage());
  // }
  //
  // @Test
  // void ifThrowsIfGameIsTooWideForWindow() {
  // RuntimeException ex = assertThrows(RuntimeException.class,
  // () -> scalingStrategy.findBestFit(1920, 1080, 1600, 1080));
  // String expectedMessage = "Game is too big for window (game 1920x1080 vs window 1600x1080)";
  // assertEquals(expectedMessage, ex.getMessage());
  // }
  //
  // @Test
  // public void itCalculatesViewportSizeCorrectly() {
  // ResolutionV0ScalingStrategy ss = new ResolutionV0ScalingStrategy();
  // Vector2f viewportSize = ss.findViewportSize(426, 240, 1920, 1080);
  // assertEquals(426, viewportSize.x);
  // assertEquals(240, viewportSize.y);
  // }
  //
  // @Test
  // public void itCalculatesViewportSizeCorrectly2() {
  // ResolutionV0ScalingStrategy ss = new ResolutionV0ScalingStrategy();
  // Vector2f viewportSize = ss.findViewportSize(527, 360, 2560, 1440);
  // assertEquals(527, viewportSize.x);
  // assertEquals(360, viewportSize.y);
  // }

  @Test
  public void itFoos() {
    ResolutionScalingStrategyV0 ss = new ResolutionScalingStrategyV0();
    ResolutionCalculations rc = ss.calculate(569, 320, 569, 320, 2560, 1440);
    assertEquals(142, rc.getOffsetX());
    assertEquals(80, rc.getOffsetY());
    assertEquals(4, rc.getScale());
    assertEquals(569, rc.getViewportWidth());
    assertEquals(320, rc.getViewportHeight());
  }

  @Test
  public void itFoos2() {
    ResolutionScalingStrategyV0 ss = new ResolutionScalingStrategyV0();
    ResolutionCalculations rc = ss.calculate(569, 320, 900, 320, 2560, 1440);
    assertEquals(142, rc.getOffsetX());
    assertEquals(80, rc.getOffsetY());
    assertEquals(4, rc.getScale());
    assertEquals(569, rc.getViewportWidth());
    assertEquals(320, rc.getViewportHeight());
  }

  @Test
  public void itFoos3() {
    ResolutionScalingStrategyV0 ss = new ResolutionScalingStrategyV0();
    ResolutionCalculations rc = ss.calculate(569, 320, 569, 800, 2560, 1440);
    assertEquals(142, rc.getOffsetX());
    assertEquals(80, rc.getOffsetY());
    assertEquals(4, rc.getScale());
    assertEquals(569, rc.getViewportWidth());
    assertEquals(320, rc.getViewportHeight());
  }
}
