# game1-resolutions

Window size and scene size can be changed at any time.

## Scaling strategies

### V1

Scales the scene in integer intervals to find the largest size that will fit in the window.

For horizontally-scrolling levels it tends to have black bars on the left and right.


### V2

Scales the scene using non-integer scaling.

A horizontally-scrolling level should have no black bars unless the scene is less than one screen in width or height.

### V3

A mixture of v1 and v2. The scale is rounded down to the nearest integer.

But, unlike v1, a horizontally-scrolling level will not have black bars on the left and right.

## Viewports

The new viewport code is more specific as to the fact that its values are scene-scaled. At the minute we have `SceneViewport` and `MobileControlledSceneViewport`. This sort of thing is best as it is most specific, and we can check in `Transformation` whether the type is `SceneViewport` and offset by `x * scale` pixels.

It should be easy to add a window-scaled version if we ever need to.

If we do, we will need to update `Transformation` and `Mouse` to perform different calculations if the viewport is a SceneViewport or a WindowViewport.

