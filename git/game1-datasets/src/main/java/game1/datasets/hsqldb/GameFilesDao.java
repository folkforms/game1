package game1.datasets.hsqldb;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import folkforms.log.Log;
import game1.datasets.MultiLoaderOptions;

public class GameFilesDao {

  private Database database;

  public GameFilesDao(Database database) {
    this.database = database;
  }

  public void addGameFile(String path, String tag) {
    try {
      database.execute(
          String.format("INSERT INTO gameFiles (path, tag) values ('%s', '%s')", path, tag));
    } catch (SQLException ex) {
      throw new RuntimeException(ex);
    }
  }

  public void addGameFile(String path, String tag, MultiLoaderOptions options) {
    try {
      database.execute(
          String.format("INSERT INTO gameFiles (path, tag, options) values ('%s', '%s', '%s')",
              path, tag, options.toSerialisableString()));
    } catch (SQLException ex) {
      throw new RuntimeException(ex);
    }
  }

  public List<String> listGameFiles(List<String> tags) {
    try {
      String whereClause = tags.stream().map(tag -> String.format("tag = '%s'", tag))
          .collect(Collectors.joining(" OR "));
      List<String> files = new ArrayList<>();
      ResultSet rs = database
          .execute(String.format("SELECT path FROM gameFiles WHERE %s", whereClause));
      while (rs.next()) {
        files.add(rs.getString("path"));
      }
      return files;
    } catch (SQLException ex) {
      throw new RuntimeException(ex);
    }
  }

  public List<String> listGameFiles(String tag) {
    return listGameFiles(List.of(tag));
  }

  public List<String> listAllTags() {
    try {
      List<String> tags = new ArrayList<>();
      ResultSet rs = database.execute("SELECT DISTINCT(tag) FROM gameFiles");
      while (rs.next()) {
        tags.add(rs.getString("tag"));
      }
      return tags;
    } catch (SQLException ex) {
      throw new RuntimeException(ex);
    }
  }

  public Optional<MultiLoaderOptions> getOptions(String path, String tag) {
    try {
      MultiLoaderOptions options = null;
      ResultSet rs = database.execute(String
          .format("SELECT options FROM gameFiles WHERE path = '%s' AND tag = '%s'", path, tag));
      if (rs.next()) {
        rs.getString("options");
        if (!rs.wasNull()) {
          options = MultiLoaderOptions.from(rs.getString("options"));
        }
      }
      return Optional.ofNullable(options);
    } catch (SQLException ex) {
      throw new RuntimeException(ex);
    }
  }

  public List<String> listGameFilesToUnload(List<String> tagsToUnload, List<String> tagsToKeep) {
    List<String> pathsToUnload = listGameFiles(tagsToUnload);
    List<String> pathsToKeep = listGameFiles(tagsToKeep);
    pathsToUnload.removeAll(pathsToKeep);
    return pathsToUnload;
  }

  public void setPersistent(String tag) {
    try {
      database.execute(String.format("INSERT INTO persistentTags (tag) values ('%s')", tag));
    } catch (SQLException ex) {
      throw new RuntimeException(ex);
    }
  }

  public List<String> listPersistentTags() {
    try {
      List<String> tags = new ArrayList<>();
      ResultSet rs = database.execute("SELECT DISTINCT(tag) FROM persistentTags");
      while (rs.next()) {
        tags.add(rs.getString("tag"));
      }
      return tags;
    } catch (SQLException ex) {
      throw new RuntimeException(ex);
    }
  }

  public void debug_dumpTable(String table) {
    try {
      ResultSet rs = database.execute(String.format("SELECT * FROM %s", table));
      List<String> columns = debug_getColumns(rs);
      Log.info("%s table dump:", table);
      Log.info("%s", columns.stream().collect(Collectors.joining(", ")));
      while (rs.next()) {
        List<String> data = new ArrayList<>();
        for (int i = 0; i < columns.size(); i++) {
          data.add(rs.getString(columns.get(i)));
        }
        Log.info("%s", data.stream().collect(Collectors.joining(", ")));
      }
    } catch (SQLException ex) {
      throw new RuntimeException(ex);
    }
  }

  private List<String> debug_getColumns(ResultSet rs) throws SQLException {
    List<String> columns = new ArrayList<>();
    ResultSetMetaData metadata = rs.getMetaData();
    for (int i = 1; i <= metadata.getColumnCount(); i++) {
      columns.add(metadata.getColumnName(i));
    }
    return columns;
  }
}
