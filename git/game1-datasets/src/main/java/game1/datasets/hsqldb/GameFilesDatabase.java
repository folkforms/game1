package game1.datasets.hsqldb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.hsqldb.Server;
import org.hsqldb.persist.HsqlProperties;

import folkforms.log.Log;

public class GameFilesDatabase implements Database {

  private Server server;
  private Connection connection;
  private Statement statement;

  public GameFilesDatabase() throws Exception {
    HsqlProperties props = new HsqlProperties();
    props.setProperty("server.database.0", "mem:hsqldb/game1datasets");
    props.setProperty("server.dbname.0", "game1datasets");
    server = new Server();
    // server.setTrace(true);
    // server.setSilent(false);
    try {
      server.setProperties(props);
    } catch (Exception ex) {
      return;
    }
    server.start();
    openConnection();
    statement = connection.createStatement();
    insertTables();
  }

  // FIXME DATASETS: Username/password?
  // http://hsqldb.org/doc/2.0/guide/accesscontrol-chapt.html
  // CREATE USER NewUser PASSWORD 'randomPassword'
  // GRANT ALL ON ALL TABLES IN SCHEMA mySchema TO NewUser
  // DROP USER SA
  // Will need to check that I can modify database when game is running, and that doing the above
  // fixes things
  private void openConnection() throws ClassNotFoundException, SQLException {
    Class.forName("org.hsqldb.jdbc.JDBCDriver");
    connection = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/game1datasets", "SA",
        "");
    if (connection == null) {
      throw new RuntimeException("game1-datasets could not create in-memory database");
    }
  }

  private void insertTables() throws SQLException {
    statement.executeQuery(
        "CREATE TABLE gameFiles (index INT IDENTITY, path VARCHAR(250), tag VARCHAR(250), options VARCHAR(1000), loaded BOOLEAN)");
    statement.executeQuery("CREATE TABLE persistentTags (index INT IDENTITY, tag VARCHAR(250))");
  }

  private void close() {
    cleanup();
  }

  @Override
  public void cleanup() {
    try {
      connection.close();
      server.stop();
    } catch (SQLException ex) {
      Log.error("Failed to clean up %s: %s", getClass());
      Log.error(ex);
    }
  }

  @Override
  public ResultSet execute(String sql) throws SQLException {
    return statement.executeQuery(sql);
  }

  public static void main(String[] args) throws Exception {
    GameFilesDatabase db = new GameFilesDatabase();
    Statement stmt = db.connection.createStatement();
    stmt.executeQuery("INSERT INTO gameFiles (path, tag) values ('path/to/foo', 'level-1-tag')");
    stmt.executeQuery("INSERT INTO gameFiles (path, tag) values ('path/to/bar', 'level-1-tag')");
    stmt.executeQuery("INSERT INTO gameFiles (path, tag) values ('path/to/bar', 'level-2-tag')");
    stmt.executeQuery("INSERT INTO gameFiles (path, tag) values ('path/to/muk', 'level-2-tag')");
    stmt.executeQuery(
        "INSERT INTO gameFiles (path, tag, options) values ('resources/particle_effects/plane_1x1_vertical.obj', 'WidgetsTag', 'some-options\ntwo lines')");
    printSelect1(stmt);
    printSelect2(stmt);
    db.close();
  }

  private static void printSelect1(Statement stmt) throws SQLException {
    ResultSet rs = stmt.executeQuery("SELECT * FROM gameFiles");
    Log.info(String.format("index  path         tag          options"));
    while (rs.next()) {
      Log.info(String.format("%s      %s  %s  %s", rs.getInt("index"), rs.getString("path"),
          rs.getString("tag"), rs.getString("options")));
    }
    Log.info("----");
  }

  private static void printSelect2(Statement stmt) throws SQLException {
    ResultSet rs = stmt.executeQuery(
        String.format("SELECT options FROM gameFiles WHERE path = '%s' AND tag = '%s'",
            "resources/particle_effects/plane_1x1_vertical.obj", "WidgetsTag"));
    Log.info(String.format("options"));
    while (rs.next()) {
      Log.info(String.format("%s", rs.getString("options")));
    }
    Log.info("----");
  }
}
