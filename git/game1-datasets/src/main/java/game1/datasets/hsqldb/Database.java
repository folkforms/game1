package game1.datasets.hsqldb;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface Database {

  public ResultSet execute(String sql) throws SQLException;

  public void cleanup();
}
