package game1.datasets;

import java.util.Set;

class GameFile {

  private String filename;
  private boolean isLoaded = false;
  private Set<String> supplierNamesForUnloading = Set.of();
  private String tempTag;

  GameFile(String filename) {
    this.filename = filename;
  }

  public String getFilename() {
    return filename;
  }

  public boolean isLoaded() {
    return isLoaded;
  }

  public void setLoaded(boolean b) {
    isLoaded = b;
  }

  public Set<String> getSupplierNamesForUnloading() {
    return supplierNamesForUnloading;
  }

  public void setSupplierNamesForUnloading(Set<String> supplierNamesForUnloading) {
    this.supplierNamesForUnloading = supplierNamesForUnloading;
  }

  public String getTempTag() {
    return tempTag;
  }

  public void setTempTag(String tempTag) {
    this.tempTag = tempTag;
  }

  @Override
  public String toString() {
    return String.format("%s[filename='%s',isLoaded=%s,supplierNamesForUnloading=%s]",
        getClass().getSimpleName(), filename, isLoaded, supplierNamesForUnloading);
  }

  @Override
  public boolean equals(Object other) {
    if (other == null) {
      return false;
    }
    GameFile otherGameFile = (GameFile) other;
    return filename.equals(otherGameFile.getFilename());
  }

  @Override
  public int hashCode() {
    return filename.hashCode();
  }
}
