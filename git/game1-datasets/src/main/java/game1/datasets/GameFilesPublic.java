package game1.datasets;

import java.util.List;

public interface GameFilesPublic {

  public void addMultiLoader(String extension, Class<? extends MultiLoader> multiLoader);

  public void register(String filename, String tagName);

  public void register(List<String> filenames, String tagName);

  public void register(String filename, String tagName, MultiLoaderOptions options);

  public void register(List<String> filenames, String tagName, MultiLoaderOptions options);

  public void setInitialTags(List<String> blockingTags, List<String> nonBlockingTags);

  public void setPersistent(String tag);
}
