package game1.datasets;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

import folkforms.log.Log;
import game1.debug.DebugScopes;

public class GameData {

  protected Map<String, Supplier<?>> suppliers;
  protected Map<String, String> aliases = new HashMap<>();

  public GameData() {
    suppliers = new HashMap<>();
  }

  // FIXME DATASETS 2023-10-03: Is this actually used? If I'm making it package-protected then no?
  void addAlias(String alias, String path) {
    aliases.put(alias, path);
  }

  /**
   * This is used for simple suppliers of heavyweight items. For example: `() ->
   * Background.create(...)` is a mesh but it doesn't involve a file.
   */
  public void addSupplier(String name, Supplier<?> supplier) {
    addSuppliers(Map.of(name, supplier));
  }

  void addSuppliers(Map<String, Supplier<?>> newSuppliers) {
    Iterator<String> iterator = newSuppliers.keySet().iterator();
    while (iterator.hasNext()) {
      String key = iterator.next();
      Supplier<?> prev = suppliers.put(key, newSuppliers.get(key));
      if (prev != null) {
        throw new RuntimeException(String.format("Duplicate supplier added for key '%s'", key));
      }
    }
  }

  void removeSuppliers(Set<String> supplierNamesForUnloading) {
    Log.scoped(DebugScopes.GAME_CACHE, "GameData.removeSuppliers(%s)", supplierNamesForUnloading);
    supplierNamesForUnloading.forEach(suppliers::remove);
  }

  @SuppressWarnings("unchecked")
  public <T> T get(String name) {
    Supplier<?> supplier = suppliers.get(name);
    if (supplier == null) {
      String fromAlias = aliases.get(name);
      supplier = suppliers.get(fromAlias);
      if (supplier == null) {
        throw new RuntimeException(
            String.format("No supplier for '%s', suppliers = %s", name, suppliers.keySet()));
      }
    }
    return (T) supplier.get();
  }

  public void debug_printSuppliers() {
    Log.info("debug_printSuppliers = %s", suppliers.keySet());
  }
}
