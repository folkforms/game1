package game1.datasets;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import folkforms.log.Log;
import game1.datasets.hsqldb.GameFilesDao;
import game1.debug.DebugScopes;

public class GameFiles implements GameFilesPublic {

  private GameData gameData;
  private GameFilesDao gameFilesDao;
  private Map<String, Class<? extends MultiLoader>> loaderMap = new HashMap<>();
  private Map<String, GameFile> gameFiles = new HashMap<>();
  private List<String> loadTagQueue = new ArrayList<>();
  private List<String> initialBlockingTags = List.of();
  private List<String> initialNonBlockingTags = List.of();

  public GameFiles(GameData gameData, GameFilesDao gameFilesDao) {
    this.gameData = gameData;
    this.gameFilesDao = gameFilesDao;
  }

  @Override
  public void addMultiLoader(String extension, Class<? extends MultiLoader> multiLoader) {
    if (extension.startsWith(".")) {
      extension = extension.substring(1);
    }
    loaderMap.put(extension, multiLoader);
  }

  @Override
  public void register(String filename, String tagName) {
    gameFilesDao.addGameFile(filename, tagName);
    gameFiles.put(filename, new GameFile(filename));
  }

  @Override
  public void register(List<String> filenames, String tagName) {
    filenames.forEach(filename -> register(filename, tagName));
  }

  @Override
  public void register(String filename, String tagName, MultiLoaderOptions options) {
    gameFilesDao.addGameFile(filename, tagName, options);
    gameFiles.put(filename, new GameFile(filename));
    options.getAlias().ifPresent(alias -> gameData.addAlias(alias, filename));
  }

  @Override
  public void register(List<String> filenames, String tagName, MultiLoaderOptions options) {
    filenames.forEach(filename -> register(filename, tagName, options));
  }

  public void internal_enqueueAll(List<String> tagsToLoad) {
    loadTagQueue.addAll(tagsToLoad);
  }

  public void internal_processLoadingQueue() throws IOException {
    if (loadTagQueue.isEmpty()) {
      return;
    }
    String topEntry = loadTagQueue.get(0);
    Optional<GameFile> nextUnloadedGameFileForTagMaybe = getNextUnloadedGameFileForTag(topEntry);
    if (nextUnloadedGameFileForTagMaybe.isPresent()) {
      blockingLoadFile(nextUnloadedGameFileForTagMaybe.get());
    } else {
      loadTagQueue.remove(0);
    }
  }

  private Optional<GameFile> getNextUnloadedGameFileForTag(String tag) {
    List<String> unloadedGameFilesForTag = gameFilesDao.listGameFiles(tag).stream()
        .filter(path -> !gameFiles.get(path).isLoaded()).toList();
    if (unloadedGameFilesForTag.isEmpty()) {
      return Optional.empty();
    }
    GameFile gameFile = gameFiles.get(unloadedGameFilesForTag.get(0));
    gameFile.setTempTag(tag);
    return Optional.of(gameFile);
  }

  public void internal_blockingLoadTag(String tag) throws IOException {
    Optional<GameFile> nextUnloadedGameFileMaybe;
    while ((nextUnloadedGameFileMaybe = getNextUnloadedGameFileForTag(tag)).isPresent()) {
      blockingLoadFile(nextUnloadedGameFileMaybe.get());
    }
  }

  /**
   * Perform an immediate blocking load of a file. Used when initialising the engine as we need
   * certain yaml files immediately.
   */
  public void internal_blockingLoadFile(String filename) throws IOException {
    GameFile gameFile = gameFiles.get(filename);
    blockingLoadFile(gameFile);
  }

  private void blockingLoadFile(GameFile gameFile) throws IOException {
    if (gameFile.isLoaded()) {
      return;
    }

    Log.scoped(DebugScopes.GAME_CACHE, "Loading game file %s", gameFile);

    Optional<MultiLoaderOptions> optionsMaybe = gameFilesDao.getOptions(gameFile.getFilename(),
        gameFile.getTempTag());

    try {
      String filename = gameFile.getFilename();
      String extension = filename.substring(filename.lastIndexOf('.') + 1);
      Class<? extends MultiLoader> loader = loaderMap.get(extension);
      if (loader == null) {
        throw new RuntimeException(String.format(
            "No loader for extension '%s' is registered. Use Engine.GAME_DATA_FILE_REGISTRY.addMultiLoader(extension, class).",
            extension));
      }
      MultiLoader multiLoader = loader.getDeclaredConstructor().newInstance();
      optionsMaybe.ifPresent(multiLoader::setOptions);
      Map<String, Supplier<?>> suppliers = multiLoader.load(filename);
      gameData.addSuppliers(suppliers);
      gameFile.setSupplierNamesForUnloading(suppliers.keySet());
      gameFile.setLoaded(true);
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
      throw new IOException(ex);
    }
  }

  @Override
  public void setInitialTags(List<String> blockingTags, List<String> nonBlockingTags) {
    initialBlockingTags = blockingTags;
    initialNonBlockingTags = nonBlockingTags;
  }

  public void internal_init() throws IOException {
    for (int i = 0; i < initialBlockingTags.size(); i++) {
      internal_blockingLoadTag(initialBlockingTags.get(i));
    }
    loadTagQueue.addAll(initialNonBlockingTags);
  }

  public void internal_unloadAllExcept(List<String> tagsToKeep) {
    Log.scoped(DebugScopes.GAME_CACHE, "GameFiles.unloadAllExcept(%s)", tagsToKeep);
    List<String> allTags = gameFilesDao.listAllTags();
    List<String> tagsToUnload = allTags;
    tagsToUnload.removeAll(tagsToKeep);
    List<String> persistentTags = gameFilesDao.listPersistentTags();
    tagsToUnload.removeAll(persistentTags);
    Log.scoped(DebugScopes.GAME_CACHE, "Unloading tags %s", tagsToUnload);
    if (tagsToUnload.isEmpty()) {
      return;
    }
    List<String> pathsToUnload = gameFilesDao.listGameFilesToUnload(tagsToUnload, tagsToKeep);
    Log.scoped(DebugScopes.GAME_CACHE, "Unloading files %s", pathsToUnload);
    List<GameFile> gameFilesToUnload = pathsToUnload.stream().map(path -> gameFiles.get(path))
        .toList();
    gameFilesToUnload.forEach(gameFile -> {
      gameData.removeSuppliers(gameFile.getSupplierNamesForUnloading());
      gameFile.setLoaded(false);
    });
  }

  public boolean internal_allTagsLoaded(List<String> tags) {
    List<String> paths = gameFilesDao.listGameFiles(tags);
    return paths.stream().map(path -> gameFiles.get(path)).allMatch(GameFile::isLoaded);
  }

  @Override
  public void setPersistent(String tag) {
    gameFilesDao.setPersistent(tag);
  }

  // FIXME DATASETS: This data should find its way to the loading screen via events not direct calls
  public int internal_getNumGameFiles(List<String> tags) {
    int size = gameFilesDao.listGameFiles(tags).size();
    return size;
  }

  public int internal_getNumLoadedGameFiles(List<String> tags) {
    List<String> listGameFiles = gameFilesDao.listGameFiles(tags);
    int size = listGameFiles.stream().filter(g -> gameFiles.get(g).isLoaded()).toList().size();
    return size;
  }
}
