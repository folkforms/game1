package game1.datasets;

import java.io.IOException;
import java.util.Map;
import java.util.function.Supplier;

import game1.filesystem.FileSystem;
import game1.filesystem.FileSystemFactory;

public abstract class MultiLoader {

  protected String path;
  protected MultiLoaderOptions options;
  protected FileSystem fileSystem;

  public MultiLoader() {
    this.fileSystem = FileSystemFactory.getInstance();
  }

  public void setOptions(MultiLoaderOptions options) {
    this.options = options;
  }

  public abstract Map<String, Supplier<?>> load(String path) throws IOException;
}
