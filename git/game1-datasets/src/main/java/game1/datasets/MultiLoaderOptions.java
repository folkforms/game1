package game1.datasets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class MultiLoaderOptions {

  private Map<String, Object> options;
  private static final Map<String, Class<?>> TYPES = Map.of("scale", Float.class);

  public MultiLoaderOptions() {
    this.options = new LinkedHashMap<>();
  }

  private MultiLoaderOptions(Map<String, Object> options) {
    this.options = options;
  }

  @SuppressWarnings("unchecked")
  public <T> T get(String key) {
    return (T) options.get(key);
  }

  @SuppressWarnings("unchecked")
  public <T> T getOrDefault(String key, T defaultValue) {
    Object value = options.get(key);
    return value != null ? (T) value : defaultValue;
  }

  public void set(String key, Object value) {
    options.put(key, value);
  }

  public Optional<String> getAlias() {
    return Optional.ofNullable(get("alias"));
  }

  public String toSerialisableString() {
    List<String> output = new ArrayList<>();
    options.forEach((k, v) -> output.add(String.format("%s=%s", k, v)));
    String str = output.stream().collect(Collectors.joining("\n"));
    if (str.length() > 1000) {
      throw new RuntimeException(String.format(
          "MultiLoaderOptions was too long to fit in database (%s characters, limit is 1000.) Options = %s",
          str.length(), options));
    }
    return str;
  }

  public static MultiLoaderOptions from(String input) {
    return from(input, "\\n");
  }

  public static MultiLoaderOptions from(String input, String delimiter) {
    List<String> input2 = Arrays.stream(input.split(delimiter)).toList();
    Map<String, Object> options = new HashMap<>();
    input2.forEach(line -> {
      String[] tokens = line.split("=");
      Object cast = cast(tokens[0], tokens[1]);
      options.put(tokens[0], cast);
    });
    return new MultiLoaderOptions(options);
  }

  private static Object cast(String key, String value) {
    Class<?> clazz = TYPES.get(key);
    if (clazz == null) {
      return value;
    }
    if (clazz == Float.class) {
      return Float.valueOf(value);
    }
    throw new RuntimeException(
        String.format("MultiLoaderOptions has no type mapped for key/value: %s, %s", key, value));
  }
}
