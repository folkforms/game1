package game1.datasets;

public class GameDataFactory {

  private static GameData gameData = null;

  public static GameData getInstance() {
    if (gameData == null) {
      gameData = new GameData();
    }
    return gameData;
  }

  public static void resetForTesting() {
    gameData = new GameData();
  }
}
