package game1.datasets.hsqldb;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.SQLException;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import game1.datasets.mocks.MockGameFilesDatabase;

class GameFilesDaoTest {

  private MockGameFilesDatabase mockGameFilesDatabase;

  private GameFilesDao gameFilesDao;

  @BeforeEach
  public void beforeEach() throws Exception {
    mockGameFilesDatabase = new MockGameFilesDatabase();
    gameFilesDao = new GameFilesDao(mockGameFilesDatabase);
  }

  @Test
  void test() throws SQLException {
    gameFilesDao.addGameFile("path/to/file", "some-tag");

    assertEquals(List.of("INSERT INTO gameFiles (path, tag) values ('path/to/file', 'some-tag')"),
        mockGameFilesDatabase.test_getExecuteCalls());
  }
}
