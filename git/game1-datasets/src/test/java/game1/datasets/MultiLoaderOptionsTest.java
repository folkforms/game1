package game1.datasets;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class MultiLoaderOptionsTest {

  @Test
  void itSerialisesOptions() {
    MultiLoaderOptions options = new MultiLoaderOptions();
    options.set("foo", 1);
    options.set("bar", "bar");
    assertEquals("foo=1\nbar=bar", options.toSerialisableString());
  }

  @Test
  void itDeserialisesOptions() {
    String string = "scale=1\nbar=bar";
    MultiLoaderOptions options = MultiLoaderOptions.from(string);
    compare(1f, options, "scale", Float.class);
  }

  @Test
  void itDeserialisesOptionsWithDelimiter() {
    String string = "scale=1, bar=bar";
    MultiLoaderOptions options = MultiLoaderOptions.from(string, ", ");
    compare(1f, options, "scale", Float.class);
  }

  private void compare(Object expected, MultiLoaderOptions options, String key, Class<?> clazz) {
    Object object = options.get(key);
    assertEquals(clazz, object.getClass());
    assertEquals(expected, clazz.cast(object));
  }
}
