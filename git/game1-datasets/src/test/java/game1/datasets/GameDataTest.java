package game1.datasets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Set;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import game1.debug.DebugScopes;

class GameDataTest {

  private GameData gameData;

  @BeforeAll
  public static void beforeAll() {
    DebugScopes.add(DebugScopes.GAME_CACHE);
  }

  @BeforeEach
  public void beforeEach() throws Exception {
    gameData = new GameData();
  }

  @Test
  void itGetsAnItem() {
    String foo = "foo";
    gameData.addSupplier("ref", () -> foo);
    String actual = gameData.get("ref");
    assertEquals(foo, actual);
  }

  @Test
  void itGetsAnItemViaAlias() {
    String foo = "foo";
    gameData.addSupplier("ref", () -> foo);
    gameData.addAlias("ref2", "ref");
    String actual = gameData.get("ref2");
    assertEquals(foo, actual);
  }

  @Test
  void itAddsSuppliers() {
    String foo = "foo";
    gameData.addSupplier("ref", () -> foo);
    String actual = gameData.get("ref");
    assertEquals(foo, actual);
  }

  @Test
  void itRemovesSuppliers() {
    String foo = "foo";
    gameData.addSupplier("ref", () -> foo);
    gameData.removeSuppliers(Set.of("ref"));
    assertThrows(RuntimeException.class, () -> gameData.get("ref"));
  }
}
