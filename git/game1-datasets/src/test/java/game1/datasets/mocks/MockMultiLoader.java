package game1.datasets.mocks;

import java.io.IOException;
import java.util.Map;
import java.util.function.Supplier;

import game1.datasets.MultiLoader;

public class MockMultiLoader extends MultiLoader {

  @Override
  public Map<String, Supplier<?>> load(String path) throws IOException {
    return Map.of( //
        "foo", () -> "foo1", //
        "bar", () -> "bar1", //
        "muk", () -> "muk1" //
    );
  }
}
