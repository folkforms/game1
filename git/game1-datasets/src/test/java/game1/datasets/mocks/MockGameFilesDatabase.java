package game1.datasets.mocks;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import game1.datasets.hsqldb.Database;

public class MockGameFilesDatabase implements Database {

  private List<String> executeCalls = new ArrayList<>();

  @Override
  public ResultSet execute(String sql) {
    executeCalls.add(sql);
    return null;
  }

  @Override
  public void cleanup() {
  }

  public List<String> test_getExecuteCalls() {
    return executeCalls;
  }
}
