package game1.datasets;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import game1.datasets.hsqldb.GameFilesDao;
import game1.datasets.hsqldb.GameFilesDatabase;
import game1.datasets.mocks.MockMultiLoader;
import game1.debug.DebugScopes;

class GameFilesTest {

  private GameFilesDatabase gameFilesDatabase;
  private GameFilesDao gameFilesDao;
  private GameData gameData;
  private GameFiles gameFiles;

  @BeforeEach
  public void beforeEach() throws Exception {
    gameFilesDatabase = new GameFilesDatabase();
    gameFilesDao = new GameFilesDao(gameFilesDatabase);
    gameData = new GameData();
    gameFiles = new GameFiles(gameData, gameFilesDao);
    gameFiles.addMultiLoader(".foo", MockMultiLoader.class);
  }

  @Test
  void itRegistersAndLoadsAFile() throws IOException {
    DebugScopes.add(DebugScopes.GAME_CACHE);
    gameFiles.register("path/to/file.foo", "some-tag");
    gameFiles.internal_blockingLoadTag("some-tag");
    String foo = gameData.get("foo");
    String bar = gameData.get("bar");
    assertEquals("foo1", foo);
    assertEquals("bar1", bar);
  }
}
