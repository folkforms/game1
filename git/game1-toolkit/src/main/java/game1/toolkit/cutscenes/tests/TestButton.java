package game1.toolkit.cutscenes.tests;

import game1.events.Command;
import game1.toolkit.cutscenes.internal.Cutscene;
import game1.toolkit.widgets.button.Button;

/**
 * @deprecated Old Cutscenes v5 code. Use C9 instead.
 */
@Deprecated
public class TestButton extends Button {
  public TestButton(String text, int code, int x, int y, int z) {
    super(text, 5, x, y, z, 100, 50);
    setCommand(new Command() {
      @Override
      public void execute() {
        Cutscene.setUserInput(code);
      }
    });
  }
}
