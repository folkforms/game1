package game1.toolkit.cutscenes.tests;

import game1.core.input.KeyBindings;
import game1.core.input.KeyCommand;
import game1.core.input.Keys;
import game1.toolkit.internal.levels.cutscenes.SetUserInputCommand;

/**
 * @deprecated Old Cutscenes v5 code. Use C9 instead.
 */
@Deprecated
public class Keys123 extends KeyBindings {

  public Keys123() {
    register(KeyCommand.init().keys(Keys.KEY_1).command(new SetUserInputCommand(1)));
    register(KeyCommand.init().keys(Keys.KEY_2).command(new SetUserInputCommand(2)));
    register(KeyCommand.init().keys(Keys.KEY_3).command(new SetUserInputCommand(3)));
  }
}
