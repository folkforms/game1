package game1.toolkit.cutscenes.internal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joml.Vector4f;

import folkforms.log.Log;
import folkforms.stringutils.StringUtils;
import game1.actors.Actor;
import game1.actors.Moveable;
import game1.actors.Tickable;
import game1.core.engine.Engine;
import game1.core.engine.internal.Camera;
import game1.core.engine.internal.CameraFactory;
import game1.core.graphics.Animation;
import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.pipelines.Stencil2d;
import game1.datasets.GameDataFactory;
import game1.debug.DebugScopes;
import game1.events.Command;
import game1.resolutions.layers.ResolutionLayers;
import game1.toolkit.cutscenes.CutsceneCommand;
import game1.toolkit.cutscenes.utils.ChangeAnimationByRef;
import game1.toolkit.cutscenes.utils.CutsceneMoveable;
import game1.toolkit.cutscenes.utils.CutsceneRotatable;

/**
 * The Cutscene class handles automatic display and movement of actors in a pre-rendered sequence of
 * events. Cutscenes start playing as soon as they are added to state.
 *
 * @deprecated Old Cutscenes v5 code. Use C9 instead.
 */
@Deprecated
public class Cutscene implements Actor, Tickable {

  private boolean DEBUG_PRINT_LIST = Engine.PROPERTIES
      .getBooleanProperty("cutscene.print_events_at_start");
  private boolean DEBUG_EVENTS = Engine.PROPERTIES
      .getBooleanProperty("cutscene.print_events_on_execution");

  /**
   * If this value is set when a cutscene is started it will insert "jump, &lt;value&gt;" as the
   * first command. This allows you to put debug points inside the cutscene and jump to them using
   * buttons.
   */
  public static String DEBUG_JUMP_LABEL = Engine.PROPERTIES
      .getProperty("cutscene.debug_jump_label");

  /**
   * Debug mode flag. Various events work differently in debug mode:
   * <li>Commands prefixed with '?' will be executed</li>
   * <li>'wait' will be ignored</li>
   * <li>'waitForAnyKey' will be ignored</li>
   * <li>'waitForKey' will be triggered???</li>
   * <li>'waitForUserInput' will take user input from a predetermined list</li>
   */
  public static boolean DEBUG = false;
  public static List<Integer> DEBUG_USER_INPUT = new ArrayList<>();
  public static boolean PAUSE_TICKING = false;

  private List<String> eventQueue = new ArrayList<>();
  private int index = 0;
  private boolean playing = false;
  private static boolean waiting = false;
  private long waitTime = 0;
  private long waitTimeout = 0;
  private static int userInput = Integer.MIN_VALUE;
  private static int[] allowedInput = null;
  private Map<String, CutsceneCommand> userEventRegistry = new HashMap<>();

  private static FastForwarder fastForwarder;
  private static boolean fastForwarding = false;
  public static int FAST_FORWARD_SPEED = Engine.PROPERTIES
      .getIntProperty("cutscene.fast_forward_speed");
  private static boolean isSkipping = false;

  private List<String> gameCacheRefs = new ArrayList<>();
  private String nextState;

  private CutsceneDataLoader cutsceneDataLoaderForCloning;
  private boolean isClone = false;
  private String debugFilename;

  private Map<String, String> properties = new HashMap<>();

  /**
   * Creates a new {@link Cutscene} from the given cutscene data.
   *
   * @param cutsceneDataLoader
   *          cutscene data
   */
  public Cutscene(CutsceneDataLoader cutsceneDataLoader) {
    this.cutsceneDataLoaderForCloning = cutsceneDataLoader;
    this.debugFilename = cutsceneDataLoader.getFilename();
  }

  // @Override
  // public void postAdd() {
  // ensureClone();
  // isSkipping = false;
  // fastForwarding = false;
  // play();
  // }

  /**
   * Prepares the cutscene objects for playing.
   */
  private void ensureClone() {
    if (isClone) {
      CutsceneParser cutsceneParser = new CutsceneParser(cutsceneDataLoaderForCloning);
      gameCacheRefs.addAll(cutsceneParser.getGameCacheRefs());
      eventQueue.addAll(cutsceneParser.getOutput());
    } else {
      throw new RuntimeException(
          "Only clones of Cutscenes can be played. Use cutscene.getClone().");
    }
  }

  /**
   * Registers a user event with the given name that will execute the given {@link CutsceneCommand}.
   *
   * @param userEventName
   *          the name of the user event
   * @param command
   *          the cutscene command that will be executed when this user event is actioned
   */
  public void registerUserEvent(String userEventName, CutsceneCommand command) {
    userEventRegistry.put(userEventName, command);
  }

  private static int getUserInput() {
    return userInput;
  }

  /**
   * Sets the user input to the given value and unpauses the cutscene. Does nothing if you send the
   * same value twice in a row.
   *
   * @param newUserInput
   *          new user input value
   */
  public static void setUserInput(int newUserInput) {
    // Check that the new input is part of the list of allowed inputs
    if (allowedInput != null) {
      boolean found = false;
      for (int i : allowedInput) {
        if (i == newUserInput) {
          found = true;
          break;
        }
      }
      if (!found) {
        return;
      }
    }

    if (userInput != newUserInput) {
      userInput = newUserInput;
      allowedInput = null;
      waiting = false;
    }
  }

  /**
   * Sets the value of <code>waiting</code>.
   *
   * @param b
   *          new value
   */
  public static void setWaiting(boolean b) {
    waiting = b;
  }

  /**
   * Starts playing the cutscene.
   */
  private void play() {
    if (!playing) {
      if (DEBUG_PRINT_LIST) {
        Log.info("PRINT");
        for (String s : eventQueue) {
          Log.info("%s", s);
        }
        Log.info("----");
      }
      if (DEBUG_EVENTS) {
        Log.info("START");
      }
      if (DEBUG_JUMP_LABEL != null && DEBUG_JUMP_LABEL.length() != 0) {
        eventQueue.add(0, String.format("jump, %s", DEBUG_JUMP_LABEL));
        DEBUG_JUMP_LABEL = null;
      }

      userInput = Integer.MIN_VALUE;
      playing = true;
      fastForwarder = new FastForwarder();
      Engine.GAME_STATE.addActor(fastForwarder, ResolutionLayers.WINDOW_LAYER);
    }
  }

  public static void setFastForwarding(boolean b) {
    fastForwarding = b;
    if (fastForwarding) {
      fastForwarder.update();
    }
  }

  public static boolean isFastForwarding() {
    return fastForwarding || isSkipping;
  }

  public static void setSkipping(boolean b) {
    isSkipping = b;
  }

  public static boolean isSkipping() {
    return isSkipping;
  }

  @Override
  public void onTick() {
    if (PAUSE_TICKING) {
      return;
    }

    if (waiting) {
      waitTime++;
      if (fastForwarding || isSkipping) {
        waitTime += FAST_FORWARD_SPEED;
      }
    }
    if (waiting && waitTimeout != -1 && waitTime > waitTimeout) {
      waiting = false;
    }

    if (!playing || waiting) {
      return;
    }

    while (playing && !waiting) {
      if (index >= eventQueue.size()) {
        break;
      }
      actionNextEvent();
    }

    if (playing && !waiting && index >= eventQueue.size()) {
      if (DEBUG_EVENTS) {
        Log.info("END");
        Log.info("----");
      }
      end();
    }
  }

  private void end() {
    setVisible(false);

    if (nextState != null) {

      // Remove any actors that the parser added to Engine.GAME_CACHE
      for (String ref : gameCacheRefs) {
        // Engine.ACTOR_CACHE.remove(ref);
        Log.temp("Cutscene: Remove any actors that the parser added to Engine.GAME_CACHE");
      }

      Engine.GAME_STATE.moveToState(nextState);
    }
  }

  /**
   * Gets the state that follows this cutscene.
   *
   * @return the state that follows this cutscene
   */
  public void setNextState(String nextState) {
    this.nextState = nextState;
  }

  private void actionNextEvent() {
    String s = eventQueue.get(index);
    if (!DEBUG && s.startsWith("?")) {
      index++;
      return;
    } else if (s.startsWith("?")) {
      s = s.substring(1).trim();
    } else if (s.trim().startsWith("#")) {
      index++;
      return;
    }

    try {
      if (DEBUG_EVENTS) {
        Log.info("event: %s", s);
      }
      String[] tokens = StringUtils.splitCSV(s, ", ");
      tokens = substituteVars(tokens);
      switch (tokens[0]) {
      case "add":
        actionAddEvent(tokens);
        break;
      case "remove":
        actionRemoveEvent(tokens);
        break;
      case "setPosition":
        actionSetPositionEvent(tokens);
        break;
      case "setRotation":
        actionSetRotationEvent(tokens);
        break;
      case "setPositionRotation":
        actionSetPositionRotationEvent(tokens);
        break;
      case "move":
        actionMoveEvent(tokens);
        break;
      case "rotate":
        actionRotateEvent(tokens);
        break;
      case "moveRotate":
        actionMoveRotateEvent(tokens);
        break;
      case "setAnimation":
        actionSetAnimationEvent(tokens);
        break;
      case "startAnimation":
        actionStartAnimationEvent(tokens);
        break;
      case "stopAnimation":
        actionStopAnimationEvent(tokens);
        break;
      case "resetAnimation":
        actionResetAnimationEvent(tokens);
        break;
      case "setStencil":
        actionSetStencilEvent(tokens);
        break;
      case "clearStencil":
        actionClearStencilEvent(tokens);
        break;
      case "wait":
        actionWaitEvent(tokens);
        break;
      case "waitForKey":
        actionWaitForKeyEvent(tokens);
        break;
      case "waitForAnyKey":
        actionWaitForAnyKeyEvent(tokens);
        break;
      case "waitForUserInput":
        actionWaitForUserInputEvent(tokens);
        break;
      case "pushDebugUserInput":
        actionPushDebugUserInput(tokens);
        break;
      case "label":
        // Do nothing
        break;
      case "jump":
        actionJumpEvent(tokens);
        break;
      case "jumpIfEqual":
        actionJumpIfEqualEvent(tokens);
        break;
      case "activateKeyBindings":
        actionActivateKeyBindingsEvent(tokens);
        break;
      case "deactivateKeyBindings":
        actionDeactivateKeyBindingsEvent(tokens);
        break;
      case "sound":
        actionSoundEvent(tokens);
        break;
      case "loopSound":
        actionLoopSoundEvent(tokens);
        break;
      case "stopSound":
        actionStopSoundEvent(tokens);
        break;
      case "setCamera":
        actionSetCameraEvent(tokens);
        break;
      case "user":
        actionUserEvent(tokens);
        break;
      case "error":
        actionErrorEvent(tokens);
        break;
      case "log":
        actionLogEvent(tokens);
        break;
      case "skipPoint":
        actionSkipPoint(tokens);
        break;
      default:
        Log.error("Unknown event type: '%s' in '%s'", tokens[0], s);
        break;
      }
      index++;
    } catch (RuntimeException ex) {
      throw new RuntimeException(String.format(
          "Error actioning cutscene command '%s'. Cutscene file: %s", s, debugFilename), ex);
    }
  }

  public void setProperty(String key, String value) {
    properties.put(key, value);
  }

  /**
   * Replaces vars like {@code @var("foo")} with a value pulled from a cutscene property.
   */
  private String[] substituteVars(String[] tokens) {
    for (int i = 0; i < tokens.length; i++) {
      while (tokens[i].contains("@var(\"")) {
        String start = tokens[i].substring(0, tokens[i].indexOf("@var(\""));
        String key = tokens[i].substring(tokens[i].indexOf("@var(") + 6, tokens[i].indexOf("\")"));
        String end = tokens[i].substring(tokens[i].indexOf("\")") + 2);
        String value = properties.get(key);
        tokens[i] = start + value + end;
      }
    }
    return tokens;
  }

  private void actionAddEvent(String[] tokens) {
    for (int i = 1; i < tokens.length; i++) {
      String reference = tokens[i];
      if (reference.contains("#")) {
        reference = reference.substring(0, reference.indexOf("#")).trim();
      }
      Actor actor = GameDataFactory.getInstance().get(reference);
      try {
        Engine.GAME_STATE.addActor(actor, ResolutionLayers.WINDOW_LAYER);
      } catch (NullPointerException ex) {
        throw new RuntimeException(String.format(
            "Reference '%s' was null. Did you forget to register an actor with that name in a Dataset?",
            reference), ex);
      }
    }
  }

  private void actionRemoveEvent(String[] tokens) {
    for (int i = 1; i < tokens.length; i++) {
      // FIXME DATASETS: This needs the exact item
      Actor actor = GameDataFactory.getInstance().get(tokens[i]);
      try {
        actor.setVisible(false);
      } catch (NullPointerException ex) {
        throw new RuntimeException(
            String.format("Reference '%s' was null when removing from state.", tokens[i]), ex);
      }
    }
  }

  private void actionSetPositionEvent(String[] tokens) {
    Actor actor = GameDataFactory.getInstance().get(tokens[1]);
    Moveable moveable = (Moveable) actor;
    int newPX = Integer.parseInt(tokens[2]);
    int newPY = Integer.parseInt(tokens[3]);
    if (tokens.length == 4) {
      moveable.setPosition(newPX, newPY);
    } else {
      int newPZ = Integer.parseInt(tokens[4]);
      moveable.setPosition(newPX, newPY, newPZ);
    }
  }

  private void actionSetRotationEvent(String[] tokens) {
    Actor actor = GameDataFactory.getInstance().get(tokens[1]);
    CutsceneRotatable c = (CutsceneRotatable) actor;
    float newRX = Float.parseFloat(tokens[2]);
    float newRY = Float.parseFloat(tokens[3]);
    float newRZ = Float.parseFloat(tokens[4]);
    c.setRotation(newRX, newRY, newRZ);
  }

  private void actionSetPositionRotationEvent(String[] tokens) {
    Actor actor = GameDataFactory.getInstance().get(tokens[1]);
    Moveable cm = (Moveable) actor;
    int newPX = Integer.parseInt(tokens[2]);
    int newPY = Integer.parseInt(tokens[3]);
    int newPZ = Integer.parseInt(tokens[4]);
    cm.setPosition(newPX, newPY, newPZ);
    CutsceneRotatable cr = (CutsceneRotatable) actor;
    float newRX = Float.parseFloat(tokens[5]);
    float newRY = Float.parseFloat(tokens[6]);
    float newRZ = Float.parseFloat(tokens[7]);
    cr.setRotation(newRX, newRY, newRZ);
  }

  private void actionMoveEvent(String[] tokens) {
    Actor actor = GameDataFactory.getInstance().get(tokens[2]);
    CutsceneMoveable c = (CutsceneMoveable) actor;
    long duration = Long.parseLong(tokens[1]);
    float newX = Float.parseFloat(tokens[3]);
    float newY = Float.parseFloat(tokens[4]);
    if (tokens.length == 5) {
      c.startMoving(duration, newX, newY);
    } else {
      float newZ = Float.parseFloat(tokens[5]);
      c.startMoving(duration, newX, newY, newZ);
    }
  }

  private void actionRotateEvent(String[] tokens) {
    Actor actor = GameDataFactory.getInstance().get(tokens[2]);
    CutsceneRotatable c = (CutsceneRotatable) actor;
    long duration = Long.parseLong(tokens[1]);
    float newX = Float.parseFloat(tokens[3]);
    float newY = Float.parseFloat(tokens[4]);
    float newZ = Float.parseFloat(tokens[5]);
    c.startRotating(duration, newX, newY, newZ);
  }

  private void actionMoveRotateEvent(String[] tokens) {
    Actor actor = GameDataFactory.getInstance().get(tokens[2]);
    CutsceneMoveable cm = (CutsceneMoveable) actor;
    long duration = Long.parseLong(tokens[1]);
    float newPX = Float.parseFloat(tokens[3]);
    float newPY = Float.parseFloat(tokens[4]);
    float newPZ = Float.parseFloat(tokens[5]);
    cm.startMoving(duration, newPX, newPY, newPZ);
    CutsceneRotatable cr = (CutsceneRotatable) actor;
    float newRX = Float.parseFloat(tokens[6]);
    float newRY = Float.parseFloat(tokens[7]);
    float newRZ = Float.parseFloat(tokens[8]);
    cr.startRotating(duration, newRX, newRY, newRZ);
  }

  private void actionSetAnimationEvent(String[] tokens) {
    ChangeAnimationByRef c = GameDataFactory.getInstance().get(tokens[1]);
    c.setAnimation(tokens[2]);
  }

  private void actionStartAnimationEvent(String[] tokens) {
    Animation animation = GameDataFactory.getInstance().get(tokens[1]);
    animation.start();
  }

  private void actionStopAnimationEvent(String[] tokens) {
    Animation animation = GameDataFactory.getInstance().get(tokens[1]);
    animation.stop();

  }

  private void actionResetAnimationEvent(String[] tokens) {
    Animation animation = GameDataFactory.getInstance().get(tokens[1]);
    animation.reset();
  }

  private void actionSetStencilEvent(String[] tokens) {
    Drawable drawable = GameDataFactory.getInstance().get(tokens[1]);
    float x = Float.parseFloat(tokens[2]);
    float y = Float.parseFloat(tokens[3]);
    float w = Float.parseFloat(tokens[4]);
    float h = Float.parseFloat(tokens[5]);
    Vector4f area = new Vector4f(x, y, w, h);
    int type = tokens.length == 7 ? Integer.parseInt(tokens[6]) : Stencil2d.OUTSIDE;
    Colour colour = Colour.fromHex(tokens.length == 8 ? tokens[7] : "000");
    drawable.setStencil(new Stencil2d(type, area, colour));
  }

  private void actionClearStencilEvent(String[] tokens) {
    Drawable drawable = GameDataFactory.getInstance().get(tokens[1]);
    drawable.setStencil(null);
  }

  private void actionWaitEvent(String[] tokens) {
    waiting = true;
    if (tokens.length == 2) {
      waitTime = 0;
      waitTimeout = Long.parseLong(tokens[1]);
    } else {
      waitTimeout = -1;
    }
  }

  private void actionWaitForKeyEvent(String[] tokens) {
    userInput = Integer.MIN_VALUE;
    actionWaitEvent(new String[0]);
    int[] keyCodes = new int[tokens.length - 1];
    for (int i = 1; i < tokens.length; i++) {
      keyCodes[i - 1] = Integer.parseInt(tokens[i]);
    }
    Engine.GAME_STATE.addActor(new WaitForKey(keyCodes), ResolutionLayers.WINDOW_LAYER);
  }

  private void actionWaitForAnyKeyEvent(String[] tokens) {
    actionWaitEvent(tokens);
    Engine.GAME_STATE.addActor(new WaitForAnyKey(), ResolutionLayers.WINDOW_LAYER);
  }

  private void actionWaitForUserInputEvent(String[] tokens) {
    Log.scoped(DebugScopes.CUTSCENES, "actionWaitForUserInputEvent");
    Log.scoped(DebugScopes.CUTSCENES, "DEBUG = %s", DEBUG);
    Log.scoped(DebugScopes.CUTSCENES, "DEBUG_USER_INPUT = %s", DEBUG_USER_INPUT);
    if (DEBUG && DEBUG_USER_INPUT.size() > 0) {
      userInput = DEBUG_USER_INPUT.remove(0);
    } else {
      userInput = Integer.MIN_VALUE;
      allowedInput = new int[tokens.length - 1];
      for (int i = 1; i < tokens.length; i++) {
        allowedInput[i - 1] = Integer.parseInt(tokens[i]);
      }
      actionWaitEvent(new String[0]);
    }
  }

  private void actionPushDebugUserInput(String[] tokens) {
    for (int i = 1; i < tokens.length; i++) {
      DEBUG_USER_INPUT.add(Integer.parseInt(tokens[i]));
    }
    Log.scoped(DebugScopes.CUTSCENES, "Pushed '%s' to DEBUG_USER_INPUT", Arrays.toString(tokens));
  }

  private void actionJumpEvent(String[] tokens) {
    String label = tokens[1];
    jumpToLabel(label);
  }

  private void actionJumpIfEqualEvent(String[] tokens) {
    int expectedInput = Integer.parseInt(tokens[1]);
    String target = tokens[2];
    if (Cutscene.getUserInput() == expectedInput) {
      jumpToLabel(target);
    }
  }

  private void jumpToLabel(String targetLabel) {
    for (int i = 0; i < eventQueue.size(); i++) {
      String peekLabel = peekEventQueueLabel(i);
      if (targetLabel.equals(peekLabel)) {
        index = i - 1;
        return;
      } else {
        continue;
      }
    }
    throw new RuntimeException(String.format("Label '%s' not found", targetLabel));
  }

  private String peekEventQueueLabel(int i) {
    if (i >= eventQueue.size()) {
      return null;
    }

    String s = eventQueue.get(i);
    if (s.startsWith("?")) {
      s = s.substring(1).trim();
    }
    String[] tokens = s.split(", ", 2);
    if (tokens[0].equals("label")) {
      return tokens[1];
    } else {
      return null;
    }
  }

  /**
   * Actions an 'activateKeyBindings' event. Activates all of the given sets of key bindings.
   *
   * @param tokens
   *          event parameters
   */
  private void actionActivateKeyBindingsEvent(String[] tokens) {
    for (int i = 1; i < tokens.length; i++) {
      Engine.KEY_BINDINGS.activate(tokens[i]);
    }
  }

  /**
   * Actions a 'deactivateKeyBindings' event. Deactivates all of the given sets of key bindings.
   *
   * @param tokens
   *          event parameters
   */
  private void actionDeactivateKeyBindingsEvent(String[] tokens) {
    for (int i = 1; i < tokens.length; i++) {
      Engine.KEY_BINDINGS.deactivate(tokens[i]);
    }
  }

  /**
   * Used to share the token-parsing logic between {@link Cutscene#actionSoundEvent(String[])} and
   * {@link Cutscene#actionLoopSoundEvent(String[])} methods.
   */
  private abstract class ParseSoundParamsCommand implements Command {
    protected String asset, ref;
    protected float volume;

    public ParseSoundParamsCommand(String tokens[]) {
      asset = tokens[1];
      ref = null;
      volume = 1;
      if (tokens.length == 3) {
        // If first char is a digit then assume it is the volume parameter
        if (tokens[2].substring(0, 1).matches("\\d")) {
          volume = Float.parseFloat(tokens[2]);
        } else {
          ref = tokens[2];
        }
      } else if (tokens.length == 4) {
        volume = Float.parseFloat(tokens[2]);
        ref = tokens[3];
      }
    }
  }

  /**
   * Actions a sound event. Input is of the form <code>"sound, asset, [volume], [ref]"</code>.
   *
   * @param tokens
   *          parameters to the sound event
   */
  private void actionSoundEvent(String[] tokens) {
    new ParseSoundParamsCommand(tokens) {
      @Override
      public void execute() {
        Engine.SOUND_BOARD.play(asset, ref, volume);
      }
    }.execute();
  }

  /**
   * Actions a loopSound event. Input is of the form
   * <code>"loopSound, asset, [volume], [ref]"</code>.
   *
   * @param tokens
   *          parameters to the sound event
   */
  private void actionLoopSoundEvent(String[] tokens) {
    new ParseSoundParamsCommand(tokens) {
      @Override
      public void execute() {
        Engine.SOUND_BOARD.loop(asset, ref, volume);
      }
    }.execute();
  }

  /**
   * Actions a stopSound event. Input is of the form <code>"stopSound, ref"</code>.
   *
   * @param tokens
   *          parameters to the sound event
   */
  private void actionStopSoundEvent(String[] tokens) {
    String ref = tokens[1];
    Engine.SOUND_BOARD.stop(ref);
  }

  /**
   * Actions a setCamera event. Input is of the form
   * <code>"setCamera, px, py, pz, rx, ry, rz"</code>.
   *
   * @param tokens
   *          parameters to the set camera event
   */
  private void actionSetCameraEvent(String[] tokens) {
    float px = Float.parseFloat(tokens[1]);
    float py = Float.parseFloat(tokens[2]);
    float pz = Float.parseFloat(tokens[3]);
    float rx = Float.parseFloat(tokens[4]);
    float ry = Float.parseFloat(tokens[5]);
    float rz = Float.parseFloat(tokens[6]);
    Camera camera = CameraFactory.getInstance();
    camera.setPosition(px, py, pz);
    camera.setRotation(rx, ry, rz);
  }

  /**
   * Actions an error event. Input is of the form <code>"error, [message]"</code>.
   *
   * @param tokens
   *          parameters to the error event
   */
  private void actionErrorEvent(String[] tokens) {
    String message = tokens.length > 1 ? tokens[1]
      : "ERROR: Hit 'error' event when playing cutscene";
    List<String> data = new ArrayList<>(eventQueue);
    data.set(index, String.format("%s    <==== ERROR THROWN HERE (userInput = %s)",
        eventQueue.get(index), userInput));
    data.add(0, String.format("ERROR: %s", message));
    throw new RuntimeException(StringUtils.join(data, "\n"));
  }

  /**
   * Actions a log event. Input is of the form {@code "log, "message""}. Note that {@message} does
   * not strictly need to be quoted unless it contains a comma.
   *
   * @param tokens
   *          parameters
   */
  private void actionLogEvent(String[] tokens) {
    String message = tokens[1];
    if (tokens[1].startsWith("\"") && tokens[1].endsWith("\"")) {
      message = tokens[1].substring(1, tokens[1].length() - 1);
    }
    Log.info(message);
  }

  /**
   * Actions a user event. Input is of the form <code>"user, name, params..."</code>.
   *
   * @param tokens
   *          parameters to the user event
   */
  private void actionUserEvent(String[] tokens) {
    String userEventName = tokens[1];
    CutsceneCommand cutsceneCommand = userEventRegistry.get(userEventName);
    if (cutsceneCommand == null) {
      throw new RuntimeException(String.format(
          "User event '%s' not found. Did you forget to register it in a Dataset? See CutsceneLoader#registerUserEvent.",
          userEventName));
    }
    cutsceneCommand.execute(tokens);
  }

  private void actionSkipPoint(String[] tokens) {
    isSkipping = false;
  }

  @Override
  public Cutscene getClone() {
    Cutscene clone = new Cutscene(cutsceneDataLoaderForCloning);
    clone.nextState = nextState;
    clone.isClone = true;
    clone.userEventRegistry = new HashMap<>();
    userEventRegistry.keySet()
        .forEach(key -> clone.userEventRegistry.put(key, userEventRegistry.get(key)));
    clone.properties = new HashMap<>();
    properties.keySet().forEach(key -> clone.properties.put(key, properties.get(key)));
    return clone;
  }
}
