package game1.toolkit.cutscenes.utils;

public interface CutsceneRotatable {

  public void startRotating(long duration, float targetX, float targetY, float targetZ);

  public void stopRotating();

  public void setRotation(float degreesX, float degreesY, float degreesZ);
}
