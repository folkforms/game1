package game1.toolkit.cutscenes.effects;

import game1.actors.Actor;

/**
 * Utility class for instantiating default versions of various screen wipes:
 * <ul>
 * <li>Fade out</li>
 * <li>Fade in</li>
 * <li>Wipe out</li>
 * <li>Wipe in</li>
 * <li>Star wipe (work in progress)</li>
 * </ul>
 */
public class WipeUtils {

  public static final int FADE_OUT = 1;
  public static final int FADE_IN = 2;
  public static final int WIPE_OUT = 3;
  public static final int WIPE_IN = 4;

  public static Actor get(int type, long duration) {
    if (type == FADE_OUT) {
      return new FadeOut(duration);
    } else if (type == FADE_IN) {
      return new FadeIn(duration);
    } else if (type == WIPE_OUT) {
      return new WipeOut(duration);
    } else if (type == WIPE_IN) {
      return new WipeIn(duration);
    }
    throw new RuntimeException(String.format("Invalid type '%s' passed to WipeUtils.get", type));
  }
}
