package game1.toolkit.cutscenes.examples;

import org.joml.Vector3f;

import game1.actors.Actor;
import game1.actors.Moveable;
import game1.actors.Tickable;
import game1.core.graphics.Drawable;
import game1.core.graphics.FontUtils;
import game1.core.graphics.RenderedFont;
import game1.datasets.GameData;
import game1.datasets.GameDataFactory;
import game1.toolkit.cutscenes.internal.Cutscene;
import game1.toolkit.widgets.label.Label;
import game1.toolkit.widgets.label.LabelBuilder;

/**
 * Speech bubble for cutscenes.
 */
public class SpeechBubble implements Actor, Tickable {

  private static Drawable TOP, TOP_LEFT, TOP_RIGHT, LEFT, RIGHT, BOTTOM_1, BOTTOM_2, BOTTOM_LEFT,
      BOTTOM_RIGHT, LEFT_TAIL, RIGHT_TAIL, CENTER;

  static {
    GameData gameData = GameDataFactory.getInstance();
    TOP = gameData.get("speechbubble-top");
    TOP_LEFT = gameData.get("speechbubble-top-left");
    TOP_RIGHT = gameData.get("speechbubble-top-right");
    LEFT = gameData.get("speechbubble-left");
    RIGHT = gameData.get("speechbubble-right");
    BOTTOM_1 = gameData.get("speechbubble-bottom-1");
    BOTTOM_2 = gameData.get("speechbubble-bottom-2");
    BOTTOM_LEFT = gameData.get("speechbubble-bottom-left");
    BOTTOM_RIGHT = gameData.get("speechbubble-bottom-right");
    LEFT_TAIL = gameData.get("speechbubble-left-tail");
    RIGHT_TAIL = gameData.get("speechbubble-right-tail");
    CENTER = gameData.get("speechbubble-center");
  }

  private RenderedFont fontFace;
  private float fontScale;
  private Label label;

  private Moveable moveable;

  private String text;
  private int lineHeight;
  private int numLines;

  private int x, y, z, w, h;
  private int tailSize = 40;
  private int pixel = 4;
  private boolean leftTail;

  private int padding = pixel * 3;
  private int lineGap = pixel;

  private String[] visibleLines;
  private int numVisibleChars = 0;
  private int charDelayTimer = 0;

  protected int charDelay = 3;

  public SpeechBubble(String reference, String text, boolean leftTail, int z, RenderedFont fontFace,
      float fontScale) {
    this.fontFace = fontFace;
    this.fontScale = fontScale;
    // FIXME DATASETS: Speechbubble needs an exact item
    this.moveable = GameDataFactory.getInstance().get(reference);
    this.text = text.replaceAll("\\\\n", "\n");
    this.numLines = countNumLines(text);
    this.visibleLines = new String[0];
    this.z = z;
    this.w = calcWidth();
    this.h = calcHeight();
    this.leftTail = leftTail;

    Vector3f pos = moveable.getPosition();
    this.x = (int) pos.x + 20 + (leftTail ? 0 : -w);
    this.y = (int) pos.y + 170;
    int adjust = 0;
    int textX = x + padding + adjust + pixel;
    int textY = y + padding + tailSize + pixel;

    this.label = LabelBuilder.init("").setFont(fontFace).setFontSize(fontScale)
        .setPosition(textX, textY, z).build();
    addChild(label);

    createBubble();
  }

  private int countNumLines(String t) {
    int count = 1;
    for (int i = 0; i < t.length() - 1; i++) {
      if (t.substring(i, i + 2).equals("\\n")) {
        count++;
      }
    }
    return count;
  }

  private void createBubble() {
    int tailW = pixel * 10;
    int tailH = pixel * 10;
    int farX = x + w;
    int farY = y + h + tailH;
    int tailX = leftTail ? x + pixel * 3 : x + w - pixel * 3 - tailH;
    addBubblePart(TOP_LEFT, x, farY);
    addBubblePart(TOP_RIGHT, farX, farY);
    addBubblePart(BOTTOM_LEFT, x, y + tailH);
    addBubblePart(BOTTOM_RIGHT, farX, y + tailH);
    stretchBubblePartHorizontally(TOP, x, farX, farY);
    stretchBubblePartHorizontally(BOTTOM_1, x, tailX, y + tailH);
    stretchBubblePartHorizontally(BOTTOM_2, tailX + tailW - pixel, farX, y + tailH);
    stretchBubblePartVertically(LEFT, x, y + tailH, farY);
    stretchBubblePartVertically(RIGHT, farX, y + tailH, farY);
    addBubblePart(leftTail ? LEFT_TAIL : RIGHT_TAIL, tailX, y);
    addCentre(tailH);
  }

  private void addCentre(int tailH) {
    int cx = x + pixel;
    int cy = y + tailH;
    Drawable clone = CENTER.deepClone();
    clone.resize(w / CENTER.getScale(), h / CENTER.getScale());
    clone.setName("speechbubble-center");
    clone.setPosition(cx, cy, z - 2);
    addChild(clone);
  }

  private void stretchBubblePartHorizontally(Drawable d, int startX, int endX, int y) {
    Drawable clone = d.deepClone();
    clone.setPosition(startX + pixel * 2, y, z - 1);
    clone.resize((endX - startX - pixel * 2) / clone.getScale(), (pixel * 2) / clone.getScale());
    addChild(clone);
  }

  private void stretchBubblePartVertically(Drawable d, int x, int startY, int endY) {
    Drawable clone = d.deepClone();
    clone.setPosition(x, startY + pixel * 2, z - 1);
    clone.resize((pixel * 2) / clone.getScale(), (endY - startY - pixel * 2) / clone.getScale());
    addChild(clone);
  }

  private void addBubblePart(Drawable d, int dx, int dy) {
    Drawable clone = d.deepClone();
    clone.setPosition(dx, dy, z - 1);
    addChild(clone);
  }

  private int calcWidth() {
    String[] lines = text.split("\n");
    numLines = lines.length;
    int maxWidth = 0;
    for (int i = 0; i < lines.length; i++) {
      int lineWidth = FontUtils.getStringWidth(lines[i], fontFace, fontScale);
      if (lineWidth > maxWidth) {
        maxWidth = lineWidth;
      }
    }
    int output = maxWidth + padding * 2;
    if (output % 4 == 0) {
      return output;
    } else {
      return output + 4 - (output % 4);
    }
  }

  private int calcHeight() {
    int countEndLine = 0;
    for (int i = 0; i < text.length(); i++) {
      if (text.charAt(i) == '\n') {
        countEndLine++;
      }
    }
    lineHeight = FontUtils.getStringHeight(fontFace, fontScale);
    return (lineHeight * (countEndLine + 1)) + (padding * 2) + (countEndLine * lineGap);
  }

  @Override
  public void onTick() {
    charDelayTimer++;
    if (Cutscene.isFastForwarding()) {
      charDelayTimer += Cutscene.FAST_FORWARD_SPEED;
    }
    while (charDelayTimer > charDelay && numVisibleChars <= text.length()) {
      charDelayTimer -= charDelay;
      numVisibleChars++;
    }
    visibleLines = StringSplitter.split(text, numVisibleChars);
    label.setText(createLabelText(visibleLines));
  }

  private String createLabelText(String[] lines) {
    StringBuffer sb = new StringBuffer(lines[0]);
    int countLines = 0;
    for (int i = 1; i < lines.length; i++) {
      sb.append("\n").append(lines[i]);
      countLines++;
    }
    // Append invisible lines so the label text has correct height
    while (countLines < numLines) {
      sb.append(" \n");
      countLines++;
    }
    return sb.toString();
  }

  /**
   * Private constructor used to create a completed speech bubble for debugging purposes.
   */
  private SpeechBubble(String reference, String text, boolean leftTail, int z,
      RenderedFont fontFace, float fontScale, boolean debug) {
    this(reference, text, leftTail, z, fontFace, fontScale);
    text = text.replaceAll("\\\\n", "\n");
    numVisibleChars = text.length();
    visibleLines = StringSplitter.split(text, numVisibleChars);
  }

  /**
   * Debug method used to create a completed speech bubble.
   */
  public static SpeechBubble debug_createFinishedSpeechBubble(String reference, String text,
      boolean leftTail, int z, RenderedFont fontFace, float fontScale) {
    return new SpeechBubble(reference, text, leftTail, z, fontFace, fontScale, true);
  }
}
