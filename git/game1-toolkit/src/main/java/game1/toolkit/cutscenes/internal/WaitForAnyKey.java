package game1.toolkit.cutscenes.internal;

import game1.actors.Actor;
import game1.actors.Tickable;
import game1.core.input.KeyboardFactory;

/**
 * @deprecated Old Cutscenes v5 code. Use C9 instead.
 */
@Deprecated
class WaitForAnyKey implements Actor, Tickable {

  @Override
  public void onTick() {
    if (KeyboardFactory.getInstance().anyKeyDown()) {
      Cutscene.setWaiting(false);
      Cutscene.setUserInput(Integer.MIN_VALUE);
      // Engine.GAME_STATE.removeActor(this);
    }
  }
}
