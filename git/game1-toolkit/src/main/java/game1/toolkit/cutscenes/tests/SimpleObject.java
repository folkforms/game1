package game1.toolkit.cutscenes.tests;

import game1.actors.Actor;
import game1.toolkit.widgets.label.Label;
import game1.toolkit.widgets.label.LabelBuilder;

/**
 * @deprecated Old Cutscenes v5 code. Use C9 instead.
 */
@Deprecated
public class SimpleObject implements Actor {

  public SimpleObject() {
    this("Default constructor!", 100, 150, 1000);
  }

  public SimpleObject(String text) {
    this(text, 100, 125, 1000);
  }

  public SimpleObject(int y) {
    this("One-arg (int) version", 100, y, 1000);
  }

  public SimpleObject(String text, int x, int y, int z) {
    Label label = LabelBuilder.init(text).setPosition(x, y, z).build();
    addChild(label);
  }
}
