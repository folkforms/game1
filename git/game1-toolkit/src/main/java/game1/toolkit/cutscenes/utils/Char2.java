package game1.toolkit.cutscenes.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joml.Vector3f;

import game1.actors.Actor;
import game1.actors.Moveable;
import game1.actors.Tickable;
import game1.core.graphics.Animation;
import game1.datasets.GameDataFactory;
import game1.toolkit.cutscenes.internal.Cutscene;

/**
 * Represents a character in a cutscene. This is a convenience class that implements
 * {@link ChangeAnimationByRef}, {@link CutsceneMoveable} and {@link Tickable}.
 */
public class Char2 implements Actor, ChangeAnimationByRef, CutsceneMoveable, Moveable, Tickable {

  protected Animation currentAnimation = null;
  protected Map<String, Animation> animations = new HashMap<>();
  protected int x, y, z;
  private Vector3f startPos, targetPos;
  private boolean isMoving = false;
  private long moveDuration = 0;
  private long numTicks = 0;

  public Char2(int x, int y, int z, List<String> animationRefs) {
    this.x = x;
    this.y = y;
    this.z = z;
    animationRefs.forEach(ref -> {
      Animation clone = GameDataFactory.getInstance().get(ref);
      clone.setVisible(false);
      animations.put(ref, clone);
      addChild(clone);
    });
    setPosition(x, y, z);
  }

  @Override
  public void setAnimation(String ref) {
    if (currentAnimation != null) {
      currentAnimation.setVisible(false);
    }

    Animation newAnimation = animations.get(ref);
    if (newAnimation == null) {
      throw new RuntimeException(String.format("Error: Unknown animation '%s' for %s", ref,
          this.getClass().getSimpleName()));
    }
    newAnimation.setVisible(true);
    currentAnimation = newAnimation;
  }

  @Override
  public void startMoving(long duration, float targetX, float targetY) {
    startMoving(duration, targetX, targetY, z);
  }

  @Override
  public void startMoving(long duration, float targetX, float targetY, float targetZ) {
    numTicks = 0;
    moveDuration = duration;
    startPos = new Vector3f(x, y, z);
    targetPos = new Vector3f(targetX, targetY, targetZ);
    isMoving = true;
  }

  @Override
  public void stopMoving() {
    isMoving = false;
    numTicks = 0;
    setPosition(targetPos.x, targetPos.y, targetPos.z);
  }

  @Override
  public void onTick() {
    if (isMoving) {
      int newX = (int) (startPos.x + (targetPos.x - startPos.x) * numTicks / moveDuration);
      int newY = (int) (startPos.y + (targetPos.y - startPos.y) * numTicks / moveDuration);
      int newZ = (int) (startPos.z + (targetPos.z - startPos.z) * numTicks / moveDuration);
      setPosition(newX, newY, newZ);
      numTicks++;
      if (Cutscene.isFastForwarding()) {
        numTicks += Cutscene.FAST_FORWARD_SPEED;
      }
    }
    if (isMoving && numTicks >= moveDuration) {
      stopMoving();
    }
  }

  @Override
  public Vector3f getPosition() {
    return new Vector3f(x, y, z);
  }

  @Override
  public void setPosition(float x, float y, float z) {
    this.x = (int) x;
    this.y = (int) y;
    this.z = (int) z;
    animations.values().stream().forEach(animation -> animation.setPosition(x, y, z));
  }
}
