package game1.toolkit.cutscenes;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import game1.actors.Actor;
import game1.core.resources.loaders.Loader;
import game1.filesystem.FileSystem;
import game1.toolkit.cutscenes.internal.Cutscene;
import game1.toolkit.cutscenes.internal.CutsceneDataLoader;

/**
 * Loads a {@link Cutscene} from disk.
 *
 * @deprecated Old Cutscenes v5 code. Use C9 instead.
 */
@Deprecated
public class CutsceneLoader implements Loader {

  private String cutsceneFile;
  private String nextState;
  private Map<String, CutsceneCommand> userEvents = new HashMap<>();
  private Map<String, String> properties = new HashMap<>();

  public static CutsceneLoader init(String file) {
    CutsceneLoader cutscenePlayerLoader = new CutsceneLoader();
    cutscenePlayerLoader.cutsceneFile = file;
    return cutscenePlayerLoader;
  }

  public CutsceneLoader nextState(String nextState) {
    this.nextState = nextState;
    return this;
  }

  public CutsceneLoader registerUserEvent(String name, CutsceneCommand command) {
    userEvents.put(name, command);
    return this;
  }

  public CutsceneLoader addProperties(Map<String, String> properties) {
    this.properties.putAll(properties);
    return this;
  }

  @Override
  public Actor load(FileSystem fileSystem) throws IOException {
    CutsceneDataLoader cutsceneDataLoader = new CutsceneDataLoader(fileSystem, cutsceneFile);
    Cutscene cutscene = new Cutscene(cutsceneDataLoader);
    cutscene.setNextState(nextState);
    userEvents.keySet()
        .forEach(eventName -> cutscene.registerUserEvent(eventName, userEvents.get(eventName)));
    properties.keySet().forEach(key -> cutscene.setProperty(key, properties.get(key).toString()));
    return cutscene;
  }

  @Override
  public void unload(FileSystem fileSystem) {
    // fileSystem.unload(cutsceneFile);
  }
}
