package game1.toolkit.cutscenes.utils;

public interface ChangeAnimationByRef {

  /**
   * Sets the current animation by reference.
   *
   * @param animationReference
   *          reference for the new animation
   */
  public void setAnimation(String animationReference);
}
