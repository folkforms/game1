package game1.toolkit.cutscenes;

import game1.core.engine.Engine;
import game1.core.input.KeyBindings;
import game1.core.input.KeyCommand;
import game1.events.Command;
import game1.toolkit.cutscenes.internal.Cutscene;

/**
 * @deprecated Old Cutscenes v5 code. Use C9 instead.
 */
@Deprecated
public class FastForwardKeyBindings extends KeyBindings {

  private int[] fastForwardKey = Engine.PROPERTIES
      .getKeyboardKeysProperty("cutscene.key.fast_forward");

  public FastForwardKeyBindings() {
    Command fastForwardCommand = new Command() {
      @Override
      public void execute() {
        Cutscene.setFastForwarding(true);
      }
    };
    register(KeyCommand.init().command(fastForwardCommand).noBlock().keys(fastForwardKey));
  }
}
