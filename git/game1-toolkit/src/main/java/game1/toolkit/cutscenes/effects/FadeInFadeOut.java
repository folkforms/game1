package game1.toolkit.cutscenes.effects;

import folkforms.maths.Graph;
import game1.actors.Actor;
import game1.actors.Tickable;
import game1.core.engine.Engine;
import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.resolutions.layers.ResolutionLayers;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;
import game1.toolkit.cutscenes.internal.Cutscene;
import game1.toolkit.widgets.background.Background;

public class FadeInFadeOut implements Actor, Tickable {

  protected long numTicks = 0;
  protected long totalTicks;
  protected Colour colour = new Colour(0, 0, 0, 1);
  protected float[] alphaValues = { 1, 0, 0, 1 };
  protected float[] xValues;
  protected Drawable background;

  public FadeInFadeOut(long fadeInTicks, long holdTicks, long fadeOutTicks) {
    totalTicks = fadeInTicks + holdTicks + fadeOutTicks;
    xValues = new float[] { 0, (float) fadeInTicks / totalTicks,
        (float) (fadeInTicks + holdTicks) / totalTicks, 1 };

    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(ResolutionLayers.WINDOW_LAYER).getResolutionHints();
    float scale = hints.getResolutionScale();
    float width = hints.getResolutionWidth() * scale;
    float height = hints.getResolutionHeight() * scale;
    background = Background.create(0, 0, Engine.PROPERTIES.getIntProperty("cutscene.fade.zindex"),
        (int) width, (int) height, colour);
    addChild(background);
  }

  @Override
  public void onTick() {
    float alpha = Graph.calculateValue(xValues, alphaValues, numTicks, totalTicks);
    colour.setAlpha(alpha);
    background.setColour(colour);
    numTicks++;
    if (Cutscene.isFastForwarding()) {
      numTicks += Cutscene.FAST_FORWARD_SPEED;
    }
  }
}
