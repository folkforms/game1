package game1.toolkit.cutscenes;

import game1.core.engine.Engine;
import game1.core.input.KeyBindings;
import game1.core.input.KeyCommand;
import game1.core.input.Mouse;
import game1.events.Command;
import game1.toolkit.cutscenes.internal.Cutscene;

/**
 * @deprecated Old Cutscenes v5 code. Use C9 instead.
 */
@Deprecated
public class SkipTextKeyBindings extends KeyBindings {
  private int[] skipTextKey = Engine.PROPERTIES.getKeyboardKeysProperty("cutscene.key.skip_text");

  public SkipTextKeyBindings() {
    Command skipTextCommand = new Command() {
      @Override
      public void execute() {
        Cutscene.setSkipping(true);
      }
    };
    register(KeyCommand.init().command(skipTextCommand).mouse(Mouse.LEFT_BUTTON));
    register(KeyCommand.init().command(skipTextCommand).keys(skipTextKey));
  }
}
