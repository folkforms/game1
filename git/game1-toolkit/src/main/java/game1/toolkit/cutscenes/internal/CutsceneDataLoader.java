package game1.toolkit.cutscenes.internal;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import folkforms.log.Log;
import folkforms.stringutils.StringUtils;
import folkforms.textio.TextIO;
import game1.core.engine.Game1RuntimeException;
import game1.core.input.KeyboardUtils;
import game1.debug.DebugScopes;
import game1.filesystem.FileSystem;
import game1.toolkit.cutscenes.effects.WipeUtils;
import game1.toolkit.internal.levels.cutscenes.ChooseDebugUserInput;
import game1.toolkit.internal.levels.cutscenes.CreateAndActivateKeyBindings;
import game1.toolkit.internal.levels.cutscenes.PollUserInput;

/**
 * Loads a cutscene from disk and validates its data.
 *
 * @deprecated Old Cutscenes v5 code. Use C9 instead.
 */
// FIXME DATASETS: Are imports correctly removed from InMemoryStorage?
@Deprecated
public class CutsceneDataLoader {

  private String filename;
  private static int fileCount = 0;
  private int fileNumber = 0;

  private final String DEFAULT_ERROR_MESSAGE = "Error parsing %s:%s: %s";

  private List<String> contents;
  private String[] allowedCommands = { "add", "remove", "setPosition", "setRotation",
      "setPositionRotation", "move", "rotate", "moveRotate", "wait", "label", "showText",
      "setAnimation", "startAnimation", "stopAnimation", "resetAnimation", "setStencil",
      "clearStencil", "wipe", "jump", "jumpIfEqual", "waitForKey", "waitForAnyKey",
      "waitForUserInput", "waitForAnimation", "activateKeyBindings", "deactivateKeyBindings",
      "createActor", "sound", "loopSound", "stopSound", "setCamera", "user", "error", "log",
      "createAndActivateKeyBindings", "pollUserInput", "pushDebugUserInput", "chooseDebugUserInput",
      "skipPoint" };

  public CutsceneDataLoader(FileSystem fileSystem, String filename, String... args)
      throws IOException {
    Log.scoped(DebugScopes.CUTSCENES, "CutsceneLoader");
    Log.scoped(DebugScopes.CUTSCENES, "    filename = %s, args = %s", filename,
        Arrays.toString(args));
    this.filename = filename;
    fileNumber = fileCount;
    fileCount++;

    List<String> contents = fileSystem.loadList(filename);

    this.contents = contents;
    TextIO textIO = new TextIO();
    textIO.stripInlineComments(contents, "#");
    textIO.stripBlankLines(contents);

    trimLines();
    resolveImports(fileSystem);
    removeTildes();
    validateAllCommandsAllowed();
    validateNumParams("add", 1, Integer.MAX_VALUE);
    validateNumParams("remove", 1, Integer.MAX_VALUE);
    validateNumParams("setPosition", 3, 4);
    validateNumParams("setRotation", 4, 4);
    validateNumParams("setPositionRotation", 7, 7);
    validateNumParams("move", 4, 5);
    validateNumParams("rotate", 4, 5);
    validateNumParams("moveRotate", 8, 8);
    validateNumParams("label", 1, 1);
    validateNumParams("showText", 3, 5);
    validateNumParams("setAnimation", 2, 2);
    validateNumParams("startAnimation", 1, 1);
    validateNumParams("stopAnimation", 1, 1);
    validateNumParams("resetAnimation", 1, 1);
    validateNumParams("setStencil", 5, 7);
    validateNumParams("clearStencil", 1, 1);
    validateNumParams("wipe", 3, 3);
    validateNumParams("jump", 1, 1);
    validateNumParams("jumpIfEqual", 2, 2);
    validateNumParams("waitForKey", 1, Integer.MAX_VALUE);
    validateNumParams("waitForAnyKey", 0, 1);
    validateNumParams("waitForUserInput", 1, Integer.MAX_VALUE);
    validateNumParams("waitForAnimation", 1, Integer.MAX_VALUE);
    validateNumParams("activateKeyBindings", 1, Integer.MAX_VALUE);
    validateNumParams("deactivateKeyBindings", 1, Integer.MAX_VALUE);
    validateNumParams("createActor", 2, Integer.MAX_VALUE);
    validateNumParams("sound", 1, Integer.MAX_VALUE);
    validateNumParams("loopSound", 1, Integer.MAX_VALUE);
    validateNumParams("stopSound", 1, 1);
    validateNumParams("setCamera", 6, 6);
    validateNumParams("user", 1, Integer.MAX_VALUE);
    validateNumParams("error", 0, 1);
    validateNumParams("log", 1, 1);
    validateNumParams("createAndActivateKeyBindings", 2, Integer.MAX_VALUE);
    validateNumParams("pollUserInput", 1, Integer.MAX_VALUE);
    validateNumParams("pushDebugUserInput", 1, Integer.MAX_VALUE);
    validateNumParams("chooseDebugUserInput", 2, Integer.MAX_VALUE);
    validateNumParams("skipPoint", 0, 0);

    fixAtRefs();
    fixArgs(args);
    fixWipes();
    fixKeyboardReferences();
    fixCreateAndActivateKeyBindings();
    fixPollUserInput();
    fixChooseDebugUserInput();
  }

  List<String> getOutput() {
    return contents;
  }

  private void resolveImports(FileSystem fileSystem) throws IOException {
    outer: while (hasImports(contents)) {
      // Go backwards for imports in order to preserve line numbers when an import is not found
      for (int i = contents.size() - 1; i >= 0; i--) {
        String line = contents.get(i);
        if (line.contains("@import")) {
          String importedFilename = line.substring(8);

          String[] args = {};
          List<String> groups = StringUtils.listGroups(importedFilename, "(.*?) \\((.*)\\)");
          if (groups.size() > 0) {
            importedFilename = groups.get(0);
            args = StringUtils.splitCSV(groups.get(1), ", ");
          }

          // Check argument syntax
          if ((importedFilename.startsWith("\"") && !importedFilename.endsWith("\""))
              || importedFilename.contains(" ")) {
            String specificErrorMessage = String.format(
                "Arguments to @import must be wrapped in brackets, i.e. \"@import <filename> (arg1, arg2, ...)\". Instead found \"%s\".",
                line);
            throw new RuntimeException(createDefaultErrorMessage(i, specificErrorMessage));
          }

          importedFilename = importedFilename.replace("\"", "");
          if (!fileSystem.exists(importedFilename)) {
            throw new Game1RuntimeException("File %s does not exist", importedFilename);
          }

          // Load the file
          List<String> importedFileContents = new CutsceneDataLoader(fileSystem, importedFilename,
              args).getOutput();
          contents.remove(i);
          contents.addAll(i, importedFileContents);
          continue outer;
        }
      }
    }
  }

  private boolean hasImports(List<String> contents) {
    for (int i = 0; i < contents.size(); i++) {
      if (contents.get(i).startsWith("@import")) {
        return true;
      }
    }
    return false;
  }

  String getFilename() {
    return filename;
  }

  /**
   * Lines prefixed with tildes ('~') will have this prefix removed and will be trimmed but will
   * otherwise be left intact. This prefix can be used to visually separate debug code from real
   * code.
   *
   * <p>
   * Examples:
   * </p>
   * <code>
   * "~foo" &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; => "foo"<br/>
   * "~~~ foo" &nbsp; &nbsp; &nbsp; => "foo"<br/>
   * "~~~~~~~~foo" &nbsp; => "foo"<br/>
   * "~~~~~~ &nbsp;&nbsp; foo" => "foo"<br/>
   * </code>
   */
  private void removeTildes() {
    contents = contents.stream().map(line -> {
      if (line.startsWith("~")) {
        return line.replaceAll("~*(.*)", "$1").trim();
      } else {
        return line;
      }
    }).collect(Collectors.toList());
  }

  private void trimLines() {
    contents = contents.stream().map(line -> line.trim()).collect(Collectors.toList());
  }

  private String createDefaultErrorMessage(int line, String specificErrorMessage) {
    return String.format(DEFAULT_ERROR_MESSAGE, filename, line + 1, specificErrorMessage);
  }

  private void validateAllCommandsAllowed() {
    outer: for (int i = 0; i < contents.size(); i++) {
      String line = contents.get(i);
      if (line.trim().startsWith("#")) {
        continue outer;
      }
      String[] tokens = line.split(", ");
      for (int j = 0; j < allowedCommands.length; j++) {
        if (tokens[0].startsWith("?")) {
          tokens[0] = tokens[0].substring(1).trim();
        }
        if (line.length() == 0 || allowedCommands[j].equals(tokens[0])) {
          continue outer;
        }
      }
      String specificErrorMessage = String.format(
          "Unknown command '%s'. If this is a user command you need to prefix it with \"user\", i.e. \"user, <command>, <args>\".",
          tokens[0], line);
      throw new RuntimeException(createDefaultErrorMessage(i, specificErrorMessage));
    }
  }

  private void validateNumParams(String command, int minimum, int maximum) {
    for (int i = 0; i < contents.size(); i++) {
      String line = contents.get(i);
      String[] tokens = StringUtils.splitCSV(line, ", ");
      if (tokens[0].startsWith("?")) {
        tokens[0] = tokens[0].substring(1).trim();
      }
      if (tokens[0].equals(command)) {
        if (tokens.length - 1 < minimum || tokens.length - 1 > maximum) {
          String message;
          if (minimum == maximum) {
            String specificErrorMessage = String.format(
                "Incorrect number of params for '%s'. Expected exactly %s but was %s.", tokens[0],
                minimum, tokens.length - 1, line);
            message = createDefaultErrorMessage(i, specificErrorMessage);
          } else if (maximum == Integer.MAX_VALUE) {
            String specificErrorMessage = String.format(
                "Incorrect number of params for '%s'. Expected at least %s but was %s.", tokens[0],
                minimum, tokens.length - 1, line);
            message = createDefaultErrorMessage(i, specificErrorMessage);
          } else {
            String specificErrorMessage = String.format(
                "Incorrect number of params for '%s'. Expected between %s and %s params but was %s.",
                tokens[0], minimum, maximum, tokens.length - 1, line);
            message = createDefaultErrorMessage(i, specificErrorMessage);
          }
          throw new RuntimeException(message);
        }
      }
    }
  }

  /**
   * Replaces "@ref" with a string unique to this file. Cutscenes should use e.g. "@ref1" and they
   * will get something like "file-5-ref-1".
   */
  private void fixAtRefs() {
    for (int i = 0; i < contents.size(); i++) {
      String line = contents.get(i);
      if (line.contains("@ref")) {
        if (line.contains("@ref,")) {
          String specificErrorMessage = "@ref is missing an identifier. Should be '@ref<identifier>' e.g. '@ref1' or '@ref-foo' but was '@ref'.";
          throw new RuntimeException(createDefaultErrorMessage(i, specificErrorMessage));
        }
        line = line.replace("@ref", String.format("file-%s-ref-", fileNumber));
        contents.set(i, line);
      }
    }
  }

  /**
   * Replaces <code>@arg&lt;index&gt;</code> with an argument passed in via the import statement.
   * This allows you to template repeated sections of code.
   */
  private void fixArgs(String[] args) {
    // Fix \n's
    for (int i = 0; i < args.length; i++) {
      args[i] = args[i].replace("\\n", "\n");
    }

    // Replace @arg<index>
    for (int i = 0; i < contents.size(); i++) {
      String line = contents.get(i);
      while (line.contains("@arg")) {
        // Validate we have an index
        if (line.contains("@arg,")) {
          String specificErrorMessage = "Error: @arg is missing an identifier. Should be '@arg<index>' e.g. '@arg1' but was '@arg'.";
          throw new RuntimeException(createDefaultErrorMessage(i, specificErrorMessage));
        }
        // Validate the index is a number and is within the array bounds
        validateArgIndexes(line, args, i);
        // Replace the arg
        for (int j = 0; j < args.length; j++) {
          String arg = "@arg" + j;
          line = line.replaceAll(arg, args[j]);
          contents.set(i, line);
        }
      }
    }
  }

  private void validateArgIndexes(String line, String[] args, int lineNum) {
    String[] tokens = StringUtils.splitCSV(line, ", ");
    for (int i = 0; i < tokens.length; i++) {
      if (tokens[i].startsWith("@arg")) {
        String index = tokens[i].substring(4);
        // Validate index is a number
        if (!index.matches("\\d+")) {
          String specificErrorMessage = String.format(
              "@arg should be suffixed with a number, i.e. '@arg<index>', but found '%s'.",
              tokens[i]);
          throw new RuntimeException(createDefaultErrorMessage(lineNum, specificErrorMessage));
        }
        // Validate index is within the array bounds
        if (Integer.parseInt(index) >= args.length) {
          String specificErrorMessage = String.format(
              "No such argument '@arg%s'. %s %s supplied to the file. Args is a zero-based array.",
              index, args.length, args.length == 1 ? "arg was" : "args were");
          throw new RuntimeException(createDefaultErrorMessage(lineNum, specificErrorMessage));
        }
      }
    }
  }

  private void fixWipes() {
    for (int i = 0; i < contents.size(); i++) {
      String line = contents.get(i);

      boolean isDebugLine = line.startsWith("?");
      if (isDebugLine) {
        line = line.substring(1).trim();
      }

      if (line.startsWith("wipe,")) {
        line = line.replace("FADE_IN", Integer.toString(WipeUtils.FADE_IN));
        line = line.replace("FADE_OUT", Integer.toString(WipeUtils.FADE_OUT));
        line = line.replace("WIPE_IN", Integer.toString(WipeUtils.WIPE_IN));
        line = line.replace("WIPE_OUT", Integer.toString(WipeUtils.WIPE_OUT));
      }

      if (isDebugLine) {
        line = "? " + line;
      }

      contents.set(i, line);
    }
  }

  private void fixKeyboardReferences() {
    String keyPrefix = "KEY_";
    for (int i = 0; i < contents.size(); i++) {
      String line = contents.get(i);
      while (line.contains(keyPrefix)) {
        int start = line.indexOf(keyPrefix);
        int end = findEnd(line, start);
        String keyName = line.substring(start, end);
        int keyIndex = KeyboardUtils.convert(keyName)[0];
        Log.scoped(DebugScopes.CUTSCENES, "    fixKeyboardReferences: Parsing %s -> %s", keyName,
            keyIndex);
        line = line.replace(keyName, Integer.toString(keyIndex));
      }
      contents.set(i, line);
    }
  }

  private int findEnd(String line, int start) {
    char[] keyTerminators = { ' ', ',', '-' };
    for (int i = start; i < line.length(); i++) {
      char c = line.charAt(i);
      for (int j = 0; j < keyTerminators.length; j++) {
        if (keyTerminators[j] == c) {
          return i;
        }
      }
    }
    return line.length();
  }

  private void fixCreateAndActivateKeyBindings() {
    for (int i = 0; i < contents.size(); i++) {
      String line = contents.get(i);

      boolean isDebugLine = line.startsWith("?");
      if (isDebugLine) {
        line = line.substring(1).trim();
      }

      if (line.startsWith("createAndActivateKeyBindings")) {
        String[] tokens = line.split(", ");
        String ref = tokens[1];
        String[] keyTokens = Arrays.copyOfRange(tokens, 2, tokens.length);
        String newLine = CreateAndActivateKeyBindings.parse(ref, keyTokens);
        if (isDebugLine) {
          newLine = "? " + newLine;
        }
        contents.set(i, newLine);
      }
    }
  }

  private void fixPollUserInput() {
    for (int i = 0; i < contents.size(); i++) {
      String line = contents.get(i);

      boolean isDebugLine = line.startsWith("?");
      if (isDebugLine) {
        line = line.substring(1).trim();
      }

      if (line.startsWith("pollUserInput")) {
        List<String> output = PollUserInput.parse(line, isDebugLine);
        contents.remove(i);
        for (int j = 0; j < output.size(); j++) {
          contents.add(i + j, output.get(j));
        }
      }
    }
  }

  private void fixChooseDebugUserInput() {
    for (int i = 0; i < contents.size(); i++) {
      String line = contents.get(i);

      boolean isDebugLine = line.startsWith("?");
      if (isDebugLine) {
        line = line.substring(1).trim();
      }

      if (line.startsWith("chooseDebugUserInput")) {
        List<String> output = ChooseDebugUserInput.parse(line, isDebugLine);
        contents.remove(i);
        for (int j = 0; j < output.size(); j++) {
          contents.add(i + j, output.get(j));
        }
      }
    }
  }
}
