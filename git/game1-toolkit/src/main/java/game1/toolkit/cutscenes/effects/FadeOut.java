package game1.toolkit.cutscenes.effects;

import folkforms.maths.Graph;
import game1.actors.Actor;
import game1.actors.Tickable;
import game1.core.engine.Engine;
import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.resolutions.layers.ResolutionLayers;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;
import game1.toolkit.cutscenes.internal.Cutscene;
import game1.toolkit.widgets.background.Background;

public class FadeOut implements Actor, Tickable {

  protected long numTicks = 0;
  protected long durationTicks;
  protected Colour colour = new Colour(0, 0, 0, 0);
  protected float alpha = 0;
  protected float[] alphaValues = { 0, 1 };
  protected float[] timePercents = { 0, 1 };
  protected Drawable background;

  public FadeOut(long durationTicks) {
    this.durationTicks = durationTicks;
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(ResolutionLayers.WINDOW_LAYER).getResolutionHints();
    float scale = hints.getResolutionScale();
    float width = hints.getResolutionWidth() * scale;
    float height = hints.getResolutionHeight() * scale;
    background = Background.create(0, 0, Engine.PROPERTIES.getIntProperty("cutscene.fade.zindex"),
        (int) width, (int) height, colour);
    addChild(background);
  }

  @Override
  public void onTick() {
    alpha = Graph.calculateValue(timePercents, alphaValues, numTicks, durationTicks);
    colour.setAlpha(alpha);
    background.setColour(colour);
    numTicks++;
    if (Cutscene.isFastForwarding()) {
      numTicks += Cutscene.FAST_FORWARD_SPEED;
    }
  }
}
