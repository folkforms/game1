package game1.toolkit.cutscenes.internal;

import java.lang.reflect.Constructor;
import java.util.Arrays;

import game1.actors.Actor;

/**
 * @deprecated Old Cutscenes v5 code. Use C9 instead.
 */
@Deprecated
class CreateActor {

  /**
   * Creates an actor from the given data.
   *
   * @param tokens
   * @return
   * @throws Exception
   */
  public static Actor create(String[] tokens) {
    try {
      String clazzStr = tokens[2];
      String[] params = new String[tokens.length - 3];
      for (int j = 0; j < params.length; j++) {
        params[j] = tokens[j + 3];
      }
      Class<?>[] argTypes = getArgTypes(params);
      Object[] args = getArgs(params, argTypes);
      Class<?> clazz = Class.forName(clazzStr); // e.g. "com.foo.HelloWorld"
      Constructor<?> constructor = clazz.getConstructor(argTypes);
      Object instance = constructor.newInstance(args);
      Actor actor = (Actor) instance;
      return actor;

    } catch (Exception ex) {
      RuntimeException wrapper = new RuntimeException(
          String.format("Error: Could not create object from line: %s", Arrays.toString(tokens)));
      wrapper.initCause(ex);
      throw wrapper;
    }
  }

  private static Class<?>[] getArgTypes(String[] params) {
    Class<?>[] argTypes = new Class[params.length];
    for (int i = 0; i < params.length; i++) {
      if (params[i].matches("\\d+L")) {
        argTypes[i] = long.class;
      } else if (params[i].matches("\\d+")) {
        argTypes[i] = int.class;
      } else {
        argTypes[i] = String.class;
      }
    }
    return argTypes;
  }

  private static Object[] getArgs(String[] params, Class<?>[] argTypes) {
    Object[] args = new Object[params.length];
    for (int i = 0; i < params.length; i++) {
      if (argTypes[i] == long.class) {
        args[i] = Long.parseLong(params[i].substring(0, params[i].length() - 1));
      } else if (argTypes[i] == int.class) {
        args[i] = Integer.parseInt(params[i]);
      } else {
        args[i] = params[i];
        if (params[i].charAt(0) == '"' && params[i].charAt(params[i].length() - 1) == '"') {
          args[i] = params[i].substring(1, params[i].length() - 1);
        }
      }
    }
    return args;
  }
}
