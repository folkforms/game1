package game1.toolkit.cutscenes;

/**
 * @deprecated Old Cutscenes v5 code. Use C9 instead.
 */
@Deprecated
public interface CutsceneCommand {

  void execute(String[] tokens);
}
