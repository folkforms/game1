# Cutscenes

## Overview

Cutscenes must be loaded using the `CutsceneLoader` class. For example:

```
class MyDataset extends Dataset {
  MyDataset() {
    register("cutscene-reference",
        CutsceneLoader.init("resources/my-cutscene.txt")
            .nextState(MyStates.STATE_2));
    
  }
}

class State1 extends UserState {
  @Override
  public void apply() {
    Engine.GAME_STATE.addActor(TempActors.get("cutscene-reference"));
  }
}
```

Cutscenes will automatically be parsed and variables substituted (see `@var` below). Some commands are changed, and required references are created (e.g. showText creates a Label object, a reference to the Label, and is then replaced in the code with `add, labelReference`.)

Cutscenes start playing as soon as they are added to the game state.

You may also want to use the `Char` class. This is a convenience class for representing a character in a cutscene. It implements `ChangeAnimationByRef`, `CutsceneMoveable` and `Tickable`.

## Commands

**add, <reference1>, [<reference2>, ...]**

Adds the actors with the given references to the game state.

**remove, <reference>, [<reference2>, ...]**

Removes the actors with the given references from the game state.

**setPosition, <reference>, <x>, <y>, [<z>]**

Moves an actor to the new `x,y,[z]` coordinates immediately. The actor must be an instance of `CutsceneMoveable`.

**setRotation, <reference>, <rx>, <ry>, <rz>**

Rotates an actor by the given `rx,ry,rz` degrees immediately. The actor must be an instance of `CutsceneRotatable`.

**setPositionRotation, <ref>, <px>, <py>, <pz>, <rx>, <ry>, <rz>**

Sets the position of and rotates an actor to the given position and rotation values immediately. The actor must be an instance of both `CutsceneMoveable` and `CutsceneRotatable`.

**move, <duration>, <reference>, <x>, <y>, [<z>]**

Moves an actor to the new `x,y,[z]` coordinates over the given duration. The actor must be an instance of `CutsceneMoveable`.

**rotate, <duration>, <ref>, <rx>, <ry>, <rz>**

Rotates an actor to the new `rx,ry,rz` rotation over the given duration. The actor must be an instance of `CutsceneRotatable`.

**moveRotate, <duration>, <ref>, <px>, <py>, <pz>, <rx>, <ry>, <rz>**

Moves and rotates an actor to the new `px,py,pz` position and `rx,ry,rz` rotation over the given duration. The actor must be an instance of both `CutsceneMoveable` and `CutsceneRotatable`.

**setAnimation, <reference>, <value>**

Sets the animation of the given reference to `value`. The actor must be an instance of `ChangeAnimationByRef`.

**startAnimation, <reference>**

Starts the referenced animation.

**stopAnimation, <reference>**

Stops the referenced animation.

**resetAnimation, <reference>**

Resets the referenced animation.

**setStencil, <drawableRef>, <x>, <y>, <w>, <h>, <type>, <hex colour>, [<debug>]**

Creates and sets a stencil on a given animation. Stencils are set on Drawables, not Chars (a Char can contains multiple animations, each of which is a Drawable and each of which would require a separate stencil.) Type is typically '1' for an 'outside' stencil e.g. black bars. Type '2' is for an 'inside' stencil but you probably don't want that. If debug is true the stencil will be replaced with a red stencil for visibility.

It's often easier to use a separate 'black bars' stencil object (see `SimpleStencil`) to achieve the same effect. This command is just in case you need fine-grained control of stencils during a cutscene, or you have animations that are used in multiple cutscenes with different stencils.

**clearStencil, <animationRef>**

Clears the stencil on a given animation.

**wait, [<duration>]**

Waits a given number of ticks. If `duration` is blank it will wait indefinitely. In the latter case you would use `Cutscene.setUserInput(int)` or `Cutscene.setWaiting(false)` to tell it to stop waiting.

**pollUserInput, <key1> -> <output1>, [<key2> -> <output2>, ...]**

Waits for user input and converts the input key into one of the given outputs.

Example: `pollUserInput, KEY_A -> 1, KEY_B -> 2, KEY_C -> 3, KEY_D -> 4`

**waitForKey, <key1>, [<key2>, ...]**

Waits until the user presses a specific key. Can use names from the Keys class, for example `KEY_SPACE`, or the integer key codes.

**waitForAnyKey, [<timeout>]**

Waits until the user presses a key. If `timeout` is blank it will wait indefinitely.

**waitForUserInput, <value1>, [<value2>, ...]**

Waits until `Cutscene.getUserInput()` equals one of the given values.

**waitForAnimation, <asset reference>, [<count>]**

Waits a number of ticks equal to the total length of the given animation multiplied by `count`.

**label, <name>**

Creates a label at this point in time. `jump` and `jumpIfEqual` commands can be used to jump to this label. 

**jump, <label>**

Jumps to the given label.

**jumpIfEqual, <value>, <label>**

Compares `Cutscene.getUserInput()` to `value`, and jumps to `label` if they are equal.

**activateKeyBindings, <names...>**

Activates the named sets of key bindings.

**deactivateKeyBindings, <names...>**

Deactivates the named sets of key bindings.

**showText, <ref>, <text>, <x>, <y>, <z>**

Adds some text to the screen at the given coordinates.

**showText, <ref>, <text>, <speakerRef>**

Adds some text to the screen relative to the `speakerRef` Drawable.

**wipe, <ref>, <duration>, <type>**

Creates a screen wipe with the given ref, duration and type. Type can be one of FADE\_IN, FADE\_OUT, WIPE\_IN, WIPE\_OUT. 

**createActor, <ref>, <Class>, [args...]**

Creates an actor of type `Class` using the given args in the constructor, with a reference of `ref`. It will attempt to guess the argument types, where `\\d+L` => `long`, `\\d+` => `int` and anything else => `String`. If no suitable constructor is found for the given arguments then an error will be thrown.

**sound, <asset>, [volume], [<ref>]**

Plays a given sound asset. Volume must be a value between 0 and 1, and defaults to 1 if not specified. If `ref` is provided then a reference to the currently-playing sound object will be stored.

**loopSound, <asset>, [volume], [<ref>]**

Plays a sound asset in a loop. See `sound` above for arguments.

**stopSound, <ref>**

Stops a currently-playing sound. It cannot be restarted.

**createAndActivateKeyBindings, <ref>, <key1>, [<key2>, ...]**

Creates a temporary in-memory set of keybindings. For example:

```
showText, @ref0, "Press 'A' or 'B'", 500, 800, 1000
createAndActivateKeyBindings, @ref1, KEY_A, KEY_B
waitForUserInput, KEY_A, KEY_B
deactivateKeyBindings, @ref1
jumpIfEqual, KEY_A, branch-for-key-a
jumpIfEqual, KEY_B, branch-for-key-b
```

**user, <command>, [args...]**

Executes a user-defined command. The command must be registered beforehand using `Cutscene.registerUserEvent(String name, CutsceneCommand cutsceneCommand`.

**error, [<message>]**

Throws an error. Only used for debugging.

## Camera controls

In order to use the camera controls you will need to add an object to the scene that is capable of controlling the camera, such as `game1.toolkit.actors.Player`. In order to move the camera the object must implement `CutsceneMoveable`, and in order to rotate the camera the object must implement `CutsceneRotatable`.

You can then use `setPosition`, `setRotation`, `setPositionRotation`, `move`, `rotate` and `moveRotate` to control the camera.

## Variables

**@import <file> [(args...)]**

Inserts the given file's contents at this point. Can pass a set of arguments of the form `(arg1, arg2, ...)`. Example: `@import "myfile.txt" (foo, 1, 2)`.

**@arg<index>**

Any arguments to an @import statement will be used to replace instances of `@arg<index>`, where index is the argument number (arguments array is 0-based.)

**@ref<value>**

Creates a reference unique to this file with `value` appended to it. For example, `@ref1` might become `file-5-ref-1`. This is used to quickly create references that are scoped to the current file.

**@var("key")**

Lines containing `@var("key")` will check the cutscene properties for a value for `key` and substitute it before actioning the line. Use `Cutscene#setProperty(key, value)` to set cutscene properties.

## Misc

**Fast forwarding**

Fast-forwarding is built-in to the cutscenes. For user events that need to fast-forward they should tick faster when fast-forwarding is active, e.g.:

```
  public void onTick() {
    numTicks++;

    // Fast-forward code
    if (Cutscene.isFastForwarding()) {
      numTicks += Cutscene.FAST_FORWARD_SPEED;
    }
    
    // ...
  }
```

Note: Fast-forwarding speed can be adjusted using the property `cutscene.fast_forward_speed`. The default value is 4.

## Debug options

**The '~' prefix**

Lines prefixed with tildes ('~') will have this prefix removed and will be trimmed but will otherwise be left intact. This prefix can be used to visually separate debug code from real code.

Examples:

```
~foo          => foo
~~~ foo       => foo
~~~~~~~~foo   => foo
~~~~~~    foo => foo
```

**The '?' prefix**

Lines prefixed with '?' will only execute if `game1.dev.dev_mode` is true.

**chooseDebugUserInput, input -> [ output1, output2 ], ...**

Takes the most recent user input and converts it into a set of outputs and pushes those outputs to a set of pre-selected user inputs held in suspense.

The idea here is for cutscenes with many branch points where we want to test a specific combination of branch points but we don't want to have to choose the answer for every one of them. Instead we pre-select the options beforehand thus allowing us to fast-forward through the cutscene.

In the example below we ask the user to choose one of three options. Pressing 'B' will cause the next three user inputs to be automatically triggered with inputs of 1, 2 and 4 respectively.

```
? showText, @ref0, "Debug: Pick option A, B or C", 200, 900, 1000
? pollUserInput, KEY_A -> 1, KEY_B -> 2, KEY_C -> 3
? chooseDebugUserInput, 1 -> [ 1, 2, 3 ], 2 -> [ 1, 2, 4 ], 3 -> [ 1, 3, 4 ]
```

