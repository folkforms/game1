package game1.toolkit.cutscenes.internal;

import game1.actors.Actor;
import game1.actors.Tickable;
import game1.core.input.KeyboardFactory;

/**
 * @deprecated Old Cutscenes v5 code. Use C9 instead.
 */
@Deprecated
class WaitForKey implements Actor, Tickable {

  private int[] keyCodes;

  WaitForKey(int... keyCodes) {
    this.keyCodes = keyCodes;
  }

  @Override
  public void onTick() {
    for (int keyCode : keyCodes) {
      if (KeyboardFactory.getInstance().isKeyDown(keyCode)) {
        Cutscene.setWaiting(false);
        Cutscene.setUserInput(Integer.MIN_VALUE);
        // Engine.GAME_STATE.removeActor(this);
      }
    }
  }
}
