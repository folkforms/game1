package game1.toolkit.cutscenes.utils;

public interface CutsceneMoveable {

  public void startMoving(long duration, float newX, float newY);

  public void startMoving(long duration, float newX, float newY, float newZ);

  public void stopMoving();
}
