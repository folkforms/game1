package game1.toolkit.cutscenes.internal;

import java.util.ArrayList;
import java.util.List;

import org.joml.Vector3f;

import folkforms.log.Log;
import folkforms.stringutils.StringUtils;
import game1.actors.Actor;
import game1.actors.Moveable;
import game1.core.engine.Engine;
import game1.core.graphics.Animation;
import game1.datasets.GameDataFactory;
import game1.toolkit.cutscenes.effects.WipeUtils;
import game1.toolkit.widgets.label.Label;
import game1.toolkit.widgets.label.LabelBuilder;

/**
 * Converts data loaded by a {@link CutsceneDataLoader} into a series of events. Allows us to lazily
 * parse the cutscene data, because methods like {@link #fixWipe()} and {@link #fixShowText()}
 * create actors and put them in memory so we only want to action them immediately before a cutscene
 * plays.
 *
 * @deprecated Old Cutscenes v5 code. Use C9 instead.
 */
@Deprecated
class CutsceneParser {

  private List<String> contents;

  /**
   * A list of all actors added to {@link Engine#ACTOR_CACHE} by the parser. The player will
   * automatically remove them when it finishes playing.
   */
  private List<String> gameCacheRefs = new ArrayList<>();

  public CutsceneParser(CutsceneDataLoader loader) {
    try {
      // Make a copy of loader contents otherwise fixWipe etc. will make changes to the loader
      contents = new ArrayList<>();
      for (String s : loader.getOutput()) {
        contents.add(s);
      }

      fixWipe();
      fixShowText();
      fixCreateActor();
      fixWaitForAnimations();

    } catch (RuntimeException ex) {
      RuntimeException ex2 = new RuntimeException(
          String.format("Error parsing file '%s'", loader.getFilename()));
      ex2.initCause(ex);
      throw ex2;
    }
  }

  List<String> getOutput() {
    return contents;
  }

  /**
   * Converts a 'wipe' command into an 'add' command that adds a {@link WipeUtils} object and also
   * creates that object and adds it to {@link Engine#ACTOR_CACHE}.
   */
  private void fixWipe() {
    for (int i = 0; i < contents.size(); i++) {
      String line = contents.get(i);
      if (line.startsWith("wipe,")) {
        String[] tokens = line.split(", ");
        String ref = tokens[1];
        long duration = Long.parseLong(tokens[2]);
        int type = Integer.parseInt(tokens[3]);
        Log.temp("Need to create WipeSupplier for cutscenes");
        GameDataFactory.getInstance().addSupplier(ref, () -> WipeUtils.get(type, duration));
        gameCacheRefs.add(ref);
        String wipe = String.format("add, %s", ref);
        contents.set(i, wipe);
      }
    }
  }

  /**
   * Converts a 'showText' command into an 'add' command that adds a {@link Label} object and also
   * creates that object and adds it to {@link Engine#ACTOR_CACHE}.
   */
  private void fixShowText() {
    for (int i = 0; i < contents.size(); i++) {
      String line = contents.get(i);
      boolean isDebugLine = line.startsWith("?");
      if (isDebugLine) {
        line = line.substring(1).trim();
      }
      if (line.startsWith("showText,")) {
        String[] tokens = StringUtils.splitCSV(line, ", ");
        String ref = tokens[1];
        String text = tokens[2].substring(1, tokens[2].length() - 1);
        int x, y, z;
        if (tokens.length == 6) {
          // 5-arg version: showText, ref, text, x, y, z
          x = Integer.parseInt(tokens[3]);
          y = Integer.parseInt(tokens[4]);
          z = Integer.parseInt(tokens[5]);
        } else {
          // 3-arg version: showText, ref, text, speakerRef => x, y, z will be based on speakerRef
          String speakerRef = tokens[3];
          // FIXME DATASETS: This needs the exact item
          Moveable moveable = GameDataFactory.getInstance().get(speakerRef);
          Vector3f pos = moveable.getPosition();
          x = (int) pos.x;
          y = (int) pos.y + 160;
          z = (int) pos.z;
        }
        GameDataFactory.getInstance().addSupplier(ref,
            () -> LabelBuilder.init(text).setFontSize(5).setPosition(x, y, z).build());
        gameCacheRefs.add(ref);
        String addText = String.format("%sadd, %s   # \"%s\"", (isDebugLine ? "? " : ""), ref,
            text);
        contents.set(i, addText);
      }
    }
  }

  /**
   * Takes a 'createObject' command, creates the desired object and adds a reference to References.
   */
  private void fixCreateActor() {
    for (int i = 0; i < contents.size(); i++) {
      String line = contents.get(i);
      if (line.startsWith("createActor,")) {
        String[] tokens = StringUtils.splitCSV(line, ", ");
        String ref = tokens[1];
        Actor actor = CreateActor.create(tokens);
        GameDataFactory.getInstance().addSupplier(ref, () -> actor);
        gameCacheRefs.add(ref);
        contents.remove(i);
        i--;
      }
    }
  }

  /**
   * Convert a "waitForAnimation, &lt;reference&gt;, [&lt;count&gt;]" event into a "wait, X" event,
   * where X is the total ticks of the animation multiplied by <code>count</code>.
   */
  private void fixWaitForAnimations() {
    for (int i = 0; i < contents.size(); i++) {
      String line = contents.get(i);
      if (line.startsWith("waitForAnimation,")) {
        String[] tokens = StringUtils.splitCSV(line, ", ");
        String animationRef = tokens[1];
        int count = 1;
        if (tokens.length == 3) {
          count = Integer.parseInt(tokens[2]);
        }
        Animation animation = null;
        // FIXME DATASETS: This needs the exact item
        animation = GameDataFactory.getInstance().get(animationRef);
        if (animation == null) {
          throw new RuntimeException(String.format(
              "Failed to parse line '%s' as Engine.GAME_CACHE.get('%s') returned null", line,
              animationRef));
        }
        int waitTime = count * animation.getTotalTicks();
        String wait = String.format("wait, %s", waitTime);
        contents.set(i, wait);
      }
    }
  }

  List<String> getGameCacheRefs() {
    return gameCacheRefs;
  }
}
