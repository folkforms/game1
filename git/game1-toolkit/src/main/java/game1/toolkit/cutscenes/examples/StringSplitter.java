package game1.toolkit.cutscenes.examples;

class StringSplitter {

  /**
   * Splits a string into an array broken around '\n' chars, and adds chars one by one until
   * <code>numChars</code> chars have been added.
   *
   * @param text
   * @param numChars
   * @return
   */
  public static String[] split(String text, int numChars) {
    String[] lines = text.split("\n");
    int numLines = lines.length;
    int x = 0;
    int y = 0;

    // Create empty output array
    String[] output = new String[numLines];
    for (int i = 0; i < output.length; i++) {
      output[i] = "";
    }

    // Go through text and add chars one by one
    int numCharsAdded = 0;
    while (numCharsAdded < numChars) {
      output[y] += lines[y].substring(x, x + 1);
      x++;
      if (x >= lines[y].length()) {
        x = 0;
        y++;
        if (y >= lines.length) {
          break;
        }
      }
      numCharsAdded++;
    }

    return output;
  }
}
