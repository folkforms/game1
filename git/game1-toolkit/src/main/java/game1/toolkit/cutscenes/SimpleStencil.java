package game1.toolkit.cutscenes;

import game1.actors.Actor;
import game1.core.engine.Engine;
import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.datasets.GameDataFactory;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;

/**
 * A simple 'black bars' stencil. Everything outside the area will be overdrawn with the given
 * colour.
 */
public class SimpleStencil implements Actor {
  private Drawable top, right, bottom, left;
  private Colour debugColour = new Colour(1, 1, 1, 0.3f);

  public SimpleStencil(String resolutionLayer, String primitive2DRef, int x, int y, int z, int w,
      int h) {
    this(resolutionLayer, primitive2DRef, x, y, z, w, h, new Colour(0, 0, 0));
  }

  public SimpleStencil(String resolutionLayer, String primitive2DRef, int x, int y, int z, int w,
      int h, Colour colour) {
    boolean debugStencil = Engine.PROPERTIES.getBooleanProperty("game1.dev_mode.show_stencils");
    Drawable blackBar = GameDataFactory.getInstance().get(primitive2DRef);
    ResolutionHints hints = ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayer)
        .getResolutionHints();
    float scale = hints.getResolutionScale();
    float width = hints.getResolutionWidth() * scale;
    float height = hints.getResolutionHeight() * scale;
    top = blackBar.deepClone();
    top.setPosition(0, y + h, z);
    top.resize(width, height - h - y);
    top.setColour(debugStencil ? debugColour : colour);
    addChild(top);
    right = blackBar.deepClone();
    right.setPosition(x + w, y, z);
    right.resize(width - w - x, h);
    right.setColour(debugStencil ? debugColour : colour);
    addChild(right);
    bottom = blackBar.deepClone();
    bottom.setPosition(0, 0, z);
    bottom.resize(width, y);
    bottom.setColour(debugStencil ? debugColour : colour);
    addChild(bottom);
    left = blackBar.deepClone();
    left.setPosition(0, y, z);
    left.resize(x, h);
    left.setColour(debugStencil ? debugColour : colour);
    addChild(left);
  }
}
