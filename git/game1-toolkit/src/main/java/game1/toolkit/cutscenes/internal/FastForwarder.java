package game1.toolkit.cutscenes.internal;

import game1.actors.Actor;
import game1.actors.Tickable;

/**
 * @deprecated Old Cutscenes v5 code. Use C9 instead.
 */
@Deprecated
class FastForwarder implements Actor, Tickable {

  private long numTicks = 0;
  private long lastTickUpdated = 0;
  private long delta = 5;

  @Override
  public void onTick() {
    numTicks++;
    if (numTicks - delta > lastTickUpdated) {
      Cutscene.setFastForwarding(false);
    }
  }

  public void update() {
    lastTickUpdated = numTicks;
  }
}
