package game1.toolkit.cutscenes.effects;

import folkforms.maths.Graph;
import game1.actors.Actor;
import game1.actors.Tickable;
import game1.core.engine.Engine;
import game1.core.graphics.Drawable;
import game1.core.pipelines.GraphicsPipelineName;
import game1.core.resources.builders.PrimitiveBuilder;
import game1.resolutions.layers.ResolutionLayers;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;
import game1.toolkit.cutscenes.internal.Cutscene;

class WipeOut implements Actor, Tickable {

  private long numTicks = 0;
  private long duration;
  private int x = 0;
  protected float[] xValues;
  private float[] timePercents = { 0, 1 };
  private Drawable drawable;

  public WipeOut(long duration) {
    this.duration = duration;
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(ResolutionLayers.WINDOW_LAYER).getResolutionHints();
    float scale = hints.getResolutionScale();
    float width = hints.getResolutionWidth() * scale;
    float height = hints.getResolutionHeight() * scale;
    xValues = new float[] { 0 - width, 0 };
    drawable = PrimitiveBuilder.init(GraphicsPipelineName.SCENE_2D).rect2d(width, height)
        .position(0 - width, 0, Engine.PROPERTIES.getIntProperty("cutscene.wipe.zindex"))
        .colour(0, 0, 0).build();
    addChild(drawable);
  }

  @Override
  public void onTick() {
    x = (int) Graph.calculateValue(timePercents, xValues, numTicks, duration);
    numTicks++;
    drawable.setPosition(x, 0);
    if (Cutscene.isFastForwarding()) {
      numTicks += Cutscene.FAST_FORWARD_SPEED;
    }
  }
}
