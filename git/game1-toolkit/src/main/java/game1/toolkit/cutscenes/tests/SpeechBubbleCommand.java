package game1.toolkit.cutscenes.tests;

import game1.actors.Actor;
import game1.core.engine.Engine;
import game1.core.graphics.FontUtils;
import game1.resolutions.layers.ResolutionLayers;
import game1.toolkit.cutscenes.CutsceneCommand;
import game1.toolkit.cutscenes.examples.SpeechBubble;
import game1.toolkit.internal.main.ToolkitZIndex;

/**
 * @deprecated Old Cutscenes v5 code. Use C9 instead.
 */
@Deprecated
public class SpeechBubbleCommand implements CutsceneCommand {

  @Override
  public void execute(String[] tokens) {
    // user, speechBubble, @ref, henchard, "Susan? Susan!", true

    String speechBubbleRef = tokens[2];
    String speakerRef = tokens[3];
    String text = tokens[4];
    text = text.replace("\"", "");
    boolean leftTail = Boolean.parseBoolean(tokens[5]);

    Actor speechBubble = new SpeechBubble(speakerRef, text, leftTail, ToolkitZIndex.CUTSCENE_TEXT,
        FontUtils.DEFAULT_FONT_FACE, 4) {
      // @Override
      // public void postRemove() {
      // TempActors.remove(speechBubbleRef);
      // }
    };

    Engine.GAME_STATE.addActor(speechBubble, ResolutionLayers.WINDOW_LAYER);
  }
}
