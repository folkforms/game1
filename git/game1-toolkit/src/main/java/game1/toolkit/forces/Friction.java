package game1.toolkit.forces;

import org.joml.Vector3f;

import game1.actors.Tickable;
import game1.actors.Velocity;
import game1.core.engine.Engine;

/**
 * Applies friction to a given {@link Velocity}.
 *
 * <p>
 * Friction can be applied to the object during the {@link Tickable#onTick()} method. For example:
 * </p>
 *
 * <pre>
 * public void onTick() {
 *   numTicks++;
 *   if (!onSolidGround) {
 *     Gravity.apply(this, numTicks);
 *   } else {
 *     Friction.apply(this, numTicks);
 *   }
 * }
 * </pre>
 */
public class Friction {

  public static int FRICTION_TICK_RATE, FRICTION_FORCE;

  static {
    FRICTION_TICK_RATE = Engine.PROPERTIES.getIntProperty("friction.tick.rate");
    FRICTION_FORCE = Engine.PROPERTIES.getIntProperty("friction.force");
  }

  public static void apply(Velocity v, long tick) {
    if (tick % FRICTION_TICK_RATE == 0) {
      Vector3f speed = v.getSpeed();
      float speedX = speed.x;
      if (speedX > 0) {
        speedX -= FRICTION_FORCE;
        if (speedX < 0) {
          // If the subtraction above took us into negative numbers don't
          // start going backwards!
          speedX = 0;
        }
      }
      if (speedX < 0) {
        speedX += FRICTION_FORCE;
        if (speedX > 0) {
          speedX = 0;
        }
      }
      v.setSpeed(speedX, speed.y);
    }
  }
}
