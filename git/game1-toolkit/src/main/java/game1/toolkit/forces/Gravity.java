package game1.toolkit.forces;

import org.joml.Vector3f;

import game1.actors.Tickable;
import game1.actors.Velocity;
import game1.core.engine.Engine;

/**
 * Applies gravity to a given {@link Velocity}.
 *
 * <p>
 * Gravity can be applied to the object during the {@link Tickable#onTick()} method. For example:
 * </p>
 *
 * <pre>
 * public void onTick() {
 *   numTicks++;
 *   if (!onSolidGround) {
 *     Gravity.apply(this, numTicks);
 *   } else {
 *     Friction.apply(this, numTicks);
 *   }
 * }
 * </pre>
 */
public class Gravity {

  public static int GRAVITY_TICK_RATE, GRAVITY_FORCE, GRAVITY_MAX_FORCE;

  static {
    GRAVITY_TICK_RATE = Engine.PROPERTIES.getIntProperty("gravity.tick.rate");
    GRAVITY_FORCE = Engine.PROPERTIES.getIntProperty("gravity.force");
    GRAVITY_MAX_FORCE = Engine.PROPERTIES.getIntProperty("gravity.max.force");
  }

  public static void apply(Velocity v, long tick) {
    if (tick % GRAVITY_TICK_RATE == 0) {
      Vector3f speed = v.getSpeed();
      float newSpeedY = speed.y + GRAVITY_FORCE;
      if (newSpeedY < GRAVITY_MAX_FORCE) {
        newSpeedY = GRAVITY_MAX_FORCE;
      }
      v.setSpeed(speed.x, newSpeedY);
    }
  }
}
