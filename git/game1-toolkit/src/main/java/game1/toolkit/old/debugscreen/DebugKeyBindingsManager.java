package game1.toolkit.old.debugscreen;

import java.util.List;

import game1.actors.Actor;
import game1.actors.Tickable;
import game1.core.engine.Engine;

/**
 * Displays the currently active sets of keybindings, as well as any stashed keybindings.
 *
 * @deprecated This is not currently used, but it's a good idea. I may fix it up.
 */
@Deprecated
public class DebugKeyBindingsManager implements Actor, Tickable {

  /**
   * Allow this tool to always be visible, because sometimes we need to debug how opening and
   * closing the dev tools affects the keybindings.
   */
  private boolean ALWAYS_VISIBLE = Engine.PROPERTIES
      .getBooleanProperty("toolkit.debug_key_bindings_manager.always_visible");

  private int x, y;
  private String title = "KeyBindings";
  private String stashText = "";
  private String activeText = "";

  public DebugKeyBindingsManager(int x, int y) {
    this.x = x;
    this.y = y;
  }

  @Override
  public void onTick() {
    List<String> stash = Engine.KEY_BINDINGS.debug_listStash();
    List<String> active = Engine.KEY_BINDINGS.debug_listActiveNames();
    stashText = String.format("  stash: %s", stash.toString());
    activeText = String.format("  active: %s", active.toString());
  }

  // @Override
  // public void draw(Graphics g) {
  // if (Engine.DEV_TOOLS_VISIBLE || ALWAYS_VISIBLE) {
  // g.setColour(1, 1, 1);
  // g.setFont(FontUtils.DEFAULT_FONT_FACE, 3);
  // g.drawString(title, x, y);
  // g.drawString(stashText, x, y - 20);
  // g.drawString(activeText, x, y - 40);
  // }
  // }
  //
  // @Override
  // public int getZ() {
  // return Integer.MAX_VALUE;
  // }

  public void setAlwaysVisible(boolean b) {
    ALWAYS_VISIBLE = b;
  }
}
