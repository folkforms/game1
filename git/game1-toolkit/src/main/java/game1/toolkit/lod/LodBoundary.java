package game1.toolkit.lod;

import org.joml.Vector3f;

import game1.actors.Actor;
import game1.actors.Moveable;
import game1.actors.Tickable;
import game1.core.graphics.Drawable;
import game1.datasets.GameDataFactory;

/**
 * A simple LOD boundary that flips the visibility of two items as we get closer or move farther
 * away. There are two boundary lines to avoid rapid flipping between the two states if we are
 * treading along a boundary line.
 */
public class LodBoundary implements Actor, Tickable {

  private Moveable moveable;
  private float x, y, z;
  private float boundary1, boundary2;
  private Drawable lowLodActor, highLodActor;
  private boolean usingLowLod = true;

  /**
   * Creates a new {@link LodBoundary} object. boundaryLowToHigh should be less than
   * boundaryHighToLow.
   *
   * @param x
   *          x position
   * @param y
   *          y position
   * @param z
   *          z position
   * @param boundaryLowToHigh
   *          the boundary that flips from low to high
   * @param boundaryHighToLow
   *          the boundary that flips from high to low
   * @param lowLodAssetRef
   *          low LOD asset ref
   * @param highLodAssetRef
   *          high LOD asset ref
   */
  public LodBoundary(float x, float y, float z, float boundaryLowToHigh, float boundaryHighToLow,
      String lowLodAssetRef, String highLodAssetRef) {
    this.x = x;
    this.y = y;
    this.z = z;
    this.boundary1 = boundaryLowToHigh;
    this.boundary2 = boundaryHighToLow;
    this.lowLodActor = GameDataFactory.getInstance().get(lowLodAssetRef);
    this.highLodActor = GameDataFactory.getInstance().get(highLodAssetRef);
    lowLodActor.setVisible(true);
    highLodActor.setVisible(false);
    addChild(lowLodActor);
    addChild(highLodActor);
  }

  public void setMoveable(Moveable moveable) {
    this.moveable = moveable;
  }

  @Override
  public void onTick() {
    if (moveable != null) {
      Vector3f moveablePos = moveable.getPosition();
      float x2 = moveablePos.x;
      float y2 = moveablePos.y;
      float z2 = moveablePos.z;
      float distance = (float) Math
          .sqrt((x2 - x) * (x2 - x) + (y2 - y) * (y2 - y) + (z2 - z) * (z2 - z));
      if (distance < boundary1 && usingLowLod) {
        lowLodActor.setVisible(false);
        highLodActor.setVisible(true);
        usingLowLod = false;
      } else if (distance >= boundary2 && !usingLowLod) {
        highLodActor.setVisible(false);
        lowLodActor.setVisible(true);
        usingLowLod = true;
      }
    }
  }
}
