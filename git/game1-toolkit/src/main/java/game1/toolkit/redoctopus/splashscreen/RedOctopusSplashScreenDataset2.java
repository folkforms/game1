package game1.toolkit.redoctopus.splashscreen;

import game1.core.engine.Engine;

public class RedOctopusSplashScreenDataset2 {

  public static final String TAG = "RedOctopusSplashScreenTag";

  public RedOctopusSplashScreenDataset2() {
    loadSplashScreenImage();
    loadSplashScreenSound();
  }

  private void loadSplashScreenImage() {
    String path = "src/main/resources/packed/red-octopus.spritesheet";
    Engine.GAME_FILES.register(path, TAG);
  }

  private void loadSplashScreenSound() {
    Engine.GAME_FILES.register("src/main/resources/resources/red_octopus/octopus.ogg", TAG);
  }
}
