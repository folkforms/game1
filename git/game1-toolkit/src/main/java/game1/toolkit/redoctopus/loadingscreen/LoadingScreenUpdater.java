package game1.toolkit.redoctopus.loadingscreen;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import game1.actors.Actor;
import game1.actors.Tickable;
import game1.core.engine.EngineInternal;
import game1.core.registries.TickableAnnotations.TickSlowly;
import game1.toolkit.widgets.label.Label;
import game1.toolkit.widgets.progress.ProgressBar;

public class LoadingScreenUpdater implements Actor, Tickable {
  private Label loadingInfoLabel;
  private ProgressBar progressBar;
  private List<String> allTags;

  public LoadingScreenUpdater(Label loadingInfoLabel, ProgressBar progressBar,
      List<String> tagsToLoad, List<String> requiredTags) {
    this.loadingInfoLabel = loadingInfoLabel;
    this.progressBar = progressBar;

    Set<String> allTags2 = new HashSet<>(tagsToLoad);
    allTags2.addAll(requiredTags);
    allTags = allTags2.stream().toList();

    updateProgressBar();
  }

  @Override
  @TickSlowly
  public void onTick() {
    updateProgressBar();
  }

  private void updateProgressBar() {
    int numerator = EngineInternal.GAME_FILES.internal_getNumLoadedGameFiles(allTags);
    int denominator = EngineInternal.GAME_FILES.internal_getNumGameFiles(allTags);
    loadingInfoLabel.setText(String.format("Loading %s of %s", numerator, denominator));
    progressBar.setCurrentValue(numerator);
    progressBar.setMaxValue(denominator);
  }
}
