package game1.toolkit.redoctopus.splashscreen;

import java.util.List;

import org.joml.Vector2i;

import game1.core.engine.Engine;
import game1.core.engine.userstate.UserState;
import game1.core.engine.userstate.UserStateHelper;
import game1.core.graphics.Image;
import game1.core.input.Mouse;
import game1.core.states.SplashScreenChecker;
import game1.datasets.GameDataFactory;
import game1.resolutions.layers.ResolutionLayer;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.scaling.ResolutionScalingStrategyV1;
import game1.resolutions.shared.ResolutionHints;
import game1.toolkit.cutscenes.effects.FadeInFadeOut;
import game1.toolkit.redoctopus.loadingscreen.RedOctopusLoadingScreenDataset2;
import game1.toolkit.redoctopus.loadingscreen.RedOctopusLoadingScreenState2;

// FIXME RESOLUTIONS: This should be the minimum resolution the splash screen can possibly fit
public class RedOctopusSplashScreenState2 extends UserState {

  public static final String RED_OCTOPUS_SPLASH_SCREEN_STATE = "RED_OCTOPUS_SPLASH_SCREEN_STATE";

  private String nextState;

  public RedOctopusSplashScreenState2(String nextState) {
    this.nextState = nextState;
    UserStateHelper.setDoNotUnloadDatasets(this, true);
  }

  public RedOctopusSplashScreenState2() {
    this(RedOctopusLoadingScreenState2.RED_OCTOPUS_LOADING_SCREEN_STATE);
  }

  @Override
  public void apply() {
    String layerName = "splash-screen-layer";
    ResolutionLayer resolutionLayer = ResolutionLayersFactory.getInstance()
        .createStaticLayer(layerName, new Vector2i(1920, 1080), new ResolutionScalingStrategyV1());
    ResolutionHints hints = resolutionLayer.getResolutionHints();

    UserStateHelper.setMouse(Mouse.MOUSE_OFF);
    Engine.GAME_STATE.addActor(new FadeInFadeOut(200, 150, 150), layerName);
    Image tentacles = GameDataFactory.getInstance().get("red_octopus_logo.png");
    tentacles.setPosition(0, hints.getResolutionHeight() / 2 - tentacles.getHeight() / 2);
    Engine.GAME_STATE.addActor(tentacles, layerName);
    Engine.GAME_STATE.addActor(
        RedOctopusLogoText.create(hints.getResolutionWidth(), hints.getResolutionHeight()),
        layerName);
    Engine.SOUND_BOARD.play("src/main/resources/resources/red_octopus/octopus.ogg",
        "playing_splash_screen_sound");
    Engine.GAME_STATE.addActor(new SplashScreenChecker(nextState, "playing_splash_screen_sound"),
        layerName);
  }

  @Override
  public List<String> listRequiredTags() {
    return List.of(RedOctopusSplashScreenDataset2.TAG, RedOctopusLoadingScreenDataset2.TAG);
  }
}
