package game1.toolkit.redoctopus.loadingscreen;

import java.util.List;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import game1.core.engine.Engine;
import game1.core.states.LoadingScreenState2;
import game1.datasets.GameDataFactory;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.scaling.ResolutionScalingStrategyV2;
import game1.toolkit.widgets.label.Label;
import game1.toolkit.widgets.label.LabelBuilder;
import game1.toolkit.widgets.progress.ProgressBar;
import game1.toolkit.widgets.progress.ProgressBarBuilder;

public class RedOctopusLoadingScreenState2 extends LoadingScreenState2 {

  public static final String RED_OCTOPUS_LOADING_SCREEN_STATE = "RED_OCTOPUS_LOADING_SCREEN_STATE";

  public RedOctopusLoadingScreenState2(List<String> tagsToLoad, String nextState) {
    super(tagsToLoad, nextState);
  }

  @Override
  public void apply() {
    String layerName = "loading-screen-layer";
    ResolutionLayersFactory.getInstance().createStaticLayer(layerName, new Vector2i(1920, 1080),
        new ResolutionScalingStrategyV2());

    Engine.GAME_STATE.addActor(GameDataFactory.getInstance().get("red_octopus_loading.png"),
        layerName);
    Label loadingInfoLabel = LabelBuilder.init("").setPosition(100, 750, 0).setFontSize(8)
        .setModifiable(true).build();
    Engine.GAME_STATE.addActor(loadingInfoLabel, layerName);
    ProgressBar progressBar = ProgressBarBuilder.init().position(new Vector3f(460, 500, 1000))
        .size(new Vector2f(1000, 50)).build();
    Engine.GAME_STATE.addActor(progressBar, layerName);
    Engine.GAME_STATE.addActor(
        new LoadingScreenUpdater(loadingInfoLabel, progressBar, tagsToLoad, listRequiredTags()),
        layerName);
  }

  @Override
  public List<String> listRequiredTags() {
    return List.of(RedOctopusLoadingScreenDataset2.TAG);
  }
}
