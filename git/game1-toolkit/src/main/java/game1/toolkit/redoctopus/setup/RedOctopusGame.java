package game1.toolkit.redoctopus.setup;

import java.io.IOException;
import java.util.List;

import game1.core.engine.Engine;
import game1.toolkit.redoctopus.loadingscreen.RedOctopusLoadingScreenDataset2;
import game1.toolkit.redoctopus.loadingscreen.RedOctopusLoadingScreenState2;
import game1.toolkit.redoctopus.splashscreen.RedOctopusSplashScreenDataset2;
import game1.toolkit.redoctopus.splashscreen.RedOctopusSplashScreenState2;

/**
 * Convenience method that loads the Red Octopus splash screen and loading screen datasets and
 * states, registers the typical sound channels "effects" and "music", and starts the game.
 */
public class RedOctopusGame {

  public static void start(Engine engine, List<String> tagsToLoadDuringLoadingScreen,
      String stateAfterLoading) throws IOException {
    new RedOctopusSplashScreenDataset2();
    new RedOctopusLoadingScreenDataset2();
    Engine.GAME_STATE.register(RedOctopusSplashScreenState2.RED_OCTOPUS_SPLASH_SCREEN_STATE,
        new RedOctopusSplashScreenState2());
    Engine.GAME_STATE.register(RedOctopusLoadingScreenState2.RED_OCTOPUS_LOADING_SCREEN_STATE,
        new RedOctopusLoadingScreenState2(tagsToLoadDuringLoadingScreen, stateAfterLoading));

    Engine.MIXER.registerChannel("effects");
    Engine.MIXER.registerChannel("music");

    engine.start(RedOctopusSplashScreenState2.RED_OCTOPUS_SPLASH_SCREEN_STATE);
  }
}
