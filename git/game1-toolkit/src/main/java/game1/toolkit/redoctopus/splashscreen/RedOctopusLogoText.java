package game1.toolkit.redoctopus.splashscreen;

import game1.actors.Actor;
import game1.core.graphics.Colour;
import game1.core.graphics.FontUtils;
import game1.toolkit.widgets.label.Label;
import game1.toolkit.widgets.label.LabelBuilder;

public class RedOctopusLogoText implements Actor {

  private Label[] labels;

  public RedOctopusLogoText(Label main, Label border1, Label border2, Label border3,
      Label border4) {
    labels = new Label[5];
    labels[0] = main;
    labels[1] = border1;
    labels[2] = border2;
    labels[3] = border3;
    labels[4] = border4;
    addChild(main);
    addChild(border1);
    addChild(border2);
    addChild(border3);
    addChild(border4);
  }

  protected static RedOctopusLogoText create(float w, float h) {
    int border = 5;
    Label main = createLabel(w, h, false, 0, 0, 0);
    Label border1 = createLabel(w, h, true, -border, -border, -1);
    Label border2 = createLabel(w, h, true, border, -border, -2);
    Label border3 = createLabel(w, h, true, -border, border, -3);
    Label border4 = createLabel(w, h, true, border, border, -4);
    return new RedOctopusLogoText(main, border1, border2, border3, border4);
  }

  private static Label createLabel(float w, float h, boolean border, int xMod, int yMod, int zMod) {
    String text = "Red Octopus Games";
    int strWidth = FontUtils.getStringWidth(text, FontUtils.DEFAULT_FONT_FACE, 12);
    int strHeight = FontUtils.getStringHeight(FontUtils.DEFAULT_FONT_FACE, 12);

    float textX = w / 2 - strWidth / 2;
    float textY = h / 2 - strHeight / 2;

    Colour mainColour = Colour.fromHex("#fff");
    Colour borderColour = Colour.fromHex("#000");

    Label logoText = LabelBuilder.init(text).setFontSize(12)
        .setPosition(textX + xMod, textY + yMod, 1000 + zMod).build();
    logoText.setColour(border ? borderColour : mainColour);
    return logoText;
  }
}
