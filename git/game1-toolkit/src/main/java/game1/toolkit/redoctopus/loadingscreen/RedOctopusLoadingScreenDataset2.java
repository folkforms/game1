package game1.toolkit.redoctopus.loadingscreen;

import game1.core.engine.Engine;

public class RedOctopusLoadingScreenDataset2 {

  public static final String TAG = "RedOctopusLoadingScreenTag";

  public RedOctopusLoadingScreenDataset2() {
    Engine.GAME_FILES.register("src/main/resources/packed/red-octopus.spritesheet", TAG);
  }
}
