package game1.toolkit.screens.rebindkeys;

import game1.core.engine.Engine;
import game1.events.Command;

/**
 * Changes the state when executed.
 */
public class CancelRebindMenuCommand implements Command {

  private String state;

  /**
   * Creates a new CancelRebindMenuCommand object that will change to <code>state</code> when
   * executed.
   * 
   * @param state
   *          the state to change to
   */
  public CancelRebindMenuCommand(String state) {
    this.state = state;
  }

  @Override
  public void execute() {
    RebindCoordinator.clear();
    Engine.GAME_STATE.moveToState(state);
  }
}
