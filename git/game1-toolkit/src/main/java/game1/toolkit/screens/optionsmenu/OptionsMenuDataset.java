package game1.toolkit.screens.optionsmenu;

import game1.core.engine.Engine;
import game1.core.graphics.Colour;
import game1.core.graphics.FontUtils;
import game1.core.graphics.RenderedFont;
import game1.datasets.GameDataFactory;

public class OptionsMenuDataset {

  public static final String TAG = "OptionsMenuDataset";
  public static final String OPTIONS_MENU_REF = "options-menu";

  public static OptionsMenuDataset build(int z) {
    return new OptionsMenuDataset(z);
  }

  public static OptionsMenuDataset build(int z, String title, String font, float scale, int w,
      int h, Colour backgroundColour, int volumeSliderVerticalBoost, int checkboxVerticalBoost) {
    return new OptionsMenuDataset(z, title, font, scale, w, h, backgroundColour,
        volumeSliderVerticalBoost, checkboxVerticalBoost);
  }

  private OptionsMenuDataset(int z) {
    this(z, "Options", null, 5, 900, 420, new Colour(0.2f, 0, 0), 20, 10);
  }

  private OptionsMenuDataset(int z, String title, String fontName, float scale, int w, int h,
      Colour backgroundColour, int volumeSliderVerticalBoost, int checkboxVerticalBoost) {
    Engine.GAME_FILES.register("src/main/resources/packed/toolkit.spritesheet", TAG);
    GameDataFactory.getInstance().addSupplier(OPTIONS_MENU_REF, () -> {
      RenderedFont font = FontUtils.DEFAULT_FONT_FACE;
      if (fontName != null) {
        font = GameDataFactory.getInstance().get(fontName);
      }
      return new OptionsMenu(title, font, scale, w, h, backgroundColour, volumeSliderVerticalBoost,
          checkboxVerticalBoost) {
        @Override
        public int getZ() {
          return z;
        }
      };
    });
  }
}
