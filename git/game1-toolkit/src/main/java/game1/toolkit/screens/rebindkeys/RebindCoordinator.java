package game1.toolkit.screens.rebindkeys;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import folkforms.log.Log;
import game1.core.engine.Engine;

/**
 * Stores details of a pending rebind, which will be actioned when the user leaves the
 * {@link RebindKeysMenu}.
 */
public class RebindCoordinator {

  private static Map<String, Integer> rebinds = new HashMap<>();

  private static String inProgressAction;
  private static RebindButton inProgressRebindButton;
  private static boolean rebindingInProgress = false;

  private RebindCoordinator() {
  }

  /**
   * Clear any pending rebinds. Should be called when we action the rebinds or when we exit the
   * rebind controls page without saving.
   */
  public static void clear() {
    rebinds.clear();
  }

  public static void startRebind(String action, RebindButton rebindButton) {
    inProgressAction = action;
    inProgressRebindButton = rebindButton;
    inProgressRebindButton.setRebindInProgress(true);
    rebindingInProgress = true;
  }

  public static boolean rebindingInProgress() {
    return rebindingInProgress;
  }

  public static void endRebind(int newKeyCode) {
    rebinds.put(inProgressAction, newKeyCode);
    inProgressRebindButton.setRebindInProgress(false);
    // FIXME MISC_STUFF: Maybe add a "getText" or "getDisplayName" method to Keys class
    // inProgressRebindButton.setText(KeyCodeToText.getText(newKeyCode));
    rebindingInProgress = false;
  }

  public static void processPendingRebinds() {
    Iterator<String> iterator = rebinds.keySet().iterator();
    while (iterator.hasNext()) {
      String key = iterator.next();
      try {
        Engine.PROPERTIES.saveProperty(key, rebinds.get(key));
      } catch (IOException ex) {
        Log.error(ex);
      }
    }
  }
}
