package game1.toolkit.screens.menusystem.testmenus;

import game1.actors.Clickable;
import game1.core.graphics.Drawable;
import game1.core.input.Mouse;
import game1.datasets.GameDataFactory;
import game1.primitives.Rectangle;
import game1.primitives.Shape;
import game1.toolkit.internal.main.ToolkitZIndex;
import game1.toolkit.widgets.menu.Menu;

public class MenuC implements Menu, Clickable {

  private Rectangle shape = new Rectangle(650, 890, 100, 100);
  private boolean active;
  protected boolean isResolutionAffected = true;
  protected boolean isViewportAffected = false;
  private Menu menuD;

  public MenuC(MenuD menuD) {
    Drawable drawable = GameDataFactory.getInstance().get("MenuC.png");
    drawable.setPosition(650, 890, ToolkitZIndex.MENU_C);
    addChild(drawable);
    this.menuD = menuD;
    setActive(false);
    setVisible(false);
  }

  @Override
  public int getZ() {
    return ToolkitZIndex.MENU_C;
  }

  @Override
  public Shape<Rectangle> getShape() {
    return shape;
  }

  @Override
  public void onClick(int button, float windowX, float windowY) {
    if (button == Mouse.LEFT_BUTTON) {
      menuD.setActive(true);
      menuD.setVisible(true);
    }
    if (button == Mouse.RIGHT_BUTTON) {
      this.setActive(false);
      this.setVisible(false);
    }
  }

  @Override
  public boolean isActive() {
    return active;
  }

  @Override
  public void setActive(boolean b) {
    active = b;
  }
}
