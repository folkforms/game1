package game1.toolkit.screens.rebindkeys;

import game1.core.engine.Engine;
import game1.events.Command;

public class StartRebindCommand implements Command {

  private String action;
  private RebindButton rebindButton;

  public StartRebindCommand(String action, RebindButton rb) {
    this.action = action;
    this.rebindButton = rb;
  }

  @Override
  public void execute() {
    Engine.KEY_BINDINGS.stashSave();
    Engine.KEY_BINDINGS.activate("rebind");
    RebindCoordinator.startRebind(action, rebindButton);
  }
}
