package game1.toolkit.screens.debug;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import game1.core.engine.Engine;
import game1.core.engine.userstate.UserState;
import game1.core.input.MouseFactory;
import game1.core.sound.SoundEffect;

/**
 * Creates a screen with buttons to play all the sound assets. Used to compare sound effect volume
 * levels in a game.
 *
 * <p>
 * <ul>
 * <li>FIXME Needs a way to alter the volume of each sound</li>
 * <li>FIXME Needs a way to save that volume to disk</li>
 * </ul>
 * </p>
 */
public class TestSoundsState extends UserState {

  @Override
  public void apply() {
    MouseFactory.getInstance().setVisible(true);
    Engine.KEY_BINDINGS.activate("toggle-main-menu");
    Engine.KEY_BINDINGS.activate("debug-move-asset");

    // FIXME SOUNDS: Need some way of iterating through TempActors and grabbing all the SoundEffects
    Map<String, SoundEffect> soundAssets = new HashMap<>(); // Assets.debug_listSoundAssets();
    List<String> assets = soundAssets.keySet().stream().sorted().collect(Collectors.toList());

    // Grid grid = new Grid(50, 50, 840, 980, 28, 4);
    // for (String asset : assets) {
    // Vector2i p = grid.getNextCell();
    // createPlaySoundButton(asset, p.x, p.y);
    // }
  }

  private void createPlaySoundButton(String asset, int x, int y) {
    // SoundEffect soundEffect = (SoundEffect) TempActors.get(asset);
    //
    // Slider slider = new Slider(x + 210, y, 0, "toolkit.slider_hbar", "toolkit.slider_knob");
    // slider.setValue((int) soundEffect.debug_getVolume() * 100);
    // Engine.GAME_STATE.addActor(slider);
    //
    // Button playSoundButton = new Button(asset, 2, x, y, 0, 200, 30) {
    // @Override
    // public void onClick(int button, float mouseX, float mouseY) {
    // if (currentState != STATE_DISABLED && button == Mouse.LEFT_BUTTON) {
    // float volume = (slider.getValue()) / 100;
    // String ref = String.format("%s_%s", asset, System.currentTimeMillis());
    // Engine.SOUND_BOARD.play(asset, ref, volume);
    // }
    // }
    // };
    // Engine.GAME_STATE.addActor(playSoundButton);
  }

  @Override
  public List<String> listRequiredTags() {
    return List.of();
  }
}
