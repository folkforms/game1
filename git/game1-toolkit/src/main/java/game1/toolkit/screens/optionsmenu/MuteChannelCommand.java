package game1.toolkit.screens.optionsmenu;

import java.io.IOException;

import folkforms.log.Log;
import game1.core.engine.Engine;
import game1.events.Command;

public class MuteChannelCommand implements Command {

  private String channel;

  public MuteChannelCommand(String channel) {
    this.channel = channel;
  }

  @Override
  public void execute() {
    boolean newValue = !Engine.MIXER.getMuted(channel);
    Engine.MIXER.setMuted(channel, newValue);
    try {
      Engine.PROPERTIES.saveProperty(Engine.MIXER.getChannel(channel).getMutedPropertyName(),
          newValue);
    } catch (IOException ex) {
      Log.error(ex);
    }
  }
}
