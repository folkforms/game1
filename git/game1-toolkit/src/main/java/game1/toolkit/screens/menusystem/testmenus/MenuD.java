package game1.toolkit.screens.menusystem.testmenus;

import game1.actors.Clickable;
import game1.core.graphics.Drawable;
import game1.core.input.Mouse;
import game1.datasets.GameDataFactory;
import game1.primitives.Rectangle;
import game1.primitives.Shape;
import game1.toolkit.internal.main.ToolkitZIndex;
import game1.toolkit.widgets.menu.Menu;

public class MenuD implements Menu, Clickable {

  private Rectangle shape = new Rectangle(700, 940, 100, 100);
  private boolean active;
  protected boolean isResolutionAffected = true;
  protected boolean isViewportAffected = false;

  public MenuD() {
    Drawable drawable = GameDataFactory.getInstance().get("MenuD.png");
    drawable.setPosition(700, 940, ToolkitZIndex.MENU_D);
    addChild(drawable);
    setActive(false);
    setVisible(false);
  }

  @Override
  public int getZ() {
    return ToolkitZIndex.MENU_D;
  }

  @Override
  public Shape<Rectangle> getShape() {
    return shape;
  }

  @Override
  public void onClick(int button, float windowX, float windowY) {
    if (button == Mouse.RIGHT_BUTTON) {
      this.setActive(false);
      this.setVisible(false);
    }
  }

  @Override
  public boolean isActive() {
    return active;
  }

  @Override
  public void setActive(boolean b) {
    active = b;
  }
}
