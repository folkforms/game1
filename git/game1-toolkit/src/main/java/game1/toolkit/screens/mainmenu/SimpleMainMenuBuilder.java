package game1.toolkit.screens.mainmenu;

import java.util.LinkedHashMap;

/**
 * Builder class for {@link SimpleMainMenu}.
 */
public class SimpleMainMenuBuilder {

  private LinkedHashMap<String, String> buttons = new LinkedHashMap<>();
  private LinkedHashMap<String, String> properties = new LinkedHashMap<>();
  private String resolutionLayer;

  private SimpleMainMenuBuilder() {
  }

  public static SimpleMainMenuBuilder init() {
    return new SimpleMainMenuBuilder();
  }

  /**
   * Adds a button to the {@link SimpleMainMenu} with label <code>text</code> that will move to the
   * named state when clicked.
   *
   * @param text
   *          button text
   * @param state
   *          state the button will move to when clicked
   */
  public SimpleMainMenuBuilder addButton(String text, String state) {
    buttons.put(text, state);
    return this;
  }

  /**
   * Sets a property of the menu, for example <code>z</code> value.
   *
   * @param key
   *          key
   * @param value
   *          value
   */
  public SimpleMainMenuBuilder setProperty(String key, String value) {
    properties.put(key, value);
    return this;
  }

  public SimpleMainMenuBuilder setResolutionLayer(String resolutionLayer) {
    this.resolutionLayer = resolutionLayer;
    return this;
  }

  /**
   * Creates a {@link SimpleMainMenu} from the data that was previously added.
   *
   * @return a {@link SimpleMainMenu} object
   */
  public SimpleMainMenu build() {
    return SimpleMainMenu.build(resolutionLayer, properties, buttons);
  }
}
