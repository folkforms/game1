package game1.toolkit.screens.optionsmenu;

import java.io.IOException;

import folkforms.log.Log;
import game1.core.engine.Engine;
import game1.events.Command;
import game1.toolkit.widgets.checkbox.Checkbox;

public class SetChannelMuteCommand implements Command {

  private String channel;
  private Checkbox checkbox;

  public SetChannelMuteCommand(String channel, Checkbox checkbox) {
    this.channel = channel;
    this.checkbox = checkbox;
  }

  @Override
  public void execute() {
    boolean value = checkbox.isChecked();
    Engine.MIXER.setMuted(channel, value);
    try {
      Engine.PROPERTIES.saveProperty(Engine.MIXER.getChannel(channel).getMutedPropertyName(),
          value);
    } catch (IOException ex) {
      Log.error(ex);
    }
  }
}
