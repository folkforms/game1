package game1.toolkit.screens.rebindkeys;

import game1.actors.Actor;
import game1.resolutions.layers.ResolutionLayers;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;
import game1.toolkit.widgets.button.Button;
import game1.toolkit.widgets.label.Label;
import game1.toolkit.widgets.label.LabelBuilder;

abstract public class RebindKeysMenu implements Actor {

  // private int listY = 800;

  public RebindKeysMenu() {
    addOkButton();
    addCancelButton();
    // addResetToDefaultsButton();
    // FIXME Add a 'reset to defaults' option that pulls values from default.X.game1.properties e.g.
    // Button resetButton = new Button("Reset", 5, 700, 200, ZIndex.MENU_BUTTON, 200, 50);
    // resetButton.setCommand(new ResetBindingsCommand());
    // childActors.add(resetButton);
  }

  abstract public String getMainMenuState();

  abstract public int getButtonZIndex();

  abstract public int getRebindMenuZIndex();

  public void createRebind(String labelText, String propertyKey, int y) {
    createRebind(labelText, propertyKey, 200, 400, y);
  }

  /**
   * Creates a rebind button for the given property key.
   *
   * @param labelText
   *          label text
   * @param propertyKey
   *          the property key associated with this rebind
   */
  public void createRebind(String labelText, String propertyKey, int labelX, int buttonX, int y) {
    Label label = LabelBuilder.init(labelText).setFontSize(5)
        .setPosition(labelX, y, getButtonZIndex()).build();
    addChild(label);
    // FIXME MISC_STUFF: Why are we not just using propertyKey here? It is a display name thing? Add
    // a "getText" or "getDisplayName" method to Keys class if needed.
    RebindButton button = new RebindButton(propertyKey,
        /* KeyCodeToText.getText(Engine.PROPERTIES.getIntProperty(propertyKey)) */ "FIXME", 5,
        buttonX, y, getButtonZIndex(), 200, 40);
    addChild(button);
  }

  public void addOkButton() {
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(ResolutionLayers.WINDOW_LAYER).getResolutionHints();
    int x = hints.getWindowWidth() - 500;
    int y = 50;
    Button okButton = new Button("Ok", 5, x, y, getButtonZIndex(), 200, 50);
    okButton.setCommand(new ProcessPendingRebindsCommand(getMainMenuState()));
    addChild(okButton);
  }

  public void addCancelButton() {
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(ResolutionLayers.WINDOW_LAYER).getResolutionHints();
    int x = hints.getWindowWidth() - 250;
    int y = 50;
    Button cancelButton = new Button("Cancel", 5, x, y, getButtonZIndex(), 200, 50);
    cancelButton.setCommand(new CancelRebindMenuCommand(getMainMenuState()));
    addChild(cancelButton);
  }

  // @Override
  // public void draw(Graphics g) {
  // g.setColour(0f, 0.2f, 0.2f);
  // g.fillRect(0, 0, Engine.GAME_WIDTH, Engine.GAME_HEIGHT);
  // g.setColour(1, 1, 1);
  // g.setFontFace(FontUtils.DEFAULT_FONT_FACE);
  // g.setFontScale(10);
  // g.drawString("Rebind Controls", 200, Engine.GAME_HEIGHT - 200);
  // }
  //
  // @Override
  // public int getZ() {
  // return getRebindMenuZIndex();
  // }
}
