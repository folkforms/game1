package game1.toolkit.screens.optionsmenu;

import java.io.IOException;

import folkforms.log.Log;
import game1.core.engine.Engine;
import game1.events.Command;
import game1.toolkit.widgets.label.Label;
import game1.toolkit.widgets.slider.Slider;

public class SetChannelVolumeCommand implements Command {

  private String channel;
  private Slider slider;
  private Label label;

  public SetChannelVolumeCommand(String channel, Slider slider, Label label) {
    this.channel = channel;
    this.slider = slider;
    this.label = label;
  }

  @Override
  public void execute() {
    label.setText(String.format("%.0f", slider.getValue() * 100));
    float value = slider.getValue();
    Engine.MIXER.setVolume(channel, value);
    try {
      Engine.PROPERTIES.saveProperty(Engine.MIXER.getChannel(channel).getVolumePropertyName(),
          value);
    } catch (IOException ex) {
      Log.error(ex);
    }
  }
}
