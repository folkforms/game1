package game1.toolkit.screens.rebindkeys;

import game1.core.input.KeyBindings;
import game1.core.input.KeyCommand;
import game1.core.input.Keys;
import game1.events.Command;

/**
 * Set of {@link KeyBindings} used for typing into a field. Covers all typeable keys. Supports
 * backspace.
 */
public class RebindKeyBindings extends KeyBindings {

  public RebindKeyBindings() {
    addEscapeKeyCommand(new ClearBindingCommand());

    createKeyCommand(Keys.KEY_F1);
    createKeyCommand(Keys.KEY_F2);
    createKeyCommand(Keys.KEY_F3);
    createKeyCommand(Keys.KEY_F4);
    createKeyCommand(Keys.KEY_F5);
    createKeyCommand(Keys.KEY_F6);
    createKeyCommand(Keys.KEY_F7);
    createKeyCommand(Keys.KEY_F8);
    createKeyCommand(Keys.KEY_F9);
    createKeyCommand(Keys.KEY_F10);
    createKeyCommand(Keys.KEY_F11);
    createKeyCommand(Keys.KEY_F12);

    createKeyCommand(Keys.KEY_SPACE);
    createKeyCommand(Keys.KEY_ENTER);
    createKeyCommand(Keys.KEY_UP_ARROW);
    createKeyCommand(Keys.KEY_DOWN_ARROW);
    createKeyCommand(Keys.KEY_LEFT_ARROW);
    createKeyCommand(Keys.KEY_RIGHT_ARROW);

    createKeyCommand(Keys.KEY_A);
    createKeyCommand(Keys.KEY_B);
    createKeyCommand(Keys.KEY_C);
    createKeyCommand(Keys.KEY_D);
    createKeyCommand(Keys.KEY_E);
    createKeyCommand(Keys.KEY_F);
    createKeyCommand(Keys.KEY_G);
    createKeyCommand(Keys.KEY_H);
    createKeyCommand(Keys.KEY_I);
    createKeyCommand(Keys.KEY_J);
    createKeyCommand(Keys.KEY_K);
    createKeyCommand(Keys.KEY_L);
    createKeyCommand(Keys.KEY_M);
    createKeyCommand(Keys.KEY_N);
    createKeyCommand(Keys.KEY_O);
    createKeyCommand(Keys.KEY_P);
    createKeyCommand(Keys.KEY_Q);
    createKeyCommand(Keys.KEY_R);
    createKeyCommand(Keys.KEY_S);
    createKeyCommand(Keys.KEY_T);
    createKeyCommand(Keys.KEY_U);
    createKeyCommand(Keys.KEY_V);
    createKeyCommand(Keys.KEY_W);
    createKeyCommand(Keys.KEY_X);
    createKeyCommand(Keys.KEY_Y);
    createKeyCommand(Keys.KEY_Z);

    createKeyCommand(Keys.KEY_1);
    createKeyCommand(Keys.KEY_2);
    createKeyCommand(Keys.KEY_3);
    createKeyCommand(Keys.KEY_4);
    createKeyCommand(Keys.KEY_5);
    createKeyCommand(Keys.KEY_6);
    createKeyCommand(Keys.KEY_7);
    createKeyCommand(Keys.KEY_8);
    createKeyCommand(Keys.KEY_9);
    createKeyCommand(Keys.KEY_0);

    createKeyCommand(Keys.KEY_MINUS);
    createKeyCommand(Keys.KEY_EQUALS);

    createKeyCommand(Keys.KEY_LEFT_BRACKET);
    createKeyCommand(Keys.KEY_RIGHT_BRACKET);

    createKeyCommand(Keys.KEY_SEMICOLON);
    createKeyCommand(Keys.KEY_APOSTROPHE);
    // FIXME '/@ symbols are happening when I press #/~
    // FIXME There is no key code for hash key

    createKeyCommand(Keys.KEY_COMMA);
    createKeyCommand(Keys.KEY_PERIOD);
    createKeyCommand(Keys.KEY_FORWARD_SLASH);

    createKeyCommand(Keys.KEY_BACKSLASH);

    createKeyCommand(Keys.KEY_LEFT_SHIFT);
    createKeyCommand(Keys.KEY_RIGHT_SHIFT);
    createKeyCommand(Keys.KEY_LEFT_CTRL);
    createKeyCommand(Keys.KEY_RIGHT_CTRL);
    createKeyCommand(Keys.KEY_LEFT_ALT);
    createKeyCommand(Keys.KEY_RIGHT_ALT);
    createKeyCommand(Keys.KEY_CAPSLOCK);
    createKeyCommand(Keys.KEY_TAB);
  }

  private void createKeyCommand(int keyCode) {
    register(KeyCommand.init().command(new EndRebindCommand(keyCode)).keys(keyCode));
  }

  private void addEscapeKeyCommand(Command command) {
    register(KeyCommand.init().command(command).keys(Keys.KEY_ESCAPE));
  }
}
