package game1.toolkit.screens.mainmenu;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import game1.actors.Active;
import game1.actors.Actor;
import game1.actors.ParentChildRegistryFactory;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;
import game1.toolkit.commands.ExitCommand;
import game1.toolkit.commands.MoveToStateCommand;
import game1.toolkit.widgets.button.Button;
import game1.toolkit.widgets.menu.Menu;

/**
 * A main menu class that displays a few state buttons on the screen. See
 * {@link SimpleMainMenuBuilder}.
 */
public class SimpleMainMenu implements Actor, Menu {

  private int z;
  private boolean isActive = false;

  public static SimpleMainMenu build(String resolutionLayer,
      LinkedHashMap<String, String> properties, LinkedHashMap<String, String> buttons) {

    SimpleMainMenu simpleMainMenu = new SimpleMainMenu();

    int w = getIntProperty(properties, "w", 340);
    int h = getIntProperty(properties, "h", 40);
    int vGap = getIntProperty(properties, "vgap", 10);
    int x = getIntProperty(properties, "x", calculateX(w, resolutionLayer));
    int y = getIntProperty(properties, "y", calculateY(buttons.size(), h, vGap, resolutionLayer));
    int z = getIntProperty(properties, "z", 10001);

    Iterator<String> buttonIterator = buttons.keySet().iterator();
    while (buttonIterator.hasNext()) {
      String text = buttonIterator.next();
      String state = buttons.get(text);
      Button button = new Button(text, 5, x, y, z, w, h);
      button.setCommand(new MoveToStateCommand(state));
      simpleMainMenu.addChild(button);
      y = y - h - vGap;
    }

    Button exitButton = new Button("Exit", 5, x, y, z, w, h);
    exitButton.setCommand(new ExitCommand());
    simpleMainMenu.addChild(exitButton);
    simpleMainMenu.setZ(z);

    return simpleMainMenu;
  }

  @Override
  public int getZ() {
    return z;
  }

  private void setZ(int z) {
    this.z = z;
  }

  private static int calculateX(int width, String resolutionLayer) {
    ResolutionHints hints = ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayer)
        .getResolutionHints();
    return (int) hints.getResolutionWidth() / 2 - width / 2;
  }

  private static int calculateY(int numButtons, int height, int vGap, String resolutionLayer) {
    ResolutionHints hints = ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayer)
        .getResolutionHints();
    int y = (int) hints.getResolutionHeight() / 2;
    y = y + ((numButtons / 2 + 1) * (height + vGap));
    return y;
  }

  private static int getIntProperty(LinkedHashMap<String, String> properties, String key,
      int defaultValue) {
    String value = properties.get(key);
    if (value == null) {
      return defaultValue;
    } else {
      return Integer.parseInt(value);
    }
  }

  @Override
  public boolean isActive() {
    return isActive;
  }

  @Override
  public void setActive(boolean b) {
    isActive = b;
    List<Actor> children = ParentChildRegistryFactory.getInstance().getChildren(this);
    for (int i = 0; i < children.size(); i++) {
      Actor actor = children.get(i);
      if (actor instanceof Active) {
        ((Active) actor).setActive(isActive);
      }
    }
  }
}
