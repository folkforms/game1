package game1.toolkit.screens.optionsmenu;

import java.util.List;

import game1.actors.Active;
import game1.actors.Actor;
import game1.actors.ParentChildRegistryFactory;
import game1.core.engine.Engine;
import game1.core.graphics.Colour;
import game1.core.graphics.RenderedFont;
import game1.core.sound.internal.MixerChannel;
import game1.events.Command;
import game1.resolutions.layers.ResolutionLayers;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;
import game1.toolkit.widgets.background.Background;
import game1.toolkit.widgets.button.Button;
import game1.toolkit.widgets.checkbox.Checkbox;
import game1.toolkit.widgets.checkbox.CheckboxBuilder;
import game1.toolkit.widgets.label.Label;
import game1.toolkit.widgets.label.LabelBuilder;
import game1.toolkit.widgets.menu.Menu;
import game1.toolkit.widgets.slider.Slider;

// FIXME, I feel this needs to pass its Active state down to the children
/**
 * A basic options menu with sliders to change volume.
 */
abstract public class OptionsMenu implements Actor, Menu {

  protected int x, y, w, h;
  protected RenderedFont font;
  protected String title;
  protected float scale = 5;
  protected Button closeButton;
  protected Colour backgroundColour;
  protected int volumeSliderVerticalBoost;
  protected int checkboxVerticalBoost;
  protected boolean isActive = false;

  public OptionsMenu(String title, RenderedFont font, float scale, int w, int h,
      Colour backgroundColour, int volumeSliderVerticalBoost, int checkboxVerticalBoost) {
    this.font = font;
    this.title = title;
    this.scale = scale;
    this.w = w;
    this.h = h;
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(ResolutionLayers.WINDOW_LAYER).getResolutionHints();
    this.x = hints.getWindowWidth() / 2 - w / 2;
    this.y = hints.getWindowHeight() / 2 - h / 2;
    this.backgroundColour = backgroundColour;
    this.volumeSliderVerticalBoost = volumeSliderVerticalBoost;
    this.checkboxVerticalBoost = checkboxVerticalBoost;

    addTitle();
    addVolumeControls();
    addCloseButton();
    addBackground();
  }

  protected void addTitle() {
    addChild(LabelBuilder.init(title).setFont(font).setFontSize(scale + 2)
        .setPosition(x + 35, y + h - 70, getZ() + 10).build());
  }

  protected void addVolumeControls() {
    addVolumeControl("master", "Master volume", x + 35, y + 110 + 160);
    addVolumeControl("effects", "Effects volume", x + 35, y + 110 + 80);
    addVolumeControl("music", "Music volume", x + 35, y + 110);
  }

  protected void addVolumeControl(String channelName, String labelText, int channelX,
      int channelY) {
    MixerChannel channel = Engine.MIXER.getChannel(channelName);
    String volumePropertyName = channel.getVolumePropertyName();
    float volume = Engine.PROPERTIES.getFloatProperty(volumePropertyName);
    // Channel label
    Label label = LabelBuilder.init(labelText).setFont(font).setFontSize(scale)
        .setPosition(channelX, channelY, getZ() + 10).build();
    addChild(label);
    // Volume label
    Label label2 = LabelBuilder.init(String.format("%.0f", volume * 100)).setFont(font)
        .setFontSize(scale).setPosition(channelX + 590, channelY, getZ() + 10).build();
    addChild(label2);
    // Volume slider
    Slider channelVolumeSlider = getVolumeSlider(channelX + 360,
        channelY + volumeSliderVerticalBoost, getZ() + 10);
    channelVolumeSlider.setValue(volume);
    channelVolumeSlider
        .setCommand(new SetChannelVolumeCommand(channelName, channelVolumeSlider, label2));
    addChild(channelVolumeSlider);
    // Mute checkbox
    Label muteCheckboxLabel = LabelBuilder.init("Mute")
        .setPosition(channelX + 690, channelY, getZ() + 10).setFont(font).setFontSize(scale)
        .build();
    Checkbox muteCheckbox = CheckboxBuilder.init(muteCheckboxLabel)
        .vIconBoost(checkboxVerticalBoost).build();
    muteCheckbox.setCommand(new SetChannelMuteCommand(channelName, muteCheckbox));
    addChild(muteCheckbox);
  }

  protected Slider getVolumeSlider(int vsX, int vsY, int vsZ) {
    return new Slider(vsX, vsY, vsZ, "toolkit.slider_hbar", "toolkit.slider_knob");
  }

  private void addCloseButton() {
    closeButton = new Button("Close", scale, x + w - 300, y + 20, getZ() + 10, 250, 50);
    closeButton.setCommand(new Command() {
      @Override
      public void execute() {
        setVisible(false);
      }
    });
    addChild(closeButton);
  }

  private void addBackground() {
    addChild(Background.create(x, y, getZ(), w, h, backgroundColour));
  }

  public void setCloseButtonCommand(Command command) {
    this.closeButton.setCommand(command);
  }

  @Override
  public boolean isActive() {
    return isActive;
  }

  @Override
  public void setActive(boolean b) {
    isActive = b;
    List<Actor> children = ParentChildRegistryFactory.getInstance().getChildren(this);
    for (int i = 0; i < children.size(); i++) {
      Actor actor = children.get(i);
      if (actor instanceof Active) {
        ((Active) actor).setActive(isActive);
      }
    }
  }

  @Override
  abstract public int getZ();
}
