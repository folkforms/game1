package game1.toolkit.screens.rebindkeys;

import game1.core.engine.Engine;
import game1.events.Command;

public class EndRebindCommand implements Command {

  private int keyCode;

  public EndRebindCommand(int keyCode) {
    this.keyCode = keyCode;
  }

  @Override
  public void execute() {
    RebindCoordinator.endRebind(keyCode);
    Engine.KEY_BINDINGS.stashPop();
  }
}
