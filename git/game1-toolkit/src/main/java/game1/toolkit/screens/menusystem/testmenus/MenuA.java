package game1.toolkit.screens.menusystem.testmenus;

import org.joml.Vector2f;

import game1.actors.Clickable;
import game1.core.graphics.Drawable;
import game1.core.input.Mouse;
import game1.core.input.MouseUtils;
import game1.datasets.GameDataFactory;
import game1.primitives.Rectangle;
import game1.primitives.Shape;
import game1.toolkit.internal.main.ToolkitZIndex;
import game1.toolkit.widgets.menu.Menu;

public class MenuA implements Menu, Clickable {

  private Rectangle shape = new Rectangle(600, 840, 100, 100);
  private Rectangle topHalf = new Rectangle(shape.x, shape.y + shape.h / 2, shape.w, shape.h / 2);
  private Rectangle bottomHalf = new Rectangle(shape.x, shape.y, shape.w, shape.h / 2);
  private boolean active;
  protected boolean isResolutionAffected = true;
  protected boolean isViewportAffected = false;

  private Menu menuB;
  private Menu menuC;

  public MenuA(MenuB menuB, MenuC menuC) {
    Drawable drawable = GameDataFactory.getInstance().get("MenuA.png");
    drawable.setPosition(600, 840, ToolkitZIndex.MENU_A);
    addChild(drawable);
    this.menuB = menuB;
    this.menuC = menuC;
    setActive(false);
    setVisible(false);
  }

  @Override
  public int getZ() {
    return ToolkitZIndex.MENU_A;
  }

  @Override
  public Shape<Rectangle> getShape() {
    return shape;
  }

  @Override
  public void onClick(int button, float windowX, float windowY) {
    Vector2f pos = MouseUtils.translateToResolutionLayerPosition(new Vector2f(windowX, windowY),
        this);
    if (button == Mouse.LEFT_BUTTON) {
      if (bottomHalf.contains(pos.x, pos.y)) {
        menuB.setActive(true);
        menuB.setVisible(true);
      }
      if (topHalf.contains(pos.x, pos.y)) {
        menuC.setActive(true);
        menuC.setVisible(true);
      }
    }
    if (button == Mouse.RIGHT_BUTTON) {
      this.setActive(false);
      this.setVisible(false);
    }
  }

  @Override
  public boolean isActive() {
    return active;
  }

  @Override
  public void setActive(boolean b) {
    active = b;
  }
}
