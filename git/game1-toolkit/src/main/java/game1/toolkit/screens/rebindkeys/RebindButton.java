package game1.toolkit.screens.rebindkeys;

import game1.core.input.Mouse;
import game1.toolkit.widgets.button.Button;

public class RebindButton extends Button {

  private boolean rebindInProgress = false;
  private String text;

  public RebindButton(String action, String text, float textScale, int x, int y, int z, int w,
      int h) {
    super(text, textScale, x, y, z, w, h);
    this.setCommand(new StartRebindCommand(action, this));
    // this.text = String.join("", super.lines);
    this.text = String.join("", new String[] { "fixme1", "fixme2" });
  }

  public void setRebindInProgress(boolean b) {
    this.rebindInProgress = b;
  }

  // FIXME
  // @Override
  // public void drawNormal(Graphics g) {
  // g.setColor(0.4f, 0.4f, 0.4f);
  // g.fillRect(buttonX, buttonY, buttonW, buttonH);
  // g.setColor(1, 1, 1);
  // g.setFont(font, scale);
  // drawRebindText(g);
  // }
  //
  // @Override
  // protected void drawHover(Graphics g) {
  // g.setColor(0.6f, 0.6f, 0.6f);
  // g.fillRect(buttonX, buttonY, buttonW, buttonH);
  // g.setColor(0.9f, 0.9f, 0.9f);
  // g.setFont(font, scale);
  // drawRebindText(g);
  // }
  //
  // @Override
  // protected void drawActive(Graphics g) {
  // g.setColor(0.4f, 0.4f, 0.4f);
  // g.fillRect(buttonX, buttonY, buttonW, buttonH);
  // g.setColor(1, 1, 1);
  // g.setFont(font, scale);
  // drawRebindText(g);
  // }

  // private void drawRebindText(Graphics g) {
  // g.setFontFace(FontUtils.DEFAULT_FONT_FACE);
  // g.setFontScale(scale);
  //
  // if (!rebindInProgress) {
  // g.setColour(1, 1, 1);
  // int textX = x + w / 2
  // - FontUtils.getStringWidth(text, FontUtils.DEFAULT_FONT_FACE, scale) / 2;
  // g.drawString(text, textX, (int) (y + scale));
  // } else {
  // g.setColour(1, 1, 0);
  // int textX = x + w / 2
  // - FontUtils.getStringWidth("...", FontUtils.DEFAULT_FONT_FACE, scale) / 2;
  // g.drawString("...", textX, (int) (y + scale));
  // }
  // }

  @Override
  public void onClick(int button, float windowX, float windowY) {
    if (!RebindCoordinator.rebindingInProgress() && button == Mouse.LEFT_BUTTON) {
      command.execute();
    }
  }

  // @Override
  // public void setText(String newText) {
  // super.setText(newText);
  // // this.text = String.join("", super.lines);
  // this.text = String.join("", new String[] { "fixme1", "fixme2" });
  // }
}
