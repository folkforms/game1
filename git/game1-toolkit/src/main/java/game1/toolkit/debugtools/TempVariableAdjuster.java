package game1.toolkit.debugtools;

import game1.core.devtools.HiddenDebugTool;
import game1.events.EventSubscriptionsFactory;
import game1.events.RaiseEventCommand;
import game1.events.ReceiveEvent;
import game1.toolkit.widgets.button.Button;
import game1.toolkit.widgets.label.Label;
import game1.toolkit.widgets.label.LabelBuilder;
import game1.variables.VariablesFactory;

// FIXME VARIABLES: POC that we can adjust variables on the fly and trigger a reload of the object.
// Next it needs variable value as part of the text and it needs to look a bit nicer.
public class TempVariableAdjuster extends HiddenDebugTool {

  private String variableName;
  private float smallDelta;
  private float largeDelta;

  // Pass it e.g. "player.speed", "0.1", "1" and it will display a device for editing
  // the variable on the debug screen. You can adjust up or down by either of the deltas.
  // When the value changes it will call Engine.VARIABLES.set("player.speed", newValue) which
  // will notify the listeners.
  public TempVariableAdjuster(String variableName, float smallDelta, float largeDelta) {
    EventSubscriptionsFactory.getInstance().subscribe(this);
    this.variableName = variableName;
    this.smallDelta = smallDelta;
    this.largeDelta = largeDelta;

    int labelX = 100;
    Label description = LabelBuilder.init(variableName).setPosition(100, 900, 50000).build();
    Button decreaseByLargeDelta = new Button("<<", labelX + 300, 900, 80000, 45, 30);
    Button decreaseBySmallDelta = new Button("<", labelX + 350, 900, 80000, 45, 30);
    Button increaseBySmallDelta = new Button(">", labelX + 400, 900, 80000, 45, 30);
    Button increaseByLargeDelta = new Button(">>", labelX + 450, 900, 80000, 45, 30);
    addChild(description);
    addChild(decreaseByLargeDelta);
    addChild(decreaseBySmallDelta);
    addChild(increaseBySmallDelta);
    addChild(increaseByLargeDelta);

    decreaseByLargeDelta
        .setCommand(new RaiseEventCommand("TempVariableAdjuster", "decrease-by-large-delta"));
    decreaseBySmallDelta
        .setCommand(new RaiseEventCommand("TempVariableAdjuster", "decrease-by-small-delta"));
    increaseBySmallDelta
        .setCommand(new RaiseEventCommand("TempVariableAdjuster", "increase-by-small-delta"));
    increaseByLargeDelta
        .setCommand(new RaiseEventCommand("TempVariableAdjuster", "increase-by-large-delta"));
  }

  @ReceiveEvent("TempVariableAdjuster")
  public void adjust(String data) {
    Float f = VariablesFactory.getInstance().getAsNumber(variableName);
    if (data.equals("decrease-by-large-delta")) {
      f -= largeDelta;
    }
    if (data.equals("decrease-by-small-delta")) {
      f -= smallDelta;
    }
    if (data.equals("increase-by-small-delta")) {
      f += smallDelta;
    }
    if (data.equals("increase-by-large-delta")) {
      f += largeDelta;
    }
    VariablesFactory.getInstance().set(variableName, f);
  }
}
