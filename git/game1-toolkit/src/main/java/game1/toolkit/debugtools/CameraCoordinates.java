package game1.toolkit.debugtools;

import org.joml.Vector3f;

import game1.core.engine.internal.Camera;
import game1.core.engine.internal.CameraFactory;

public class CameraCoordinates extends DebugDynamicText {

  public CameraCoordinates(int x, int y) {
    super("CameraCoordinates", x, y);
  }

  @Override
  public String getData() {
    Camera camera = CameraFactory.getInstance();
    Vector3f p = camera.getPosition();
    Vector3f r = camera.getRotation();
    return String.format("Camera: %.0f,%.0f,%.0f,%.0f,%.0f,%.0f", p.x, p.y, p.z, r.x, r.y, r.z);
  }
}
