package game1.toolkit.debugtools;

import org.joml.Quaternionf;
import org.joml.Vector3f;

import game1.core.graphics.Drawable;
import game1.events.EventSubscriptionsFactory;
import game1.events.ReceiveEvent;

public class Adjuster extends DebugDynamicText {

  private Drawable SELECTED_ITEM = null;

  private int selectedOption = 0;
  private String[] options = { "position", "rotation" };
  private float delta = 1;

  public Adjuster(int x, int y) {
    super("Adjuster", x, y);
    EventSubscriptionsFactory.getInstance().subscribe(this);
  }

  @Override
  public String getData() {
    StringBuffer sb = new StringBuffer("Adjuster:\n");
    sb.append(String.format("  Selected: %s\n", SELECTED_ITEM));
    if (SELECTED_ITEM != null) {
      Vector3f p = SELECTED_ITEM.getPosition();
      sb.append(String.format("  Position: %.0f,%.0f,%.0f\n", p.x, p.y, p.z));
      Quaternionf r = SELECTED_ITEM.getRotation();
      sb.append(String.format("  Rotation: %.0f,%.0f,%.0f,%.0f\n", r.x, r.y, r.z, r.w));
      sb.append(String.format("  Option: %s\n", options[selectedOption]));
    }
    return sb.toString();
  }

  @ReceiveEvent("adjuster.set_selected")
  public void setSelected(Drawable drawable) {
    SELECTED_ITEM = drawable;
  }

  @ReceiveEvent("adjuster.decrement_option")
  public void decrementOption() {
    int newStep = selectedOption - 1;
    if (newStep < 0) {
      newStep = options.length - 1;
    }
    selectedOption = newStep;
  }

  @ReceiveEvent("adjuster.increment_option")
  public void incrementOption() {
    int newStep = selectedOption + 1;
    if (newStep >= options.length) {
      newStep = 0;
    }
    selectedOption = newStep;
  }

  @ReceiveEvent("adjuster.apply")
  public void apply(String data) {
    if (SELECTED_ITEM == null) {
      return;
    }

    Vector3f p = SELECTED_ITEM.getPosition();
    switch (options[selectedOption]) {
    case "position":
      switch (data) {
      case "dec_x":
        p.set(p.x - delta, p.y, p.z);
        break;
      case "inc_x":
        p.set(p.x + delta, p.y, p.z);
        break;
      case "dec_y":
        p.set(p.x, p.y - delta, p.z);
        break;
      case "inc_y":
        p.set(p.x, p.y + delta, p.z);
        break;
      case "dec_z":
        p.set(p.x, p.y, p.z - delta);
        break;
      case "inc_z":
        p.set(p.x, p.y, p.z + delta);
        break;
      }
      break;
    case "rotation":
      // FIXME
      break;
    }
  }
}
