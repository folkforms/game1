package game1.toolkit.debugtools;

import game1.core.pipelines.GraphicsPipeline;

public class DebugShaderTestMode extends DebugDynamicText {

  public DebugShaderTestMode(int x, int y) {
    super("ShaderTest", x, y);
  }

  @Override
  public String getData() {
    return String.format("Shader test mode: %s (%s)", GraphicsPipeline.debug_testModeName,
        GraphicsPipeline.debug_testMode);
  }
}
