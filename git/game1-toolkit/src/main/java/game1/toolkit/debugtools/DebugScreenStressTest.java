package game1.toolkit.debugtools;

/**
 * Prints lines of random data to the debug screen. These lines change every tick, forcing the Label
 * to destroy create new Drawables each time.
 */
public class DebugScreenStressTest extends DebugDynamicText {

  /**
   * How many lines of random data to show.
   */
  private int scale = 20;

  public DebugScreenStressTest(int x, int y) {
    super("DebugScreenRandomData", x, y);
  }

  @Override
  public String getData() {
    StringBuffer sb = new StringBuffer("DebugScreenRandomData:\n");
    for (int i = 0; i < scale; i++) {
      double random = Math.random();
      String s = Double.toString(random);
      for (int j = 0; j < s.length() - 1; j += 2) {
        // 65 & 97
        String substring = s.substring(j, j + 2);
        if (substring.contains(".") || substring.contains("E") || substring.contains("-")) {
          continue;
        }
        int intValue = Integer.parseInt(substring);
        if ((intValue >= 65 && intValue <= 65 + 26) || (intValue >= 97 && intValue <= 97 + 26)) {
          char c = (char) intValue;
          sb.append(c);
        }
      }

      sb.append(String.format("%s\n", random));
    }
    return sb.toString();
  }
}
