package game1.toolkit.debugtools;

import java.util.Map;
import java.util.stream.Collectors;

import game1.resolutions.layers.ResolutionLayer;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.viewports.Viewport;

public class DebugViewport extends DebugDynamicText {

  public DebugViewport(int x, int y) {
    super("DebugViewport", x, y);
  }

  @Override
  public String getData() {
    Map<String, ResolutionLayer> map = ResolutionLayersFactory.getInstance()
        .debug_getResolutionLayers();
    String resolutionLayerData = map.entrySet().stream().map(entry -> {
      ResolutionLayer layer = entry.getValue();
      Viewport viewportForLayer = layer.getViewport();
      return viewportForLayer.toString();
    }).collect(Collectors.joining(", "));
    return String.format("Viewport: %s", resolutionLayerData);
  }
}
