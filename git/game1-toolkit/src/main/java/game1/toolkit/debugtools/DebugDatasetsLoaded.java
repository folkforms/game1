package game1.toolkit.debugtools;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

// FIXME DATASETS: Need to convert this to DebugTagsLoaded
public class DebugDatasetsLoaded extends DebugDynamicText {

  public DebugDatasetsLoaded(int x, int y) {
    super("DebugDatasetsLoaded", x, y);
  }

  @Override
  public String getData() {
    List<String> out = new ArrayList<>();
    out.add("Dataset Loader\n");
    out.add("---- Loaded ----\n");
    List<String> loaded = List.of("FIXME DebugDatasetsLoaded (1)");// EngineInternal.DATASETS.debug_listLoaded();
    out.addAll(loaded.stream().map(s -> s + "\n").collect(Collectors.toList()));
    out.add("---- Unloaded ----\n");
    List<String> unloaded = List.of("FIXME DebugDatasetsLoaded (2)"); // EngineInternal.DATASETS.debug_listUnloaded();
    out.addAll(unloaded.stream().map(s -> s + "\n").collect(Collectors.toList()));
    StringBuffer sb = new StringBuffer();
    for (String s : out) {
      sb.append(s);
    }
    return sb.toString();
  }
}
