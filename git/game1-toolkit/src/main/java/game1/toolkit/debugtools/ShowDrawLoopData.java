package game1.toolkit.debugtools;

import game1.core.engine.internal.DrawLoop;

public class ShowDrawLoopData extends DebugDynamicText {

  public ShowDrawLoopData(int x, int y) {
    super("ShowDrawLoopData", x, y);
  }

  @Override
  public String getData() {
    StringBuffer sb = new StringBuffer("DrawLoop:\n");
    sb.append(String.format("  Avg FPS: %s\n", DrawLoop.DEBUG_AVG_FPS));
    sb.append(String.format("  Avg UPS: %s\n", DrawLoop.DEBUG_AVG_UPS));
    sb.append(String.format("  Avg frame time: %.2f", DrawLoop.DEBUG_AVG_FRAME_TIME));
    return sb.toString();
  }
}
