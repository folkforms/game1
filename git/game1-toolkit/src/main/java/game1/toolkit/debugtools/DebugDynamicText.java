package game1.toolkit.debugtools;

import game1.actors.Tickable;
import game1.core.devtools.HiddenDebugTool;
import game1.core.registries.TickableAnnotations;
import game1.toolkit.widgets.label.Label;
import game1.toolkit.widgets.label.LabelBuilder;

/**
 * Implementation of {@link HiddenDebugTool} that renders a label containing data.
 */
abstract public class DebugDynamicText extends HiddenDebugTool implements Tickable {

  private Label label;

  public DebugDynamicText(String initialData, int x, int y) {
    this(initialData, 2, x, y);
  }

  public DebugDynamicText(String initialData, float scale, int x, int y) {
    label = LabelBuilder.init(initialData).setFontSize(scale).setPosition(x, y, Z_INDEX)
        .setModifiable(true).build();
    label.setVisible(false);
    addChild(label);
  }

  @TickableAnnotations.TickSlowly
  @Override
  public void onTick() {
    label.setText(getData());
  }

  abstract public String getData();
}
