package game1.toolkit.debugtools;

import java.util.Map;
import java.util.stream.Collectors;

import org.joml.Vector2f;

import game1.core.input.Mouse;
import game1.core.input.MouseFactory;
import game1.core.input.MouseUtils;
import game1.resolutions.layers.ResolutionLayer;
import game1.resolutions.layers.ResolutionLayersFactory;

public class DebugMouse extends DebugDynamicText {

  public DebugMouse(int x, int y) {
    super("MouseCoordinates", x, y);
  }

  @Override
  public String getData() {
    Mouse mouse = MouseFactory.getInstance();
    Vector2f windowPos = mouse.getWindowPos();

    Map<String, ResolutionLayer> map = ResolutionLayersFactory.getInstance()
        .debug_getResolutionLayers();
    String layerData = map.entrySet().stream().map(entry -> {
      ResolutionLayer layer = entry.getValue();
      Vector2f pos = MouseUtils.translateToResolutionLayerPosition(windowPos, layer);
      return String.format("%s pos: %s,%s", entry.getKey(), (int) pos.x, (int) pos.y);
    }).collect(Collectors.joining(", "));

    return String.format("Mouse: %.0f,%.0f (%s)", windowPos.x, windowPos.y, layerData);
  }
}
