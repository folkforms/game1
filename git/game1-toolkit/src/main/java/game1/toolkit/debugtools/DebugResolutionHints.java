package game1.toolkit.debugtools;

import game1.resolutions.layers.ResolutionLayers;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;
import game1.resolutions.shared.ResolutionHintsImpl;

public class DebugResolutionHints extends DebugDynamicText {

  public DebugResolutionHints(int x, int y) {
    super("DebugResolutionHints", x, y);
  }

  @Override
  public String getData() {
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(ResolutionLayers.WINDOW_LAYER).getResolutionHints();
    float scale = hints.getResolutionScale();
    float scaledWidth = hints.getResolutionWidth() * scale;
    float scaledHeight = hints.getResolutionHeight() * scale;
    return String.format(
        "Resolution hints: Offsets: %.0f,%.0f, Scale: %.3f, Resolution: %.0f,%.0f, Level: %.0f,%.0f, Window: %s,%s, Scaled resolution: %.1f,%.1f, Scaling strategy: %s",
        hints.getResolutionOffsetX(), hints.getResolutionOffsetY(), hints.getResolutionScale(),
        hints.getResolutionWidth(), hints.getResolutionHeight(), hints.getLevelWidth(),
        hints.getLevelHeight(), hints.getWindowWidth(), hints.getWindowHeight(), scaledWidth,
        scaledHeight, ((ResolutionHintsImpl) hints).debug_getScalingStrategy());
  }
}
