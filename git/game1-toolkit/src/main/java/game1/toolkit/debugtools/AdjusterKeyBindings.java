package game1.toolkit.debugtools;

import game1.core.engine.internal.CameraFactory;
import game1.core.graphics.Drawable;
import game1.core.input.KeyBindings;
import game1.core.input.KeyCommand;
import game1.core.input.Keys;
import game1.core.input.Mouse;
import game1.events.Command;
import game1.events.EventsFactory;

public class AdjusterKeyBindings extends KeyBindings {

  public AdjusterKeyBindings() {
    register(KeyCommand.init().command(new Command() {
      @Override
      public void execute() {
        Drawable drawable = CameraFactory.getInstance().selectDrawable();
        EventsFactory.getInstance().raise("adjuster.set_selected", new Object[] { drawable });
      }
    }).keys(Keys.KEY_F2));

    register(KeyCommand.init().keys(Keys.KEY_F3).event("adjuster.decrement_option"));
    register(KeyCommand.init().keys(Keys.KEY_F4).event("adjuster.increment_option"));

    register(
        KeyCommand.init().keys(Keys.KEY_LEFT_ARROW).delayRepeat().event("adjuster.apply", "dec_x"));
    register(KeyCommand.init().keys(Keys.KEY_RIGHT_ARROW).delayRepeat().event("adjuster.apply",
        "inc_x"));
    register(KeyCommand.init().delayRepeat().keys(Keys.KEY_DOWN_ARROW, Keys.KEY_LEFT_SHIFT)
        .event("adjuster.apply", "dec_z"));
    register(KeyCommand.init().delayRepeat().keys(Keys.KEY_UP_ARROW, Keys.KEY_LEFT_SHIFT)
        .event("adjuster.apply", "inc_z"));
    register(
        KeyCommand.init().keys(Keys.KEY_DOWN_ARROW).delayRepeat().event("adjuster.apply", "dec_y"));
    register(
        KeyCommand.init().keys(Keys.KEY_UP_ARROW).delayRepeat().event("adjuster.apply", "inc_y"));

    register(KeyCommand.init().keys(Keys.KEY_X).mouse(Mouse.SCROLL_WHEEL_DOWN).block()
        .event("adjuster.apply", "dec_x"));
    register(KeyCommand.init().keys(Keys.KEY_X).mouse(Mouse.SCROLL_WHEEL_UP).block()
        .event("adjuster.apply", "inc_x"));
    register(KeyCommand.init().keys(Keys.KEY_Y).mouse(Mouse.SCROLL_WHEEL_DOWN).block()
        .event("adjuster.apply", "dec_y"));
    register(KeyCommand.init().keys(Keys.KEY_Y).mouse(Mouse.SCROLL_WHEEL_UP).block()
        .event("adjuster.apply", "inc_y"));
    register(KeyCommand.init().keys(Keys.KEY_Z).mouse(Mouse.SCROLL_WHEEL_DOWN).block()
        .event("adjuster.apply", "inc_z"));
    register(KeyCommand.init().keys(Keys.KEY_Z).mouse(Mouse.SCROLL_WHEEL_UP).block()
        .event("adjuster.apply", "dec_z"));
  }
}
