package game1.toolkit.debugtools;

import game1.core.devtools.DevKeyBindings;

/**
 * Displays the dev key bindings.
 */
public class DebugShowDevKeyBindings extends DebugStaticText {

  public DebugShowDevKeyBindings(int x, int y) {
    super(DevKeyBindings.DEV_KEY_BINDINGS_DISPLAY_TEXT, x, y);
  }

  // @Override
  // public String getData() {
  // int mouseX = Engine.MOUSE.getX();
  // int mouseY = Engine.MOUSE.getY();
  // return String.format("Mouse: %s,%s", mouseX, mouseY);
  // }
}
