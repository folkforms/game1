package game1.toolkit.debugtools;

import game1.core.engine.Engine;
import game1.core.input.KeyCommand;

public class DebugKeyBindingsDelayRepeat extends DebugDynamicText {

  private KeyCommand playerLeft;
  private KeyCommand playerRight;

  public DebugKeyBindingsDelayRepeat(int x, int y) {
    super("KeyBindingsDelayRepeat", x, y);
  }

  @Override
  public String getData() {
    playerLeft = Engine.KEY_BINDINGS.debug_getKeyCommand("key.left");
    playerRight = Engine.KEY_BINDINGS.debug_getKeyCommand("key.right");
    if (playerLeft == null || playerRight == null) {
      return "Key Command Repeats\n--\n--";
    } else {
      return String.format("Key Command Repeats:\nkey.left: %s\nkey.right: %s\n",
          playerLeft.debug_getRepeatNumber(), playerRight.debug_getRepeatNumber());
    }
  }
}
