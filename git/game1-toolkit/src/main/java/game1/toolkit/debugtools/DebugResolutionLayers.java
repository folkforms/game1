package game1.toolkit.debugtools;

import java.util.Map;
import java.util.stream.Collectors;

import game1.resolutions.layers.ResolutionLayer;
import game1.resolutions.layers.ResolutionLayersFactory;

public class DebugResolutionLayers extends DebugDynamicText {

  public DebugResolutionLayers(int x, int y) {
    super("ResolutionLayers", x, y);
  }

  @Override
  public String getData() {
    Map<String, ResolutionLayer> map = ResolutionLayersFactory.getInstance()
        .debug_getResolutionLayers();
    String resolutionLayerData = map.entrySet().stream().map(entry -> entry.getValue().toString())
        .collect(Collectors.joining("\n"));
    return String.format("ResolutionLayers:\n%s", resolutionLayerData);
  }
}
