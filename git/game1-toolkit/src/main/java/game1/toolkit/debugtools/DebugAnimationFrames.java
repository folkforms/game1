package game1.toolkit.debugtools;

import game1.core.graphics.Animation3D;

public class DebugAnimationFrames extends DebugDynamicText {

  public static Animation3D ANIMATION;

  public DebugAnimationFrames(int x, int y) {
    super("AnimationFrames", x, y);
  }

  @Override
  public String getData() {
    if (ANIMATION != null) {
      int frameNumber = ANIMATION.debug_getFrameNumber();
      return String.format("Animation Name: %s\nAnimation Frame: %s",
          ANIMATION.getCurrentAnimation().getName(), frameNumber);
    } else {
      return "No animation";
    }
  }
}
