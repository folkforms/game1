package game1.toolkit.debugtools;

import java.util.List;

import game1.core.devtools.HiddenDebugTool;
import game1.toolkit.widgets.label.Label;
import game1.toolkit.widgets.label.LabelBuilder;

/**
 * Wraps a {@link Label} and only shows it when dev tools are visible.
 */
public class DebugStaticText extends HiddenDebugTool {

  private Label label;

  public DebugStaticText(String text, int x, int y) {
    this(text, x, y, Integer.MAX_VALUE);
  }

  public DebugStaticText(String text, int x, int y, int z) {
    this(LabelBuilder.init(text).setPosition(x, y, z).build());
  }

  public DebugStaticText(String[] text, int x, int y) {
    this(text, x, y, Integer.MAX_VALUE);
  }

  public DebugStaticText(String[] text, int x, int y, int z) {
    this(LabelBuilder.init(text).setPosition(x, y, z).build());
  }

  public DebugStaticText(List<String> text, int x, int y) {
    this(text, x, y, Integer.MAX_VALUE);
  }

  public DebugStaticText(List<String> text, int x, int y, int z) {
    this(LabelBuilder.init(text).setPosition(x, y, Integer.MAX_VALUE).build());
  }

  public DebugStaticText(Label label) {
    this.label = label;
    this.label.setVisible(false);
    addChild(this.label);
  }
}
