package game1.toolkit.debugtools;

import game1.core.pipelines.FrustumCullingFilter;

public class DebugFrustumCulling extends DebugDynamicText {

  public DebugFrustumCulling(int x, int y) {
    super("Frustum Culling", x, y);
  }

  @Override
  public String getData() {
    return String.format("Frustum Culling: %s culled, %s not culled, %s ineligible, %s total",
        FrustumCullingFilter.CULLED, FrustumCullingFilter.NOT_CULLED,
        FrustumCullingFilter.INELIGIBLE, FrustumCullingFilter.TOTAL);
  }
}
