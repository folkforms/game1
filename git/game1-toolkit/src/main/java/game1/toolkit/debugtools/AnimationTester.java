package game1.toolkit.debugtools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.joml.Vector3f;

import folkforms.log.Log;
import game1.actors.Actor;
import game1.core.graphics.Animation;
import game1.core.graphics.Drawable;
import game1.core.pipelines.GraphicsPipelineName;
import game1.core.resources.builders.PrimitiveBuilder;
import game1.events.EventSubscriptionsFactory;
import game1.events.ReceiveEvent;
import game1.resolutions.layers.ResolutionLayers;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;
import game1.toolkit.events.ToolkitEvents;
import game1.toolkit.widgets.label.Label;
import game1.toolkit.widgets.label.LabelBuilder;

/**
 * Used to view and to line up animations. Animations (and images) are displayed on the screen and
 * the user can tab between them. They can then use the arrow keys to move the animations and line
 * them up correctly using the red dots that represent the animations 0,0 coordinate. Once lined up,
 * use 'r' to reset the offset data and then use the arrow keys to move one of the animations until
 * its graphics line up with the other animation. Then take note of the updated offset values and
 * use those when registering the animation in the dataset.
 */
public class AnimationTester implements Actor {

  private Drawable[] outlines = new Drawable[4];
  private int index = -1;
  private int gap = 2;
  private int thickness = 2;
  private int z = 0;
  private Label label;
  private List<Drawable> drawables = new ArrayList<>();
  private Drawable selectedDrawable;
  private Map<Drawable, Drawable> redDots = new HashMap<>();
  private Map<Drawable, Properties> deltas = new HashMap<>();

  public AnimationTester() {
    EventSubscriptionsFactory.getInstance().subscribe(this);

    for (int i = 0; i < 4; i++) {
      outlines[i] = PrimitiveBuilder.init(GraphicsPipelineName.SCENE_2D).rect2d(5, 5)
          .colour(1, 1, 1).position(-50, -50, Integer.MAX_VALUE).build();
      addChild(outlines[i]);
    }

    label = LabelBuilder.init("Nothing selected").setPosition(0, 0, Integer.MAX_VALUE).build();
    addChild(label);

    String[] helpText = { "Animation Tester\n", "\n", "tab/shift+tab: select item\n",
        "arrows/shift+arrows/shift+ctrl+arrows: move item\n", "r: reset offset data\n",
        "p: print offset data to console\n" };
    Label helpTextLabel = LabelBuilder.init(helpText).build();
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(ResolutionLayers.WINDOW_LAYER).getResolutionHints();
    int windowHeight = hints.getWindowHeight();
    helpTextLabel.setPosition(10, windowHeight - 10 - helpTextLabel.getHeight());
    addChild(helpTextLabel);
  }

  public void add(Drawable drawable) {
    Vector3f pos = drawable.getPosition();
    drawables.add(drawable);
    drawable.setPosition(pos.x, pos.y, z++);
    addChild(drawable);

    Drawable zeroZeroDot = PrimitiveBuilder.init(GraphicsPipelineName.SCENE_2D).rect2d(2, 2)
        .colour(1, 0, 0).position(pos.x, pos.y, Integer.MAX_VALUE).build();
    redDots.put(drawable, zeroZeroDot);
    addChild(zeroZeroDot);

    Properties properties = new Properties();
    if (drawable instanceof Animation) {
      Animation animation = (Animation) drawable;
      properties.setProperty("offsetX", Float.toString(animation.getOffsetX()));
      properties.setProperty("offsetY", Float.toString(animation.getOffsetY()));
    }
    deltas.put(drawable, properties);
  }

  @ReceiveEvent(ToolkitEvents.ANIMATION_TESTER_SELECT)
  public void tab(int tabSize) {
    index += tabSize;
    if (index >= drawables.size()) {
      index = 0;
    }
    if (index < 0) {
      index = drawables.size() - 1;
    }
    selectedDrawable = drawables.get(index);
    setHelpers();
  }

  private void setHelpers() {
    setOutlines();
    setRedDots();
    setLabel();
  }

  private void setOutlines() {
    Vector3f pos = selectedDrawable.getPosition();
    float x = pos.x;
    float y = pos.y;
    float width = selectedDrawable.getWidth();
    float height = selectedDrawable.getHeight();

    float leftX = x - gap - thickness;
    float rightX = x + width + gap;

    float topY = y + height + gap;
    float bottomY = y - gap - thickness;

    float topAndBottomBarWidth = width + gap * 2 + thickness * 2;
    float leftAndRightBarHeight = height + gap * 2 + thickness * 2;

    outlines[0].setPosition(leftX, topY);
    outlines[0].resize(topAndBottomBarWidth, thickness);

    outlines[1].setPosition(rightX, bottomY);
    outlines[1].resize(thickness, leftAndRightBarHeight);

    outlines[2].setPosition(leftX, bottomY);
    outlines[2].resize(topAndBottomBarWidth, thickness);

    outlines[3].setPosition(leftX, bottomY);
    outlines[3].resize(thickness, leftAndRightBarHeight);
  }

  private void setRedDots() {
    redDots.keySet().forEach(key -> {
      Vector3f drawablePos = key.getPosition();
      Vector3f redDotPos = redDots.get(key).getPosition();
      redDotPos.x = drawablePos.x;
      redDotPos.y = drawablePos.y;
    });
  }

  private void setLabel() {
    Properties properties = deltas.get(selectedDrawable);
    String offset = "offset: n/a";
    if (selectedDrawable instanceof Animation) {
      offset = String.format("offset: %s,%s", properties.get("offsetX"), properties.get("offsetY"));
    }
    label.setText(offset);

    Vector3f pos = selectedDrawable.getPosition();
    float labelX = pos.x;
    float labelY = pos.y + selectedDrawable.getHeight() + gap + thickness + gap;
    label.setPosition(labelX, labelY);
  }

  @ReceiveEvent(ToolkitEvents.ANIMATION_TESTER_MOVE)
  public void move(int xSpeed, int ySpeed) {
    Vector3f pos = selectedDrawable.getPosition();
    selectedDrawable.setPosition(pos.x + xSpeed, pos.y + ySpeed);
    Properties properties = deltas.get(selectedDrawable);

    int currentOffsetX = Integer.parseInt(properties.get("offsetX").toString());
    int currentOffsetY = Integer.parseInt(properties.get("offsetY").toString());
    properties.setProperty("offsetX", Integer.toString(currentOffsetX + xSpeed));
    properties.setProperty("offsetY", Integer.toString(currentOffsetY + ySpeed));

    setHelpers();
  }

  @ReceiveEvent(ToolkitEvents.ANIMATION_TESTER_RESET_DELTAS)
  public void resetDeltas() {
    Properties properties = deltas.get(selectedDrawable);
    properties.setProperty("offsetX", "0");
    properties.setProperty("offsetY", "0");
    setHelpers();
  }

  @ReceiveEvent(ToolkitEvents.ANIMATION_TESTER_PRINT_DATA)
  public void printData() {
    Properties properties = deltas.get(selectedDrawable);
    int currentOffsetX = Integer.parseInt(properties.get("offsetX").toString());
    int currentOffsetY = Integer.parseInt(properties.get("offsetY").toString());
    Log.info("AnimationTester");
    Log.info("    drawable: %s", selectedDrawable.debug_getName());
    Log.info("    offset: %s,%s", currentOffsetX, currentOffsetY);
    setHelpers();
  }
}
