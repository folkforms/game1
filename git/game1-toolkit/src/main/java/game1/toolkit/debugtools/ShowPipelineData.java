package game1.toolkit.debugtools;

import game1.core.engine.Engine;
import game1.core.pipelines.GraphicsPipeline;

public class ShowPipelineData extends DebugDynamicText {

  public ShowPipelineData(int x, int y) {
    super("ShowPipelineData", x, y);
  }

  @Override
  public String getData() {
    StringBuffer sb = new StringBuffer("Pipeline: inst, non-inst, [drawables]\n");
    GraphicsPipeline[] pipelines = Engine.PIPELINES.debug_listPipelines();
    if (pipelines != null) {
      for (int i = 0; i < pipelines.length; i++) {
        sb.append(pipelines[i].debug_getOnScreenData()).append("\n");
      }
    }
    return sb.toString();
  }
}
