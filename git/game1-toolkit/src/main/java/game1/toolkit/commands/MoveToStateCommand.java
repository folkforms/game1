package game1.toolkit.commands;

import game1.core.engine.Engine;
import game1.events.Command;

/**
 * Changes the state when executed.
 */
public class MoveToStateCommand implements Command {

  private String state;

  /**
   * Creates a new {@link MoveToStateCommand} object that will change to the given state when
   * executed.
   *
   * @param state
   *          the state to change to
   */
  public MoveToStateCommand(String state) {
    this.state = state;
  }

  @Override
  public void execute() {
    Engine.GAME_STATE.moveToState(state);
  }
}
