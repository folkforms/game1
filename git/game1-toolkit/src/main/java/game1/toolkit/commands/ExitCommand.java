package game1.toolkit.commands;

import game1.core.infrastructure.lwjgl3.internal.LWJGL3Window;
import game1.events.Command;

public class ExitCommand implements Command {

  @Override
  public void execute() {
    LWJGL3Window.close();
  }
}
