package game1.toolkit.lights;

import game1.core.graphics.Drawable;
import game1.core.graphics.lights.Spotlight;

public class TestSpotlight extends Spotlight {

  /**
   * Create a new TestSpotlight object from an existing {@link Spotlight} and {@link Drawable}. The
   * position of the Drawable will be set to the position of the spotlight.
   *
   * @param spotlight
   *          spotlight to use
   * @param drawable
   *          drawable to use
   */
  public TestSpotlight(Spotlight spotlight, Drawable drawable) {
    super(spotlight);
    drawable.setPosition(spotlight.getPointLight().getPosition());
    drawable.setVisible(false);
    addChild(drawable);
  }
}
