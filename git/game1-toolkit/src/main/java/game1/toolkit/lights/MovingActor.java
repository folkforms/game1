package game1.toolkit.lights;

import org.joml.Vector3f;

import game1.actors.Actor;
import game1.actors.Tickable;
import game1.core.graphics.Drawable;

public class MovingActor implements Actor, Tickable {

  private Drawable drawable;
  private float speedX;
  private float speedY;
  private float speedZ;
  private int durationTicks;
  private long numTicks = 0;

  public MovingActor(Drawable drawable, float speedX, float speedY, float speedZ,
      int durationTicks) {
    addChild(drawable);
    this.drawable = drawable;
    this.speedX = speedX;
    this.speedY = speedY;
    this.speedZ = speedZ;
    this.durationTicks = durationTicks;
  }

  @Override
  public void onTick() {

    if (numTicks > durationTicks) {
      numTicks = 0;
      speedX *= -1;
      speedY *= -1;
      speedZ *= -1;
    }

    Vector3f pos = drawable.getPosition();
    pos.x += speedX;
    pos.y += speedY;
    pos.z += speedZ;

    numTicks++;
  }
}
