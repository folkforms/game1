package game1.toolkit.lights;

import game1.core.graphics.Drawable;
import game1.core.graphics.lights.PointLight;

public class TestPointLight extends PointLight {

  private Drawable drawable;

  /**
   * Create a new TestpointLight object from an existing {@link PointLight} and {@link Drawable}.
   * The position of the Drawable will be set to the position of the point light.
   *
   * @param pointLight
   *          point light to use
   * @param drawable
   *          drawable to use
   */
  public TestPointLight(PointLight pointLight, Drawable drawable) {
    super(pointLight);
    this.drawable = drawable;
    drawable.setPosition(pointLight.getPosition());
    drawable.setVisible(false);
    addChild(drawable);
  }

  public void setDrawableVisible(boolean value) {
    drawable.setVisible(value);
  }
}
