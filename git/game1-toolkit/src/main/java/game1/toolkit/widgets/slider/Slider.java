package game1.toolkit.widgets.slider;

import org.joml.Vector2f;

import game1.actors.Actor;
import game1.actors.Clickable;
import game1.actors.Draggable;
import game1.core.graphics.Image;
import game1.core.input.MouseUtils;
import game1.datasets.GameDataFactory;
import game1.events.Command;
import game1.primitives.Rectangle;
import game1.primitives.Shape;

public class Slider implements Actor, Draggable, Clickable {

  private Command command = null;

  protected Image hBarDrawable, knobDrawable;
  protected Rectangle knob;

  protected int x, y, z, w, h;

  protected float VALUE = 0.5f;
  protected float MAX_VALUE = 1;
  protected float MIN_VALUE = 0;

  protected boolean active = true;
  protected boolean isResolutionAffected = true;
  protected boolean isViewportAffected = false;

  public Slider(int x, int y, int z, String hBarRef, String knobRef) {
    this.x = x;
    this.y = y;
    this.z = z;

    hBarDrawable = GameDataFactory.getInstance().get(hBarRef);
    knobDrawable = GameDataFactory.getInstance().get(knobRef);
    this.w = (int) (hBarDrawable.getWidth());
    this.h = (int) (knobDrawable.getHeight());
    hBarDrawable.setPosition(x, y, z);
    updateKnob();
    hBarDrawable.setName("slider hbar drawable (%s)", hBarRef);
    knobDrawable.setName("slider knob drawable (%s)", knobRef);
    addChild(hBarDrawable);
    addChild(knobDrawable);
  }

  public void setCommand(Command c) {
    this.command = c;
  }

  public float getValue() {
    return VALUE;
  }

  public void setValue(float newValue) {
    if (newValue < MIN_VALUE) {
      VALUE = MIN_VALUE;
    } else if (newValue > MAX_VALUE) {
      VALUE = MAX_VALUE;
    } else {
      VALUE = newValue;
    }
    updateKnob();
  }

  @Override
  public int getZ() {
    return z;
  }

  @Override
  public Shape<Rectangle> getShape() {
    return new Rectangle(knob.x - 4, knob.y, knob.w + 4, knob.h);
  }

  protected void updateKnob() {
    int knobX = (int) (x + VALUE * w - 6);
    int knobY = y - h / 2 + 2;
    int knobW = 12;
    int knobH = h;
    knob = new Rectangle(knobX, knobY, knobW, knobH);
    knobDrawable.setPosition(knobX, knobY, z + 1);
  }

  @Override
  public void onDrag(float windowX, float windowY) {
    Vector2f pos = MouseUtils.translateToResolutionLayerPosition(new Vector2f(windowX, windowY),
        this);
    float dx = (pos.x - x) / w;
    setValue(dx);
    updateKnob();
    if (command != null) {
      command.execute();
    }
  }

  @Override
  public void dragFinished(float windowX, float windowY) {
    // Do nothing
  }

  @Override
  public void onClick(int button, float windowX, float windowY) {
    // Do nothing. This exists so that when we click on the Slider it will drop
    // the focus from other Clickables.
  }

  @Override
  public boolean isActive() {
    return active;
  }

  @Override
  public void setActive(boolean b) {
    active = b;
  }
}
