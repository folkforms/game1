package game1.toolkit.widgets.label;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import game1.datasets.MultiLoader;

/**
 * Loads a CSV file of the form: {@code id, text} and creates suppliers for Labels.
 */
public class LabelMultiLoader extends MultiLoader {

  @Override
  public Map<String, Supplier<?>> load(String path) throws IOException {
    this.path = path;
    List<String> list = fileSystem.loadList(path);
    List<Label> labels = new ArrayList<>();
    Map<String, Supplier<?>> suppliers = new HashMap<>();
    list.forEach(line -> {
      String[] tokens = line.split(",\\s*", 2);
      Label label = LabelBuilder.init(tokens[1]).build();
      labels.add(label);
      suppliers.put(tokens[0], () -> label);
    });
    suppliers.put(path, () -> labels);
    return suppliers;
  }
}
