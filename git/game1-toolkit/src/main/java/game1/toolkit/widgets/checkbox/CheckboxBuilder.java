package game1.toolkit.widgets.checkbox;

import java.util.HashMap;
import java.util.Map;

import game1.events.Command;
import game1.toolkit.widgets.label.Label;

public class CheckboxBuilder {
  private Map<String, Object> details = new HashMap<>();

  public static CheckboxBuilder init(Label label) {
    CheckboxBuilder builder = new CheckboxBuilder();
    builder.label(label);
    builder.checked(false);
    builder.hGap(Checkbox.DEFAULT_HORIZONTAL_GAP);
    builder.vIconBoost(Checkbox.DEFAULT_VERTICAL_ICON_BOOST);
    builder.images("checkbox_checked.png", "checkbox_unchecked.png");
    builder.command(null);
    return builder;
  }

  private CheckboxBuilder label(Label label) {
    details.put("label", label);
    return this;
  }

  public CheckboxBuilder checked(boolean checked) {
    details.put("checked", checked);
    return this;
  }

  public CheckboxBuilder hGap(int hGap) {
    details.put("hGap", hGap);
    return this;
  }

  public CheckboxBuilder vIconBoost(int vIconBoost) {
    details.put("vIconBoost", vIconBoost);
    return this;
  }

  public CheckboxBuilder images(String checkedImageRef, String uncheckedImageRef) {
    details.put("checkedImageRef", checkedImageRef);
    details.put("uncheckedImageRef", uncheckedImageRef);
    return this;
  }

  public CheckboxBuilder command(Command command) {
    details.put("command", command);
    return this;
  }

  public Checkbox build() {
    Label label = (Label) details.get("label");
    Boolean checked = (Boolean) details.get("checked");
    Integer hGap = (Integer) details.get("hGap");
    Integer vIconBoost = (Integer) details.get("vIconBoost");
    String checkedImageRef = (String) details.get("checkedImageRef");
    String uncheckedImageRef = (String) details.get("uncheckedImageRef");
    Command command = (Command) details.get("command");
    return new Checkbox(label, checked, hGap, vIconBoost, checkedImageRef, uncheckedImageRef,
        command);
  }
}
