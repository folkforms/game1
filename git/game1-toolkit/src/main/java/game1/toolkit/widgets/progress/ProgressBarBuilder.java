package game1.toolkit.widgets.progress;

import org.joml.Vector2f;
import org.joml.Vector3f;

import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.pipelines.GraphicsPipelineName;
import game1.core.resources.builders.PrimitiveBuilder;

public class ProgressBarBuilder {
  private Vector3f position;
  private Vector2f size;
  private Colour barColour = Colour.fromHex("#cc0e13");
  private Colour backgroundColour = Colour.fromHex("#2b0000");
  private int delta = 0;
  private boolean horizontal = true;

  public static ProgressBarBuilder init() {
    return new ProgressBarBuilder();
  }

  /**
   * Controls the rate at which the bar "fills up" when the values changes. If you set the delta to,
   * say, 3 it will fill by 3 pixels per tick until the desired size is reached. A delta of zero
   * means it will jump straight to the appropriate width.
   */
  public ProgressBarBuilder delta(int delta) {
    this.delta = delta;
    return this;
  }

  public ProgressBarBuilder vertical() {
    this.horizontal = false;
    return this;
  }

  public ProgressBarBuilder position(Vector3f position) {
    this.position = position;
    return this;
  }

  public ProgressBarBuilder size(Vector2f size) {
    this.size = size;
    return this;
  }

  public ProgressBarBuilder colours(Colour barColour, Colour backgroundColour) {
    this.barColour = barColour;
    this.backgroundColour = backgroundColour;
    return this;
  }

  public ProgressBar build() {
    if (position == null) {
      throw new RuntimeException("ProgressBarBuilder.position must be called");
    }
    if (size == null) {
      throw new RuntimeException("ProgressBarBuilder.size must be called");
    }
    Drawable bar = createRectangle(barColour);
    Drawable background = createRectangle(backgroundColour);
    bar.setPosition(position.x, position.y, position.z + 1);
    background.setPosition(position);
    return new ProgressBar(bar, background, delta, horizontal);
  }

  private Drawable createRectangle(Colour colour) {
    return PrimitiveBuilder.init(GraphicsPipelineName.SCENE_2D).rect2d(size.x, size.y)
        .position(position.x, position.y, position.z).colour(colour).build();
  }
}
