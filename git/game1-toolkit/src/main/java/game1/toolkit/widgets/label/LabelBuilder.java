package game1.toolkit.widgets.label;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.joml.Vector3f;

import game1.core.graphics.Colour;
import game1.core.graphics.FontUtils;
import game1.core.graphics.RenderedFont;
import game1.core.graphics.meshes.Mesh;
import game1.core.graphics.meshes.MultiLineStringMeshData;

public class LabelBuilder {

  private String text;
  private RenderedFont font = FontUtils.DEFAULT_FONT_FACE;
  private float fontSize = FontUtils.DEFAULT_FONT_SIZE;
  private Vector3f position = new Vector3f();
  private Colour colour = new Colour(1, 1, 1);
  private boolean modifiable = false;

  private LabelBuilder() {
  }

  public static LabelBuilder init(String text) {
    LabelBuilder labelBuilder = new LabelBuilder();
    labelBuilder.text = text;
    return labelBuilder;
  }

  public static LabelBuilder init(List<String> lines) {
    LabelBuilder labelBuilder = new LabelBuilder();
    labelBuilder.text = lines.stream().collect(Collectors.joining("\n"));
    return labelBuilder;
  }

  public static LabelBuilder init(String[] lines) {
    LabelBuilder labelBuilder = new LabelBuilder();
    labelBuilder.text = Arrays.stream(lines).collect(Collectors.joining("\n"));
    return labelBuilder;
  }

  public LabelBuilder setFont(RenderedFont font) {
    this.font = font;
    return this;
  }

  public LabelBuilder setFontSize(float fontSize) {
    this.fontSize = fontSize;
    return this;
  }

  public LabelBuilder setPosition(float x, float y, float z) {
    this.position = new Vector3f(x, y, z);
    return this;
  }

  public LabelBuilder setPosition(Vector3f position) {
    this.position = new Vector3f(position.x, position.y, position.z);
    return this;
  }

  public LabelBuilder setColour(Colour colour) {
    this.colour = colour;
    return this;
  }

  public LabelBuilder setModifiable(boolean modifiable) {
    this.modifiable = modifiable;
    return this;
  }

  public Label build() {
    Mesh mesh = new MultiLineStringMeshData(text, font, fontSize).create(!modifiable);
    Label newLabel = new Label(text, font, fontSize, mesh, colour, modifiable);
    newLabel.setPosition(position);
    return newLabel;
  }

  public Label newTestInstance() {
    Label newLabel = new Label(text, font, fontSize, colour, modifiable);
    newLabel.setPosition(position);
    return newLabel;
  }
}
