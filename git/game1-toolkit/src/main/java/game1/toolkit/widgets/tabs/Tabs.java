package game1.toolkit.widgets.tabs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import game1.actors.Actor;
import game1.core.graphics.FontUtils;
import game1.core.graphics.RenderedFont;
import game1.events.Command;

/**
 * Represents a set of tabs. Use {@link #addTab(String, TabPanel)} to add contents.
 */
public class Tabs implements Actor {

  private List<TabButton> tabButtons;
  private Map<String, Actor> panels;
  private int x, y, z, tabWidth, tabHeight;
  protected String selectedTab = null;
  protected RenderedFont font = FontUtils.DEFAULT_FONT_FACE;
  protected float scale = FontUtils.DEFAULT_FONT_SIZE;
  private Command onChange;

  public Tabs(int x, int y, int z, int tabWidth, int tabHeight) {
    this.tabButtons = new ArrayList<>();
    this.panels = new HashMap<>();
    this.x = x;
    this.y = y;
    this.z = z;
    this.tabHeight = tabHeight;
    this.tabWidth = tabWidth;
  }

  /**
   * Adds a new tab with the given contents. You may wish to use a {@link Parent} to handle multiple
   * items as contents.
   *
   * @param name
   *          the name of the new table
   * @param contents
   *          the contents of the new tab
   */
  public void addTab(String name, Actor panel) {
    int newTabX = x + panels.size() * tabWidth;
    int newTabY = y;
    TabButton tabButton = new TabButton(this, name, newTabX, newTabY, z + 1, tabWidth, tabHeight);
    tabButtons.add(tabButton);
    panels.put(name, panel);
    addChild(panel);
    addChild(tabButton);
    panel.setVisible(false);
  }

  public String getSelectedTab() {
    return selectedTab;
  }

  public void setSelectedTab(String name) {
    if (!name.equals(selectedTab)) {
      if (selectedTab != null) {
        Actor currentPanel = panels.get(selectedTab);
        currentPanel.setVisible(false);
      }
      Actor newPanel = panels.get(name);
      newPanel.setVisible(true);

      selectedTab = name;
      if (onChange != null) {
        onChange.execute();
      }

      // Update colours
      for (int i = 0; i < tabButtons.size(); i++) {
        TabButton tabButton = tabButtons.get(i);
        tabButton.setSelected(tabButton.getName().equals(selectedTab));
      }
    }
  }

  public void setOnChangeCommand(Command onChange) {
    this.onChange = onChange;
  }
}
