package game1.toolkit.widgets.checkbox;

import org.joml.Vector3f;

import game1.actors.Actor;
import game1.actors.Clickable;
import game1.core.graphics.Image;
import game1.core.input.Mouse;
import game1.datasets.GameDataFactory;
import game1.events.Command;
import game1.primitives.Rectangle;
import game1.primitives.Shape;
import game1.toolkit.widgets.label.Label;

/**
 * A checkbox is a widget that can be either checked or unchecked. Clicking on the checkbox changes
 * its state to the opposite of the current state.
 */
public class Checkbox implements Actor, Clickable {

  public static final int DEFAULT_HORIZONTAL_GAP = 8;
  public static final int DEFAULT_VERTICAL_ICON_BOOST = 0;

  private Label label;
  private boolean checked;
  private Image checkedDrawable, uncheckedDrawable;
  private Rectangle shape;
  private Command command;
  private boolean active = true;
  protected boolean isResolutionAffected = true;
  protected boolean isViewportAffected = false;

  /**
   * Creates a {@link Checkbox} from the given parameters. The {@link CheckboxBuilder} class can be
   * used to avoid specifying all parameters.
   *
   * @param label
   *          checkbox label
   * @param checked
   *          initial state of the checkbox
   * @param hGap
   *          horizontal gap between image and text
   * @param checkedImageRef
   *          image to use for checked state
   * @param uncheckedImageRef
   *          image to use for unchecked state
   * @param command
   *          optional command to execute when the checkbox state changes
   */
  Checkbox(Label label, boolean checked, int hGap, int vIconBoost, String checkedImageRef,
      String uncheckedImageRef, Command command) {
    Vector3f labelPos = label.getPosition();
    int x = (int) labelPos.x;
    int y = (int) labelPos.y;
    int z = (int) labelPos.z;
    this.label = label;
    this.checked = checked;
    this.command = command;
    checkedDrawable = GameDataFactory.getInstance().get(checkedImageRef);
    uncheckedDrawable = GameDataFactory.getInstance().get(uncheckedImageRef);

    int newLabelX = (int) (checkedDrawable.getWidth() + hGap + x);
    label.setPosition(newLabelX, y);

    shape = new Rectangle(x, y, (int) (uncheckedDrawable.getWidth() / uncheckedDrawable.getScale()
        + hGap + label.getWidth()), (int) label.getHeight());

    checkedDrawable.setPosition(x, y + vIconBoost, z);
    uncheckedDrawable.setPosition(x, y + vIconBoost, z);
    toggleImageVisibility();

    addChild(label);
    addChild(checkedDrawable);
    addChild(uncheckedDrawable);
  }

  private void toggleImageVisibility() {
    if (checked) {
      checkedDrawable.setVisible(true);
      uncheckedDrawable.setVisible(false);
    } else {
      checkedDrawable.setVisible(false);
      uncheckedDrawable.setVisible(true);
    }
  }

  @Override
  public Shape<Rectangle> getShape() {
    return shape;
  }

  public boolean isChecked() {
    return checked;
  }

  @Override
  public void onClick(int button, float windowX, float windowY) {
    if (button == Mouse.LEFT_BUTTON) {
      setChecked(!checked);
    }
  }

  private void setChecked(boolean newValue) {
    boolean oldState = checked;
    checked = newValue;
    toggleImageVisibility();
    if (checked != oldState && command != null) {
      command.execute();
    }
  }

  @Override
  public int getZ() {
    return (int) label.getPosition().z;
  }

  /**
   * Sets the command to execute when this checkbox's state changes.
   *
   * @param command
   *          command to execute when this checkbox's state changes
   */
  public void setCommand(Command command) {
    this.command = command;
  }

  @Override
  public boolean isActive() {
    return active;
  }

  @Override
  public void setActive(boolean b) {
    active = b;
  }
}
