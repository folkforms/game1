package game1.toolkit.widgets.tabs;

import game1.actors.Actor;
import game1.actors.Clickable;
import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.input.Mouse;
import game1.core.pipelines.GraphicsPipelineName;
import game1.core.resources.builders.PrimitiveBuilder;
import game1.primitives.Rectangle;
import game1.primitives.Shape;
import game1.toolkit.widgets.label.Label;
import game1.toolkit.widgets.label.LabelBuilder;

class TabButton implements Actor, Clickable {

  protected static Colour UNSELECTED_TAB_BACKGROUND = new Colour(0.1f, 0.1f, 0.1f);
  protected static Colour UNSELECTED_TAB_TEXT = new Colour(0.5f, 0.5f, 0.5f);
  protected static Colour SELECTED_TAB_BACKGROUND = new Colour(0.2f, 0.2f, 0.2f);
  protected static Colour SELECTED_TAB_TEXT = new Colour(0.9f, 0.9f, 0.9f);

  private Tabs parent;
  private String name;
  private int x, y, z, w, h;
  private Drawable buttonBG;
  private Label buttonText;
  private boolean selected = false;
  private boolean active = true;
  protected boolean isResolutionAffected = true;
  protected boolean isViewportAffected = false;

  public TabButton(Tabs parent, String name, int x, int y, int z, int w, int h) {
    this.parent = parent;
    this.name = name;
    this.x = x;
    this.y = y;
    this.z = z;
    this.w = w;
    this.h = h;

    // Button
    buttonBG = PrimitiveBuilder.init(GraphicsPipelineName.SCENE_2D).rect2d(w, h)
        .colour(UNSELECTED_TAB_BACKGROUND).build();
    buttonBG.setPosition(x, y, z - 1);
    addChild(buttonBG);

    // Label
    buttonText = LabelBuilder.init(name).setFontSize(2).setPosition(x, y, z).build();
    int textW = (int) buttonText.getWidth();
    int textH = (int) buttonText.getHeight();
    int textX = x + w / 2 - textW / 2;
    int textY = y + h / 2 - textH / 2;
    buttonText.setPosition(textX, textY, z);
    buttonText.setColour(UNSELECTED_TAB_TEXT);
    addChild(buttonText);
  }

  @Override
  public void onClick(int button, float windowX, float windowY) {
    if (button == Mouse.LEFT_BUTTON) {
      this.parent.setSelectedTab(name);
    }
  }

  @Override
  public Shape<Rectangle> getShape() {
    return new Rectangle(x, y, w, h);
  }

  @Override
  public int getZ() {
    return z;
  }

  public void setSelected(boolean newValue) {
    selected = newValue;
    if (selected) {
      buttonBG.setColour(SELECTED_TAB_BACKGROUND);
      buttonText.setColour(SELECTED_TAB_TEXT);
    } else {
      buttonBG.setColour(UNSELECTED_TAB_BACKGROUND);
      buttonText.setColour(UNSELECTED_TAB_TEXT);
    }
  }

  public String getName() {
    return name;
  }

  @Override
  public boolean isActive() {
    return active;
  }

  @Override
  public void setActive(boolean b) {
    active = b;
  }
}
