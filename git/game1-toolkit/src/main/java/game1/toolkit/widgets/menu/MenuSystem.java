package game1.toolkit.widgets.menu;

import java.util.Stack;

import folkforms.log.Log;
import game1.actors.Actor;
import game1.core.engine.Engine;
import game1.core.input.Mouse;
import game1.core.input.MouseFactory;
import game1.core.pipelines.GraphicsPipelineName;
import game1.core.resources.builders.PrimitiveBuilder;
import game1.datasets.GameDataFactory;
import game1.debug.DebugScopes;
import game1.events.EventSubscriptionsFactory;
import game1.events.Game1Events;
import game1.events.ReceiveEvent;
import game1.resolutions.layers.ResolutionLayers;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;

// FIXME STATES: This is not used anywhere
// FIXME STATES: This needs to be updated to reflect new immutable states and use setVisible instead
// of adding and removing from state. Also it should probably live in toolkit.
public class MenuSystem implements Actor {

  private Stack<Menu> openMenus = new Stack<>();
  private MenuMask mask;
  private boolean maskAdded = false;
  // FIXME This should not be hard-coded... it should come from a default Game1 variable
  private int maskOffset = 10;
  private Menu mainMenu;

  public MenuSystem() {
    EventSubscriptionsFactory.getInstance().subscribe(this);
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(ResolutionLayers.WINDOW_LAYER).getResolutionHints();
    // FIXME Push this into a MenuMask.init() method (why?)
    mask = new MenuMask(PrimitiveBuilder.init(GraphicsPipelineName.SCENE_2D)
        .rect2d(hints.getResolutionWidth(), hints.getResolutionHeight()).position(0, 0, 0)
        .colour(0, 0, 0, 0.5f).build());
    addChild(mask);
    mainMenu = GameDataFactory.getInstance().get("main_menu");
    addChild(mainMenu);
  }

  public void push(Menu menu) {
    Log.scoped(DebugScopes.MENUS, "MenuSystem: Push %s (isMainMenuState = %s, openMenus = %s)",
        menu, Engine.GAME_STATE.isMainMenuState(), openMenus);

    openMenus.push(menu);
    if (Engine.GAME_STATE.isMainMenuState()) {
      if (openMenus.size() > 1) {
        addOrUpdateMask();
      }
    } else {
      Engine.GAME_PAUSED = true;
      Log.scoped(DebugScopes.MENUS, "Stashing mouse settings");
      Mouse mouse = MouseFactory.getInstance();
      mouse.stashMouseSettings();
      mouse.setVisible(true);
      mouse.setMouseCamera(false);
      addOrUpdateMask();
    }
  }

  public void remove(Menu menu) {
    Log.scoped(DebugScopes.MENUS, "MenuSystem: Remove %s (isMainMenuState = %s, openMenus = %s)",
        menu, Engine.GAME_STATE.isMainMenuState(), openMenus);

    Menu peek = openMenus.peek();
    if (menu != peek) {
      throw new RuntimeException(String.format(
          "Attempted to remove menu %s but it was not the topmost menu (openMenus = %s)", menu,
          openMenus));
    }

    if (Engine.GAME_STATE.isMainMenuState() && openMenus.size() == 1) {
      return;
    }

    if (!Engine.GAME_STATE.isMainMenuState()) {
      Log.scoped(DebugScopes.MENUS, "Unstashing mouse settings");
      MouseFactory.getInstance().unstashMouseSettings();
    }

    openMenus.pop();
    if (openMenus.size() == 0) {
      removeMask();
      Engine.GAME_PAUSED = false;
    } else if (Engine.GAME_STATE.isMainMenuState() && openMenus.size() > 1) {
      updateMaskZIndex();
    } else if (Engine.GAME_STATE.isMainMenuState() && openMenus.size() == 1) {
      removeMask();
    } else {
      updateMaskZIndex();
    }
  }

  private void addOrUpdateMask() {
    if (!maskAdded) {
      mask.setActive(true);
      mask.setVisible(true);
      maskAdded = true;
    }
    updateMaskZIndex();
  }

  private void updateMaskZIndex() {
    mask.setZ(openMenus.peek().getZ() - maskOffset);
  }

  private void removeMask() {
    mask.setActive(false);
    mask.setVisible(false);
    maskAdded = false;
  }

  @ReceiveEvent(Game1Events.TOGGLE_MENU)
  public void toggleMenu() {
    if (Engine.GAME_STATE.isMainMenuState() == false && openMenus.size() == 0) {
      mainMenu.setActive(true);
      mainMenu.setVisible(true);
    } else if (Engine.GAME_STATE.isMainMenuState() == true && openMenus.size() == 1) {
      // Do nothing
    } else {
      openMenus.peek().setVisible(false);
    }
  }
}
