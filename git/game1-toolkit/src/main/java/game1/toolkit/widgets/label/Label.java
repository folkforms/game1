package game1.toolkit.widgets.label;

import game1.core.engine.Engine;
import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.graphics.RenderedFont;
import game1.core.graphics.meshes.Mesh;
import game1.core.graphics.meshes.MultiLineStringMeshData;
import game1.core.pipelines.GraphicsPipelineName;

public class Label extends Drawable {

  protected String text;
  protected RenderedFont font;
  protected float fontSize;
  protected boolean modifiable;

  Label(String text, RenderedFont font, float fontSize, Mesh mesh, Colour colour,
      boolean modifiable) {
    super(GraphicsPipelineName.SCENE_2D, mesh);
    this.text = text;
    this.font = font;
    this.fontSize = fontSize;
    this.colour = colour;
    this.modifiable = modifiable;
    this.name = toString();
  }

  /**
   * This constructor is only used by test instances. It has no mesh and we make up the width and
   * height.
   */
  Label(String text, RenderedFont font, float fontSize, Colour colour, boolean modifiable) {
    super(GraphicsPipelineName.SCENE_2D, new Mesh[0]);
    this.text = text;
    this.font = font;
    this.fontSize = fontSize;
    this.colour = colour;
    this.modifiable = modifiable;
    this.name = toString();
    this.width = text.length() * 10;
    this.height = 10;
  }

  public void setText(String newText) {
    if (!modifiable) {
      throw new RuntimeException(
          String.format("%s.setText('%s') called but the label is not modifiable",
              getClass().getSimpleName(), newText));
    }
    Engine.PIPELINES.removeDrawable(this);
    Mesh newMesh = new MultiLineStringMeshData(newText, font, fontSize).create(false);
    Mesh[] oldMeshes = super.meshes;
    for (int i = 0; i < oldMeshes.length; i++) {
      oldMeshes[i].cleanup();
    }
    super.meshes = new Mesh[] { newMesh };
    Engine.PIPELINES.addDrawable(this);
  }

  @Override
  public String toString() {
    return String.format("%s[text='%s',position=%s]@%s", getClass().getSimpleName(),
        text.replaceAll("\\n", "\\\\n"), position, hashCode());
  }
}
