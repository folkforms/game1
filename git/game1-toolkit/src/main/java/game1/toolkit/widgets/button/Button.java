package game1.toolkit.widgets.button;

import org.joml.Vector2f;
import org.joml.Vector3f;

import game1.actors.Actor;
import game1.actors.Clickable;
import game1.actors.HasSize;
import game1.actors.Moveable;
import game1.actors.Tickable;
import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.graphics.FontUtils;
import game1.core.graphics.RenderedFont;
import game1.core.input.Mouse;
import game1.core.input.MouseFactory;
import game1.core.input.MouseUtils;
import game1.core.pipelines.GraphicsPipelineName;
import game1.core.registries.ClickableActors;
import game1.core.registries.TickableAnnotations;
import game1.core.resources.builders.PrimitiveBuilder;
import game1.events.Command;
import game1.primitives.Rectangle;
import game1.primitives.Shape;
import game1.toolkit.widgets.label.Label;
import game1.toolkit.widgets.label.LabelBuilder;

/**
 * Standard button class. Subclasses may wish to override {@link #drawNormal}, {@link #drawHover},
 * {@link #drawActive} and {@link #drawText} methods.
 *
 * Note: This class deliberately does not have a builder pattern. Buttons rarely use default values,
 * so any use of the pattern will call virtually all of the builder's "set" methods and thus defeat
 * the point of having a builder pattern. In addition, this simple Button class will usually want to
 * be subclassed and it is difficult to subclass a builder pattern.
 */
public class Button implements Actor, Clickable, Tickable, Moveable, HasSize {

  protected int x, y, z, w, h;
  protected Command command = null;
  protected Rectangle shape;
  protected float scale;
  protected boolean active = true;
  protected boolean isResolutionAffected = true;
  protected boolean isViewportAffected = false;

  protected final static int STATE_DISABLED = -1;
  protected final static int STATE_NORMAL = 0;
  protected final static int STATE_HOVER = 1;
  protected final static int STATE_ACTIVE = 2;
  protected int currentState = STATE_NORMAL;

  protected Drawable buttonBody;
  protected Label buttonText;

  // Palette: images/palettes/goosebumps-gold-32x.png
  protected Colour c0 = Colour.fromHex("#d19f22");
  protected Colour c1 = Colour.fromHex("#e35d08");
  protected Colour c2 = Colour.fromHex("#c82108");
  protected Colour c3 = Colour.fromHex("#941434");
  protected Colour c4 = Colour.fromHex("#21191c");
  protected Colour c5 = Colour.fromHex("#372365");
  protected Colour c6 = Colour.fromHex("#404b77");
  protected Colour c7 = Colour.fromHex("#5a85a4");

  protected Colour bodyColourNormal = c6;
  protected Colour bodyColourHover = c3;
  protected Colour bodyColourActive = c1;
  protected Colour bodyColourDisabled = c4;

  protected Colour textColourNormal = c0;
  protected Colour textColourHover = c1;
  protected Colour textColourActive = c0;
  protected Colour textColourDisabled = c5;

  /**
   * Creates a button.
   *
   * @param text
   *          button text
   * @param fontFace
   *          font face
   * @param fontScale
   *          font scale
   * @param x
   *          button X position
   * @param y
   *          button Y position
   * @param z
   *          button Z position
   * @param w
   *          button width
   * @param h
   *          button height
   */
  public Button(String text, RenderedFont font, float scale, int x, int y, int z, int w, int h) {
    this.x = x;
    this.y = y;
    this.z = z;
    this.w = w;
    this.h = h;
    updateShape();
    this.scale = scale;

    buttonBody = PrimitiveBuilder.init(GraphicsPipelineName.SCENE_2D).rect2d(w, h)
        .colour(bodyColourNormal).position(x, y, z).build();
    addChild(buttonBody);

    buttonText = LabelBuilder.init(text).setFont(font).setFontSize(scale).setPosition(x, y, z + 1)
        .build();
    buttonText.setColour(textColourNormal);
    int textWidth = FontUtils.getStringWidth(text, font, scale);
    int textHeight = FontUtils.getStringHeight(font, scale);
    int textX = x + w / 2 - textWidth / 2;
    int textY = y + h / 2 - textHeight / 2 - (int) scale;
    buttonText.setPosition(textX, textY, z + 1);
    addChild(buttonText);

    updateDrawablesBasedOnState();
  }

  /**
   * Creates a button with center-aligned text in the default font.
   *
   * @param text
   *          button text
   * @param fontScale
   *          font scale
   * @param x
   *          button X position
   * @param y
   *          button Y position
   * @param z
   *          button Z position
   * @param w
   *          button width
   * @param h
   *          button height
   */
  public Button(String text, float fontScale, int x, int y, int z, int w, int h) {
    this(text, FontUtils.DEFAULT_FONT_FACE, fontScale, x, y, z, w, h);
  }

  /**
   * Creates a button with center-aligned text in the default font with the default scale.
   *
   * @param text
   *          button text
   * @param x
   *          button X position
   * @param y
   *          button Y position
   * @param z
   *          button Z position
   * @param w
   *          button width
   * @param h
   *          button height
   */
  public Button(String text, int x, int y, int z, int w, int h) {
    this(text, FontUtils.DEFAULT_FONT_FACE, FontUtils.DEFAULT_FONT_SIZE, x, y, z, w, h);
  }

  /**
   * Sets the {@link Command} to be executed when this button is clicked.
   *
   * @param c
   *          Command object
   */
  public void setCommand(Command c) {
    this.command = c;
  }

  private void updateShape() {
    shape = new Rectangle(x, y, w, h);
  }

  @Override
  public Shape<Rectangle> getShape() {
    return buttonBody.isVisible() ? shape : null;
  }

  @Override
  public void onClick(int button, float windowX, float windowY) {
    if (buttonBody.isVisible() && currentState != STATE_DISABLED && command != null
        && button == Mouse.LEFT_BUTTON) {
      command.execute();
    }
  }

  /**
   * Enables or disables this button.
   *
   * @param enabled
   *          new enabled state
   */
  public void setEnabled(boolean enabled) {
    currentState = enabled ? STATE_NORMAL : STATE_DISABLED;
    // Change colour to match state
    if (currentState != STATE_DISABLED) {
      updateStateBasedOnMouseActivity();
    } else {
      updateDrawablesBasedOnState();
    }
  }

  @TickableAnnotations.TickEvenIfGamePaused
  @Override
  public void onTick() {
    updateStateBasedOnMouseActivity();
  }

  /**
   * Updates the colour based on the mouse activity, i.e. normal, hover or active colour.
   */
  private void updateStateBasedOnMouseActivity() {
    if (currentState != STATE_DISABLED) {
      Vector2f xy = MouseUtils
          .translateToResolutionLayerPosition(MouseFactory.getInstance().getWindowPos(), this);
      if (ClickableActors.getMouseDownItem() == this) {
        currentState = STATE_ACTIVE;
        updateDrawablesBasedOnState();
      } else if (getShape() != null && getShape().contains(xy)) {
        currentState = STATE_HOVER;
        updateDrawablesBasedOnState();
      } else {
        currentState = STATE_NORMAL;
        updateDrawablesBasedOnState();
      }
    }
  }

  @Override
  public int getZ() {
    return z;
  }

  private void updateDrawablesBasedOnState() {
    if (currentState == STATE_NORMAL) {
      buttonBody.setColour(bodyColourNormal);
      buttonText.setColour(textColourNormal);
    } else if (currentState == STATE_HOVER) {
      buttonBody.setColour(bodyColourHover);
      buttonText.setColour(textColourHover);
    } else if (currentState == STATE_ACTIVE) {
      buttonBody.setColour(bodyColourActive);
      buttonText.setColour(textColourActive);
    } else if (currentState == STATE_DISABLED) {
      buttonBody.setColour(bodyColourDisabled);
      buttonText.setColour(textColourDisabled);
    }
  }

  @Override
  public void setVisible(boolean visible) {
    buttonBody.setVisible(visible);
    buttonText.setVisible(visible);
  }

  @Override
  public Vector3f getPosition() {
    return new Vector3f(x, y, z);
  }

  @Override
  public void setPosition(float x, float y, float z) {
    float dx = x - this.x;
    float dy = y - this.y;
    float dz = z - this.z;
    Vector3f buttonBodyPos = buttonBody.getPosition();
    buttonBody.setPosition(buttonBodyPos.x + dx, buttonBodyPos.y + dy, buttonBodyPos.z + dz);
    Vector3f buttonTextPos = buttonText.getPosition();
    buttonText.setPosition(buttonTextPos.x + dx, buttonTextPos.y + dy, buttonTextPos.z + dz);
    this.x = (int) x;
    this.y = (int) y;
    this.z = (int) z;
    updateShape();
  }

  @Override
  public float getWidth() {
    return shape.w;
  }

  @Override
  public float getHeight() {
    return shape.h;
  }

  @Override
  public boolean isActive() {
    return active;
  }

  @Override
  public void setActive(boolean b) {
    active = b;
  }

  @Override
  public String toString() {
    return String.format(
        "%s[xyz=%s,%s,%s,wh=%s,%s,command=%s,shape=%s,scale=%s,active=%s,isResolutionAffected=%s,isViewportAffected=%s,buttonBody=%s,buttonText=%s]",
        getClass().getSimpleName(), x, y, z, w, h, command, shape, scale, active,
        isResolutionAffected, isViewportAffected, buttonBody, buttonText);
  }
}
