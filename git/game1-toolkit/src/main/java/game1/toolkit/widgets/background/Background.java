package game1.toolkit.widgets.background;

import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.pipelines.GraphicsPipelineName;
import game1.core.resources.builders.PrimitiveBuilder;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;

/**
 * Background is a simple rectangle of any colour.
 */
public class Background {

  public static Drawable create(int x, int y, int z, int w, int h, Colour colour) {
    return PrimitiveBuilder.init(GraphicsPipelineName.SCENE_2D).rect2d(w, h).position(x, y, z)
        .colour(colour).build();
  }

  public static Drawable create(int x, int y, int z, int w, int h, float r, float g, float b,
      float a) {
    return create(x, y, z, w, h, new Colour(r, g, b, a));
  }

  public static Drawable create(int x, int y, int z, int w, int h, float r, float g, float b) {
    return create(x, y, z, w, h, r, g, b, 1);
  }

  public static Drawable create(String layerName) {
    ResolutionHints hints = ResolutionLayersFactory.getInstance().getLayerByName(layerName)
        .getResolutionHints();
    int w = (int) hints.getResolutionWidth();
    int h = (int) hints.getResolutionHeight();
    return create(0, 0, 0, w, h, new Colour(0, 0, 0));
  }

  public static Drawable create(String layerName, int z) {
    ResolutionHints hints = ResolutionLayersFactory.getInstance().getLayerByName(layerName)
        .getResolutionHints();
    int w = (int) hints.getResolutionWidth();
    int h = (int) hints.getResolutionHeight();
    return create(0, 0, z, w, h, new Colour(0, 0, 0));
  }

  public static Drawable create(String layerName, float r, float g, float b) {
    ResolutionHints hints = ResolutionLayersFactory.getInstance().getLayerByName(layerName)
        .getResolutionHints();
    int w = (int) hints.getResolutionWidth();
    int h = (int) hints.getResolutionHeight();
    return create(0, 0, 0, w, h, new Colour(r, g, b));
  }

  public static Drawable create(String layerName, float r, float g, float b, float a) {
    ResolutionHints hints = ResolutionLayersFactory.getInstance().getLayerByName(layerName)
        .getResolutionHints();
    int w = (int) hints.getResolutionWidth();
    int h = (int) hints.getResolutionHeight();
    return create(0, 0, 0, w, h, new Colour(r, g, b, a));
  }

  public static Drawable create(String layerName, float r, float g, float b, float a, int z) {
    ResolutionHints hints = ResolutionLayersFactory.getInstance().getLayerByName(layerName)
        .getResolutionHints();
    int w = (int) hints.getResolutionWidth();
    int h = (int) hints.getResolutionHeight();
    return create(0, 0, z, w, h, new Colour(r, g, b, a));
  }

  public static Drawable create(String layerName, Colour colour) {
    ResolutionHints hints = ResolutionLayersFactory.getInstance().getLayerByName(layerName)
        .getResolutionHints();
    int w = (int) hints.getResolutionWidth();
    int h = (int) hints.getResolutionHeight();
    return create(0, 0, 0, w, h, colour);
  }
}
