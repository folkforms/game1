package game1.toolkit.widgets.menu;

import game1.actors.Active;
import game1.actors.Actor;

/**
 * Classes that implement {@link Menu} will be displayed on screen with a mask behind them.
 * Additionally when they are displayed the game will be paused and the mouse cursor will become
 * visible.
 */
public interface Menu extends Actor, Active {

  /**
   * Gets the z-index of the menu in order to calculate the z-index of the {@link MenuMask} (which
   * will be placed at {@code getZ() - 1}).
   *
   * @return z-index of the menu
   */
  public int getZ();
}
