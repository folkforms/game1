package game1.toolkit.widgets.menu;

import org.joml.Vector3f;

import game1.actors.Active;
import game1.actors.Actor;
import game1.actors.Clickable;
import game1.core.graphics.Drawable;
import game1.primitives.Rectangle;
import game1.primitives.Shape;
import game1.resolutions.layers.ResolutionLayers;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;

/**
 * A {@link MenuMask} sits behind a {@link Menu} to prevent the user from clicking on any items
 * while the menu is open. It is typically a full-screen semi-transparent rectangle.
 */
public class MenuMask implements Actor, Active, Clickable {

  private Rectangle shape;
  private Drawable drawable;
  private boolean active = true;
  protected boolean isResolutionAffected = true;
  protected boolean isViewportAffected = false;

  public MenuMask(Drawable drawable) {
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(ResolutionLayers.WINDOW_LAYER).getResolutionHints();
    shape = new Rectangle(0, 0, (int) hints.getResolutionWidth(),
        (int) hints.getResolutionHeight());
    this.drawable = drawable;
    addChild(drawable);
  }

  @Override
  public int getZ() {
    return (int) drawable.getPosition().z;
  }

  public void setZ(int newZ) {
    Vector3f p = drawable.getPosition();
    drawable.setPosition(p.x, p.y, newZ);
  }

  @Override
  public Shape<Rectangle> getShape() {
    return shape;
  }

  @Override
  public void onClick(int button, float windowX, float windowY) {
    // Do nothing
  }

  @Override
  public String toString() {
    return String.format("%s[x=%s,y=%s,z=%s,w=%s,h=%s]", this.getClass().getSimpleName(), shape.x,
        shape.y, drawable.getPosition().z, shape.w, shape.h);
  }

  @Override
  public boolean isActive() {
    return active;
  }

  @Override
  public void setActive(boolean b) {
    active = b;
  }
}
