package game1.toolkit.widgets.progress;

import org.joml.Vector3f;

import game1.actors.Actor;
import game1.actors.Tickable;
import game1.core.graphics.Drawable;
import game1.core.pipelines.Stencil2d;
import game1.toolkit.cutscenes.internal.Cutscene;

public class ProgressBar implements Actor, Tickable {

  private int currentValue = 0;
  private int maxValue = 100;
  private Drawable foregroundDrawable;
  private Drawable backgroundDrawable;
  private int delta;
  private boolean horizontal;

  public ProgressBar(Drawable foregroundDrawable, Drawable backgroundDrawable, int delta,
      boolean horizontal) {
    this.foregroundDrawable = foregroundDrawable;
    this.backgroundDrawable = backgroundDrawable;
    addChild(foregroundDrawable);
    addChild(backgroundDrawable);
    this.delta = delta;
    this.horizontal = horizontal;
    onTick();
  }

  public void setPosition(Vector3f position) {
    foregroundDrawable.setPosition(position);
    backgroundDrawable.setPosition(position);
  }

  public void setCurrentValue(int newValue) {
    currentValue = newValue;
  }

  public void setMaxValue(int newMaxValue) {
    maxValue = newMaxValue;
  }

  @Override
  public void onTick() {
    float barX = foregroundDrawable.getPosition().x;
    float barY = foregroundDrawable.getPosition().y;
    float percentFull = (float) currentValue / maxValue;
    if (delta > 0) {
      int localDelta = delta;
      if (Cutscene.isFastForwarding()) {
        localDelta = Cutscene.FAST_FORWARD_SPEED * delta;
      }
      float backgroundSize = horizontal ? backgroundDrawable.getWidth()
        : backgroundDrawable.getHeight();
      float barSize = horizontal ? foregroundDrawable.getWidth() : foregroundDrawable.getHeight();

      float desiredSize = backgroundSize * percentFull;
      if (barSize + delta > desiredSize) {
        localDelta = (int) (desiredSize - barSize);
      }
      if (barSize < desiredSize) {
        if (horizontal) {
          foregroundDrawable.setStencil(
              new Stencil2d(barX, barY, barSize + localDelta, foregroundDrawable.getHeight()));
        } else {
          foregroundDrawable.setStencil(
              new Stencil2d(barX, barY, foregroundDrawable.getWidth(), barSize + localDelta));
        }
      }
    } else {
      if (horizontal) {
        foregroundDrawable.setStencil(new Stencil2d(barX, barY,
            foregroundDrawable.getWidth() * percentFull, foregroundDrawable.getHeight()));
      } else {
        foregroundDrawable.setStencil(new Stencil2d(barX, barY, foregroundDrawable.getWidth(),
            foregroundDrawable.getHeight() * percentFull));
      }
    }
  }
}
