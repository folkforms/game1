package game1.toolkit.soundtest;

import game1.core.engine.Engine;
import game1.events.Command;
import game1.toolkit.internal.main.ToolkitZIndex;
import game1.toolkit.widgets.button.Button;

public class PlaySoundButton extends Button {

  public PlaySoundButton(String text, int x, int y) {
    super(text, 3, x, y, ToolkitZIndex.WIDGET, 80, 30);
    setCommand(new Command() {
      @Override
      public void execute() {
        Engine.SOUND_BOARD.play("resources-test/sounds/soft_beep.ogg", 0.5f);
      }
    });
  }
}
