package game1.toolkit.soundtest;

import game1.actors.Actor;
import game1.core.engine.Engine;
import game1.events.Command;
import game1.toolkit.internal.main.ToolkitZIndex;
import game1.toolkit.widgets.button.Button;

public class PauseAllSoundTest implements Actor {

  private int x, y;
  private int buttonWidth = 110;
  private int buttonHeight = 30;
  private Button playButton, pauseButton, unpauseButton;
  private String asset1, asset2;
  private float volume1, volume2;

  public PauseAllSoundTest(String asset1, float volume1, String asset2, float volume2, int x,
      int y) {
    this.asset1 = asset1;
    this.asset2 = asset2;
    this.volume1 = volume1;
    this.volume2 = volume2;
    this.x = x;
    this.y = y;
    addChild(createPlaySoundButton());
    addChild(createPauseSoundButton());
    addChild(createUnpauseSoundButton());
  }

  private class PlayCommand implements Command {
    @Override
    public void execute() {
      Engine.SOUND_BOARD.play(asset1, String.format("%s_pause_all_sound_test_1", asset1), volume1);
      Engine.SOUND_BOARD.play(asset2, String.format("%s_pause_all_sound_test_2", asset2), volume2);
      playButton.setCommand(null);
      playButton.setEnabled(false);
      pauseButton.setEnabled(true);
      unpauseButton.setEnabled(false);
    }
  }

  private Actor createPlaySoundButton() {
    playButton = new Button("Play", 3, x, y, ToolkitZIndex.WIDGET, buttonWidth, buttonHeight);
    playButton.setCommand(new PlayCommand());
    return playButton;
  }

  private Actor createPauseSoundButton() {
    pauseButton = new Button("Pause", 3, x + buttonWidth + 10, y, ToolkitZIndex.WIDGET, buttonWidth,
        buttonHeight);
    pauseButton.setCommand(new Command() {
      @Override
      public void execute() {
        Engine.SOUND_BOARD.pauseAll();
        playButton.setCommand(null);
        playButton.setEnabled(false);
        pauseButton.setEnabled(false);
        unpauseButton.setEnabled(true);
      }
    });
    pauseButton.setEnabled(false);
    return pauseButton;
  }

  private Actor createUnpauseSoundButton() {
    unpauseButton = new Button("Unpause", 3, x + buttonWidth * 2 + 10 * 2, y, ToolkitZIndex.WIDGET,
        buttonWidth, buttonHeight);
    unpauseButton.setCommand(new Command() {
      @Override
      public void execute() {
        Engine.SOUND_BOARD.unpauseAll();
        playButton.setCommand(new PlayCommand());
        playButton.setEnabled(false);
        pauseButton.setEnabled(true);
        unpauseButton.setEnabled(false);
      }
    });
    unpauseButton.setEnabled(false);
    return unpauseButton;
  }
}
