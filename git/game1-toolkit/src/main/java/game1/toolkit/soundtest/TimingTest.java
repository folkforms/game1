package game1.toolkit.soundtest;

import game1.actors.Actor;
import game1.actors.Clickable;
import game1.actors.Tickable;
import game1.core.engine.Engine;
import game1.primitives.Rectangle;
import game1.primitives.Shape;
import game1.toolkit.internal.main.ToolkitZIndex;

public class TimingTest implements Actor, Tickable, Clickable {

  private int x, y, w, h;
  private Rectangle shape;
  private int blockSize = 20;
  private long numTicks = 0;
  private boolean isPlaying = false;
  private boolean active = true;
  protected boolean isViewportAffected = true;
  protected boolean isResolutionAffected = true;

  private int soundIndex = 0;
  private int totalSounds = 4;

  public TimingTest(int x, int y) {
    this.x = x;
    this.y = y;
    this.w = totalSounds * blockSize;
    this.h = blockSize;
    shape = new Rectangle(x, y, w, h);
  }

  // @Override
  // public void draw(Graphics g) {
  // g.setColour(1, 1, 1);
  // g.setFont(FontUtils.DEFAULT_FONT_FACE, FontUtils.DEFAULT_FONT_SCALE);
  // g.drawString(String.format("Timing test (%s)", numTicks / 10), x, y + blockSize + 4);
  // g.setColour(1, 0, 0);
  // g.fillRect(x, y, w, h);
  // if (isPlaying) {
  // g.setColour(0, 1, 0);
  // for (int i = 0; i < soundIndex; i++) {
  // g.fillRect(x, y, soundIndex * blockSize, blockSize);
  // }
  // }
  // }

  @Override
  public void onTick() {
    if (isPlaying) {
      numTicks++;
      if (numTicks > soundIndex * 100) {
        Engine.SOUND_BOARD.play("resources-test/sounds/soft_beep.ogg", 0.5f);
        soundIndex++;
      }
      if (numTicks >= totalSounds * 100) {
        numTicks = 0;
        soundIndex = 0;
        isPlaying = false;
      }
    }
  }

  @Override
  public int getZ() {
    return ToolkitZIndex.WIDGET;
  }

  @Override
  public Shape<?> getShape() {
    return shape;
  }

  @Override
  public void onClick(int button, float windowX, float windowY) {
    if (!isPlaying) {
      isPlaying = true;
    }
  }

  @Override
  public boolean isActive() {
    return active;
  }

  @Override
  public void setActive(boolean b) {
    active = b;
  }
}
