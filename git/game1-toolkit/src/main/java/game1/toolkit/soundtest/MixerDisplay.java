package game1.toolkit.soundtest;

import game1.actors.Actor;
import game1.actors.Tickable;
import game1.core.engine.Engine;

public class MixerDisplay implements Actor, Tickable {

  private String masterVolume = "";
  private String totalEffectsVolume = "";
  private String totalMusicVolume = "";
  private int x, y;

  public MixerDisplay(int x, int y) {
    this.x = x;
    this.y = y;
  }

  // @Override
  // public void draw(Graphics g) {
  // g.setColour(1, 1, 1);
  // g.setFont(FontUtils.DEFAULT_FONT_FACE, 3);
  // g.drawString(masterVolume, x, y + 40);
  // g.drawString(totalEffectsVolume, x, y + 20);
  // g.drawString(totalMusicVolume, x, y);
  // }

  @Override
  public void onTick() {
    float master = Engine.MIXER.getVolume("master");
    float effects = Engine.MIXER.getVolume("effects");
    float music = Engine.MIXER.getVolume("music");
    masterVolume = String.format("Master: %s", master);
    totalEffectsVolume = String.format("Effects: %s (%s)", effects, effects * master);
    totalMusicVolume = String.format("Music: %s (%s)", music, music * master);
  }

  // @Override
  // public int getZ() {
  // return ToolkitZIndex.WIDGET;
  // }
}
