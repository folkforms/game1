package game1.toolkit.soundtest;

import game1.core.engine.Engine;
import game1.events.Command;

public class MasterVolumeButton extends VolumeButton {

  public MasterVolumeButton(float volume, int x, int y, int w, int h) {
    super(volume, x, y, w, h);
    this.command = new Command() {
      @Override
      public void execute() {
        Engine.MIXER.setVolume("master", volume);
      }
    };
  }
}
