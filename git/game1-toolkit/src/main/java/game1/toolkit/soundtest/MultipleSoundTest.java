package game1.toolkit.soundtest;

import game1.actors.Actor;
import game1.core.engine.Engine;
import game1.events.Command;
import game1.toolkit.internal.main.ToolkitZIndex;
import game1.toolkit.widgets.button.Button;

// FIXME SOUND: Update this to have buttons for 1 sound, 2 sounds, 3 sounds, etc.
public class MultipleSoundTest implements Actor {

  private int x, y;
  private int buttonWidth = 70;
  private int buttonHeight = 30;
  // FIXME private OldTextField inputField;
  private Button playButton, stopButton, resetButton;
  private int numSounds = 0;

  public MultipleSoundTest(int x, int y) {
    this.x = x;
    this.y = y;
    // FIXME addChild(createInputField());
    addChild(createPlayButton());
    addChild(createStopButton());
    addChild(createResetButton());
  }

  // FIXME
  // private Actor createInputField() {
  // inputField = new OldTextField("10", 3, x, y, ToolkitZIndex.WIDGET, buttonWidth, buttonHeight);
  // return inputField;
  // }

  private class PlayCommand implements Command {
    @Override
    public void execute() {
      String inputStr = "4"; // FIXME inputField.getText();
      numSounds = Integer.parseInt(inputStr);
      for (int i = 0; i < numSounds; i++) {
        Engine.SOUND_BOARD.play("resources-test/sounds/soft_beep.ogg",
            String.format("multiple_sound_test_%s", i), 0.2f);
      }
      playButton.setCommand(null);
      playButton.setEnabled(false);
      stopButton.setEnabled(true);
      resetButton.setEnabled(true);
    }
  }

  private Actor createPlayButton() {
    playButton = new Button("Play", 3, x + 100, y, ToolkitZIndex.WIDGET, buttonWidth, buttonHeight);
    playButton.setCommand(new PlayCommand());
    return playButton;
  }

  private Actor createStopButton() {
    stopButton = new Button("Stop", 3, x + 100 + buttonWidth + 10, y, ToolkitZIndex.WIDGET,
        buttonWidth, buttonHeight);
    stopButton.setCommand(new Command() {
      @Override
      public void execute() {
        for (int i = 0; i < numSounds; i++) {
          Engine.SOUND_BOARD.stop(String.format("multiple_sound_test_%s", i));
        }
        playButton.setCommand(new PlayCommand());
        playButton.setEnabled(true);
        stopButton.setEnabled(false);
        resetButton.setEnabled(false);
      }
    });
    stopButton.setEnabled(false);
    return stopButton;
  }

  private Actor createResetButton() {
    resetButton = new Button("Reset", 3, x + 100 + buttonWidth * 2 + 10 * 2, y,
        ToolkitZIndex.WIDGET, buttonWidth + 10, buttonHeight);
    resetButton.setCommand(new Command() {
      @Override
      public void execute() {
        playButton.setCommand(new PlayCommand());
        playButton.setEnabled(true);
        stopButton.setEnabled(false);
        resetButton.setEnabled(false);
      }
    });
    resetButton.setEnabled(false);
    return resetButton;
  }
}
