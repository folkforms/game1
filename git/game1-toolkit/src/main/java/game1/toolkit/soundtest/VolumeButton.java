package game1.toolkit.soundtest;

import game1.toolkit.internal.main.ToolkitZIndex;
import game1.toolkit.widgets.button.Button;

public class VolumeButton extends Button {

  protected float volume;
  protected static final float fontScale = 3;
  protected static final int z = ToolkitZIndex.WIDGET;

  public VolumeButton(float volume, int x, int y, int w, int h) {
    super(Float.toString(volume), fontScale, x, y, z, w, h);
    this.volume = volume;
  }
}
