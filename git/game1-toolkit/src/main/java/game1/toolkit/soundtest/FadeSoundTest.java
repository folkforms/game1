package game1.toolkit.soundtest;

import game1.actors.Actor;
import game1.core.engine.Engine;
import game1.events.Command;
import game1.toolkit.internal.main.ToolkitZIndex;
import game1.toolkit.widgets.button.Button;

public class FadeSoundTest implements Actor {

  private int x, y;
  private int buttonWidth = 70;
  private int buttonHeight = 30;
  private Button playButton, fadeButton;
  private String asset;
  private float volume;
  private long fadeTimeMs;

  public FadeSoundTest(String asset, float volume, long fadeTimeMs, int x, int y) {
    this.asset = asset;
    this.volume = volume;
    this.fadeTimeMs = fadeTimeMs;
    this.x = x;
    this.y = y;
    addChild(createPlaySoundButton());
    addChild(createFadeSoundButton());
  }

  private class PlayCommand implements Command {
    @Override
    public void execute() {
      Engine.SOUND_BOARD.play(asset, String.format("%s_fade_sound_test", asset), volume);
      playButton.setCommand(null);
      playButton.setEnabled(false);
      fadeButton.setEnabled(true);
    }
  }

  private Actor createPlaySoundButton() {
    playButton = new Button("Play", 3, x, y, ToolkitZIndex.WIDGET, buttonWidth, buttonHeight);
    playButton.setCommand(new PlayCommand());
    return playButton;
  }

  private Actor createFadeSoundButton() {
    fadeButton = new Button("Fade", 3, x + buttonWidth + 10, y, ToolkitZIndex.WIDGET, buttonWidth,
        buttonHeight);
    fadeButton.setCommand(new Command() {
      @Override
      public void execute() {
        Engine.SOUND_BOARD.fade(String.format("%s_fade_sound_test", asset), fadeTimeMs);
        playButton.setCommand(new PlayCommand());
        playButton.setEnabled(true);
        fadeButton.setEnabled(false);
      }
    });
    fadeButton.setEnabled(false);
    return fadeButton;
  }
}
