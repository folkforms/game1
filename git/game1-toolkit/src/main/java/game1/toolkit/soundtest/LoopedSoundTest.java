package game1.toolkit.soundtest;

import game1.actors.Actor;
import game1.core.engine.Engine;
import game1.events.Command;
import game1.toolkit.internal.main.ToolkitZIndex;
import game1.toolkit.widgets.button.Button;

public class LoopedSoundTest implements Actor {

  private int x, y;
  private int buttonWidth = 70;
  private int buttonHeight = 30;
  private Button loopButton, stopButton;
  private String asset;
  private float volume;

  public LoopedSoundTest(String asset, float volume, int x, int y) {
    this.asset = asset;
    this.volume = volume;
    this.x = x;
    this.y = y;
    addChild(createLoopedSoundButton());
    addChild(createStopSoundButton());
  }

  private class LoopCommand implements Command {
    @Override
    public void execute() {
      Engine.SOUND_BOARD.loop(asset, String.format("%s_looped_sound_test", asset), volume);
      loopButton.setCommand(null);
      loopButton.setEnabled(false);
      stopButton.setEnabled(true);
    }
  }

  private Actor createLoopedSoundButton() {
    loopButton = new Button("Loop", 3, x, y, ToolkitZIndex.WIDGET, buttonWidth, buttonHeight);
    loopButton.setCommand(new LoopCommand());
    return loopButton;
  }

  private Actor createStopSoundButton() {
    stopButton = new Button("Stop", 3, x + buttonWidth + 10, y, ToolkitZIndex.WIDGET, buttonWidth,
        buttonHeight);
    stopButton.setCommand(new Command() {
      @Override
      public void execute() {
        Engine.SOUND_BOARD.stop(String.format("%s_looped_sound_test", asset));
        loopButton.setCommand(new LoopCommand());
        loopButton.setEnabled(true);
        stopButton.setEnabled(false);
      }
    });
    stopButton.setEnabled(false);
    return stopButton;
  }
}
