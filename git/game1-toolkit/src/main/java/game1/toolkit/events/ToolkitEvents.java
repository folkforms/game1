package game1.toolkit.events;

public class ToolkitEvents {
  public static final String PLAYER_MOVE = "game1.toolkit.player_move";

  public static final String ANIMATION_TESTER_SELECT = "toolkit.animation_tester.select";
  public static final String ANIMATION_TESTER_MOVE = "toolkit.animation_tester.move";
  public static final String ANIMATION_TESTER_RESET_DELTAS = "toolkit.animation_tester.reset_deltas";
  public static final String ANIMATION_TESTER_PRINT_DATA = "toolkit.animation_tester.print_data";
}
