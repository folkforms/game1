package game1.toolkit.actors;

import org.joml.Vector3f;

import folkforms.maths.Graph;
import game1.actors.Actor;
import game1.actors.Moveable;
import game1.actors.Tickable;
import game1.core.graphics.CollisionMeshUtils;
import game1.events.EventSubscriptionsFactory;
import game1.events.EventsFactory;
import game1.events.Game1Events;
import game1.events.ReceiveEvent;
import game1.toolkit.cutscenes.internal.Cutscene;
import game1.toolkit.cutscenes.utils.CutsceneMoveable;
import game1.toolkit.cutscenes.utils.CutsceneRotatable;
import game1.toolkit.events.ToolkitEvents;

/**
 * Represents the player in the world. Can be used as a simple moveable camera.
 */
public class Player3D implements Actor, CutsceneMoveable, CutsceneRotatable, Moveable, Tickable {

  private final Vector3f position;
  private final Vector3f rotation;
  private Vector3f startPos, targetPos;
  private boolean isMoving = false;
  private long moveDuration = 0;
  private long numTicksMove = 0;
  private Vector3f startRot, targetRot;
  private boolean isRotating = false;
  private long rotationDuration = 0;
  private long numTicksRot = 0;
  private static final float[] ZERO_ONE = new float[] { 0, 1 };

  public float mouseSensitivity = 0.02f;

  public Player3D(float px, float py, float pz, float rx, float ry, float rz) {
    EventSubscriptionsFactory.getInstance().subscribe(this);
    position = new Vector3f(px, py, pz);
    rotation = new Vector3f(rx, ry, rz);
  }

  @ReceiveEvent(ToolkitEvents.PLAYER_MOVE)
  public void move(float offsetX, float offsetY, float offsetZ) {
    Vector3f allowedMove = CollisionMeshUtils.getAllowedMove(position, rotation, offsetX, offsetY,
        offsetZ);
    position.x = allowedMove.x;
    position.y = allowedMove.y;
    position.z = allowedMove.z;
    EventsFactory.getInstance().raise(Game1Events.CAMERA_SET_POSITION_V3F, position);
  }

  @ReceiveEvent(Game1Events.MOUSE_MOVE)
  public void rotate(float rx, float ry, float rz) {
    rotation.x += rx;
    rotation.y += ry;
    rotation.z += rz;
    EventsFactory.getInstance().raise(Game1Events.CAMERA_ROTATE, rx, ry, rz);
  }

  @Override
  public void onTick() {
    if (isMoving) {
      float newX = Graph.calculate(ZERO_ONE, new float[] { startPos.x, targetPos.x }, numTicksMove,
          moveDuration);
      float newY = Graph.calculate(ZERO_ONE, new float[] { startPos.y, targetPos.y }, numTicksMove,
          moveDuration);
      float newZ = Graph.calculate(ZERO_ONE, new float[] { startPos.z, targetPos.z }, numTicksMove,
          moveDuration);
      setPositionFloats(newX, newY, newZ);
      numTicksMove++;
      if (Cutscene.isFastForwarding()) {
        numTicksMove += Cutscene.FAST_FORWARD_SPEED;
      }
    }
    if (isMoving && numTicksMove >= moveDuration) {
      stopMoving();
    }

    if (isRotating) {
      float rx = Graph.calculate(ZERO_ONE, new float[] { startRot.x, targetRot.x }, numTicksRot,
          rotationDuration);
      float ry = Graph.calculate(ZERO_ONE, new float[] { startRot.y, targetRot.y }, numTicksRot,
          rotationDuration);
      float rz = Graph.calculate(ZERO_ONE, new float[] { startRot.z, targetRot.z }, numTicksRot,
          rotationDuration);
      setRotation(rx, ry, rz);
      numTicksRot++;
      if (Cutscene.isFastForwarding()) {
        numTicksRot += Cutscene.FAST_FORWARD_SPEED;
      }
    }
    if (isRotating && numTicksRot >= rotationDuration) {
      stopRotating();
    }
  }

  @Override
  public void startMoving(long duration, float targetX, float targetY) {
    startMoving(duration, targetX, targetY, position.z);
  }

  @Override
  public void startMoving(long duration, float targetX, float targetY, float targetZ) {
    numTicksMove = 0;
    moveDuration = duration;
    startPos = new Vector3f(position.x, position.y, position.z);
    targetPos = new Vector3f(targetX, targetY, targetZ);
    isMoving = true;
  }

  @Override
  public void stopMoving() {
    isMoving = false;
    setPosition(targetPos.x, targetPos.y, targetPos.z);
  }

  private void setPositionFloats(float newX, float newY, float newZ) {
    position.x = newX;
    position.y = newY;
    position.z = newZ;
    EventsFactory.getInstance().raise(Game1Events.CAMERA_SET_POSITION_V3F,
        new Vector3f(newX, newY, newZ));
  }

  @Override
  public void startRotating(long duration, float targetX, float targetY, float targetZ) {
    if (targetX - rotation.x > 180) {
      targetX -= 360;
    } else if (targetX - rotation.x < -180) {
      targetX += 360;
    }
    if (targetY - rotation.y > 180) {
      targetY -= 360;
    } else if (targetY - rotation.y < -180) {
      targetY += 360;
    }
    if (targetZ - rotation.z > 180) {
      targetZ -= 360;
    } else if (targetZ - rotation.z < -180) {
      targetZ += 360;
    }
    numTicksRot = 0;
    rotationDuration = duration;
    startRot = new Vector3f(rotation.x, rotation.y, rotation.z);
    targetRot = new Vector3f(targetX, targetY, targetZ);
    isRotating = true;
  }

  @Override
  public void stopRotating() {
    isRotating = false;
    setRotation(targetRot.x, targetRot.y, targetRot.z);
  }

  @Override
  public void setRotation(float degreesX, float degreesY, float degreesZ) {
    rotation.x = degreesX;
    rotation.y = degreesY;
    rotation.z = degreesZ;
    EventsFactory.getInstance().raise(Game1Events.CAMERA_SET_ROTATION_V3F,
        new Vector3f(rotation.x, rotation.y, rotation.z));
    // FIXME CUTSCENES: I feel like it should not be necessary to call updateCamera, since the
    // update thread calls it anyway. But this method is currently unused as the 3D cutscenes are
    // broken. Once they are fixed I can test it out.
    // Engine.CAMERA.updateCamera();
  }

  @Override
  public Vector3f getPosition() {
    return position;
  }

  public Vector3f getRotation() {
    return rotation;
  }

  @Override
  public void setPosition(float x, float y, float z) {
    position.x = x;
    position.y = y;
    position.z = z;
    EventsFactory.getInstance().raise(Game1Events.CAMERA_SET_POSITION_V3F, position);
  }
}
