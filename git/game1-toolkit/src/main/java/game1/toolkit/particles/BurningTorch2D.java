package game1.toolkit.particles;

public class BurningTorch2D extends BurningTorch {

  public BurningTorch2D(int x, int y, int z) {
    super(x, y, z);
    speedY = 1;
  }

  @Override
  protected void setDrawablePosition(int i) {
    drawables[i].setPosition(arrX[i], arrY[i]);
  }

  @Override
  protected void setNewPositions(int i) {
    float w = 4;
    arrX[i] = x + w - (float) (Math.random() * w * 2) + (float) (Math.random() * w * 2);
    arrY[i] = y + w - (float) (Math.random() * w * 2) + (float) (Math.random() * w * 2);
    setDrawablePosition(i);
  }
}
