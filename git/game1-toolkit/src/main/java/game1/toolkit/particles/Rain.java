package game1.toolkit.particles;

import game1.actors.Actor;
import game1.actors.Tickable;
import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.datasets.GameDataFactory;

public class Rain implements Actor, Tickable {

  private float x1, y1, z1, x2, y2, z2;

  private long numTicks = 0;
  private int count = 0;
  private int maxParticles = 100;
  private double fireChance = 0.5;
  private float speedX = -0.03f;
  private float speedY = -0.07f;
  private float speedZ = -0.03f;
  private final int tail = 40;
  private Colour c0 = new Colour(1, 1, 1, 0.5f);
  private Colour c1 = new Colour(1, 1, 1, 0.4f);
  private Colour c2 = new Colour(1, 1, 1, 0.3f);
  private Colour c3 = new Colour(1, 1, 1, 0.2f);
  private Colour c4 = new Colour(1, 1, 1, 0.1f);

  private float[] arrX, arrY, arrZ;
  private float[] splashX, splashY, splashZ, splashFrame;
  private int splashCount = 0;
  private Drawable[] drawables;

  public Rain(float x1, float y1, float z1, float x2, float y2, float z2) {
    this.x1 = x1;
    this.y1 = y1;
    this.z1 = z1;
    this.x2 = x2;
    this.y2 = y2;
    this.z2 = z2;
    arrX = new float[maxParticles];
    arrY = new float[maxParticles];
    arrZ = new float[maxParticles];
    splashX = new float[maxParticles];
    splashY = new float[maxParticles];
    splashZ = new float[maxParticles];
    splashFrame = new float[maxParticles];
    drawables = new Drawable[maxParticles * 5];

    for (int i = 0; i < maxParticles * 5; i++) {
      // FIXME DATASETS: Do we need getClone here now that we have suppliers?
      drawables[i] = ((Drawable) GameDataFactory.getInstance().get("toolkit.raindrop_particle"))
          .getClone();
      addChild(drawables[i]);
    }
    for (int i = 0; i < maxParticles; i++) {
      drawables[i * 5].setColour(c0);
      drawables[i * 5 + 1].setColour(c1);
      drawables[i * 5 + 2].setColour(c2);
      drawables[i * 5 + 3].setColour(c3);
      drawables[i * 5 + 4].setColour(c4);
    }
  }

  // @Override
  // public void draw(Graphics g) {
  // g.setStencil(x, y, w, h);
  // for (int i = 0; i < count; i++) {
  //
  // final int segment = tail / 5;
  // g.setColor(1, 1, 1, 0.5f);
  // g.fillRect(arrX[i], arrY[i], 3, segment);
  // g.setColor(1, 1, 1, 0.4f);
  // g.fillRect(arrX[i], arrY[i] + segment, 3, segment);
  // g.setColor(1, 1, 1, 0.3f);
  // g.fillRect(arrX[i], arrY[i] + segment * 2, 3, segment);
  // g.setColor(1, 1, 1, 0.2f);
  // g.fillRect(arrX[i], arrY[i] + segment * 3, 3, segment);
  // g.setColor(1, 1, 1, 0.1f);
  // g.fillRect(arrX[i], arrY[i] + segment * 4, 3, segment);
  // }
  //
  // // Render splashes
  // for (int i = 0; i < splashCount; i++) {
  // if (splashFrame[i] == 0) {
  // g.setColor(1, 1, 1, 0.5f);
  // g.fillRect(splashX[i], splashY[i], 3, 3);
  // } else if (splashFrame[i] == 1) {
  // g.setColor(1, 1, 1, 0.4f);
  // g.fillRect(splashX[i] - 3, splashY[i], 9, 3);
  // } else if (splashFrame[i] == 2) {
  // g.setColor(1, 1, 1, 0.3f);
  // g.fillRect(splashX[i] - 6, splashY[i] + 3, 3, 3);
  // g.fillRect(splashX[i], splashY[i], 3, 3);
  // g.fillRect(splashX[i] + 6, splashY[i] + 3, 3, 3);
  // } else if (splashFrame[i] == 3) {
  // g.setColor(1, 1, 1, 0.2f);
  // g.fillRect(splashX[i] - 9, splashY[i] + 3, 3, 3);
  // g.fillRect(splashX[i] + 9, splashY[i] + 3, 3, 3);
  // } else if (splashFrame[i] == 4) {
  // g.setColor(1, 1, 1, 0.1f);
  // g.fillRect(splashX[i] - 12, splashY[i], 3, 3);
  // g.fillRect(splashX[i] + 12, splashY[i], 3, 3);
  // }
  // }
  //
  // g.removeStencil();
  // }

  @Override
  public void onTick() {
    numTicks++;

    // Maybe add a new particle
    if (count < maxParticles && Math.random() < fireChance) {
      addParticle();
    }

    // Handle active particles
    for (int i = 0; i < count; i++) {
      arrX[i] += speedX;
      arrY[i] += speedY;
      arrZ[i] += speedZ;
      setDrawablePosition(i);

      // // Add splashes
      // if (arrY[i] < y1 && splashCount < maxParticles) {
      // addSplash(arrX[i], y1, arrZ[i]);
      // }

      // Kill any old particles
      if (arrY[i] < y1) {
        kill(i);
      }
    }

    // if (numTicks % 3 == 0) {
    // // Handle active splashes
    // for (int i = 0; i < splashCount; i++) {
    // splashFrame[i]++;
    //
    // // Kill any old splashes
    // if (splashFrame[i] >= 5) {
    // killSplash(i);
    // }
    // }
    // }
  }

  protected void setDrawablePosition(int i) {
    for (int j = 0; j < 5; j++) {
      drawables[i * 5 + j].setPosition(arrX[i] - speedX * j, arrY[i] - speedY * j,
          arrZ[i] - speedZ * j);
    }
  }

  private void addParticle() {
    arrX[count] = x1 + (float) (Math.random() * (x2 - x1));
    arrY[count] = y2;
    arrZ[count] = z1 + (float) (Math.random() * (z2 - z1));
    for (int j = 0; j < 5; j++) {
      drawables[count * 5 + j].setVisible(true);
    }
    count++;
  }

  // private void addSplash(float x, float y, float z) {
  // splashX[splashCount] = x;
  // splashY[splashCount] = y;
  // splashZ[splashCount] = z;
  // splashFrame[splashCount] = 0;
  // splashCount++;
  // }

  /**
   * Remove a raindrop by swapping it with the last item and decrementing the count.
   *
   * @param i
   *          the index of the item to kill
   */
  private void kill(int i) {
    count--;
    arrX[i] = arrX[count];
    arrY[i] = arrY[count];
    arrZ[i] = arrZ[count];
    for (int j = 0; j < 5; j++) {
      drawables[i * 5 + j].setVisible(false);
      Drawable temp = drawables[i * 5 + j];
      drawables[i * 5 + j] = drawables[count * 5 + j];
      drawables[count * 5 + j] = temp;
    }
  }

  // /**
  // * Remove a splash by swapping it with the last item and decrementing the count.
  // *
  // * @param i
  // * the index of the item to kill
  // */
  // private void killSplash(int i) {
  // splashCount--;
  // splashX[i] = splashX[splashCount];
  // splashY[i] = splashY[splashCount];
  // splashZ[i] = splashZ[splashCount];
  //
  // // FIXME Will probably need splash drawables
  // // drawables[i].setVisible(false);
  // // Drawable temp = drawables[i];
  // // drawables[i] = drawables[count];
  // // drawables[count] = temp;
}
