package game1.toolkit.particles;

import java.util.Arrays;

import folkforms.maths.Graph;
import game1.actors.Actor;
import game1.actors.Tickable;
import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.datasets.GameDataFactory;

public class BurningTorch implements Actor, Tickable {

  protected int x, y, z;

  private int numActive = 0;
  private int maxParticles = 100;
  private int maxTTL = 150;
  private double fireChance = 0.8;
  protected float speedX = 0;
  protected float speedY = 0.015f;
  protected float speedZ = 0;
  protected boolean torchVisible = true;

  protected float[] arrX, arrY, arrZ;
  protected long[] ttl, individualTTL;
  protected Drawable[] drawables;

  private float[] values = { 1, 0 };
  private float[] timePercents = { 0, 1 };
  private float[] redValues = { 1, 1, 0 };
  private float[] greenValues = { 1, 0, 0 };
  private float[] colourTimePercents = { 0, 0.5f, 1 };
  private float[] alpha, red, green;
  private float[] ttlGraphX = { 0, 0.7f, 0.8f, 1 };
  private float[] ttlGraphY = { 1, 1, 0.2f, 0 };

  public BurningTorch(int x, int y, int z) {
    this.x = x;
    this.y = y;
    this.z = z;
    arrX = new float[maxParticles];
    arrY = new float[maxParticles];
    arrZ = new float[maxParticles];
    Arrays.fill(arrX, x);
    Arrays.fill(arrY, y);
    Arrays.fill(arrZ, z);
    ttl = new long[maxParticles];
    individualTTL = new long[maxParticles];
    drawables = new Drawable[maxParticles];
    alpha = new float[maxParticles];
    red = new float[maxParticles];
    green = new float[maxParticles];

    for (int i = 0; i < maxParticles; i++) {
      drawables[i] = GameDataFactory.getInstance().get("toolkit.burning_torch_particle");
      drawables[i].setVisible(false);
      addChild(drawables[i]);
    }
  }

  @Override
  public void onTick() {
    if (!torchVisible) {
      return;
    }

    // Maybe add a new particle
    if (numActive < maxParticles && Math.random() < fireChance) {
      addParticle();
    }

    // Handle active particles
    for (int i = 0; i < numActive; i++) {
      arrX[i] += speedX;
      arrY[i] += speedY;
      arrZ[i] += speedZ;
      ttl[i]++;

      alpha[i] = Graph.calculateValue(timePercents, values, ttl[i], individualTTL[i]);
      red[i] = Graph.calculateValue(colourTimePercents, redValues, ttl[i], individualTTL[i]);
      green[i] = Graph.calculateValue(colourTimePercents, greenValues, ttl[i], individualTTL[i]);

      setDrawablePosition(i);
      drawables[i].setColour(new Colour(red[i], green[i], 0, alpha[i]));

      // Kill old particles
      if (ttl[i] > individualTTL[i]) {
        kill(i);
      }
    }
  }

  protected void setDrawablePosition(int i) {
    drawables[i].setPosition(arrX[i], arrY[i], arrZ[i]);
  }

  private void addParticle() {
    setNewPositions(numActive);

    ttl[numActive] = 0;
    long t0 = (long) (Math.random() * maxTTL);
    individualTTL[numActive] = (int) (Graph.calculateValue(ttlGraphX, ttlGraphY, t0, maxTTL) * 100);

    alpha[numActive] = Graph.calculateValue(colourTimePercents, redValues, ttl[numActive],
        individualTTL[numActive]);
    red[numActive] = Graph.calculateValue(colourTimePercents, redValues, ttl[numActive],
        individualTTL[numActive]);
    green[numActive] = Graph.calculateValue(colourTimePercents, redValues, ttl[numActive],
        individualTTL[numActive]);
    drawables[numActive]
        .setColour(new Colour(red[numActive], green[numActive], 0, alpha[numActive]));
    drawables[numActive].setVisible(true);
    numActive++;
  }

  protected void setNewPositions(int i) {
    float w = 0.05f;
    arrX[i] = x + w + (float) (Math.random() * w * 2) - (float) (Math.random() * w * 4);
    arrY[i] = y + w + (float) (Math.random() * w * 2) - (float) (Math.random() * w * 4);
    arrZ[i] = z + w + (float) (Math.random() * w * 2) - (float) (Math.random() * w * 4);
    setDrawablePosition(i);
  }

  private void kill(int i) {
    numActive--;
    arrX[i] = arrX[numActive];
    arrY[i] = arrY[numActive];
    arrZ[i] = arrZ[numActive];
    ttl[i] = ttl[numActive];
    drawables[i].setVisible(false);
    Drawable temp = drawables[i];
    drawables[i] = drawables[numActive];
    drawables[numActive] = temp;
  }

  @Override
  public void setVisible(boolean visible) {
    if (visible) {
      for (int i = 0; i < numActive; i++) {
        drawables[i].setVisible(true);
      }
    } else {
      for (int i = 0; i < drawables.length; i++) {
        drawables[i].setVisible(false);
      }
    }
    torchVisible = visible;
  }
}
