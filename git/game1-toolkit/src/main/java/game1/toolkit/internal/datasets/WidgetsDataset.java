package game1.toolkit.internal.datasets;

import game1.core.engine.Engine;
import game1.datasets.MultiLoaderOptions;

public class WidgetsDataset {

  public static final String TAG = "WidgetsTag";

  private String spritesheetPath = "src/main/resources/packed/toolkit.spritesheet";

  public WidgetsDataset() {
    Engine.GAME_FILES.register(spritesheetPath, TAG);

    // spritesheetLoader = new SpritesheetLoader2("widgets-spritesheet", spritesheetPath, 1);

    // // Test Label
    // Engine.ACTOR_LOADERS.register(new TempLoader<Label3>("toolkit.test.label", () -> {
    // String text =
    // "abcdefghi\njklmnopqr\nstuvwxyz\n1234567890\n!\"�$%^&*\n()_+-={}[]\n:@~;'#<>?\n,./\\|`";
    // return new Label3Builder().setText(text).setScale(5).setPosition(100, 100, 100).build();
    // }), "WidgetsTag");

    // // Test Button
    // Engine.ACTOR_LOADERS.register(new TempLoader<Button>("toolkit.test.button", () -> {
    // Button button = new Button("Button", FontUtils.DEFAULT_FONT_FACE, 5, 350, 100, 100, 200, 50);
    // button.setCommand(new Command() {
    // @Override
    // public void execute() {
    // Label label = Engine.ACTOR_CACHE.getNew("toolkit.test.label").getObject();
    // label.setText("foo\nbar");
    // }
    // });
    // return button;
    // }), "WidgetsTag");

    loadBurningTorch();
    // loadCheckbox();
    // loadSlider();
  }

  private void loadBurningTorch() {
    MultiLoaderOptions options = MultiLoaderOptions.from(String.format(
        "scale=2, colour=#fff, positionInt=100 100 %s, visible=false, alias=toolkit.burning_torch_particle",
        Integer.MAX_VALUE), ", ");
    Engine.GAME_FILES.register("resources/particle_effects/plane_1x1_vertical.obj", TAG, options);
  }

  // private void loadCheckbox() {
  // ImageLoader checkboxCheckedImageLoader = new ImageLoader("toolkit.checkbox.checked_image",
  // "checkbox_checked.png", spritesheetLoader);
  // ImageLoader checkboxUncheckedImageLoader = new ImageLoader("toolkit.checkbox.unchecked_image",
  // "checkbox_unchecked.png", spritesheetLoader);
  // Engine.ACTOR_LOADERS.register(checkboxCheckedImageLoader, "WidgetsTag");
  // Engine.ACTOR_LOADERS.register(checkboxUncheckedImageLoader, "WidgetsTag");
  //
  // Engine.ACTOR_LOADERS.register(new TempLoader<Checkbox>("toolkit.test.checkbox", () -> {
  // Label3 label = new Label3Builder().setText("Checkbox test").setScale(3)
  // .setPosition(100, 500, ToolkitZIndex.WIDGET).build();
  // return CheckboxBuilder.init(label).build();
  // }), "WidgetsTag");
  // }

  // private void loadSlider() {
  // ImageLoader sliderHBarLoader = new ImageLoader("toolkit.slider_hbar", "slider_hbar.png",
  // spritesheetLoader);
  // ImageLoader sliderKnobLoader = new ImageLoader("toolkit.slider_knob", "slider_knob.png",
  // spritesheetLoader);
  // Engine.ACTOR_LOADERS.register(sliderHBarLoader, "WidgetsTag");
  // Engine.ACTOR_LOADERS.register(sliderKnobLoader, "WidgetsTag");
  //
  // Engine.ACTOR_LOADERS.register(new TempLoader<Label3>("toolkit.test.slider.value_label", () -> {
  // return new Label3Builder().setText(Float.toString(0.25f))
  // .setPosition(225, 600, ToolkitZIndex.WIDGET).build();
  // }), "WidgetsTag");
  //
  // Engine.ACTOR_LOADERS.register(new TempLoader<Slider>("toolkit.test.slider", () -> {
  // Slider slider = new Slider(100, 600, ToolkitZIndex.WIDGET, "toolkit.slider_hbar",
  // "toolkit.slider_knob");
  // slider.setValue(0.25f);
  // slider.setCommand(new Command() {
  // @Override
  // public void execute() {
  // Label sliderValueLabel = Engine.ACTOR_CACHE.getNew("toolkit.test.slider.value_label")
  // .getObject();
  // sliderValueLabel.setText(String.format("%.2f", slider.getValue()));
  // }
  // });
  // return slider;
  // }), "WidgetsTag");
  // }
}
