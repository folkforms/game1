package game1.toolkit.internal.levels.cutscenes;

import game1.events.Command;
import game1.toolkit.cutscenes.internal.Cutscene;

public class SetUserInputCommand implements Command {
  private int value = Integer.MIN_VALUE;

  public SetUserInputCommand(int value) {
    this.value = value;
  }

  @Override
  public void execute() {
    Cutscene.setUserInput(value);
  }
}
