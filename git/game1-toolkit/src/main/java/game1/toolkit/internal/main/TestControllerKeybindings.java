package game1.toolkit.internal.main;

import folkforms.log.Log;
import game1.core.input.Controller;
import game1.core.input.KeyBindings;
import game1.core.input.KeyCommand;
import game1.events.EventSubscriptionsFactory;
import game1.events.ReceiveEvent;

public class TestControllerKeybindings extends KeyBindings {

  public TestControllerKeybindings() {
    EventSubscriptionsFactory.getInstance().subscribe(this);

    register(KeyCommand.init().controller(Controller.DPAD_UP).block().event("test_controller",
        "dpad up"));
    register(KeyCommand.init().controller(Controller.DPAD_DOWN).block().event("test_controller",
        "dpad down"));
    register(KeyCommand.init().controller(Controller.DPAD_LEFT).block().event("test_controller",
        "dpad left"));
    register(KeyCommand.init().controller(Controller.DPAD_RIGHT).block().event("test_controller",
        "dpad right"));
    register(KeyCommand.init().controller(Controller.BUTTON_1).block().event("test_controller",
        "Button 1 pressed"));
    register(KeyCommand.init().controller(Controller.BUTTON_2).block().event("test_controller",
        "Button 2 pressed"));
    register(KeyCommand.init().controller(Controller.BUTTON_3).block().event("test_controller",
        "Button 3 pressed"));
    register(KeyCommand.init().controller(Controller.BUTTON_4).block().event("test_controller",
        "Button 4 pressed"));
    register(KeyCommand.init().controller(Controller.BUTTON_5).block().event("test_controller",
        "Button 5 pressed"));
    register(KeyCommand.init().controller(Controller.BUTTON_6).block().event("test_controller",
        "Button 6 pressed"));
    register(KeyCommand.init().controller(Controller.BUTTON_7).block().event("test_controller",
        "Button 7 pressed"));
    register(KeyCommand.init().controller(Controller.BUTTON_8).block().event("test_controller",
        "Button 8 pressed"));
    register(KeyCommand.init().controller(Controller.BUTTON_9).block().event("test_controller",
        "Button 9 pressed"));
    register(KeyCommand.init().controller(Controller.BUTTON_10).block().event("test_controller",
        "Button 10 pressed"));
    register(KeyCommand.init().controller(Controller.BUTTON_11).block().event("test_controller",
        "Button 11 pressed"));
    register(KeyCommand.init().controller(Controller.BUTTON_12).block().event("test_controller",
        "Button 12 pressed"));
    register(KeyCommand.init().controller(Controller.BUTTON_13).block().event("test_controller",
        "Button 13 pressed"));
    register(KeyCommand.init().controller(Controller.BUTTON_14).block().event("test_controller",
        "Button 14 pressed"));
    register(KeyCommand.init().controller(Controller.BUTTON_15).block().event("test_controller",
        "Button 15 pressed"));
    register(KeyCommand.init().controller(Controller.BUTTON_16).block().event("test_controller",
        "Button 16 pressed"));
  }

  @ReceiveEvent("test_controller")
  public void testController(String message) {
    Log.temp("Event: %s", message);
  }
}
