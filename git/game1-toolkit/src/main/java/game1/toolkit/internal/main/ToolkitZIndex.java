package game1.toolkit.internal.main;

public class ToolkitZIndex {

  public static final int WIDGET = 1000;
  public static final int POPUP_MENU = 2000;
  public static final int MENU_A = 2000;
  public static final int MENU_B = 2010;
  public static final int MENU_C = 2020;
  public static final int MENU_D = 2030;

  public static final int CUTSCENE_BASE = 900;
  public static final int CUTSCENE_CHARACTER = 1000;
  public static final int CUTSCENE_TEXT = 2000;
  public static final int CUTSCENE_FADE = 3000;
}
