package game1.toolkit.internal.levels.cutscenes;

import game1.core.engine.Engine;
import game1.core.input.KeyBindings;
import game1.core.input.KeyCommand;

/**
 * Creates a temporary in-memory set of keybindings.
 */
public class CreateAndActivateKeyBindings {
  public static String parse(String ref, String[] keyTokens) {
    KeyBindings keybindings = new KeyBindings();
    for (int i = 0; i < keyTokens.length; i++) {

      int keyCode = -1;
      int userInput = -1;

      if (keyTokens[i].contains("->")) {
        String[] tokens = keyTokens[i].split("->");
        keyCode = Integer.parseInt(tokens[0].trim());
        userInput = Integer.parseInt(tokens[1].trim());
      } else {
        keyCode = Integer.parseInt(keyTokens[i]);
        userInput = keyCode;
      }

      keybindings
          .register(KeyCommand.init().keys(keyCode).command(new SetUserInputCommand(userInput)));
    }

    Engine.KEY_BINDINGS.register(ref, keybindings);
    return String.format("activateKeyBindings, %s", ref);
  }
}
