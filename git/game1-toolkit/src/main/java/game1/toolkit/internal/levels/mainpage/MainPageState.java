package game1.toolkit.internal.levels.mainpage;

import java.util.List;

import org.joml.Vector2i;

import game1.core.devtools.DebugToolsMask;
import game1.core.engine.Engine;
import game1.core.engine.userstate.UserState;
import game1.core.engine.userstate.UserStateHelper;
import game1.core.input.Mouse;
import game1.datasets.GameData;
import game1.datasets.GameDataFactory;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.scaling.ResolutionScalingStrategyV2;
import game1.toolkit.debugtools.DebugMouse;
import game1.toolkit.internal.datasets.WidgetsDataset;
import game1.toolkit.internal.levels.cutscenes.CutscenesDataset;
import game1.toolkit.screens.menusystem.testmenus.MenuA;
import game1.toolkit.screens.menusystem.testmenus.MenuB;
import game1.toolkit.screens.menusystem.testmenus.MenuC;
import game1.toolkit.screens.menusystem.testmenus.MenuD;
import game1.toolkit.widgets.background.Background;

public class MainPageState extends UserState {

  public static final String STATIC_LAYER = "static-layer";

  @Override
  public void apply() {
    ResolutionLayersFactory.getInstance().createStaticLayer(STATIC_LAYER, new Vector2i(1920, 1080),
        new ResolutionScalingStrategyV2());

    UserStateHelper.setMouse(Mouse.MOUSE_CURSOR);
    UserStateHelper.setKeyBindings("dev", "toolkit-main", "controller");

    MenuD menuD = new MenuD();
    MenuC menuC = new MenuC(menuD);
    MenuB menuB = new MenuB();
    MenuA menuA = new MenuA(menuB, menuC);

    GameData gameData = GameDataFactory.getInstance();
    gameData.addSupplier("menu-a", () -> menuA);
    gameData.addSupplier("menu-b", () -> menuB);
    gameData.addSupplier("menu-c", () -> menuC);
    gameData.addSupplier("menu-d", () -> menuD);

    Engine.GAME_STATE.addActor(gameData.get("menu-a"), STATIC_LAYER);
    Engine.GAME_STATE.addActor(gameData.get("menu-b"), STATIC_LAYER);
    Engine.GAME_STATE.addActor(gameData.get("menu-c"), STATIC_LAYER);
    Engine.GAME_STATE.addActor(gameData.get("menu-d"), STATIC_LAYER);

    Engine.GAME_STATE.addActor(Background.create(0, 0, 0, 1920, 1060, 0.2f, 0.2f, 0.2f),
        STATIC_LAYER);
    Engine.GAME_STATE.addActor(MainPageStateHelper.createTabs(), STATIC_LAYER);

    Engine.GAME_STATE.addActor(new DebugMouse(15, 1050), STATIC_LAYER);
    Engine.GAME_STATE.addActor(new DebugToolsMask(), STATIC_LAYER);
  }

  @Override
  public List<String> listRequiredTags() {
    return List.of(MainPageDataset.TAG, CutscenesDataset.TAG, WidgetsDataset.TAG);
  }
}
