package game1.toolkit.internal.levels.mainpage;

import game1.core.engine.Engine;

public class MainPageDataset {

  public static final String TAG = "MainPageTag";

  public MainPageDataset() {
    Engine.GAME_FILES.register("src/main/resources/packed/toolkit.spritesheet", TAG);
  }
}
