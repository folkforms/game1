package game1.toolkit.internal.levels.mainpage;

import game1.resolutions.layers.ResolutionLayers;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;
import game1.toolkit.internal.main.ToolkitZIndex;
import game1.toolkit.internal.panels.CutscenesPanel;
import game1.toolkit.internal.panels.LabelsPanel;
import game1.toolkit.internal.panels.WidgetsPanel;
import game1.toolkit.widgets.tabs.Tabs;

public class MainPageStateHelper {
  public static Tabs createTabs() {
    int z = ToolkitZIndex.WIDGET;
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(ResolutionLayers.WINDOW_LAYER).getResolutionHints();
    float scale = hints.getResolutionScale();
    float height = hints.getResolutionHeight() * scale;
    Tabs mainTabs = new Tabs(0, (int) height - 20, z, 120, 20);
    mainTabs.addTab("Labels", new LabelsPanel());
    mainTabs.addTab("Widgets", new WidgetsPanel());
    mainTabs.addTab("Cutscenes", new CutscenesPanel(z));
    mainTabs.setSelectedTab("Cutscenes");
    return mainTabs;
  }
}
