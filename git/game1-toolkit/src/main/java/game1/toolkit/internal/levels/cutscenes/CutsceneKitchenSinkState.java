package game1.toolkit.internal.levels.cutscenes;

import java.util.List;

import game1.core.engine.Engine;
import game1.core.engine.userstate.UserState;
import game1.datasets.GameDataFactory;
import game1.resolutions.layers.ResolutionLayers;
import game1.toolkit.cutscenes.internal.Cutscene;

public class CutsceneKitchenSinkState extends UserState {

  @Override
  public void apply() {
    Cutscene cutscene = GameDataFactory.getInstance()
        .get("test-cutscene-kitchen-sink-player-loader");
    cutscene.setProperty("test-char-pos-x", "500");
    cutscene.setProperty("test-char-pos-y", "500");
    Engine.GAME_STATE.addActor(cutscene, ResolutionLayers.WINDOW_LAYER);
  }

  @Override
  public List<String> listRequiredTags() {
    return List.of("CutscenesTag");
  }
}
