package game1.toolkit.internal.panels;

import game1.actors.Actor;
import game1.toolkit.internal.main.ToolkitZIndex;
import game1.toolkit.widgets.label.LabelBuilder;

public class LabelsPanel implements Actor {

  public LabelsPanel() {
    addChild(LabelBuilder.init("Label font size 1").setFontSize(1)
        .setPosition(50, 1015, ToolkitZIndex.WIDGET).build());
    addChild(LabelBuilder.init("Label font size 2").setFontSize(2)
        .setPosition(50, 995, ToolkitZIndex.WIDGET).build());
    addChild(LabelBuilder.init("Label font size 3").setFontSize(3)
        .setPosition(50, 970, ToolkitZIndex.WIDGET).build());
    addChild(LabelBuilder.init("Label font size 4").setFontSize(4)
        .setPosition(50, 940, ToolkitZIndex.WIDGET).build());
    addChild(LabelBuilder.init("Multi\nline\nlabel").setFontSize(3)
        .setPosition(50, 800, ToolkitZIndex.WIDGET).build());
  }
}
