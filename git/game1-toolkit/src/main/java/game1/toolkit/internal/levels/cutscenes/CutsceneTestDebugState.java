package game1.toolkit.internal.levels.cutscenes;

import java.util.List;

import game1.core.engine.Engine;
import game1.core.engine.userstate.UserState;
import game1.datasets.GameDataFactory;
import game1.resolutions.layers.ResolutionLayers;
import game1.toolkit.cutscenes.internal.Cutscene;

public class CutsceneTestDebugState extends UserState {

  @Override
  public void apply() {
    Cutscene.DEBUG = true;
    Engine.GAME_STATE.addActor(GameDataFactory.getInstance().get("test-cutscene-debug"),
        ResolutionLayers.WINDOW_LAYER);
  }

  @Override
  public List<String> listRequiredTags() {
    return List.of("CutscenesTag");
  }
}
