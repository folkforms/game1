package game1.toolkit.internal.levels.cutscenes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Converts {@code pollForUserInput, KEY_A -> 1, KEY_B -> 2} into:
 *
 * <pre>
 * createAndActivateKeyBindings, @ref-poll-1, KEY_A -> 1, KEY_B -> 2
 * waitForUserInput, 1, 2
 * deactivateKeyBindings, @ref-poll-1
 * </pre>
 */
public class PollUserInput {

  private static int refCount = 0;

  /**
   * Converts a string like "pollUserInput, 65 -> 1, 66 -> 2" into separate commands. Note that the
   * keys have already been converted from e.g. "KEY_A" to "65".
   *
   * @param line
   *          input line
   * @return list of commands
   */
  public static List<String> parse(final String line, boolean isDebugLine) {
    String[] tokens = line.split(", ");

    String ref = String.format("ref-wait-user-input-keybindings-%s", refCount++);
    String[] keyTokens = Arrays.copyOfRange(tokens, 1, tokens.length);

    List<String> output = new ArrayList<>();

    String line1 = CreateAndActivateKeyBindings.parse(ref, keyTokens);
    String line2 = String.format("waitForUserInput, %s",
        String.join(", ", getUserInputValues(keyTokens)));
    String line3 = String.format("deactivateKeyBindings, %s", ref);

    if (isDebugLine) {
      line1 = "? " + line1;
      line2 = "? " + line2;
      line3 = "? " + line3;
    }

    output.add(String.format("# Converted '%s'", line));
    output.add(line1);
    output.add(line2);
    output.add(line3);

    return output;
  }

  private static String[] getUserInputValues(String[] keyTokens) {
    String[] output = new String[keyTokens.length];
    for (int i = 0; i < output.length; i++) {
      if (keyTokens[i].indexOf("->") != -1) {
        String[] tokens = keyTokens[i].split("->");
        output[i] = tokens[1].trim();
      } else {
        output[i] = keyTokens[i];
      }
    }
    return output;
  }
}
