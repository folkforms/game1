package game1.toolkit.internal.levels.cutscenes;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Converts {@code chooseDebugUserInput, KEY_A -> 1/2, KEY_B -> 8/9} into:
 * <p>
 * <li>createAndActivateKeyBindings, @ref-poll-1, KEY_A, KEY_B -> 2</li>
 * <li>waitForUserInput, KEY_A, KEY_B</li>
 * <li>user, foo</li>
 * </p>
 * Note that keys will have already been translated.
 */
public class ChooseDebugUserInput {

  private static int refCount = 0;

  /**
   * Converts a string like "chooseDebugUserInput, 65 -> [ 1, 2, 3 ], 66 -> [ 7, 8, 9 ]" into
   * separate commands. Note that the keys have already been converted from e.g. "KEY_A" to "65".
   *
   * @param line
   *          input line
   * @return list of commands
   */
  public static List<String> parse(String line, boolean isDebugLine) {
    line = line.replace("chooseDebugUserInput,", "").trim();
    String[] keyTokens = line.split("],");
    List<String> output = new ArrayList<>();
    List<String> refs = new ArrayList<>();
    String[] leftValues = getLeftValuesOnly(keyTokens);
    String[] rightValues = getRightValuesOnly(keyTokens);

    for (int i = 0; i < leftValues.length; i++) {
      String ref = getRef();
      output.add(String.format("jumpIfEqual, %s, %s", leftValues[i], ref));
      refs.add(ref);
    }
    output.add("error, \"user input did not match any of the options\"");
    String refEnd = getRef("end");
    for (int i = 0; i < leftValues.length; i++) {
      output.add(String.format("label, %s", refs.get(i)));
      output.add(String.format("pushDebugUserInput, %s", getUserInputArray(rightValues[i])));
      output.add(String.format("jump, %s", refEnd));
    }
    output.add(String.format("label, %s", refEnd));

    if (isDebugLine) {
      output = output.stream().map(x -> String.format("? %s", x)).collect(Collectors.toList());
    }
    output.add(0, String.format("# Converted '%s'", line));
    return output;
  }

  private static String getUserInputArray(String rightValue) {
    String[] tokens = rightValue.split("/");
    return String.join(", ", tokens);
  }

  private static String getRef() {
    return getRef(null);
  }

  private static String getRef(String suffix) {
    if (suffix == null) {
      return String.format("ref-label-%s", refCount++);
    } else {
      return String.format("ref-label-%s-%s", suffix, refCount++);
    }
  }

  private static String[] getLeftValuesOnly(String[] keyTokens) {
    String[] output = new String[keyTokens.length];
    for (int i = 0; i < output.length; i++) {
      if (keyTokens[i].indexOf("->") != -1) {
        String[] tokens = keyTokens[i].split("->");
        output[i] = tokens[0].trim();
      } else {
        output[i] = keyTokens[i];
      }
    }
    return output;
  }

  private static String[] getRightValuesOnly(String[] keyTokens) {
    String[] output = new String[keyTokens.length];
    for (int i = 0; i < output.length; i++) {
      if (keyTokens[i].indexOf("->") != -1) {
        String[] tokens = keyTokens[i].split("->");
        output[i] = tokens[1].replace("[", "").replace("]", "").trim();
      } else {
        output[i] = keyTokens[i];
      }
    }
    return output;
  }
}
