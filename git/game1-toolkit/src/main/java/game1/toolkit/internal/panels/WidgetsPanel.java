package game1.toolkit.internal.panels;

import java.util.List;

import org.joml.Vector2f;
import org.joml.Vector3f;

import folkforms.log.Log;
import game1.actors.Actor;
import game1.core.graphics.FontUtils;
import game1.core.pipelines.GraphicsPipelineName;
import game1.core.resources.builders.PolygonBuilder;
import game1.core.resources.builders.QuadBuilder;
import game1.core.resources.builders.TriangleBuilder;
import game1.datasets.GameDataFactory;
import game1.events.Command;
import game1.toolkit.internal.main.ToolkitZIndex;
import game1.toolkit.particles.BurningTorch2D;
import game1.toolkit.screens.menusystem.testmenus.MenuA;
import game1.toolkit.widgets.button.Button;
import game1.toolkit.widgets.checkbox.CheckboxBuilder;
import game1.toolkit.widgets.label.Label;
import game1.toolkit.widgets.label.LabelBuilder;
import game1.toolkit.widgets.slider.Slider;

public class WidgetsPanel implements Actor {

  public WidgetsPanel() {

    addLabelTest();

    // Test Button
    Button button = new Button("Button", FontUtils.DEFAULT_FONT_FACE, 5, 350, 100, 100, 200, 50);
    button.setCommand(new Command() {
      @Override
      public void execute() {
        Log.info("Button clicked");
      }
    });
    addChild(button);

    // Test particles
    addChild(new BurningTorch2D(900, 500, ToolkitZIndex.WIDGET));

    // FIXME DATASETS: Checkbox is appearing but both drawables are visible
    // Test checkbox
    Label label = LabelBuilder.init("Checkbox test").setFontSize(3)
        .setPosition(100, 500, ToolkitZIndex.WIDGET).build();
    addChild(CheckboxBuilder.init(label).build());

    // FIXME DATASETS: Slider is working but label is not updating
    // Test slider
    Slider slider = new Slider(100, 600, ToolkitZIndex.WIDGET, "slider_hbar.png",
        "slider_knob.png");
    addChild(slider);
    Label sliderValueLabel = LabelBuilder.init(Float.toString(0.25f))
        .setPosition(225, 600, ToolkitZIndex.WIDGET).build();
    addChild(sliderValueLabel);

    // Menu system
    MenuA menuA = GameDataFactory.getInstance().get("menu-a");
    Button openMenuButton = new Button("Open Menu", 600, 950, ToolkitZIndex.WIDGET, 150, 20);
    openMenuButton.setCommand(new Command() {
      @Override
      public void execute() {
        menuA.setActive(true);
        menuA.setVisible(true);
      }
    });
    addChild(openMenuButton);
    addChild(LabelBuilder.init(
        "Click the 'Open Menu' button to open the menu.\nClick the top or bottom half of the red menu\nto open a different menu. Right-click to close.")
        .setPosition(600, 980, ToolkitZIndex.WIDGET).build());

    // // Checkbox
    // Checkbox checkbox = new Checkbox("Checkbox", 100, 100, z);
    // checkbox.setFont(FontUtils.DEFAULT_FONT_FACE, 5);
    // Label checkboxLabel = new Label("Checkbox value is: " + checkbox.isChecked(), 5, 1000, 100,
    // z);
    // CheckboxCommand checkboxCommand = new CheckboxCommand(checkbox, checkboxLabel);
    // checkbox.setCommand(checkboxCommand);
    // children.add(checkbox);
    // children.add(checkboxLabel);

    // // Spinner
    // Spinner spinner = new Spinner(100, 200, z, 200, 50);
    // Label spinnerLabel = new Label("Spinner value is: ?", 5, 1000, 200, z);
    // SpinnerCommand spinnerCommand = new SpinnerCommand(spinner, spinnerLabel);
    // spinner.setCommand(spinnerCommand);
    // children.add(spinner);
    // children.add(spinnerLabel);

    // // Slider
    // Slider slider = new Slider(100, 300, z, 270, 50);
    // Label sliderLabel = new Label("Slider value is: ?", 5, 1000, 300, z);
    // SliderCommand sliderCommand = new SliderCommand(slider, sliderLabel);
    // slider.setCommand(sliderCommand);
    // children.add(slider);
    // children.add(sliderLabel);

    // // Button
    // Button button = new Button("Button", 5, 100, 400, z, 200, 50);
    // Label buttonLabel = new Label("Button clicked ? times", 5, 1000, 400, z);
    // ButtonCommand buttonCommand = new ButtonCommand(button, buttonLabel);
    // button.setCommand(buttonCommand);
    // children.add(button);
    // children.add(buttonLabel);

    // // Tabs
    // Tabs tabs = new Tabs(100, 600, z, 200, 50);
    // tabs.addTab("Tab 1", new Label("Child of Tab 1", 5, 110, 525, z));
    // tabs.addTab("Tab 2", new Label("Child of Tab 2", 5, 110, 525, z));
    // tabs.addTab("Tab 3", new Label("Child of Tab 3", 5, 110, 525, z));
    // children.add(tabs);
    // children.add(new Background(90, 490, z - 2, 620, 170, 0.5f, 0.5f, 0.5f));
    // children.add(new Background(100, 500, z - 1, 600, 100, 0.2f, 0.2f, 0.2f));

    // // Custom mouse cursor
    // Button customCursorButton = new Button("Custom Cursor off", 5, 1000, 500, z, 400, 50);
    // Command toggleCustomCursorCommand = new ToggleCustomCursorCommand(customCursorButton);
    // customCursorButton.setCommand(toggleCustomCursorCommand);
    // children.add(customCursorButton);

    // // TextField
    // OldTextField oldTextField1 = new OldTextField("Textfield 1", 5, 100, 700, z, 600, 50);
    // children.add(oldTextField1);
    // OldTextField oldTextField2 = new OldTextField("Textfield 2", 5, 100, 760, z, 600, 50);
    // children.add(oldTextField2);
    // Label oldTextField1Label = new Label("?", 5, 1000, 700, z);
    // children.add(oldTextField1Label);
    // Button copyTextbutton = new Button("Copy", 5, 750, 700, z, 150, 50);
    // CopyTextCommand copyTextCommand = new CopyTextCommand(oldTextField1, oldTextField1Label);
    // copyTextbutton.setCommand(copyTextCommand);
    // oldTextField1.addEnterKeyCommand(copyTextCommand);
    // children.add(copyTextbutton);
    // Command tabCommand = new TabCommand();
    // oldTextField1.addTabKeyCommand(tabCommand);
    // oldTextField2.addTabKeyCommand(tabCommand);

    // // SelectableList
    // List<String> data = new ArrayList<>();
    // for (int i = 0; i < 7; i++) {
    // data.add(String.format("item %s", i));
    // }
    // SelectableList selectableList = new SelectableList(1000, 800, z, 400, 150, 5, data);
    // children.add(selectableList);

    Vector3f s1 = new Vector3f(0, 0, 0);
    Vector3f s2 = new Vector3f(0, 50, 0);
    Vector3f s3 = new Vector3f(50, 50, 0);
    Vector3f s4 = new Vector3f(50, 0, 0);
    addChild(QuadBuilder.init(GraphicsPipelineName.SCENE_2D).points(s1, s2, s3, s4)
        .position(new Vector3f(600, 10, 5000)).colour("#297").build());

    Vector3f p1 = new Vector3f(0, 0, 0);
    Vector3f p2 = new Vector3f(0, 50, 0);
    Vector3f p3 = new Vector3f(50, 50, 0);
    addChild(TriangleBuilder.init(GraphicsPipelineName.SCENE_2D).points(p1, p2, p3)
        .position(new Vector3f(670, 10, 5000)).colour("#82e").build());

    Vector3f p1b = new Vector3f(0, 50, 0);
    Vector3f p2b = new Vector3f(70, 90, 0);
    Vector3f p3b = new Vector3f(80, 20, 0);
    addChild(TriangleBuilder.init(GraphicsPipelineName.SCENE_2D).points(p1b, p2b, p3b)
        .position(new Vector3f(740, 10, 5000)).colour("#ff08").build());

    Vector3f p1c = new Vector3f(20, 20, 0);
    Vector3f p2c = new Vector3f(20, 100, 0);
    Vector3f p3c = new Vector3f(100, 40, 0);
    addChild(TriangleBuilder.init(GraphicsPipelineName.SCENE_2D).points(p1c, p2c, p3c)
        .position(new Vector3f(740, 10, 4900)).colour("#0ff8").build());

    Vector3f q1b = new Vector3f(0, 30, 0);
    Vector3f q2b = new Vector3f(40, 80, 0);
    Vector3f q3b = new Vector3f(90, 70, 0);
    Vector3f q4b = new Vector3f(150, 10, 0);
    addChild(QuadBuilder.init(GraphicsPipelineName.SCENE_2D).points(q1b, q2b, q3b, q4b)
        .position(new Vector3f(850, 10, 5000)).colour("#7e9").build());

    Vector2f poly1a = new Vector2f(0, 200);
    Vector2f poly1b = new Vector2f(150, 150);
    Vector2f poly1c = new Vector2f(270, 170);
    Vector2f poly1d = new Vector2f(330, 130);
    Vector2f poly1e = new Vector2f(300, 20);
    Vector2f poly1f = new Vector2f(210, 60);
    Vector2f poly1g = new Vector2f(30, 50);
    addChild(PolygonBuilder.init(GraphicsPipelineName.SCENE_2D)
        .points2d(List.of(poly1a, poly1b, poly1c, poly1d, poly1e, poly1f, poly1g))
        .position(new Vector3f(1000, 10, 5000)).colour("#b37").build());

    addChild(LabelBuilder.init("Top item should be red-green-red-green each row")
        .setPosition(750, 420, 5000).setFontSize(2).build());
    addOverlappingTriangles(200, "#f007", "#0f07");
    addOverlappingTriangles(300, "#f00", "#0f0");
  }

  private void addLabelTest() {

    String text = "abcdefghi\njklmnopqr\nstuvwxyz\n1234567890\n!\"�$%^&*\n()_+-={}[]\n:@~;'#<>?\n,./\\|`";
    Label label = LabelBuilder.init(text).setFontSize(5).setPosition(100, 100, 100)
        .setModifiable(true).build();
    addChild(label);

    Button button = new Button("Button", FontUtils.DEFAULT_FONT_FACE, 5, 100, 25, 100, 200, 50);
    button.setCommand(new Command() {
      @Override
      public void execute() {
        label.setText("Hello\nthere!\n" + System.currentTimeMillis());
      }
    });
    addChild(button);
  }

  private void addOverlappingTriangles(int y, String red, String green) {
    // Red drawn first, red > green
    addTriangle1(red, 750, y, 5000);
    addTriangle2(green, 750, y, 4900);

    // Red drawn first, red < green
    addTriangle1(red, 850, y, 4900);
    addTriangle2(green, 850, y, 5000);

    // Red drawn second, red > green
    addTriangle1(green, 950, y, 4900);
    addTriangle2(red, 950, y, 5000);

    // Red drawn second, red < green
    addTriangle1(green, 1050, y, 5000);
    addTriangle2(red, 1050, y, 4900);
  }

  private void addTriangle1(String colour, float x, float y, float z) {
    Vector3f p1c = new Vector3f(20, 20, 0);
    Vector3f p2c = new Vector3f(20, 100, 0);
    Vector3f p3c = new Vector3f(100, 40, 0);
    addChild(TriangleBuilder.init(GraphicsPipelineName.SCENE_2D).points(p1c, p2c, p3c)
        .position(new Vector3f(x, y, z)).colour(colour).build());
  }

  private void addTriangle2(String colour, float x, float y, float z) {
    Vector3f p1b = new Vector3f(0, 50, 0);
    Vector3f p2b = new Vector3f(70, 90, 0);
    Vector3f p3b = new Vector3f(80, 20, 0);
    addChild(TriangleBuilder.init(GraphicsPipelineName.SCENE_2D).points(p1b, p2b, p3b)
        .position(new Vector3f(x, y, z)).colour(colour).build());
  }
}
