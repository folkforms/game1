package game1.toolkit.internal.main;

import game1.core.input.KeyBindings;
import game1.core.input.KeyCommand;
import game1.core.input.Keys;
import game1.toolkit.commands.ExitCommand;

public class ToolkitKeyBindings extends KeyBindings {

  public ToolkitKeyBindings() {
    register(KeyCommand.init().keys(Keys.KEY_ESCAPE).command(new ExitCommand()));
  }
}
