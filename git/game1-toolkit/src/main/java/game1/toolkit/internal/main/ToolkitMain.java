package game1.toolkit.internal.main;

import java.util.List;

import folkforms.log.Log;
import game1.core.engine.Engine;
import game1.toolkit.cutscenes.FastForwardKeyBindings;
import game1.toolkit.cutscenes.tests.Keys123;
import game1.toolkit.internal.datasets.WidgetsDataset;
import game1.toolkit.internal.levels.cutscenes.CutsceneKitchenSinkState;
import game1.toolkit.internal.levels.cutscenes.CutsceneTestDebugState;
import game1.toolkit.internal.levels.cutscenes.CutsceneTestFlickerState;
import game1.toolkit.internal.levels.cutscenes.CutsceneTestSoundsState;
import game1.toolkit.internal.levels.cutscenes.CutscenesDataset;
import game1.toolkit.internal.levels.mainpage.MainPageDataset;
import game1.toolkit.internal.levels.mainpage.MainPageState;

public class ToolkitMain {

  public static void main(String[] args) {
    try {
      Engine engine = new Engine(/* "--debug=GameCache" */);

      Engine.KEY_BINDINGS.register("toolkit-main", new ToolkitKeyBindings());
      Engine.KEY_BINDINGS.register("fast-forward", new FastForwardKeyBindings());
      Engine.KEY_BINDINGS.register("keys123", new Keys123());
      Engine.KEY_BINDINGS.register("controller", new TestControllerKeybindings());

      new CutscenesDataset();
      new WidgetsDataset();
      new MainPageDataset();

      Engine.GAME_FILES.setInitialTags(List.of(MainPageDataset.TAG, CutscenesDataset.TAG),
          List.of());

      Engine.GAME_STATE.register(ToolkitStates.MAIN_PAGE, new MainPageState());
      Engine.GAME_STATE.register(ToolkitStates.CUTSCENE_TEST_FLICKER,
          new CutsceneTestFlickerState());
      Engine.GAME_STATE.register(ToolkitStates.CUTSCENE_KITCHEN_SINK,
          new CutsceneKitchenSinkState());
      Engine.GAME_STATE.register(ToolkitStates.CUTSCENE_TEST_SOUNDS, new CutsceneTestSoundsState());
      Engine.GAME_STATE.register(ToolkitStates.CUTSCENE_TEST_DEBUG, new CutsceneTestDebugState());

      Engine.MIXER.registerChannel("effects");
      Engine.MIXER.registerChannel("music");

      engine.start(ToolkitStates.MAIN_PAGE);
    } catch (Exception ex) {
      Log.error(ex);
      System.exit(1);
    }
  }
}
