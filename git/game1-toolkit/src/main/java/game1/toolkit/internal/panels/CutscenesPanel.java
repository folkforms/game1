package game1.toolkit.internal.panels;

import game1.actors.Actor;
import game1.core.engine.Engine;
import game1.events.Command;
import game1.toolkit.internal.main.ToolkitStates;
import game1.toolkit.internal.main.ToolkitZIndex;
import game1.toolkit.widgets.button.Button;

public class CutscenesPanel implements Actor {

  public CutscenesPanel(int z) {
    addChild(createButton("Cutscene Flicker", 800, ToolkitZIndex.WIDGET,
        ToolkitStates.CUTSCENE_TEST_FLICKER));
    addChild(createButton("Cutscene Kitchen Sink", 740, ToolkitZIndex.WIDGET,
        ToolkitStates.CUTSCENE_KITCHEN_SINK));
    addChild(createButton("Cutscene Sounds", 680, ToolkitZIndex.WIDGET,
        ToolkitStates.CUTSCENE_TEST_SOUNDS));
    addChild(createButton("Cutscene Debug", 620, ToolkitZIndex.WIDGET,
        ToolkitStates.CUTSCENE_TEST_DEBUG));
  }

  private Button createButton(String text, int y, int z, String state) {
    Button button = new Button(text, 5, 100, y, z, 650, 50);
    Command command = new Command() {
      @Override
      public void execute() {
        Engine.GAME_STATE.moveToState(state);
      }
    };
    button.setCommand(command);
    return button;
  }
}
