package game1.toolkit.internal.levels.cutscenes;

import java.io.IOException;

import game1.actors.Actor;
import game1.core.engine.Engine;
import game1.datasets.GameData;
import game1.datasets.GameDataFactory;
import game1.filesystem.FileSystem;
import game1.filesystem.FileSystemFactory;
import game1.toolkit.cutscenes.CutsceneLoader;
import game1.toolkit.cutscenes.tests.SpeechBubbleCommand;
import game1.toolkit.internal.main.ToolkitStates;

public class CutscenesDataset {

  public static final String TAG = "CutscenesTag";

  // FIXME DATASETS: Remove old code...
  public CutscenesDataset() throws IOException {
    // createButtons();
    loadCutscenes();
    loadSprites();
    loadSounds();
  }

  // private void createButtons() {
  // Engine.ACTOR_LOADERS.register(new TempLoader<Button>("cutscene_panel_button.cutscene_flicker",
  // () -> createButton("Cutscene Flicker", 800, ToolkitZIndex.WIDGET,
  // ToolkitStates.CUTSCENE_TEST_FLICKER)),
  // "CutscenesTag");
  // Engine.ACTOR_LOADERS.register(new TempLoader<Button>(
  // "cutscene_panel_button.cutscene_kitchen_sink", () -> createButton("Cutscene Kitchen Sink",
  // 740, ToolkitZIndex.WIDGET, ToolkitStates.CUTSCENE_KITCHEN_SINK)),
  // "CutscenesTag");
  // Engine.ACTOR_LOADERS.register(new TempLoader<Button>("cutscene_panel_button.cutscene_sounds",
  // () -> createButton("Cutscene Sounds", 680, ToolkitZIndex.WIDGET,
  // ToolkitStates.CUTSCENE_TEST_SOUNDS)),
  // "CutscenesTag");
  // Engine.ACTOR_LOADERS.register(new TempLoader<Button>("cutscene_panel_button.cutscene_debug",
  // () -> createButton("Cutscene Debug", 620, ToolkitZIndex.WIDGET,
  // ToolkitStates.CUTSCENE_TEST_DEBUG)),
  // "CutscenesTag");
  // }
  //
  // private Button createButton(String text, int y, int z, String state) {
  // Button button = new Button(text, 5, 100, y, z, 650, 50);
  // Command command = new Command() {
  // @Override
  // public void execute() {
  // Engine.GAME_STATE.moveToState(state);
  // }
  // };
  // button.setCommand(command);
  // return button;
  // }

  private void loadCutscenes() throws IOException {
    FileSystem fileSystem = FileSystemFactory.getInstance();

    // FIXME C9: I should create a CutsceneMultiLoader but I don't want to create anything for the
    // old cutscene types. When I migrate them across we will create that and clean up this code.

    Actor c1 = CutsceneLoader.init("resources-test/cutscenes/test-flicker/test-flicker-start.txt")
        .nextState(ToolkitStates.MAIN_PAGE).load(fileSystem).getClone();

    Actor c2 = CutsceneLoader.init("resources-test/cutscenes/kitchen-sink/test-start.txt")
        .nextState(ToolkitStates.MAIN_PAGE)
        .registerUserEvent("speechBubble", new SpeechBubbleCommand()).load(fileSystem).getClone();

    Actor c3 = CutsceneLoader.init("resources-test/cutscenes/sounds/test-sounds.txt")
        .nextState(ToolkitStates.MAIN_PAGE).load(fileSystem).getClone();

    Actor c4 = CutsceneLoader.init("resources-test/cutscenes/debug-mode/debug-mode.txt")
        .nextState(ToolkitStates.MAIN_PAGE).load(fileSystem).getClone();

    // FIXME CUTSCENES/FIXME DATASETS: These are here temporarily because I don't want to write a
    // CutsceneMultiLoader for the old cutscenes. Once I get the new Cutscenes working I will
    // replace these test cutscenes and write the CutsceneMultiLoader.
    GameData gameData = GameDataFactory.getInstance();
    gameData.addSupplier("test-flicker-start.txt", () -> c1);
    gameData.addSupplier("test-start.txt", () -> c2);
    gameData.addSupplier("test-sounds.txt", () -> c3);
    gameData.addSupplier("debug-mode.txt", () -> c4);

    // Engine.ACTOR_LOADERS
    // .register(new TempLoader<Cutscene>("test-cutscene-flicker-player-loader", () -> {
    // try {
    // return (Cutscene) CutsceneLoader
    // .init("resources-test/cutscenes/test-flicker/test-flicker-start.txt")
    // .nextState(ToolkitStates.MAIN_PAGE).load().getClone();
    // } catch (IOException e) {
    // throw new RuntimeException(e);
    // }
    // }), "CutscenesTag");
    //
    // Engine.ACTOR_LOADERS
    // .register(new TempLoader<Cutscene>("test-cutscene-kitchen-sink-player-loader", () -> {
    // try {
    // return (Cutscene) CutsceneLoader
    // .init("resources-test/cutscenes/kitchen-sink/test-start.txt")
    // .nextState(ToolkitStates.MAIN_PAGE)
    // .registerUserEvent("speechBubble", new SpeechBubbleCommand()).load().getClone();
    // } catch (IOException e) {
    // throw new RuntimeException(e);
    // }
    // }), "CutscenesTag");
    //
    // Engine.ACTOR_LOADERS
    // .register(new TempLoader<Cutscene>("test-cutscene-sounds-player-loader", () -> {
    // try {
    // return (Cutscene) CutsceneLoader.init("resources-test/cutscenes/sounds/test-sounds.txt")
    // .nextState(ToolkitStates.MAIN_PAGE).load().getClone();
    // } catch (IOException e) {
    // throw new RuntimeException(e);
    // }
    // }), "CutscenesTag");
    //
    // Engine.ACTOR_LOADERS.register(new TempLoader<Cutscene>("test-cutscene-debug", () -> {
    // try {
    // return (Cutscene) CutsceneLoader.init("resources-test/cutscenes/debug-mode/debug-mode.txt")
    // .nextState(ToolkitStates.MAIN_PAGE).load().getClone();
    // } catch (IOException e) {
    // throw new RuntimeException(e);
    // }
    // }), "CutscenesTag");
  }

  private void loadSprites() {
    String spritesheet = "src/main/resources/packed/toolkit.spritesheet";
    Engine.GAME_FILES.register(spritesheet, TAG);

    // SpritesheetLoader2 spritesheetLoader = new SpritesheetLoader2("cutscenes-spritesheet",
    // spritesheet, 1);
    //
    // AnimationLoader henchardWalkLoader = new AnimationLoader("walking", "henchard_walk.png",
    // spritesheetLoader, ab -> ab.setTickRate(30));
    // AnimationLoader henchardSpeakingLoader = new AnimationLoader("speaking", "henchard_talk.png",
    // spritesheetLoader, ab -> ab.setTickRate(30));
    // AnimationLoader henchardIdleLoader = new AnimationLoader("idle", "henchard_idle.png",
    // spritesheetLoader, ab -> ab.setTickRate(30));
    // Engine.ACTOR_LOADERS.register(henchardWalkLoader, "CutscenesTag");
    // Engine.ACTOR_LOADERS.register(henchardSpeakingLoader, "CutscenesTag");
    // Engine.ACTOR_LOADERS.register(henchardIdleLoader, "CutscenesTag");
    //
    // DrawableLoader backgroundLoader = new DrawableLoader("background",
    // () -> Background.create(100, 450, 0, 1720, 550, 0, 0.3f, 0.3f));
    // TempLoader<Char2> testCharLoader = new TempLoader<Char2>("test-char", () -> new Char2(500,
    // 500,
    // ToolkitZIndex.CUTSCENE_CHARACTER, List.of("walking", "speaking", "idle")));
    // Engine.ACTOR_LOADERS.register(backgroundLoader, "CutscenesTag");
    // Engine.ACTOR_LOADERS.register(testCharLoader, "CutscenesTag");
    //
    // ImageLoader speechbubbleLeftTailLoader = new ImageLoader("speechbubble-left-tail",
    // "speechbubble-left-tail.png", spritesheetLoader, i -> i.setScale(4), null);
    // ImageLoader speechbubbleRightTailLoader = new ImageLoader("speechbubble-right-tail",
    // "speechbubble-left-tail.png", spritesheetLoader, i -> i.setScale(4).setMirrored(true),
    // null);
    // ImageLoader speechbubbleTopLoader = new ImageLoader("speechbubble-top",
    // "speechbubble-top.png",
    // spritesheetLoader, i -> i.setScale(4), null);
    // ImageLoader speechbubbleLeftLoader = new ImageLoader("speechbubble-left",
    // "speechbubble-left.png", spritesheetLoader, i -> i.setScale(4), null);
    // ImageLoader speechbubbleRightLoader = new ImageLoader("speechbubble-right",
    // "speechbubble-left.png", spritesheetLoader, i -> i.setScale(4).setMirrored(true), null);
    // ImageLoader speechbubbleBottom1Loader = new ImageLoader("speechbubble-bottom-1",
    // "speechbubble-bottom.png", spritesheetLoader, i -> i.setScale(4), null);
    // ImageLoader speechbubbleBottom2Loader = new ImageLoader("speechbubble-bottom-2",
    // "speechbubble-bottom.png", spritesheetLoader, i -> i.setScale(4), null);
    // ImageLoader speechbubbleTopLeftLoader = new ImageLoader("speechbubble-top-left",
    // "speechbubble-top-left.png", spritesheetLoader, i -> i.setScale(4), null);
    // ImageLoader speechbubbleTopRightLoader = new ImageLoader("speechbubble-top-right",
    // "speechbubble-top-left.png", spritesheetLoader, i -> i.setScale(4).setMirrored(true), null);
    // ImageLoader speechbubbleBottomLeftLoader = new ImageLoader("speechbubble-bottom-left",
    // "speechbubble-bottom-left.png", spritesheetLoader, i -> i.setScale(4), null);
    // ImageLoader speechbubbleBottomRightLoader = new ImageLoader("speechbubble-bottom-right",
    // "speechbubble-bottom-left.png", spritesheetLoader, i -> i.setScale(4).setMirrored(true),
    // null);
    // ImageLoader speechbubbleCenterLoader = new ImageLoader("speechbubble-center",
    // "speechbubble-center.png", spritesheetLoader, i -> i.setScale(4), null);
    // Engine.ACTOR_LOADERS.register(speechbubbleLeftTailLoader, "CutscenesTag");
    // Engine.ACTOR_LOADERS.register(speechbubbleRightTailLoader, "CutscenesTag");
    // Engine.ACTOR_LOADERS.register(speechbubbleTopLoader, "CutscenesTag");
    // Engine.ACTOR_LOADERS.register(speechbubbleLeftLoader, "CutscenesTag");
    // Engine.ACTOR_LOADERS.register(speechbubbleRightLoader, "CutscenesTag");
    // Engine.ACTOR_LOADERS.register(speechbubbleBottom1Loader, "CutscenesTag");
    // Engine.ACTOR_LOADERS.register(speechbubbleBottom2Loader, "CutscenesTag");
    // Engine.ACTOR_LOADERS.register(speechbubbleTopLeftLoader, "CutscenesTag");
    // Engine.ACTOR_LOADERS.register(speechbubbleTopRightLoader, "CutscenesTag");
    // Engine.ACTOR_LOADERS.register(speechbubbleBottomLeftLoader, "CutscenesTag");
    // Engine.ACTOR_LOADERS.register(speechbubbleBottomRightLoader, "CutscenesTag");
    // Engine.ACTOR_LOADERS.register(speechbubbleCenterLoader, "CutscenesTag");
  }

  private void loadSounds() {
    Engine.GAME_FILES.register("resources-test/sounds/hmm.ogg", TAG);
    Engine.GAME_FILES.register("resources-test/sounds/soft_beep.ogg", TAG);
    Engine.GAME_FILES.register("resources-test/sounds/tone.ogg", TAG);
    Engine.GAME_FILES.register("resources-test/sounds/music.ogg", TAG);
  }
}
