package game1.toolkit.internal.levels.cutscenes;

import java.util.List;

import game1.core.engine.Engine;
import game1.core.engine.userstate.UserState;
import game1.datasets.GameDataFactory;
import game1.resolutions.layers.ResolutionLayers;

public class CutsceneTestFlickerState extends UserState {

  @Override
  public void apply() {
    Engine.GAME_STATE.addActor(
        GameDataFactory.getInstance().get("test-cutscene-flicker-player-loader"),
        ResolutionLayers.WINDOW_LAYER);
  }

  @Override
  public List<String> listRequiredTags() {
    return List.of("CutscenesTag");
  }
}
