package game1.toolkit.internal.main;

public class ToolkitStates {

  public static final String MAIN_PAGE = "MAIN_PAGE";
  public static final String CUTSCENE_TEST_FLICKER = "CUTSCENES_TEST_FLICKER";
  public static final String CUTSCENE_KITCHEN_SINK = "CUTSCENE_KITCHEN_SINK";
  public static final String CUTSCENE_TEST_SOUNDS = "CUTSCENE_TEST_SOUNDS";
  public static final String CUTSCENE_TEST_DEBUG = "CUTSCENE_TEST_DEBUG";
}
