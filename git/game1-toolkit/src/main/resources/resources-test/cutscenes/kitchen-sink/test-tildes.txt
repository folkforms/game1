# Test various tilde prefixes work
~~  showText, @ref-test-tildes-1, "Test tildes", 900, 900, 1000
~~  showText, @ref-test-tildes-2, "(These lines should appear as normal)", 900, 850, 1000
~~  showText, @ref-test-tildes-3, "(The tildes are in the code)", 900, 800, 1000
~~~ wait, 100
~~~~remove, @ref-test-tildes-1
~~~~remove, @ref-test-tildes-2
~~~~remove, @ref-test-tildes-3
