label, option-2
setAnimation, test-char, speaking
showText, @ref1, "Option 2", 500, 800, 1000
wait, 50
remove, @ref1
showText, @ref2, "Option 2 part 2", 800, 750, 1000
wait, 50
remove, @ref2

@import "./resources-test/cutscenes/kitchen-sink/test-nested-import.txt"
