package game1.installer.dependencies.projects;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Set;
import java.util.zip.ZipFile;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import folkforms.xpathutils.XPathUtils;
import game1.installer.InstallerException;
import game1.installer.dependencies.Dependency;
import game1.installer.utils.JarUtils;

public class ProjectDependencies {
  private String jarPath;
  private String pomPath;
  private String source;

  public ProjectDependencies(String source, String game1Version) {
    this.jarPath = String.format("%s/game1/%s/%s/%s-%s.jar", System.getenv("MAVEN_REPO"), source,
        game1Version, source, game1Version);
    this.pomPath = String.format("META-INF/maven/game1/%s/pom.xml", source);
    this.source = source;
  }

  public Set<Dependency> listDependencies() throws InstallerException {
    try {
      Document pom = loadPom(jarPath, pomPath);
      DependencySet dependencySet = new DependencySet(pom, source);
      return dependencySet.listDependencies();
    } catch (Exception ex) {
      throw new InstallerException(ex);
    }
  }

  private Document loadPom(String jarPath, String pomPath)
      throws ParserConfigurationException, SAXException, IOException {
    byte[] bytes = JarUtils.loadFromJar(new ZipFile(jarPath), pomPath);
    return XPathUtils.getDocument(new ByteArrayInputStream(bytes));
  }
}
