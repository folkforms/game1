package game1.installer.dependencies;

import java.util.ArrayList;
import java.util.List;

import game1.installer.InstallerConstants;

public class Dependency {

  String groupId, artifactId, version, source, jar;

  public Dependency(String groupId, String artifactId, String version, String source) {
    this.groupId = groupId;
    this.artifactId = artifactId;
    this.version = version;
    this.source = source;
    this.jar = String.format("%s-%s.jar", artifactId, version);
  }

  public String getMavenFolder() {
    return String.format("%s/%s/%s", groupId.replaceAll("\\.", "/"), artifactId, version);
  }

  public String getJarName() {
    return String.format("%s-%s.jar", artifactId, version);
  }

  public boolean hasNatives(String os) {
    List<String> lwjglJarsWithNoNatives = new ArrayList<>(List.of("lwjgl-bom", "lwjgl-cuda",
        "lwjgl-egl", "lwjgl-jawt", "lwjgl-odbc", "lwjgl-opencl", "lwjgl-vulkan"));
    if (os.equals(InstallerConstants.MAC_OS)) {
      lwjglJarsWithNoNatives.add("lwjgl-ovr");
    }
    return artifactId.startsWith("lwjgl") && !lwjglJarsWithNoNatives.contains(artifactId);
  }

  public String getGroupId() {
    return groupId;
  }

  public String getArtifactId() {
    return artifactId;
  }

  public String getVersion() {
    return version;
  }

  public String getClassPath() {
    return String.format("%s-%s.jar", artifactId, version);
  }

  @Override
  public String toString() {
    return String.format("%s %s %s (%s, %s, %s)", groupId, artifactId, version, getMavenFolder(),
        getJarName(), source);
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof Dependency) {
      Dependency d = (Dependency) o;
      return groupId.equals(d.groupId) && artifactId.equals(d.artifactId)
          && version.equals(d.version);
    }
    return false;
  }

  @Override
  public int hashCode() {
    return groupId.hashCode() + artifactId.hashCode() + version.hashCode();
  }
}
