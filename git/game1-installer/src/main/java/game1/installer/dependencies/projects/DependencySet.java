package game1.installer.dependencies.projects;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;

import folkforms.xpathutils.XPathUtils;
import game1.installer.InstallerException;
import game1.installer.dependencies.Dependency;

public class DependencySet {

  private Set<Dependency> dependencies;

  public DependencySet(Document document, String source) throws InstallerException {
    try {
      Optional<String> game1Version = getGame1Version(document);
      dependencies = new HashSet<>();
      int numNodes = XPathUtils.list("/pom:project/pom:dependencies/pom:dependency", document)
          .size();
      for (int i = 1; i <= numNodes; i++) {
        if (isTestScope(document, i)) {
          continue;
        }
        String groupId = getGroupId(document, i);
        String artifactId = getArtifactId(document, i);
        String version = getVersion(document, i);
        if (version.equals("${game1.version}")) {
          version = game1Version.get();
        }

        dependencies.add(new Dependency(groupId, artifactId, version, source));
      }
    } catch (Exception ex) {
      throw new InstallerException(ex);
    }
  }

  public Set<Dependency> listDependencies() {
    return dependencies;
  }

  private Optional<String> getGame1Version(Document document) throws XPathExpressionException {
    List<String> list = XPathUtils.list("/pom:project/pom:properties/pom:game1.version/text()",
        document);
    return list.size() == 1 ? Optional.of(list.get(0)) : Optional.empty();
  }

  private boolean isTestScope(Document document, int index) throws XPathExpressionException {
    List<String> scopeList = XPathUtils.list(String.format(
        "/pom:project/pom:dependencies/pom:dependency[%s]/pom:scope/text()[1]", index), document);
    return scopeList.size() == 1 && scopeList.get(0).equals("test");
  }

  private String getGroupId(Document document, int index) throws XPathExpressionException {
    return XPathUtils.list(String.format(
        "/pom:project/pom:dependencies/pom:dependency[%s]/pom:groupId/text()[1]", index), document)
        .get(0);
  }

  private String getArtifactId(Document document, int index) throws XPathExpressionException {
    return XPathUtils
        .list(
            String.format(
                "/pom:project/pom:dependencies/pom:dependency[%s]/pom:artifactId/text()[1]", index),
            document)
        .get(0);
  }

  private String getVersion(Document document, int index) throws XPathExpressionException {
    return XPathUtils.list(String.format(
        "/pom:project/pom:dependencies/pom:dependency[%s]/pom:version/text()[1]", index), document)
        .get(0);
  }
}
