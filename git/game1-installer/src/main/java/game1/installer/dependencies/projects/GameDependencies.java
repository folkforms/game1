package game1.installer.dependencies.projects;

import java.util.Set;

import org.w3c.dom.Document;

import folkforms.xpathutils.XPathUtils;
import game1.installer.InstallerException;
import game1.installer.dependencies.Dependency;

public class GameDependencies {
  private String source;

  public GameDependencies(String source) {
    this.source = source;
  }

  public Set<Dependency> listDependencies() throws InstallerException {
    try {
      Document pom = XPathUtils.getDocument("pom.xml");
      DependencySet deps = new DependencySet(pom, source);
      return deps.listDependencies();
    } catch (Exception ex) {
      throw new InstallerException(ex);
    }
  }
}
