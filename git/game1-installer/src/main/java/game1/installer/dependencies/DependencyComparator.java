package game1.installer.dependencies;

import java.util.Comparator;

public class DependencyComparator implements Comparator<Dependency> {

  @Override
  public int compare(Dependency d1, Dependency d2) {
    String s1 = String.format("%s %s", d1.groupId, d1.artifactId);
    String s2 = String.format("%s %s", d2.groupId, d2.artifactId);
    return s1.compareTo(s2);
  }
}
