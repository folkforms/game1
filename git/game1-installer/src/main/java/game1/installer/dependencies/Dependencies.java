package game1.installer.dependencies;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import folkforms.log.Log;
import game1.installer.dependencies.projects.GameDependencies;
import game1.installer.dependencies.projects.ProjectDependencies;

/**
 * Scans pom files and gathers a list of dependencies.
 */
public class Dependencies {

  private String game1Version;
  private String gameShortName;
  private List<Dependency> dependenciesList = new ArrayList<>();

  public Dependencies(String game1Version, String gameShortName) {
    this.game1Version = game1Version;
    this.gameShortName = gameShortName;
  }

  /**
   * Gathers dependencies from the pom files.
   */
  public void gather() throws IOException {
    try {
      Set<Dependency> dependenciesSet = new HashSet<>();

      List<String> projectDependencies = List.of("game1-core", "game1-cutscenes", "game1-datasets",
          "game1-debug", "game1-filesystem", "game1-toolkit", "game1-toolkit-cutscenes",
          "game1-variables");
      for (int i = 0; i < projectDependencies.size(); i++) {
        String projectName = projectDependencies.get(i);
        ProjectDependencies deps = new ProjectDependencies(projectName, game1Version);
        dependenciesSet.addAll(deps.listDependencies());
      }

      GameDependencies gameDeps = new GameDependencies(gameShortName);
      dependenciesSet.addAll(gameDeps.listDependencies());

      dependenciesList = dependenciesSet.stream().sorted(new DependencyComparator()).toList();
    } catch (Exception ex) {
      throw new IOException(ex);
    }
  }

  /**
   * Checks that we don't have two of the same jar with different versions.
   */
  public void checkForDifferentVersions() {
    List<String> found = new ArrayList<>();
    dependenciesList.stream().forEach(d -> {
      dependenciesList.stream().forEach(d2 -> {
        if (d.groupId.equals(d2.groupId) && d.artifactId.equals(d2.artifactId)
            && d.version.equals(d2.version) == false) {
          String data = String.format("%s %s %s (%s) vs %s (%s)", d.groupId, d.artifactId,
              d.version, d.source, d2.version, d2.source);
          String firstPart = String.format("%s %s", d.groupId, d.artifactId);
          if (found.stream().noneMatch(line -> line.startsWith(firstPart))) {
            found.add(data);
          }
        }
      });
    });
    if (found.size() > 0) {
      Log.info("Found same jar with different versions:");
      found.stream().forEach(f -> Log.info(f));
      StringBuffer sb = new StringBuffer("Found same jar with different versions:\n");
      found.stream().forEach(f -> sb.append(f).append("\n"));
      throw new RuntimeException(sb.toString());
    }
  }

  /**
   * Lists the dependencies.
   *
   * @return list of dependencies
   */
  public List<Dependency> listDependencies() {
    return dependenciesList;
  }
}
