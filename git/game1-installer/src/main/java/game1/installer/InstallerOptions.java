package game1.installer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import folkforms.log.Log;
import folkforms.xpathutils.XPathUtils;

/**
 * Stores the options used when creating an installer.
 */
class InstallerOptions {

  public static String TEST_POM_FILE = null;

  static final String GAME1_LIB_VERSION = "installer.game1LibVersion";
  static final String GAME_SHORT_NAME = "installer.gameShortName";
  static final String GAME_LONG_NAME = "installer.gameLongName";
  static final String GAME_VERSION = "installer.gameVersion";
  static final String MAIN_CLASS = "installer.mainClass";
  static final String VENDOR = "installer.vendor";

  private static final List<String> requiredProps = List.of(GAME_SHORT_NAME, GAME_LONG_NAME,
      GAME_VERSION, MAIN_CLASS, VENDOR);

  static final String WIN_MENU_GROUP = "installer.winMenuGroup";
  static final String INSTALLER_ICON = "installer.installerIcon";
  static final String LICENSE = "installer.license";
  static final String RELEASE_FOLDER = "installer.releaseFolder";
  static final String DEBUG = "installer.debug";
  static final String DRY_RUN = "installer.dryRun";

  // These properties are not modifiable
  static final String JAR_FILE = "installer.jarFile";
  static final String JAR_FILE_SNAPSHOT = "installer.jarFileSnapshot";
  static final String MAVEN_REPO = "installer.mavenRepo";
  static final String JAVA_HOME = "installer.javaHome";
  static final String TARGET_OPERATING_SYSTEM = "installer.targetOperatingSystem";
  static final String INSTALLER_TYPE = "installer.type";
  static final String INSTALLER_FOLDER = "installer.outputFolder";
  static final String INSTALLER_DEBUG_FOLDER = "installer.debugOutputFolder";

  private final Map<String, String> properties = new HashMap<>();

  /**
   * Creates a new {@link InstallerOptions} class.
   *
   * @param options
   *          options as a <code>Map&lt;String, Object&gt;
   * @throws IOException
   *           if an error occurs reading the data
   */
  InstallerOptions(Map<String, String> options) {
    setDefaultOptions();
    validateRequiredProperties(options);
    applyUserProperties(options);
    applyCalculatedProperties();
    Log.debug("InstallerOptions: Final properties = %s", options);
  }

  private void setDefaultOptions() {
    properties.put(RELEASE_FOLDER, "_temp_release");
    properties.put(DEBUG, "false");
    properties.put(DRY_RUN, "false");
    properties.put(INSTALLER_DEBUG_FOLDER, "_installer_debug");
    properties.put(INSTALLER_FOLDER, "_installer");
    properties.put(INSTALLER_ICON,
        "D:/dev/projects/game1/git/game1-installer/src/main/resources/default-game1-icon.ico");
    properties.put(JAVA_HOME, "c:\\Program Files\\Java\\jdk-17.0.1");
    properties.put(LICENSE,
        "D:/dev/projects/game1/git/game1-installer/src/main/resources/default-game-license.txt");
    properties.put(MAVEN_REPO, System.getenv("MAVEN_REPO").replaceAll("\\\\", "/"));
  }

  private void validateRequiredProperties(Map<String, String> userOptions) {
    List<String> missing = new ArrayList<>();
    requiredProps.forEach(key -> {
      if (userOptions.get(key) == null || userOptions.get(key).trim().length() == 0) {
        missing.add(key);
      }
    });
    if (missing.size() > 0) {
      throw new RuntimeException(String.format("Missing required properties: %s", missing));
    }
  }

  private void applyUserProperties(Map<String, String> userOptions) {
    userOptions.forEach((k, v) -> properties.put(k, v));
  }

  private void applyCalculatedProperties() {
    properties.put(JAR_FILE,
        String.format("%s-%s.jar", properties.get(GAME_SHORT_NAME), properties.get(GAME_VERSION)));
    properties.put(JAR_FILE_SNAPSHOT, String.format("target/%s-%s-SNAPSHOT.jar",
        properties.get(GAME_SHORT_NAME), properties.get(GAME_VERSION)));
    if (properties.get(WIN_MENU_GROUP) == null) {
      properties.put(WIN_MENU_GROUP, properties.get(GAME_LONG_NAME));
    }

    String os = properties.get(TARGET_OPERATING_SYSTEM);
    if (os.equals(InstallerConstants.WINDOWS)) {
      properties.put(INSTALLER_TYPE, InstallerConstants.WINDOWS_INSTALLER_TYPE);
    } else if (os.equals(InstallerConstants.MAC_OS)) {
      properties.put(INSTALLER_TYPE, InstallerConstants.MAC_OS_INSTALLER_TYPE);
    } else {
      throw new RuntimeException(String.format(
          "Unsupported target operating system: '%s'. Valid options are '%s' and '%s'.", os,
          InstallerConstants.WINDOWS, InstallerConstants.MAC_OS));
    }
  }

  /**
   * Gets the value of the given property.
   *
   * @param property
   *          property name
   * @return property value
   */
  String get(String property) {
    String value = properties.get(property);
    if (value == null) {
      throw new RuntimeException(String.format("Property '%s' does not exist", property));
    }
    return value;
  }

  @Override
  public String toString() {
    return String.format("InstallerOptions[properties=%s]", properties);
  }

  public static InstallerOptions newInstance(String os, boolean debug, boolean dryRun)
      throws IOException, InstallerException {
    try {
      Map<String, String> map = new HashMap<>();
      Document document = XPathUtils.getDocument(TEST_POM_FILE != null ? TEST_POM_FILE : "pom.xml");
      String game1Version = getPomProperty("game1.version", document);
      String gameLongName = getPomProperty("game.longName", document);
      String gameShortName = getPomProperty("game.shortName", document);
      String gameVersion = getPomProperty("game.version", document);
      String gameMainClass = getPomProperty("game.mainClass", document);
      String gameVendor = getPomProperty("game.vendor", document);
      map.put(InstallerOptions.GAME1_LIB_VERSION, game1Version);
      map.put(InstallerOptions.GAME_LONG_NAME, gameLongName);
      map.put(InstallerOptions.GAME_SHORT_NAME, gameShortName);
      map.put(InstallerOptions.GAME_VERSION, gameVersion);
      map.put(InstallerOptions.MAIN_CLASS, gameMainClass);
      map.put(InstallerOptions.VENDOR, gameVendor);

      Optional<String> winMenuGroupMaybe = getOptionalPomProperty("game.winMenuGroup", document);
      Optional<String> installerIconMaybe = getOptionalPomProperty("game.installerIcon", document);
      Optional<String> licenseFileMaybe = getOptionalPomProperty("game.licenseFile", document);
      winMenuGroupMaybe.ifPresent(winMenuGroup -> map.put(WIN_MENU_GROUP, winMenuGroup));
      installerIconMaybe.ifPresent(installerIcon -> map.put(INSTALLER_ICON, installerIcon));
      licenseFileMaybe.ifPresent(licenseFile -> map.put(LICENSE, licenseFile));

      map.put(InstallerOptions.TARGET_OPERATING_SYSTEM, os);
      if (debug) {
        map.put(InstallerOptions.DEBUG, "true");
      }
      if (dryRun) {
        map.put(InstallerOptions.DRY_RUN, "true");
      }
      return new InstallerOptions(map);
    } catch (ParserConfigurationException | SAXException | IOException
        | XPathExpressionException ex) {
      throw new IOException(ex);
    }
  }

  private static String getPomProperty(String pomProperty, Document document)
      throws IOException, XPathExpressionException, InstallerException {
    try {
      return XPathUtils.getFirst(
          String.format("/pom:project/pom:properties/pom:%s/text()", pomProperty), document);
    } catch (IndexOutOfBoundsException ex) {
      throw new InstallerException("Could not find project > properties > %s in pom.xml",
          pomProperty);
    }
  }

  private static Optional<String> getOptionalPomProperty(String pomProperty, Document document)
      throws IOException, XPathExpressionException, InstallerException {
    try {
      return Optional.of(XPathUtils.getFirst(
          String.format("/pom:project/pom:properties/pom:%s/text()", pomProperty), document));
    } catch (IndexOutOfBoundsException ex) {
      return Optional.empty();
    }
  }
}
