package game1.installer;

public class InstallerConstants {
  public static final String WINDOWS = "windows";
  public static final String MAC_OS = "macos";

  public static final String WINDOWS_INSTALLER_TYPE = "msi";
  public static final String MAC_OS_INSTALLER_TYPE = "dmg";
}
