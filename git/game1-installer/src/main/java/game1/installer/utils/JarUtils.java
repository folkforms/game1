package game1.installer.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class JarUtils {

  // FIXME JARS: Direct copy of OpenFiles.loadFromJar
  public static byte[] loadFromJar(ZipFile zipFile, String path) throws IOException {
    Enumeration<? extends ZipEntry> entries = zipFile.entries();
    while (entries.hasMoreElements()) {
      ZipEntry entry = entries.nextElement();
      if (entry.getName().equals(path)) {
        InputStream stream = zipFile.getInputStream(entry);
        try (stream) {
          return stream.readAllBytes();
        }
      }
    }
    throw new IOException(
        String.format("zipFile '%s' did not contain an entry called '%s'. Entries: %s",
            zipFile.getName(), path, zipFile.entries()));
  }
}
