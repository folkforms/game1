package game1.installer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import folkforms.log.Log;

class ExecHandler {
  public void exec(String... cmd) throws IOException {
    try {
      ProcessBuilder pb = new ProcessBuilder();
      pb.command(cmd);
      Process p = pb.start();
      BufferedReader outReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
      BufferedReader errReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
      String out = "";
      String err = "";
      while ((out = outReader.readLine()) != null) {
        System.out.println(out);
      }
      while ((err = errReader.readLine()) != null) {
        System.err.println(err);
      }
      int exitCode = p.waitFor();
      if (exitCode == 0) {
        Log.info("Success!");
      } else {
        Log.error("Process exited with exit code %s", exitCode);
      }
    } catch (InterruptedException ex) {
      throw new IOException(ex);
    }
  }
}
