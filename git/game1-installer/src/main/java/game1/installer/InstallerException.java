package game1.installer;

public class InstallerException extends Exception {

  private static final long serialVersionUID = -8985466932503592582L;

  public InstallerException(Exception ex) {
    super(ex);
  }

  public InstallerException(String message, Object... args) {
    super(String.format(message, args));
  }
}
