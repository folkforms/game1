package game1.installer;

import java.io.IOException;

import folkforms.args.Args;
import folkforms.fileutils.FileUtils;
import folkforms.textio.TextIO;

/**
 * Command-line interface for {@link GenerateInstaller}.
 */
public class InstallerCli {

  public static void main(String[] args) throws IOException, InstallerException {
    // Args are passed to exec-maven-plugin execution as -Dargs="--foo=1 --bar=2" which appears here
    // as a single string
    String[] tokens = args.length > 0 ? args[0].split(" ") : new String[0];
    Args a = new Args(tokens);

    String os = a.get("os");
    if (os == null) {
      os = InstallerConstants.WINDOWS;
    }

    boolean debug = a.get("debug") != null;
    boolean dryRun = a.get("dryRun") != null;
    InstallerOptions options = InstallerOptions.newInstance(os, debug, dryRun);
    new GenerateInstaller(new FileUtils(), new TextIO(), new ExecHandler()).generate(options);
  }
}
