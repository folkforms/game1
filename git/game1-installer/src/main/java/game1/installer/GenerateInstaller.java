package game1.installer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import folkforms.fileutils.FileUtils;
import folkforms.log.Log;
import folkforms.textio.TextIO;
import game1.installer.dependencies.Dependencies;
import game1.installer.dependencies.Dependency;

public class GenerateInstaller {

  private InstallerOptions options;
  private String gameJarDestPath;
  private String mavenRepo;
  private String javaHome;
  private String targetOperatingSystem;
  private Dependencies dependencies;
  private FileUtils fileUtils;
  private TextIO textIO;
  private ExecHandler execHandler;

  public GenerateInstaller(FileUtils fileUtils, TextIO textIO, ExecHandler execHandler) {
    this.fileUtils = fileUtils;
    this.textIO = textIO;
    this.execHandler = execHandler;
  }

  public void generate(InstallerOptions options) throws IOException {
    this.options = options;
    this.gameJarDestPath = options.get(InstallerOptions.RELEASE_FOLDER) + "/"
        + options.get(InstallerOptions.JAR_FILE);
    this.mavenRepo = options.get(InstallerOptions.MAVEN_REPO);
    this.javaHome = options.get(InstallerOptions.JAVA_HOME);
    this.targetOperatingSystem = options.get(InstallerOptions.TARGET_OPERATING_SYSTEM);

    findDependencies();
    createReleaseFolders();
    copyGameJar();
    createProductionFile();
    copyLibraries();
    copyPomFile();
    writeVersionDetails();
    runJPackage();
    deleteReleaseFolder();
  }

  private void findDependencies() throws IOException {
    dependencies = new Dependencies(options.get(InstallerOptions.GAME1_LIB_VERSION),
        options.get(InstallerOptions.GAME_SHORT_NAME));
    dependencies.gather();
    dependencies.checkForDifferentVersions();
  }

  private void createReleaseFolders() throws IOException {
    fileUtils.rmrf(options.get(InstallerOptions.RELEASE_FOLDER));
    fileUtils.mkdirp(options.get(InstallerOptions.RELEASE_FOLDER));

    if (options.get(InstallerOptions.DEBUG) == null
        || Boolean.parseBoolean(options.get(InstallerOptions.DEBUG)) == false) {
      fileUtils.rmrf(options.get(InstallerOptions.INSTALLER_FOLDER));
      fileUtils.mkdirp(options.get(InstallerOptions.INSTALLER_FOLDER));
    } else {
      fileUtils.rmrf(options.get(InstallerOptions.INSTALLER_DEBUG_FOLDER));
      fileUtils.mkdirp(options.get(InstallerOptions.INSTALLER_DEBUG_FOLDER));
    }
  }

  private void copyGameJar() throws IOException {
    fileUtils.cp(options.get(InstallerOptions.JAR_FILE_SNAPSHOT), gameJarDestPath);
  }

  private void createProductionFile() throws IOException {
    fileUtils.touch(options.get(InstallerOptions.RELEASE_FOLDER) + "/.production");
  }

  private void copyLibraries() throws IOException {
    fileUtils.mkdirp(options.get(InstallerOptions.RELEASE_FOLDER) + "/game1");
    String nativesFolder = options.get(InstallerOptions.RELEASE_FOLDER) + "/game1/natives";
    fileUtils.mkdirp(nativesFolder);
    List<Dependency> d = dependencies.listDependencies();
    for (int i = 0; i < d.size(); i++) {
      Dependency dep = d.get(i);
      String source = mavenRepo + "/" + dep.getMavenFolder() + "/" + dep.getJarName();
      String dest = options.get(InstallerOptions.RELEASE_FOLDER) + "/game1/"
          + source.substring(source.lastIndexOf("/") + 1);
      fileUtils.cp(source, dest);

      if (dep.hasNatives(options.get(InstallerOptions.TARGET_OPERATING_SYSTEM))) {
        String nativesJar = String.format("%s-%s-natives-%s.jar", dep.getArtifactId(),
            dep.getVersion(), targetOperatingSystem);
        String nativesSource = mavenRepo + "/" + dep.getMavenFolder() + "/" + nativesJar;
        String nativesDest = nativesFolder + "/" + nativesJar;
        fileUtils.cp(nativesSource, nativesDest);
      }
    }
  }

  private void copyPomFile() throws IOException {
    fileUtils.cp("pom.xml", options.get(InstallerOptions.RELEASE_FOLDER) + "/pom.xml");
  }

  private void writeVersionDetails() throws IOException {
    Runtime run = Runtime.getRuntime();
    Process pr = run.exec(new String[] { "cmd", "/c", "git rev-parse HEAD" });
    BufferedReader outReader = new BufferedReader(new InputStreamReader(pr.getInputStream()));
    BufferedReader errReader = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
    List<String> outList = new ArrayList<>();
    String out = "";
    String err = "";
    while ((out = outReader.readLine()) != null || (err = errReader.readLine()) != null
        || pr.isAlive()) {
      if (out != null) {
        outList.add(out);
      }
      if (err != null && err.length() > 0) {
        Log.error(err);
      }
    }
    List<String> versionDetails = List.of(String.format("commit: %s", outList.get(0)),
        String.format("release: %s", options.get(InstallerOptions.GAME_VERSION)));
    String outputFile = options.get(InstallerOptions.RELEASE_FOLDER) + "/release-version.yaml";
    textIO.write(versionDetails, outputFile);
  }

  private void runJPackage() throws IOException {
    if (options.get(InstallerOptions.DEBUG) == null
        || Boolean.parseBoolean(options.get(InstallerOptions.DEBUG)) == false) {
      runFullJPackage();
    } else {
      runDebugJPackage();
    }
  }

  private void runFullJPackage() throws IOException {
    Log.info("Running full installer jpackage");
    String[] jPackageCmd = { String.format("\"%s\\bin\\jpackage\"", javaHome), "--input",
        options.get(InstallerOptions.RELEASE_FOLDER), "--dest",
        options.get(InstallerOptions.INSTALLER_FOLDER), "--name",
        String.format("\"%s\"", options.get(InstallerOptions.GAME_LONG_NAME)), "--icon",
        options.get(InstallerOptions.INSTALLER_ICON), "--app-version",
        options.get(InstallerOptions.GAME_VERSION), "--license-file",
        options.get(InstallerOptions.LICENSE), "--vendor", options.get(InstallerOptions.VENDOR),
        "--main-jar", options.get(InstallerOptions.JAR_FILE), "--main-class",
        options.get(InstallerOptions.MAIN_CLASS), "--type",
        options.get(InstallerOptions.INSTALLER_TYPE) };

    String[] windowsOptions = { "--win-dir-chooser", "--win-shortcut", "--win-menu",
        "--win-menu-group", String.format("\"%s\"", options.get(InstallerOptions.WIN_MENU_GROUP)) };

    String gameShortName = options.get(InstallerOptions.GAME_SHORT_NAME);
    String[] macosOptions = { "--mac-package-name",
        gameShortName.substring(0, Math.min(gameShortName.length(), 16))

        // --mac-package-identifier (ID string) An identifier that uniquely identifies the
        // application for macOSX. Defaults to the the main class name. May only use alphanumeric
        // (A-Z,a-z,0-9), hyphen (-), and period (.) characters.

        // --mac-package-name (name string) Name of the application as it appears in the Menu Bar.
        // This can be different from the application name. This name must be less than 16
        // characters long and be suitable for displaying in the menu bar and the application Info
        // window. Defaults to the application name.

        // --mac-bundle-signing-prefix (prefix string) When signing the application bundle, this
        // value is prefixed to all components that need to be signed that don't have an existing
        // bundle identifier.

        // --mac-sign Request that the bundle be signed.

        // --mac-signing-keychain (file path) Path of the keychain to search for the signing
        // identity (absolute path or relative to the current directory). If not specified, the
        // standard keychains are used.

        // --mac-signing-key-user-name (team name) Team name portion in Apple signing identities'
        // names. For example "Developer ID Application: "

    };

    String os = options.get(InstallerOptions.TARGET_OPERATING_SYSTEM);
    if (os.equals(InstallerConstants.WINDOWS)) {
      jPackageCmd = concatArrays(jPackageCmd, windowsOptions);
    } else if (os.equals(InstallerConstants.MAC_OS)) {
      jPackageCmd = concatArrays(jPackageCmd, macosOptions);
    } else {
      throw new RuntimeException(String.format(
          "Unsupported target operating system: '%s'. Valid options are '%s' and '%s'.", os,
          InstallerConstants.WINDOWS, InstallerConstants.MAC_OS));
    }

    if (options.get(InstallerOptions.DRY_RUN).equals("true")) {
      Log.info("DRY RUN: jPackageCmd = %s", Arrays.toString(jPackageCmd));
    } else {
      execHandler.exec(jPackageCmd);
    }
  }

  private String[] concatArrays(String[] array1, String[] array2) {
    String[] output = new String[array1.length + array2.length];
    for (int i = 0; i < array1.length; i++) {
      output[i] = array1[i];
    }
    for (int i = 0; i < array2.length; i++) {
      output[array1.length + i] = array2[i];
    }
    return output;
  }

  private void runDebugJPackage() throws IOException {
    Log.info("Running debug installer jpackage");
    String[] jPackageCmd = { String.format("\"%s\\bin\\jpackage\"", javaHome), "--input",
        options.get(InstallerOptions.RELEASE_FOLDER), "--dest",
        options.get(InstallerOptions.INSTALLER_DEBUG_FOLDER), "--name",
        String.format("\"%s\"", options.get(InstallerOptions.GAME_LONG_NAME)), "--icon",
        options.get(InstallerOptions.INSTALLER_ICON), "--main-jar",
        options.get(InstallerOptions.JAR_FILE), "--main-class",
        options.get(InstallerOptions.MAIN_CLASS), "--type", "app-image", "--win-console" };

    if (options.get(InstallerOptions.DRY_RUN).equals("true")) {
      Log.info("DRY RUN: jPackageCmd = %s", Arrays.toString(jPackageCmd));
    } else {
      execHandler.exec(jPackageCmd);
    }
  }

  private void deleteReleaseFolder() throws IOException {
    if (options.get(InstallerOptions.DEBUG).equals("false")) {
      fileUtils.rmrf(options.get(InstallerOptions.RELEASE_FOLDER));
    }
  }
}
