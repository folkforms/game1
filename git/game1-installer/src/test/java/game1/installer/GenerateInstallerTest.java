package game1.installer;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import folkforms.fileutils.FileUtils;
import folkforms.textio.TextIO;

/**
 * Note: If these tests are failing after updating dependencies, check `game1-installer/pom.xml` and
 * `game1-installer/src/test/resources/installer-test/pom.xml` have up-to-date versions.
 */
class GenerateInstallerTest {

  private static FileUtils fileUtils;
  private static TextIO textIO;
  private static ExecHandler execHandler;

  @BeforeAll
  public static void beforeAll() throws IOException {
    fileUtils = mock(FileUtils.class);
    doNothing().when(fileUtils).cp(anyString(), anyString());
    doNothing().when(fileUtils).mkdirp(anyString());
    doNothing().when(fileUtils).rmrf(anyString());
    doNothing().when(fileUtils).touch(anyString());
    textIO = mock(TextIO.class);
    doNothing().when(textIO).write(anyList(), anyString());
    execHandler = mock(ExecHandler.class);
    doNothing().when(execHandler).exec(any());
  }

  @Test
  void testFullInstaller() throws IOException, InstallerException {
    String os = InstallerConstants.WINDOWS;
    boolean debug = false;
    boolean dryRun = false;

    InstallerOptions.TEST_POM_FILE = "src/test/resources/installer-test/pom.xml";
    InstallerOptions options = InstallerOptions.newInstance(os, debug, dryRun);
    new GenerateInstaller(fileUtils, textIO, execHandler).generate(options);

    String[] expected = { //
        "\"c:\\Program Files\\Java\\jdk-17.0.1\\bin\\jpackage\"", //
        "--input", //
        "_temp_release", //
        "--dest", //
        "_installer", //
        "--name", //
        "\"Debug Game\"", //
        "--icon", //
        "D:/dev/projects/game1/git/game1-installer/src/main/resources/default-game1-icon.ico", //
        "--app-version", //
        "0.0.1", //
        "--license-file", //
        "D:/dev/projects/game1/git/game1-installer/src/main/resources/default-game-license.txt", //
        "--vendor", //
        "Red Octopus Games", //
        "--main-jar", //
        "debug-game-0.0.1.jar", //
        "--main-class", //
        "folkforms.debuggame.main.DebugGameMain", //
        "--type", //
        "msi", //
        "--win-dir-chooser", //
        "--win-shortcut", //
        "--win-menu", //
        "--win-menu-group", //
        "\"Debug Win Menu Group\"" //
    };
    verify(execHandler, times(1)).exec(expected);
  }

  @Test
  void testDebugInstaller() throws IOException, InstallerException {
    String os = InstallerConstants.WINDOWS;
    boolean debug = true;
    boolean dryRun = false;

    InstallerOptions.TEST_POM_FILE = "src/test/resources/installer-test/pom.xml";
    InstallerOptions options = InstallerOptions.newInstance(os, debug, dryRun);
    new GenerateInstaller(fileUtils, textIO, execHandler).generate(options);

    String[] expected = { //
        "\"c:\\Program Files\\Java\\jdk-17.0.1\\bin\\jpackage\"", //
        "--input", //
        "_temp_release", //
        "--dest", //
        "_installer_debug", //
        "--name", //
        "\"Debug Game\"", //
        "--icon", //
        "D:/dev/projects/game1/git/game1-installer/src/main/resources/default-game1-icon.ico", //
        "--main-jar", //
        "debug-game-0.0.1.jar", //
        "--main-class", //
        "folkforms.debuggame.main.DebugGameMain", //
        "--type", //
        "app-image", //
        "--win-console" //
    };
    verify(execHandler, times(1)).exec(expected);
  }
}
