package game1.installer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.Test;

class InstallerOptionsTest {

  @Test
  void testInstallerOptions() throws IOException, InstallerException {
    String os = InstallerConstants.WINDOWS;
    boolean debug = false;
    boolean dryRun = false;

    InstallerOptions.TEST_POM_FILE = "src/test/resources/installer-test/pom.xml";
    InstallerOptions options = InstallerOptions.newInstance(os, debug, dryRun);

    assertEquals("0.0.14-SNAPSHOT", options.get(InstallerOptions.GAME1_LIB_VERSION));
    assertEquals("Debug Game", options.get(InstallerOptions.GAME_LONG_NAME));
    assertEquals("debug-game", options.get(InstallerOptions.GAME_SHORT_NAME));
    assertEquals("0.0.1", options.get(InstallerOptions.GAME_VERSION));
    assertEquals("folkforms.debuggame.main.DebugGameMain",
        options.get(InstallerOptions.MAIN_CLASS));
    assertEquals("Red Octopus Games", options.get(InstallerOptions.VENDOR));
    assertEquals("debug-game-0.0.1.jar", options.get(InstallerOptions.JAR_FILE));
    assertEquals("false", options.get(InstallerOptions.DEBUG));
    assertEquals("false", options.get(InstallerOptions.DRY_RUN));
    assertEquals(InstallerConstants.WINDOWS, options.get(InstallerOptions.TARGET_OPERATING_SYSTEM));
  }
}
