package game1.toolkit.cutscenes.player;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import game1.cutscenes.v9c.antlr.c9.C9LanguageFragment;
import game1.toolkit.cutscenes.test_fixtures.CutscenesTestBase;

class StartSmallTest extends CutscenesTestBase {

  @Test
  void itSetsVariables() throws IOException {
    C9LanguageFragment c9LanguageFragment = new C9LanguageFragment("foo",
        loadFile("src/test/resources/start-small.c9"));
    globalVariables.put("foo", c9LanguageFragment);

    CutscenePlayerUtils.playStatement("foo()", globalVariables);

    float x = globalVariables.getAsNumber("x");
    float y = globalVariables.getAsNumber("y");
    float a = globalVariables.getAsNumber("a");
    float b = globalVariables.getAsNumber("b");
    float c = globalVariables.getAsNumber("c");
    float d = globalVariables.getAsNumber("d");
    String str3 = globalVariables.getAsString("str3");
    assertEquals(2, x);
    assertEquals(3, y);
    assertEquals(5, a);
    assertEquals(-1, b);
    assertEquals(6, c);
    assertEquals(0.6666666, d, 0.0000001);
    assertEquals("foobar", str3);
  }
}
