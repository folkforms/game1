package game1.toolkit.cutscenes.player;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;

import game1.toolkit.cutscenes.test_fixtures.CutscenesTestBase;
import game1.toolkit.cutscenes.test_fixtures.DummyCutsceneFunction1;
import game1.toolkit.cutscenes.test_fixtures.DummyCutsceneFunction2;

class CutscenePlayerUtilsTest extends CutscenesTestBase {

  @Test
  void testSimpleFunction() throws IOException {
    float x = CutscenePlayerUtils.playStatement("1 + 2", globalVariables);
    assertEquals(3, x);
  }

  @Test
  void testFunctionWithVariables() throws IOException {
    globalVariables.put("a", 2);
    globalVariables.put("b", 3);
    float x = CutscenePlayerUtils.playStatement("@a + @b", globalVariables);
    assertEquals(5, x);
  }

  @Test
  void testEqualityFunctionWithVariables() throws IOException {
    globalVariables.put("a", 2);
    globalVariables.put("b", 3);
    globalVariables.put("c", 3);
    globalVariables.put("d", true);
    globalVariables.put("e", false);
    globalVariables.put("f", false);
    globalVariables.put("g", true);
    boolean b1 = CutscenePlayerUtils.playStatement("@a == @b", globalVariables);
    assertEquals(false, b1);
    boolean b2 = CutscenePlayerUtils.playStatement("@b == @c", globalVariables);
    assertEquals(true, b2);
    boolean b3 = CutscenePlayerUtils.playStatement("@d == @e", globalVariables);
    assertEquals(false, b3);
    boolean b4 = CutscenePlayerUtils.playStatement("@e == @f", globalVariables);
    assertEquals(true, b4);
    boolean b5 = CutscenePlayerUtils.playStatement("@d == @g", globalVariables);
    assertEquals(true, b5);
  }

  @Test
  void testSetVariable() throws IOException {
    globalVariables.put("a", 2);
    float x = CutscenePlayerUtils.playStatement("@a = 3", globalVariables);
    assertEquals(3, x);
    assertEquals(3f, (float) globalVariables.getAsNumber("a"));
  }

  @Test
  void testPlayList() throws IOException {
    List<String> list = List.of("@a = 2", "@b = 3", "@c = @a + @b");
    CutscenePlayerUtils.playStatements(list, globalVariables);
    assertEquals(5f, (float) globalVariables.getAsNumber("c"));
  }

  @Test
  void itRegistersCutsceneFunctionsAsGlobalVariables() {
    CutscenePlayerUtils.registerCutsceneFunctionsAsGlobalVariables(
        List.of(DummyCutsceneFunction1.class, DummyCutsceneFunction2.class), globalVariables);

    DummyCutsceneFunction1 dummyCutsceneFunction1 = globalVariables
        .get("dummy-cutscene-function-1");
    Object result1 = dummyCutsceneFunction1.execute(List.of(), localVariables);
    assertTrue(result1 instanceof String);
    String result1Str = (String) result1;
    assertEquals("dummy-cutscene-function-1-ok", result1Str);
    DummyCutsceneFunction2 dummyCutsceneFunction2 = globalVariables
        .get("dummy-cutscene-function-2");
    Object result2 = dummyCutsceneFunction2.execute(List.of(), localVariables);
    assertTrue(result2 instanceof String);
    String result2Str = (String) result2;
    assertEquals("dummy-cutscene-function-2-ok", result2Str);
  }
}
