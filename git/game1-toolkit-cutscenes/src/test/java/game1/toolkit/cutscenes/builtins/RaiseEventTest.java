package game1.toolkit.cutscenes.builtins;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.cutscenes.v9c.antlr.statements.ArgType;
import game1.debug.DebugScopes;
import game1.events.EventWithData;
import game1.events.Events;
import game1.events.EventsFactory;
import game1.variables.Variables;

public class RaiseEventTest {

  protected Variables globalVariables;
  protected Variables localVariables;
  private Events events;

  @BeforeEach
  public void beforeEach() throws IOException {
    DebugScopes.init();
    globalVariables = new Variables();
    localVariables = new Variables();
    events = new Events();
    EventsFactory.setInstance(events);
  }

  @Test
  void itRaisesEvents() throws IOException {
    Object obj1 = new Object();
    Object obj2 = new Object();
    globalVariables.put("global_variable", obj1);
    localVariables.put("local_variable", obj2);
    Arg arg1 = new Arg("\"foo_event\"", ArgType.STRING_LITERAL);
    Arg arg2 = new Arg("\"plain_string\"", ArgType.STRING_LITERAL);
    Arg arg3 = new Arg("@global_variable", ArgType.VARIABLE);
    Arg arg4 = new Arg("local_variable", ArgType.VARIABLE);
    List<Arg> args = List.of(arg1, arg2, arg3, arg4);
    RaiseEvent raiseEvent = new RaiseEvent(globalVariables);

    raiseEvent.execute(args, localVariables);

    EventWithData e = events.debug_listEvents().get(0);
    assertEquals("foo_event", e.getName());
    Object[] eventData = e.getData();
    assertEquals("plain_string", eventData[0]);
    assertEquals(obj1, eventData[1]);
    assertEquals(obj2, eventData[2]);
  }
}
