package game1.toolkit.cutscenes.test_fixtures;

import java.io.IOException;

import folkforms.textio.TextIO;
import folkforms.yaml.YamlFile;
import game1.core.engine.GameVariables;

public class GameVariablesForTests extends GameVariables {

  @Override
  protected YamlFile loadYamlFile(String filename) throws IOException {
    String contents = new TextIO().readAsString(filename);
    return new YamlFile(contents);
  }
}
