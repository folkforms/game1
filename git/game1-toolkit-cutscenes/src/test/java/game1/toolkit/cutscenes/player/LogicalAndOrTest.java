package game1.toolkit.cutscenes.player;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import game1.cutscenes.v9c.antlr.c9.C9LanguageFragment;
import game1.toolkit.cutscenes.test_fixtures.CutscenesTestBase;

class LogicalAndOrTest extends CutscenesTestBase {

  @Test
  void itSetsVariables() throws IOException {
    C9LanguageFragment c9LanguageFragment = new C9LanguageFragment("foo",
        loadFile("src/test/resources/logical-and-or.c9"));
    globalVariables.put("foo", c9LanguageFragment);

    CutscenePlayerUtils.playStatement("foo()", globalVariables);

    boolean a = globalVariables.getAsBoolean("a");
    boolean b = globalVariables.getAsBoolean("b");
    boolean c = globalVariables.getAsBoolean("c");
    boolean d = globalVariables.getAsBoolean("d");
    boolean e = globalVariables.getAsBoolean("e");
    boolean f = globalVariables.getAsBoolean("f");
    boolean g = globalVariables.getAsBoolean("g");
    boolean h = globalVariables.getAsBoolean("h");
    assertEquals(true, a);
    assertEquals(false, b);
    assertEquals(false, c);
    assertEquals(false, d);
    assertEquals(true, e);
    assertEquals(true, f);
    assertEquals(true, g);
    assertEquals(false, h);
  }
}
