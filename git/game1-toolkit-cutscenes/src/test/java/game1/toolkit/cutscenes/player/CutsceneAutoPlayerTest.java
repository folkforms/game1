package game1.toolkit.cutscenes.player;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import game1.cutscenes.v9c.antlr.c9.C9LanguageFragment;
import game1.toolkit.cutscenes.test_fixtures.CutscenesTestBase;

public class CutsceneAutoPlayerTest extends CutscenesTestBase {

  @Test
  public void itPlaysAsManyLinesAsPossible() {
    List<String> functionText = List.of("@a = 1;", "@a = 2;", "@a = 3;");
    C9LanguageFragment fragment = new C9LanguageFragment("test1", functionText);
    globalVariables.put("foo", fragment);
    CutsceneAutoPlayer cutsceneAutoPlayer = new CutsceneAutoPlayer("foo", globalVariables);
    globalVariables.put("a", 0);

    cutsceneAutoPlayer.onTick();
    float a = globalVariables.get("a");
    assertEquals(3, a);
  }

  @Test
  public void itWaits() {
    List<String> functionText = List.of("@a = 1;", "@a = 2;", "@a = 3;", "wait(2);", "@a = 4;",
        "@a = 5;");
    C9LanguageFragment fragment = new C9LanguageFragment("test1", functionText);
    globalVariables.put("foo", fragment);
    CutsceneAutoPlayer cutsceneAutoPlayer = new CutsceneAutoPlayer("foo", globalVariables);
    globalVariables.put("a", 0);

    // Action first three statements
    cutsceneAutoPlayer.onTick();
    float a1 = globalVariables.get("a");
    assertEquals(3, a1);
    int waiting1 = globalVariables.get("game1.cutscenes.numWaitingTicks");
    assertEquals(2, waiting1);

    // Wait this tick
    cutsceneAutoPlayer.onTick();
    float a2 = globalVariables.get("a");
    assertEquals(3, a2);
    int waiting2 = globalVariables.get("game1.cutscenes.numWaitingTicks");
    assertEquals(1, waiting2);

    // Wait another tick
    cutsceneAutoPlayer.onTick();
    float a3 = globalVariables.get("a");
    assertEquals(3, a3);
    int waiting3 = globalVariables.get("game1.cutscenes.numWaitingTicks");
    assertEquals(0, waiting3);

    // Action final two statements
    cutsceneAutoPlayer.onTick();
    float a4 = globalVariables.get("a");
    assertEquals(5, a4);
  }
}
