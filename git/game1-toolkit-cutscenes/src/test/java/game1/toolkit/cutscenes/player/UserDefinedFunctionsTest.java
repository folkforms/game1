package game1.toolkit.cutscenes.player;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;

import game1.cutscenes.v9c.antlr.c9.C9LanguageFragment;
import game1.cutscenes.v9c.antlr.c9fileparser.C9FileParser;
import game1.toolkit.cutscenes.test_fixtures.CutscenesTestBase;

class UserDefinedFunctionsTest extends CutscenesTestBase {

  @Test
  void itCanRunUserDefinedFunctions() throws IOException {
    List<String> input = loadFile(
        "src/test/resources/user-defined-functions/user-defined-functions-1.c9");
    List<C9LanguageFragment> functions = new C9FileParser(input).parse();
    functions.forEach(f -> globalVariables.put(f.getFunctionName(), f));
    globalVariables.put("x", "x is a global variable");

    CutscenePlayerUtils.playStatement("foo(2, 'hi')", globalVariables);
    assertEquals(1f, (float) globalVariables.getAsNumber("fooOutput"));

    CutscenePlayerUtils.playCutsceneTag("#bar bar", globalVariables);
    assertEquals(2f, (float) globalVariables.getAsNumber("barOutput"));
  }

  @Test
  void itCanRunUserDefinedFunctionsWithinUserDefinedFunctions() throws IOException {
    List<String> input = loadFile(
        "src/test/resources/user-defined-functions/user-defined-functions-2.c9");
    List<C9LanguageFragment> functions = new C9FileParser(input).parse();
    functions.forEach(f -> globalVariables.put(f.getFunctionName(), f));

    CutscenePlayerUtils.playStatement("foo()", globalVariables);
    assertEquals(1f, (float) globalVariables.getAsNumber("fooOutput"));
    assertEquals(2f, (float) globalVariables.getAsNumber("barOutput"));
    assertEquals(4f, (float) globalVariables.getAsNumber("mukOutput"));
  }
}
