package game1.toolkit.cutscenes.test_fixtures;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import folkforms.textio.TextIO;
import game1.core.engine.GameVariables;
import game1.debug.DebugScopes;
import game1.toolkit.cutscenes.init.Game1ToolkitCutscenesModule;
import game1.variables.Variables;
import game1.variables.VariablesFactory;

public class CutscenesTestBase {

  protected GameVariables globalVariables;
  protected Variables localVariables;

  @BeforeAll
  public static void beforeAll() {
    DebugScopes.init();
    DebugScopes.activate("Cutscenes");
    // Log.setLevel(Log.LEVEL_DEBUG);
  }

  @BeforeEach
  public void beforeEach() throws IOException {
    globalVariables = new GameVariablesForTests();
    VariablesFactory.setInstance(globalVariables);
    Game1ToolkitCutscenesModule.postEngineInit();
    localVariables = new Variables();
  }

  public List<String> loadFile(String path) throws IOException {
    return new TextIO().readAsList(path);
  }
}
