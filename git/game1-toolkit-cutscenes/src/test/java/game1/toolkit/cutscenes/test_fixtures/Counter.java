package game1.toolkit.cutscenes.test_fixtures;

public class Counter {

  private int count;

  public Counter(int initialValue) {
    this.count = initialValue;
  }

  public int getValue() {
    return count;
  }

  public void increment() {
    count++;
  }

  public void setValueToSumOf(float a, float b, float c) {
    count = (int) (a + b + c);
  }

  public void setValueToSumOf(float a, float b) {
    count = (int) (a + b);
  }
}
