package game1.toolkit.cutscenes.builtins;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.cutscenes.v9c.antlr.statements.ArgType;
import game1.variables.Variables;

public class GetGlobalVariableTest {

  protected Variables globalVariables;
  protected Variables localVariables;

  @BeforeEach
  public void beforeEach() throws IOException {
    globalVariables = new Variables();
    localVariables = new Variables();
  }

  @Test
  void itGetsGlobalVariables() throws IOException {
    globalVariables.put("foo.bar1", true);
    globalVariables.put("foo.bar2", false);
    globalVariables.put("muk", 5f);
    globalVariables.put("pad", "hello");
    GetGlobalVariable getGlobalVariables = new GetGlobalVariable(globalVariables);

    List<Arg> args1 = List.of(new Arg("\"foo.bar1\"", ArgType.STRING_LITERAL));
    Object result1 = getGlobalVariables.execute(args1, localVariables);
    assertTrue(result1 instanceof Boolean);
    assertEquals(true, Boolean.parseBoolean(result1.toString()));

    List<Arg> args2 = List.of(new Arg("\"foo.bar2\"", ArgType.STRING_LITERAL));
    Object result2 = getGlobalVariables.execute(args2, localVariables);
    assertTrue(result2 instanceof Boolean);
    assertEquals(false, Boolean.parseBoolean(result2.toString()));

    List<Arg> args3 = List.of(new Arg("\"muk\"", ArgType.STRING_LITERAL));
    Object result3 = getGlobalVariables.execute(args3, localVariables);
    assertTrue(result3 instanceof Float);
    assertEquals(5f, Float.parseFloat(result3.toString()));

    List<Arg> args4 = List.of(new Arg("\"pad\"", ArgType.STRING_LITERAL));
    Object result4 = getGlobalVariables.execute(args4, localVariables);
    assertTrue(result4 instanceof String);
    assertEquals("hello", result4.toString());
  }
}
