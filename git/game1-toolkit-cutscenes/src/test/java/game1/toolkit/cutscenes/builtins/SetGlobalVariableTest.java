package game1.toolkit.cutscenes.builtins;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.cutscenes.v9c.antlr.statements.ArgType;
import game1.variables.Variables;

public class SetGlobalVariableTest {

  protected Variables globalVariables;
  protected Variables localVariables;

  @BeforeEach
  public void beforeEach() throws IOException {
    globalVariables = new Variables();
    localVariables = new Variables();
  }

  @Test
  void itSetsGlobalVariables() throws IOException {
    globalVariables.put("foo.bar1", true);
    globalVariables.put("foo.bar2", false);
    globalVariables.put("muk", 5f);
    globalVariables.put("pad", "hello");
    SetGlobalVariable setGlobalVariables = new SetGlobalVariable(globalVariables);

    List<Arg> args1 = List.of(new Arg("\"foo.bar1\"", ArgType.STRING_LITERAL),
        new Arg("false", ArgType.BOOLEAN_LITERAL));
    Object result1 = setGlobalVariables.execute(args1, localVariables);
    assertTrue(result1 instanceof Boolean);
    assertEquals(false, Boolean.parseBoolean(result1.toString()));
    boolean result1b = globalVariables.get("foo.bar1");
    assertEquals(false, result1b);

    List<Arg> args2 = List.of(new Arg("\"foo.bar2\"", ArgType.STRING_LITERAL),
        new Arg("true", ArgType.BOOLEAN_LITERAL));
    Object result2 = setGlobalVariables.execute(args2, localVariables);
    assertTrue(result2 instanceof Boolean);
    assertEquals(true, Boolean.parseBoolean(result2.toString()));
    boolean result2b = globalVariables.get("foo.bar2");
    assertEquals(true, result2b);

    List<Arg> args3 = List.of(new Arg("\"muk\"", ArgType.STRING_LITERAL),
        new Arg("6", ArgType.FLOAT_LITERAL));
    Object result3 = setGlobalVariables.execute(args3, localVariables);
    assertTrue(result3 instanceof Float);
    assertEquals(6f, Float.parseFloat(result3.toString()));
    float result3b = globalVariables.get("muk");
    assertEquals(6f, result3b);

    List<Arg> args4 = List.of(new Arg("\"pad\"", ArgType.STRING_LITERAL),
        new Arg("goodbye", ArgType.STRING_LITERAL));
    Object result4 = setGlobalVariables.execute(args4, localVariables);
    assertTrue(result4 instanceof String);
    assertEquals("goodbye", result4.toString());
    String result4b = globalVariables.get("pad");
    assertEquals("goodbye", result4b);

  }
}
