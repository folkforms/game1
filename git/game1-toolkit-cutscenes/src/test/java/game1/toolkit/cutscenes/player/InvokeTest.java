package game1.toolkit.cutscenes.player;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import game1.cutscenes.v9c.antlr.c9.C9LanguageFragment;
import game1.toolkit.cutscenes.test_fixtures.Counter;
import game1.toolkit.cutscenes.test_fixtures.CutscenesTestBase;

class InvokeTest extends CutscenesTestBase {

  @Test
  void itInvokesObjectMethods() throws IOException {
    C9LanguageFragment c9LanguageFragment = new C9LanguageFragment("foo",
        loadFile("src/test/resources/invoke.c9"));
    globalVariables.put("foo", c9LanguageFragment);
    globalVariables.put("counter", new Counter(22));

    CutscenePlayerUtils.playStatement("foo()", globalVariables);

    float x = globalVariables.getAsNumber("x");
    float y = globalVariables.getAsNumber("y");
    float z = globalVariables.getAsNumber("z");
    float w = globalVariables.getAsNumber("w");
    assertEquals(22, x);
    assertEquals(23, y);
    assertEquals(18, z);
    assertEquals(7, w);
  }
}
