package game1.toolkit.cutscenes.test_fixtures;

import java.util.List;

import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.toolkit.cutscenes.builtins.CutsceneFunction;
import game1.variables.Variables;

public class DummyCutsceneFunction2 extends CutsceneFunction {

  public DummyCutsceneFunction2(Variables globalVariables) {
    super(globalVariables);
  }

  @Override
  public String getName() {
    return "dummy-cutscene-function-2";
  }

  @Override
  public Object execute(List<Arg> args, Variables localVariables) {
    return "dummy-cutscene-function-2-ok";
  }
}
