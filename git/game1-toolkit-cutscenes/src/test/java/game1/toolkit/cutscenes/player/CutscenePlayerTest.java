package game1.toolkit.cutscenes.player;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import game1.cutscenes.v9c.antlr.c9.C9LanguageFragment;
import game1.toolkit.cutscenes.test_fixtures.CutscenesTestBase;

public class CutscenePlayerTest extends CutscenesTestBase {

  private List<String> functionText = List.of("@a = 1;", "@a = 2;", "@a = 3;");

  @Test
  public void itPlaysAFragmentLineByLine() {
    C9LanguageFragment fragment = new C9LanguageFragment("test1", functionText);
    CutscenePlayer cutscenePlayer = new CutscenePlayer(fragment, globalVariables);
    globalVariables.put("a", 0);

    cutscenePlayer.playNextStatement();
    float a1 = globalVariables.get("a");
    assertEquals(1, a1);

    cutscenePlayer.playNextStatement();
    float a2 = globalVariables.get("a");
    assertEquals(2, a2);

    cutscenePlayer.playNextStatement();
    float a3 = globalVariables.get("a");
    assertEquals(3, a3);
  }
}
