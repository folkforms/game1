package game1.toolkit.cutscenes.player;

import java.lang.reflect.Constructor;
import java.util.List;

import game1.core.engine.Game1RuntimeException;
import game1.cutscenes.v9c.antlr.c9.C9LanguageFragment;
import game1.cutscenes.v9c.antlr.c9fileparser.CutsceneUtils;
import game1.toolkit.cutscenes.builtins.CutsceneFunction;
import game1.variables.Variables;

/**
 * Utility functions for playing cutscenes either one line at a time, or playing an arbitrary set of
 * statements.
 *
 * Does not support the `wait` function.
 */
public class CutscenePlayerUtils {

  private static long functionNameIndex = 0;

  /**
   * Plays a cutscene tag e.g. "#foo". The '#' is optional.
   */
  public static <T> T playCutsceneTag(String cutsceneTag, Variables globalVariables) {
    String statement = CutsceneUtils.convertTagToFunctionName(cutsceneTag) + "()";
    return playStatement(statement, globalVariables);
  }

  public static <T> T playStatement(String statement, Variables globalVariables) {
    return playSingleLine(statement, globalVariables);
  }

  @SuppressWarnings("unchecked")
  private static <T> T playSingleLine(String line, Variables globalVariables) {
    if (!line.endsWith(";")) {
      line += ";";
    }
    String randomFunctionName = getRandomFunctionName("CutscenePlayerUtils#playSingleLine");
    C9LanguageFragment frag = new C9LanguageFragment(randomFunctionName, line);
    CutscenePlayer cutscenePlayer = new CutscenePlayer(frag, globalVariables);
    // The "single line" might be a function call like "foo()" so we play everything
    playToEnd(cutscenePlayer);
    Object result = globalVariables.getAsObject("__prev");
    globalVariables.remove(randomFunctionName);
    return (T) result;
  }

  @SuppressWarnings("unchecked")
  public static <T> T playStatements(List<String> lines, Variables globalVariables) {
    lines = lines.stream().map(line -> line.endsWith(";") ? line : line + ";").toList();
    String randomFunctionName = getRandomFunctionName("CutscenePlayerUtils#playStatements");
    C9LanguageFragment frag = new C9LanguageFragment(randomFunctionName, lines);
    CutscenePlayer cutscenePlayer = new CutscenePlayer(frag, globalVariables);
    playToEnd(cutscenePlayer);
    Object result = globalVariables.getAsObject("__prev");
    return (T) result;
  }

  private static void playToEnd(CutscenePlayer cutscenePlayer) {
    while (cutscenePlayer.hasNext()) {
      cutscenePlayer.playNextStatement();
    }
  }

  private static String getRandomFunctionName(String functionName) {
    return String.format("generated-function-name-%s-%s", functionName, functionNameIndex++);
  }

  /**
   * Used by games to register their custom functions.
   *
   * @param cutsceneFunctionClasses
   *          CutsceneFunction classes to register
   * @param globalVariables
   *          global variables object to store the custom functions
   */
  public static void registerCutsceneFunctionsAsGlobalVariables(
      List<Class<?>> cutsceneFunctionClasses, Variables globalVariables) {
    cutsceneFunctionClasses.forEach(cutsceneFunctionClass -> {
      try {
        Constructor<?> constructor = cutsceneFunctionClass.getConstructor(Variables.class);
        Object instance = constructor.newInstance(globalVariables);
        CutsceneFunction cutsceneFunction = (CutsceneFunction) instance;
        globalVariables.put(cutsceneFunction.getName(), cutsceneFunction);
      } catch (Exception e) {
        throw new Game1RuntimeException(e,
            "Error registering cutscene function as global variable: cutsceneFunctionClass = %s, globalVariables = %s",
            cutsceneFunctionClass, globalVariables);
      }
    });
  }
}
