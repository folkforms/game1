package game1.toolkit.cutscenes.builtins;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import folkforms.log.Log;
import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.debug.DebugScopes;
import game1.variables.Variables;

public class Invoke extends CutsceneFunction {

  public Invoke(Variables globalVariables) {
    super(globalVariables);
  }

  @Override
  public String getName() {
    return "_invoke";
  }

  @Override
  public boolean shouldResolveArgs() {
    return false;
  }

  @Override
  public Object execute(List<Arg> args, Variables localVariables) {
    Log.debug(DebugScopes.CUTSCENES, "args = %s", args);
    String objName = args.get(0).getText();
    String method = args.get(1).getText();
    method = method.substring(1, method.length() - 1);
    Object[] params = new Object[args.size() - 2];
    for (int i = 2; i < args.size(); i++) {
      params[i - 2] = args.get(i).asObject();
    }

    Object obj = objName.startsWith("@") ? globalVariables.get(objName.substring(1))
      : localVariables.get(objName);

    try {
      Class<?> forName = Class.forName(obj.getClass().getName());
      Method[] methods = forName.getMethods();
      for (Method m : methods) {
        if (m.getName().equals(method) && m.getParameterCount() == params.length) {
          return m.invoke(obj, params);
        }
      }
    } catch (ClassNotFoundException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException ex) {
      throw new RuntimeException(
          String.format("ERROR: _invoke(%s) failed. See stack trace below.", args), ex);
    }
    return null;
  }
}
