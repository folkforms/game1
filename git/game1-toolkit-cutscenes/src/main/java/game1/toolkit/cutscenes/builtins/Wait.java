package game1.toolkit.cutscenes.builtins;

import java.util.List;

import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.variables.Variables;

public class Wait extends CutsceneFunction {

  public Wait(Variables globalVariables) {
    super(globalVariables);
  }

  @Override
  public String getName() {
    return "wait";
  }

  @Override
  public Object execute(List<Arg> args, Variables localVariables) {
    validateNumArgsIsExactly(args, 1);

    // FIXME Set something in cutscenes to wait for a countdown

    int numWaitingTicks = (int) args.get(0).asFloat();

    globalVariables.set("game1.cutscenes.numWaitingTicks", numWaitingTicks);

    return null;
  }
}
