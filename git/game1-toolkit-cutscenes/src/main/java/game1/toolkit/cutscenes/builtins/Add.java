package game1.toolkit.cutscenes.builtins;

import java.util.List;

import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.cutscenes.v9c.antlr.statements.ArgType;
import game1.variables.Variables;

public class Add extends CutsceneFunction {

  public Add(Variables globalVariables) {
    super(globalVariables);
  }

  @Override
  public String getName() {
    return "_add";
  }

  @Override
  public Object execute(List<Arg> args, Variables localVariables) {
    validateNumArgsIsExactly(args, 2);
    Arg arg0 = args.get(0);
    Arg arg1 = args.get(1);

    if (arg0.getType() == ArgType.FLOAT_LITERAL && arg1.getType() == ArgType.FLOAT_LITERAL) {
      float float0 = arg0.asFloat();
      float float1 = arg1.asFloat();
      return float0 + float1;
    }
    if (arg0.getType() == ArgType.STRING_LITERAL && arg1.getType() == ArgType.STRING_LITERAL) {
      String s0 = arg0.asString();
      s0 = removeQuotes(s0);
      String s1 = arg1.asString();
      s1 = removeQuotes(s1);
      return s0 + s1;
    }
    if (arg0.getType() == ArgType.STRING_LITERAL && arg1.getType() == ArgType.FLOAT_LITERAL) {
      String s0 = arg0.asString();
      s0 = removeQuotes(s0);
      float f1 = arg1.asFloat();
      return s0 + f1;
    }
    if (arg0.getType() == ArgType.FLOAT_LITERAL && arg1.getType() == ArgType.STRING_LITERAL) {
      float f0 = arg0.asFloat();
      String s1 = arg1.asString();
      s1 = removeQuotes(s1);
      return f0 + s1;
    }

    throw new RuntimeException(String.format("%s: Unable to handle addition of %s + %s",
        getClass().getSimpleName(), arg0, arg1));
  }

  private String removeQuotes(String str) {
    if (str.charAt(0) == '"' && str.charAt(str.length() - 1) == '"') {
      str = str.substring(1, str.length() - 1);
    }
    return str;
  }
}
