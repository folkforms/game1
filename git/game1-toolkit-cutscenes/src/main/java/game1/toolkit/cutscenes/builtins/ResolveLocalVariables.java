package game1.toolkit.cutscenes.builtins;

import java.util.List;

import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.cutscenes.v9c.antlr.statements.ArgType;
import game1.variables.Variables;

public class ResolveLocalVariables extends CutsceneFunction {

  public ResolveLocalVariables(Variables globalVariables) {
    super(globalVariables);
  }

  @Override
  public String getName() {
    return "_resolveLocalVariables";
  }

  @Override
  public boolean shouldResolveArgs() {
    return false;
  }

  @Override
  public Object execute(List<Arg> args, Variables localVariables) {
    for (int i = 0; i < args.size(); i++) {
      Arg arg = args.get(i);
      if (arg.getType() == ArgType.VARIABLE) {
        String otherName = "__localArg" + i;
        Object obj = localVariables.getAsObject(otherName);
        localVariables.put(arg.getText(), obj);
        localVariables.remove(otherName);
      }
    }
    return null;
  }
}
