package game1.toolkit.cutscenes.builtins;

import java.util.List;

import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.variables.Variables;

public class LogicalOr extends CutsceneFunction {

  public LogicalOr(Variables globalVariables) {
    super(globalVariables);
  }

  @Override
  public String getName() {
    return "_logicalOr";
  }

  @Override
  public Object execute(List<Arg> args, Variables localVariables) {
    validateNumArgsIsExactly(args, 2);

    boolean b1 = args.get(0).asBoolean();
    boolean b2 = args.get(1).asBoolean();

    return b1 || b2;
  }
}
