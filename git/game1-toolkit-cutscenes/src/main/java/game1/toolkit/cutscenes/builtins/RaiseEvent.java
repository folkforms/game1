package game1.toolkit.cutscenes.builtins;

import java.util.List;

import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.cutscenes.v9c.antlr.statements.ArgType;
import game1.events.EventsFactory;
import game1.variables.Variables;

public class RaiseEvent extends CutsceneFunction {

  public RaiseEvent(Variables globalVariables) {
    super(globalVariables);
  }

  @Override
  public String getName() {
    return "raiseEvent";
  }

  @Override
  public Object execute(List<Arg> args, Variables localVariables) {
    validateNumArgsIsAtLeast(args, 1);
    String eventName = removeQuotes(args.get(0).asString());
    Object[] otherArgs = new Object[args.size() - 1];
    for (int i = 1; i < args.size(); i++) {
      Arg arg = args.get(i);
      if (arg.getType() == ArgType.STRING_LITERAL) {
        otherArgs[i - 1] = removeQuotes(arg.asString());
      } else if (arg.getType() == ArgType.VARIABLE) {
        String name = arg.getText();
        Object obj = name.startsWith("@") ? globalVariables.get(name.substring(1))
          : localVariables.get(name);
        otherArgs[i - 1] = obj;
      } else {
        otherArgs[i - 1] = arg.asObject();
      }
    }
    EventsFactory.getInstance().raise(eventName, otherArgs);
    return null;
  }

  private String removeQuotes(String str) {
    if (str.charAt(0) == '"' && str.charAt(str.length() - 1) == '"') {
      str = str.substring(1, str.length() - 1);
    }
    return str;
  }
}
