package game1.toolkit.cutscenes.builtins;

import java.util.List;

import game1.core.engine.Game1RuntimeException;
import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.cutscenes.v9c.antlr.statements.ArgType;
import game1.variables.Variables;

public abstract class CutsceneFunction {

  protected Variables globalVariables;

  protected CutsceneFunction(Variables globalVariables) {
    this.globalVariables = globalVariables;
  }

  public abstract String getName();

  public abstract Object execute(List<Arg> args, Variables localVariables);

  protected void validateNumArgsIsExactly(List<Arg> args, int exactNumberOfArgs) {
    if (args.size() != exactNumberOfArgs) {
      throw new RuntimeException(String.format("%s: Expected exactly %s args but received %s (%s)",
          getClass().getSimpleName(), exactNumberOfArgs, args.size()));
    }
  }

  protected void validateNumArgsIsAtLeast(List<Arg> args, int minimumNumberOfArgs) {
    if (args.size() < minimumNumberOfArgs) {
      throw new RuntimeException(String.format("%s: Expected at least %s args but received %s (%s)",
          getClass().getSimpleName(), minimumNumberOfArgs, args.size()));
    }
  }

  public boolean shouldResolveArgs() {
    return true;
  }

  /**
   * Convert arguments into global or local variables when it is correct to do so.
   *
   * <ul>
   * <li>If arg type is "VARIABLE", then if the name starts with an '@' we check global variables
   * for a value, or else we check local variables for a value.</li>
   * <li>If arg type is not "VARIABLE" then we return the arg object as-is.</li>
   * </ul>
   */
  public List<Arg> resolveArgs(List<Arg> args, Variables localVariables) {
    return args.stream().map(arg -> {
      if (arg.getType() != ArgType.VARIABLE) {
        return arg;
      } else {
        String name = arg.getText();
        Object obj;
        if (name.startsWith("@")) {
          obj = globalVariables.getAsObject(name.substring(1));
        } else {
          obj = localVariables.getAsObject(name);
        }
        return Arg.resolveValue(obj);
      }
    }).toList();
  }

  protected String unquoteStringLiteral(Arg arg) {
    String value = arg.getText();
    if (arg.getType() == ArgType.STRING_LITERAL && value.startsWith("\"") && value.endsWith("\"")) {
      return value.substring(1, value.length() - 1);
    } else {
      throw new Game1RuntimeException(
          "Tried to unquote value of '%s' as a quoted string but it did not have double quotes.",
          arg);
    }
  }

  public Variables testing_getVariables() {
    return globalVariables;
  }
}
