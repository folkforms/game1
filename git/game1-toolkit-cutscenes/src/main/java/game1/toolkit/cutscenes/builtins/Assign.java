package game1.toolkit.cutscenes.builtins;

import java.util.List;

import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.variables.Variables;

public class Assign extends CutsceneFunction {

  public Assign(Variables globalVariables) {
    super(globalVariables);
  }

  @Override
  public String getName() {
    return "_assign";
  }

  @Override
  public boolean shouldResolveArgs() {
    return false;
  }

  @Override
  public Object execute(List<Arg> args, Variables localVariables) {
    String name = args.get(0).getText();
    if (name.startsWith("\"") && name.endsWith("\"")) {
      name = name.substring(1, name.length() - 1);
    }

    // Only resolve the value part, i.e. the 2nd argument
    Arg valueArg = resolveArgs(List.of(args.get(1)), localVariables).get(0);
    Object valueArgAsObject = valueArg.asObject();
    if (name.startsWith("@")) {
      globalVariables.set(name.substring(1), valueArgAsObject);
    } else {
      localVariables.set(name, valueArgAsObject);
    }

    return valueArg.asObject();
  }
}
