package game1.toolkit.cutscenes.builtins;

import java.util.List;

import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.variables.Variables;

public class GetGlobalVariable extends CutsceneFunction {

  public GetGlobalVariable(Variables globalVariables) {
    super(globalVariables);
  }

  @Override
  public String getName() {
    return "getGlobalVariable";
  }

  @Override
  public boolean shouldResolveArgs() {
    return false;
  }

  @Override
  public Object execute(List<Arg> args, Variables localVariables) {
    validateNumArgsIsExactly(args, 1);

    // arg0 is a quoted string because the cutscene has e.g. getGlobalVariable("foo.bar")
    String name = unquoteStringLiteral(args.get(0));
    return globalVariables.getAsObject(name);
  }
}
