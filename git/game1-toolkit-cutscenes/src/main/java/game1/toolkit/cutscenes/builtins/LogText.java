package game1.toolkit.cutscenes.builtins;

import java.util.List;

import folkforms.log.Log;
import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.cutscenes.v9c.antlr.statements.ArgType;
import game1.variables.Variables;

public class LogText extends CutsceneFunction {

  public LogText(Variables globalVariables) {
    super(globalVariables);
  }

  @Override
  public String getName() {
    return "log";
  }

  @Override
  public String execute(List<Arg> args, Variables localVariables) {
    if (args.size() == 0) {
      Log.info("");
      return null;
    }

    String format = toString(args.get(0));
    Object[] argsArray = new Object[args.size() - 1];
    for (int i = 1; i < args.size(); i++) {
      argsArray[i - 1] = args.get(i).getText();
    }
    Log.info(format, argsArray);
    return null;
  }

  private String toString(Arg arg) {
    if (arg.getType() == ArgType.STRING_LITERAL) {
      String text = arg.getText();
      return text.substring(1, text.length() - 1);
    } else {
      return arg.getText();
    }
  }
}
