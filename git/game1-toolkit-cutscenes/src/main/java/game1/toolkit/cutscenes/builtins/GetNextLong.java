package game1.toolkit.cutscenes.builtins;

import java.util.List;

import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.variables.Variables;

public class GetNextLong extends CutsceneFunction {

  private static long COUNTER = 0;

  public GetNextLong(Variables globalVariables) {
    super(globalVariables);
  }

  @Override
  public String getName() {
    return "_getNextLong";
  }

  @Override
  public Object execute(List<Arg> args, Variables localVariables) {
    validateNumArgsIsExactly(args, 0);
    return COUNTER++;
  }
}
