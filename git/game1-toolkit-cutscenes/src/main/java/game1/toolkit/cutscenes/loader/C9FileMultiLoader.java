package game1.toolkit.cutscenes.loader;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import game1.cutscenes.v9c.antlr.c9.C9LanguageFragment;
import game1.cutscenes.v9c.antlr.c9fileparser.C9FileParser;
import game1.datasets.MultiLoader;

public class C9FileMultiLoader extends MultiLoader {

  @Override
  public Map<String, Supplier<?>> load(String path) throws IOException {
    try {
      this.path = path;
      String input = fileSystem.loadString(path);
      List<C9LanguageFragment> fragments = new C9FileParser(input).parse();
      return Map.of(path, () -> fragments);
    } catch (Exception e) {
      throw new RuntimeException(String.format("Error loading file '%s'", path), e);
    }
  }
}
