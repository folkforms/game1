package game1.toolkit.cutscenes.builtins;

import java.util.List;

import game1.core.engine.Engine;
import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.variables.Variables;

public class MoveToState extends CutsceneFunction {

  public MoveToState(Variables globalVariables) {
    super(globalVariables);
  }

  @Override
  public String getName() {
    return "moveToState";
  }

  @Override
  public Object execute(List<Arg> args, Variables localVariables) {
    validateNumArgsIsExactly(args, 1);
    String nextState = unquoteStringLiteral(args.get(0));
    Engine.GAME_STATE.moveToState(nextState);
    return null;
  }
}
