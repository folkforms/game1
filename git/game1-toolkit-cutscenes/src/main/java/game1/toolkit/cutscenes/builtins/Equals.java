package game1.toolkit.cutscenes.builtins;

import java.util.List;

import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.cutscenes.v9c.antlr.statements.ArgType;
import game1.variables.Variables;

public class Equals extends CutsceneFunction {

  public Equals(Variables globalVariables) {
    super(globalVariables);
  }

  @Override
  public String getName() {
    return "_equals";
  }

  // FIXME C9: I should add comparing strings and maybe comparing things like float and string?
  @Override
  public Object execute(List<Arg> args, Variables localVariables) {
    validateNumArgsIsExactly(args, 2);

    Arg arg0 = args.get(0);
    Arg arg1 = args.get(1);

    if (arg0.getType() == ArgType.FLOAT_LITERAL && arg1.getType() == ArgType.FLOAT_LITERAL) {
      float float1 = args.get(0).asFloat();
      float float2 = args.get(1).asFloat();
      return float1 == float2;
    } else if (arg0.getType() == ArgType.BOOLEAN_LITERAL
        && arg1.getType() == ArgType.BOOLEAN_LITERAL) {
      boolean boolean1 = args.get(0).asBoolean();
      boolean boolean2 = args.get(1).asBoolean();
      return boolean1 == boolean2;
    }
    throw new RuntimeException(String.format("%s is not able to compare %s and %s",
        getClass().getSimpleName(), arg0, arg1));
  }
}
