package game1.toolkit.cutscenes.init;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import game1.core.engine.GameVariables;
import game1.toolkit.cutscenes.builtins.CutsceneFunction;
import game1.toolkit.cutscenes.player.BuiltInCutsceneFunctionsList;
import game1.variables.Variables;
import game1.variables.VariablesFactory;

public class Game1ToolkitCutscenesModule {

  public static void postEngineInit() throws IOException {
    addProperties();
    addBuiltInFunctions();
  }

  private static void addProperties() throws IOException {
    GameVariables g = (GameVariables) VariablesFactory.getInstance();
    g.apply("src/main/resources/properties/game1-toolkit-cutscenes.yaml");
  }

  private static void addBuiltInFunctions() {
    Variables variables = VariablesFactory.getInstance();
    BuiltInCutsceneFunctionsList.functions.forEach(function -> {
      try {
        CutsceneFunction builtInFunction = (CutsceneFunction) function
            .getConstructor(Variables.class).newInstance(variables);
        variables.put(builtInFunction.getName(), builtInFunction);
      } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
          | InvocationTargetException | NoSuchMethodException | SecurityException e) {
        throw new RuntimeException(e);
      }
    });
  }
}
