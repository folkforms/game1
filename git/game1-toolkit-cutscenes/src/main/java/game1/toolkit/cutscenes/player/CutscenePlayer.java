package game1.toolkit.cutscenes.player;

import java.util.List;
import java.util.Optional;
import java.util.Stack;

import folkforms.log.Log;
import game1.actors.Actor;
import game1.cutscenes.v9c.antlr.c9.C9LanguageFragment;
import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.cutscenes.v9c.antlr.statements.Function;
import game1.cutscenes.v9c.antlr.statements.Statement;
import game1.toolkit.cutscenes.builtins.CutsceneFunction;
import game1.variables.Variables;

/**
 * Executes a cutscene fragment one line at a time. Does not support the `wait` function.
 */
public class CutscenePlayer implements Actor {

  private Variables globalVariables;

  private Stack<ExecutionStackFragment> executionStack = new Stack<>();
  private ExecutionStackFragment currentExecutionStackFragment = null;

  public CutscenePlayer(C9LanguageFragment c9LanguageFragment, Variables variables) {
    executionStack.push(new ExecutionStackFragment(c9LanguageFragment, new Variables()));
    this.globalVariables = variables;
  }

  public Statement peek() {
    if (currentExecutionStackFragment != null && currentExecutionStackFragment.hasNext()) {
      return currentExecutionStackFragment.peek();
    } else if (executionStack.size() > 0) {
      ExecutionStackFragment nextStackObject = executionStack.peek();
      return nextStackObject.peek();
    } else {
      return null;
    }
  }

  public boolean hasNext() {
    return peek() != null;
  }

  public void playNextStatement() {
    if ((currentExecutionStackFragment == null && executionStack.size() > 0)
        || !currentExecutionStackFragment.hasNext()) {
      currentExecutionStackFragment = executionStack.pop();
    }

    if (currentExecutionStackFragment != null && currentExecutionStackFragment.hasNext()) {
      playStatement(currentExecutionStackFragment.next());
    }
  }

  private void playStatement(Statement statement) {
    if (statement instanceof Function) {
      executeFunction((Function) statement);
    } else {
      Log.warn("CutscenePlayer.playStatement: Don't know how to handle this: %s", statement);
      Log.warn("globalVariables = %s", globalVariables);
      Log.warn("localVariables = %s", currentExecutionStackFragment.getLocalVariables());
    }
  }

  private void executeFunction(Function functionStatement) {
    Object obj = globalVariables.getAsObject(functionStatement.getName());
    if (obj instanceof CutsceneFunction) {
      handleCutsceneFunction((CutsceneFunction) obj, functionStatement);
    } else if (obj instanceof C9LanguageFragment) {
      handleC9LanguageFragment((C9LanguageFragment) obj, functionStatement);
    }
  }

  private void handleCutsceneFunction(CutsceneFunction cutsceneFunction,
      Function functionStatement) {
    List<Arg> functionArgs = functionStatement.getArgs();
    functionArgs = cutsceneFunction.shouldResolveArgs() ? cutsceneFunction.resolveArgs(functionArgs,
        currentExecutionStackFragment.getLocalVariables())
      : functionArgs;
    Object prev = cutsceneFunction.execute(functionArgs,
        currentExecutionStackFragment.getLocalVariables());

    Optional<String> assignmentVariableMaybe = functionStatement.getAssignmentVariable();
    if (assignmentVariableMaybe.isPresent()) {
      currentExecutionStackFragment.getLocalVariables().set(assignmentVariableMaybe.get(), prev);
    }

    globalVariables.set("__prev", prev);
  }

  private void handleC9LanguageFragment(C9LanguageFragment c9LanguageFragment,
      Function functionStatement) {
    List<Arg> functionArgs = functionStatement.getArgs();
    Variables localVariables = new Variables();
    for (int i = 0; i < functionArgs.size(); i++) {
      localVariables.put(String.format("__localArg%s", i), functionArgs.get(i).asObject());
    }

    ExecutionStackFragment newESF = new ExecutionStackFragment(c9LanguageFragment, localVariables);
    executionStack.push(currentExecutionStackFragment);
    currentExecutionStackFragment = null;
    executionStack.push(newESF);
  }
}
