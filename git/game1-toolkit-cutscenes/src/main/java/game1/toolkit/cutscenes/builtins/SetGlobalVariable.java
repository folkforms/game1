package game1.toolkit.cutscenes.builtins;

import java.util.List;

import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.variables.Variables;

public class SetGlobalVariable extends CutsceneFunction {

  public SetGlobalVariable(Variables globalVariables) {
    super(globalVariables);
  }

  @Override
  public String getName() {
    return "setGlobalVariable";
  }

  @Override
  public boolean shouldResolveArgs() {
    return false;
  }

  @Override
  public Object execute(List<Arg> args, Variables localVariables) {
    validateNumArgsIsExactly(args, 2);

    // arg0 is a quoted string because the cutscene has e.g. setGlobalVariable("foo.bar", true)
    String name = unquoteStringLiteral(args.get(0));

    // Only resolve the value part, i.e. the 2nd argument
    Arg valueArg = resolveArgs(List.of(args.get(1)), localVariables).get(0);
    Object valueArgAsObject = valueArg.asObject();
    globalVariables.set(name, valueArgAsObject);

    return valueArg.asObject();
  }
}
