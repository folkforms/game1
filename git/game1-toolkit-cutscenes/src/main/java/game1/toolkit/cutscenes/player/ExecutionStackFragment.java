package game1.toolkit.cutscenes.player;

import java.util.List;

import folkforms.log.Log;
import game1.cutscenes.v9c.antlr.c9.C9LanguageFragment;
import game1.cutscenes.v9c.antlr.statements.Statement;
import game1.variables.Variables;

class ExecutionStackFragment {

  private int index = 0;
  private List<Statement> statements;
  private Variables localVariables;

  public ExecutionStackFragment(C9LanguageFragment c9LanguageFragment, Variables localVariables) {
    this.statements = c9LanguageFragment.getStatements();
    this.index = 0;
    this.localVariables = localVariables;
  }

  public boolean hasNext() {
    return index < statements.size();
  }

  public Statement next() {
    return statements.get(index++);
  }

  public Statement peek() {
    if (index == statements.size()) {
      return null;
    }
    return statements.get(index);
  }

  public Variables getLocalVariables() {
    return localVariables;
  }

  public void debug_printData() {
    Log.info("execution stack");
    statements.forEach(s -> Log.info("    statement: %s", s));
  }
}
