package game1.toolkit.cutscenes.player;

import java.util.List;

import game1.toolkit.cutscenes.builtins.Add;
import game1.toolkit.cutscenes.builtins.Assign;
import game1.toolkit.cutscenes.builtins.DebugPrintVariables;
import game1.toolkit.cutscenes.builtins.Divide;
import game1.toolkit.cutscenes.builtins.Equals;
import game1.toolkit.cutscenes.builtins.GetGlobalVariable;
import game1.toolkit.cutscenes.builtins.GetNextLong;
import game1.toolkit.cutscenes.builtins.Invoke;
import game1.toolkit.cutscenes.builtins.LogText;
import game1.toolkit.cutscenes.builtins.LogicalAnd;
import game1.toolkit.cutscenes.builtins.LogicalOr;
import game1.toolkit.cutscenes.builtins.MoveToState;
import game1.toolkit.cutscenes.builtins.Multiply;
import game1.toolkit.cutscenes.builtins.RaiseEvent;
import game1.toolkit.cutscenes.builtins.ResolveLocalVariables;
import game1.toolkit.cutscenes.builtins.SetGlobalVariable;
import game1.toolkit.cutscenes.builtins.Subtract;
import game1.toolkit.cutscenes.builtins.Wait;

/**
 * Built-in cutscene functions. They may depend on Variables.
 */
public class BuiltInCutsceneFunctionsList {
  public static List<Class<?>> functions = List.of( //
      Add.class, //
      Assign.class, //
      DebugPrintVariables.class, //
      Divide.class, //
      Equals.class, //
      GetGlobalVariable.class, //
      GetNextLong.class, //
      Invoke.class, //
      LogicalAnd.class, //
      LogicalOr.class, //
      LogText.class, //
      MoveToState.class, //
      Multiply.class, //
      RaiseEvent.class, //
      ResolveLocalVariables.class, //
      SetGlobalVariable.class, //
      Subtract.class, //
      Wait.class //
  );
}
