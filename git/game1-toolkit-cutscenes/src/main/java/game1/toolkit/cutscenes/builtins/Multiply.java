package game1.toolkit.cutscenes.builtins;

import java.util.List;

import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.variables.Variables;

public class Multiply extends CutsceneFunction {

  public Multiply(Variables globalVariables) {
    super(globalVariables);
  }

  @Override
  public String getName() {
    return "_multiply";
  }

  @Override
  public Object execute(List<Arg> args, Variables localVariables) {
    validateNumArgsIsExactly(args, 2);

    float float1 = args.get(0).asFloat();
    float float2 = args.get(1).asFloat();

    return float1 * float2;
  }
}
