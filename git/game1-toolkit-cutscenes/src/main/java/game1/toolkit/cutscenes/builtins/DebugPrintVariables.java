package game1.toolkit.cutscenes.builtins;

import java.util.List;

import folkforms.log.Log;
import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.variables.Variables;

public class DebugPrintVariables extends CutsceneFunction {

  public DebugPrintVariables(Variables globalVariables) {
    super(globalVariables);
  }

  @Override
  public String getName() {
    return "debugPrintVariables";
  }

  @Override
  public String execute(List<Arg> args, Variables localVariables) {
    Log.info("%s", getClass().getSimpleName());
    Log.info("  Global:");
    globalVariables.debug_listVariables().forEach(str -> Log.info("%s", str));
    Log.info("  Local:");
    localVariables.debug_listVariables().forEach(str -> Log.info("%s", str));
    return null;
  }
}
