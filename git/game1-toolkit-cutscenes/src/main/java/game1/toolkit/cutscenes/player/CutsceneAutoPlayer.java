package game1.toolkit.cutscenes.player;

import game1.actors.Actor;
import game1.actors.Tickable;
import game1.cutscenes.v9c.antlr.c9.C9LanguageFragment;
import game1.cutscenes.v9c.antlr.statements.Function;
import game1.cutscenes.v9c.antlr.statements.Statement;
import game1.variables.Variables;

/**
 * Plays as many cutscene lines as it can, until it hits a `wait` command, at which point it will
 * wait that many ticks.
 */
public class CutsceneAutoPlayer implements Actor, Tickable {

  private Variables globalVariables;
  private CutscenePlayer cutscenePlayer;

  /**
   * Plays a cutscene tag e.g. "#foo". The '#' is optional.
   */
  public CutsceneAutoPlayer(String cutsceneTag, Variables globalVariables) {
    if (cutsceneTag.startsWith("#")) {
      cutsceneTag = cutsceneTag.substring(1);
    }
    C9LanguageFragment c9LanguageFragment = globalVariables.get(cutsceneTag);
    cutscenePlayer = new CutscenePlayer(c9LanguageFragment, globalVariables);
    this.globalVariables = globalVariables;
  }

  @Override
  public void onTick() {
    // FIXME C9: We might be able to get away with checking the return value of the wait function
    // and setting a local variable. That would remove our dependency on Variables.
    int numWaitingTicks = globalVariables.get("game1.cutscenes.numWaitingTicks");
    if (numWaitingTicks > 0) {
      numWaitingTicks--;
      globalVariables.set("game1.cutscenes.numWaitingTicks", numWaitingTicks);
      return;
    }

    Statement nextStatement = null;
    Function nextFunction = null;
    boolean hasNextStatement = false;
    boolean nextStatementIsWaitFunction = false;
    do {
      nextStatement = cutscenePlayer.peek();
      hasNextStatement = nextStatement != null;
      if (hasNextStatement && nextStatement instanceof Function) {
        nextFunction = (Function) nextStatement;
        nextStatementIsWaitFunction = nextFunction.getName().equals("wait");
      }
      if (hasNextStatement) {
        cutscenePlayer.playNextStatement();
      }
    } while (hasNextStatement && !nextStatementIsWaitFunction);
  }
}
