package testgame.states;

import java.util.List;

import game1.core.engine.Engine;
import game1.core.engine.userstate.UserState;
import game1.core.graphics.Drawable;
import game1.core.input.MouseFactory;
import game1.resolutions.layers.ResolutionLayers;
import game1.toolkit.widgets.background.Background;
import testgame.rebindcontrols.TGRebindKeysMenu;

public class RebindControlsState extends UserState {

  @Override
  public List<String> listRequiredTags() {
    return List.of();
  }

  @Override
  public void apply() {
    Drawable initialBackground = Background.create(null);
    Engine.GAME_STATE.addActor(initialBackground, ResolutionLayers.WINDOW_LAYER);
    TGRebindKeysMenu rebindControlsMenu = new TGRebindKeysMenu();
    Engine.GAME_STATE.addActor(rebindControlsMenu, ResolutionLayers.WINDOW_LAYER);
    Engine.KEY_BINDINGS.clearAllActive();
    MouseFactory.getInstance().setVisible(true);
    Engine.GAME_PAUSED = true;
  }
}
