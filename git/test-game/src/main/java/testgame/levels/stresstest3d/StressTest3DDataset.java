package testgame.levels.stresstest3d;

import java.io.IOException;

import game1.core.pipelines.GraphicsPipelineName;
import game1.core.resources.loaders.DrawableLoader;
import game1.datasets.GameDataFactory;
import game1.filesystem.FileSystem;
import game1.filesystem.FileSystemFactory;

public class StressTest3DDataset {

  public static final String TAG = "StressTest3DDataset";
  public static final int NUM_INSTANCED_CUBES = 10000;
  private FileSystem fileSystem;

  public StressTest3DDataset() {
    fileSystem = FileSystemFactory.getInstance();
    loadCubeInstanced();
    // loadDiceInstanced(); // Use either this or loadCubeInstanced
    loadCubeNonInstanced();
  }

  private void loadCubeInstanced() {
    GameDataFactory.getInstance().addSupplier("test-3d-cube-instanced", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SCENE_3D).instances(NUM_INSTANCED_CUBES)
            .model("src/main/resources/stress_test_3d/stress_test_3d_cube/cube.obj")
            .texture("src/main/resources/stress_test_3d/stress_test_3d_cube/cube.png")
            .load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
  }

  private void loadCubeNonInstanced() {
    GameDataFactory.getInstance().addSupplier("test-3d-cube-non-instanced", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SCENE_3D).instances(-1)
            .model("src/main/resources/stress_test_3d/stress_test_3d_cube/cube.obj")
            .texture("src/main/resources/stress_test_3d/stress_test_3d_cube/cube.png")
            .load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
    // drawable.internal_getMeshes()[0].getMaterial()
    // .setAmbientColour(new Vector4f(0.8f, 0.3f, 0.3f, 1));
  }

  private void loadDiceInstanced() {
    GameDataFactory.getInstance().addSupplier("test-3d-cube-instanced", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SCENE_3D).instances(NUM_INSTANCED_CUBES)
            .model("src/main/resources/level3d/dice/dice.obj").sameTexturesDir().load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
  }
}
