package testgame.levels.stresstest3d;

import java.util.List;

import org.joml.Vector3f;

import game1.core.engine.Engine;
import game1.core.engine.userstate.UserState;
import game1.core.engine.userstate.UserStateHelper;
import game1.core.graphics.Drawable;
import game1.core.input.Mouse;
import game1.datasets.GameDataFactory;
import game1.toolkit.actors.Player3D;

public class StressTest3DState extends UserState {

  @Override
  public List<String> listRequiredTags() {
    return List.of(StressTest3DDataset.TAG);
  }

  @Override
  public void apply() {
    UserStateHelper.setMouse(Mouse.MOUSE_CAMERA);
    UserStateHelper.setKeyBindings("Level3DKeyBindings", "CommonKeyBindings");
    UserStateHelper.setDirectionalLight(new Vector3f(0, 1, 0));

    Player3D player = new Player3D(-25, 18, 31, 32, 41, 0);
    Engine.GAME_STATE.add3dActor(player);

    addRandomCubesInstanced();
    addRandomCubesNonInstanced();
  }

  private void addRandomCubesInstanced() {
    int numCubes = StressTest3DDataset.NUM_INSTANCED_CUBES;
    int delta = 30;
    Drawable drawableItem = GameDataFactory.getInstance().get("test-3d-cube-instanced");
    for (int i = 0; i < numCubes; i++) {
      Drawable drawableItemClone = drawableItem.getClone();
      drawableItemClone.setPosition(getRandom(delta), getRandom(delta), getRandom(delta));
      drawableItemClone.setScale(0.5f);
      drawableItemClone.setName(String.format("random-cube-instanced-%s", i));
      Engine.GAME_STATE.add3dActor(drawableItemClone);
    }
  }

  private float getRandom(int scale) {
    return (float) (Math.random() * scale - scale / 2);
  }

  private void addRandomCubesNonInstanced() {
    int numCubes = 15;
    int delta = 30;
    Drawable drawableItem = GameDataFactory.getInstance().get("test-3d-cube-non-instanced");
    for (int i = 0; i < numCubes; i++) {
      Drawable drawableItemClone = drawableItem.getClone();
      drawableItemClone.setPosition(getRandom(delta), getRandom(delta), getRandom(delta));
      drawableItemClone.setScale(0.5f);
      drawableItemClone.setName(String.format("random-cube-non-instanced-%s", i));
      Engine.GAME_STATE.add3dActor(drawableItemClone);
    }
  }
}
