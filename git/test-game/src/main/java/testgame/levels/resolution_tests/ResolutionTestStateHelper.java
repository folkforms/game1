package testgame.levels.resolution_tests;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector4f;

import folkforms.log.Log;
import game1.core.engine.viewport.MouseControlledSceneViewport;
import game1.resolutions.layers.ResolutionLayer;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.scaling.ScalingStrategies;
import game1.variables.VariablesFactory;

public class ResolutionTestStateHelper {

  public static void createResolutionLayers(Vector2i resolution, Vector2i levelSize,
      String viewportControlledLayerName, String staticLayerName) {
    int scalingStrategy = VariablesFactory.getInstance().get("resolution_test.scaling_strategy");
    Log.info("Using scaling strategy %s", scalingStrategy);
    ResolutionLayer viewportControlledLayer = ResolutionLayersFactory.getInstance().createLayer(
        viewportControlledLayerName, resolution, levelSize, ScalingStrategies.get(scalingStrategy),
        null);
    MouseControlledSceneViewport viewport = new MouseControlledSceneViewport(
        viewportControlledLayerName, new Vector2f(200, 0), new Vector4f(5, 5, 5, 5), 1.5f);
    viewportControlledLayer.setViewport(viewport);
    ResolutionLayersFactory.getInstance().createStaticLayer(staticLayerName, resolution,
        ScalingStrategies.get(scalingStrategy));
  }
}
