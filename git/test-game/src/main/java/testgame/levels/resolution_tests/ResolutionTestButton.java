package testgame.levels.resolution_tests;

import folkforms.log.Log;
import game1.toolkit.widgets.button.Button;

public class ResolutionTestButton extends Button {

  public ResolutionTestButton(String text, int x, int y, int z) {
    super(text, 3f, x, y, z, 50, 50);
    setCommand(() -> Log.temp(text));
  }
}
