package testgame.levels.resolution_tests;

import java.io.IOException;

import game1.core.engine.Engine;

public class ResolutionTest1Dataset {

  public static final String TAG = "ResolutionTest1Dataset";

  public ResolutionTest1Dataset() throws IOException {

    Engine.GAME_FILES.register("src/main/resources/packed/test-game-resolution-tests.spritesheet",
        TAG);
  }
}
