package testgame.levels.resolution_tests;

import org.joml.Vector2f;
import org.joml.Vector3f;

import folkforms.maths.Graph;
import game1.actors.Actor;
import game1.actors.Tickable;
import game1.core.graphics.Animation;
import game1.datasets.GameDataFactory;

public class TempHenchard1 implements Actor, Tickable {

  private Animation henchardWalking1;
  private Animation henchardWalking2;
  private Vector2f startPos = new Vector2f(85, 32);
  private Vector2f midPos = new Vector2f(248, 127);
  private Vector2f endPos = new Vector2f(402, 32);
  private int stage = 0;
  private long tickNum = 0;
  private long numTicksBeforeChange = 120;

  public TempHenchard1() {
    henchardWalking1 = GameDataFactory.getInstance().get("coatless_henchard_walk.png");
    henchardWalking1.setVisible(true);
    henchardWalking1.setPosition(startPos.x, startPos.y, 10_010);
    henchardWalking1.setScale(3);
    addChild(henchardWalking1);
    henchardWalking2 = GameDataFactory.getInstance().get("coatless_henchard_walk.png-mirrored");
    henchardWalking2.setVisible(false);
    henchardWalking2.setPosition(startPos.x, startPos.y, 10_010);
    henchardWalking2.setScale(3);
    addChild(henchardWalking2);
  }

  @Override
  public void onTick() {
    tickNum++;
    if (tickNum % numTicksBeforeChange == 0) {
      tickNum = 0;
      stage++;
      if (stage > 3) {
        stage = 0;
      }
    }

    if (stage == 0) {
      henchardWalking1.setVisible(true);
      henchardWalking2.setVisible(false);
      calculatePos(startPos, midPos);
    } else if (stage == 1) {
      calculatePos(midPos, endPos);
    } else if (stage == 2) {
      henchardWalking1.setVisible(false);
      henchardWalking2.setVisible(true);
      calculatePos(endPos, midPos);
    } else if (stage == 3) {
      calculatePos(midPos, startPos);
    }
    adjustScale();
  }

  private void adjustScale() {
    float scaleMin = 2;
    float scaleMax = 4;
    Vector3f charPos = henchardWalking1.getPosition();
    float newScale = Graph.calculate(Graph.LINEAR, new float[] { scaleMax, scaleMin },
        (long) (charPos.y - startPos.y), (long) (midPos.y - startPos.y));
    henchardWalking1.setScale(newScale);
    henchardWalking2.setScale(newScale);
  }

  private void calculatePos(Vector2f v1, Vector2f v2) {
    float xPos = Graph.calculate(Graph.LINEAR, new float[] { v1.x, v2.x }, tickNum,
        numTicksBeforeChange);
    float yPos = Graph.calculate(Graph.LINEAR, new float[] { v1.y, v2.y }, tickNum,
        numTicksBeforeChange);
    henchardWalking1.setPosition(xPos, yPos);
    henchardWalking2.setPosition(xPos, yPos);
  }
}
