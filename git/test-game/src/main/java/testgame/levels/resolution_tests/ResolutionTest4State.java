package testgame.levels.resolution_tests;

import java.util.List;

import org.joml.Vector2i;

import game1.core.devtools.DebugToolsMask;
import game1.core.engine.Engine;
import game1.core.engine.userstate.UserState;
import game1.core.engine.userstate.UserStateHelper;
import game1.core.graphics.Image;
import game1.datasets.GameDataFactory;
import game1.toolkit.debugtools.DebugMouse;
import game1.toolkit.debugtools.DebugResolutionHints;
import game1.toolkit.debugtools.DebugViewport;

public class ResolutionTest4State extends UserState {

  @Override
  public List<String> listRequiredTags() {
    return List.of(ResolutionTest4Dataset.TAG);
  }

  @Override
  public void apply() {
    String viewportControlledLayer = "viewport-controlled-layer";
    String staticLayer = "static-layer";
    ResolutionTestStateHelper.createResolutionLayers(new Vector2i(569, 320), new Vector2i(569, 320),
        viewportControlledLayer, staticLayer);

    UserStateHelper.setKeyBindings("CommonKeyBindings");

    Image background = GameDataFactory.getInstance().get("resolution_test_4_background.png");
    Engine.GAME_STATE.addActor(background, viewportControlledLayer);

    ResolutionTestButton button1 = new ResolutionTestButton("01", 100, 50, 10_000);
    Engine.GAME_STATE.addActor(button1, viewportControlledLayer);
    ResolutionTestFloatingButton button2 = new ResolutionTestFloatingButton("02", 100, 110, 10_020);
    Engine.GAME_STATE.addActor(button2, staticLayer);

    Engine.GAME_STATE.addActor(new TempHenchard1(), viewportControlledLayer);

    Engine.GAME_STATE.addActor(new DebugToolsMask(), viewportControlledLayer);
    Engine.GAME_STATE.addActor(new DebugMouse(15, 1050), viewportControlledLayer);
    Engine.GAME_STATE.addActor(new DebugResolutionHints(15, 990), viewportControlledLayer);
    Engine.GAME_STATE.addActor(new DebugViewport(15, 1020), viewportControlledLayer);
  }
}
