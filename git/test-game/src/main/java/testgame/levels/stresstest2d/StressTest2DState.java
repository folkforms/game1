package testgame.levels.stresstest2d;

import java.util.List;

import org.joml.Vector2i;

import game1.core.engine.Engine;
import game1.core.engine.userstate.UserState;
import game1.core.engine.userstate.UserStateHelper;
import game1.core.input.Mouse;
import game1.datasets.GameDataFactory;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.scaling.ScalingStrategies;
import testgame.actors.stresstest.StressTestBalls;

public class StressTest2DState extends UserState {

  public static final String LAYER_NAME = "stress-test-2d-layer";

  @Override
  public List<String> listRequiredTags() {
    return List.of(StressTest2DDataset.TAG);
  }

  @Override
  public void apply() {

    ResolutionLayersFactory.getInstance().createStaticLayer(LAYER_NAME, new Vector2i(1920, 1080),
        ScalingStrategies.get(0));

    UserStateHelper.setMouse(Mouse.MOUSE_CURSOR);
    UserStateHelper.setKeyBindings("CommonKeyBindings");
    Engine.GAME_STATE.addActor(GameDataFactory.getInstance().get("stress-test-background"),
        LAYER_NAME);
    Engine.GAME_STATE.addActor(new StressTestBalls(LAYER_NAME), LAYER_NAME);
  }
}
