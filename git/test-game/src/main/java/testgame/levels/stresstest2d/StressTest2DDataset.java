package testgame.levels.stresstest2d;

import game1.core.pipelines.GraphicsPipelineName;
import game1.core.resources.builders.PrimitiveBuilder;
import game1.datasets.GameDataFactory;
import game1.toolkit.widgets.background.Background;

public class StressTest2DDataset {

  public static final String TAG = "StressTest2DDataset";

  public StressTest2DDataset() {
    GameDataFactory.getInstance().addSupplier("stress-test-background",
        () -> Background.create(StressTest2DState.LAYER_NAME, 0, 0, 0.2f));
    GameDataFactory.getInstance().addSupplier("stress-test-ball", () -> PrimitiveBuilder
        .init(GraphicsPipelineName.SCENE_2D).rect2d(3, 3).colour(0, 1, 0).build());
  }
}
