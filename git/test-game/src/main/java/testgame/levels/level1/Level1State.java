package testgame.levels.level1;

import java.util.List;

import org.joml.Vector2i;

import game1.core.engine.Engine;
import game1.core.engine.userstate.UserState;
import game1.core.engine.userstate.UserStateHelper;
import game1.core.graphics.Drawable;
import game1.datasets.GameDataFactory;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.scaling.ScalingStrategies;

public class Level1State extends UserState {

  @Override
  public List<String> listRequiredTags() {
    return List.of(Level1Dataset.TAG);
  }

  @Override
  public void apply() {
    String resolutionLayerName = "level-1-loading-state-layer";
    ResolutionLayersFactory.getInstance().createStaticLayer(resolutionLayerName,
        new Vector2i(1920, 1080), ScalingStrategies.get(0));

    UserStateHelper.setKeyBindings("CommonKeyBindings");

    Drawable drawable1 = GameDataFactory.getInstance().get("abcd.png");
    drawable1.setPosition(100, 1000);
    Engine.GAME_STATE.addActor(drawable1, resolutionLayerName);

    Drawable drawable2 = GameDataFactory.getInstance().get("abcd.png-mirrored");
    drawable2.setPosition(200, 900);
    Engine.GAME_STATE.addActor(drawable2, resolutionLayerName);
  }
}
