package testgame.levels.level1;

import java.io.IOException;
import java.util.Map;
import java.util.function.Supplier;

import game1.datasets.MultiLoader;

public class DummyFileMultiLoader extends MultiLoader {

  private static int count = 0;

  @Override
  public Map<String, Supplier<?>> load(String path) throws IOException {
    try {
      Thread.sleep(500);
    } catch (InterruptedException ex) {
      throw new RuntimeException(ex);
    }
    String ref = "dummy-" + count++;
    return Map.of(ref, () -> ref);
  }
}
