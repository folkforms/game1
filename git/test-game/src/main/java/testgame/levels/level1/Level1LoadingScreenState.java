package testgame.levels.level1;

import java.util.List;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import game1.core.engine.Engine;
import game1.core.states.LoadingScreenState2;
import game1.datasets.GameDataFactory;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.scaling.ScalingStrategies;
import game1.toolkit.redoctopus.loadingscreen.LoadingScreenUpdater;
import game1.toolkit.widgets.background.Background;
import game1.toolkit.widgets.label.Label;
import game1.toolkit.widgets.label.LabelBuilder;
import game1.toolkit.widgets.progress.ProgressBar;
import game1.toolkit.widgets.progress.ProgressBarBuilder;
import testgame.main.TGStates;

public class Level1LoadingScreenState extends LoadingScreenState2 {

  public Level1LoadingScreenState() {
    super(List.of("Level1Dataset"), TGStates.LEVEL_1);
  }

  @Override
  public List<String> listRequiredTags() {
    return List.of(Level1LoadingScreenDataset.TAG);
  }

  @Override
  public void apply() {
    String resolutionLayerName = "level-1-loading-state-layer";
    ResolutionLayersFactory.getInstance().createStaticLayer(resolutionLayerName,
        new Vector2i(1920, 1080), ScalingStrategies.get(0));

    Engine.GAME_STATE.addActor(Background.create(null, 0.65f, 0.9f, 0.2f, 0.2f),
        resolutionLayerName);
    Engine.GAME_STATE.addActor(GameDataFactory.getInstance().get("abcd.png-mirrored"),
        resolutionLayerName);

    Label loadingInfoLabel = LabelBuilder.init("Level1LoadingScreenState info")
        .setPosition(100, 750, 0).setFontSize(8).setModifiable(true).build();
    Engine.GAME_STATE.addActor(loadingInfoLabel, resolutionLayerName);

    ProgressBar progressBar = ProgressBarBuilder.init().position(new Vector3f(460, 500, 1000))
        .size(new Vector2f(1000, 50)).build();
    Engine.GAME_STATE.addActor(progressBar, resolutionLayerName);

    Engine.GAME_STATE.addActor(
        new LoadingScreenUpdater(loadingInfoLabel, progressBar, tagsToLoad, listRequiredTags()),
        resolutionLayerName);
  }
}
