package testgame.levels.level1;

import java.io.IOException;

import game1.core.engine.Engine;

public class Level1Dataset {

  public static final String TAG = "Level1Dataset";

  public Level1Dataset() throws IOException {
    Engine.GAME_FILES.addMultiLoader("dummy", DummyFileMultiLoader.class);
    Engine.GAME_FILES.register("src/main/resources/packed/test-game-level-3d.spritesheet", TAG);
    Engine.GAME_FILES.register("src/main/resources/level1/1.dummy", TAG);
    Engine.GAME_FILES.register("src/main/resources/level1/2.dummy", TAG);
    Engine.GAME_FILES.register("src/main/resources/level1/3.dummy", TAG);
    Engine.GAME_FILES.register("src/main/resources/level1/4.dummy", TAG);
    Engine.GAME_FILES.register("src/main/resources/level1/5.dummy", TAG);
    Engine.GAME_FILES.register("src/main/resources/level1/6.dummy", TAG);
    Engine.GAME_FILES.register("src/main/resources/level1/7.dummy", TAG);
    Engine.GAME_FILES.register("src/main/resources/level1/8.dummy", TAG);
  }
}
