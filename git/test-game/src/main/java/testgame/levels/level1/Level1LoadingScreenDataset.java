package testgame.levels.level1;

import java.io.IOException;

import game1.core.engine.Engine;

public class Level1LoadingScreenDataset {

  public static final String TAG = "Level1LoadingScreenDataset";

  public Level1LoadingScreenDataset() throws IOException {
    Engine.GAME_FILES.register("src/main/resources/packed/test-game-level-3d.spritesheet", TAG);
  }
}
