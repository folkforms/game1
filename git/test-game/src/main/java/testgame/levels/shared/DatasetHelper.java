package testgame.levels.shared;

import game1.core.graphics.Colour;
import game1.core.pipelines.GraphicsPipelineName;
import game1.core.resources.loaders.DrawableLoader;
import game1.core.resources.loaders.Loader;

public class DatasetHelper {
  /**
   * Creates a flat plane with the given {@code x,y,z} coordinates and {@code colour}, and adds it
   * to TempActors with the given {@code ref}.
   *
   * @param x
   *          x coordinate
   * @param y
   *          y coordinate
   * @param z
   *          z coordinate
   * @param colour
   *          colour
   */
  public static Loader createFloor(int x, int y, int z, String colour) {
    return createFloor(x, y, z, 0, 0, 0, 10, colour);
  }

  /**
   * Creates a flat plane with the given {@code x,y,z} coordinates and {@code colour}, rotated with
   * {@code xDegrees yDegrees zDegrees}, scaled as {@code scale}, and adds it to TempActors with the
   * given {@code ref}.
   *
   * @param x
   *          x coordinate
   * @param y
   *          y coordinate
   * @param z
   *          z coordinate
   * @param xDegrees
   *          x axis rotation
   * @param yDegrees
   *          y axis rotation
   * @param zDegrees
   *          z axis rotation
   * @param scale
   *          scale
   * @param colour
   *          colour
   */
  private static Loader createFloor(int x, int y, int z, int xDegrees, int yDegrees, int zDegrees,
      float scale, String colour) {
    return DrawableLoader.init(GraphicsPipelineName.SCENE_3D)
        .model("src/main/resources/level3d/plane/plane_1x1_flat.obj").colour(Colour.fromHex(colour))
        .position(x, y, z).scale(scale).rotation(xDegrees, yDegrees, zDegrees);
  }
}
