package testgame.levels.shared;

import game1.core.infrastructure.lwjgl3.internal.LWJGL3Window;
import game1.core.input.KeyBindings;
import game1.core.input.KeyCommand;
import game1.core.input.Keys;
import game1.events.EventSubscriptionsFactory;
import game1.events.ReceiveEvent;
import game1.toolkit.commands.MoveToStateCommand;
import testgame.main.TGStates;

public class CommonKeyBindings extends KeyBindings {

  public CommonKeyBindings() {
    EventSubscriptionsFactory.getInstance().subscribe(this);
    register(KeyCommand.init().keys(Keys.KEY_LEFT_SHIFT, Keys.KEY_ESCAPE).event("close_window"));
    register(KeyCommand.init().keys(Keys.KEY_ESCAPE)
        .command(new MoveToStateCommand(TGStates.MAIN_MENU)));
  }

  @ReceiveEvent("close_window")
  public void closeWindow() {
    LWJGL3Window.close();
  }
}
