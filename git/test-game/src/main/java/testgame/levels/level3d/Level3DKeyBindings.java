package testgame.levels.level3d;

import game1.core.engine.Engine;
import game1.core.engine.internal.CameraFactory;
import game1.core.input.KeyBindings;
import game1.core.input.KeyCommand;
import game1.core.input.Keys;
import game1.events.Command;
import game1.events.EventSubscriptionsFactory;
import game1.events.Game1Events;
import game1.events.ReceiveEvent;
import game1.toolkit.events.ToolkitEvents;

public class Level3DKeyBindings extends KeyBindings {

  private float movementSpeed = 0.05f;
  private int fastMovementMultiplier = 5;
  private float rotationSpeed = 0.4f;

  private int currentAnimation = 0;

  public Level3DKeyBindings() {
    EventSubscriptionsFactory.getInstance().subscribe(this);

    /*
     * Note: You can use camera.setPosition, e.g. "Vector3f v = DrawThread.camera.getPosition();
     * DrawThread.camera.setPosition(v.x, v.y, v.z - cameraSpeed);" to rotate the player's head
     * without rotating the body.
     */

    register(KeyCommand.init().keys(Keys.KEY_W).noBlock().event(ToolkitEvents.PLAYER_MOVE, 0, 0,
        -movementSpeed));
    register(KeyCommand.init().keys(Keys.KEY_S).noBlock().event(ToolkitEvents.PLAYER_MOVE, 0, 0,
        movementSpeed));
    register(KeyCommand.init().keys(Keys.KEY_A).noBlock().event(ToolkitEvents.PLAYER_MOVE,
        -movementSpeed, 0, 0));
    register(KeyCommand.init().keys(Keys.KEY_D).noBlock().event(ToolkitEvents.PLAYER_MOVE,
        movementSpeed, 0, 0));
    register(KeyCommand.init().keys(Keys.KEY_SPACE).noBlock().event(ToolkitEvents.PLAYER_MOVE, 0,
        movementSpeed, 0));
    register(KeyCommand.init().keys(Keys.KEY_C).noBlock().event(ToolkitEvents.PLAYER_MOVE, 0,
        -movementSpeed, 0));

    register(KeyCommand.init().keys(Keys.KEY_LEFT_SHIFT, Keys.KEY_W).noBlock()
        .event(ToolkitEvents.PLAYER_MOVE, 0, 0, -movementSpeed * fastMovementMultiplier));
    register(KeyCommand.init().keys(Keys.KEY_LEFT_SHIFT, Keys.KEY_S).noBlock()
        .event(ToolkitEvents.PLAYER_MOVE, 0, 0, movementSpeed * fastMovementMultiplier));
    register(KeyCommand.init().keys(Keys.KEY_LEFT_SHIFT, Keys.KEY_A).noBlock()
        .event(ToolkitEvents.PLAYER_MOVE, -movementSpeed * fastMovementMultiplier, 0, 0));
    register(KeyCommand.init().keys(Keys.KEY_LEFT_SHIFT, Keys.KEY_D).noBlock()
        .event(ToolkitEvents.PLAYER_MOVE, movementSpeed * fastMovementMultiplier, 0, 0));
    register(KeyCommand.init().keys(Keys.KEY_LEFT_SHIFT, Keys.KEY_SPACE).noBlock()
        .event("player.move", 0, movementSpeed * fastMovementMultiplier, 0));
    register(KeyCommand.init().keys(Keys.KEY_LEFT_SHIFT, Keys.KEY_C).noBlock()
        .event(ToolkitEvents.PLAYER_MOVE, 0, -movementSpeed * fastMovementMultiplier, 0));

    register(KeyCommand.init().keys(Keys.KEY_Q).noBlock().event(Game1Events.MOUSE_MOVE, 0,
        -rotationSpeed, 0));
    register(KeyCommand.init().keys(Keys.KEY_E).noBlock().event(Game1Events.MOUSE_MOVE, 0,
        rotationSpeed, 0));

    // Select item under crosshairs
    register(KeyCommand.init().command(new Command() {
      @Override
      public void execute() {
        CameraFactory.getInstance().selectDrawable();
      }
    }).keys(Keys.KEY_P));

    // Play a sound
    register(KeyCommand.init().command(new Command() {
      @Override
      public void execute() {
        Engine.SOUND_MANAGER.playSoundSource("BEEP");
      }
    }).keys(Keys.KEY_N));

    // Resize a 2D mesh. Does not block, so is used to check that calling the OpenGL code at this
    // particular point does not crash the game (it calls it on the main thread.)
    register(KeyCommand.init().command(new Command() {
      @Override
      public void execute() {
        int newSize = (int) (Math.random() * 100);
        Level3DState.TEST_2D_RESIZE.resize(newSize, newSize);
      }
    }).noBlock().keys(Keys.KEY_V));

    // Animation
    register(
        KeyCommand.init().keys(Keys.KEY_LEFT_SHIFT, Keys.KEY_M).block().event("change.animation"));
  }

  @ReceiveEvent("change.animation")
  public void changeAnimation() {
    String[] animations = { //
        "000|000|000|Casual1|000|Casual1", //
        "000|000|000|Casual2|000|Casual2", //
        "000|000|000|Death|000|Death", //
        "000|000|000|FigthStanding|000|FigthStanding", //
        "000|000|000|Injured1|000|Injured1", //
        "000|000|000|Injured2|000|Injured2", //
        "000|000|000|NormalAttack1|000|NormalAttack1", //
        "000|000|000|Run|000|Run", //
        "000|000|000|SkillAttack1|000|SkillAttack1", //
        "000|000|000|SkillAttack2|000|SkillAttack2", //
        "000|000|000|SkillAttack3|000|SkillAttack3", //
        "000|000|000|Standing|000|Standing", //
        "000|000|000|Walk|000|Walk", //
    };
    int next = currentAnimation + 1;
    if (next >= animations.length) {
      next = 0;
    }
    currentAnimation = next;
    Level3DState.ANIMATED_3D_MODEL.setCurrentAnimation(animations[currentAnimation]);
  }
}
