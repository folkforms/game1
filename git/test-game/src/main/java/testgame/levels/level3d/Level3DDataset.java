package testgame.levels.level3d;

import java.io.IOException;

import org.joml.Vector3f;

import game1.core.engine.Engine;
import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.graphics.Image;
import game1.core.graphics.lights.PointLight;
import game1.core.graphics.lights.Spotlight;
import game1.core.pipelines.GraphicsPipeline;
import game1.core.pipelines.GraphicsPipelineName;
import game1.core.resources.loaders.AssimpLoader;
import game1.core.resources.loaders.DrawableLoader;
import game1.core.sound2.SoundBuffer;
import game1.core.sound2.SoundListener;
import game1.core.sound2.SoundSource;
import game1.datasets.GameDataFactory;
import game1.filesystem.FileSystem;
import game1.filesystem.FileSystemFactory;
import game1.toolkit.lights.TestPointLight;
import game1.toolkit.lights.TestSpotlight;
import game1.toolkit.lod.LodBoundary;
import game1.toolkit.particles.BurningTorch;
import game1.toolkit.particles.Rain;
import testgame.levels.shared.DatasetHelper;

public class Level3DDataset {

  public static final String TAG = "Level3DDataset";
  private FileSystem fileSystem;

  public Level3DDataset() throws IOException {
    FileSystem fileSystem = FileSystemFactory.getInstance();
    Engine.GAME_FILES.register("src/main/resources/packed/test-game-level-3d.spritesheet", TAG);

    loadSkybox();
    loadNormalMappingTest();
    loadFloor();
    loadDebugCubes();
    load3D();
    load3DWireframe();
    load2DDrawableResizeTest();
    load3DAnimation();
    // load3DAnimationMultipleAnimations();
    loadBillboard();
    loadLights();
    loadTransparencyTest();
    loadBurningTorch();
    loadRain();
    loadLODTest();

    // loadPlane();
    // loadAndCreateSounds();
    // loadLime();
    // loadPlayerModel();
  }

  /**
   * Loads a textured skybox to be rendered by {@link GraphicsPipeline#SKYBOX}.
   */
  private void loadSkybox() {
    GameDataFactory.getInstance().addSupplier("test-3d-skybox", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SKYBOX)
            .model("src/main/resources/level3d/skybox/skybox.obj")
            .texture("src/main/resources/level3d/skybox/skybox.png").scale(200).load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
  }

  /**
   * Loads two flat planes with textures, the second of which has a normal mapping.
   */
  private void loadNormalMappingTest() {
    GameDataFactory.getInstance().addSupplier("test-3d-normal-mapping-1", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SCENE_3D)
            .model("src/main/resources/level3d/normal_mapping_test/plane_4x3_vertical.obj")
            .texture("src/main/resources/level3d/normal_mapping_test/rock_texture.png")
            .position(10, 0, -5).load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
    GameDataFactory.getInstance().addSupplier("test-3d-normal-mapping-2", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SCENE_3D)
            .model("src/main/resources/level3d/normal_mapping_test/plane_4x3_vertical.obj")
            .texture("src/main/resources/level3d/normal_mapping_test/rock_texture.png")
            .normalMap("src/main/resources/level3d/normal_mapping_test/rock_normal_map.png")
            .position(15, 0, -5).load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
  }

  /**
   * Loads some flat planes to represent the floor.
   */
  private void loadFloor() {
    int y = -3;
    GameDataFactory.getInstance().addSupplier("test-3d-floor-1", () -> {
      try {
        return DatasetHelper.createFloor(-10, y, 10, "#333").load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });

    GameDataFactory.getInstance().addSupplier("test-3d-floor-2", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SCENE_3D)
            .model("src/main/resources/level3d/plane/plane_1x1_flat.obj")
            .texture("src/main/resources/level3d/plane/cobblestone_diff.png")
            .normalMap("src/main/resources/level3d/plane/cobblestone_normal.png")
            .position(10, y, 10).scale(10).load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });

    GameDataFactory.getInstance().addSupplier("test-3d-floor-3", () -> {
      try {
        return DatasetHelper.createFloor(-30, y, 10, "#334").load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
    GameDataFactory.getInstance().addSupplier("test-3d-floor-4", () -> {
      try {
        return DatasetHelper.createFloor(-30, y, 30, "#343").load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
    GameDataFactory.getInstance().addSupplier("test-3d-floor-5", () -> {
      try {
        return DatasetHelper.createFloor(-10, y, 30, "#344").load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
    GameDataFactory.getInstance().addSupplier("test-3d-floor-6", () -> {
      try {
        return DatasetHelper.createFloor(10, y, 30, "#433").load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
  }

  /**
   * Loads a debug cube.
   */
  private void loadDebugCubes() {
    String cubeMeshFilename = "resources/cube/cube.obj";
    GameDataFactory.getInstance().addSupplier("debug-cube", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SCENE_3D).model(cubeMeshFilename)
            .sameTexturesDir().instances(10).load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
  }

  /**
   * Loads a textured 3D Drawable.
   */
  private void load3D() {
    GameDataFactory.getInstance().addSupplier("test-pipelines-3d", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SCENE_3D)
            .model("src/main/resources/level3d/dice/dice.obj").sameTexturesDir().position(-5, 0, -5)
            .name("Dice 3D").load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
  }

  /**
   * Loads a 3D Drawable but uses {@link GraphicsPipeline#WIREFRAME} to draw it, so it is rendered
   * as wireframe.
   */
  private void load3DWireframe() {
    GameDataFactory.getInstance().addSupplier("test-pipelines-3d-wireframe", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.WIREFRAME)
            .model("src/main/resources/level3d/dice/dice.obj").sameTexturesDir()
            .position(-10, 0, -5).name("Dice 3D").load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
  }

  /**
   * Loads a textured 2D Drawable.
   */
  private void load2DDrawableResizeTest() {
    GameDataFactory.getInstance().addSupplier("test-2d-resize", () -> {
      Image image = GameDataFactory.getInstance().get("fancy.png");
      image.setPosition(1000, 20);
      image.setScale(2);
      return image;
    });
  }

  /**
   * Loads a textured and animated 3D Drawable.
   */
  private void load3DAnimation() {
    GameDataFactory.getInstance().addSupplier("test-pipelines-3d-animated", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SCENE_3D)
            .model("src/main/resources/level3d/bob/boblamp.md5mesh").sameTexturesDir().instances(1)
            .scale(0.05f).position(5, 0, -5).animated(true).load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
    // FIXME DATASETS: Work on collision meshes
    // Mesh collisionMesh = MeshUtils.createCuboid(30, 65, 30, Colour.fromHex("#0f05"), -15, 0,
    // -15);
    // drawable.setCollisionMesh(collisionMesh);
  }

  /**
   * Loads a textured and animated 3D Drawable that has multiple animations.
   */
  private void load3DAnimationMultipleAnimations() {
    GameDataFactory.getInstance().addSupplier("test-pipelines-3d-animated-multiple", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SCENE_3D)
            .model("resources/ignore/goblin/goblin.fbx").sameTexturesDir().position(10, 0, 5)
            .scale(0.05f).name("test-pipelines-3d-animated-multiple").animated(true)
            .load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
  }

  /**
   * Loads an example object rendered by {@link GraphicsPipeline#BILLBOARD}.
   */
  private void loadBillboard() {
    GameDataFactory.getInstance().addSupplier("billboard", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.BILLBOARD)
            .model("src/main/resources/level3d/billboard/plane_1x1_vertical.obj")
            .texture("src/main/resources/level3d/billboard/billboard.png").position(-15, 0, -5)
            .rotation(90, 0, 0).name("billboard").load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
  }

  /**
   * Loads a point light and a spotlight.
   */
  private void loadLights() {
    // Point light
    String sphereFilename = "resources/basic_shapes/icosphere.obj";
    GameDataFactory.getInstance().addSupplier("test-point-light-drawable", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SCENE_3D).model(sphereFilename)
            .sameTexturesDir().scale(0.1f).load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
    // drawable1.internal_getMeshes()[0].setMaterial(Material.createTestMaterial());

    GameDataFactory.getInstance().addSupplier("test-point-light", () -> {
      Drawable drawable1 = GameDataFactory.getInstance().get("test-point-light-drawable");
      PointLight p1 = new PointLight(Colour.fromHex("#00f"), new Vector3f(-2.5f, 5, 0), 1);
      return new TestPointLight(p1, drawable1);
    });

    // Spotlight
    String coneFilename = "resources/basic_shapes/cone.obj";
    GameDataFactory.getInstance().addSupplier("test-spotlight-drawable", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SCENE_3D).model(coneFilename)
            .sameTexturesDir().scale(0.1f).load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
    // drawable2.internal_getMeshes()[0].setMaterial(Material.createTestMaterial());

    GameDataFactory.getInstance().addSupplier("test-spotlight", () -> {
      Drawable drawable2 = GameDataFactory.getInstance().get("test-spotlight-drawable");
      PointLight p2 = new PointLight(Colour.fromHex("#f00"), new Vector3f(10, 5, 5), 5);
      Spotlight s = new Spotlight(p2, new Vector3f(0, -1, 0), 45);
      return new TestSpotlight(s, drawable2);
    });
  }

  /**
   * Test that a transparent window functions correctly when viewed through a transparent window.
   */
  private void loadTransparencyTest() {
    GameDataFactory.getInstance().addSupplier("test-transparency-1", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SCENE_3D)
            .model("src/main/resources/level3d/transparency_test/plane_1x1_vertical.obj")
            .texture("src/main/resources/level3d/transparency_test/purple_window.png")
            .position(-5, 0, 5).load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
    GameDataFactory.getInstance().addSupplier("test-transparency-2", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SCENE_3D)
            .model("src/main/resources/level3d/transparency_test/plane_1x1_vertical.obj")
            .texture("src/main/resources/level3d/transparency_test/purple_window.png")
            .position(-5, 0, 6).load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
  }

  /**
   * Loads a burning torch particle example.
   */
  private void loadBurningTorch() {
    GameDataFactory.getInstance().addSupplier("burning_torch_body", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SCENE_3D)
            .model("src/main/resources/level3d/particle_effects/tall_cylinder_5x1.obj").scale(0.1f)
            .colour(Colour.fromHex("#A47449")).position(-9, -0.6f, 5).load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
    GameDataFactory.getInstance().addSupplier("toolkit.burning_torch_particle", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.BILLBOARD).instances(100)
            .model("src/main/resources/level3d/billboard/plane_1x1_vertical.obj").scale(0.05f)
            .colour(Colour.fromHex("#fff")).visible(false).load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
    GameDataFactory.getInstance().addSupplier("burning_torch_flame",
        () -> new BurningTorch(-9, 0, 5));
  }

  /**
   * Loads a raindrops particle example.
   */
  private void loadRain() {
    GameDataFactory.getInstance().addSupplier("toolkit.raindrop_particle", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.BILLBOARD).instances(500)
            .model("src/main/resources/level3d/billboard/plane_1x1_vertical.obj").scale(0.02f)
            .colour(Colour.fromHex("#fff")).visible(false).load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
    GameDataFactory.getInstance().addSupplier("rain", () -> new Rain(20, -3, 10, 30, 5, 0));
  }

  /**
   * Loads a level-of-detail (LOD) test object that renders a simple object when far away and a
   * complex one as you approach.
   */
  private void loadLODTest() {
    GameDataFactory.getInstance().addSupplier("test-lod-high", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SCENE_3D)
            .model("src/main/resources/level3d/dice/dice.obj").sameTexturesDir().position(30, 0, 0)
            .name("High LOD").load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
    String cubeMeshFilename = "resources/cube/cube.obj";
    GameDataFactory.getInstance().addSupplier("test-lod-low", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SCENE_3D).model(cubeMeshFilename)
            .sameTexturesDir().position(30, 0, 0).load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
    GameDataFactory.getInstance().addSupplier("test-lod-boundary",
        () -> new LodBoundary(30, 0, 0, 20, 30, "test-lod-low", "test-lod-high"));
  }

  // ================================================================

  private void loadLime() throws IOException {
    // Texture limeTexture = Engine.TEXTURE_LOADER.loadTexture("./resources/lime.png");
    // Material limeMaterial = new Material(limeTexture);
    // Mesh mesh = OBJLoader.loadMesh("./resources/arm4b/arm4b.obj");
    // mesh.setMaterial(limeMaterial);

    // FIXME ASSIMP: AssimpLoader claims this file is 5 meshes and crashes when I try to render it.
    // OBJLoader works just fine...
    Drawable lime = AssimpLoader.init(GraphicsPipelineName.SCENE_3D)
        .modelPath("./resources/arm4b/arm4b.obj").sameTexturesDir().load();
    lime.setPosition(0, 0, -5);
    lime.setName("Lime -> arm4b.obj loaded via StaticMeshesLoader");
    // lime.setName("Lime -> arm4b.obj loaded via OBJLoader");
  }

  // FIXME SOUND: Here 'fire' is the only non-relative sound... but surely it should be the only
  // relative sound?
  private void loadAndCreateSounds() throws IOException {
    SoundBuffer buffBack = new SoundBuffer("./resources/sounds/music.ogg");
    Engine.SOUND_MANAGER.addSoundBuffer(buffBack);
    SoundSource sourceMusic = new SoundSource(true, true);
    sourceMusic.setBuffer(buffBack.getBufferId());
    Engine.SOUND_MANAGER.addSoundSource("MUSIC", sourceMusic);

    SoundBuffer buffBeep = new SoundBuffer("./resources/sounds/beep.ogg");
    Engine.SOUND_MANAGER.addSoundBuffer(buffBeep);
    SoundSource sourceBeep = new SoundSource(false, true);
    sourceBeep.setBuffer(buffBeep.getBufferId());
    Engine.SOUND_MANAGER.addSoundSource("BEEP", sourceBeep);

    SoundBuffer buffFire = new SoundBuffer("./resources/sounds/fire.ogg");
    Engine.SOUND_MANAGER.addSoundBuffer(buffFire);
    SoundSource sourceFire = new SoundSource(true, false);
    // FIXME SOUND: I should use an object here to 'emit' the sound
    // Vector3f pos = PARTICLE_EMITTER.getBaseParticle().getPosition();
    sourceFire.setPosition(new Vector3f(0, 0, 0));
    sourceFire.setBuffer(buffFire.getBufferId());
    Engine.SOUND_MANAGER.addSoundSource("FIRE", sourceFire);

    Engine.SOUND_MANAGER.setListener(new SoundListener(new Vector3f(0, 0, 0)));
  }
}
