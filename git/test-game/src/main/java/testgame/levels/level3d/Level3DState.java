package testgame.levels.level3d;

import java.util.List;

import org.joml.Vector2i;

import game1.core.engine.Engine;
import game1.core.engine.userstate.UserState;
import game1.core.engine.userstate.UserStateHelper;
import game1.core.graphics.Animation3D;
import game1.core.graphics.Drawable;
import game1.core.graphics.Image;
import game1.core.graphics.lights.DirectionalLight;
import game1.core.input.Mouse;
import game1.core.sound2.SoundSource;
import game1.datasets.GameData;
import game1.datasets.GameDataFactory;
import game1.events.EventsFactory;
import game1.events.Game1Events;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.scaling.ScalingStrategies;
import game1.resolutions.shared.ResolutionHints;
import game1.toolkit.actors.Player3D;
import game1.toolkit.debugtools.DebugAnimationFrames;
import game1.toolkit.debugtools.DebugStaticText;
import game1.toolkit.lights.MovingActor;
import game1.toolkit.lod.LodBoundary;
import testgame.devtools.TestGameDevTools;

public class Level3DState extends UserState {

  public static DirectionalLight DIRECTIONAL_LIGHT;
  public static Animation3D ANIMATED_3D_MODEL;
  public static Drawable TEST_2D_RESIZE;

  private String uiLayer = "level-3d-ui-layer";

  @Override
  public List<String> listRequiredTags() {
    return List.of(Level3DDataset.TAG);
  }

  @Override
  public void apply() {
    ResolutionLayersFactory.getInstance().createStaticLayer(uiLayer, new Vector2i(1920, 1080),
        ScalingStrategies.get(0));

    UserStateHelper.setKeyBindings("Level3DKeyBindings", "CommonKeyBindings", "AdjusterKeyBindings",
        "DirectionalLightKeyBindings");
    UserStateHelper.setMouse(Mouse.MOUSE_CAMERA);

    Player3D player = new Player3D(-14, 8.5f, 23, 20, 36, 0);
    EventsFactory.getInstance().raise(Game1Events.CAMERA_SET_POSITION_V3F, player.getPosition());
    EventsFactory.getInstance().raise(Game1Events.CAMERA_SET_ROTATION_V3F, player.getRotation());
    Engine.GAME_STATE.add3dActor(player);
    Engine.GAME_STATE.add3dActor(DirectionalLight.createDefault());

    add2DAnimation();
    addCrosshairs();
    addSkybox();
    addNormalMappingTest();
    addFloor();
    addDebugCubes();
    add3D();
    add3DWireframe();
    add2DDrawableResizeTest();
    add3DAnimation();
    // add3DAnimationMultipleAnimations();
    addBillboard();
    addLights();
    addTransparencyTest();
    addBurningTorch();
    addRain();
    addLODTest(player);

    // addShadowTest();
    // addPlane();
    // addLime();
    // playSounds();

    TestGameDevTools.addActors();
  }

  private void add2DAnimation() {
    Engine.GAME_STATE.addActor(GameDataFactory.getInstance().get("abcd.png"), uiLayer);
  }

  private void addCrosshairs() {
    Image image = GameDataFactory.getInstance().get("crosshairs.png");
    ResolutionHints hints = ResolutionLayersFactory.getInstance().getLayerByName(uiLayer)
        .getResolutionHints();
    float scale = hints.getResolutionScale();
    float width = hints.getResolutionWidth() * scale;
    float height = hints.getResolutionHeight() * scale;
    image.setPosition(width / 2 - 16, height / 2 - 16, Integer.MAX_VALUE);
    Engine.GAME_STATE.addActor(image, uiLayer);
  }

  private void addSkybox() {
    Engine.GAME_STATE.add3dActor(GameDataFactory.getInstance().get("test-3d-skybox"));
  }

  private void addNormalMappingTest() {
    Engine.GAME_STATE.add3dActor(GameDataFactory.getInstance().get("test-3d-normal-mapping-1"));
    Engine.GAME_STATE.add3dActor(GameDataFactory.getInstance().get("test-3d-normal-mapping-2"));
  }

  private void addFloor() {
    GameData gameData = GameDataFactory.getInstance();
    Engine.GAME_STATE.add3dActor(gameData.get("test-3d-floor-1"));
    Engine.GAME_STATE.add3dActor(gameData.get("test-3d-floor-2"));
    Engine.GAME_STATE.add3dActor(gameData.get("test-3d-floor-3"));
    Engine.GAME_STATE.add3dActor(gameData.get("test-3d-floor-4"));
    Engine.GAME_STATE.add3dActor(gameData.get("test-3d-floor-5"));
    Engine.GAME_STATE.add3dActor(gameData.get("test-3d-floor-6"));
  }

  private void addDebugCubes() {
    Drawable d = (Drawable) GameDataFactory.getInstance().get("debug-cube");
    createDebugCube(d, 0, 0, 0);
    createDebugCube(d, 10, 0, 0);
    createDebugCube(d, 20, 0, 0);
    createDebugCube(d, 30, 0, 0);
    createDebugCube(d, 0, 10, 0);
    createDebugCube(d, 0, 20, 0);
    createDebugCube(d, 0, 30, 0);
    createDebugCube(d, 0, 0, 10);
    createDebugCube(d, 0, 0, 20);
    createDebugCube(d, 0, 0, 30);
  }

  private void createDebugCube(Drawable d, float x, float y, float z) {
    Drawable clone = d.getClone();
    clone.setPosition(x, y, z);
    clone.setScale(0.1f);
    clone.setName("Debug Cube [%s,%s,%s]", x, y, z);
    Engine.GAME_STATE.add3dActor(clone);
  }

  private void add3D() {
    Engine.GAME_STATE.add3dActor(GameDataFactory.getInstance().get("test-pipelines-3d"));
  }

  private void add3DWireframe() {
    Engine.GAME_STATE.add3dActor(GameDataFactory.getInstance().get("test-pipelines-3d-wireframe"));
  }

  private void add2DDrawableResizeTest() {
    TEST_2D_RESIZE = (Drawable) GameDataFactory.getInstance().get("test-2d-resize");
    Engine.GAME_STATE.addActor(TEST_2D_RESIZE, uiLayer);
    Engine.GAME_STATE.addActor(new DebugStaticText("V: Test 2D mesh resizing", 1550, 870), uiLayer);
  }

  private void add3DAnimation() {
    Engine.GAME_STATE.add3dActor(GameDataFactory.getInstance().get("test-pipelines-3d-animated"));
  }

  private void add3DAnimationMultipleAnimations() {
    Animation3D animation3d = (Animation3D) GameDataFactory.getInstance()
        .get("test-pipelines-3d-animated-multiple");
    ANIMATED_3D_MODEL = animation3d;
    DebugAnimationFrames.ANIMATION = ANIMATED_3D_MODEL;
    Engine.GAME_STATE.add3dActor(animation3d);
  }

  private void addBillboard() {
    Engine.GAME_STATE.add3dActor(GameDataFactory.getInstance().get("billboard"));
  }

  private void addLights() {
    Engine.GAME_STATE.add3dActor(GameDataFactory.getInstance().get("test-point-light"));
    Engine.GAME_STATE.add3dActor(GameDataFactory.getInstance().get("test-spotlight"));
  }

  private void addTransparencyTest() {
    Engine.GAME_STATE.add3dActor(GameDataFactory.getInstance().get("test-transparency-1"));
    Engine.GAME_STATE.add3dActor(GameDataFactory.getInstance().get("test-transparency-2"));
  }

  private void addBurningTorch() {
    Engine.GAME_STATE.add3dActor(GameDataFactory.getInstance().get("burning_torch_body"));
    Engine.GAME_STATE.add3dActor(GameDataFactory.getInstance().get("burning_torch_flame"));
  }

  private void addRain() {
    Engine.GAME_STATE.add3dActor(GameDataFactory.getInstance().get("rain"));
  }

  private void addLODTest(Player3D player) {
    LodBoundary lodBoundary = GameDataFactory.getInstance().get("test-lod-boundary");
    lodBoundary.setMoveable(player);
    Engine.GAME_STATE.add3dActor(lodBoundary);
  }

  // ================================================================

  private void addPlane() {
    Engine.GAME_STATE.add3dActor(GameDataFactory.getInstance().get("test-create-plane"));
  }

  private void addLime() {
    Engine.GAME_STATE.add3dActor(GameDataFactory.getInstance().get("test-lime"));
  }

  private void playSounds() {
    SoundSource music = (SoundSource) GameDataFactory.getInstance().get("openal-sounds-music");
    music.play();
    SoundSource fire = (SoundSource) GameDataFactory.getInstance().get("openal-sounds-fire");
    fire.play();
  }

  private void addShadowTest() {
    Drawable drawable = (Drawable) GameDataFactory.getInstance().get("debug-cube");

    Drawable clone1 = drawable.getClone();
    clone1.setPosition(5, 0, 10);
    clone1.setScale(0.1f);
    clone1.setName("Shadow Test 1");
    Engine.GAME_STATE.add3dActor(clone1);

    Drawable clone2 = drawable.getClone();
    clone2.setPosition(6, 0, 10);
    clone2.setScale(0.1f);
    clone2.setName("Shadow Test 2");
    Engine.GAME_STATE.add3dActor(clone2);

    Drawable clone3 = drawable.getClone();
    clone3.setPosition(7, 0, 8);
    clone3.setScale(0.1f);
    clone3.setName("Shadow Test 3");
    Engine.GAME_STATE.add3dActor(clone3);

    Drawable clone4 = drawable.getClone();
    clone4.setPosition(8.5f, 0, 11);
    clone4.setScale(0.1f);
    clone4.setName("Shadow Test 4");
    Engine.GAME_STATE.add3dActor(new MovingActor(clone4, 0, 0, -0.01f, 200));

    Drawable clone5 = drawable.getClone();
    clone5.setPosition(8, 0, 12);
    clone5.setScale(0.1f);
    clone5.setName("Shadow Test 5");
    Engine.GAME_STATE.add3dActor(clone5);

    Drawable clone6 = drawable.getClone();
    clone6.setPosition(9, 0, 9);
    clone6.setScale(0.1f);
    clone6.setName("Shadow Test 6");
    Engine.GAME_STATE.add3dActor(new MovingActor(clone6, 0.01f, 0, 0, 200));
  }
}
