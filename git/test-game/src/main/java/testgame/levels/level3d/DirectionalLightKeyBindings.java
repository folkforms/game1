package testgame.levels.level3d;

import game1.core.input.KeyBindings;
import game1.core.input.KeyCommand;
import game1.core.input.Keys;
import game1.events.Command;

public class DirectionalLightKeyBindings extends KeyBindings {

  public DirectionalLightKeyBindings() {
    register(KeyCommand.init().command(new Command() {
      @Override
      public void execute() {
        Level3DState.DIRECTIONAL_LIGHT.decreaseLightAngle1();
      }
    }).noBlock().keys(Keys.KEY_UP_ARROW));
    register(KeyCommand.init().command(new Command() {
      @Override
      public void execute() {
        Level3DState.DIRECTIONAL_LIGHT.increaseLightAngle1();
      }
    }).noBlock().keys(Keys.KEY_DOWN_ARROW));
    register(KeyCommand.init().command(new Command() {
      @Override
      public void execute() {
        Level3DState.DIRECTIONAL_LIGHT.increaseLightAngle2();
      }
    }).noBlock().keys(Keys.KEY_LEFT_ARROW));
    register(KeyCommand.init().command(new Command() {
      @Override
      public void execute() {
        Level3DState.DIRECTIONAL_LIGHT.decreaseLightAngle2();
      }
    }).noBlock().keys(Keys.KEY_RIGHT_ARROW));
  }
}
