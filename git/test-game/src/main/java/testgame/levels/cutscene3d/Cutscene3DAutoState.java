package testgame.levels.cutscene3d;

import game1.core.engine.Engine;
import game1.core.engine.userstate.UserStateHelper;
import game1.core.input.Mouse;
import game1.datasets.GameDataFactory;
import game1.toolkit.cutscenes.internal.Cutscene;

public class Cutscene3DAutoState extends Cutscene3DBaseState {
  @Override
  public void apply() {
    super.apply();

    UserStateHelper.setKeyBindings("CommonKeyBindings", "FastForwardKeyBindings");
    UserStateHelper.setMouse(Mouse.MOUSE_OFF);

    Cutscene cutscene = GameDataFactory.getInstance().get("test-cutscene-3d");
    Engine.GAME_STATE.add3dActor(cutscene.getClone());
  }
}
