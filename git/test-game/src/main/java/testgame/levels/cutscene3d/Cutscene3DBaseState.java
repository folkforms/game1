package testgame.levels.cutscene3d;

import java.util.List;

import game1.core.engine.Engine;
import game1.core.engine.userstate.UserState;
import game1.core.graphics.lights.DirectionalLight;
import game1.datasets.GameData;
import game1.datasets.GameDataFactory;

public class Cutscene3DBaseState extends UserState {

  @Override
  public List<String> listRequiredTags() {
    return List.of(Cutscene3DDataset.TAG);
  }

  @Override
  public void apply() {
    GameData gameData = GameDataFactory.getInstance();
    Engine.GAME_STATE.add3dActor(gameData.get("player"));
    Engine.GAME_STATE.add3dActor(DirectionalLight.createDefault());
    Engine.GAME_STATE.add3dActor(gameData.get("cube"));
    Engine.GAME_STATE.add3dActor(gameData.get("cutscene-3d-floor-1"));
    Engine.GAME_STATE.add3dActor(gameData.get("cutscene-3d-floor-2"));
    Engine.GAME_STATE.add3dActor(gameData.get("cutscene-3d-floor-3"));
    Engine.GAME_STATE.add3dActor(gameData.get("cutscene-3d-floor-4"));
    Engine.GAME_STATE.add3dActor(gameData.get("cutscene-3d-floor-5"));
    Engine.GAME_STATE.add3dActor(gameData.get("cutscene-3d-floor-6"));
  }
}
