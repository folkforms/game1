package testgame.levels.cutscene3d;

import game1.core.engine.userstate.UserStateHelper;
import game1.core.input.Mouse;

public class Cutscene3DPrepState extends Cutscene3DBaseState {

  @Override
  public void apply() {
    UserStateHelper.setKeyBindings("Level3DKeyBindings", "CommonKeyBindings",
        "FastForwardKeyBindings");
    UserStateHelper.setMouse(Mouse.MOUSE_CAMERA);
  }
}
