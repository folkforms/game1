package testgame.levels.cutscene3d;

import java.io.IOException;

import game1.core.pipelines.GraphicsPipelineName;
import game1.core.resources.loaders.DrawableLoader;
import game1.datasets.GameData;
import game1.datasets.GameDataFactory;
import game1.filesystem.FileSystem;
import game1.filesystem.FileSystemFactory;
import game1.toolkit.actors.Player3D;
import game1.toolkit.cutscenes.CutsceneLoader;
import testgame.levels.shared.DatasetHelper;
import testgame.main.TGStates;

public class Cutscene3DDataset {

  public static final String TAG = "Cutscene3DDataset";
  private FileSystem fileSystem;

  public Cutscene3DDataset() {
    fileSystem = FileSystemFactory.getInstance();
    GameDataFactory.getInstance().addSupplier("test-cutscene-3d", () -> {
      try {
        return CutsceneLoader.init("src/main/resources/cutscene_3d/cutscenes/test-cutscene-3d.txt")
            .nextState(TGStates.MAIN_MENU).load(fileSystem).getClone();
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });

    loadPlayer();
    loadCube();
    loadFloor();
  }

  private void loadPlayer() {
    GameDataFactory.getInstance().addSupplier("player", () -> new Player3D(-25, 18, 31, 32, 41, 0));
  }

  private void loadCube() {
    GameDataFactory.getInstance().addSupplier("cube", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SCENE_3D).instances(1)
            .model("src/main/resources/stress_test_3d/stress_test_3d_cube/cube.obj")
            .texture("src/main/resources/stress_test_3d/stress_test_3d_cube/cube.png")
            .position(0, 3, 0).load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
  }

  private void loadFloor() {
    int y = 0;
    GameData gameData = GameDataFactory.getInstance();
    gameData.addSupplier("cutscene-3d-floor-1",
        () -> DatasetHelper.createFloor(-10, y, 10, "#333"));
    gameData.addSupplier("cutscene-3d-floor-2", () -> DatasetHelper.createFloor(10, y, 10, "#434"));
    gameData.addSupplier("cutscene-3d-floor-3",
        () -> DatasetHelper.createFloor(-30, y, 10, "#334"));
    gameData.addSupplier("cutscene-3d-floor-4",
        () -> DatasetHelper.createFloor(-30, y, 30, "#343"));
    gameData.addSupplier("cutscene-3d-floor-5",
        () -> DatasetHelper.createFloor(-10, y, 30, "#344"));
    gameData.addSupplier("cutscene-3d-floor-6", () -> DatasetHelper.createFloor(10, y, 30, "#433"));
  }
}
