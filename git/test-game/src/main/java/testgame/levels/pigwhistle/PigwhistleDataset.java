package testgame.levels.pigwhistle;

import java.io.IOException;

import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.pipelines.GraphicsPipelineName;
import game1.core.resources.builders.PrimitiveBuilder;
import game1.core.resources.loaders.DrawableLoader;
import game1.datasets.GameData;
import game1.datasets.GameDataFactory;
import game1.filesystem.FileSystem;
import game1.filesystem.FileSystemFactory;

public class PigwhistleDataset {

  public static final String TAG = "PigwhistleDataset";
  private FileSystem fileSystem;
  private final String treePackPrefix = "../../../../../assets/models/cc0/creativetrio.art/tree pack/";

  public PigwhistleDataset() {
    GameData gameData = GameDataFactory.getInstance();
    fileSystem = FileSystemFactory.getInstance();

    gameData.addSupplier("pw.asset.skybox", () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SKYBOX)
            .model("src/main/resources/level3d/skybox/skybox.obj")
            .texture("src/main/resources/level3d/skybox/skybox.png").scale(200).load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });

    addTree("pw.asset.tree-01", "Tree_Pack_01.obj", "Tree_Pack_001_Diffuse.png", -14, 0, 7);
    addTree("pw.asset.tree-02", "Tree_Pack_02.obj", "Tree_Pack_001_Diffuse.png", -20, 0, 10);
    addTree("pw.asset.tree-03", "Tree_Pack_03.obj", "Tree_Pack_001_Diffuse.png", -18, 0, 5);
    addTree("pw.asset.tree-04", "Tree_Pack_04.obj", "Tree_Pack_001_Diffuse.png", -10, 0, 2);
    addTree("pw.asset.tree-05", "Tree_Pack_05.obj", "Tree_Pack_001_Diffuse.png", -15, 0, 11);
    addTree("pw.asset.tree-06", "Tree_Pack_06.obj", "Tree_Pack_001_Diffuse.png", -3, 0, 8);
    addTree("pw.asset.tree-07", "Tree_Pack_07.obj", "Tree_Pack_001_Diffuse.png", -8, 0, 4);
    addTree("pw.asset.tree-08", "Tree_Pack_08.obj", "Tree_Pack_001_Diffuse.png", -15, 0, 14);
    addTree("pw.asset.tree-09", "Tree_Pack_09.obj", "Tree_Pack_001_Diffuse.png", -14, 0, 13);
    addTree("pw.asset.tree-10", "Tree_Pack_10.obj", "Tree_Pack_001_Diffuse.png", -15, 0, 6);
    addTree("pw.asset.tree-11", "Tree_Pack_11.obj", "Tree_Pack_001_Diffuse.png", -11, 0, 12);
    addTree("pw.asset.tree-12", "Tree_Pack_12.obj", "Tree_Pack_001_Diffuse.png", -9, 0, 3);
    addTree("pw.asset.tree-13", "Tree_Pack_13.obj", "Tree_Pack_001_Diffuse.png", -17, 0, 1);
    addTree("pw.asset.tree-14", "Tree_Pack_14.obj", "Tree_Pack_001_Diffuse.png", -12, 0, 9);

    // Floors
    gameData.addSupplier("room-1-floor", () -> PrimitiveBuilder.init(GraphicsPipelineName.SCENE_3D)
        .rect3d(30, 0, -30).colour(Colour.fromHex("f005")).build());
    gameData.addSupplier("room-1-floor-collision", () -> PrimitiveBuilder
        .init(GraphicsPipelineName.WIREFRAME).rect3d(30, 0, -30).collisionMesh(true).build());
    gameData.addSupplier("corridor-floor",
        () -> PrimitiveBuilder.init(GraphicsPipelineName.SCENE_3D).rect3d(10, 0, -20)
            .position(10, 0, -30).colour(Colour.fromHex("0f05")).build());
    gameData.addSupplier("corridor-floor-collision",
        () -> PrimitiveBuilder.init(GraphicsPipelineName.WIREFRAME).rect3d(10, 0, -20)
            .position(10, 0, -30).collisionMesh(true).build());
    gameData.addSupplier("room-2-floor", () -> PrimitiveBuilder.init(GraphicsPipelineName.SCENE_3D)
        .rect3d(30, 0, -30).position(0, 0, -50).colour(Colour.fromHex("00f5")).build());
    gameData.addSupplier("room-2-floor-collision",
        () -> PrimitiveBuilder.init(GraphicsPipelineName.WIREFRAME).rect3d(30, 0, -30)
            .position(0, 0, -50).collisionMesh(true).build());

    // Walls
    gameData.addSupplier("wall-10", () -> PrimitiveBuilder.init(GraphicsPipelineName.SCENE_3D)
        .rect3d(10, 11, 0).colour(Colour.fromHex("f0f5")).build());
    gameData.addSupplier("wall-20", () -> PrimitiveBuilder.init(GraphicsPipelineName.SCENE_3D)
        .rect3d(20, 11, 0).colour(Colour.fromHex("ff05")).build());
    gameData.addSupplier("wall-30b", () -> PrimitiveBuilder.init(GraphicsPipelineName.SCENE_3D)
        .rect3d(30, 11, 0).colour(Colour.fromHex("0ff5")).build());
    gameData.addSupplier("wall-30a", () -> PrimitiveBuilder.init(GraphicsPipelineName.SCENE_3D)
        .rect3d(30, 11, 0).colour(Colour.fromHex("f555")).build());
    gameData.addSupplier("wall-10-collision", () -> PrimitiveBuilder
        .init(GraphicsPipelineName.WIREFRAME).rect3d(10, 11, 0).collisionMesh(true).build());
    gameData.addSupplier("wall-20-collision", () -> PrimitiveBuilder
        .init(GraphicsPipelineName.WIREFRAME).rect3d(20, 11, 0).collisionMesh(true).build());
    gameData.addSupplier("wall-30b-collision", () -> PrimitiveBuilder
        .init(GraphicsPipelineName.WIREFRAME).rect3d(30, 11, 0).collisionMesh(true).build());
    gameData.addSupplier("wall-30a-collision", () -> PrimitiveBuilder
        .init(GraphicsPipelineName.WIREFRAME).rect3d(30, 11, 0).collisionMesh(true).build());
    create("room-1-wall-1", "wall-30a", 30, 0, 0, 0, 180, 0);
    create("room-1-wall-2", "wall-30b", 0, 0, 0, 0, 90, 0);
    create("room-1-wall-3", "wall-30b", 30, 0, -30, 0, -90, 0);
    create("room-1-wall-4", "wall-10", 0, 0, -30, 0, 0, 0);
    create("room-1-wall-5", "wall-10", 20, 0, -30, 0, 0, 0);
    create("corridor-wall-1", "wall-20", 10, 0, -30, 0, 90, 0);
    create("corridor-wall-2", "wall-20", 20, 0, -50, 0, -90, 0);
    create("room-2-wall-1", "wall-10", 10, 0, -50, 0, 180, 0);
    create("room-2-wall-2", "wall-10", 30, 0, -50, 0, 180, 0);
    create("room-2-wall-3", "wall-30b", 0, 0, -50, 0, 90, 0);
    create("room-2-wall-4", "wall-30b", 30, 0, -80, 0, -90, 0);
    create("room-2-wall-5", "wall-30a", 0, 0, -80, 0, 0, 0);
  }

  private void addTree(String ref, String model, String texture, int x, int y, int z) {
    GameDataFactory.getInstance().addSupplier(ref, () -> {
      try {
        return DrawableLoader.init(GraphicsPipelineName.SCENE_3D).model(treePackPrefix + model)
            .texture(treePackPrefix + texture).position(x, y, z).load(fileSystem);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
  }

  private void create(String newRef, String ref, float x, float y, float z, int xDegrees,
      int yDegrees, int zDegrees) {
    createWall(newRef, ref, x, y, z, xDegrees, yDegrees, zDegrees);
    createCollisionMesh(newRef, ref, x, y, z, xDegrees, yDegrees, zDegrees);
  }

  private void createWall(String newRef, String ref, float x, float y, float z, int xDegrees,
      int yDegrees, int zDegrees) {
    GameDataFactory.getInstance().addSupplier(newRef, () -> {
      Drawable temp = GameDataFactory.getInstance().get(ref);
      Drawable d = temp.getClone();
      d.setPosition(x, y, z);
      d.rotate(xDegrees, yDegrees, zDegrees);
      return d;
    });
  }

  private void createCollisionMesh(String newRef, String ref, float x, float y, float z,
      int xDegrees, int yDegrees, int zDegrees) {
    GameDataFactory.getInstance().addSupplier(newRef + "-collision", () -> {
      Drawable temp = GameDataFactory.getInstance().get(ref + "-collision");
      Drawable d = temp.getClone();
      d.setPosition(x, y, z);
      d.rotate(xDegrees, yDegrees, zDegrees);
      d.setCollisionMesh(true);
      return d;
    });
  }
}
