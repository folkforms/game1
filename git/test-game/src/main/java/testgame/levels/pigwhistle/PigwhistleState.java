package testgame.levels.pigwhistle;

import java.util.List;

import org.joml.Vector3f;

import game1.core.engine.Engine;
import game1.core.engine.userstate.UserState;
import game1.core.engine.userstate.UserStateHelper;
import game1.core.graphics.Colour;
import game1.core.graphics.lights.DirectionalLight;
import game1.core.input.Mouse;
import game1.datasets.GameData;
import game1.datasets.GameDataFactory;
import game1.toolkit.actors.Player3D;

public class PigwhistleState extends UserState {

  @Override
  public List<String> listRequiredTags() {
    return List.of(PigwhistleDataset.TAG);
  }

  @Override
  public void apply() {
    UserStateHelper.setMouse(Mouse.MOUSE_CAMERA);
    UserStateHelper.setKeyBindings("Level3DKeyBindings", "CommonKeyBindings",
        "AdjusterKeyBindings");

    addPlayer();
    addLight();
    GameData gameData = GameDataFactory.getInstance();
    Engine.GAME_STATE.add3dActor(gameData.get("pw.asset.skybox"));
    Engine.GAME_STATE.add3dActor(gameData.get("pw.asset.tree-01"));
    Engine.GAME_STATE.add3dActor(gameData.get("pw.asset.tree-02"));
    Engine.GAME_STATE.add3dActor(gameData.get("pw.asset.tree-03"));
    Engine.GAME_STATE.add3dActor(gameData.get("pw.asset.tree-04"));
    Engine.GAME_STATE.add3dActor(gameData.get("pw.asset.tree-05"));
    Engine.GAME_STATE.add3dActor(gameData.get("pw.asset.tree-06"));
    Engine.GAME_STATE.add3dActor(gameData.get("pw.asset.tree-07"));
    Engine.GAME_STATE.add3dActor(gameData.get("pw.asset.tree-08"));
    Engine.GAME_STATE.add3dActor(gameData.get("pw.asset.tree-09"));
    Engine.GAME_STATE.add3dActor(gameData.get("pw.asset.tree-10"));
    Engine.GAME_STATE.add3dActor(gameData.get("pw.asset.tree-11"));
    Engine.GAME_STATE.add3dActor(gameData.get("pw.asset.tree-12"));
    Engine.GAME_STATE.add3dActor(gameData.get("pw.asset.tree-13"));
    Engine.GAME_STATE.add3dActor(gameData.get("pw.asset.tree-14"));

    Engine.GAME_STATE.add3dActor(gameData.get("room-1-floor"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-1-floor-collision"));
    Engine.GAME_STATE.add3dActor(gameData.get("corridor-floor"));
    Engine.GAME_STATE.add3dActor(gameData.get("corridor-floor-collision"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-2-floor"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-2-floor-collision"));

    Engine.GAME_STATE.add3dActor(gameData.get("room-1-wall-1"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-1-wall-1-collision"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-1-wall-2"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-1-wall-2-collision"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-1-wall-3"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-1-wall-3-collision"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-1-wall-4"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-1-wall-4-collision"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-1-wall-5"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-1-wall-5-collision"));

    Engine.GAME_STATE.add3dActor(gameData.get("corridor-wall-1"));
    Engine.GAME_STATE.add3dActor(gameData.get("corridor-wall-1-collision"));
    Engine.GAME_STATE.add3dActor(gameData.get("corridor-wall-2"));
    Engine.GAME_STATE.add3dActor(gameData.get("corridor-wall-2-collision"));

    Engine.GAME_STATE.add3dActor(gameData.get("room-2-wall-1"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-2-wall-1-collision"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-2-wall-2"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-2-wall-2-collision"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-2-wall-3"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-2-wall-3-collision"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-2-wall-4"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-2-wall-4-collision"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-2-wall-5"));
    Engine.GAME_STATE.add3dActor(gameData.get("room-2-wall-5-collision"));
  }

  private void addPlayer() {
    Player3D player = new Player3D(-11, 9, 18, 23, 35, 0);
    Engine.GAME_STATE.add3dActor(player);
  }

  private void addLight() {
    DirectionalLight directionalLight = DirectionalLight.create(Colour.fromHex("#fff"),
        new Vector3f(0, 1, 0), 1);
    directionalLight.setLightSourceDistance(10);
    directionalLight.setOrthoCoords(-10.0f, 10.0f, -10.0f, 10.0f, -1.0f, 20.0f);
    Engine.GAME_STATE.add3dActor(directionalLight);
  }
}
