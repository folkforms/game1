package testgame.levels.mainmenu;

import game1.core.graphics.Colour;
import game1.datasets.GameDataFactory;
import game1.resolutions.layers.ResolutionLayers;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;
import game1.toolkit.screens.mainmenu.SimpleMainMenu;
import game1.toolkit.screens.mainmenu.SimpleMainMenuBuilder;
import game1.toolkit.widgets.background.Background;
import game1.toolkit.widgets.button.Button;
import testgame.main.TGStates;
import testgame.main.ZIndex;

public class TGMainMenuDataset {

  public static final String TAG = "TGMainMenuDataset";

  public TGMainMenuDataset() {
    loadMainMenu();
    loadInGameMenu();
    // FIXME DATASETS: Add this test object back in
    // loadDrawableComparatorTest();
  }

  private void loadMainMenu() {
    GameDataFactory.getInstance().addSupplier("main-menu",
        () -> SimpleMainMenuBuilder.init().setProperty("z", Integer.toString(ZIndex.MENU))
            .addButton("Level 1", TGStates.LEVEL_1_LOADING_SCREEN)
            .addButton("2D Level", TGStates.LEVEL_2D)
            .addButton("3D Level", TGStates.LEVEL_3D_LOADING_SCREEN)
            .addButton("2D Stress Test", TGStates.STRESS_TEST_2D)
            .addButton("3D Stress Test", TGStates.STRESS_TEST_3D)
            .addButton("Pigwhistle", TGStates.PIGWHISTLE)
            .addButton("Cutscene 3D Auto", TGStates.CUTSCENE_3D_AUTO)
            .addButton("Cutscene 3D Prep", TGStates.CUTSCENE_3D_PREP)
            .addButton("Resolution Test 1", TGStates.RESOLUTION_TEST_1)
            .addButton("Resolution Test 2", TGStates.RESOLUTION_TEST_2)
            .addButton("Resolution Test 3", TGStates.RESOLUTION_TEST_3)
            .addButton("Resolution Test 4", TGStates.RESOLUTION_TEST_4)
            .addButton("Fireplace", TGStates.FIREPLACE)
            .setResolutionLayer(TGMainMenuState.LAYER_NAME).build());

    // // DrawableComparator test
    // LinkedActors.link(mainMenu, "drawable-comparator-test-1");
    // LinkedActors.link(mainMenu, "drawable-comparator-test-2");
    // LinkedActors.link(mainMenu, "drawable-comparator-test-3");

    Button b1 = new Button("resolution-button-1", 2f, 100, 100, 50_000, 200, 100);
    GameDataFactory.getInstance().addSupplier("scaling-strategy-button-1", () -> b1);
  }

  private void loadInGameMenu() {
    int w = 800;
    int h = 400;
    ResolutionHints hints = ResolutionLayersFactory.getInstance()
        .getLayerByName(ResolutionLayers.WINDOW_LAYER).getResolutionHints();
    int x = hints.getWindowWidth() / 2 - w / 2;
    int y = hints.getWindowHeight() / 2 - h / 2;

    GameDataFactory.getInstance().addSupplier("in-game-menu", () -> {
      SimpleMainMenu inGameMenu = SimpleMainMenuBuilder.init()
          .addButton("2D Level", TGStates.LEVEL_2D).build();
      inGameMenu
          .addChild(Background.create(x, y, ZIndex.MENU, w, h, new Colour(0.3f, 0.57f, 0.1f, 1)));
      return inGameMenu;
    });
  }

  /**
   * Test that 3 items with identical z-indices are draw in y-index order with the lowest one (in
   * this case the blue square) being at the front.
   */
  private void loadDrawableComparatorTest() {
    // TempActors.put("drawable-comparator-test-1",
    // Background.create(100, 900, 1000, 50, 50, 1, 0, 0, 1));
    // TempActors.put("drawable-comparator-test-2",
    // Background.create(100, 875, 1000, 50, 50, 0, 1, 0, 1));
    // TempActors.put("drawable-comparator-test-3",
    // Background.create(100, 850, 1000, 50, 50, 0, 0, 1, 1));
  }
}
