package testgame.levels.mainmenu;

import java.util.List;

import org.joml.Vector2i;

import game1.core.devtools.DebugToolsMask;
import game1.core.engine.Engine;
import game1.core.engine.userstate.UserState;
import game1.core.engine.userstate.UserStateHelper;
import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.input.Mouse;
import game1.datasets.GameDataFactory;
import game1.events.Command;
import game1.resolutions.layers.ResolutionLayers;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.scaling.ResolutionScalingStrategyV0;
import game1.toolkit.debugtools.DebugMouse;
import game1.toolkit.debugtools.DebugResolutionHints;
import game1.toolkit.debugtools.DebugViewport;
import game1.toolkit.screens.mainmenu.SimpleMainMenu;
import game1.toolkit.screens.optionsmenu.OptionsMenuDataset;
import game1.toolkit.widgets.background.Background;
import game1.toolkit.widgets.button.Button;
import game1.variables.VariablesFactory;

public class TGMainMenuState extends UserState {

  public static String LAYER_NAME = "tg-main-menu-state-layer";

  @Override
  public List<String> listRequiredTags() {
    return List.of(TGMainMenuDataset.TAG, OptionsMenuDataset.TAG);
  }

  @Override
  public void apply() {
    ResolutionLayersFactory.getInstance().createStaticLayer(LAYER_NAME, new Vector2i(1920, 1080),
        new ResolutionScalingStrategyV0());

    UserStateHelper.setMouse(Mouse.MOUSE_CURSOR);
    UserStateHelper.setKeyBindings("TGMainMenuKeyBindings", "CommonKeyBindings");
    UserStateHelper.setMainMenu(this, true);

    Drawable mainMenuBackground = Background.create(LAYER_NAME, Colour.fromHex("#94143433"));
    Engine.GAME_STATE.addActor(mainMenuBackground, LAYER_NAME);

    SimpleMainMenu mainMenu = GameDataFactory.getInstance().get("main-menu");
    Engine.GAME_STATE.addActor(mainMenu, LAYER_NAME);

    // FIXME RESOLUTIONS: Why is this button not appearing?
    Button button = GameDataFactory.getInstance().get("scaling-strategy-button-1");
    Engine.GAME_STATE.addActor(button, LAYER_NAME);

    createScalingStrategyButton("Scaling strategy v0", 0, 1740, 600);
    createScalingStrategyButton("Scaling strategy v1", 1, 1740, 550);
    createScalingStrategyButton("Scaling strategy v2", 2, 1740, 500);

    // Some test buttons to mark out the scene corners (buttons are 40x40)
    createMarkerButton("1", 0, 1080 - 40);
    createMarkerButton("2", 1920 - 40, 1080 - 40);
    createMarkerButton("3", 1920 - 40, 0);
    createMarkerButton("4", 0, 0);

    DebugToolsMask debugToolsMask = new DebugToolsMask();
    DebugMouse debugMouse = new DebugMouse(15, 1050);
    DebugResolutionHints debugResolutionHints = new DebugResolutionHints(15, 990);
    DebugViewport debugViewport = new DebugViewport(15, 1020);
    Engine.GAME_STATE.addActor(debugToolsMask, ResolutionLayers.WINDOW_LAYER);
    Engine.GAME_STATE.addActor(debugMouse, ResolutionLayers.WINDOW_LAYER);
    Engine.GAME_STATE.addActor(debugResolutionHints, ResolutionLayers.WINDOW_LAYER);
    Engine.GAME_STATE.addActor(debugViewport, ResolutionLayers.WINDOW_LAYER);

    ResolutionLayersFactory.getInstance().debug_printData();
  }

  private void createScalingStrategyButton(String label, int strategy, int x, int y) {
    Button scalingStrategyButton = new Button(label, 2f, x, y, 90_000, 180, 40);
    scalingStrategyButton.setCommand(new Command() {
      @Override
      public void execute() {
        VariablesFactory.getInstance().set("resolution_test.scaling_strategy", strategy);
      }
    });
    Engine.GAME_STATE.addActor(scalingStrategyButton, LAYER_NAME);
  }

  private void createMarkerButton(String label, int x, int y) {
    Button markerButton = new Button(label, 2f, x, y, 90_000, 40, 40);
    Engine.GAME_STATE.addActor(markerButton, LAYER_NAME);
  }
}
