package testgame.levels.mainmenu;

import game1.core.input.KeyBindings;
import game1.core.input.KeyCommand;
import game1.core.input.Keys;
import game1.toolkit.commands.MoveToStateCommand;
import testgame.main.TGStates;

public class TGMainMenuKeyBindings extends KeyBindings {

  public TGMainMenuKeyBindings() {
    register(KeyCommand.init().keys(Keys.KEY_1)
        .command(new MoveToStateCommand(TGStates.LEVEL_1_LOADING_SCREEN)));
    register(KeyCommand.init().keys(Keys.KEY_2).command(new MoveToStateCommand(TGStates.LEVEL_2D)));
    register(KeyCommand.init().keys(Keys.KEY_3).command(new MoveToStateCommand(TGStates.LEVEL_3D)));
    register(KeyCommand.init().keys(Keys.KEY_4)
        .command(new MoveToStateCommand(TGStates.STRESS_TEST_2D)));
    register(KeyCommand.init().keys(Keys.KEY_5)
        .command(new MoveToStateCommand(TGStates.STRESS_TEST_3D)));
    register(KeyCommand.init().keys(Keys.KEY_6).block()
        .command(new MoveToStateCommand(TGStates.PIGWHISTLE)));
    register(KeyCommand.init().keys(Keys.KEY_7).block()
        .command(new MoveToStateCommand(TGStates.VIEW_OBJECT)));
    register(KeyCommand.init().keys(Keys.KEY_8).block()
        .command(new MoveToStateCommand(TGStates.CUTSCENE_3D_AUTO)));
    register(KeyCommand.init().keys(Keys.KEY_9).block()
        .command(new MoveToStateCommand(TGStates.CUTSCENE_3D_PREP)));
  }
}
