package testgame.levels.level2d;

import java.util.List;

import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector4f;

import game1.core.devtools.DebugToolsMask;
import game1.core.engine.Engine;
import game1.core.engine.userstate.UserState;
import game1.core.engine.userstate.UserStateHelper;
import game1.core.graphics.Drawable;
import game1.core.input.Mouse;
import game1.datasets.GameDataFactory;
import game1.resolutions.layers.ResolutionLayer;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.scaling.ScalingStrategies;
import game1.resolutions.scaling.ScalingStrategy;
import game1.resolutions.viewports.MobileCentrePointControlledSceneViewport;
import game1.toolkit.debugtools.DebugMouse;
import game1.toolkit.debugtools.DebugResolutionHints;
import game1.toolkit.debugtools.DebugViewport;
import game1.toolkit.widgets.background.Background;
import game1.variables.VariablesFactory;

public class Level2DState extends UserState {

  public static Platforms2 PLATFORMS;

  @Override
  public List<String> listRequiredTags() {
    return List.of(Level2DDataset.TAG);
  }

  @Override
  public void apply() {
    UserStateHelper.setKeyBindings("Level2DKeyBindings", "CommonKeyBindings");
    UserStateHelper.setMouse(Mouse.MOUSE_OFF);

    String resolutionLayerName = "level-2d-layer";
    int scalingStrategyNum = VariablesFactory.getInstance().get("resolution_test.scaling_strategy");
    ScalingStrategy scalingStrategy = ScalingStrategies.get(scalingStrategyNum);
    ResolutionLayer mobileControlledLayer = ResolutionLayersFactory.getInstance().createLayer(
        resolutionLayerName, new Vector2i(1920, 1080), new Vector2i(1920 * 3, 1080 * 3),
        scalingStrategy, null);

    String staticLayerName = "level-2d-static-layer";
    ResolutionLayersFactory.getInstance().createStaticLayer(staticLayerName,
        new Vector2i(1920, 1080), scalingStrategy);
    Drawable background = Background.create(staticLayerName, 0.1f, 0, 0);
    Engine.GAME_STATE.addActor(background, staticLayerName);

    Player2D player = new Player2D(2880, 1620);
    Engine.GAME_STATE.addActor(player, resolutionLayerName);
    MobileCentrePointControlledSceneViewport viewport = new MobileCentrePointControlledSceneViewport(
        player, resolutionLayerName, new Vector2f(1920, 1080), new Vector4f(20, 20, 20, 20));
    mobileControlledLayer.setViewport(viewport);

    PLATFORMS = GameDataFactory.getInstance().get("2d-level-platforms");
    Engine.GAME_STATE.addActor(PLATFORMS, resolutionLayerName);

    Engine.GAME_STATE.addActor(new DebugToolsMask(), resolutionLayerName);
    Engine.GAME_STATE.addActor(new DebugMouse(15, 1050), resolutionLayerName);
    Engine.GAME_STATE.addActor(new DebugResolutionHints(15, 990), resolutionLayerName);
    Engine.GAME_STATE.addActor(new DebugViewport(15, 1020), resolutionLayerName);
  }
}
