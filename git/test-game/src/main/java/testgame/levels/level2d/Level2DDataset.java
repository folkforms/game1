package testgame.levels.level2d;

import game1.core.engine.Engine;

public class Level2DDataset {

  public static final String TAG = "level2d";

  public Level2DDataset() {
    Engine.GAME_FILES.register("src/main/resources/packed/test-game-level-2d.spritesheet", TAG);

    // FIXME DATASETS: Need to find a way to add spritesheet tweaks
    // SpritesheetLoader2 spritesheetLoader = new SpritesheetLoader2("level2dspritesheet",
    // "src/main/resources/packed/test-game.spritesheet", 4);
    // AnimationLoader al0 = new AnimationLoader("player_idle_left", "idle.png", spritesheetLoader,
    // x -> x.setTickRate(80).setMirrored(true));
    // AnimationLoader al1 = new AnimationLoader("player_idle_right", "idle.png", spritesheetLoader,
    // x -> x.setTickRate(80));
    // AnimationLoader al2 = new AnimationLoader("player_run_left", "run.png", spritesheetLoader,
    // x -> x.setTickRate(10).setMirrored(true));
    // AnimationLoader al3 = new AnimationLoader("player_run_right", "run.png", spritesheetLoader,
    // x -> x.setTickRate(10));
    // AnimationLoader al4 = new AnimationLoader("player_jump_left", "jump.png", spritesheetLoader,
    // x -> x.setTickRate(20).setMirrored(true));
    // AnimationLoader al5 = new AnimationLoader("player_jump_right", "jump.png", spritesheetLoader,
    // x -> x.setTickRate(20));
    // Engine.ACTOR_LOADERS.register(List.of(al0, al1, al2, al3, al4, al5), "level2d");

    Engine.GAME_FILES.addMultiLoader(".platforms", PlatformsMultiLoader.class);
    Engine.GAME_FILES.register("src/main/resources/level2d/2d-level-platforms.platforms", TAG);

    // Platforms2Loader platformsLoader = new Platforms2Loader("2d-level-platforms",
    // "src/main/resources/level2d/2d-level-platforms.txt");
    // Engine.ACTOR_LOADERS.register(platformsLoader, "level2d");
  }
}
