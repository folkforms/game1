package testgame.levels.level2d;

import org.joml.Vector3f;
import org.joml.Vector4f;

import game1.actors.Actor;
import game1.actors.Mobile;
import game1.actors.Tickable;
import game1.actors.Velocity;
import game1.core.engine.Engine;
import game1.core.graphics.Animation;
import game1.core.pipelines.Stencil2d;
import game1.datasets.GameData;
import game1.datasets.GameDataFactory;
import game1.events.EventSubscriptionsFactory;
import game1.events.ReceiveEvent;
import game1.primitives.Rectangle;
import game1.toolkit.forces.Friction;
import game1.toolkit.forces.Gravity;
import testgame.main.ZIndex;

// FIXME MOVEABLE Can we make this extend toolkit.Player?
public class Player2D implements Actor, Velocity, Mobile, Tickable {

  private Animation PLAYER_IDLE_LEFT;
  private Animation PLAYER_IDLE_RIGHT;
  private Animation PLAYER_RUN_LEFT;
  private Animation PLAYER_RUN_RIGHT;
  private Animation PLAYER_JUMP_LEFT;
  private Animation PLAYER_JUMP_RIGHT;

  private Animation currentAnimation;
  private long numTicks = 0;
  private int speedX, speedY;
  private int speedHorizontal = 6;
  private boolean onSolidGround;
  private int x, y, z;
  private int facing = 0;

  public Player2D(int x, int y) {
    EventSubscriptionsFactory.getInstance().subscribe(this);
    this.x = x;
    this.y = y;
    this.z = ZIndex.PLAYER;

    GameData gameData = GameDataFactory.getInstance();
    PLAYER_IDLE_LEFT = gameData.get("idle.png-mirrored");
    PLAYER_IDLE_RIGHT = gameData.get("idle.png");
    PLAYER_RUN_LEFT = gameData.get("run.png-mirrored");
    PLAYER_RUN_RIGHT = gameData.get("run.png");
    PLAYER_JUMP_LEFT = gameData.get("jump.png-mirrored");
    PLAYER_JUMP_RIGHT = gameData.get("jump.png");

    PLAYER_IDLE_LEFT.setVisible(false);
    PLAYER_IDLE_RIGHT.setVisible(false);
    PLAYER_RUN_LEFT.setVisible(false);
    PLAYER_RUN_RIGHT.setVisible(false);
    PLAYER_JUMP_LEFT.setVisible(false);
    PLAYER_JUMP_RIGHT.setVisible(false);

    PLAYER_IDLE_LEFT.setPosition(x, y, z + 1);
    PLAYER_IDLE_RIGHT.setPosition(x, y, z + 1);
    PLAYER_RUN_LEFT.setPosition(x, y, z + 1);
    PLAYER_RUN_RIGHT.setPosition(x, y, z + 1);
    PLAYER_JUMP_LEFT.setPosition(x, y, z + 1);
    PLAYER_JUMP_RIGHT.setPosition(x, y, z + 1);

    // This is a silly stencil test that shows an arbitrary rectangle of semi-transparent colour
    Stencil2d stencil = new Stencil2d(Stencil2d.INSIDE, new Vector4f(700, 480, 200, 200),
        new Vector4f(0, 1, 1, 0.5f));
    PLAYER_IDLE_LEFT.setStencil(stencil);
    PLAYER_IDLE_RIGHT.setStencil(stencil);
    PLAYER_RUN_LEFT.setStencil(stencil);
    PLAYER_RUN_RIGHT.setStencil(stencil);
    PLAYER_JUMP_LEFT.setStencil(stencil);
    PLAYER_JUMP_RIGHT.setStencil(stencil);

    addChild(PLAYER_IDLE_LEFT);
    addChild(PLAYER_IDLE_RIGHT);
    addChild(PLAYER_RUN_LEFT);
    addChild(PLAYER_RUN_RIGHT);
    addChild(PLAYER_JUMP_LEFT);
    addChild(PLAYER_JUMP_RIGHT);

    facing = 1;
    currentAnimation = PLAYER_IDLE_RIGHT;
    currentAnimation.setPosition(x, y, z + 1);
    currentAnimation.setVisible(true);
  }

  private void chooseAnimation() {
    if (!onSolidGround) {
      if (facing < 0) {
        setAnimation(PLAYER_JUMP_LEFT);
      } else {
        setAnimation(PLAYER_JUMP_RIGHT);
      }
    } else {
      if (speedX < 0) {
        setAnimation(PLAYER_RUN_LEFT);
      } else if (speedX > 0) {
        setAnimation(PLAYER_RUN_RIGHT);
      } else {
        if (facing < 0) {
          setAnimation(PLAYER_IDLE_LEFT);
        } else {
          setAnimation(PLAYER_IDLE_RIGHT);
        }
      }
    }
  }

  private void setAnimation(Animation newAnimation) {
    newAnimation.setPosition(x, y);
    currentAnimation.setVisible(false);
    newAnimation.setVisible(true);
    currentAnimation = newAnimation;
  }

  @Override
  public Vector3f getSpeed() {
    return new Vector3f(speedX, speedY, 0);
  }

  @Override
  public void setSpeed(float x, float y, float z) {
    speedX = (int) x;
    speedY = (int) y;

    if (speedX < 0) {
      facing = -1;
    }
    if (speedX > 0) {
      facing = 1;
    }
    chooseAnimation();
  }

  @ReceiveEvent("player.jump")
  public void jump() {
    if (!Engine.GAME_PAUSED) {
      if (onSolidGround) {
        setSpeed(speedX, speedY + 30);
        onSolidGround = false;
        chooseAnimation();
      }
    }
  }

  @ReceiveEvent("player.left")
  public void moveLeft() {
    if (!Engine.GAME_PAUSED) {
      if (speedX > -speedHorizontal) {
        setSpeed(speedX - 1, speedY);
      }
    }
  }

  @ReceiveEvent("player.right")
  public void moveRight() {
    if (!Engine.GAME_PAUSED) {
      if (speedX < speedHorizontal) {
        setSpeed(speedX + 1, speedY);
      }
    }
  }

  @Override
  public void onTick() {
    numTicks++;
    if (!onSolidGround) {
      Gravity.apply(this, numTicks);
    } else {
      Friction.apply(this, numTicks);
    }

    // Apply inertia and check for platform collisions
    int w = currentAnimation.getCurrentSprite().w;
    int h = currentAnimation.getCurrentSprite().h;
    int projectedX = x + speedX;
    int projectedY = y + speedY;

    boolean smallCX = checkSolidPlatformCollisionsXSmallHeight(projectedX, y, w, h);
    if (smallCX) {
      x += 0.5f * speedX;
      y += 10;
    }

    boolean cx1 = checkSolidPlatformCollisionsX(projectedX, y, w, h);
    if (!cx1) {
      // Not projected to hit any platform, so move the full distance
      x += speedX;
    }

    boolean cy1 = checkSolidPlatformCollisionsY(x, projectedY, w, h);
    boolean cy2 = checkThinPlatformCollisionsY(x, projectedY, w, h);
    if (!cy1 && !cy2) {
      // Not projected to hit any platform, so move the full distance
      y += speedY;
    }

    boolean prevOnSolidGround = onSolidGround;

    // Check if we are on solid ground or not
    boolean sg1 = checkSolidPlatformCollisionsY(x, y - 1, w, h);
    boolean sg2 = checkThinPlatformCollisionsY(x, y - 1, w, h);
    onSolidGround = sg1 || sg2;

    if (onSolidGround != prevOnSolidGround && onSolidGround) {
      chooseAnimation();
    }

    // Update animation position to match player
    Vector3f currentAnimationPosition = currentAnimation.getPosition();
    currentAnimationPosition.set(x, y, ZIndex.PLAYER + 1);
  }

  // Check for X collisions
  private boolean checkSolidPlatformCollisionsX(int projectedX, int projectedY, int w, int h) {
    boolean collisionX = Level2DState.PLATFORMS.overlapsSolidPlatform(projectedX, y, w, h) != null;
    return collisionX;
  }

  /**
   * Did we hit a platform with height < 10? If so we will step up onto it.
   */
  private boolean checkSolidPlatformCollisionsXSmallHeight(int projectedX, int projectedY, int w,
      int h) {
    Rectangle platform = Level2DState.PLATFORMS.overlapsSolidPlatform(projectedX, y, w, h);
    boolean collisionX = platform != null;
    if (!collisionX) {
      return false;
    } else {
      return platform.h <= 10;
    }
  }

  // Check for Y collisions
  private boolean checkSolidPlatformCollisionsY(int projectedX, int projectedY, int w, int h) {
    Rectangle platform = Level2DState.PLATFORMS.overlapsSolidPlatform(x, projectedY, w, h);
    if (platform == null) {
      return false;
    } else {
      if (platform.y + platform.h <= y) {
        // Platform below? Fall just enough to place us on top of the platform
        int maxFall = platform.y + platform.h - y;
        y += maxFall;
        onSolidGround = true;
        speedY = 0;
      } else if (!onSolidGround && speedY > 0) {
        // Platform above? Move us up just enough to hit our head against the platform
        int maxUp = platform.y - (y + h);
        y += maxUp;
        speedY = 0;
      }
    }
    return true;
  }

  // Check for Y collisions only
  private boolean checkThinPlatformCollisionsY(int projectedX, int projectedY, int w, int h) {
    Rectangle platform = Level2DState.PLATFORMS.overlapsThinPlatform(x, projectedY, w, h);
    if (platform == null) {
      return false;
    } else {

      if (platform.y + platform.h <= y) {
        // Platform below? Fall just enough to place us on top of the platform
        int maxFall = platform.y + platform.h - y;
        y += maxFall;
        onSolidGround = true;
        speedY = 0;
      } else {
        // Platform above? We can jump up through thin platforms
        return false;
      }
    }
    return true;
  }

  @Override
  public Vector3f getPosition() {
    return new Vector3f(x, y, z);
  }

  @Override
  public void setPosition(float x, float y, float z) {
    this.x = (int) x;
    this.y = (int) y;
    this.z = (int) z;
  }

  @Override
  public float getWidth() {
    return currentAnimation.getWidth();
  }

  @Override
  public float getHeight() {
    return currentAnimation.getHeight();
  }
}
