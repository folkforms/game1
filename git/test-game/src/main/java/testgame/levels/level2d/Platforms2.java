package testgame.levels.level2d;

import java.util.ArrayList;
import java.util.List;

import game1.actors.Actor;
import game1.core.graphics.Drawable;
import game1.primitives.Rectangle;

public class Platforms2 implements Actor {

  private List<Rectangle> solidPlatforms = new ArrayList<>();
  private List<Rectangle> thinPlatforms = new ArrayList<>();

  public Platforms2(List<Rectangle> solidPlatforms, List<Rectangle> thinPlatforms,
      List<Drawable> platforms) {
    this.solidPlatforms = solidPlatforms;
    this.thinPlatforms = thinPlatforms;
    for (Drawable p : platforms) {
      addChild(p);
    }
  }

  public Rectangle overlapsSolidPlatform(int itemX, int itemY, int itemWidth, int itemHeight) {
    Rectangle r = new Rectangle(itemX, itemY, itemWidth, itemHeight);
    for (int i = 0; i < solidPlatforms.size(); i++) {
      Rectangle r2 = solidPlatforms.get(i);
      if (r.intersects(r2)) {
        return r2;
      }
    }
    return null;
  }

  public Rectangle overlapsThinPlatform(int itemX, int itemY, int itemWidth, int itemHeight) {
    Rectangle r = new Rectangle(itemX, itemY, itemWidth, itemHeight);
    for (int i = 0; i < thinPlatforms.size(); i++) {
      Rectangle r2 = thinPlatforms.get(i);
      if (r.intersects(r2)) {
        return r2;
      }
    }
    return null;
  }
}
