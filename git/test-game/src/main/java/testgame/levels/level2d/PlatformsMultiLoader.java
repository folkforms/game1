package testgame.levels.level2d;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import org.joml.Vector3f;

import folkforms.textio.TextIO;
import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.graphics.meshes.Mesh;
import game1.core.graphics.meshes.PlaneUtils;
import game1.core.pipelines.GraphicsPipelineName;
import game1.datasets.MultiLoader;
import game1.primitives.Rectangle;

public class PlatformsMultiLoader extends MultiLoader {

  private static Colour solidPlatformColor = new Colour(1, 0, 0);
  private static Colour thinPlatformColor = new Colour(1, 1, 0);

  @Override
  public Map<String, Supplier<?>> load(String path) throws IOException {
    List<String> platformData = fileSystem.loadList(path);
    TextIO.stripBlankLines(platformData);
    TextIO.stripComments(platformData, "#");
    TextIO.stripInlineComments(platformData, "#");

    List<Rectangle> solidPlatforms = new ArrayList<>();
    List<Rectangle> thinPlatforms = new ArrayList<>();
    List<Drawable> primitives = new ArrayList<>();

    for (int i = 0; i < platformData.size(); i++) {
      String line = platformData.get(i);
      String[] tokens = line.split(", ");
      boolean isSolid = tokens[0].toLowerCase().equals("solid");
      Rectangle r = new Rectangle(Integer.parseInt(tokens[1]), Integer.parseInt(tokens[2]),
          Integer.parseInt(tokens[3]), Integer.parseInt(tokens[4]));
      if (isSolid) {
        solidPlatforms.add(r);
      } else {
        thinPlatforms.add(r);
      }
      // String pRef = String.format("platform-%s", i);
      Drawable d = createDrawable(GraphicsPipelineName.SCENE_2D, r.x, r.y, 1, r.w, r.h,
          isSolid ? solidPlatformColor : thinPlatformColor);
      primitives.add(d);
    }
    Platforms2 platforms2 = new Platforms2(solidPlatforms, thinPlatforms, primitives);
    return Map.of("2d-level-platforms", () -> platforms2);
  }

  private static Drawable createDrawable(GraphicsPipelineName pipeline, int x, int y, int z, int w, int h,
      Colour colour) {
    Mesh mesh;
    if (pipeline.equals(GraphicsPipelineName.SCENE_2D)) {
      mesh = PlaneUtils.createPlane2D(w, h, colour);
    } else {
      mesh = PlaneUtils.createPlane3D(x, y, z, colour);
    }
    Drawable drawable = new Drawable(pipeline, mesh);
    drawable.setPosition(new Vector3f(x, y, z));
    drawable.setColour(colour);
    drawable.setCollisionMesh(false);
    return drawable;
  }
}
