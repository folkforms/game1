package testgame.levels.level2d;

import game1.core.input.KeyBindings;
import game1.core.input.KeyCommand;

public class Level2DKeyBindings extends KeyBindings {

  public Level2DKeyBindings() {
    register(KeyCommand.init().noBlock().property("key.jump").event("player.jump"));
    register(KeyCommand.init().noBlock().property("key.left").event("player.left"));
    register(KeyCommand.init().noBlock().property("key.right").event("player.right"));
  }
}
