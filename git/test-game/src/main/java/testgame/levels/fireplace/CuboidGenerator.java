package testgame.levels.fireplace;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joml.Vector3f;

import folkforms.log.Log;
import folkforms.textio.TextIO;

public class CuboidGenerator {

  private static String templateFile = "src/main/resources/fireplace/cuboid-placeholder.obj";
  private static String materialFile = "src/main/resources/fireplace/colours.mtl";
  private static String outputFolder = "src/main/resources/fireplace/temp";
  private static Map<String, String> refAndFileMap = new HashMap<>();
  private static int index = 0;
  private static float scale = 0.01f;

  public static List<String> listRefs() {
    return refAndFileMap.keySet().stream().toList();
  }

  public static Map<String, String> generateFiles() throws IOException {
    refAndFileMap.clear();
    clearOutputFolder();

    List<Vector3fPairWithOffset> data = listData();
    data.forEach(d -> {
      try {
        CuboidGenerator.createFile(d);
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    });
    return refAndFileMap;
  }

  private static void clearOutputFolder() {
    File[] files = new File(outputFolder).listFiles();
    Arrays.stream(files).forEach(f -> {
      if (!f.getName().endsWith(".keep")) {
        f.delete();
      }
    });
  }

  public static void createFile(Vector3fPairWithOffset vPair) throws IOException {
    TextIO textIo = new TextIO();
    String output = textIo.readAsString(templateFile);
    Vector3f vector1 = vPair.getVector1();
    Vector3f vector2 = vPair.getVector2();
    Vector3f offset = vPair.getOffset();
    Vector3f v1 = new Vector3f((vector1.x + offset.x) * scale, (vector1.y + offset.y) * scale,
        (vector1.z + offset.z) * scale);
    Vector3f v2 = new Vector3f((vector2.x + offset.x) * scale, (vector2.y + offset.y) * scale,
        (vector2.z + offset.z) * scale);
    output = output.replace("{v1.x}", Float.toString(v1.x));
    output = output.replace("{v1.y}", Float.toString(v1.y));
    output = output.replace("{v1.z}", Float.toString(v1.z));
    output = output.replace("{v2.x}", Float.toString(v2.x));
    output = output.replace("{v2.y}", Float.toString(v2.y));
    output = output.replace("{v2.z}", Float.toString(v2.z));
    String ref = String.format("gen-cuboid-%s", index);
    output = output.replace("{ref}", ref);
    output = output.replace("{material}", vPair.getMaterial());
    String outputFilename = outputFolder + "/" + ref + ".obj";
    textIo.write(output, outputFilename);
    Log.temp("Wrote file %s", outputFilename);

    createMaterialFile(ref);

    refAndFileMap.put(ref, outputFilename);
    index++;
  }

  private static void createMaterialFile(String ref) throws IOException {
    String outputFilename = outputFolder + "/" + ref + ".mtl";
    TextIO textIo = new TextIO();
    String output = textIo.readAsString(materialFile);
    textIo.write(output, outputFilename);
  }

  private static List<Vector3fPairWithOffset> listData() {
    List<Vector3fPairWithOffset> data = new ArrayList<>();
    data.addAll(createOslo44(0));
    data.addAll(createOslo48(1600));
    return data;
  }

  private static List<Vector3fPairWithOffset> createOslo44(float offsetX) {
    float stoveWidth = 424;
    float stoveHeight = 575;
    float stoveDepth = 370;
    float chamberWidth = 685;
    float chamberHeight = 840;
    float legWidth = 170;
    float legDepth = 160;
    float topLegHeight = 198;
    float hearthWidth = 1117;
    float hearthHeight = 65;
    float hearthDepth = 508;
    float mantleWidth = 1120;
    float mantleHeight = 20;
    float mantleDepth = 210;
    float surroundWidth = 1025;
    return createFireplace(stoveWidth, stoveHeight, stoveDepth, chamberWidth, chamberHeight,
        legWidth, legDepth, topLegHeight, hearthWidth, hearthHeight, hearthDepth, mantleWidth,
        mantleHeight, mantleDepth, surroundWidth, offsetX);
  }

  private static List<Vector3fPairWithOffset> createOslo48(float offsetX) {
    float stoveWidth = 424;
    float stoveHeight = 575;
    float stoveDepth = 370;
    float chamberWidth = 786;
    float chamberHeight = 840;
    float legWidth = 170;
    float legDepth = 160;
    float topLegHeight = 198;
    float hearthWidth = 1372;
    float hearthHeight = 65;
    float hearthDepth = 508;
    float mantleWidth = 1220;
    float mantleHeight = 20;
    float mantleDepth = 210;
    float surroundWidth = 1127;
    return createFireplace(stoveWidth, stoveHeight, stoveDepth, chamberWidth, chamberHeight,
        legWidth, legDepth, topLegHeight, hearthWidth, hearthHeight, hearthDepth, mantleWidth,
        mantleHeight, mantleDepth, surroundWidth, offsetX);
  }

  private static List<Vector3fPairWithOffset> createFireplace(float stoveWidth, float stoveHeight,
      float stoveDepth, float chamberWidth, float chamberHeight, float legWidth, float legDepth,
      float topLegHeight, float hearthWidth, float hearthHeight, float hearthDepth,
      float mantleWidth, float mantleHeight, float mantleDepth, float surroundWidth,
      float offsetX) {

    float chamberDepth = 370;
    float mantleOverhang = (mantleWidth - surroundWidth) / 2;
    float stoveX = mantleOverhang + legWidth + chamberWidth / 2 - stoveWidth / 2;
    float surroundHeight = hearthHeight + chamberHeight + topLegHeight;
    float wallHeight = 2310;
    float chimneyBreastWidth = 1425;
    float chimneyBreastLowerPartWidth = (chimneyBreastWidth - surroundWidth) / 2;
    float mantleX = (chimneyBreastWidth - mantleWidth) / 2;
    float hearthX = (chimneyBreastWidth - hearthWidth) / 2;
    return List.of( //
        // Leg 1
        new Vector3fPairWithOffset(0, 0, 0, legWidth, chamberHeight, legDepth,
            offsetX + mantleX + mantleOverhang, hearthHeight, 0, "white"), //
        // Leg 2
        new Vector3fPairWithOffset(0, 0, 0, legWidth, chamberHeight, legDepth,
            offsetX + mantleX + mantleOverhang + legWidth + chamberWidth, hearthHeight, 0, "white"), //
        // Top part (not mantle)
        new Vector3fPairWithOffset(0, 0, 0, surroundWidth, topLegHeight, legDepth,
            offsetX + mantleX + mantleOverhang, hearthHeight + chamberHeight, 0, "white"), //
        // Mantle
        new Vector3fPairWithOffset(0, 0, 0, mantleWidth, mantleHeight, mantleDepth,
            offsetX + mantleX, hearthHeight + chamberHeight + topLegHeight, 0, "white"), //

        // Chamber
        // Left wall
        new Vector3fPairWithOffset(0, 0, 0, 5, chamberHeight, chamberDepth,
            offsetX + mantleX + mantleOverhang + legWidth - 5, hearthHeight, -chamberDepth,
            "grey70"), //
        // Right wall
        new Vector3fPairWithOffset(0, 0, 0, 5, chamberHeight, -chamberDepth,
            offsetX + mantleX + mantleOverhang + legWidth + chamberWidth, hearthHeight, 0,
            "grey70"), //
        // Rear wall
        new Vector3fPairWithOffset(0, 0, 0, chamberWidth, chamberHeight, 5,
            offsetX + mantleX + mantleOverhang + legWidth, hearthHeight, -chamberDepth - 5,
            "grey70"), //
        // Ceiling
        new Vector3fPairWithOffset(0, 0, 0, chamberWidth, 5, -chamberDepth,
            offsetX + mantleX + mantleOverhang + legWidth, hearthHeight + chamberHeight, 0,
            "grey70"), //
        // Floor
        new Vector3fPairWithOffset(0, 0, 0, chamberWidth, -5, -chamberDepth,
            offsetX + mantleX + mantleOverhang + legWidth, hearthHeight, 0, "grey70"), //

        // Stove
        new Vector3fPairWithOffset(0, 0, 0, stoveWidth, stoveHeight, stoveDepth,
            offsetX + mantleX + stoveX, hearthHeight, -chamberDepth + 100, "white"), //

        // Hearth
        new Vector3fPairWithOffset(0, 0, 0, hearthWidth, hearthHeight, hearthDepth,
            offsetX + hearthX, 0, 0, "black"), //

        // Chimney breast top
        new Vector3fPairWithOffset(0, 0, 0, chimneyBreastWidth, wallHeight - surroundHeight, 5,
            offsetX, surroundHeight, -5, "grey50"),
        // Chimney breast left
        new Vector3fPairWithOffset(0, 0, 0, chimneyBreastLowerPartWidth, surroundHeight, 5, offsetX,
            0, -5, "grey50"),
        // Chimney breast left
        new Vector3fPairWithOffset(0, 0, 0, chimneyBreastLowerPartWidth, surroundHeight, 5,
            offsetX + chimneyBreastWidth - chimneyBreastLowerPartWidth, 0, -5, "grey50"));
  }
}
