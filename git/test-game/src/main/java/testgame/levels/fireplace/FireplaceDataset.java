package testgame.levels.fireplace;

import java.io.IOException;
import java.util.Map;

import game1.core.graphics.Drawable;
import game1.core.pipelines.GraphicsPipelineName;
import game1.core.resources.loaders.DrawableLoader;
import game1.datasets.GameDataFactory;
import game1.filesystem.FileSystem;

public class FireplaceDataset {

  public static final String TAG = "Level3DDataset";
  private FileSystem fileSystem;

  public FireplaceDataset() throws IOException {
    loadFireplace();
  }

  private void loadFireplace() throws IOException {
    Map<String, String> refAndFileMap = CuboidGenerator.generateFiles();
    refAndFileMap.entrySet().forEach(e -> create(e.getKey(), e.getValue()));
  }

  private void create(String ref, String filename) {
    GameDataFactory.getInstance().addSupplier(ref, () -> {
      try {
        Drawable drawable = DrawableLoader.init(GraphicsPipelineName.SCENE_3D).model(filename)
            .load(fileSystem);
        return drawable;
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    });
  }
}
