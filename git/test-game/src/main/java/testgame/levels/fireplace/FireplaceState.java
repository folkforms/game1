package testgame.levels.fireplace;

import java.util.List;

import org.joml.Vector3f;

import game1.core.engine.Engine;
import game1.core.engine.userstate.UserState;
import game1.core.engine.userstate.UserStateHelper;
import game1.core.graphics.Animation3D;
import game1.core.graphics.Colour;
import game1.core.graphics.Drawable;
import game1.core.graphics.lights.DirectionalLight;
import game1.core.input.Mouse;
import game1.datasets.GameDataFactory;
import game1.events.EventsFactory;
import game1.events.Game1Events;
import game1.toolkit.actors.Player3D;
import testgame.devtools.TestGameDevTools;

public class FireplaceState extends UserState {

  public static DirectionalLight DIRECTIONAL_LIGHT;
  public static Animation3D ANIMATED_3D_MODEL;
  public static Drawable TEST_2D_RESIZE;

  @Override
  public List<String> listRequiredTags() {
    return List.of(FireplaceDataset.TAG);
  }

  @Override
  public void apply() {
    UserStateHelper.setKeyBindings("FireplaceKeyBindings", "CommonKeyBindings",
        "AdjusterKeyBindings", "DirectionalLightKeyBindings");
    UserStateHelper.setMouse(Mouse.MOUSE_CAMERA);

    Player3D player = new Player3D(-14, 8.5f, 23, 20, 36, 0);
    EventsFactory.getInstance().raise(Game1Events.CAMERA_SET_POSITION_V3F, player.getPosition());
    EventsFactory.getInstance().raise(Game1Events.CAMERA_SET_ROTATION_V3F, player.getRotation());
    Engine.GAME_STATE.add3dActor(player);

    Engine.GAME_STATE
        .add3dActor(DirectionalLight.create(Colour.fromHex("#fff"), new Vector3f(0, 1, 1), 0.5f));

    addFireplace();

    TestGameDevTools.addActors();
  }

  private void addFireplace() {
    CuboidGenerator.listRefs()
        .forEach(ref -> Engine.GAME_STATE.add3dActor(GameDataFactory.getInstance().get(ref)));
  }
}
