package testgame.levels.fireplace;

import game1.core.engine.internal.CameraFactory;
import game1.core.input.KeyBindings;
import game1.core.input.KeyCommand;
import game1.core.input.Keys;
import game1.events.Command;
import game1.events.EventSubscriptionsFactory;
import game1.events.Game1Events;
import game1.toolkit.events.ToolkitEvents;

public class FireplaceKeyBindings extends KeyBindings {

  private float movementSpeed = 0.05f;
  private int fastMovementMultiplier = 5;
  private float rotationSpeed = 0.4f;

  public FireplaceKeyBindings() {
    EventSubscriptionsFactory.getInstance().subscribe(this);

    /*
     * Note: You can use camera.setPosition, e.g. "Vector3f v = DrawThread.camera.getPosition();
     * DrawThread.camera.setPosition(v.x, v.y, v.z - cameraSpeed);" to rotate the player's head
     * without rotating the body.
     */

    register(KeyCommand.init().keys(Keys.KEY_W).noBlock().event(ToolkitEvents.PLAYER_MOVE, 0, 0,
        -movementSpeed));
    register(KeyCommand.init().keys(Keys.KEY_S).noBlock().event(ToolkitEvents.PLAYER_MOVE, 0, 0,
        movementSpeed));
    register(KeyCommand.init().keys(Keys.KEY_A).noBlock().event(ToolkitEvents.PLAYER_MOVE,
        -movementSpeed, 0, 0));
    register(KeyCommand.init().keys(Keys.KEY_D).noBlock().event(ToolkitEvents.PLAYER_MOVE,
        movementSpeed, 0, 0));
    register(KeyCommand.init().keys(Keys.KEY_SPACE).noBlock().event(ToolkitEvents.PLAYER_MOVE, 0,
        movementSpeed, 0));
    register(KeyCommand.init().keys(Keys.KEY_C).noBlock().event(ToolkitEvents.PLAYER_MOVE, 0,
        -movementSpeed, 0));

    register(KeyCommand.init().keys(Keys.KEY_LEFT_SHIFT, Keys.KEY_W).noBlock()
        .event(ToolkitEvents.PLAYER_MOVE, 0, 0, -movementSpeed * fastMovementMultiplier));
    register(KeyCommand.init().keys(Keys.KEY_LEFT_SHIFT, Keys.KEY_S).noBlock()
        .event(ToolkitEvents.PLAYER_MOVE, 0, 0, movementSpeed * fastMovementMultiplier));
    register(KeyCommand.init().keys(Keys.KEY_LEFT_SHIFT, Keys.KEY_A).noBlock()
        .event(ToolkitEvents.PLAYER_MOVE, -movementSpeed * fastMovementMultiplier, 0, 0));
    register(KeyCommand.init().keys(Keys.KEY_LEFT_SHIFT, Keys.KEY_D).noBlock()
        .event(ToolkitEvents.PLAYER_MOVE, movementSpeed * fastMovementMultiplier, 0, 0));
    register(KeyCommand.init().keys(Keys.KEY_LEFT_SHIFT, Keys.KEY_SPACE).noBlock()
        .event("player.move", 0, movementSpeed * fastMovementMultiplier, 0));
    register(KeyCommand.init().keys(Keys.KEY_LEFT_SHIFT, Keys.KEY_C).noBlock()
        .event(ToolkitEvents.PLAYER_MOVE, 0, -movementSpeed * fastMovementMultiplier, 0));

    register(KeyCommand.init().keys(Keys.KEY_Q).noBlock().event(Game1Events.MOUSE_MOVE, 0,
        -rotationSpeed, 0));
    register(KeyCommand.init().keys(Keys.KEY_E).noBlock().event(Game1Events.MOUSE_MOVE, 0,
        rotationSpeed, 0));

    // Select item under crosshairs
    register(KeyCommand.init().command(new Command() {
      @Override
      public void execute() {
        CameraFactory.getInstance().selectDrawable();
      }
    }).keys(Keys.KEY_P));
  }
}
