package testgame.levels.fireplace;

import org.joml.Vector3f;

public class Vector3fPairWithOffset {

  private Vector3f vector1;
  private Vector3f vector2;
  private Vector3f vector3;
  private String material;

  public Vector3fPairWithOffset(float x1, float y1, float z1, float x2, float y2, float z2,
      float ox, float oy, float oz, String material) {
    vector1 = new Vector3f(x1, y1, z1);
    vector2 = new Vector3f(x2, y2, z2);
    vector3 = new Vector3f(ox, oy, oz);
    this.material = material;
  }

  public Vector3fPairWithOffset(float x1, float y1, float z1, float x2, float y2, float z2,
      String material) {
    this(x1, y1, z1, x2, y2, z2, 0, 0, 0, material);
  }

  public Vector3f getVector1() {
    return vector1;
  }

  public Vector3f getVector2() {
    return vector2;
  }

  public Vector3f getOffset() {
    return vector3;
  }

  public String getMaterial() {
    return material;
  }
}
