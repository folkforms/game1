package testgame.menu;

import game1.core.engine.Engine;
import game1.events.Command;
import testgame.main.TGStates;

public class ReturnToMainMenuCommand implements Command {

  @Override
  public void execute() {
    Engine.GAME_STATE.moveToState(TGStates.MAIN_MENU);
  }
}
