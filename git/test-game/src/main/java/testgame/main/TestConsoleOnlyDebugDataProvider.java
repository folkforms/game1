package testgame.main;

import java.util.List;

import game1.debug.dump_data.DebugDataProvider;

public class TestConsoleOnlyDebugDataProvider implements DebugDataProvider {

  @Override
  public String debug_getDumpFilename() {
    return "TestConsoleOnlyDebugDataProvider";
  }

  @Override
  public List<String> debug_listData() {
    return List.of("aaa", "bbb");
  }
}
