package testgame.main;

import java.io.IOException;
import java.util.List;

import folkforms.log.Log;
import game1.core.engine.Engine;
import game1.debug.dump_data.DebugDataProviderRegistry;
import game1.toolkit.cutscenes.FastForwardKeyBindings;
import game1.toolkit.debugtools.AdjusterKeyBindings;
import game1.toolkit.redoctopus.loadingscreen.RedOctopusLoadingScreenDataset2;
import game1.toolkit.redoctopus.loadingscreen.RedOctopusLoadingScreenState2;
import game1.toolkit.redoctopus.splashscreen.RedOctopusSplashScreenDataset2;
import game1.toolkit.redoctopus.splashscreen.RedOctopusSplashScreenState2;
import game1.toolkit.screens.optionsmenu.OptionsMenuDataset;
import testgame.levels.cutscene3d.Cutscene3DAutoState;
import testgame.levels.cutscene3d.Cutscene3DDataset;
import testgame.levels.cutscene3d.Cutscene3DPrepState;
import testgame.levels.fireplace.FireplaceDataset;
import testgame.levels.fireplace.FireplaceKeyBindings;
import testgame.levels.fireplace.FireplaceState;
import testgame.levels.level1.Level1Dataset;
import testgame.levels.level1.Level1LoadingScreenDataset;
import testgame.levels.level1.Level1LoadingScreenState;
import testgame.levels.level1.Level1State;
import testgame.levels.level2d.Level2DDataset;
import testgame.levels.level2d.Level2DKeyBindings;
import testgame.levels.level2d.Level2DState;
import testgame.levels.level3d.DirectionalLightKeyBindings;
import testgame.levels.level3d.Level3DDataset;
import testgame.levels.level3d.Level3DKeyBindings;
import testgame.levels.level3d.Level3DState;
import testgame.levels.mainmenu.TGMainMenuDataset;
import testgame.levels.mainmenu.TGMainMenuKeyBindings;
import testgame.levels.mainmenu.TGMainMenuState;
import testgame.levels.pigwhistle.PigwhistleDataset;
import testgame.levels.pigwhistle.PigwhistleState;
import testgame.levels.resolution_tests.ResolutionTest1Dataset;
import testgame.levels.resolution_tests.ResolutionTest1State;
import testgame.levels.resolution_tests.ResolutionTest2Dataset;
import testgame.levels.resolution_tests.ResolutionTest2State;
import testgame.levels.resolution_tests.ResolutionTest3Dataset;
import testgame.levels.resolution_tests.ResolutionTest3State;
import testgame.levels.resolution_tests.ResolutionTest4Dataset;
import testgame.levels.resolution_tests.ResolutionTest4State;
import testgame.levels.shared.CommonKeyBindings;
import testgame.levels.stresstest2d.StressTest2DDataset;
import testgame.levels.stresstest2d.StressTest2DState;
import testgame.levels.stresstest3d.StressTest3DDataset;
import testgame.levels.stresstest3d.StressTest3DState;

public class TGMain {

  public static void main(String[] args) throws IOException {
    try {
      Engine engine = new Engine("--warn-on-external-paths", /* "--debug=State", */
          "--log-file=test-game.log");
      engine.applyVariables("src/main/resources/properties/test-game.yaml");

      Engine.KEY_BINDINGS.register("TGMainMenuKeyBindings", new TGMainMenuKeyBindings());
      Engine.KEY_BINDINGS.register("CommonKeyBindings", new CommonKeyBindings());
      Engine.KEY_BINDINGS.register("Level2DKeyBindings", new Level2DKeyBindings());
      Engine.KEY_BINDINGS.register("Level3DKeyBindings", new Level3DKeyBindings());
      Engine.KEY_BINDINGS.register("DirectionalLightKeyBindings",
          new DirectionalLightKeyBindings());
      Engine.KEY_BINDINGS.register("AdjusterKeyBindings", new AdjusterKeyBindings());
      Engine.KEY_BINDINGS.register("FastForwardKeyBindings", new FastForwardKeyBindings());
      Engine.KEY_BINDINGS.register("FireplaceKeyBindings", new FireplaceKeyBindings());

      new RedOctopusSplashScreenDataset2();
      new RedOctopusLoadingScreenDataset2();
      new TGMainMenuDataset();
      OptionsMenuDataset.build(ZIndex.OPTIONS_MENU);
      new Level1LoadingScreenDataset();
      new Level1Dataset();
      new Level2DDataset();
      new Level3DDataset();
      new StressTest2DDataset();
      new StressTest3DDataset();
      new PigwhistleDataset();
      new Cutscene3DDataset();
      new ResolutionTest1Dataset();
      new ResolutionTest2Dataset();
      new ResolutionTest3Dataset();
      new ResolutionTest4Dataset();
      new FireplaceDataset();

      Engine.GAME_FILES.setPersistent(TGMainMenuDataset.TAG);
      Engine.GAME_FILES.setPersistent(OptionsMenuDataset.TAG);

      Engine.GAME_STATE.register(RedOctopusSplashScreenState2.RED_OCTOPUS_SPLASH_SCREEN_STATE,
          new RedOctopusSplashScreenState2());
      Engine.GAME_STATE.register(RedOctopusLoadingScreenState2.RED_OCTOPUS_LOADING_SCREEN_STATE,
          new RedOctopusLoadingScreenState2(List.of(TGMainMenuDataset.TAG), TGStates.MAIN_MENU));
      Engine.GAME_STATE.register(TGStates.MAIN_MENU, new TGMainMenuState());
      Engine.GAME_STATE.register(TGStates.LEVEL_1_LOADING_SCREEN, new Level1LoadingScreenState());
      Engine.GAME_STATE.register(TGStates.LEVEL_1, new Level1State());
      Engine.GAME_STATE.register(TGStates.LEVEL_2D, new Level2DState());
      Engine.GAME_STATE.register(TGStates.LEVEL_3D_LOADING_SCREEN,
          new RedOctopusLoadingScreenState2(List.of("Level3DDataset"), TGStates.LEVEL_3D));
      Engine.GAME_STATE.register(TGStates.LEVEL_3D, new Level3DState());
      Engine.GAME_STATE.register(TGStates.STRESS_TEST_2D, new StressTest2DState());
      Engine.GAME_STATE.register(TGStates.STRESS_TEST_3D, new StressTest3DState());
      Engine.GAME_STATE.register(TGStates.PIGWHISTLE, new PigwhistleState());
      Engine.GAME_STATE.register(TGStates.CUTSCENE_3D_AUTO, new Cutscene3DAutoState());
      Engine.GAME_STATE.register(TGStates.CUTSCENE_3D_PREP, new Cutscene3DPrepState());
      Engine.GAME_STATE.register(TGStates.RESOLUTION_TEST_1, new ResolutionTest1State());
      Engine.GAME_STATE.register(TGStates.RESOLUTION_TEST_2, new ResolutionTest2State());
      Engine.GAME_STATE.register(TGStates.RESOLUTION_TEST_3, new ResolutionTest3State());
      Engine.GAME_STATE.register(TGStates.RESOLUTION_TEST_4, new ResolutionTest4State());
      Engine.GAME_STATE.register(TGStates.FIREPLACE, new FireplaceState());

      Engine.MIXER.registerChannel("effects");
      Engine.MIXER.registerChannel("music");

      DebugDataProviderRegistry.register(new TestConsoleOnlyDebugDataProvider(),
          DebugDataProviderRegistry.Context.CONSOLE);

      engine.start(RedOctopusSplashScreenState2.RED_OCTOPUS_SPLASH_SCREEN_STATE);
    } catch (Exception ex) {
      Log.error(ex);
      System.exit(1);
    }
  }
}
