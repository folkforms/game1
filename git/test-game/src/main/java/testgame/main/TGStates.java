package testgame.main;

public class TGStates {

  public static final String MAIN_MENU = "MAIN_MENU";
  public static final String LEVEL_2D = "LEVEL_2D_TEST";
  public static final String LEVEL_3D_LOADING_SCREEN = "LEVEL_3D_LOADING_SCREEN";
  public static final String LEVEL_3D = "LEVEL_3D_TEST";
  public static final String STRESS_TEST_2D = "STRESS_TEST_2D";
  public static final String STRESS_TEST_3D = "STRESS_TEST_3D";
  public static final String PIGWHISTLE = "PIGWHISTLE";
  public static final String VIEW_OBJECT = "VIEW_OBJECT";
  public static final String CUTSCENE_3D_AUTO = "CUTSCENE_3D_AUTO";
  public static final String CUTSCENE_3D_PREP = "CUTSCENE_3D_PREP";
  public static final String OPTIONS_MENU = "OPTIONS_MENU";
  public static final String REBIND_CONTROLS = "REBIND_CONTROLS";
  public static final String LEVEL_1_LOADING_SCREEN = "LEVEL_1_LOADING_SCREEN";
  public static final String LEVEL_1 = "LEVEL_1";
  public static final String RESOLUTION_TEST_1 = "RESOLUTION_TEST_1";
  public static final String RESOLUTION_TEST_2 = "RESOLUTION_TEST_2";
  public static final String RESOLUTION_TEST_3 = "RESOLUTION_TEST_3";
  public static final String RESOLUTION_TEST_4 = "RESOLUTION_TEST_4";
  public static final String FIREPLACE = "FIREPLACE";
}
