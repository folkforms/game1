package testgame.main;

public interface ZIndex {

  public static final int BACKGROUND = 100;
  public static final int GAME = 1000;
  public static final int PLAYER = 9000;
  public static final int SOLID_PLATFORM = 5000;
  public static final int THIN_PLATFORM = 5000;
  public static final int EXPLOSION = 1100;
  public static final int HEALTH_BAR = 1200;

  public static final int MENU = 10000;
  public static final int MENU_BUTTON = 10001;
  public static final int OPTIONS_MENU = 11000;

  public static final int GAME_OVER_ANIMATION = 2000;
  public static final int PLAYER_DEATH_ANIMATION = 2100;
}
