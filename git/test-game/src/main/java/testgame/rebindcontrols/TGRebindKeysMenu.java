package testgame.rebindcontrols;

import game1.toolkit.screens.rebindkeys.RebindKeysMenu;
import testgame.main.TGStates;
import testgame.main.ZIndex;

public class TGRebindKeysMenu extends RebindKeysMenu {

  public TGRebindKeysMenu() {
    createRebind("Jump", "key.jump", 800);
    createRebind("Left", "key.left", 750);
    createRebind("Right", "key.right", 700);
  }

  @Override
  public String getMainMenuState() {
    return TGStates.MAIN_MENU;
  }

  @Override
  public int getButtonZIndex() {
    return ZIndex.MENU_BUTTON;
  }

  @Override
  public int getRebindMenuZIndex() {
    return ZIndex.MENU;
  }
}
