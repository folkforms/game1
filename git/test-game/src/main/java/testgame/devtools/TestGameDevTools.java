package testgame.devtools;

import game1.core.devtools.DebugToolsMask;
import game1.core.engine.Engine;
import game1.resolutions.layers.ResolutionLayers;
import game1.toolkit.debugtools.Adjuster;
import game1.toolkit.debugtools.CameraCoordinates;
import game1.toolkit.debugtools.DebugAnimationFrames;
import game1.toolkit.debugtools.DebugDatasetsLoaded;
import game1.toolkit.debugtools.DebugFrustumCulling;
import game1.toolkit.debugtools.DebugKeyBindingsDelayRepeat;
import game1.toolkit.debugtools.DebugMouse;
import game1.toolkit.debugtools.DebugShaderTestMode;
import game1.toolkit.debugtools.DebugShowDevKeyBindings;
import game1.toolkit.debugtools.ShowDrawLoopData;
import game1.toolkit.debugtools.ShowPipelineData;

public class TestGameDevTools {

  public static void addActors() {
    Engine.GAME_STATE.addActor(new DebugToolsMask(), ResolutionLayers.WINDOW_LAYER);
    Engine.GAME_STATE.addActor(new DebugMouse(15, 1050), ResolutionLayers.WINDOW_LAYER);
    Engine.GAME_STATE.addActor(new CameraCoordinates(15, 1020), ResolutionLayers.WINDOW_LAYER);
    Engine.GAME_STATE.addActor(new DebugAnimationFrames(15, 990), ResolutionLayers.WINDOW_LAYER);
    Engine.GAME_STATE.addActor(new DebugFrustumCulling(15, 960), ResolutionLayers.WINDOW_LAYER);
    Engine.GAME_STATE.addActor(new DebugShaderTestMode(15, 930), ResolutionLayers.WINDOW_LAYER);
    Engine.GAME_STATE.addActor(new ShowDrawLoopData(15, 855), ResolutionLayers.WINDOW_LAYER);
    Engine.GAME_STATE.addActor(new Adjuster(15, 815), ResolutionLayers.WINDOW_LAYER);
    Engine.GAME_STATE.addActor(new DebugKeyBindingsDelayRepeat(15, 765),
        ResolutionLayers.WINDOW_LAYER);
    Engine.GAME_STATE.addActor(new DebugDatasetsLoaded(600, 750), ResolutionLayers.WINDOW_LAYER);
    Engine.GAME_STATE.addActor(new DebugShowDevKeyBindings(1550, 900),
        ResolutionLayers.WINDOW_LAYER);

    // Engine.GAME_STATE.addActor(new DebugKeyBindingsManager(20, 980));
    // // Engine.GAME_STATE.addActor(new TestDebugEvents(20, 1000));
    // console = new Console();
    // Engine.GAME_STATE.addActor(console);

    Engine.GAME_STATE.addActor(new ShowPipelineData(15, 10), ResolutionLayers.WINDOW_LAYER);

    // Stress test items:
    // Engine.GAME_STATE.addActor(new DebugScreenRandomData(15, 300));
  }
}
