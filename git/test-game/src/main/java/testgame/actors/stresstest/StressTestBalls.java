package testgame.actors.stresstest;

import org.joml.Vector3f;

import game1.actors.Actor;
import game1.actors.Tickable;
import game1.core.graphics.Drawable;
import game1.datasets.GameDataFactory;
import game1.resolutions.layers.ResolutionLayersFactory;
import game1.resolutions.shared.ResolutionHints;

public class StressTestBalls implements Actor, Tickable {

  private String resolutionLayer;
  private float[] arraySpeedX, arraySpeedY;
  private int maxBalls = 100_000;
  private int startingBalls = 800;
  private int index = 0;
  private int z = 100;
  private Drawable[] drawables;

  public StressTestBalls(String resolutionLayer) {
    this.resolutionLayer = resolutionLayer;
    arraySpeedX = new float[maxBalls];
    arraySpeedY = new float[maxBalls];
    drawables = new Drawable[startingBalls];
    for (int i = 0; i < startingBalls; i++) {
      createBall();
    }
  }

  private void createBall() {
    Drawable drawable = ((Drawable) GameDataFactory.getInstance().get("stress-test-ball"))
        .getClone();
    arraySpeedX[index] = (float) (20 * Math.random()) - 10;
    arraySpeedY[index] = (float) (20 * Math.random()) - 10;
    drawables[index] = drawable;
    ResolutionHints hints = ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayer)
        .getResolutionHints();
    float scale = hints.getResolutionScale();
    float width = hints.getResolutionWidth() * scale;
    float height = hints.getResolutionHeight() * scale;
    drawables[index].setPosition(width / 2, height / 2, z);
    addChild(drawables[index]);
    index++;
  }

  @Override
  public void onTick() {
    for (int i = 0; i < index; i++) {
      Vector3f position = drawables[i].getPosition();
      position.x += arraySpeedX[i];
      position.y += arraySpeedY[i];
      ResolutionHints hints = ResolutionLayersFactory.getInstance().getLayerByName(resolutionLayer)
          .getResolutionHints();
      float scale = hints.getResolutionScale();
      float width = hints.getResolutionWidth() * scale;
      float height = hints.getResolutionHeight() * scale;
      if (position.x >= width || position.x < 0) {
        arraySpeedX[i] = arraySpeedX[i] * -1;
      }
      if (position.y >= height || position.y < 0) {
        arraySpeedY[i] = arraySpeedY[i] * -1;
      }
    }
  }
}
