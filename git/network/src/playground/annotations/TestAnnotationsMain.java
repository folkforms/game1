package playground.annotations;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TestAnnotationsMain {

  public static void main(String[] args) throws Exception {
    printAnnotatedFieldsAndValues();
    changeAnnotatedFieldsAndValues();
  }

  /**
   * Get fields annotated with '@Transmit' and their values.
   */
  private static void printAnnotatedFieldsAndValues()
      throws IllegalAccessException {

    TestAnnotationsObject obj = new TestAnnotationsObject();

    // Loop through the fields and list all of the annotations, the annotation
    // values and the field values
    Field[] fields = obj.getClass().getDeclaredFields();
    for (int i = 0; i < fields.length; i++) {
      System.out.println(fields[i]);
      Annotation[] annotations = fields[i].getAnnotations();
      if (annotations.length > 0) {
        for (int j = 0; j < annotations.length; j++) {
          System.out.println("  - " + ((Transmit) annotations[j]).value()
              + "    (" + annotations[j] + ")");
          Object contents = fields[i].get(obj);
          System.out
              .println("  - " + contents + "    (" + contents.getClass() + ")");
        }
      } else {
        System.out.println("  - No annotations");
      }
    }
    System.out.println();
    System.out.println("----");
    System.out.println();
  }

  /**
   * Change fields annotated with '@Transmit' to a value taken from an external
   * source.
   */
  private static void changeAnnotatedFieldsAndValues()
      throws IllegalAccessException {

    // This HashMap is our external data source...
    HashMap<String, Object> newValues = new HashMap<>();
    newValues.put("id", "success!");
    newValues.put("x", 99);
    newValues.put("y", 98);
    List<String> newList = new ArrayList<>();
    newList.add("test");
    newList.add("change");
    newList.add("values");
    newList.add("worked!");
    newValues.put("list", newList);

    // Create the object and print its initial state
    TestAnnotationsObject obj = new TestAnnotationsObject();
    System.out.println("Before: " + obj);

    // Loop through fields, and if a field has "@Transmit" then apply the new
    // value from 'newValues' hash map
    Field[] fields = obj.getClass().getDeclaredFields();
    for (int i = 0; i < fields.length; i++) {
      Annotation[] annotations = fields[i].getAnnotations();
      if (annotations.length > 0) {
        for (int j = 0; j < annotations.length; j++) {
          if (annotations[j] instanceof Transmit) {
            String annotationValue = ((Transmit) annotations[j]).value();
            fields[i].set(obj, newValues.get(annotationValue));
          }
        }
      }
    }

    // Print the new object state
    System.out.println("After: " + obj);
  }
}
