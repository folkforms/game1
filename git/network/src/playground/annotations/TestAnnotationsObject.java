package playground.annotations;

import java.util.ArrayList;
import java.util.List;

public class TestAnnotationsObject {

  // Note: Fields must be protected so other classes can access them

  @Transmit("id")
  protected String id = "test";

  @Transmit("x")
  protected int x = 10;

  @Transmit("y")
  protected int y = 15;

  @Transmit("list")
  protected List<String> list = new ArrayList<>();

  // Do not transmit
  protected int z = 20;

  public TestAnnotationsObject() {
    list.add("hello");
    list.add("world");
  }

  public void apply(String newId, int newX, int newY, List<String> newList) {
    this.id = newId;
    this.x = newX;
    this.y = newY;
    this.list = newList;
  }

  @Override
  public String toString() {
    return String.format(
        "TestAnnotationsObject [ id = %s, x = %s, y = %s, list = %s ]", id, x,
        y, list);
  }
}
