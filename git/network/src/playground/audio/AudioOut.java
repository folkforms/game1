package playground.audio;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import game1.network.Message;
import game1.network.ReliableDeliveryOverUDP;
import game1.network.utils.ByteQueue;

/**
 * Plays any Audio that it receives.
 */
public class AudioOut {
  public static void main(String[] args) throws IOException {

    AudioFormat format = new AudioFormat(8000.0f, 16, 1, true, true);
    AudioInputStream audioInputStream = null;
    SourceDataLine sourceDataLine = null;

    ReliableDeliveryOverUDP network = new ReliableDeliveryOverUDP(5000);

    boolean infinite = true;
    try {
      DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, format);
      sourceDataLine = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
      sourceDataLine.open(format);
      sourceDataLine.start();

      while (infinite) {
        // Construct a byte[] of all of the data we have received so far...
        ByteQueue bq = new ByteQueue();
        List<Message> list = network.getIncomingMessages();
        for (Message m : list) {
          bq.addByteArray(m.getBytes());
        }
        byte[] audioData = bq.toByteArray();

        // Play the audio data...
        InputStream byteArrayInputStream = new ByteArrayInputStream(audioData);
        audioInputStream = new AudioInputStream(byteArrayInputStream, format,
            audioData.length / format.getFrameSize());
        int count = 0;
        byte tempBuffer[] = new byte[10000];
        try {
          while ((count = audioInputStream.read(tempBuffer, 0, tempBuffer.length)) != -1) {
            if (count > 0) {
              // Write data to internal buffer of data line, which will send to speaker
              sourceDataLine.write(tempBuffer, 0, count);
            }
          }
        } catch (IOException e) {
          e.printStackTrace();
        }
      }

      // Block and wait for internal buffer of the data line to empty.
      sourceDataLine.drain();
      sourceDataLine.close();

    } catch (LineUnavailableException e) {
      e.printStackTrace();
    }
  }
}
