package playground.audio;

import java.io.IOException;
import java.net.InetSocketAddress;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

import game1.network.Message;
import game1.network.ReliableDeliveryOverUDP;
import game1.network.utils.ByteQueue;

/**
 * Stores any Audio that it hears.
 */
public class AudioIn {

  private static ReliableDeliveryOverUDP network;

  public static void main(String[] args) throws IOException {

    AudioFormat format = new AudioFormat(8000.0f, 16, 1, true, true);
    TargetDataLine microphone;

    try {
      microphone = AudioSystem.getTargetDataLine(format);

      DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
      microphone = (TargetDataLine) AudioSystem.getLine(info);
      microphone.open(format);

      int numBytesRead;
      int CHUNK_SIZE = 1024;
      byte[] data = new byte[microphone.getBufferSize() / 5];
      microphone.start();

      network = new ReliableDeliveryOverUDP(9999);

      boolean infinite = true;
      try {
        while (infinite) {
          // Read data from the microphone, create a message and put it on the outgoing queue
          numBytesRead = microphone.read(data, 0, CHUNK_SIZE);
          ByteQueue bq = new ByteQueue(data);
          Message m = new Message(bq.getByteArray(numBytesRead));
          m.setTarget(new InetSocketAddress("127.0.0.1", 5000));
          network.sendMessage(m);
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
      microphone.close();

    } catch (LineUnavailableException e) {
      e.printStackTrace();
    }
  }

}
