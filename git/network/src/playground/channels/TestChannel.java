package playground.channels;

import java.nio.channels.DatagramChannel;

public class TestChannel {

  public static DatagramChannel CHANNEL;

  public static void main(String[] args) {
    TestChannelGui gui = new TestChannelGui();
    gui.setVisible(true);
  }
}
