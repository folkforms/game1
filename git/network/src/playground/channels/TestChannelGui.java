package playground.channels;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class TestChannelGui extends JFrame implements ActionListener {

  private JTextField connectField, openPortField;
  private JButton connectButton, openPortButton, sendButton, receiveButton;
  private JTextArea infoPanel;

  public TestChannelGui() {
    setTitle("Test Channel");
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    getContentPane().setLayout(new BorderLayout(2, 2));
    getContentPane().add(createButtonsPanel(), BorderLayout.NORTH);
    getContentPane().add(createInfoPanel(), BorderLayout.CENTER);
    setSize(500, 500);
    setLocation(100, 100);
  }

  private Component createButtonsPanel() {
    JPanel panel = new JPanel(new GridLayout(0, 2, 2, 2));
    connectField = new JTextField("127.0.0.1:8008");
    panel.add(connectField);
    connectButton = new JButton("Connect");
    connectButton.addActionListener(this);
    panel.add(connectButton);
    openPortField = new JTextField("8008");
    panel.add(openPortField);
    openPortButton = new JButton("Open Port");
    openPortButton.addActionListener(this);
    panel.add(openPortButton);
    sendButton = new JButton("Send");
    sendButton.addActionListener(this);
    panel.add(sendButton);
    receiveButton = new JButton("Receive");
    receiveButton.addActionListener(this);
    panel.add(receiveButton);
    return panel;
  }

  private Component createInfoPanel() {
    infoPanel = new JTextArea("");
    JScrollPane sp = new JScrollPane(infoPanel);
    return sp;
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    try {
      if (TestChannel.CHANNEL == null) {
        if (event.getSource() == connectButton) {
          // Connect to a remote system (actually just sets Address:Port in the
          // channel object... UDP does not have "connections")
          String connectionString = connectField.getText();
          String[] tokens = connectionString.split(":");
          String address = tokens[0];
          int port = Integer.parseInt(tokens[1]);
          TestChannel.CHANNEL = DatagramChannel.open();
          TestChannel.CHANNEL.connect(new InetSocketAddress(address, port));
          infoPanel.setText(
              infoPanel.getText() + "Connected to " + connectionString + "\n");
        } else if (event.getSource() == openPortButton) {
          // Open a port locally
          TestChannel.CHANNEL = DatagramChannel.open();
          TestChannel.CHANNEL.socket().bind(
              new InetSocketAddress(Integer.parseInt(openPortField.getText())));
          infoPanel.setText(infoPanel.getText() + "Opened port "
              + openPortField.getText() + "\n");
        }
      } else {
        if (event.getSource() == sendButton) {
          // Send data to remote system
          String data = "data-" + System.currentTimeMillis();
          ByteBuffer buf = ByteBuffer.allocate(48);
          buf.clear();
          buf.put(data.getBytes());
          buf.flip();
          TestChannel.CHANNEL.write(buf);
          infoPanel.setText(infoPanel.getText() + "Sent data: " + data + "\n");
        } else if (event.getSource() == receiveButton) {
          // Receive data locally
          ByteBuffer buf = ByteBuffer.allocate(48);
          buf.clear();
          TestChannel.CHANNEL.receive(buf);
          infoPanel.setText(infoPanel.getText() + "Received data: "
              + new String(buf.array()) + "\n");
        }
      }
    } catch (Exception ex) {
      System.out.println(ex.getClass() + ": " + ex.getMessage());
      ex.printStackTrace();
      System.exit(1);
    }
  }
}
