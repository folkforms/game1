package game1.toolkit.modules.voip;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

import game1.network.Message;
import game1.network.utils.ByteQueue;

/**
 * Receives data via the microphone and sends it to a given handler FIXME.
 */
public class VoipPlayer extends TimerTask {

  private AudioFormat format;
  private AudioInputStream audioInputStream;
  private SourceDataLine sourceDataLine;

  /**
   * Specifies how long the playing indicator should remain on screen after the last audio data was
   * received. Should be a reasonable non-zero value to prevent the indicator from flickering if the
   * audio is intermittent.
   */
  private long playingIndicatorTimeout = 500L;
  private long lastPlayTime = 0L;

  private List<byte[]> list = new ArrayList<>();

  public VoipPlayer() throws LineUnavailableException {
    format = new AudioFormat(8000.0f, 16, 1, true, true);
    DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, format);
    sourceDataLine = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
    sourceDataLine.open(format);
    sourceDataLine.start();
  }

  protected void addMessage(Message message) {
    list.add(message.getBytes());
  }

  @Override
  public void run() {

    // Construct a byte[] of all of the data we have received so far...
    ByteQueue bq = new ByteQueue();
    while (list.size() > 0) {
      byte[] bytes = list.remove(0);
      bq.addByteArray(bytes);
    }
    byte[] audioData = bq.toByteArray();

    long now = System.currentTimeMillis();
    long timeSinceLastPlay = now - lastPlayTime;

    if (audioData.length > 0) {
      VoipDisplay.IS_PLAYING = true;
      lastPlayTime = now;

      // Play the audio data...
      InputStream byteArrayInputStream = new ByteArrayInputStream(audioData);
      audioInputStream = new AudioInputStream(byteArrayInputStream, format,
          audioData.length / format.getFrameSize());
      int count = 0;
      byte tempBuffer[] = new byte[10000];
      try {
        while ((count = audioInputStream.read(tempBuffer, 0, tempBuffer.length)) != -1) {
          if (count > 0) {
            // Write data to internal buffer of data line, which will send to speaker
            sourceDataLine.write(tempBuffer, 0, count);
          }
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    } else if (timeSinceLastPlay > playingIndicatorTimeout) {
      VoipDisplay.IS_PLAYING = false;
    }
  }
}
