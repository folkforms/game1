package game1.toolkit.modules.voip;

import java.util.Timer;

import javax.sound.sampled.LineUnavailableException;

import game1.core.actors.Actor;
import game1.core.engine.GameState;
import game1.core.networking.MessageTypes;
import game1.network.MessageHandlers;

/**
 * VoipModule implements a player voice over IP (voip) feature.
 *
 * <p>
 * To use voip, you need to:
 * <ul>
 * <li>Call {@link VoipModule#init(ClientState)} in your {@link ClientState#apply()} method</li>
 * <li>Call <code>addActor(VoipModule.getActor())</code> and
 * <code>removeActor(VoipModule.getActor())</code> as appropriate to show/hide the voip display
 * <li>Call {@link VoipModule#setEnabled(boolean)} to allow/disallow voip at the appropriate
 * times</li>
 * <li>Call {@link VoipModule#setOption(OPTIONS)} to set voip to off/push to talk/always on</li>
 * <li>Add a key binding for {@link PushToTalkCommand}</li>
 * <li>Your game server should call
 * <code>Engine.MESSAGE_HANDLERS.addHandler(MessageTypes.VOIP, new VoipFromPlayerMessageHandler());</code></li>
 * </ul>
 * </p>
 */
public class VoipModule {

  // FIXME I don't need clientState right now but will probably need it when I add push-to-talk
  protected static GameState state;
  protected static VoipDisplay voipDisplay = new VoipDisplay();
  protected static VoipPlayer voipPlayer;
  protected static VoipRecorder voipRecorder;

  protected static enum OPTIONS {
    OFF, PUSH_TO_TALK, ALWAYS_ON
  };

  protected static OPTIONS pushToTalkSetting = OPTIONS.PUSH_TO_TALK;

  protected static boolean enabled = false;

  public static void init(GameState s) throws LineUnavailableException {
    state = s;
    voipPlayer = new VoipPlayer();
    voipRecorder = new VoipRecorder();

    MessageHandlers.addHandler(MessageTypes.VOIP, new VoipBroadcastMessageHandler(voipPlayer));

    // FIXME Experiment with slowing down these threads. It doesn't need to play every packet
    // immediately, it can probably wait 100ms or more. And maybe the same goes for recording.
    Timer timer = new Timer();
    timer.schedule(voipPlayer, 0, 16);
    timer.schedule(voipRecorder, 0, 16);
  }

  public static Actor getActor() {
    return VoipModule.voipDisplay;
  }

  /**
   * Enables or disables voip. This is used to disable it entirely in states where it is not
   * applicable (e.g. before a network connection has been established). This method should not be
   * used to toggle sending data on/off. For that, use FIXME.
   *
   * @param b
   *          if <code>true</code> voip will be enabled, if <code>false</code> it will be disabled
   */
  public static void setEnabled(boolean b) {
    VoipModule.enabled = b;
  }

  public static void setOption(OPTIONS newOption) {
    pushToTalkSetting = newOption;
  }

  protected static void pushToTalkButtonPressed() {
    if (pushToTalkSetting == OPTIONS.PUSH_TO_TALK) {
      voipRecorder.pushToTalkButtonPressed();
    }
  }
}
