package game1.toolkit.modules.voip;

import game1.network.Message;
import game1.network.handlers.MessageHandler;

public class VoipBroadcastMessageHandler implements MessageHandler {

  private VoipPlayer voipPlayer;

  public VoipBroadcastMessageHandler(VoipPlayer voipPlayer) {
    this.voipPlayer = voipPlayer;
  }

  @Override
  public void handle(Message message) {
    voipPlayer.addMessage(message);
  }
}
