package game1.toolkit.modules.voip;

import game1.core.engine.Command;

public class PushToTalkCommand implements Command {

  @Override
  public void execute() {
    VoipModule.pushToTalkButtonPressed();
  }
}
