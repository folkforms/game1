package game1.toolkit.modules.voip;

import java.util.TimerTask;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

import game1.core.networking.MessageTypes;
import game1.network.Message;
import game1.network.players.NetworkPlayers;
import game1.network.utils.ByteQueue;

/**
 * Receives audio data and plays it.
 */
public class VoipRecorder extends TimerTask {

  private TargetDataLine microphone;

  /**
   * Specifies the root-mean-squared volume threshold to start recording at. With the microphone off
   * the average is 1-2, with ambient noise it is 15-25, and when speaking it is in the 25-60 range.
   */
  private int rmsThreshold = 40;

  /**
   * Specifies how long the recording indicator should remain on screen after the recording.
   */
  private long recordingIndicatorTimeout = 500L;
  private long lastRecordingTime = 0L;

  protected long pushToTalkStartTime = 0L;
  protected long pushToTalkTimeout = 100L;

  public VoipRecorder() throws LineUnavailableException {
    AudioFormat format = new AudioFormat(8000.0f, 16, 1, true, true);
    microphone = AudioSystem.getTargetDataLine(format);
    DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
    microphone = (TargetDataLine) AudioSystem.getLine(info);
    microphone.open(format);
    microphone.start();
  }

  @Override
  public void run() {
    if (VoipModule.enabled) {
      if (VoipModule.pushToTalkSetting == VoipModule.OPTIONS.OFF) {
        VoipDisplay.IS_RECORDING = false;
        return;
      }
      if (VoipModule.pushToTalkSetting == VoipModule.OPTIONS.PUSH_TO_TALK
          && System.currentTimeMillis() - pushToTalkStartTime > pushToTalkTimeout) {
        VoipDisplay.IS_RECORDING = false;
        return;
      }

      int numBytesRead;
      int CHUNK_SIZE = 1024;
      byte[] data = new byte[microphone.getBufferSize() / 5];

      // Read data from the microphone, create a message and put it on the outgoing queue
      numBytesRead = microphone.read(data, 0, CHUNK_SIZE);
      ByteQueue bq = new ByteQueue(data);
      byte[] audioData = bq.getByteArray(numBytesRead);

      long timeSinceLastRecording = System.currentTimeMillis() - lastRecordingTime;
      int rms = calculateRMSLevel(audioData);

      if (numBytesRead > 0
          && (rms > rmsThreshold || timeSinceLastRecording < recordingIndicatorTimeout)) {

        ByteQueue voipMessageData = new ByteQueue(1 + audioData.length);
        voipMessageData.addByte(MessageTypes.VOIP);
        voipMessageData.addByteArray(audioData);
        Message message = new Message(voipMessageData.toByteArray());
        NetworkPlayers.sendMessage(message, NetworkPlayers.SERVER);
        VoipDisplay.IS_RECORDING = true;
        lastRecordingTime = timeSinceLastRecording;
      } else {
        VoipDisplay.IS_RECORDING = false;
      }
    }
  }

  /**
   * Calculate the Root Mean Squared volume, which is the average volume level over time.
   *
   * Taken from https://stackoverflow.com/questions/3899585/microphone-level-in-java
   *
   * @param audioData
   *          the audio data to analyse
   * @return the root mean squared volume of the audio data
   */
  private int calculateRMSLevel(byte[] audioData) {
    long lSum = 0;
    for (int i = 0; i < audioData.length; i++)
      lSum = lSum + audioData[i];

    double dAvg = lSum / audioData.length;
    double sumMeanSquare = 0d;

    for (int j = 0; j < audioData.length; j++)
      sumMeanSquare += Math.pow(audioData[j] - dAvg, 2d);

    double averageMeanSquare = sumMeanSquare / audioData.length;

    return (int) (Math.pow(averageMeanSquare, 0.5d) + 0.5);
  }

  public void pushToTalkButtonPressed() {
    pushToTalkStartTime = System.currentTimeMillis();
  }
}
