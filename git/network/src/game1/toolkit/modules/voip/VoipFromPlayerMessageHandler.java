package game1.toolkit.modules.voip;

import game1.network.Message;
import game1.network.handlers.MessageHandler;
import game1.network.players.NetworkPlayers;

public class VoipFromPlayerMessageHandler implements MessageHandler {

  @Override
  public void handle(Message message) {
    // Send message to all players
    // FIXME Do not send voip back to player who originally sent it!
    NetworkPlayers.sendToAll(message);
  }
}
