package game1.toolkit.modules.chat;

import game1.core.graphics.Colour;
import game1.toolkit.old.textarea.OldTextField;

public class ChatInput extends OldTextField {

  public ChatInput() {
    super("", 5, 0, 200, 10000, 400, 50);

    // Make this text field semi-transparent
    COLOR_SELECTED = new Colour(0.45f, 0.45f, 0.45f, 0.5f);
    COLOR_NOT_SELECTED = new Colour(0.3f, 0.3f, 0.3f, 0.5f);
    COLOR_OUTLINE = new Colour(0.5f, 0.5f, 0.5f, 0.5f);
    COLOR_WHITE = new Colour(1, 1, 1, 0.8f);
  }
}
