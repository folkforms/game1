package game1.toolkit.modules.chat;

import game1.network.Message;
import game1.network.handlers.MessageHandler;
import game1.network.utils.ByteQueue;

public class ChatBroadcastMessageHandler implements MessageHandler {

  @Override
  public void handle(Message message) {
    byte[] data = message.getBytes();
    ByteQueue bq = new ByteQueue(data);
    bq.getByte(); // Discard type
    final String text = bq.getString(data.length - 1);
    ChatModule.chatDisplay.addText(text);
  }
}
