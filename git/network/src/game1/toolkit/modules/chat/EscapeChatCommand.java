package game1.toolkit.modules.chat;

import game1.core.engine.Command;

public class EscapeChatCommand implements Command {

  @Override
  public void execute() {
    ChatModule.state.removeActor(ChatModule.chatInput);
    ChatModule.chatInput.onBlur();
  }
}
