package game1.toolkit.modules.chat;

import java.util.ArrayList;
import java.util.List;

import game1.core.actors.Actor;
import game1.toolkit.widgets.label.Label;
import game1.toolkit.widgets.label.LabelBuilder;

public class ChatDisplay implements Actor {

  private int w, h;
  private List<ChatMessage> chatMessages = new ArrayList<>();
  private final int MAX_MESSAGES = 5;
  private final long MAX_MESSAGE_AGE = 5000;
  private Label label;

  public ChatDisplay() {
    this.w = 400;
    this.h = MAX_MESSAGES * 40;
    label = LabelBuilder.init("").position(0, 0, 1).build();
  }

  public void addText(String newText) {
    chatMessages.add(new ChatMessage(newText, System.currentTimeMillis()));
    while (chatMessages.size() > MAX_MESSAGES) {
      chatMessages.remove(0);
    }
    long now = System.currentTimeMillis();
    for (int i = chatMessages.size() - 1; i >= 0; i--) {
      ChatMessage chatMessage = chatMessages.get(i);
      long age = now - chatMessage.getTime();
      if (age > MAX_MESSAGE_AGE) {
        chatMessages.remove(i);
        continue;
      }
    }
    String[] text = new String[chatMessages.size()];
    for (int i = 0; i < chatMessages.size(); i++) {
      text[i] = chatMessages.get(i).getText();
    }
    label.setText(text);
  }

  // @Override
  // public void draw(Graphics g) {
  // long now = System.currentTimeMillis();
  // g.setFontFace(font);
  // g.setFontScale(scale);
  //
  // int textHeight = 35;
  // for (int i = chatMessages.size() - 1; i >= 0; i--) {
  // ChatMessage chatMessage = chatMessages.get(i);
  // long age = now - chatMessage.getTime();
  // if (age > MAX_MESSAGE_AGE) {
  // chatMessages.remove(i);
  // continue;
  // }
  // // Start fading out text if message age > 50% of MAX_MESSAGE_AGE
  // float agePercent = 1 - ((float) age / MAX_MESSAGE_AGE);
  // if (agePercent > 0.5f) {
  // agePercent = 1;
  // }
  // g.setColour(1, 1, 1, agePercent * 2);
  // g.drawString(chatMessage.getText(), x, y + (chatMessages.size() - i - 1) * (textHeight + 5));
  // }
  // }

  private class ChatMessage {
    private String text;
    private long time;

    public ChatMessage(String text, long time) {
      this.text = text;
      this.time = time;
    }

    public String getText() {
      return text;
    }

    public long getTime() {
      return time;
    }
  }
}
