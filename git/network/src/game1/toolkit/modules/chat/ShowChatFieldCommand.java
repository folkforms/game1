package game1.toolkit.modules.chat;

import game1.core.engine.Command;

public class ShowChatFieldCommand implements Command {

  @Override
  public void execute() {
    ChatModule.state.addActor(ChatModule.chatInput);
    ChatModule.chatInput.onFocus();
  }
}
