package game1.toolkit.modules.chat;

import game1.core.networking.MessageTypes;
import game1.network.Message;
import game1.network.handlers.MessageHandler;
import game1.network.players.NetworkPlayers;
import game1.network.utils.ByteQueue;

/**
 * Server will use this handler to receive a message from a player and broadcast it to all players.
 */
public class ChatFromPlayerMessageHandler implements MessageHandler {

  @Override
  public void handle(Message message) {
    // Get player name from incoming slotNumber
    byte[] payload = message.getBytes();
    ByteQueue bq = new ByteQueue(payload);
    bq.getByte(); // Discard type
    int slotNumber = bq.getInt();
    String text = new String(bq.getByteArray(payload.length - 5));
    String playerName = NetworkPlayers.getPlayerName(slotNumber);
    String broadcastText = String.format("%s: %s", playerName, text);

    // Construct broadcast message
    ByteQueue broadcastMessageData = new ByteQueue();
    broadcastMessageData.addByte(MessageTypes.CHAT_BROADCAST);
    broadcastMessageData.addString(broadcastText);
    Message broadcastMessage = new Message(broadcastMessageData.toByteArray());

    // Send message to all players
    NetworkPlayers.sendToAll(broadcastMessage);
  }
}
