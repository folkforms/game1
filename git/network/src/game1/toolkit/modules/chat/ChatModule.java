package game1.toolkit.modules.chat;

import game1.core.actors.Actor;
import game1.core.engine.GameState;
import game1.core.networking.MessageTypes;
import game1.network.MessageHandlers;

/**
 * ChatModule implements a player chat feature.
 *
 * <p>
 * To use the ChatModule, you need to:
 * <ul>
 * <li>Call <code>ChatModule.init(ClientState)</code> in your ClientState.init method</li>
 * <li>Call <code>addActor(ChatModule.getActor())</code> and
 * <code>removeActor(ChatModule.getActor())</code> as appropriate to show/hide the chat window
 * <li>Add a key binding to places where chat is allowed that executes
 * <code>ShowChatFieldCommand</code></li>
 * <li>Your game server should call
 * <code>Engine.MESSAGE_HANDLERS.addHandler(MessageTypes.CHAT_FROM_PLAYER, new ChatFromPlayerMessageHandler());</code></li>
 * </ul>
 * Note that hiding the chat field on ESC and sending the chat message on Enter are both built into
 * the chat field.
 * </p>
 */
public class ChatModule {

  protected static GameState state;
  protected static ChatDisplay chatDisplay = new ChatDisplay();
  protected static ChatInput chatInput = new ChatInput();

  public static void init(GameState s) {
    state = s;
    chatInput.addEnterKeyCommand(new SendChatCommand());
    chatInput.addEscapeKeyCommand(new EscapeChatCommand());
    MessageHandlers.addHandler(MessageTypes.CHAT_BROADCAST, new ChatBroadcastMessageHandler());
  }

  public static Actor getActor() {
    return ChatModule.chatDisplay;
  }
}
