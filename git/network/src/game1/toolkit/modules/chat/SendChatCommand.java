package game1.toolkit.modules.chat;

import game1.core.engine.Command;
import game1.core.networking.MessageTypes;
import game1.network.Message;
import game1.network.players.NetworkPlayers;
import game1.network.utils.ByteQueue;

public class SendChatCommand implements Command {

  @Override
  public void execute() {
    final String text = ChatModule.chatInput.getText();
    ChatModule.state.removeActor(ChatModule.chatInput);
    ChatModule.chatInput.onBlur();
    ChatModule.chatInput.setText("");

    ByteQueue bq = new ByteQueue(1 + 4 + text.length());
    bq.addByte(MessageTypes.CHAT_FROM_PLAYER);
    bq.addInt(NetworkPlayers.SLOT_NUMBER);
    bq.addString(text);
    Message message = new Message(bq.toByteArray());
    NetworkPlayers.sendMessage(message, NetworkPlayers.SERVER);
  }
}
