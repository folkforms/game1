package game1.network;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import folkforms.log.Log;
import game1.network.test.Mitm;
import game1.network.utils.ByteQueue;
import game1.network.utils.IpAddressUtils;
import game1.network.utils.NetworkMessageUtils;

/**
 * FIXME This class should provide reliable delivery over UDP, by adding a header with messageId,
 * sending ACK (acknowledgement) messages back to the source, and resending any outgoing messages
 * until it receives an ACK for that message or it times out.
 *
 * But right now it does nothing other than stick a header that is not actually used onto messages,
 * because reliable delivery is very complicated!
 */
public class ReliableDeliveryOverUDP {

  private List<Message> incomingMessages;

  private int listenPort;
  private NetworkMessageUtils networkMessageUtils;
  private static int MESSAGE_ID = 0;
  private static final byte ACK = 1;
  private static final byte OTHER = 9;
  // private List<ReliableMessage> unacknowledgedOutgoingMessages;
  // private List<Integer> previousIncomingMessages;

  public ReliableDeliveryOverUDP(int listenPort) throws IOException {
    this.listenPort = listenPort;
    if (useMitm()) {
      networkMessageUtils = new Mitm(listenPort);
    } else {
      networkMessageUtils = new NetworkMessageUtils(listenPort);
    }
    incomingMessages = new ArrayList<>();
    // unacknowledgedOutgoingMessages = new ArrayList<>();
    // previousIncomingMessages = new ArrayList<>();
  }

  public void close() throws IOException {
    networkMessageUtils.close();
  }

  public List<Message> getIncomingMessages() {
    // This will put all new messages into 'incomingMessages'
    handleIncomingMessages();

    // Copy array and return it
    List<Message> incoming = new ArrayList<>();
    incoming.addAll(incomingMessages);
    incomingMessages.clear();
    return incoming;
  }

  public void sendMessage(Message m) {
    // Wrap the message in a reliable message
    int messageId = ++MESSAGE_ID;
    ReliableMessage rm = new ReliableMessage(OTHER, messageId, listenPort, m);
    networkMessageUtils.sendMessage(rm.getBytes(), rm.getMessage().getTarget());
    // unacknowledgedOutgoingMessages.add(rm);
  }

  private void handleIncomingMessages() {
    List<Message> messages = networkMessageUtils.getIncomingMessages();
    for (Message m : messages) {
      ReliableMessage reliableMessage = new ReliableMessage(m);
      byte type = reliableMessage.getType();
      int messageId = reliableMessage.getMessageId();

      if (type == ACK) {
        // acknowledge(messageId);
      } else {
        Message message = reliableMessage.getMessage();
        incomingMessages.add(message);
        // sendAck(message.getSource(), reliableMessage.getReplyPort(),
        // reliableMessage.getMessageId());
      }
    }

    // Resend unacknowledged messages
    // long now = System.currentTimeMillis();
    // for (SentReliableMessage sentMessage : unacknowledgedOutgoingMessages) {
    // if (now - sentMessage.getTimeSent() < 10000L) {
    // ReliableMessage rm = sentMessage.getReliableMessage();
    // SocketAddress target = rm.getTarget();
    // networkMessageUtils.sendMessage(rm.getBytes(), target);
    // }
    // }
  }

  // private void acknowledge(int messageId) {
  // for (int i = 0; i < unacknowledgedOutgoingMessages.size(); i++) {
  // if (unacknowledgedOutgoingMessages.get(i).getMessageId() == messageId) {
  // unacknowledgedOutgoingMessages.remove(i);
  // previousIncomingMessages.add(messageId);
  // Log.temp("previousIncomingMessages = %s", previousIncomingMessages);
  // break;
  // }
  // }
  // }

  private void sendAck(SocketAddress source, int replyPort, int messageId) {
    String hostname = IpAddressUtils.getHostname(source);
    SocketAddress replyTarget = new InetSocketAddress(hostname, replyPort);
    ByteQueue bq = new ByteQueue(9);
    bq.addByte(ACK);
    bq.addInt(messageId);
    // Add a dummy port here, so that when decoding the message the header will always be same size
    bq.addInt(0);
    Log.temp("Sending ACK to %s (id=%s)", replyTarget, messageId);
    networkMessageUtils.sendMessage(bq.toByteArray(), replyTarget);
  }

  private boolean useMitm() throws IOException {
    FixmePaths paths = new FixmePaths();
    Properties p = new Properties();
    p.load(new FileReader(new File(paths.rootPath + "/ReliableDeliveryOverUDP.properties")));
    boolean useMitm = Boolean.parseBoolean(p.getProperty("use.mitm"));
    return useMitm;
  }
}
