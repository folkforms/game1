package game1.network;

import java.net.SocketAddress;

import game1.network.utils.ByteQueue;

/**
 * The Message class contains a byte[] (message data) and one of either a source address (where the
 * message came from) or a target address (where the message should be sent to).
 */
public class Message {

  protected byte[] bytes;
  protected SocketAddress source, target;

  public Message(byte[] bytes) {
    this.bytes = bytes;
  }

  public byte[] getBytes() {
    return this.bytes;
  }

  public SocketAddress getSource() {
    return source;
  }

  public void setSource(SocketAddress newSource) {
    this.source = newSource;
  }

  public SocketAddress getTarget() {
    return target;
  }

  public void setTarget(SocketAddress newTarget) {
    this.target = newTarget;
  }

  /**
   * Creates a copy of this message.
   *
   * @return a copy of this message
   */
  public Message copy() {
    ByteQueue bq = new ByteQueue(bytes.length);
    bq.addByteArray(bytes);
    Message messageCopy = new Message(bq.toByteArray());
    messageCopy.setSource(source);
    messageCopy.setTarget(target);
    return messageCopy;
  }

  @Override
  public String toString() {
    if (bytes.length != 0) {
      StringBuffer sbBytes = new StringBuffer();
      StringBuffer sbString = new StringBuffer();
      for (int i = 0; i < bytes.length; i++) {
        sbBytes.append(String.format("%02X", bytes[i])).append(":");
        if (bytes[i] >= 32 && bytes[i] <= 126) {
          sbString.append(new String(new byte[] { bytes[i] }));
        } else {
          sbString.append(String.format("(%s)", bytes[i]));
        }
      }
      String s = sbBytes.toString();
      s = s.substring(0, s.length() - 1); // Remove trailing ":"
      return String.format("Message[source=%s;target=%s;bytes=[%s];ascii=[%s]]", getSource(),
          getTarget(), s, sbString);
    }
    return String.format("Message[source=%s;target=%s;bytes=[empty];ascii=[empty]]", getSource(),
        getTarget());
  }
}
