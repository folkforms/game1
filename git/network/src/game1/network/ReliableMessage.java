package game1.network;

import game1.network.utils.ByteQueue;

/**
 * Adds a byte (type) and an int (ID) to the head of a message when sending so that we can send an
 * acknowledgement to the sender. Also keeps track of the time the message was sent so we can time
 * out if it takes too long.
 */
public class ReliableMessage {

  private byte type;
  private int messageId;
  private int replyPort;
  private Message message;
  private long timeSent;

  public ReliableMessage(byte type, int messageId, int replyPort, Message message) {
    this.type = type;
    this.messageId = messageId;
    this.replyPort = replyPort;
    this.message = message;
    this.timeSent = System.currentTimeMillis();
  }

  public ReliableMessage(Message message) {
    byte[] bytes = message.getBytes();
    ByteQueue bq = new ByteQueue(bytes);
    this.type = bq.getByte();
    this.messageId = bq.getInt();
    this.replyPort = bq.getInt();
    byte[] messageData = bq.getByteArray(bytes.length - 9);
    this.message = new Message(messageData);
    this.message.setSource(message.getSource());
    this.message.setTarget(message.getTarget());
  }

  public byte getType() {
    return type;
  }

  public int getMessageId() {
    return messageId;
  }

  public int getReplyPort() {
    return replyPort;
  }

  public Message getMessage() {
    return message;
  }

  public long getTimeSent() {
    return timeSent;
  }

  public byte[] getBytes() {
    byte[] messageData = message.getBytes();
    ByteQueue bq = new ByteQueue(9 + messageData.length);
    bq.addByte(type);
    bq.addInt(messageId);
    bq.addInt(replyPort);
    bq.addByteArray(messageData);
    return bq.toByteArray();
  }
}
