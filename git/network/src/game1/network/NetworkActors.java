package game1.network;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import game1.core.networking.NetworkActor;

public class NetworkActors {

  private static List<NetworkActor> items = new CopyOnWriteArrayList<>();

  private NetworkActors() {
  }

  public static void register(NetworkActor item) {
    items.add(item);
  }

  public static void unregister(NetworkActor item) {
    items.remove(item);
  }

  public static List<NetworkActor> listNetworkActors() {
    return items;
  }
}
