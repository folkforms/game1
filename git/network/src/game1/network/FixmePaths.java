package game1.network;

import java.net.URL;

/**
 * Manages paths based on environment variables. Defaults to production paths. Set
 * <code>GAME1_FOLDER</code> to override the root path. Note that this value is relative to the
 * folder the main method executes in, i.e. you will typically want to use something like "../core"
 * or "../lib/game1-[version]".
 */
// FIXME This class is an almost carbon copy of Paths.java from game1.core. It needs to be here so
// that ReliableDeliveryOverUDP.java and Mitm.java knows where to look for their property files.
// FIXME I think ultimately network will depend on core, not the other way around, so this class can
// be removed.
public class FixmePaths {

  // Eclipse default
  public String rootPath = "../network";

  public FixmePaths() {
    if (runningInJar()) {
      // Production mode
      rootPath = "game1";
    }

    // We can use GAME1_FOLDER to override the paths. Note that this is relative to the
    // project root (i.e. will typically be "../lib/game1-<version>")
    final String libFolder = System.getenv("GAME1_FOLDER");
    if (libFolder != null) {
      rootPath = libFolder;
    }
  }

  private boolean runningInJar() {
    URL url = FixmePaths.class.getResource("Paths.class");
    return url.toString().startsWith("jar:");
  }
}
