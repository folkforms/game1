package game1.network.test;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import game1.network.Message;
import game1.network.ReliableDeliveryOverUDP;

public class TestReceiveGui extends JFrame implements ActionListener {

  private ReliableDeliveryOverUDP reliableDeliveryOverUDP;
  private JTextField openPortField;
  private JButton openPortButton, clearButton;
  private JTextArea infoPanel;

  public static void main(String[] args) throws IOException {
    TestReceiveGui gui = new TestReceiveGui();
    gui.setVisible(true);
  }

  public TestReceiveGui() {
    setTitle("Test Receive");
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    getContentPane().setLayout(new BorderLayout(2, 2));
    getContentPane().add(createButtonsPanel(), BorderLayout.NORTH);
    getContentPane().add(createInfoPanel(), BorderLayout.CENTER);
    setSize(530, 500);
    setLocation(100, 100);
  }

  private Component createButtonsPanel() {
    JPanel panel = new JPanel(new GridLayout(0, 3, 2, 2));
    openPortField = new JTextField("8009");
    panel.add(openPortField);
    openPortButton = new JButton("Open Port");
    openPortButton.addActionListener(this);
    panel.add(openPortButton);
    clearButton = new JButton("Clear");
    clearButton.addActionListener(this);
    panel.add(clearButton);
    return panel;
  }

  private Component createInfoPanel() {
    infoPanel = new JTextArea("");
    JScrollPane sp = new JScrollPane(infoPanel);
    return sp;
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    try {
      if (event.getSource() == openPortButton) {

        // Open port
        this.reliableDeliveryOverUDP = new ReliableDeliveryOverUDP(
            Integer.parseInt(openPortField.getText()));
        openPortButton.setEnabled(false);

        // Start a task to pull messages off the NMM and print them
        TimerTask checkForMessagesTask = new CheckForMessages();
        new Timer().schedule(checkForMessagesTask, 0, 16);

        infoPanel.setText(
            String.format("%sOpened port %s\n", infoPanel.getText(), openPortField.getText()));
      } else if (event.getSource() == clearButton) {
        infoPanel.setText("");
      }
    } catch (Exception ex) {
      System.out.println(ex.getClass() + ": " + ex.getMessage());
      ex.printStackTrace();
      System.exit(1);
    }
  }

  private class CheckForMessages extends TimerTask {
    @Override
    public void run() {
      List<Message> messages = reliableDeliveryOverUDP.getIncomingMessages();
      for (Message m : messages) {
        infoPanel.setText(String.format("%sReceived: '%s'\n", infoPanel.getText(), m.toString()));
      }
    }
  }

}
