package game1.network.test;

import java.io.IOException;

import game1.network.ReliableDeliveryOverUDP;

public class TestSendAndReceiveGui {

  public static void main(String[] args) throws IOException {
    TestReceiveGui rGui = new TestReceiveGui();
    rGui.setVisible(true);

    ReliableDeliveryOverUDP reliableDeliveryOverUDP = new ReliableDeliveryOverUDP(9999);
    TestSendGui sGui = new TestSendGui(reliableDeliveryOverUDP);
    sGui.setVisible(true);
  }
}
