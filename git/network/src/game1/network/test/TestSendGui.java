package game1.network.test;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetSocketAddress;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import game1.network.Message;
import game1.network.ReliableDeliveryOverUDP;
import game1.network.utils.ByteQueue;

public class TestSendGui extends JFrame implements ActionListener {

  private ReliableDeliveryOverUDP reliableDeliveryOverUDP;
  private JTextField addressField, stringField, bytesField, multipleWithIdField;
  private JCheckBox prefixTypeCheckbox;
  private JTextField prefixTypeField;
  private JButton sendButton, clearButton;
  private JRadioButton stringRadioButton, bytesRadioButton, multipleWithIdRadioButton;
  private JTextArea infoPanel;

  public static void main(String[] args) throws IOException {
    ReliableDeliveryOverUDP reliableDeliveryOverUDP = new ReliableDeliveryOverUDP(9999);
    TestSendGui gui = new TestSendGui(reliableDeliveryOverUDP);
    gui.setVisible(true);
  }

  public TestSendGui(ReliableDeliveryOverUDP reliableDeliveryOverUDP) {
    setTitle("Test Send");
    this.reliableDeliveryOverUDP = reliableDeliveryOverUDP;
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    getContentPane().setLayout(new BorderLayout(2, 2));
    getContentPane().add(createInputPanel(), BorderLayout.NORTH);
    getContentPane().add(createInfoPanel(), BorderLayout.CENTER);
    setSize(530, 500);
    setLocation(650, 100);
  }

  private Component createInputPanel() {
    JPanel panel = new JPanel(new BorderLayout(2, 2));

    JPanel buttonPanel = new JPanel(new GridLayout(0, 3, 2, 2));
    addressField = new JTextField("127.0.0.1:8009");
    buttonPanel.add(addressField);
    sendButton = new JButton("Send");
    sendButton.addActionListener(this);
    buttonPanel.add(sendButton);
    clearButton = new JButton("Clear");
    clearButton.addActionListener(this);
    buttonPanel.add(clearButton);
    panel.add(buttonPanel, BorderLayout.SOUTH);

    JPanel dataPanel = new JPanel(new GridLayout(0, 2, 2, 2));

    stringRadioButton = new JRadioButton("String");
    dataPanel.add(stringRadioButton);
    stringField = new JTextField("127.0.0.1:8888");
    dataPanel.add(stringField);

    prefixTypeCheckbox = new JCheckBox("Prefix byte to string?");
    dataPanel.add(prefixTypeCheckbox);
    prefixTypeField = new JTextField("30");
    dataPanel.add(prefixTypeField);

    bytesRadioButton = new JRadioButton("Bytes");
    dataPanel.add(bytesRadioButton);
    bytesField = new JTextField("32");
    dataPanel.add(bytesField);

    multipleWithIdRadioButton = new JRadioButton("Multiple with ID");
    dataPanel.add(multipleWithIdRadioButton);
    multipleWithIdField = new JTextField("message");
    dataPanel.add(multipleWithIdField);

    ButtonGroup group = new ButtonGroup();
    group.add(stringRadioButton);
    group.add(bytesRadioButton);
    group.add(multipleWithIdRadioButton);

    multipleWithIdRadioButton.setSelected(true);

    panel.add(dataPanel, BorderLayout.CENTER);

    return panel;
  }

  private Component createInfoPanel() {
    infoPanel = new JTextArea("");
    JScrollPane sp = new JScrollPane(infoPanel);
    return sp;
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    try {
      if (event.getSource() == sendButton) {

        // Sort out target address
        String host = null;
        int port = -1;
        String address = addressField.getText();
        if (!address.startsWith("[")) {
          String[] tokens = address.split(":");
          host = tokens[0];
          port = Integer.parseInt(tokens[1]);
        } else {
          String[] tokens = address.split("]:");
          host = tokens[0] + "]";
          port = Integer.parseInt(tokens[1]);
        }

        // Sort out data to send
        byte[] dataToSend = "Should not see this!".getBytes();
        if (stringRadioButton.isSelected() && !prefixTypeCheckbox.isSelected()) {

          dataToSend = stringField.getText().getBytes();

        } else if (stringRadioButton.isSelected() && prefixTypeCheckbox.isSelected()) {

          dataToSend = (new String(new byte[] { Byte.parseByte(prefixTypeField.getText()) })
              + stringField.getText()).getBytes();

        } else if (bytesRadioButton.isSelected()) {
          ByteQueue b = new ByteQueue();
          String text = bytesField.getText();
          String[] bytes = text.split(" ");
          for (String s : bytes) {
            b.addByte(Byte.parseByte(s));
          }
          dataToSend = b.toByteArray();
        } else if (multipleWithIdRadioButton.isSelected()) {
          dataToSend = multipleWithIdField.getText().getBytes();
        }

        // Create message
        Message message = new Message(dataToSend);
        message.setTarget(new InetSocketAddress(host, port));

        // Send the message
        if (!multipleWithIdRadioButton.isSelected()) {
          reliableDeliveryOverUDP.sendMessage(message);
        } else {
          for (int i = 0; i < 50; i++) {
            reliableDeliveryOverUDP.sendMessage(message);
          }
        }

        infoPanel.setText(
            String.format("%sSent: '%s'\n", infoPanel.getText(), message.toString(), address));
      } else if (event.getSource() == clearButton) {
        infoPanel.setText("");
      }
    } catch (Exception ex) {
      System.out.println(ex.getClass() + ": " + ex.getMessage());
      ex.printStackTrace();
      System.exit(1);
    }
  }
}
