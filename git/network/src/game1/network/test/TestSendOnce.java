package game1.network.test;

import java.io.IOException;
import java.net.InetSocketAddress;

import folkforms.log.Log;
import game1.network.Message;
import game1.network.ReliableDeliveryOverUDP;

public class TestSendOnce {

  public static void main(String[] args) throws IOException, InterruptedException {

    if (args.length != 2) {
      Log.error("Expected arguments: <hostname:port> <message text>");
      System.exit(1);
    }

    ReliableDeliveryOverUDP reliableDeliveryOverUDP = new ReliableDeliveryOverUDP(9999);

    // Sort out target address
    String address = args[0];
    String host = null;
    int port = -1;
    if (!address.startsWith("[")) {
      String[] tokens = address.split(":");
      host = tokens[0];
      port = Integer.parseInt(tokens[1]);
    } else {
      String[] tokens = address.split("]:");
      host = tokens[0] + "]";
      port = Integer.parseInt(tokens[1]);
    }

    // Create message
    Message message = new Message(args[1].getBytes());
    message.setTarget(new InetSocketAddress(host, port));

    // Send the message
    Log.info("Sending message %s to %s:%s", message, host, port);
    reliableDeliveryOverUDP.sendMessage(message);

    // Required to kill the Network Message Manager tasks
    Thread.sleep(1000);
    System.exit(0);
  }
}
