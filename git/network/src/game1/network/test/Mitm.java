package game1.network.test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.UnresolvedAddressException;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

import folkforms.log.Log;
import game1.network.Message;
import game1.network.FixmePaths;
import game1.network.utils.NetworkMessageUtils;

/**
 * Simulates an unreliable network by adding packet loss, lag and out-of-order delivery.
 */
public class Mitm extends NetworkMessageUtils {

  private boolean SEND_COPY = false;
  private double PACKET_LOSS_CHANCE = 0;
  private long LAG = 0L;
  private List<LaggedMessage> laggedMessages;
  private double OUT_OF_ORDER_CHANCE = 0;
  private long OUT_OF_ORDER_DELAY = 0L;

  public Mitm(int listenPort) throws IOException {
    super(listenPort);
    loadProperties();
    laggedMessages = new CopyOnWriteArrayList<>();
    Timer timer = new Timer();
    timer.schedule(new LaggedMessageThread(this), 0L, 10L);
  }

  @Override
  public void sendMessage(byte[] bytes, SocketAddress target) {
    // Simulate packet loss by dropping random packets
    if (Math.random() < PACKET_LOSS_CHANCE) {
      return;
    }

    long now = System.currentTimeMillis();

    // Simulate lag by lagging all packets
    if (LAG > 0L) {
      LaggedMessage laggedMessage = new LaggedMessage(bytes, target, now + LAG);
      laggedMessages.add(laggedMessage);
      return;
    }

    // Simulate out of order delivery by randomly lagging some packets
    if (Math.random() < OUT_OF_ORDER_CHANCE) {
      LaggedMessage laggedMessage = new LaggedMessage(bytes, target, now + OUT_OF_ORDER_DELAY);
      laggedMessages.add(laggedMessage);
      return;
    }

    if (SEND_COPY) {
      ByteBuffer buf = ByteBuffer.allocate(bytes.length);
      buf.clear();
      buf.put(bytes);
      buf.flip();
      try {
        super.outgoingChannel.send(buf, new InetSocketAddress("127.0.0.1", 12000));
      } catch (IOException ex) {
        Log.error("Error pushing outgoing message to %s: %s", target, new Message(bytes));
        Log.error(ex);
      } catch (UnresolvedAddressException ex) {
        Log.error("Invalid address: %s (message: %s)", target, new Message(bytes));
        Log.error(ex);
      }
    }

    // Normal
    super.sendMessage(bytes, target);
  }

  /**
   * Version of sendMessage that does not apply any network issues. Used by LaggedMessageThread to
   * send the lagged messages without them running into additional problems.
   *
   * @param bytes
   *          bytes message data
   * @param target
   *          message target
   */
  public void _sendMessage(byte[] bytes, SocketAddress target) {
    super.sendMessage(bytes, target);
  }

  private class LaggedMessage {
    private byte[] bytes;
    private SocketAddress target;
    private long sendTime;

    public LaggedMessage(byte[] bytes, SocketAddress target, long sendTime) {
      this.bytes = bytes;
      this.target = target;
      this.sendTime = sendTime;
    }

    public byte[] getBytes() {
      return bytes;
    }

    public SocketAddress getTarget() {
      return target;
    }

    public long getSendTime() {
      return sendTime;
    }
  }

  private class LaggedMessageThread extends TimerTask {

    private Mitm mitm;

    public LaggedMessageThread(Mitm mitm) {
      this.mitm = mitm;
    }

    @Override
    public void run() {
      for (int i = 0; i < laggedMessages.size(); i++) {
        LaggedMessage m = laggedMessages.get(i);
        if (System.currentTimeMillis() > m.getSendTime()) {
          mitm._sendMessage(m.getBytes(), m.getTarget());
          laggedMessages.remove(i);
          i--;
        }
      }
    }
  }

  private void loadProperties() throws IOException {
    FixmePaths paths = new FixmePaths();
    Properties p = new Properties();
    p.load(new FileReader(new File(paths.rootPath + "/Mitm.properties")));

    SEND_COPY = Boolean.parseBoolean(p.getProperty("send.copy.of.network.messages"));
    PACKET_LOSS_CHANCE = Double.parseDouble(p.getProperty("packet.loss.chance"));
    LAG = Long.parseLong(p.getProperty("lag")) / 2;
    OUT_OF_ORDER_CHANCE = Double.parseDouble(p.getProperty("out.of.order.chance"));
    OUT_OF_ORDER_DELAY = Long.parseLong(p.getProperty("out.of.order.delay"));
  }
}
