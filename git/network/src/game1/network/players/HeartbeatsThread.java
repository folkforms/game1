package game1.network.players;

import java.util.TimerTask;

import folkforms.log.Log;
import game1.core.networking.MessageTypes;
import game1.network.Message;
import game1.network.utils.ByteQueue;

/**
 * Scheduled task to send HEARTBEAK_ASK messages to all connected players, and to kick any players
 * who have exceeded the {@link #TIMEOUT} value.
 */
public class HeartbeatsThread extends TimerTask {

  private static final long TIMEOUT = 5000L;

  @Override
  public void run() {
    long now = System.currentTimeMillis();

    // Remove any players who have stopped responding
    for (int slotNumber = 0; slotNumber < NetworkPlayers.slots.size(); slotNumber++) {
      Player p = NetworkPlayers.slots.get(slotNumber);
      if (p != null) {
        if (NetworkPlayers.heartbeats.started(slotNumber)) {
          long lastHeartbeat = NetworkPlayers.heartbeats.getLastHeartbeatTime(slotNumber);

          if (now - lastHeartbeat > TIMEOUT) {
            Log.info("Removing player from slot #%s due to heartbeat timeout", slotNumber);
            NetworkPlayers.slots.remove(slotNumber);
            NetworkPlayers.heartbeats.remove(slotNumber);

            // There may be other events here... server might notify everyone that player X
            // disconnected, it might stop the game and give them 30 seconds to reconnect, it
            // might abandon the game due to lack of players, etc.

            // FIXME
            // if (Engine.IS_CLIENT) {
            // Engine.moveToState(States.SERVER_TIMEOUT);
            // }
          }
        }
      }
    }

    // Send new heartbeats
    if (NetworkPlayers.SLOT_NUMBER != -1) {
      for (Player p : NetworkPlayers.slots) {
        if (p != null) {
          ByteQueue bq = new ByteQueue();
          bq.addByte(MessageTypes.HEARTBEAT_ASK);
          bq.addInt(NetworkPlayers.SLOT_NUMBER);
          bq.addLong(System.nanoTime());
          Message message = new Message(bq.toByteArray());
          message.setTarget(p.getAddress());
          NetworkPlayers.reliableDeliveryOverUDP.sendMessage(message);
        }
      }
    }
  }
}