package game1.network.players;

import java.io.IOException;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import folkforms.log.Log;
import game1.core.engine.Engine;
import game1.core.networking.MessageTypes;
import game1.core.networking.NetworkActorFactory;
import game1.network.Message;
import game1.network.MessageHandlers;
import game1.network.ReliableDeliveryOverUDP;
import game1.network.handlers.HeartbeatAskMessageHandler;
import game1.network.handlers.HeartbeatOkMessageHandler;
import game1.network.handlers.StateCreateMessageHandler;

public class NetworkPlayers {

  public static int SERVER_LISTEN_PORT;
  public static int CLIENT_LISTEN_PORT;

  /**
   * Slot number for the server in a standard server-client game.
   */
  public static final int SERVER = 0; // Server is slot 0 for clients

  /**
   * Slot number of this client instance. Assigned when connected to a server. Used to identify the
   * client instance.
   */
  public static int SLOT_NUMBER = -1;

  /**
   * Denotes whether a game is currently in progress or not.
   */
  public static boolean NETWORK_GAME_IN_PROGRESS = false;

  /**
   * Used to create new Actors based on data transmitted in a StateCreate message.
   *
   * @see {@link StateCreateMessageHandler}
   */
  public static NetworkActorFactory NETWORK_ACTOR_FACTORY;

  private static int maxSlots = -1;
  private static int minPlayersRequired = -1;
  static List<Player> slots;
  static Heartbeats heartbeats;
  private static Timer timer;
  static ReliableDeliveryOverUDP reliableDeliveryOverUDP;

  public static void preInit(int maxSlots, int minPlayersRequired) throws IOException {
    NetworkPlayers.maxSlots = maxSlots;
    NetworkPlayers.minPlayersRequired = minPlayersRequired;
    NetworkPlayers.slots = new ArrayList<>(maxSlots);
    // Initialise slots as null so the list has a size
    for (int i = 0; i < maxSlots; i++) {
      NetworkPlayers.slots.add(null);
    }

    NetworkPlayers.heartbeats = new Heartbeats(maxSlots);

    MessageHandlers.addHandler(MessageTypes.HEARTBEAT_ASK, new HeartbeatAskMessageHandler());
    MessageHandlers.addHandler(MessageTypes.HEARTBEAT_OK, new HeartbeatOkMessageHandler());

    NetworkPlayers.SERVER_LISTEN_PORT = Engine.PROPERTIES.getIntProperty("server.listen.port");
    NetworkPlayers.CLIENT_LISTEN_PORT = Engine.PROPERTIES.getIntProperty("client.listen.port");
  }

  public static void init(int listenPort) throws IOException {
    NetworkPlayers.NETWORK_GAME_IN_PROGRESS = true;

    // Create reliable delivery object, open listening port, etc.
    reliableDeliveryOverUDP = new ReliableDeliveryOverUDP(listenPort);

    // Start thread to send heartbeats and handle failed heartbeats
    timer = new Timer();
    timer.schedule(new HeartbeatsThread(), 0L, 100L);
  }

  /**
   * Called by clients when they want to close the connections.
   *
   * @throws IOException
   *           if an error occurs closing the connection
   */
  public static void reset() throws IOException {
    // Stop sending/receiving messages
    NetworkPlayers.NETWORK_GAME_IN_PROGRESS = false;

    // Wipe slots
    slots = new ArrayList<>(maxSlots);
    for (int i = 0; i < maxSlots; i++) {
      slots.add(null);
    }

    // Kill heartbeat thread
    timer = null;

    // Wipe heartbeat object
    heartbeats = new Heartbeats(maxSlots);

    // Close UDP connections
    reliableDeliveryOverUDP.close();
    reliableDeliveryOverUDP = null;
  }

  public static List<Message> getIncomingMessages() {
    return reliableDeliveryOverUDP.getIncomingMessages();
  }

  /**
   * Adds a player to the first free slot. Returns the slot number, or <code>-1</code> if there are
   * no free slots.
   *
   * @param socketAddress
   *          player address
   * @param replyPort
   *          player reply port
   * @param name
   *          player name
   * @return the slot number, or <code>-1</code> if there are no free slots
   */
  public static int addPlayer(SocketAddress socketAddress, int replyPort, String name) {
    // Create player object
    Player p = new Player(socketAddress, replyPort, name);

    // Find first free slot
    int firstFreeSlot = -1;
    for (int i = 0; i < slots.size(); i++) {
      if (slots.get(i) == null) {
        firstFreeSlot = i;
        break;
      }
    }

    // If no free slots
    if (firstFreeSlot == -1) {
      return -1;
    }

    // Set their playerNumber and add player to slots
    p.setSlotNumber(firstFreeSlot);
    slots.add(firstFreeSlot, p);
    Log.info("Registered player in slot #%s, name: '%s', address: %s", p.getSlotNumber(),
        p.getPlayerName(), p.getAddress());

    return firstFreeSlot;
  }

  /**
   * Clears player details from a given slot.
   *
   * @param slotNumber
   *          the slot to clear
   */
  public static void removePlayer(int slotNumber) {
    slots.set(slotNumber, null);
    Log.info("Removed player in slot #%s", slotNumber);
  }

  /**
   * Gets the name of the player in the given slot.
   *
   * @param slotNumber
   *          the slot to check
   * @return the name of the player
   */
  public static String getPlayerName(int slotNumber) {
    return slots.get(slotNumber).getPlayerName();
  }

  /**
   * Sends a message to a given player. Clients wishing to send a message to the server should send
   * to {@link NetworkPlayers#SERVER} (slot number 0).
   *
   * @param gm
   *          the message to send
   * @param slotNumber
   *          slot number of the recipient
   */
  public static void sendMessage(Message message, int slotNumber) {
    if (slotNumber < 0 || slotNumber > slots.size() - 1) {
      Log.error("Tried to send message to invalid slot: %s", slotNumber);
      Thread.dumpStack();
      return;
    }

    final Player p = slots.get(slotNumber);
    if (p != null) {
      message.setTarget(p.getAddress());
      reliableDeliveryOverUDP.sendMessage(message);
    }
  }

  /**
   * Sends a message to all connected players.
   *
   * @param message
   *          the message to send
   */
  public static void sendToAll(Message message) {
    for (Player p : slots) {
      if (p != null) {
        Message m2 = message.copy();
        m2.setTarget(p.getAddress());
        reliableDeliveryOverUDP.sendMessage(m2);
      }
    }
  }

  /**
   * Updates the heartbeat data for the player in the given slot.
   *
   * @param slotNumber
   *          the slot number of the player
   * @param timeSent
   *          the time the original heartbeat request was sent at
   */
  public static void updateHeartbeat(int slotNumber, long timeSent) {
    long ping = (System.nanoTime() - timeSent) / 1000000;
    heartbeats.addHeartbeat(slotNumber, ping);
  }

  public static void setReady(int slotNumber, boolean b) {
    for (int i = 0; i < slots.size(); i++) {
      final Player p = slots.get(i);
      if (p != null && p.getSlotNumber() == slotNumber) {
        p.setReady(b);
      }
    }
  }

  public static boolean allPlayersReady() {
    boolean allReady = true;
    for (int i = 0; i < slots.size(); i++) {
      final Player p = slots.get(i);
      if (p != null) {
        allReady &= p.isReady();
      }
    }
    return allReady;
  }

  public static boolean hasEnoughPlayersToStart() {
    int numJoined = 0;
    for (int i = 0; i < slots.size(); i++) {
      final Player p = slots.get(i);
      if (p != null) {
        numJoined++;
      }
    }
    return numJoined >= minPlayersRequired;
  }
}
