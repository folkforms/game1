package game1.network.players;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

import game1.network.utils.IpAddressUtils;

/**
 * Represents a player on the network that we want to send messages to.
 */
class Player {

  private int slotNumber = -1;
  private SocketAddress sendAddress;
  private String hostname;
  private int replyPort;
  private String name;
  private boolean ready = false;

  public Player(SocketAddress socketAddress, int replyPort, String name) {
    this.hostname = IpAddressUtils.getHostname(socketAddress);
    this.replyPort = replyPort;
    this.sendAddress = new InetSocketAddress(this.hostname, this.replyPort);
    this.name = name;
  }

  public int getSlotNumber() {
    return slotNumber;
  }

  public void setSlotNumber(int number) {
    this.slotNumber = number;
  }

  public String getPlayerName() {
    return name;
  }

  public SocketAddress getAddress() {
    return sendAddress;
  }

  public boolean isReady() {
    return ready;
  }

  public void setReady(boolean b) {
    ready = b;
  }
}
