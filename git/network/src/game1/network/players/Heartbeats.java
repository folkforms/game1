package game1.network.players;

import java.util.ArrayList;
import java.util.List;

/**
 * Keeps track of heartbeat values for each slot.
 */
class Heartbeats {

  private List<Heartbeat> heartbeats;

  public Heartbeats(int numPlayers) {
    heartbeats = new ArrayList<>(numPlayers);
    for (int i = 0; i < numPlayers; i++) {
      heartbeats.add(new Heartbeat());
    }
  }

  public void addHeartbeat(int slot, long time) {
    Heartbeat hb = heartbeats.get(slot);
    hb.addHeartbeat(time);

    // Calculate average ping
    // FIXME
    // if (Engine.IS_CLIENT) {
    // Heartbeat heartbeat = heartbeats.get(0);
    // long ping = heartbeat.getAveragePing();
    // Engine.DEBUG_SCREEN.update("heartbeat_ref", "Network ping: %s", ping);
    // }
  }

  public long getLastHeartbeatTime(int slotNumber) {
    Heartbeat hb = heartbeats.get(slotNumber);
    return hb.getLastHeartbeatTime();
  }

  public void remove(int i) {
    heartbeats.set(i, null);
  }

  public boolean started(int slotNumber) {
    Heartbeat hb = heartbeats.get(slotNumber);
    return hb.started();
  }

  private class Heartbeat {

    private long lastHeartbeatTime = -1L;
    private static final int numHeartbeats = 10;
    private List<Long> values = new ArrayList<>();

    public Heartbeat() {
    }

    public long getAveragePing() {
      long totalPing = 0;
      for (int i = 0; i < values.size(); i++) {
        totalPing += values.get(i);
      }
      long averagePing = totalPing / numHeartbeats;
      return averagePing;
    }

    public boolean started() {
      return values.size() > 0;
    }

    public void addHeartbeat(long time) {
      values.add(time);
      if (values.size() > numHeartbeats) {
        values.remove(0);
      }
      lastHeartbeatTime = System.currentTimeMillis();
    }

    public long getLastHeartbeatTime() {
      return lastHeartbeatTime;
    }
  }
}
