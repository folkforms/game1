package game1.network.debug;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import game1.network.ReliableDeliveryOverUDP;

/**
 * Listens for messages on a given port.
 */
// FIXME This needs to be moved to its own project I think. It can't live in network. It needs to
// live above everything, even Pong.
public class MessageListener {

  private int listenPort = 12000;
  private ReliableDeliveryOverUDP source;
  private Timer timer = new Timer();
  private TimerTask listenTask;

  public static List<CapturedMessage> CAPTURED_MESSAGES = new ArrayList<>(10000);

  public MessageListener() throws IOException {
    source = new ReliableDeliveryOverUDP(listenPort);

    listenTask = new ListenTask(source);
    timer.schedule(listenTask, 0, 500);
  }

  public static void main(String[] args) throws IOException {
    new MessageListener();
  }
}
