package game1.network.debug;

import game1.network.Message;

public class CapturedMessage {

  private long time;
  private Message message;

  public CapturedMessage(Message message) {
    this.time = System.nanoTime();
    this.message = message;
  }

  public long getTime() {
    return time;
  }

  public Message getMessage() {
    return message;
  }

  public byte getType() {
    return message.getBytes()[0];
  }
}
