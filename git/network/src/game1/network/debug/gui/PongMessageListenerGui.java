package game1.network.debug.gui;

import java.awt.BorderLayout;
import java.io.IOException;

import javax.swing.JFrame;

import game1.network.debug.MessageListener;

public class PongMessageListenerGui extends JFrame {

  public PongMessageListenerGui() {
    setTitle("Pong Message Listener Gui");
    setLocation(100, 100);
    setSize(500, 500);
    setDefaultCloseOperation(EXIT_ON_CLOSE);

    setLayout(new BorderLayout(2, 2));
    getContentPane().add(new MessageCounter(), BorderLayout.WEST);
    getContentPane().add(new Filters(), BorderLayout.NORTH);
    getContentPane().add(new MessageList(), BorderLayout.CENTER);
    getContentPane().add(new MessageDetail(), BorderLayout.SOUTH);
  }

  public static void main(String[] args) throws IOException {
    new MessageListener();
    new PongMessageListenerGui().setVisible(true);
  }
}
