package game1.network.debug.gui;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import game1.network.debug.CapturedMessage;
import game1.network.debug.MessageListener;
import game1.network.debug.pubsub.Subscriber;
import game1.network.debug.pubsub.TopicTypes;
import game1.network.debug.pubsub.Topics;

public class MessageCounter extends JPanel implements Subscriber {

  private JLabel stateCreateMessagesValue, stateUpdateMessagesValue, otherMessagesValue;

  public MessageCounter() {
    setLayout(new BorderLayout(2, 2));
    createLayout();
    updateFields();
    Topics.subscribe(this, TopicTypes.MESSAGE_DATA_UPDATED);
  }

  private void createLayout() {
    JPanel panel = new JPanel(new GridLayout(0, 2, 2, 2));
    panel.add(new JLabel("[13] StateCreate: "));
    panel.add(stateCreateMessagesValue = new JLabel("0"));
    panel.add(new JLabel("[01] StateUpdate: "));
    panel.add(stateUpdateMessagesValue = new JLabel("0"));
    panel.add(new JLabel("[??] Other: "));
    panel.add(otherMessagesValue = new JLabel("0"));
    add(panel, BorderLayout.NORTH);
  }

  @Override
  public void receiveData(Object data) {
    updateFields();
  }

  private void updateFields() {
    int scm = 0;
    int sum = 0;
    int om = 0;
    for (int i = 0; i < MessageListener.CAPTURED_MESSAGES.size(); i++) {
      CapturedMessage cm = MessageListener.CAPTURED_MESSAGES.get(i);
      if (cm.getType() == 13) {
        scm++;
      } else if (cm.getType() == 1) {
        sum++;
      } else {
        om++;
      }
    }
    stateCreateMessagesValue.setText(Integer.toString(scm));
    stateUpdateMessagesValue.setText(Integer.toString(sum));
    otherMessagesValue.setText(Integer.toString(om));
  }
}
