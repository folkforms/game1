package game1.network.debug;

import java.util.List;
import java.util.TimerTask;

import game1.network.Message;
import game1.network.ReliableDeliveryOverUDP;
import game1.network.debug.pubsub.TopicTypes;
import game1.network.debug.pubsub.Topics;

public class ListenTask extends TimerTask {

  private ReliableDeliveryOverUDP source;

  public ListenTask(ReliableDeliveryOverUDP source) {
    this.source = source;
  }

  @Override
  public void run() {
    List<Message> incomingMessages = source.getIncomingMessages();
    for (Message message : incomingMessages) {
      MessageListener.CAPTURED_MESSAGES.add(new CapturedMessage(message));
    }
    Topics.publish(TopicTypes.MESSAGE_DATA_UPDATED, null);
  }
}
