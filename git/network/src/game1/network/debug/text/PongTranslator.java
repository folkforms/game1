package game1.network.debug.text;

import folkforms.log.Log;
import game1.network.Message;
import game1.network.utils.ByteQueue;

public class PongTranslator {

  public static void translate(Message message) {
    ByteQueue bq = new ByteQueue(message.getBytes());
    byte networkType = bq.getByte();
    int messageId = bq.getInt();
    int portNumber = bq.getInt();
    byte type = bq.getByte();

    if (type == 1) {
      // StateUpdate
      Log.info("StateUpdate (%s / %s / %s) {", networkType, messageId, portNumber);
      while (!bq.isReadPointerAtEnd()) {
        int id = bq.getInt();
        int dataSize = bq.getInt();
        byte[] data = bq.getByteArray(dataSize);
        Log.info("  id: %s, dataSize: %s, data: %s", id, dataSize, bytesToString(data));
      }
      Log.info("}");
    } else if (type == 13) {
      // StateCreate
      Log.info("StateCreate (%s / %s / %s) {", networkType, messageId, portNumber);
      while (!bq.isReadPointerAtEnd()) {
        int itemType = bq.getInt();
        int id = bq.getInt();
        int dataSize = bq.getInt();
        byte[] data = bq.getByteArray(dataSize);
        Log.info("  type: %s, id: %s, dataSize: %s, data: %s", itemType, id, dataSize,
            bytesToString(data));
      }
      Log.info("}");
    } else if (type == 15) {
      Log.info("HEARTBEAT_ASK { ... }");
    } else if (type == 16) {
      Log.info("HEARTBEAT_OK { ... }");
    } else {
      Log.info("Unknown message type (%s)", type);
    }
  }

  private static String bytesToString(byte[] bytes) {
    StringBuffer sb = new StringBuffer();
    for (int i = 0; i < bytes.length; i++) {
      sb.append(String.format("%02X", bytes[i])).append(":");
    }
    String out = sb.toString();
    out = out.substring(0, out.length() - 1);
    return out;
  }
}
