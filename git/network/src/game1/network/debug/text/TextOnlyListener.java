package game1.network.debug.text;

import java.io.IOException;

import game1.network.debug.CapturedMessage;
import game1.network.debug.MessageListener;
import game1.network.debug.pubsub.Subscriber;
import game1.network.debug.pubsub.TopicTypes;
import game1.network.debug.pubsub.Topics;

public class TextOnlyListener implements Subscriber {

  public TextOnlyListener() {
    Topics.subscribe(this, TopicTypes.MESSAGE_DATA_UPDATED);
  }

  @Override
  public void receiveData(Object data) {
    for (int i = 0; i < MessageListener.CAPTURED_MESSAGES.size(); i++) {
      CapturedMessage cm = MessageListener.CAPTURED_MESSAGES.get(i);
      PongTranslator.translate(cm.getMessage());
    }
  }

  public static void main(String[] args) throws InterruptedException, IOException {
    new MessageListener();
    new TextOnlyListener();
  }
}
