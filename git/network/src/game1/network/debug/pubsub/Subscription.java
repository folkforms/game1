package game1.network.debug.pubsub;

public class Subscription {

  private Subscriber subscriber;
  private int type;

  public Subscription(Subscriber subscriber, int type) {
    this.subscriber = subscriber;
    this.type = type;
  }

  public Subscriber getSubscriber() {
    return subscriber;
  }

  public int getType() {
    return type;
  }
}
