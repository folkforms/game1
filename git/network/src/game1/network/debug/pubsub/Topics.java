package game1.network.debug.pubsub;

import java.util.ArrayList;
import java.util.List;

public class Topics {

  private static List<Subscription> subscriptions = new ArrayList<>();

  public static void subscribe(Subscriber subscriber, int type) {
    subscriptions.add(new Subscription(subscriber, type));
  }

  public static void publish(int type, Object data) {
    for (Subscription subscription : subscriptions) {
      if (subscription.getType() == type) {
        subscription.getSubscriber().receiveData(data);
      }
    }
  }
}