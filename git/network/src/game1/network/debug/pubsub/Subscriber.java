package game1.network.debug.pubsub;

public interface Subscriber {

  public void receiveData(Object data);
}
