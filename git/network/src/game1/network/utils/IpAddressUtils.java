package game1.network.utils;

import java.net.SocketAddress;

public class IpAddressUtils {

  public static String getHostname(SocketAddress socketAddress) {
    String temp = socketAddress.toString().substring(1);
    String hostname = temp.substring(0, temp.indexOf(':'));
    return hostname;
  }
}
