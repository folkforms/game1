package game1.network.utils;

/**
 * Resizeable array of bytes. Get methods return data in sequence from the LHS of the array.
 */
public class ByteQueue {

  private byte[] bytes;
  private int readPointer = 0;
  private int writePointer = 0;

  /**
   * Creates an empty ByteQueue with an initial capacity of 16.
   */
  public ByteQueue() {
    this(16);
  }

  /**
   * Creates an empty ByteQueue with an initial capacity of <code>initialCapacity</code>.
   */
  public ByteQueue(int initialCapacity) {
    bytes = new byte[initialCapacity];
  }

  /**
   * Creates a ByteQueue that wraps an existing byte array.
   */
  public ByteQueue(byte[] b) {
    this.bytes = b;
    this.writePointer = b.length;
  }

  /**
   * Gets an array containing all of the bytes that have been written to this array. E.g. if
   * <code>initialCapacity<code> was 10 and you wrote 3 bytes then this method would return an array
   * of size 3.
   */
  public byte[] toByteArray() {
    byte[] output = new byte[writePointer];
    for (int i = 0; i < writePointer; i++) {
      output[i] = bytes[i];
    }
    return output;
  }

  /**
   * Gets the byte at the head of the queue and advances the read pointer by one.
   */
  public byte getByte() {
    return bytes[readPointer++];
  }

  /**
   * Gets <code>size</code> bytes from the head of the queue and advances the read pointer by
   * <code>size</code>.
   */
  public byte[] getByteArray(int size) {
    byte[] output = new byte[size];
    for (int i = 0; i < size; i++) {
      output[i] = bytes[readPointer + i];
    }
    readPointer += size;
    return output;
  }

  /**
   * Gets an int created from the four bytes at the head of the queue and advances the read pointer
   * by four.
   */
  public int getInt() {
    int value = (bytes[readPointer + 0] << 24) & 0xff000000
        | (bytes[readPointer + 1] << 16) & 0x00ff0000 | (bytes[readPointer + 2] << 8) & 0x0000ff00
        | (bytes[readPointer + 3] << 0) & 0x000000ff;
    readPointer += 4;
    return value;
  }

  /**
   * Gets a long created from the eight bytes at the head of the queue and advances the read pointer
   * by eight.
   */
  public long getLong() {
    long result = 0;
    for (int i = 0; i < 8; i++) {
      result <<= 8;
      result |= (bytes[readPointer + i] & 0xFF);
    }
    return result;
  }

  /**
   * Gets a String of <code>size</code> bytes from the head of the queue and advances the read
   * pointer by <code>size</code>.
   */
  public String getString(int size) {
    return new String(getByteArray(size));
  }

  /**
   * Check if there is enough space left to add <code>extra</code> bytes. If not, increase the size
   * of the underlying array to be just large enough.
   */
  private void checkCapacity(int extra) {
    if (bytes.length - writePointer < extra) {
      byte[] newArray = new byte[writePointer + extra];
      for (int i = 0; i < writePointer; i++) {
        newArray[i] = bytes[i];
      }
      bytes = newArray;
    }
  }

  /**
   * Adds a byte to the tail of the queue.
   */
  public void addByte(byte b) {
    checkCapacity(1);
    bytes[writePointer] = b;
    writePointer++;
  }

  /**
   * Adds a byte array to the tail of the queue.
   */
  public void addByteArray(byte[] arr) {
    checkCapacity(arr.length);
    for (int i = 0; i < arr.length; i++) {
      bytes[writePointer++] = arr[i];
    }
  }

  /**
   * Adds an int to the tail of the queue.
   */
  public void addInt(int value) {
    checkCapacity(4);
    byte[] converted = new byte[4];
    converted[0] = (byte) ((value >> 24) & 0xFF);
    converted[1] = (byte) ((value >> 16) & 0xFF);
    converted[2] = (byte) ((value >> 8) & 0xFF);
    converted[3] = (byte) (value & 0xFF);
    addByteArray(converted);
  }

  /**
   * Adds a long to the tail of the queue.
   */
  public void addLong(long value) {
    checkCapacity(8);
    byte[] converted = new byte[8];
    for (int i = 7; i >= 0; i--) {
      converted[i] = (byte) (value & 0xFF);
      value >>= 8;
    }
    addByteArray(converted);
  }

  /**
   * Adds a string to the tail of the queue.
   */
  public void addString(String str) {
    checkCapacity(str.length());
    byte[] strBytes = str.getBytes();
    addByteArray(strBytes);
  }

  /**
   * Gets whether there is more data available to be read via 'getX' methods.
   */
  public boolean isReadPointerAtEnd() {
    return readPointer >= writePointer;
  }
}
