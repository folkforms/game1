package game1.network.utils;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.UnresolvedAddressException;
import java.util.ArrayList;
import java.util.List;

import folkforms.log.Log;
import game1.network.Message;

public class NetworkMessageUtils {

  protected DatagramChannel incomingChannel, outgoingChannel;
  protected static boolean DEBUG = false;

  public NetworkMessageUtils(int listenPort) throws IOException {
    // Set up incoming channel
    incomingChannel = DatagramChannel.open();
    incomingChannel.configureBlocking(false);
    incomingChannel.bind(new InetSocketAddress(listenPort));
    Log.info("Listening on port %s", listenPort);

    // Set up outgoing channel
    outgoingChannel = DatagramChannel.open();
  }

  public void close() throws IOException {
    incomingChannel.close();
    outgoingChannel.close();
  }

  /**
   * Send the given message immediately.
   *
   * @param message
   *          the message to send
   */
  public void sendMessage(byte[] bytes, SocketAddress target) {
    ByteBuffer buf = ByteBuffer.allocate(bytes.length);
    buf.clear();
    buf.put(bytes);
    buf.flip();

    try {
      outgoingChannel.send(buf, target);
    } catch (IOException ex) {
      Log.error("Error pushing outgoing message to %s: %s", target, new Message(bytes));
      Log.error(ex);
    } catch (UnresolvedAddressException ex) {
      Log.error("Invalid address: %s (message: %s)", target, new Message(bytes));
      Log.error(ex);
    }
  }

  /**
   * Check the listening port for all incoming messages.
   *
   * @return all incoming messages on the listening port.
   */
  public List<Message> getIncomingMessages() {
    List<Message> incoming = new ArrayList<>();
    try {
      // FIXME Magic number. Ideally we would have a standard packet slice size
      // and we would read that much data at a time. We would then concatenate
      // those slices into packets based on their IDs. Then from the packets we
      // extract the messages. Something like that.
      ByteBuffer buf = ByteBuffer.allocate(1025);
      buf.clear();
      SocketAddress socketAddress = null;
      while ((socketAddress = incomingChannel.receive(buf)) != null) {

        byte[] source = buf.array();
        byte[] bytes = new byte[buf.position()];
        for (int i = 0; i < bytes.length; i++) {
          bytes[i] = source[i];
        }

        Message message = new Message(bytes);
        message.setSource(socketAddress);
        incoming.add(message);

        buf.clear();
      }
    } catch (IOException ex) {
      Log.error("Error receiving incoming messages!");
      Log.error(ex);
      // FIXME Handle this better. I guess it depends on why we can't receive messages?
    }
    return incoming;
  }
}
