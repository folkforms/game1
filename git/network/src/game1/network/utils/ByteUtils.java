package game1.network.utils;

public class ByteUtils {

  public static byte[] createPayload(byte type, byte[]... arrays) {
    ByteQueue bq = new ByteQueue();
    bq.addByte(type);
    for (int i = 0; i < arrays.length; i++) {
      bq.addByteArray(arrays[i]);
    }
    return bq.toByteArray();
  }

  public static byte[] fromInt(int value) {
    ByteQueue bq = new ByteQueue(4);
    bq.addInt(value);
    return bq.toByteArray();
  }

  public static byte[] fromLong(long value) {
    ByteQueue bq = new ByteQueue(8);
    bq.addLong(value);
    return bq.toByteArray();
  }

  public static int intFromBytes(byte[] bytes) {
    ByteQueue bq = new ByteQueue(bytes);
    return bq.getInt();
  }
}
