package game1.network.handlers;

import java.util.ArrayList;
import java.util.List;

import game1.core.networking.NetworkActor;
import game1.core.networking.StateUpdate;
import game1.network.Message;
import game1.network.NetworkActors;
import game1.network.utils.ByteQueue;

public class StateUpdateMessageHandler implements MessageHandler {

  @Override
  public void handle(Message message) {
    List<NetworkActor> networkActors = NetworkActors.listNetworkActors();
    List<StateUpdate> stateUpdates = decode(message);
    for (NetworkActor na : networkActors) {
      int id = na.getId();
      for (StateUpdate su : stateUpdates) {
        if (su.getId() == id) {
          na.applyStateUpdate(su);
        }
      }
    }
  }

  private List<StateUpdate> decode(Message message) {

    List<StateUpdate> stateUpdates = new ArrayList<>();

    byte[] messageData = message.getBytes();
    ByteQueue bq = new ByteQueue(messageData);
    bq.getByte(); // Discard type

    while (!bq.isReadPointerAtEnd()) {
      int id = bq.getInt();
      int dataSize = bq.getInt();
      byte[] data = bq.getByteArray(dataSize);
      StateUpdate su = new StateUpdate(id, data);
      stateUpdates.add(su);
    }

    return stateUpdates;
  }
}
