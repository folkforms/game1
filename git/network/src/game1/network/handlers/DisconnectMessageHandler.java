package game1.network.handlers;

import folkforms.log.Log;
import game1.network.Message;
import game1.network.players.NetworkPlayers;
import game1.network.utils.ByteQueue;

public class DisconnectMessageHandler implements MessageHandler {

  @Override
  public void handle(Message message) {
    ByteQueue bq = new ByteQueue(message.getBytes());
    bq.getByte(); // Discard type
    int slotNumber = bq.getInt();
    Log.info("Received disconnect message from slot #%s", slotNumber);
    NetworkPlayers.removePlayer(slotNumber);
  }
}
