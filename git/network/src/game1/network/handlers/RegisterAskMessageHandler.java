package game1.network.handlers;

import game1.core.networking.MessageTypes;
import game1.network.Message;
import game1.network.players.NetworkPlayers;
import game1.network.utils.ByteQueue;

public class RegisterAskMessageHandler implements MessageHandler {

  @Override
  public void handle(Message message) {

    byte[] messageData = message.getBytes();
    ByteQueue bq = new ByteQueue(messageData);
    bq.getByte(); // Discard type
    int port = bq.getInt();
    String playerName = new String(bq.getByteArray(messageData.length - 5));

    int slotNumber = NetworkPlayers.addPlayer(message.getSource(), port, playerName);

    ByteQueue replyData = new ByteQueue();
    if (slotNumber != -1) {
      replyData.addByte(MessageTypes.REGISTER_OK);
      replyData.addInt(slotNumber);
    } else {
      replyData.addByte(MessageTypes.REGISTER_DENY);
      replyData.addString("Failed to register");
    }
    NetworkPlayers.sendMessage(new Message(replyData.toByteArray()), slotNumber);
  }
}
