package game1.network.handlers;

import game1.core.engine.Engine;
import game1.network.Message;
import game1.network.utils.ByteQueue;

public class SoundCueMessageHandler implements MessageHandler {

  @Override
  public void handle(Message gm) {
    byte[] messageData = gm.getBytes();
    ByteQueue bq = new ByteQueue(messageData);
    bq.getByte(); // Discard type
    final String filename = new String(bq.getByteArray(messageData.length - 1));
    Engine.SOUND_BOARD.play(filename, "dummy-reference");
  }
}
