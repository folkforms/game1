package game1.network.handlers;

import folkforms.log.Log;
import game1.network.Message;

public class DummyMessageHandler implements MessageHandler {

  @Override
  public void handle(Message message) {
    Log.error("No MessageHandler registered for message type %s", message.getBytes()[0]);
    Log.error("Message data: %s", message);
  }
}
