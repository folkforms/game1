package game1.network.handlers;

import folkforms.log.Log;
import game1.core.engine.Engine;
import game1.core.networking.MessageTypes;
import game1.core.networking.StateCreateEncoder;
import game1.misc.States;
import game1.network.Message;
import game1.network.NetworkActors;
import game1.network.players.NetworkPlayers;
import game1.network.utils.ByteQueue;

public class ReadyMessageHandler implements MessageHandler {

  @Override
  public void handle(Message gm) {

    byte[] messageData = gm.getBytes();
    ByteQueue bq = new ByteQueue(messageData);
    bq.getByte(); // Discard type
    int slotNumber = bq.getInt();
    byte readyByte = bq.getByte();
    boolean ready = readyByte == 1;
    NetworkPlayers.setReady(slotNumber, ready);

    // Check for game start conditions
    boolean enoughPlayers = NetworkPlayers.hasEnoughPlayersToStart();
    boolean allReady = NetworkPlayers.allPlayersReady();
    if (enoughPlayers && allReady) {
      startGame();
    }
  }

  private void startGame() {
    // Move server to NETWORK_GAME state
    Log.info("Started a new game");
    Engine.GAME_STATE.moveToState(States.NETWORK_GAME_SEND);
    NetworkPlayers.sendToAll(new Message(new byte[] { MessageTypes.START_GAME }));

    // Give players initial list of items to create
    byte[] stateCreateBytes = StateCreateEncoder.encodeState(NetworkActors.listNetworkActors());
    ByteQueue bq = new ByteQueue(1 + stateCreateBytes.length);
    bq.addByte(MessageTypes.STATE_CREATE);
    bq.addByteArray(stateCreateBytes);
    NetworkPlayers.sendToAll(new Message(bq.toByteArray()));
  }
}
