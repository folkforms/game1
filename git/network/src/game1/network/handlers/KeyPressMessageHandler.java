package game1.network.handlers;

import java.util.Iterator;
import java.util.List;

import game1.core.networking.ServerKeyBindings;
import game1.core.networking.ServerKeyCommand;
import game1.network.Message;
import game1.network.utils.ByteQueue;

public class KeyPressMessageHandler implements MessageHandler {

  @Override
  public void handle(Message message) {

    byte[] messageData = message.getBytes();
    ByteQueue bq = new ByteQueue(messageData);
    bq.getByte(); // Discard type
    byte incomingKeyCode = bq.getByte();
    int playerNumber = bq.getInt();

    // FIXME Need to come up with a plan to map keys to server commands
    ServerKeyBindings keyBindings = null; // Engine.SERVER_KEY_BINDINGS;
    List<ServerKeyCommand> serverKeyCommands = keyBindings.getServerKeyCommands();
    for (Iterator<ServerKeyCommand> iterator = serverKeyCommands.iterator(); iterator.hasNext();) {
      ServerKeyCommand skc = iterator.next();

      boolean shouldExecute = false;
      int[] keyCodes = skc.getKeyCodes();
      for (int i = 0; i < keyCodes.length; i++) {
        if (incomingKeyCode == keyCodes[i]) {
          shouldExecute = true;
          break;
        }
      }
      if (shouldExecute) {
        skc.execute(playerNumber);
      }
    }
  }
}
