package game1.network.handlers;

import game1.network.Message;
import game1.network.players.NetworkPlayers;
import game1.network.utils.ByteQueue;

public class HeartbeatOkMessageHandler implements MessageHandler {

  @Override
  public void handle(Message message) {
    byte[] messageData = message.getBytes();
    ByteQueue bq = new ByteQueue(messageData);
    bq.getByte(); // Discard type
    int slotNumber = bq.getInt();
    long timeSent = bq.getLong();
    NetworkPlayers.updateHeartbeat(slotNumber, timeSent);
  }
}
