package game1.network.handlers;

import game1.core.engine.Engine;
import game1.core.networking.NetworkActor;
import game1.core.networking.StateUpdate;
import game1.network.Message;
import game1.network.players.NetworkPlayers;
import game1.network.utils.ByteQueue;

public class StateCreateMessageHandler implements MessageHandler {

  @Override
  public void handle(Message message) {
    byte[] messageData = message.getBytes();
    ByteQueue bq = new ByteQueue(messageData);
    bq.getByte(); // Discard type

    while (!bq.isReadPointerAtEnd()) {
      int type = bq.getInt();
      int id = bq.getInt();
      int dataSize = bq.getInt();
      byte[] data = bq.getByteArray(dataSize);

      // Create an item of the appropriate type
      NetworkActor newNetworkActor = NetworkPlayers.NETWORK_ACTOR_FACTORY.newInstance(type);
      newNetworkActor.setId(id);
      // Apply the data as if it were a normal state update
      StateUpdate su = new StateUpdate(id, data);
      newNetworkActor.applyStateUpdate(su);
      Engine.GAME_STATE.addActor(newNetworkActor);
    }
  }
}
