package game1.network.handlers;

import game1.core.networking.MessageTypes;
import game1.network.Message;
import game1.network.players.NetworkPlayers;
import game1.network.utils.ByteQueue;

public class HeartbeatAskMessageHandler implements MessageHandler {

  @Override
  public void handle(Message message) {
    // Extract who sent the message
    ByteQueue bq = new ByteQueue(message.getBytes());
    bq.getByte(); // Discard type
    int slotNumber = bq.getInt();
    long timeSent = bq.getLong();

    if (NetworkPlayers.SLOT_NUMBER != -1) {
      // Send response
      ByteQueue bq2 = new ByteQueue(13);
      bq2.addByte(MessageTypes.HEARTBEAT_OK);
      bq2.addInt(NetworkPlayers.SLOT_NUMBER);
      bq2.addLong(timeSent);
      Message reply = new Message(bq2.toByteArray());
      NetworkPlayers.sendMessage(reply, slotNumber);
    }
  }
}
