package game1.network.handlers;

import game1.network.Message;

public interface MessageHandler {

  void handle(Message message);
}
