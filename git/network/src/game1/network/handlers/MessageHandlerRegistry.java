package game1.network.handlers;

import java.util.HashMap;
import java.util.Map;

import folkforms.log.Log;

public class MessageHandlerRegistry {

  private Map<Byte, MessageHandler> handlers;

  public MessageHandlerRegistry() {
    handlers = new HashMap<>();
  }

  public void addHandler(byte type, MessageHandler mh) {
    MessageHandler previous = handlers.put(type, mh);
    if (previous != null) {
      Log.error("Overwrote existing handler for message type %s", type);
    }
  }

  public MessageHandler getHandler(byte type) {
    MessageHandler handler = handlers.get(type);
    if (handler != null) {
      return handler;
    } else {
      return new DummyMessageHandler();
    }
  }
}
