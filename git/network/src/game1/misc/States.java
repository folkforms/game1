package game1.misc;

/**
 * Identifiers for well-known states. Negative numbers are reserved for core states.
 */
public class States {
  public static final String NETWORK_GAME_SEND = "network-game-send";
  public static final String NETWORK_GAME_RECEIVE = "network-game-receive";
  public static final String SERVER_TIMEOUT = "server-timeout";
}
