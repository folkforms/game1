package game1.misc;

import game1.core.engine.Command;
import game1.core.networking.MessageTypes;
import game1.network.Message;
import game1.network.players.NetworkPlayers;
import game1.network.utils.ByteQueue;

public class SendKeyCommand implements Command {

  public final int KEY;

  public SendKeyCommand(int key) {
    KEY = key;
  }

  @Override
  public void execute() {
    ByteQueue bq = new ByteQueue(6);
    bq.addByte(MessageTypes.KEYPRESS);
    bq.addByte((byte) KEY);
    bq.addInt(NetworkPlayers.SLOT_NUMBER);
    Message message = new Message(bq.toByteArray());
    NetworkPlayers.sendMessage(message, NetworkPlayers.SERVER);
  }
}
