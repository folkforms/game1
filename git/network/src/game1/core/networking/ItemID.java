package game1.core.networking;

/**
 * Generates new unique IDs. Typically used by {@link NetworkActor} to assign IDs to instances so we
 * know which item to apply which state update to.
 */
public class ItemID {

  private static final int initialId = 1000;
  private static int id = initialId;

  public static int getNextId() {
    return ++id;
  }

  /**
   * Reset the ID counter. This should only be called when starting a new local game so that any
   * saved games will transfer their state update correctly.
   */
  public static void reset() {
    id = initialId;
  }
}
