package game1.core.networking;

/**
 * Allows the creation of NetworkActor objects based on an integer type value.
 */
public interface NetworkActorFactory {

  public NetworkActor newInstance(int type);
}
