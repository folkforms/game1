package game1.core.networking;

import game1.core.actors.Actor;

/**
 * A NetworkActor is an actor whose state can be reduced to a series of bytes for network
 * transmission.
 */
public interface NetworkActor extends Actor {

  /**
   * Gets the unique ID of this instance of the network actor. The ID is usually assigned by calling
   * <code>ItemID.getNextId()</code> so as to guarantee that the ID will be unique to this instance.
   *
   * @return the unique ID of this instance of the network actor
   */
  public int getId();

  /**
   * Sets the unique ID of this instance of the network actor. This is used when NetworkActors are
   * created via StateCreate message.
   */
  public void setId(int id);

  /**
   * Gets the unique type of this network actor.
   *
   * @return the unique type of this network actor
   */
  public int getType();

  /**
   * Converts this objects current state into a byte[].
   *
   * @return byte[] containing state information
   */
  public byte[] getState();

  /**
   * Takes a StateUpdate object and applies it to this NetworkActor.
   *
   * @param stateUpdate
   *          the state update
   */
  public void applyStateUpdate(StateUpdate stateUpdate);
}
