package game1.core.networking;

public class StateUpdate {

  private int id;
  private byte[] data;

  public StateUpdate(int id, byte[] data) {
    this.id = id;
    this.data = data;
  }

  public int getId() {
    return id;
  }

  public byte[] getData() {
    return data;
  }
}
