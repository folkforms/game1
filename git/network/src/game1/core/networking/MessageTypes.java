package game1.core.networking;

public class MessageTypes {

  public static final byte STATE_UPDATE = 1;
  public static final byte KEYPRESS = 2;
  public static final byte SOUND_CUE = 3;
  public static final byte REGISTER_ASK = 4;
  public static final byte REGISTER_OK = 5;
  public static final byte REGISTER_DENY = 6;
  public static final byte START_GAME = 7;
  public static final byte DISCONNECT = 8;
  public static final byte CHAT_FROM_PLAYER = 9;
  public static final byte CHAT_BROADCAST = 10;
  public static final byte VOIP = 12;
  public static final byte STATE_CREATE = 13;
  public static final byte STATE_RELEVANT = 14;
  public static final byte HEARTBEAT_ASK = 15;
  public static final byte HEARTBEAT_OK = 16;
  public static final byte READY = 17;
}
