package game1.core.networking;

import java.util.List;

import game1.network.utils.ByteQueue;

/**
 * Converts the list of all {@Link NetworkActor}s into a byte[] of their type followed by their
 * state update.
 */
public class StateCreateEncoder {

  public static byte[] encodeState(List<NetworkActor> networkActors) {
    byte[] b = internalEncode(networkActors);
    return b;
  }

  private static byte[] internalEncode(List<NetworkActor> networkActors) {
    ByteQueue b = new ByteQueue(networkActors.size() * 20);
    for (NetworkActor n : networkActors) {
      byte[] encoded = convertNetworkActor(n);
      b.addByteArray(encoded);
    }
    return b.toByteArray();
  }

  /**
   * Encodes a network actor state update in the form:
   * <ul>
   * <li>Type (4 bytes)</li>
   * <li>ID (4 bytes)</li>
   * <li>Data size (4 bytes)</li>
   * <li>Data</li>
   * </ul>
   *
   * @param networkActor
   * @return
   */
  private static byte[] convertNetworkActor(NetworkActor networkActor) {

    int typeId = networkActor.getType();
    int itemId = networkActor.getId();
    byte[] itemData = networkActor.getState();
    int dataSize = itemData.length;

    ByteQueue b = new ByteQueue(4 + 4 + 4 + dataSize);
    b.addInt(typeId);
    b.addInt(itemId);
    b.addInt(dataSize);
    b.addByteArray(itemData);

    return b.toByteArray();
  }
}
