package game1.core.networking;

import java.util.List;

import game1.core.input.KeyCommand;

/**
 * Defines a list of key bindings that can be set for your application. You application can change
 * the current key bindings using FIXME
 *
 * @see FIXME
 */
public interface ServerKeyBindings {

  /**
   * Gets a list of {@link KeyCommand}s for this set of key bindings. Note that you should use
   * {@link Keyboard} key codes regardless of whether you are using OpenGL or Swing. When using
   * Swing the keycodes will automatically be mapped from the OpenGL key codes to the Swing key
   * codes. This is so you do not have to have two bindings, one for OpenGL and one for Swing.
   *
   * @return a list of {@link KeyCommand}s for this set of key bindings
   */
  public List<ServerKeyCommand> getServerKeyCommands();

}
