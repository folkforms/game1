package game1.core.networking;

public interface ServerCommand {

  public void execute(int player);
}
