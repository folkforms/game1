package game1.core.networking;

/**
 * ServerKeyCommand links a key code to a ServerCommand, which is executed on the server.
 * 
 * Note that unlike KeyCommand this does not have to handle blocking as blocking will already have
 * been taken care of by the KeyCommand code on the server.
 */
public class ServerKeyCommand {

  private int[] keyCodes;
  private ServerCommand command;

  /**
   * Creates a ServerKeyCommand based on a single key press.
   *
   * @param keyCode
   *          Single key code
   * @param command
   *          The ServerCommand to execute
   * @see org.lwjgl.input.Keyboard
   */
  public ServerKeyCommand(int keyCode, ServerCommand command) {
    this(new int[] { keyCode }, command);
  }

  /**
   * Creates a ServerKeyCommand based on a combination of key presses.
   *
   * @param keyCodes
   *          Array of key codes
   * @param command
   *          The command to execute
   * @see org.lwjgl.input.Keyboard
   */
  public ServerKeyCommand(int[] keyCodes, ServerCommand command) {
    this.keyCodes = keyCodes;
    this.command = command;
  }

  /**
   * Gets the key codes that make up this ServerKeyCommand.
   *
   * @return
   */
  public int[] getKeyCodes() {
    return keyCodes;
  }

  /**
   * Executes the ServerCommand object's execute(int) method.
   */
  public void execute(int player) {
    command.execute(player);
  }
}
