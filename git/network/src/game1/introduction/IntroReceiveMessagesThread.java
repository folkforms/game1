package game1.introduction;

import java.util.List;
import java.util.TimerTask;

import game1.network.Message;
import game1.network.handlers.MessageHandler;

public class IntroReceiveMessagesThread extends TimerTask {

  public IntroReceiveMessagesThread() {
  }

  @Override
  public void run() {
    List<Message> messages = IntroductionServer.NETWORK.getIncomingMessages();

    for (Message message : messages) {
      handleMessage(message);
    }
  }

  private void handleMessage(Message message) {

    MessageHandler handler = IntroductionServer.MESSAGE_HANDLERS.getHandler(message.getBytes()[0]);
    handler.handle(message);
  }
}
