package game1.introduction;

import java.io.IOException;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import folkforms.log.Log;
import game1.network.ReliableDeliveryOverUDP;
import game1.network.handlers.MessageHandlerRegistry;

/**
 * The Introduction Server runs on a public machine and allows servers to register with it. Clients
 * can ask the Introduction Server for a list of servers, which they can then connect to.
 */
public class IntroductionServer {

  private static final int INTRO_LISTEN_PORT = 8010;
  private static final long NETWORK_TICK_LENGTH = 16;
  protected static ReliableDeliveryOverUDP NETWORK;
  protected static MessageHandlerRegistry MESSAGE_HANDLERS = new MessageHandlerRegistry();
  private static Timer timer = new Timer();
  protected static List<SocketAddress> SERVERS = new ArrayList<>();

  public static void main(String[] args) throws IOException {
    Log.info("Introduction server started");

    MESSAGE_HANDLERS.addHandler(IntroMessageTypes.REGISTER_SERVER,
        new RegisterServerMessageHandler());
    MESSAGE_HANDLERS.addHandler(IntroMessageTypes.LIST_SERVERS_ASK,
        new ListServersMessageHandler());

    NETWORK = new ReliableDeliveryOverUDP(INTRO_LISTEN_PORT);

    // Application will immediately send responses to received messages
    TimerTask receiveMessagesThread = new IntroReceiveMessagesThread();
    timer.schedule(receiveMessagesThread, 0, NETWORK_TICK_LENGTH);
  }
}
