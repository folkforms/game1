package game1.introduction;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

import folkforms.log.Log;
import game1.network.Message;
import game1.network.handlers.MessageHandler;
import game1.network.utils.ByteQueue;
import game1.network.utils.IpAddressUtils;

/**
 * Lists the servers registered with this system.
 */
public class ListServersMessageHandler implements MessageHandler {

  @Override
  public void handle(Message message) {
    ByteQueue bq = new ByteQueue();
    for (SocketAddress socketAddress : IntroductionServer.SERVERS) {
      String address = socketAddress.toString().substring(1);
      bq.addInt(address.length());
      bq.addString(address);
    }

    ByteQueue replyData = new ByteQueue();
    replyData.addByte(IntroMessageTypes.LIST_SERVERS_RESPONSE);
    replyData.addByteArray(bq.toByteArray());

    Message reply = new Message(replyData.toByteArray());

    SocketAddress source = message.getSource();
    reply.setTarget(new InetSocketAddress(IpAddressUtils.getHostname(source), 8009));

    // FIXME Send 5 messages for now, just to make sure it is not one of those NATs that blocks the
    // first message.
    for (int i = 0; i < 5; i++) {
      Log.temp("Sending list of servers: %s", replyData);
      IntroductionServer.NETWORK.sendMessage(reply);
    }
  }
}
