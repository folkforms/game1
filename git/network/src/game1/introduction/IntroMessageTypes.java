package game1.introduction;

public class IntroMessageTypes {

  public static final byte REGISTER_SERVER = 30;
  public static final byte UNREGISTER_SERVER = 31;
  public static final byte LIST_SERVERS_ASK = 32;
  public static final byte LIST_SERVERS_RESPONSE = 33;
}
