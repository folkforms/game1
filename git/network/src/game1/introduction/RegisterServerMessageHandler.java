package game1.introduction;

import java.net.InetSocketAddress;
import java.net.SocketAddress;

import folkforms.log.Log;
import game1.network.Message;
import game1.network.handlers.MessageHandler;
import game1.network.utils.ByteQueue;

/**
 * Registers a server.
 */
public class RegisterServerMessageHandler implements MessageHandler {

  @Override
  public void handle(Message message) {
    try {
      // FIXME Check that source address matches payload address? Does that actually stop anything
      // since hackers can spoof the source address?
      byte[] messageData = message.getBytes();
      ByteQueue bq = new ByteQueue(messageData);
      bq.getByte(); // Discard type
      byte[] payload = bq.getByteArray(messageData.length - 1);

      String server = new String(payload);
      String[] tokens = server.split(":");
      String hostname = tokens[0];
      int port = Integer.parseInt(tokens[1]);
      SocketAddress address = new InetSocketAddress(hostname, port);
      // FIXME Check server is not already in the list
      IntroductionServer.SERVERS.add(address);
      Log.info("Registered server %s:%s", hostname, port);
    } catch (Exception ex) {
      Log.error("Failed to register server from message: %s", message);
      Log.error(ex);
    }
  }
}
