package game1.network;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import game1.network.utils.ByteQueue;

class ByteQueueTest {

  @Test
  void testToByteArray() {
    ByteQueue bq = new ByteQueue(100);
    bq.addByte((byte) 1);
    bq.addInt(2);
    bq.addByteArray(new byte[] { 3 });
    bq.addString("hello");
    byte[] bytes = bq.toByteArray();
    Assertions.assertEquals(11, bytes.length);
    Assertions.assertEquals(1, bytes[0]);
    Assertions.assertEquals(0, bytes[1]);
    Assertions.assertEquals(0, bytes[2]);
    Assertions.assertEquals(0, bytes[3]);
    Assertions.assertEquals(2, bytes[4]);
    Assertions.assertEquals(3, bytes[5]);
    Assertions.assertEquals(104, bytes[6]);
    Assertions.assertEquals(101, bytes[7]);
    Assertions.assertEquals(108, bytes[8]);
    Assertions.assertEquals(108, bytes[9]);
    Assertions.assertEquals(111, bytes[10]);
  }

  @Test
  void testGetByte() {
    ByteQueue bq = new ByteQueue();
    bq.addByte((byte) 86);
    bq.addByte((byte) 87);
    bq.addByte((byte) 88);

    // Method under test
    Assertions.assertEquals(86, bq.getByte());
    Assertions.assertEquals(87, bq.getByte());
    Assertions.assertEquals(88, bq.getByte());
  }

  @Test
  void testGetByteArray() {
    byte[] array = new byte[] { 1, 2, 3, 4 };
    ByteQueue bq = new ByteQueue();
    bq.addByteArray(array);
    bq.getByte();
    bq.getByte();

    // Method under test
    byte[] actual = bq.getByteArray(2);

    byte[] expected = new byte[] { 3, 4 };
    for (int i = 0; i < actual.length; i++) {
      Assertions.assertEquals(expected[i], actual[i]);
    }
  }

  @Test
  void testGetInt() {
    ByteQueue bq = new ByteQueue();
    bq.addByte((byte) 1);
    bq.addInt(88);
    bq.addByte((byte) 1);
    bq.getByte();

    // Method under test
    Assertions.assertEquals(88, bq.getInt());
  }

  @Test
  void testGetString() {
    ByteQueue bq = new ByteQueue();
    bq.addByte((byte) 1);
    bq.addString("hello");
    bq.addByte((byte) 2);
    bq.addString("yes");
    bq.addByte((byte) 3);

    bq.getByte();
    Assertions.assertEquals("hello", bq.getString(5));
    bq.getByte();
    Assertions.assertEquals("yes", bq.getString(3));
  }

  @Test
  void testAddByte() {
    ByteQueue bq = new ByteQueue();
    bq.addByte((byte) 88);
    Assertions.assertEquals(88, bq.getByte());
    bq.addByte((byte) 89);
    Assertions.assertEquals(89, bq.getByte());
  }

  @Test
  void testAddByteArray() {
    byte[] array1 = new byte[] { 1, 2, 3, 4 };
    byte[] array2 = new byte[] { 5, 6, 7, 8 };
    ByteQueue bq = new ByteQueue();
    bq.addByteArray(array1);
    bq.addByteArray(array2);

    // Method under test
    byte[] actual = bq.getByteArray(8);

    byte[] expected = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };
    for (int i = 0; i < actual.length; i++) {
      Assertions.assertEquals(expected[i], actual[i]);
    }
  }

  @Test
  void testAddInt() {
    ByteQueue bq = new ByteQueue();
    bq.addByte((byte) 1);
    bq.addInt(88);
    bq.addByte((byte) 99);
    bq.getByte();
    Assertions.assertEquals(88, bq.getInt());
  }

  @Test
  void testAddString() {
    ByteQueue bq = new ByteQueue();
    bq.addByte((byte) 1);
    bq.addString("hello");
    bq.addByte((byte) 2);
    bq.addString("yes");
    bq.addByte((byte) 3);

    bq.getByte();
    Assertions.assertEquals("hello", bq.getString(5));
    bq.getByte();
    Assertions.assertEquals("yes", bq.getString(3));
  }
}
