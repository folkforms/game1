package game1.variables;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;

import folkforms.yaml.YamlFile;
import folkforms.yaml.YamlUtils;
import game1.variables.antlr.Var1LanguageFile;
import game1.variables.parsers.Parser;

public class Variables {

  private Map<String, Variable> variables = new HashMap<>();
  private Map<String, List<VariablesListener>> listeners = new HashMap<>();

  public void put(String name, Object value) {
    Variable previous = variables.put(name, new Variable(name, value));
    if (previous != null) {
      throw new RuntimeException(String.format(
          "Variable '%s' already in use. Previous value was %s, tried to add value %s using same name. Did you mean 'set' instead of 'put'?",
          name, previous, value));
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T getAsNumber(String name) {
    validateVariableExists(name);
    Variable variable = variables.get(name);
    Object value = variable.getValue();
    if (value instanceof Integer || value instanceof Double) {
      value = convertNumberToFloat(value);
      return (T) value;
    } else {
      Var1LanguageFile var1LanguageFile = new Var1LanguageFile(value.toString(), this);
      return (T) Float.valueOf((Float) var1LanguageFile.getResult());
    }
  }

  private Object convertNumberToFloat(Object value) {
    if (value instanceof Integer) {
      Integer i = (Integer) value;
      return Float.parseFloat(i.toString());
    }
    if (value instanceof Double) {
      Double d = (Double) value;
      return Float.parseFloat(d.toString());
    }
    return value;
  }

  public boolean getAsBoolean(String name) {
    validateVariableExists(name);
    Variable variable = variables.get(name);
    Object value = variable.getValue();
    if (value instanceof Boolean) {
      return (Boolean) value;
    } else {
      Var1LanguageFile var1LanguageFile = new Var1LanguageFile(value.toString(), this);
      return (Boolean) var1LanguageFile.getResult();
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T getAsNumber(String name, Class<?> clazz) {
    Float f = getAsNumber(name);
    if (clazz == Float.class) {
      return (T) (Object) f.floatValue();
    } else if (clazz == Integer.class) {
      return (T) (Object) f.intValue();
    } else if (clazz == Double.class) {
      return (T) (Object) f.doubleValue();
    }
    throw new RuntimeException(String.format(
        "Invalid class '%s' provided when calling getAsNumber('%s', '%s'). Only Float, Integer and Double are supported.",
        clazz, name, clazz));
  }

  public String getAsString(String name) {
    validateVariableExists(name);
    Variable variable = variables.get(name);
    return variable.getValue().toString();
  }

  public Vector2f getAsVector2f(String name) {
    validateVariableExists(name);
    Variable variable = variables.get(name);
    String str = variable.getValue().toString();
    String[] tokens = str.split(",\\s*");
    return new Vector2f(Float.parseFloat(tokens[0]), Float.parseFloat(tokens[1]));
  }

  public Vector3f getAsVector3f(String name) {
    validateVariableExists(name);
    Variable variable = variables.get(name);
    String str = variable.getValue().toString();
    String[] tokens = str.split(",\\s*");
    return new Vector3f(Float.parseFloat(tokens[0]), Float.parseFloat(tokens[1]),
        Float.parseFloat(tokens[2]));
  }

  public Vector4f getAsVector4f(String name) {
    validateVariableExists(name);
    Variable variable = variables.get(name);
    String str = variable.getValue().toString();
    String[] tokens = str.split(",\\s*");
    return new Vector4f(Float.parseFloat(tokens[0]), Float.parseFloat(tokens[1]),
        Float.parseFloat(tokens[2]), Float.parseFloat(tokens[3]));
  }

  public Object getAsObject(String name) {
    validateVariableExists(name);
    Variable variable = variables.get(name);
    return variable.getValue();
  }

  public <T> T get(String name, Class<? extends Parser<T>> parserClass) {
    Variable variable = variables.get(name);
    Object value = variable.getValue();
    try {
      Parser<T> parser = parserClass.getDeclaredConstructor().newInstance();
      return parser.parse(value, this);
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException | NoSuchMethodException | SecurityException ex) {
      throw new RuntimeException(ex);
    }
  }

  /**
   * Gets a list of variables by pattern, for example `foo.*`
   */
  public List<Variable> listVariables(String pattern) {
    pattern = pattern.replace(".", "\\.").replace("*", ".*");
    final String pattern2 = pattern;
    List<Variable> matches = new ArrayList<>();
    variables.entrySet().forEach(e -> {
      if (e.getKey().matches(pattern2)) {
        matches.add(e.getValue());
      }
    });
    return matches;
  }

  @SuppressWarnings("unchecked")
  public <T> T get(String name) {
    Variable variable = variables.get(name);
    if (variable == null) {
      throw new RuntimeException(String.format("No such variable '%s'", name));
    }
    Object value = variable.getValue();
    return (T) value;
  }

  public boolean resolveAsBoolean(String value) {
    Var1LanguageFile var1LanguageFile = new Var1LanguageFile(value, this);
    return (Boolean) var1LanguageFile.getResult();
  }

  private void validateVariableExists(String name) {
    if (!variables.containsKey(name)) {
      throw new RuntimeException(String.format(
          "No such variable '%s'. Known variable names: %s. Global variables should be accessed using an '@' prefix.",
          name, variables.keySet()));
    }
  }

  public void set(String name, Object newValue) {
    Variable v = variables.get(name);
    if (v == null) {
      v = new Variable(name, newValue);
      variables.put(name, v);
    } else {
      v.setValue(newValue);
    }
    List<VariablesListener> listenersForName = listeners.get(name);
    if (listenersForName != null) {
      listenersForName.forEach(VariablesListener::assignVariables);
    }
  }

  public void remove(String name) {
    variables.remove(name);
  }

  public void addListener(VariablesListener listener, List<String> variableNames) {
    variableNames.forEach(name -> {
      List<VariablesListener> listenersForVariable = listeners.get(name);
      if (listenersForVariable == null) {
        listenersForVariable = new ArrayList<>();
        listeners.put(name, listenersForVariable);
      }
      listenersForVariable.add(listener);
    });
  }

  public void applyYamlFile(YamlFile yamlFile) {
    Map<String, Object> flattened = YamlUtils.flatten(yamlFile.getRaw());
    flattened.keySet().forEach(key -> set(key, flattened.get(key)));
  }

  @Override
  public String toString() {
    return String.format("%s[%s]", getClass().getSimpleName(), variables);
  }

  public List<String> debug_listVariables() {
    return variables.keySet().stream().sorted()
        .map(key -> String.format("    %s: %s", key, variables.get(key))).toList();
  }
}
