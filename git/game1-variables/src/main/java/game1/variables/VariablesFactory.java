package game1.variables;

public class VariablesFactory {

  private static Variables variables;

  public static Variables getInstance() {
    if (variables == null) {
      throw new RuntimeException("Variables.setInstance has not been called");
    }
    return variables;
  }

  public static void setInstance(Variables instance) {
    variables = instance;
  }
}
