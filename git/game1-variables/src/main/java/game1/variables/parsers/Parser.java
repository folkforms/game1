package game1.variables.parsers;

import game1.variables.Variables;

public interface Parser<T> {
  T parse(Object input, Variables variables);
}
