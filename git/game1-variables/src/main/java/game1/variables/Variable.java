package game1.variables;

import java.util.Objects;
import java.util.Optional;

public class Variable {

  private String name;
  private Object value;
  private Object originalValue;
  private boolean isModified;
  private Optional<String> scope;

  public Variable(String name, Object value) {
    this(name, value, null);
  }

  public Variable(String name, Object value, String scope) {
    this.name = name;
    this.value = cleanString(value);
    this.originalValue = value;
    this.scope = Optional.ofNullable(scope);
  }

  private Object cleanString(Object input) {
    if (input == null | input instanceof String == false) {
      return input;
    }
    String str = (String) input;
    if ((str.startsWith("\"") && str.endsWith("\""))
        || (str.startsWith("'") && str.endsWith("'"))) {
      str = str.substring(1, str.length() - 1);
    }
    return str;
  }

  public String getName() {
    return name;
  }

  public Object getValue() {
    return value;
  }

  public void setValue(Object newValue) {
    value = newValue;
    if ((newValue == null && originalValue != null) || (newValue != null && originalValue == null)
        || (newValue != null && !newValue.equals(originalValue))) {
      isModified = true;
    }
  }

  public Object getOriginalValue() {
    return originalValue;
  }

  public boolean isModified() {
    return isModified;
  }

  public Optional<String> getScope() {
    return scope;
  }

  @Override
  public String toString() {
    return String.format("%s[name=%s,class=%s,value=%s,originalValue=%s,isModified=%s]",
        getClass().getSimpleName(), name, value != null ? value.getClass().getSimpleName() : null,
        value, originalValue, isModified);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, value, originalValue, isModified, scope);
  }

  @Override
  public boolean equals(Object other) {
    if (other instanceof Variable) {
      Variable otherVariable = (Variable) other;
      return name.equals(otherVariable.name) && value.equals(otherVariable.value)
          && originalValue.equals(otherVariable.originalValue)
          && isModified == otherVariable.isModified && scope.equals(otherVariable.scope);
    }
    return false;
  }
}