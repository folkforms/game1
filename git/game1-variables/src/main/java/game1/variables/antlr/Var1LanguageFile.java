package game1.variables.antlr;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import antlr.game1.Var1Lexer;
import antlr.game1.Var1Parser;
import folkforms.log.Log;
import game1.debug.DebugScopes;
import game1.variables.Variables;

public class Var1LanguageFile {

  private String inputText;
  private Variables variables;
  private Var1TreeListener treeListener;
  private Object result;

  public Var1LanguageFile(String inputText, Variables variables) {
    this.inputText = inputText;
    this.variables = variables;
    parse();
  }

  private void parse() {
    Var1Lexer lexer = new Var1Lexer(CharStreams.fromString(inputText));
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    Var1Parser parser = new Var1Parser(tokens);
    ParseTree tree = parser.prog();
    throwIfSyntaxErrors(parser.getNumberOfSyntaxErrors());
    ParseTreeWalker walker = new ParseTreeWalker();
    treeListener = new Var1TreeListener(variables);
    walker.walk(treeListener, tree);

    Log.scoped(DebugScopes.VARIABLES, "Var1LanguageFile#parse: floatStack = %s, booleanStack = %s",
        treeListener.getFloatStack(), treeListener.getBooleanStack());
    if (treeListener.getEquationType() == Var1TreeListener.EquationType.FLOAT) {
      result = treeListener.getFloatStack().peek();
    } else if (treeListener.getEquationType() == Var1TreeListener.EquationType.BOOLEAN) {
      result = treeListener.getBooleanStack().peek();
    } else {
      result = variables.getAsObject(treeListener.getTemporaryId());
    }
  }

  private void throwIfSyntaxErrors(int numErrors) {
    if (numErrors > 0) {
      throw new RuntimeException(
          String.format("Found %s syntax error%s", numErrors, numErrors < 2 ? "" : "s"));
    }
  }

  public Object getResult() {
    return result;
  }
}
