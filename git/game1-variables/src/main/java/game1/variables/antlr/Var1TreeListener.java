package game1.variables.antlr;

import java.util.Stack;

import antlr.game1.Var1BaseListener;
import antlr.game1.Var1Parser;
import folkforms.log.Log;
import game1.debug.DebugScopes;
import game1.variables.Variables;

public class Var1TreeListener extends Var1BaseListener {

  private Variables variables;
  private String temporaryId = null;
  private Stack<Float> floatStack = new Stack<>();
  private Stack<Boolean> booleanStack = new Stack<>();
  private EquationType equationType = null;

  public Var1TreeListener(Variables variables) {
    this.variables = variables;
  }

  @Override
  public void enterProg(Var1Parser.ProgContext ctx) {
  }

  @Override
  public void exitProg(Var1Parser.ProgContext ctx) {
  }

  private void setEquationType(EquationType newType) {
    equationType = newType;
    resolveTemporaryId();
  }

  private void resolveTemporaryId() {
    if (temporaryId == null) {
      return;
    }

    if (equationType == EquationType.FLOAT) {
      floatStack.push(variables.getAsNumber(temporaryId));
    } else if (equationType == EquationType.BOOLEAN) {
      booleanStack.push(variables.getAsBoolean(temporaryId));
    } else {
      throw new RuntimeException(
          String.format("Unknown equation type when handling temporary id: %s", equationType));
    }
    temporaryId = null;
  }

  @Override
  public void enterExprMultiplyDivide(Var1Parser.ExprMultiplyDivideContext ctx) {
    setEquationType(EquationType.FLOAT);
  }

  @Override
  public void exitExprMultiplyDivide(Var1Parser.ExprMultiplyDivideContext ctx) {
    float right = floatStack.pop();
    float left = floatStack.pop();
    if (ctx.op.getType() == Var1Parser.MULTIPLY) {
      floatStack.push(left * right);
    } else {
      floatStack.push(left / right);
    }
  }

  @Override
  public void enterExprAddSubtract(Var1Parser.ExprAddSubtractContext ctx) {
    setEquationType(EquationType.FLOAT);
  }

  @Override
  public void exitExprAddSubtract(Var1Parser.ExprAddSubtractContext ctx) {
    float right = floatStack.pop();
    float left = floatStack.pop();
    if (ctx.op.getType() == Var1Parser.ADD) {
      floatStack.push(left + right);
    } else {
      floatStack.push(left - right);
    }
  }

  @Override
  public void enterExprLogicalAndOr(Var1Parser.ExprLogicalAndOrContext ctx) {
    setEquationType(EquationType.BOOLEAN);
  }

  @Override
  public void exitExprLogicalAndOr(Var1Parser.ExprLogicalAndOrContext ctx) {
    boolean right = booleanStack.pop();
    boolean left = booleanStack.pop();
    if (ctx.op.getType() == Var1Parser.LOGICAL_AND) {
      booleanStack.push(left && right);
    } else {
      booleanStack.push(left || right);
    }
  }

  @Override
  public void exitExprEquals(Var1Parser.ExprEqualsContext ctx) {
    if (equationType == EquationType.BOOLEAN) {
      boolean right = booleanStack.pop();
      boolean left = booleanStack.pop();
      booleanStack.push(left == right);
    } else if (equationType == EquationType.FLOAT) {
      setEquationType(EquationType.BOOLEAN);
      float right = floatStack.pop();
      float left = floatStack.pop();
      booleanStack.push(left == right);
    }
  }

  @Override
  public void exitIdValue(Var1Parser.IdValueContext ctx) {
    Log.scoped(DebugScopes.VARIABLES, "exitIdValue, ctx.getText = %s", ctx.getText());
    if (equationType == EquationType.FLOAT) {
      floatStack.push(variables.getAsNumber(ctx.getText()));
    } else if (equationType == EquationType.BOOLEAN) {
      booleanStack.push(variables.getAsBoolean(ctx.getText()));
    } else {
      // Equation type not yet known, push id to a temporary variable
      temporaryId = ctx.getText();
    }
  }

  @Override
  public void exitValueFloatLiteral(Var1Parser.ValueFloatLiteralContext ctx) {
    setEquationType(EquationType.FLOAT);
    Log.scoped(DebugScopes.VARIABLES, "exitValueFloatLiteral, ctx.getText = %s", ctx.getText());
    floatStack.push(Float.parseFloat(ctx.getText()));
  }

  @Override
  public void exitValueBooleanLiteral(Var1Parser.ValueBooleanLiteralContext ctx) {
    setEquationType(EquationType.BOOLEAN);
    Log.scoped(DebugScopes.VARIABLES, "exitValueBooleanLiteral, ctx.getText = %s", ctx.getText());
    booleanStack.push(Boolean.parseBoolean(ctx.getText()));
  }

  public EquationType getEquationType() {
    return equationType;
  }

  public Stack<Float> getFloatStack() {
    return floatStack;
  }

  public Stack<Boolean> getBooleanStack() {
    return booleanStack;
  }

  // If we have single variable like 'foo' we will never be able to infer the type from symbols like
  // +, -, &&, ||, etc. So we just provide the temporary variable and let the caller figure it out.
  public String getTemporaryId() {
    return temporaryId;
  }

  public enum EquationType {
    FLOAT, BOOLEAN
  }
}
