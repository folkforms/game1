// Var1 is a language for parsing variables yaml files
// For example: "foo: 1 + bar"

grammar Var1;

prog: expression_list EOF;

expression_list : expression_list expression
                | expression
                ;

expression : LEFT_RBRACKET expression RIGHT_RBRACKET              # exprInBrackets
           | expression op=(MULTIPLY | DIVIDE) expression         # exprMultiplyDivide
           | expression op=(ADD | SUBTRACT) expression            # exprAddSubtract
           | expression op=(LOGICAL_AND | LOGICAL_OR) expression  # exprLogicalAndOr
           | expression EQUALS expression                         # exprEquals
           | primitiveValue                                       # exprPrimitiveValue
           | idValue                                              # exprIdValue
           ;

primitiveValue : BOOLEAN_LITERAL  # valueBooleanLiteral
               | STRING_LITERAL   # valueStringLiteral
               | FLOAT_LITERAL    # valueFloatLiteral
               ;

idValue : id_;

// -------- Constants --------

WHITESPACE : ' ' -> skip;
NEWLINE : [\r]?[\n]+ -> skip;
LEFT_RBRACKET : '(';
RIGHT_RBRACKET : ')';

BOOLEAN_LITERAL: TRUE | FALSE;
FLOAT_LITERAL : [-]?[0-9]+'.'?[0-9]*;
fragment ESCAPED_QUOTE : '\\"';
STRING_LITERAL : '"' ( ESCAPED_QUOTE | ~('\n'|'\r') )*? '"'
               | '\'' ( ESCAPED_QUOTE | ~('\n'|'\r') )*? '\'';

ADD : '+';
DIVIDE: '/';
MULTIPLY: '*';
SUBTRACT : '-';
LOGICAL_AND : '&&';
LOGICAL_OR : '||';
EQUALS: '==';

TRUE: 'true';
FALSE: 'false';

ID : [a-zA-Z_][a-zA-Z0-9_.]*;
id_ : ID;
