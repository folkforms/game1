package game1.variables;

import game1.variables.VariablesListener;

public class TestListener implements VariablesListener {
  private boolean wasNotified = false;

  @Override
  public void assignVariables() {
    wasNotified = true;
  }

  public boolean wasNotified() {
    return wasNotified;
  }
}
