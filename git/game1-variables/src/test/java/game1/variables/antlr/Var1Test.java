package game1.variables.antlr;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import game1.debug.DebugScopes;
import game1.variables.Variables;

class Var1Test {

  @BeforeAll
  public static void beforeAll() {
    DebugScopes.init();
    // DebugScopes.activate("Variables");
    // Log.setLevel(Log.LEVEL_DEBUG);
  }

  @Test
  void itHandlesSimpleMaths() throws IOException {
    test(4.25f, "2 * 3 + 5 / 4 - 3");
  }

  @Test
  void itHandlesNegativeNumbers() throws IOException {
    test(-1f, "-1");
    test(-3f, "(-1) - 2");
    test(-3f, "-1 - 2");
  }

  @Test
  void itHandlesBrackets() throws IOException {
    test(9f, "(1 + 2) * 3");
  }

  @Test
  void itHandlesIds() throws IOException {
    Variables variables = new Variables();
    variables.put("foo", 3);
    test(17f, "5 + foo * 4", variables);
  }

  @Test
  void itHandlesNestedIds() throws IOException {
    Variables variables = new Variables();
    variables.put("foo", 3);
    variables.put("bar", 5);
    variables.put("muk", "foo * bar");
    test(16f, "1 + muk", variables);
  }

  @Test
  void itHandlesBooleans() throws IOException {
    test(true, "true");
    test(false, "false");
  }

  @Test
  void itHandlesIdsThatResolveToBooleans() throws IOException {
    Variables variables = new Variables();
    variables.put("foo", true);
    variables.put("bar", false);
    test(true, "foo", variables);
    test(false, "bar", variables);
  }

  @Test
  void itHandlesBooleanLogic() throws IOException {
    test(true, "true && true");
    test(false, "true && false");
    test(false, "false || false");
    test(true, "false || true");
  }

  @Test
  void itHandlesBooleanLogicWithIds() throws IOException {
    Variables variables = new Variables();
    variables.put("foo", true);
    variables.put("bar", false);
    variables.put("muk", true);
    test(true, "foo && foo", variables);
    test(false, "foo && bar", variables);
    test(false, "bar || bar", variables);
    test(true, "bar || foo", variables);
    test(true, "foo && (bar || muk)", variables);
    test(true, "(bar || muk) && foo", variables);
  }

  @Test
  void itHandlesEquality() throws IOException {
    test(true, "true == true");
    test(false, "true == false");
    test(true, "false == false");
    test(false, "false == true");
    test(false, "1 == 2");
    test(true, "4 == 4");
  }

  private void test(Object expected, String inputText) throws IOException {
    test(expected, inputText, null);
  }

  private void test(Object expected, String inputText, Variables variables) throws IOException {
    Var1LanguageFile actual = new Var1LanguageFile(inputText, variables);
    assertEquals(expected, actual.getResult());
  }
}
