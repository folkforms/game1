package game1.variables;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import folkforms.textio.TextIO;
import folkforms.yaml.YamlFile;
import game1.debug.DebugScopes;

class VariablesTest {

  private Variables variables;

  @BeforeAll
  public static void beforeAll() {
    DebugScopes.init();
  }

  @BeforeEach
  public void beforeEach() {
    variables = new Variables();
  }

  @Test
  void itAddsAVariable() {
    variables.put("foo", "foo-value");
    assertEquals("foo-value", variables.getAsString("foo"));
  }

  @Test
  void itGetsAVariablesValue() {
    variables.put("foo", "foo-value");
    assertEquals("foo-value", variables.getAsString("foo"));
  }

  @Test
  void itSetsAVariablesValue() {
    variables.put("foo", "foo-value");
    assertEquals("foo-value", variables.getAsString("foo"));

    variables.set("foo", "new-foo-value");
    assertEquals("new-foo-value", variables.getAsString("foo"));
  }

  @Test
  void itNotifiesListenersThatAVariablesValueHasChanged() {
    TestListener testListener = new TestListener();

    variables.put("foo", "foo-value");
    assertEquals("foo-value", variables.getAsString("foo"));
    variables.addListener(testListener, List.of("foo"));

    variables.set("foo", "new-foo-value");
    assertEquals("new-foo-value", variables.getAsString("foo"));
    assertEquals(true, testListener.wasNotified());
  }

  @Test
  void itGetsFloatsByDefault() {
    variables.put("foo", 2f);
    assertEquals(Float.class, variables.getAsNumber("foo").getClass());
  }

  @Test
  void itCanConvertANumberToAFloat() {
    variables.put("foo", 2f);
    assertEquals(Float.class, variables.getAsNumber("foo", Float.class).getClass());
  }

  @Test
  void itCanConvertANumberToAnInt() {
    variables.put("foo", 2f);
    assertEquals(Integer.class, variables.getAsNumber("foo", Integer.class).getClass());
  }

  @Test
  void itCanConvertANumberToADouble() {
    variables.put("foo", 2f);
    assertEquals(Double.class, variables.getAsNumber("foo", Double.class).getClass());
  }

  @Test
  void itAddsTwoNumbers() {
    variables.put("foo", "1+2");
    assertEquals(3, (float) variables.getAsNumber("foo"));
  }

  @Test
  void itAddsSeveralNumbers() {
    variables.put("foo", "1+2 + 5 + 10");
    assertEquals(18, (float) variables.getAsNumber("foo"));
  }

  @Test
  void itHandlesVariablesThatReferenceOtherVariables() {
    variables.put("foo", 22f);
    variables.put("bar", "foo");
    assertEquals(22, (float) variables.getAsNumber("bar"));
  }

  @Test
  void itAddsAVariableAndANumber() {
    variables.put("foo", "5");
    variables.put("bar", "foo + 2");
    assertEquals(7, (float) variables.getAsNumber("bar"));
  }

  @Test
  void itAddsTwoVariables() {
    variables.put("foo", "5");
    variables.put("bar", "6");
    variables.put("muk", "foo + bar");
    assertEquals(11, (float) variables.getAsNumber("muk"));
  }

  @Test
  void itHandlesComplicatedEquations() {
    variables.put("foo", "5");
    variables.put("bar", "6");
    variables.put("muk", "foo + bar / 2");
    assertEquals(8, (float) variables.getAsNumber("muk"));
  }

  @Test
  void itHandlesNegativeVariables() {
    variables.put("foo", "-5");
    variables.put("bar", "-6");
    variables.put("muk", "foo + bar");
    assertEquals(-11f, (float) variables.getAsNumber("muk"));
  }

  @Test
  void itHandlesMultipleNegativeVariables2() {
    variables.put("foo", "-4");
    variables.put("bar", "-5");
    variables.put("muk", "foo - 1 + bar");
    assertEquals(-10, (float) variables.getAsNumber("muk"));
  }

  @Test
  void itHandlesMultipleNegativeVariables3() {
    variables.put("foo", "-4");
    variables.put("bar", "-6");
    variables.put("muk", "foo - 1 - bar");
    assertEquals(1, (float) variables.getAsNumber("muk"));
  }

  @Test
  void itSubtractsTwoNumbers() {
    variables.put("foo", "5 - 6");
    assertEquals(-1, (float) variables.getAsNumber("foo"));
  }

  @Test
  void itSubtractsAVariableFromANumber() {
    variables.put("foo", "5");
    variables.put("bar", "3 - foo");
    assertEquals(-2, (float) variables.getAsNumber("bar"));
  }

  @Test
  void itSubtractsTwoVariables() {
    variables.put("foo", "5");
    variables.put("bar", "6");
    variables.put("muk", "foo - bar");
    assertEquals(-1, (float) variables.getAsNumber("muk"));
  }

  @Test
  void itHandlesBooleans() {
    variables.put("foo", true);
    variables.put("bar", false);
    assertEquals(true, variables.getAsBoolean("foo"));
    assertEquals(false, variables.getAsBoolean("bar"));
  }

  @Test
  void itResolvesBooleanLogic() {
    variables.put("foo", true);
    variables.put("bar.muk", false);
    assertEquals(true, variables.resolveAsBoolean("foo || bar.muk"));
    assertEquals(false, variables.resolveAsBoolean("foo && bar.muk"));
  }

  @Test
  void itResolvesBooleanEquality() {
    variables.put("foo", true);
    assertEquals(true, variables.resolveAsBoolean("foo == true"));
    assertEquals(false, variables.resolveAsBoolean("foo == false"));
  }

  @Test
  void itLoadsVariablesFromAYamlFile() throws IOException, ClassNotFoundException {
    String input = new TextIO().readAsString("src/test/resources/variables/variables.yaml");
    YamlFile yamlFile = new YamlFile(input);
    variables.applyYamlFile(yamlFile);
    float x = variables.getAsNumber("foo.bar.x");
    assertEquals(10f, x);
    assertEquals(30f, (float) variables.getAsNumber("foo.muk.x"));
    assertEquals(List.of("aaa", "bb", "c"), variables.getAsObject("list.one"));
  }

  @Test
  void itLoadsQuotedStringsFromAYamlFile() throws IOException, ClassNotFoundException {
    String input = new TextIO().readAsString("src/test/resources/variables/variables.yaml");
    YamlFile yamlFile = new YamlFile(input);
    variables.applyYamlFile(yamlFile);
    assertEquals("Hello \"Jim\"!", variables.getAsString("dialog.level_1.hello"));
    assertEquals("Goodbye \"Jim\"", variables.getAsString("dialog.level_1.goodbye"));
  }

  @Test
  void itHandlesVector2fs() {
    variables.put("foo", "1,2");
    variables.put("bar", "4,5");
    assertEquals(new Vector2f(1, 2), variables.getAsVector2f("foo"));
    assertEquals(new Vector2f(4, 5), variables.getAsVector2f("bar"));
  }

  @Test
  void itHandlesVector3fs() {
    variables.put("foo", "1,2,3");
    variables.put("bar", "4,5,6");
    assertEquals(new Vector3f(1, 2, 3), variables.getAsVector3f("foo"));
    assertEquals(new Vector3f(4, 5, 6), variables.getAsVector3f("bar"));
  }

  @Test
  void itHandlesVector4fs() {
    variables.put("foo", "1,2,3,4");
    variables.put("bar", "5,6,7,8");
    assertEquals(new Vector4f(1, 2, 3, 4), variables.getAsVector4f("foo"));
    assertEquals(new Vector4f(5, 6, 7, 8), variables.getAsVector4f("bar"));
  }

  @Test
  void itGetsAListOfVariables() {
    Variable var1 = new Variable("foo.one", "aaa");
    Variable var2 = new Variable("foo.two", "bbb");
    variables.put(var1.getName(), var1.getValue());
    variables.put(var2.getName(), var2.getValue());
    List<Variable> actual = variables.listVariables("foo.*");
    assertEquals(List.of(var1, var2), actual);
  }
}
