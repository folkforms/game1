package game1.events;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import game1.debug.DebugScopes;
import game1.events.Events;
import game1.events.EventsFactory;
import game1.events.test_fixtures.DummyEventReceivingClass;
import game1.variables.Variables;
import game1.variables.VariablesFactory;

public class EventSubscriptionsTest {

  private EventSubscriptions eventSubscriptions;
  private Events events;

  @BeforeEach
  public void beforeEach() {
    DebugScopes.init();
    Variables variables = new Variables();
    variables.put("game1.events.throw_on_error", true);
    variables.put("game1.dev.dev_mode", false);
    VariablesFactory.setInstance(variables);
    events = new Events();
    EventsFactory.setInstance(events);
    eventSubscriptions = new EventSubscriptions();
  }

  @Test
  public void itConnectsEventTypesToTheClassesThatWillReceiveTheEvents() {
    DummyEventReceivingClass d = new DummyEventReceivingClass();
    eventSubscriptions.subscribe(d);
    assertEquals(false, d.wasFooed);

    events.raise("foo");
    events.processEvents();

    assertEquals(true, d.wasFooed);
  }

  @Test
  public void itCanSendDataWithEvents() {
    DummyEventReceivingClass d = new DummyEventReceivingClass();
    eventSubscriptions.subscribe(d);

    events.raise("bar", "hello", 8.1f);
    events.processEvents();

    assertEquals("hello", d.var1);
    assertEquals(8.1f, d.var2);
  }
}
