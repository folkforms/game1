package game1.events.test_fixtures;

import game1.events.ReceiveEvent;

public class DummyEventReceivingClass {

  public boolean wasFooed = false;
  public String var1;
  public float var2;

  @ReceiveEvent("foo")
  public void foo() {
    wasFooed = true;
  }

  @ReceiveEvent("bar")
  public void bar(String var1, float var2) {
    this.var1 = var1;
    this.var2 = var2;
  }
}
