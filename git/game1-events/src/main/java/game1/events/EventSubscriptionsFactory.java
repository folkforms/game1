package game1.events;

public class EventSubscriptionsFactory {

  private static EventSubscriptions annotationProcessor;

  public static EventSubscriptions getInstance() {
    if (annotationProcessor == null) {
      annotationProcessor = new EventSubscriptions();
    }
    return annotationProcessor;
  }
}
