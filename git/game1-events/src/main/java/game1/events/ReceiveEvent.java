package game1.events;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Creates a listener that will trigger the annotated method when the given event occurs.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD })
public @interface ReceiveEvent {
  String[] value();

  /**
   * Methods annotated with {@link ReceiveEvent.DevModeOnly} will only trigger if the property
   * <code>game1.dev_mode</code> is <code>true</code>.
   */
  @Retention(RetentionPolicy.RUNTIME)
  @Target({ ElementType.METHOD })
  public @interface DevModeOnly {
  }
}
