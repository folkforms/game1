package game1.events;

public class RaiseEventCommand implements Command {

  private String event;
  private Object[] data;

  public RaiseEventCommand(String event) {
    this.event = event;
    this.data = new Object[0];
  }

  public RaiseEventCommand(String event, String data) {
    this.event = event;
    this.data = new Object[] { data };
  }

  public RaiseEventCommand(String event, Object[] data) {
    this.event = event;
    this.data = data;
  }

  @Override
  public void execute() {
    EventsFactory.getInstance().raise(event, data);
  }
}
