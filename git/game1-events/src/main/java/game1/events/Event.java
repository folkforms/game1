package game1.events;

import java.util.Optional;

public class Event {

  private String name;
  private Object[] data = null;
  private Optional<String> sourceInfo;

  public Event(String name) {
    this.name = name;
  }

  public Event(String name, Object... data) {
    this.name = name;
    this.data = data;
  }

  public void setSourceInfo(String sourceInfo) {
    this.sourceInfo = Optional.of(sourceInfo);
  }

  public String getName() {
    return name;
  }

  public Object[] getData() {
    return data;
  }

  public Optional<String> getSourceInfo() {
    return sourceInfo;
  }
}
