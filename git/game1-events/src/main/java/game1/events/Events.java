package game1.events;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import folkforms.log.Log;
import game1.debug.DebugScopes;

/**
 * Event system that processes events during updates. An event consists of an event name (a string)
 * and, optionally, some data.
 *
 * @see ReceiveEvent
 */
public class Events {

  private List<EventWithData> events = new CopyOnWriteArrayList<>();
  private EventListener eventListener = new EventListener();

  /**
   * Instructs the event system to listen for events of the given type and when raised to execute
   * the given command.
   *
   * @param eventName
   *          event name to listen for
   * @param commandWithData
   *          command to execute
   */
  public void listenFor(String eventName, CommandWithData commandWithData) {
    eventListener.listenFor(eventName, commandWithData);
  }

  /**
   * Raises an event with the given name.
   *
   * @param eventName
   *          event name
   */
  public void raise(String eventName) {
    EventWithData ewd = new EventWithData(eventName);
    events.add(ewd);
    Log.scoped(DebugScopes.EVENTS, "Events: raise: %s / (no data)", eventName);
  }

  /**
   * Raises an event with the given name and data.
   *
   * @param event
   *          event name
   * @param data
   *          event data
   */
  public void raise(String event, Object... data) {
    EventWithData ewd = new EventWithData(event, data);
    events.add(ewd);
    Log.scoped(DebugScopes.EVENTS, "Events: raise: %s / %s", event, Arrays.asList(data));
  }

  /**
   * Processes all pending events. If events add more events they will be processed in the same
   * cycle, i.e. it keeps processing until the event queue is empty.
   */
  public void processEvents() {
    int index = 0;
    while (index < events.size()) {
      EventWithData ewd = events.get(index);
      Log.scoped(DebugScopes.EVENTS, "Events: processEvents: sending event '%s' to %s", ewd,
          eventListener);
      eventListener.receive(ewd);
      index++;
    }
    events.clear();
  }

  public List<EventWithData> debug_listEvents() {
    return events;
  }
}
