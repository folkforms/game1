package game1.events;

import java.util.Arrays;

/**
 * Represents an Event that was raised and the data it contains. Games should not use this.
 */
public class EventWithData {

  private String name;
  private Object[] data;

  public EventWithData(String name) {
    this.name = name;
    this.data = new Object[0];
  }

  public EventWithData(String name, String data) {
    this.name = name;
    this.data = new Object[] { data };
  }

  public EventWithData(String name, Object[] data) {
    this.name = name;
    this.data = data;
  }

  public String getName() {
    return name;
  }

  public Object[] getData() {
    return data;
  }

  @Override
  public String toString() {
    return String.format("%s:%s", name, Arrays.asList(data));
  }
}
