package game1.events;

public class EventsFactory {

  private static Events events;

  public static Events getInstance() {
    if (events == null) {
      throw new RuntimeException("Events.setInstance has not been called");
    }
    return events;
  }

  public static void setInstance(Events instance) {
    events = instance;
  }
}
