package game1.events;

public class Game1Events {
  public static final String MOUSE_MOVE = "game1event.mouse_move";
  public static final String MOUSE_CLICK = "game1event.mouse_click";
  // public static final String TAG_STARTED_LOADING = "game1event.tag_started_loading";
  // public static final String GAME_FILE_FINISHED_LOADING =
  // "game1event.game_file_finished_loading";
  public static final String CAMERA_MOVE = "game1event.camera.move";
  public static final String CAMERA_SET_POSITION_V3F = "game1event.camera.set_position_v3f";
  public static final String CAMERA_SET_POSITION = "game1event.camera.set_position";
  public static final String CAMERA_ROTATE = "game1event.camera.rotate";
  public static final String CAMERA_SET_ROTATION_V3F = "game1event.camera.set_rotation_v3f";
  public static final String CAMERA_SET_ROTATION = "game1event.camera.set_rotation";
  public static final String MOVE_TO_STATE = "game1event.move_to_state";
  public static final String TOGGLE_MENU = "game1event.menu.toggle-menu";
  public static final String DEV_TOOLS_VISIBILITY_TOGGLED = "game1event.dev_tools.visibility_toggled";
  public static final String DEV_TOOLS_MASK_COLOUR_CHANGED = "game1event.dev_tools.mask_colour_changed";
}
