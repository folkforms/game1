package game1.events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import folkforms.log.Log;
import game1.debug.DebugScopes;

/**
 * The {@link EventListener} is notified of any events that are raised, and it will invoke any
 * appropriate registered {@link CommandWithData}s. See note below on subclassing.
 */
final class EventListener {

  private Map<String, List<CommandWithData>> map = new HashMap<>();

  /**
   * Instructs the event listener to listen for an event type, and when it receives it to execute
   * the given command.
   *
   * @param eventType
   *          event type to listen for
   * @param command
   *          command to execute when the event is received
   */
  public void listenFor(String eventType, CommandWithData command) {
    List<CommandWithData> existing = map.get(eventType);
    if (existing == null) {
      existing = new ArrayList<>();
    }
    existing.add(command);
    map.put(eventType, existing);
  }

  /**
   * Invoked when the event queue receives an event. Checks the event types listened for and hands
   * the event and its data off to the appropriate handler.
   *
   * @param event
   *          the event that was raised
   */
  public void receive(EventWithData event) {
    Set<String> keys = map.keySet();
    Iterator<String> iterator = keys.iterator();
    boolean found = false;
    while (iterator.hasNext()) {
      // FIXME EVENTS: This line threw a ConcurrentModificationException
      String key = iterator.next();
      if (key.equals(event.getName())) {
        List<CommandWithData> list = map.get(key);
        list.forEach(command -> command.execute(event.getData()));
        found = true;
      }
    }
    if (!found) {
      if (event.getName().startsWith("game1.")) {
        Log.scoped(DebugScopes.EVENTS,
            "EventListener received a core event '%s' but nothing was registered to handle it. "
                + "Core events can be safely ignored if you do not care about them.",
            event);
      } else {
        Log.error(
            "EventListener received a user event '%s' but nothing was registered to handle it.",
            event);
      }
    }
  }

  @Override
  public String toString() {
    return String.format("EventListener[%s]@%s", map.keySet(), hashCode());
  }
}
