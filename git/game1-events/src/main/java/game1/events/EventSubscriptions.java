package game1.events;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

import folkforms.log.Log;
import game1.debug.DebugScopes;
import game1.variables.VariablesFactory;

public class EventSubscriptions {

  private boolean throwOnError;

  public EventSubscriptions() {
    throwOnError = VariablesFactory.getInstance().getAsBoolean("game1.events.throw_on_error");
  }

  public void subscribe(Object obj) {
    try {
      String className = obj.getClass().getName();
      Log.scoped(DebugScopes.ANNOTATIONS, "ReceiveEventUtils.process -> %s", className);
      Class<?> forName = Class.forName(className);

      for (Method method : forName.getMethods()) {
        Log.scoped(DebugScopes.ANNOTATIONS, "  -> Processing method %s", method.getName());

        Annotation[] annotations = method.getAnnotations();
        boolean devModeOnly = method.isAnnotationPresent(ReceiveEvent.DevModeOnly.class);

        // Debug code
        boolean allJdkInternal = Arrays.asList(annotations).stream()
            .allMatch(item -> item.toString().startsWith("@jdk.internal."));
        if (annotations.length > 0 && !allJdkInternal) {
          Log.scoped(DebugScopes.ANNOTATIONS, "    -> Method '%s' has annotations: %s", method,
              Arrays.toString(annotations));
        }

        if (devModeOnly && !VariablesFactory.getInstance().getAsBoolean("game1.dev.dev_mode")) {
          continue;
        }

        if (method.isAnnotationPresent(ReceiveEvent.class)) {
          ReceiveEvent annotation = method.getAnnotation(ReceiveEvent.class);
          String[] eventNames = annotation.value();

          Log.scoped(DebugScopes.ANNOTATIONS,
              "    -> @ReceiveEvent(%s) is present, adding command to event listener",
              Arrays.asList(eventNames));

          if (method.canAccess(obj) == false) {
            throw new RuntimeException(String.format(
                "Method '%s' in class %s cannot use @ReceiveEvent as it is not a public method.",
                method, obj));
          }

          for (String event : eventNames) {
            CommandWithData commandWithData = new CommandWithData() {
              @Override
              public void execute(Object... data) {
                Log.scoped(DebugScopes.ANNOTATIONS,
                    "Executing command for event '%s' -> calling %s with %s", event, method,
                    Arrays.asList(data));
                try {
                  method.invoke(obj, data);
                } catch (IllegalAccessException | IllegalArgumentException
                    | InvocationTargetException ex) {
                  Log.error(
                      "Failed to execute command for event '%s' -> calling method %s with args %s",
                      event, method, Arrays.asList(data));
                  Log.error("See the 'Caused by' part of the exception below.");
                  Log.error(ex);
                  if (throwOnError) {
                    throw new RuntimeException(ex);
                  }
                }
              }
            };

            EventsFactory.getInstance().listenFor(event, commandWithData);
          }
        }
      }
    } catch (ClassNotFoundException ex) {
      Log.error(ex);
    }
  }
}
