# Events

## Event annotations

**@ReceiveEvent(eventName)**

This method will be invoked when the event `eventName` is raised.

**@ReceiveEvent.DevModeOnly(eventName)**

This method will be invoked when the event `eventName` is raised only if the engine property `game1.dev_mode` is true.

**NB:** If a class uses either of the `ReceiveEvent` annotations it MUST call `Annotations.process(this)` in its constructor. Calling this method creates the links between the annotated methods and the events.

- You can raise an event with `Engine.EVENTS.raise(eventName, Object... data)`.
- The data will be passed as arguments to the annotated method. For example:

    Engine.EVENTS.raise("some-event", 1, 2L, "foobar");
    
    @ReceiveEvent("some-event")
    public void foo(int someInt, long someLong, String someString) {
      // ...
    }

## Useful events

Internally, loading datasets and loading the actors within a dataset fire events. Classes that subclass `LoadingScreenState` can use the `getChecker()` method and query the `LoadingScreenChecker` that is returned in order to get the current progress. See `RedOctopusLoadingScreenUpdater` for an example of how this is implemented.
