package game1.events;

/**
 * Command pattern interface. Typically used for storing data and executing a command at some future
 * point in time.
 */
public interface CommandWithData {

  public void execute(Object... data);
}
