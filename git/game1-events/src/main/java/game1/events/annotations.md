# Annotations

## Overview

Various annotations are available as convenience methods to avoid boilerplate code and to group related methods together.

For example, annotating a method with `@ReceiveEvent("player.jump")` allows you to connect the jump key to that method by creating a keybinding that raises the event "player.jump". You no longer need to create a Command object and pass around references to the Player object.

Another example is user states. The UserState class typically will want to set the mouse cursor visibility, set the active key bindings, clear the game state, and pause or unpause the game. The methods to control these actions live in different places and might not be known to the user. The `@UserState` sub-annotations provide a convenient way to group these commonly-used methods.

## Notes

Unfortunately we cannot just glob all classes and check their annotations because we need the objects, not the classes. For example, the @ReceiveEvents annotation invokes the annotated method on an object, not a class.

## List of Annotations

**@ReceiveEvent(eventName)**

See `events.md`.

**@ReceiveEvent.DevModeOnly(eventName)**

See `events.md`.

**@UserStateProps**

Contains sub-annotations that invoke commonly-used code. See the `UserStateProps` class for details.

- UserStateProps.Mouse
- UserStateProps.KeyBindings
- UserStateProps.ClearActors
- UserStateProps.Pause
- UserStateProps.Unpause

It is not necessary to call `Annotations.process(this)` for UserState classes annotated with these annotations as the superclass constructor will call it for you.

**@TickableAnnotations.TickEvenIfGamePaused**

Tickable objects with this annotation will continue to tick even if the game is paused.

It is not necessary to call `Annotations.process(this)` for classes annotated with this annotation as the annotation will be checked at runtime.
