package game1.spritesheets;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class GssCliTest {

  // ArgumentCaptor<Person> argument = ArgumentCaptor.forClass(Person.class);

  // verify(mock).doSomething(argument.capture());
  // assertEquals("John", argument.getValue().getName());
  // A captor can also be defined using the @Captor annotation:

  // @Captor ArgumentCaptor<Person> captor;
  // verify(mock).doSomething(captor.capture());
  // assertEquals("John", captor.getValue().getName());

  @Captor
  ArgumentCaptor<GssOptions> captor;

  @Mock
  GenerateSpriteSheets generateSpriteSheets;

  // @InjectMocks
  // GssCli gssCli;

  // FIXME Move this stuff into superclass once I get it working...
  AutoCloseable closeable;

  @BeforeEach
  public void openMocks() {
    closeable = MockitoAnnotations.openMocks(this);
  }

  @AfterEach
  public void releaseMocks() throws Exception {
    closeable.close();
  }

  // @Test
  // void testFoo() throws IOException {
  // GssCli.main(new String[] { "--input-file=src/test/resources/GssCliTest/game-release.yaml" });
  //
  // verify(generateSpriteSheets).generate(captor.capture());
  // List<GssOptions> actualOptionsList = captor.getValue();
  // assertEquals(2, actualOptionsList.size());
  // GssOptions gssOptions0 = actualOptionsList.get(0);
  // assertEquals("x", gssOptions0.getInputGlobs());
  //
  // GssOptions gssOptions1 = actualOptionsList.get(1);
  //
  // // GenerateSpriteSheets gss = mock(GenerateSpriteSheets.class);
  // // // ArgumentCaptor<String> valueCapture = ArgumentCaptor.forClass(List<GssOptions>);
  // // doNothing().when(gss).generateAll(captor.capture());
  // // gss.add(0, "captured");
  // //
  // // assertEquals("captured", valueCapture.getValue());
  //
  // }
}
