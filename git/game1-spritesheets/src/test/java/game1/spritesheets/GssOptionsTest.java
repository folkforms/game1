package game1.spritesheets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

class GssOptionsTest {

  @Test
  void testInputGlobsIsARequiredField() {
    try {
      Map<String, Object> options = new HashMap<>();
      options.put("spritesheet.inputFile", "foo");
      new GssOptions(options);
      fail("Expected exception to be thrown");
    } catch (RuntimeException ex) {
      assertEquals(RuntimeException.class, ex.getClass());
      assertEquals("Error: inputGlobs is a required field but was null!", ex.getMessage());
    }
  }
}
