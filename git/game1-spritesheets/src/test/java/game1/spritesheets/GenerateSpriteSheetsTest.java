package game1.spritesheets;

class GenerateSpriteSheetsTest {

  // /**
  // * Verifies that the data read from <code>policeman_run.json</code> and
  // * <code>policeman_walk.json</code> is correct.
  // *
  // * @throws IOException
  // * if an error occurs reading or writing the data
  // */
  // @Test
  // void testTimings() throws IOException {
  // FileUtils.rmrf("./spritesheets/packed/testTimings_output");
  // new File("./spritesheets/packed/testTimings_output").mkdirs();
  //
  // String[] args = { //
  // "--root=./spritesheets", //
  // "--output.folder=./packed/testTimings_output", //
  // "--output.prefix=timings", //
  // "--input=test_data/test_timings/policeman*.png", //
  // "--packer=Basic" //
  // };
  // GenerateSpriteSheet.main(args);
  //
  // List<String> expected = Arrays.asList(//
  // "animation: policeman_run.png", //
  // " frame: 0,0,7,12", //
  // " frame: 7,0,7,12", //
  // " timings: 12,34", //
  // "animation: policeman_walk.png", //
  // " frame: 14,0,7,12", //
  // " frame: 21,0,7,12", //
  // " timings: 56,78"//
  // );
  // List<String> actual = TextIO
  // .readAsList("./spritesheets/packed/testTimings_output/timings_1.txt");
  // assertEquals(expected, actual);
  //
  // FileUtils.rmrf("./spritesheets/packed/testTimings_output");
  // }
  //
  // @Test
  // void testCreateMasterFile() throws IOException {
  // FileUtils.rmrf("./spritesheets/packed/testCreateMasterFile_output");
  // new File("./spritesheets/packed/testCreateMasterFile_output").mkdirs();
  //
  // String[] args = { //
  // "--root=./spritesheets", //
  // "--output.folder=packed/testCreateMasterFile_output", //
  // "--output.prefix=timings", //
  // "--input=test_data/test_timings/policeman*.png", //
  // "--packer=Basic" //
  // };
  // GenerateSpriteSheet.main(args);
  //
  // List<String> expected = Arrays.asList("timings_1");
  // List<String> actual = TextIO
  // .readAsList("./spritesheets/packed/testCreateMasterFile_output/timings.spritesheet");
  // assertEquals(expected, actual);
  //
  // FileUtils.rmrf("./spritesheets/packed/testCreateMasterFile_output");
  // }
}
