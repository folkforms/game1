package game1.spritesheets;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import folkforms.fileutils.FileUtils;
import folkforms.log.Log;

/**
 * Command-line interface for {@link GenerateSpriteSheets}.
 */
public class GssCli {

  public static void main(String[] args) throws IOException {
    if (Arrays.stream(args).anyMatch(x -> x.equals("--skip=true"))) {
      Log.info("Skipping spritesheet generation");
      return;
    } else {
      args = Arrays.stream(args).filter(x -> !x.startsWith("--skip=")).toArray(String[]::new);
    }

    List<String> inputFiles = new ArrayList<>();
    if (args.length == 0) {
      throw new IOException(String.format(
          "No input files specified for spritesheet generation. You need to add an argument to the 'spritesheets' execution in pom.xml. "
              + "For example: <argument>src/main/resources/properties/foobar-spritesheet.properties</argument>"));
    } else {
      Arrays.stream(args).forEach(arg -> inputFiles.add(arg));
    }
    Log.info("Input files: %s", inputFiles);
    for (int i = 0; i < inputFiles.size(); i++) {
      String inputFile = inputFiles.get(i);
      Log.info("Generating: %s", inputFile);
      new GenerateSpriteSheets().generate(parsePropertiesFile(inputFile));
    }
  }

  /**
   * Parses spritesheet data from a properties file.
   *
   * @param inputFile
   *          input properties file
   * @return a GssOptions file containing spritesheet data from the input file
   * @throws IOException
   *           if an error occurs reading the file
   */
  private static GssOptions parsePropertiesFile(String inputFile) throws IOException {
    Properties p = new Properties();
    p.load(new FileReader(inputFile));
    Map<String, Object> map = new HashMap<>();
    p.entrySet().iterator()
        .forEachRemaining(entry -> map.put(entry.getKey().toString(), entry.getValue()));
    map.put("spritesheet.inputFile", inputFile);
    FileUtils fileUtils = new FileUtils();
    map.put("spritesheet.outputFilename",
        fileUtils.filenameOnly(fileUtils.removeExtension(inputFile)));
    return new GssOptions(map);
  }
}
