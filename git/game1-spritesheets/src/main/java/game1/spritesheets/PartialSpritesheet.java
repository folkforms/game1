package game1.spritesheets;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import folkforms.fileutils.FileUtils;
import folkforms.log.Log;
import folkforms.stringutils.StringUtils;
import folkforms.textio.TextIO;
import game1.spritesheets.aseprite.AsepriteJson;

/**
 * Represents a partial spritesheet that may be part of a larger spritesheet.
 */
class PartialSpritesheet {
  private Map<String, Rectangle> partialMap;
  private GssOptions gssOptions;
  private String filenameNoExt, imageFilename, textFilename;
  private int imageWidth, imageHeight;
  private List<String> additionalFiles = new ArrayList<>();

  PartialSpritesheet(Map<String, Rectangle> partialMap, GssOptions gssOptions, int index) {
    this.partialMap = partialMap;
    this.gssOptions = gssOptions;
    filenameNoExt = String.format("%s_%s", gssOptions.getOutputFilename(), index);
    String f = String.format("%s/%s", gssOptions.getFullOutputFolder(), filenameNoExt);
    this.imageFilename = f + ".png";
    this.textFilename = f + ".txt";
  }

  String getFilenameNoExt() {
    return filenameNoExt;
  }

  /**
   * Calculates the size of the output image.
   */
  private void calculateOutputImageSize() {
    imageWidth = 0;
    imageHeight = 0;
    int minSize = gssOptions.getMinSize();
    int maxSize = gssOptions.getMaxSize();

    // Find the right-most sprite and the bottom-most sprite in the map
    for (String s : partialMap.keySet()) {
      Rectangle r = partialMap.get(s);
      if (r.x + r.width > imageWidth) {
        imageWidth = r.x + r.width;
      }
      if (r.y + r.height > imageHeight) {
        imageHeight = r.y + r.height;
      }
    }

    // Pad the final image so width and height are both powers of two
    if (gssOptions.getPadToPowerOfTwo()) {
      List<Integer> powersOfTwo = createPowersOfTwoList(minSize, maxSize);
      int oldWidth = imageWidth;
      int oldHeight = imageHeight;

      // Check width and height and see if they need padding
      for (int i = 0; i < powersOfTwo.size(); i++) {
        if (oldWidth > powersOfTwo.get(i)) {
          imageWidth = powersOfTwo.get(i + 1);
        }
        if (oldHeight > powersOfTwo.get(i)) {
          imageHeight = powersOfTwo.get(i + 1);
        }
      }
      if (imageWidth != oldWidth || imageHeight != oldHeight) {
        Log.info("Padding from %sx%s to %sx%s due to --pad-to-power-of-two option", oldWidth,
            oldHeight, imageWidth, imageHeight);
      }
    }

    // Pad width/height to minSize if too small
    if (imageWidth < minSize) {
      Log.info("Padding width from %s to min size of %s", imageWidth, minSize);
      imageWidth = minSize;
    }
    if (imageHeight < minSize) {
      Log.info("Padding height from %s to min size of %s", imageHeight, minSize);
      imageHeight = minSize;
    }
  }

  /**
   * Create list of powers of two from minSize to maxSize.
   *
   * @param minSize
   *          list start
   * @param maxSize
   *          list end
   * @return a list of powers of two from minSize to maxSize, both inclusive
   */
  private List<Integer> createPowersOfTwoList(int minSize, int maxSize) {
    List<Integer> powersOfTwo = new ArrayList<>();
    powersOfTwo.add(maxSize);
    while (powersOfTwo.get(powersOfTwo.size() - 1) > minSize) {
      powersOfTwo.add(powersOfTwo.get(powersOfTwo.size() - 1) / 2);
    }
    Collections.reverse(powersOfTwo);
    return powersOfTwo;
  }

  void save() throws IOException {
    calculateOutputImageSize();
    saveSpriteSheetImage();
    saveSpriteSheetText();
  }

  /**
   * Saves the partial spritesheet image to disk.
   *
   * @throws IOException
   *           if an error occurs reading or writing the data
   */
  private void saveSpriteSheetImage() throws IOException {
    // FIXME Rename these variables...
    BufferedImage masterImage = new BufferedImage(imageWidth, imageHeight,
        BufferedImage.TYPE_4BYTE_ABGR);
    for (String spriteImage : partialMap.keySet()) {
      Rectangle spriteLocation = partialMap.get(spriteImage);
      Graphics g = masterImage.getGraphics();
      g.drawImage(ImageIO.read(new File(spriteImage)), spriteLocation.x, spriteLocation.y, null);
    }
    Log.debug("saveSpriteSheetImage: saving file '%s'", imageFilename);
    ImageIO.write(masterImage, "PNG", new File(imageFilename));
  }

  /**
   * Saves the partial spritesheet text to disk.
   *
   * @throws IOException
   *           if an error occurs writing the data
   */
  private void saveSpriteSheetText() throws IOException {
    boolean allowWxH = gssOptions.getAllowWxH();
    List<String> output = new ArrayList<>();
    FileUtils fileUtils = new FileUtils();
    for (String spriteImage : partialMap.keySet()) {
      Rectangle spriteLocation = partialMap.get(spriteImage);
      String filenameOnly = fileUtils.filenameOnly(spriteImage);
      if (hasAsepriteJsonFile(spriteImage)) {
        output.add(String.format("animation: %s", filenameOnly));
        output.addAll(readAnimationDataFromJson(spriteImage, spriteLocation.x, spriteLocation.y));
      } else if (isAnimation(filenameOnly) && allowWxH) {
        output.add(String.format("animation: %s", filenameOnly));
        output.addAll(listAnimationLocations(filenameOnly, spriteLocation));
        output.add(String.format("    timings: 160"));
      } else if (hasFontDataFiles(spriteImage)) {
        output.add(String.format("font: %s", filenameOnly));
        output.add(String.format("    frame: %s,%s,%s,%s", spriteLocation.x, spriteLocation.y,
            spriteLocation.width, spriteLocation.height));
        additionalFiles
            .add(fileUtils.removeExtension(removeSrcMainResources(spriteImage)) + ".fontdata");
      } else {
        output.add(String.format("image: %s", filenameOnly));
        output.add(String.format("    frame: %s,%s,%s,%s", spriteLocation.x, spriteLocation.y,
            spriteLocation.width, spriteLocation.height));
      }
    }
    Log.debug("saveSpriteSheetText: saving file '%s'", textFilename);
    new TextIO().write(output, textFilename);
  }

  /**
   * Checks if the given filename is an animation. If it ends with "_WxH.png" it is considered to be
   * an animation.
   *
   * @param filenameOnly
   *          the filename to check
   * @return true if the file is an animation, false otherwise
   */
  private boolean isAnimation(String filenameOnly) {
    return filenameOnly.matches(".*_\\d*x\\d*\\.png");
  }

  /**
   * Calculates the animation locations based on the image size and the animation frame size. Takes
   * the rectangle representing the entire image and chops it up into smaller pieces.
   *
   * @param filenameOnly
   *          the image filename
   * @param rectangle
   *          the rectangle that represents the entire image
   * @return a list of sub-rectangles representing the individual animation frames
   */
  private List<String> listAnimationLocations(String filenameOnly, Rectangle rectangle) {
    List<String> subRectangles = new ArrayList<>();
    List<String> groups = StringUtils.listGroups(filenameOnly, ".*_(\\d*)x(\\d*)\\.png");
    int w = Integer.parseInt(groups.get(0));
    int h = Integer.parseInt(groups.get(1));
    boolean isHorizontalStrip = rectangle.height == h;
    if (isHorizontalStrip) {
      for (int i = 0; i < rectangle.width / w; i++) {
        subRectangles
            .add(String.format("    frame: %s,%s,%s,%s", rectangle.x + i * w, rectangle.y, w, h));
      }
    } else {
      for (int i = 0; i < rectangle.height / h; i++) {
        subRectangles
            .add(String.format("    frame: %s,%s,%s,%s", rectangle.x, rectangle.y + i * h, w, h));
      }
    }
    return subRectangles;
  }

  /**
   * Checks if the file has an associated .json file. If so, this file will be used to load
   * animation details.
   *
   * @param filename
   *          the file we want to check
   * @return true of the file has an associated .json file, false otherwise
   */
  private boolean hasAsepriteJsonFile(String filename) {
    return new File(new FileUtils().removeExtension(filename) + ".json").exists();
  }

  private boolean hasFontDataFiles(String filename) {
    return new File(new FileUtils().removeExtension(filename) + ".fontdata").exists();
  }

  /**
   * Reads the animation frame data from the associated .json file.
   *
   * @param filename
   *          image filename
   * @param xOffset
   *          X offset of the image within the spritesheet
   * @param yOffset
   *          Y offset of the image within the spritesheet
   * @return a list of sub-rectangles representing the individual animation frames
   * @throws JsonSyntaxException
   *           if an error occurs processing JSON
   * @throws JsonIOException
   *           if an error occurs reading the JSON
   * @throws FileNotFoundException
   *           if the file cannot be found
   */
  private List<String> readAnimationDataFromJson(String filename, int xOffset, int yOffset)
      throws JsonSyntaxException, JsonIOException, FileNotFoundException {
    List<String> subRectangles = new ArrayList<>();
    AsepriteJson asepriteJson = AsepriteJson
        .parseJSON(new FileUtils().removeExtension(filename) + ".json");

    // Locations
    Rectangle[] framePositions = asepriteJson.getFramePositions();
    for (int i = 0; i < framePositions.length; i++) {
      Rectangle r = framePositions[i];
      subRectangles.add(
          String.format("    frame: %s,%s,%s,%s", r.x + xOffset, r.y + yOffset, r.width, r.height));
    }

    // Timings
    int[] array = asepriteJson.getFrameDurations();
    StringBuffer sb = new StringBuffer("    timings: ");
    sb.append(Integer.toString(array[0]));
    for (int i = 1; i < array.length; i++) {
      sb.append(",").append(array[i]);
    }
    subRectangles.add(sb.toString());

    return subRectangles;
  }

  private String removeSrcMainResources(String path) {
    String srcMainResources = "src/main/resources/";
    int index = path.indexOf(srcMainResources);
    if (index == -1) {
      return path;
    }
    return path.substring(index + srcMainResources.length());
  }

  public List<String> listAdditionalFiles() {
    return additionalFiles;
  }
}
