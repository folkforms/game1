package game1.spritesheets.aseprite;

import java.awt.Rectangle;

public class Frame {

  // {
  // "filename": "henchard_sitting_drinking 0.ase",
  // "frame": { "x": 0, "y": 0, "w": 148, "h": 160 },
  // "rotated": false,
  // "trimmed": false,
  // "spriteSourceSize": { "x": 0, "y": 0, "w": 148, "h": 160 },
  // "sourceSize": { "w": 148, "h": 160 },
  // "duration": 1000
  // },

  private InnerFrame frame;
  private int duration;

  public Rectangle getRectangle() {
    return frame.getRectangle();
  }

  public int getDuration() {
    return duration;
  }

  @Override
  public String toString() {
    return String.format("Frame[%s,%s]", frame, duration);
  }
}
