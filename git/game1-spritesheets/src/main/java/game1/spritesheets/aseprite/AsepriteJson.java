package game1.spritesheets.aseprite;

import java.awt.Rectangle;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import folkforms.log.Log;

public class AsepriteJson {

  private List<Frame> frames;
  private int xOffset, yOffset;

  public static AsepriteJson parseJSON(String filename)
      throws JsonSyntaxException, JsonIOException, FileNotFoundException {
    Gson gson = new GsonBuilder().create();
    try {
      AsepriteJson aseprite = gson.fromJson(new FileReader(new File(filename)), AsepriteJson.class);
      return aseprite;
    } catch (JsonSyntaxException ex) {
      Log.error(
          "Could not parse Aseprite JSON file '%s'. Make sure you exported the JSON data as an Array and not a Hash.",
          filename);
      throw ex;
    }
  }

  public int getXOffset() {
    return xOffset;
  }

  public int getYOffset() {
    return yOffset;
  }

  public void setOffset(int newXOffset, int newYOffset) {
    this.xOffset = newXOffset;
    this.yOffset = newYOffset;
  }

  public int[] getFrameDurations() {
    int[] frameDurations = new int[frames.size()];
    for (int i = 0; i < frames.size(); i++) {
      frameDurations[i] = frames.get(i).getDuration();
    }
    return frameDurations;
  }

  public Rectangle[] getFramePositions() {
    Rectangle[] framePositions = new Rectangle[frames.size()];
    for (int i = 0; i < frames.size(); i++) {
      framePositions[i] = frames.get(i).getRectangle();
    }
    return framePositions;
  }

  @Override
  public String toString() {
    return String.format("Aseprite[%s]", frames);
  }

  // Tests
  public static void main(String[] args)
      throws JsonSyntaxException, JsonIOException, FileNotFoundException {
    AsepriteJson test = AsepriteJson
        .parseJSON("./spritesheets2/test_input_1/b/henchard_sitting_drinking.json");
    System.out.println(test);

    // Frame durations
    int[] frameDurations = test.getFrameDurations();
    StringBuffer sb = new StringBuffer();
    sb.append(frameDurations[0]);
    for (int i = 1; i < frameDurations.length; i++) {
      sb.append(", ").append(frameDurations[i]);
    }
    System.out.println(sb.toString());

    // Frame positions
    test.setOffset(5, 5);
    Rectangle[] positions = test.getFramePositions();
    for (Rectangle r : positions) {
      r.translate(test.getXOffset(), test.getYOffset());
      System.out.println(r);
    }
  }
}
