package game1.spritesheets;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import folkforms.fileutils.FileUtils;
import folkforms.log.Log;
import folkforms.textio.TextIO;
import game1.spritesheets.packers.Packer;

/**
 * Generates spritesheets based on the given options.
 */
class GenerateSpriteSheets {

  /**
   * Generates a single spritesheet, which may itself be composed of multiple partial spritesheets.
   *
   * @param gssOptions
   *          GssOptions object
   * @throws IOException
   *           if an error occurs reading or writing the data
   */
  public void generate(GssOptions gssOptions) throws IOException {
    if (gssOptions.getDebug()) {
      Log.setLevel(Log.LEVEL_DEBUG);
    }
    Log.debug("generate: gssOptions = %s", gssOptions);
    List<String> inputFiles = globImageFiles(gssOptions);
    Map<String, Rectangle> spriteMap = createSpriteMap(inputFiles, gssOptions);
    Packer packer = gssOptions.getPacker();
    packer.pack(spriteMap);
    writeSpritesheet(gssOptions);
    copyTweaksFile(gssOptions);
    Log.resetLevel();
  }

  /**
   * Glob up all the spritesheet files into a list.
   *
   * @return list of the files to be included in the spritesheet
   * @throws IOException
   *           if an error occurs reading or writing the data
   */
  private List<String> globImageFiles(GssOptions gssOptions) throws IOException {
    Log.debug("globImageFiles");
    List<String> result = new ArrayList<>();
    String rootFolder = gssOptions.getRootFolder();
    FileUtils fileUtils = new FileUtils();

    List<String> fullInputGlobs = new ArrayList<>();
    String[] inputGlobs = gssOptions.getInputGlobs();
    for (String g : inputGlobs) {
      String glob = rootFolder.length() > 0 ? String.format("%s/%s", rootFolder, g) : g;
      Log.debug("  globbing: %s (rootFolder = %s, g = %s)", glob, rootFolder, g);
      fullInputGlobs.add(glob);
      List<String> globbedFiles = fileUtils.glob(glob);
      if (globbedFiles.isEmpty()) {
        throw new IOException(String.format(
            "Error: Spritesheet '%s.spritesheet' has a glob '%s' that returned zero files. Are you sure the path is correct? fullInputGlobs=%s, gssOptions=%s",
            gssOptions.getOutputFilename(), g, fullInputGlobs, gssOptions));
      }
      result.addAll(globbedFiles);
    }

    List<String> fullIgnoreGlobs = new ArrayList<>();
    String[] ignoreGlobs = gssOptions.getIgnoreGlobs();
    for (String g : ignoreGlobs) {
      String glob = rootFolder.length() > 0 ? String.format("%s/%s", rootFolder, g) : g;
      Log.debug("  ignoring: %s (rootFolder = %s, g = %s)", glob, rootFolder, g);
      fullIgnoreGlobs.add(glob);
      result.removeAll(fileUtils.glob(glob));
    }

    Log.debug("  return value = %s", result);

    if (result.size() == 0) {
      throw new IOException(String.format(
          "Error: Spritesheet '%s.spritesheet' has zero files. Are you sure the paths are correct? fullInputGlobs=%s, fullIgnoreGlobs=%s, %s",
          gssOptions.getOutputFilename(), fullInputGlobs, fullIgnoreGlobs, gssOptions));
    }
    return result;
  }

  /**
   * Reads the input files and records their widths and heights as a
   * <code>Map&lt;String, Rectangle&gt;</code>.
   *
   * @param inputFiles
   *          list of input files
   * @param gssOptions
   *          options
   * @return a map of filenames -> rectangles representing the image dimensions
   * @throws IOException
   *           if an error occurs reading any of the images
   */
  private Map<String, Rectangle> createSpriteMap(List<String> inputFiles, GssOptions gssOptions)
      throws IOException {
    Map<String, Rectangle> map = new LinkedHashMap<>();
    for (String filename : inputFiles) {
      BufferedImage bi = ImageIO.read(new File(filename));
      if (bi == null) {
        throw new IOException(String.format("Error reading image file %s", filename));
      }
      int width = bi.getWidth();
      int height = bi.getHeight();
      validateImageIsSmallerThanMaxSpritesheetSize(width, height, filename, gssOptions);
      Rectangle r = new Rectangle(0, 0, width, height);
      map.put(filename, r);
    }
    return map;
  }

  /**
   * Validates that a given image can fit inside a spritesheet. Will fail if attempting to use e.g.
   * a 5000x5000 image when max spritesheet size is 4096.
   *
   * @param width
   *          image width
   * @param height
   *          image height
   * @param filename
   *          image filename
   * @param gssOptions
   *          options
   */
  private void validateImageIsSmallerThanMaxSpritesheetSize(int width, int height, String filename,
      GssOptions gssOptions) {
    int maxSize = gssOptions.getMaxSize();
    if (width > maxSize || height > maxSize) {
      String message = String.format(
          "Error: Cannot create spritesheet. Image '%s' (%s x %s) is larger than spritesheet max "
              + "size %s. You can override this by using '--max-size=<size>' although this may "
              + "break compatibility with certain graphics cards.",
          filename, width, height, maxSize);
      throw new RuntimeException(message);
    }
  }

  /**
   * Write the partial spritesheets to their respective files, and writes a master file that
   * references them.
   *
   * @throws IOException
   *           if an error occurs writing the files
   */
  private void writeSpritesheet(GssOptions gssOptions) throws IOException {
    String outputFilename = gssOptions.getOutputFilename();
    String fullOutputFolder = gssOptions.getFullOutputFolder();
    new File(fullOutputFolder).mkdirs();

    // Save the partial spritesheets
    Packer packer = gssOptions.getPacker();
    List<String> masterTextFile = new ArrayList<>();
    List<Map<String, Rectangle>> output = packer.getOutput();
    Log.debug("writeSpritesheet: packer generated %s partial spritesheet(s)", output.size());
    for (int i = 0; i < output.size(); i++) {
      Map<String, Rectangle> partialMap = output.get(i);
      PartialSpritesheet pss = new PartialSpritesheet(partialMap, gssOptions, i);
      masterTextFile.add(pss.getFilenameNoExt());
      pss.save();
      masterTextFile.addAll(pss.listAdditionalFiles());
    }

    // Save a master file containing a list of the spritesheet parts
    String masterTextFileFilename = String.format("%s/%s.spritesheet", fullOutputFolder,
        outputFilename);
    new TextIO().write(masterTextFile, masterTextFileFilename);
  }

  private void copyTweaksFile(GssOptions gssOptions) throws IOException {
    FileUtils fileUtils = new FileUtils();
    String tweaksFile = fileUtils.removeExtension(gssOptions.getInputFile())
        + ".spritesheet-tweaks";
    if (!new File(tweaksFile).exists()) {
      return;
    }
    String fullOutputFolder = gssOptions.getFullOutputFolder();
    String dest = fullOutputFolder + "/" + fileUtils.filenameOnly(tweaksFile);
    fileUtils.cp(tweaksFile, dest);
  }
}
