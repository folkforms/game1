package game1.spritesheets.packers;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import folkforms.log.Log;

/**
 * Packs images from largest to smallest but alternates between searching horizontally and
 * vertically for more space.
 * 
 * <ol>
 * <li>Sort images by size</li>
 * <li>Put largest image on the sheet</li>
 * <li>Attempt to put the next image on the sheet by drawing an imaginary rectangle around all the
 * items placed so far and attempt to fit it inside that rectangle. If it does not fit find a new
 * place outside of that alternating between horizontal and vertical.</li>
 * <li>If the image does not fit anywhere then put it in the 'nextSheetItems' pile</li>
 * <li>If the next image is the same proportions then skip that image</li>
 * <li>Go to step 3 if there are more images left in 'allItems'</li>
 * <li>If there are no images left in 'allItems' then if 'nextSheetItems' is also empty we are
 * finished.</li>
 * <li>If 'nextSheetItems' is not empty then create a new sheet, move values from 'nextSheetItems'
 * to 'allItems' and go to step 2.</li>
 * </ol>
 */
public class DiagonalPacker extends Packer {

  private boolean horizontal = true;

  public DiagonalPacker(int maxSize) {
    super(maxSize);
  }

  @Override
  public void pack(Map<String, Rectangle> map) {
    List<Rectangle> allItems = getSortedItems(map);
    List<Rectangle> nextSheetItems = new ArrayList<>();

    int totalItems = map.keySet().size();
    int currentItemNum = 1;
    int maxXSoFar = 0, maxYSoFar = 0;

    Log.debug("start");
    while (allItems.size() > 0 || nextSheetItems.size() > 0) {

      // Create new sheet
      Map<String, Rectangle> newSheet = new LinkedHashMap<>();
      sheets.add(newSheet);
      Map<String, Rectangle> targetSheet = newSheet;
      // Move all items from nextSheetItems to allItems
      while (nextSheetItems.size() > 0) {
        allItems.add(nextSheetItems.remove(0));
      }
      Log.debug("Created new sheet");

      // Put largest image on the sheet
      Rectangle largestItem = allItems.remove(0);
      targetSheet.put(getKey(map, largestItem), largestItem);
      maxXSoFar = largestItem.width;
      maxYSoFar = largestItem.height;
      Log.debug("Put largest item onto sheet");
      Log.info("Packed item %s of %s", currentItemNum++, totalItems);

      while (allItems.size() > 0) {
        // Attempt to put next image on the sheet
        Rectangle currentItem = allItems.remove(0);
        Log.debug("Checking additional item (area %s)", currentItem.width * currentItem.height);
        boolean added = false;

        // Try to fit it within maxXSoFar,maxYSoFar boundaries
        for (int y = 0; y < maxYSoFar - currentItem.height; y += 4) {
          for (int x = 0; x < maxXSoFar - currentItem.width; x++) {
            if (!foundOverlap(currentItem, x, y, targetSheet)) {
              // Add item
              currentItem.translate(x, y);
              targetSheet.put(getKey(map, currentItem), currentItem);
              added = true;
              Log.debug("Added additional item (area %s) to sheet",
                  currentItem.width * currentItem.height);
              Log.info("Packed item %s of %s", currentItemNum++, totalItems);
              horizontal = false;
            }
          }
        }

        // Try to fit it normally
        if (!added) {
          if (horizontal) {
            for (int y = 0; y < maxSize - currentItem.height; y += 4) {
              for (int x = maxXSoFar; x < maxSize - currentItem.width; x++) {
                if (!foundOverlap(currentItem, x, y, targetSheet)) {
                  // Add item
                  currentItem.translate(x, y);
                  targetSheet.put(getKey(map, currentItem), currentItem);
                  added = true;
                  Log.debug("Added additional item (area %s) to sheet",
                      currentItem.width * currentItem.height);
                  Log.info("Packed item %s of %s", currentItemNum++, totalItems);
                  horizontal = !horizontal;
                }
              }
            }
          } else {
            for (int x = 0; x < maxSize - currentItem.width; x += 4) {
              for (int y = maxYSoFar; y < maxSize - currentItem.height; y++) {
                if (!foundOverlap(currentItem, x, y, targetSheet)) {
                  // Add item
                  currentItem.translate(x, y);
                  targetSheet.put(getKey(map, currentItem), currentItem);
                  added = true;
                  Log.debug("Added additional item (area %s) to sheet",
                      currentItem.width * currentItem.height);
                  Log.info("Packed item %s of %s", currentItemNum++, totalItems);
                  horizontal = !horizontal;
                }
              }
            }
          }
        }
        // If the image does not fit, put it in the 'nextSheet' pile and go to step 2
        if (!added) {
          Log.debug("Pushed item (area %s) to nextSheetItems",
              currentItem.width * currentItem.height);
          nextSheetItems.add(currentItem);

          // Automatically skip next item if it is the same proportions as the previous item
          while (allItems.size() > 0 && allItems.get(0).width == currentItem.width
              && allItems.get(0).height == currentItem.height) {
            Log.debug("Skipping next item (area %s) because it is of equal size",
                currentItem.width * currentItem.height);
            nextSheetItems.add(allItems.remove(0));
          }
        } else {
          maxXSoFar = Math.max(maxXSoFar, currentItem.x);
          maxYSoFar = Math.max(maxYSoFar, currentItem.y);
        }
      }
    }
  }

  private boolean foundOverlap(Rectangle currentItem, int dx, int dy,
      Map<String, Rectangle> targetSheet) {
    for (Rectangle r : targetSheet.values()) {
      currentItem.translate(dx, dy);
      if (currentItem.intersects(r)) {
        currentItem.translate(-dx, -dy);
        return true;
      }
      currentItem.translate(-dx, -dy);
    }
    return false;
  }

  private List<Rectangle> getSortedItems(Map<String, Rectangle> map) {
    List<Rectangle> list = map.values().stream().sorted(new AreaComparator())
        .collect(Collectors.toList());
    return list;
  }

  private String getKey(Map<String, Rectangle> map, Rectangle rectangle) {
    for (String key : map.keySet()) {
      if (map.get(key) == rectangle) {
        return key;
      }
    }
    throw new RuntimeException(String.format("Error: No key found for rectangle: %s", rectangle));
  }

  private class AreaComparator implements Comparator<Rectangle> {
    @Override
    public int compare(Rectangle r1, Rectangle r2) {
      int area1 = r1.width * r1.height;
      int area2 = r2.width * r2.height;
      if (area1 < area2) {
        return 1;
      } else if (area1 > area2) {
        return -1;
      } else {
        return 0;
      }
    }
  }

  @Override
  public String toString() {
    return String.format("DiagonalPacker[maxSize=%s]", maxSize);
  }
}
