package game1.spritesheets.packers;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class Packer {

  /**
   * Maximum size of a spritesheet. Required by some graphics cards. Can be set with
   * --max-size=&lt;size&gt;.
   */
  protected int maxSize = 4096;

  protected List<Map<String, Rectangle>> sheets;

  public Packer(int maxSize) {
    this.maxSize = maxSize;
    this.sheets = new ArrayList<>();
  }

  public abstract void pack(Map<String, Rectangle> map);

  public List<Map<String, Rectangle>> getOutput() {
    return sheets;
  }
}
