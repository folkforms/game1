package game1.spritesheets.packers;

import java.awt.Rectangle;
import java.util.LinkedHashMap;
import java.util.Map;

public class BasicPacker extends Packer {

  public BasicPacker(int maxSize) {
    super(maxSize);
  }

  @Override
  public void pack(Map<String, Rectangle> map) {
    sheets.add(new LinkedHashMap<>());
    Map<String, Rectangle> targetSheet = sheets.get(sheets.size() - 1);

    int x = 0;
    int y = 0;
    int currentLineHeight = 0;

    for (String key : map.keySet()) {
      Rectangle currentItem = map.get(key);

      if (!willFitX(currentItem, x)) {
        // Move to new line
        x = 0;
        y += currentLineHeight;
        currentLineHeight = 0;
      }

      if (!willFitY(currentItem, y)) {
        // Move to new sheet
        Map<String, Rectangle> newSheet = new LinkedHashMap<>();
        sheets.add(newSheet);
        targetSheet = newSheet;
        x = 0;
        y = 0;
        currentLineHeight = 0;
      }

      // Add item to sheet
      currentItem.translate(x, y);
      targetSheet.put(key, currentItem);
      x += currentItem.width;
      if (currentItem.height > currentLineHeight) {
        currentLineHeight = currentItem.height;
      }
    }
  }

  private boolean willFitX(Rectangle currentItem, int x) {
    return currentItem.width <= maxSize - x;
  }

  private boolean willFitY(Rectangle currentItem, int y) {
    return currentItem.height <= maxSize - y;
  }

  @Override
  public String toString() {
    return String.format("BasicPacker[maxSize=%s]", maxSize);
  }
}
