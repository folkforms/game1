package game1.spritesheets;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import game1.spritesheets.packers.BasicPacker;
import game1.spritesheets.packers.DiagonalPacker;
import game1.spritesheets.packers.LargeToSmallPacker;
import game1.spritesheets.packers.Packer;

/**
 * Stores the options used when creating a spritesheet. Available options and their defaults can be
 * seen in the fields of this class.
 */
class GssOptions {
  private String inputFile; // Automatic
  private String[] inputGlobs; // Required
  private String outputFilename;
  private int minSize = 64;
  private int maxSize = 4096;
  private String rootFolder = System.getProperty("user.dir");
  private String outputFolder = "src/main/resources/packed";
  private Packer packer = new DiagonalPacker(maxSize);
  private String[] ignoreGlobs = {};
  private boolean allowWxH = false;
  private boolean padToPowerOfTwo = false;
  private boolean debug = false;

  /**
   * Creates a new {@link GssOptions} class.
   *
   * @param options
   *          options as a <code>Map&lt;String, Object&gt;
   */
  GssOptions(Map<String, Object> options) {
    setInputFile(options.get("spritesheet.inputFile"));
    setMinSize(options.get("spritesheet.minSize"));
    setMaxSize(options.get("spritesheet.maxSize"));
    setOutputFilename(options.get("spritesheet.outputFilename"));
    setInputGlobs(options.get("spritesheet.inputGlobs"));
    setRootFolder(options.get("spritesheet.rootFolder"));
    setOutputFolder(options.get("spritesheet.outputFolder"));
    setPacker(options.get("spritesheet.packer"));
    setIgnoreGlobs(options.get("spritesheet.ignoreGlobs"));
    setAllowWxH(options.get("spritesheet.allowWxH"));
    setPadToPowerOfTwo(options.get("spritesheet.padToPowerOfTwo"));
    setDebug(options.get("spritesheet.debug"));
  }

  String getInputFile() {
    return inputFile;
  }

  void setInputFile(Object inputFile) {
    if (inputFile == null) {
      throw new RuntimeException("Error: inputFile is a required field but was null!");
    }
    this.inputFile = inputFile.toString();
  }

  int getMinSize() {
    return minSize;
  }

  void setMinSize(Object minSize) {
    if (minSize != null) {
      this.minSize = Integer.parseInt(minSize.toString());
    }
  }

  int getMaxSize() {
    return maxSize;
  }

  void setMaxSize(Object maxSize) {
    if (maxSize != null) {
      this.maxSize = Integer.parseInt(maxSize.toString());
    }
  }

  String[] getInputGlobs() {
    return inputGlobs;
  }

  void setInputGlobs(Object inputGlobsObj) {
    if (inputGlobsObj == null) {
      throw new RuntimeException("Error: inputGlobs is a required field but was null!");
    }
    String s = (String) inputGlobsObj;
    String[] tokens = s.split(",");
    inputGlobs = tokens;
    for (int i = 0; i < inputGlobs.length; i++) {
      while (inputGlobs[i].startsWith("./")) {
        inputGlobs[i] = inputGlobs[i].substring(2);
      }
    }
  }

  String getOutputFilename() {
    return outputFilename;
  }

  void setOutputFilename(Object outputFilename) {
    this.outputFilename = (String) outputFilename;
  }

  String getRootFolder() {
    return this.rootFolder;
  }

  void setRootFolder(Object rootFolderObj) {
    if (rootFolderObj != null) {
      rootFolder = (String) rootFolderObj;
      while (rootFolder.startsWith("./")) {
        rootFolder = rootFolder.substring(2);
      }
      if (this.rootFolder.endsWith("/")) {
        rootFolder = rootFolder.substring(0, rootFolder.length() - 1);
      }
    }
  }

  String getOutputFolder() {
    return this.outputFolder;
  }

  void setOutputFolder(Object outputFolderObj) {
    if (outputFolderObj != null) {
      outputFolder = (String) outputFolderObj;
      while (outputFolder.startsWith("./")) {
        outputFolder = outputFolder.substring(2);
      }
      if (outputFolder.endsWith("/")) {
        outputFolder = outputFolder.substring(0, outputFolder.length() - 1);
      }
    }
  }

  Packer getPacker() {
    return this.packer;
  }

  void setPacker(Object packerName) {
    if (packerName != null) {
      String packerNameStr = (String) packerName;

      Map<String, Packer> packers = new LinkedHashMap<>();
      packers.put("basic", new BasicPacker(maxSize));
      packers.put("largetosmall", new LargeToSmallPacker(maxSize));
      packers.put("diagonal", new DiagonalPacker(maxSize));

      packer = packers.get(packerNameStr.toLowerCase());
      if (packer == null) {
        throw new RuntimeException(String.format(
            "Error: Unknown packer '%s'. Valid values are: %s.", packerName, packers.keySet()));
      }
    }
  }

  String[] getIgnoreGlobs() {
    return ignoreGlobs;
  }

  void setIgnoreGlobs(Object ignoreGlobsObj) {
    if (ignoreGlobsObj != null) {
      String s = (String) ignoreGlobsObj;
      String[] tokens = s.split(",");
      ignoreGlobs = tokens;
      for (int i = 0; i < ignoreGlobs.length; i++) {
        while (ignoreGlobs[i].startsWith("./")) {
          ignoreGlobs[i] = ignoreGlobs[i].substring(2);
        }
      }
    }
  }

  boolean getAllowWxH() {
    return this.allowWxH;
  }

  void setAllowWxH(Object allowWxHObj) {
    if (allowWxHObj != null) {
      this.allowWxH = Boolean.parseBoolean(allowWxHObj.toString());
    }
  }

  boolean getPadToPowerOfTwo() {
    return this.padToPowerOfTwo;
  }

  void setPadToPowerOfTwo(Object padToPowerOfTwoObj) {
    if (padToPowerOfTwoObj != null) {
      this.padToPowerOfTwo = Boolean.parseBoolean(padToPowerOfTwoObj.toString());
    }
  }

  boolean getDebug() {
    return this.debug;
  }

  void setDebug(Object debugObj) {
    if (debugObj != null) {
      debug = Boolean.parseBoolean(debugObj.toString());
    }
  }

  String getFullOutputFolder() {
    return rootFolder.length() > 0 ? String.format("%s/%s", rootFolder, outputFolder)
      : outputFolder;
  }

  @Override
  public String toString() {
    return String.format(
        "GssOptions[inputFile=%s, outputPrefix=%s, inputGlobs=%s, minSize=%s, maxSize=%s, rootFolder=%s, "
            + "outputFolder=%s, packer=%s, ignoreGlobs=%s, allowWxH=%s, padToPowerOfTwo=%s]",
        inputFile, outputFilename, Arrays.toString(inputGlobs), minSize, maxSize, rootFolder,
        outputFolder, packer, Arrays.toString(ignoreGlobs), allowWxH, padToPowerOfTwo);
  }
}
