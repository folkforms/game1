package game1.actors;

public class ParentChildRegistryFactory {

  private static ParentChildRegistry parentChildRegistry;

  public static ParentChildRegistry getInstance() {
    if (parentChildRegistry == null) {
      parentChildRegistry = new ParentChildRegistry();
    }
    return parentChildRegistry;
  }
}
