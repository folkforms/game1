package game1.actors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import game1.debug.dump_data.DebugDataProvider;
import game1.debug.dump_data.DebugDataProviderRegistry;

public class ParentChildRegistry implements DebugDataProvider {

  private Map<Actor, List<Actor>> parentChildrenMap = new HashMap<>();
  private Map<Actor, Actor> childToParentMap = new HashMap<>();

  public ParentChildRegistry() {
    DebugDataProviderRegistry.register(this, DebugDataProviderRegistry.Context.FILE);
  }

  public void addParentChildRelationship(Actor parent, Actor child) {
    List<Actor> existing = parentChildrenMap.getOrDefault(parent, new ArrayList<>());
    existing.add(child);
    parentChildrenMap.put(parent, existing);
    childToParentMap.put(child, parent);
  }

  public List<Actor> getChildren(Actor parent) {
    return parentChildrenMap.getOrDefault(parent, List.of());
  }

  public List<Actor> getAllChildren(Actor parent) {
    List<Actor> output = new ArrayList<>();
    List<Actor> children = parentChildrenMap.getOrDefault(parent, List.of());
    output.addAll(children);
    children.forEach(child -> output.addAll(getAllChildren(child)));
    return output;
  }

  public Actor getTopLevelAncestor(Actor actor) {
    Actor current = actor;
    Actor parentMaybe;
    do {
      parentMaybe = childToParentMap.get(current);
      if (parentMaybe != null) {
        current = parentMaybe;
      }
    } while (parentMaybe != null);
    return current;
  }

  public void clear() {
    parentChildrenMap.clear();
  }

  @Override
  public String debug_getDumpFilename() {
    return "dump_parent_child_registry.txt";
  }

  @Override
  public List<String> debug_listData() {
    List<String> output = new ArrayList<>();
    output.add("# ParentChildRegistry");
    output.add("");
    output.add("## parentChildMap");
    output.add("");
    parentChildrenMap.entrySet().forEach(entry -> {
      output.add(entry.getKey().toString());
      List<Actor> list = entry.getValue();
      list.forEach(item -> output.add(String.format("    => %s", item)));
    });
    output.add("");
    output.add("## childToParentMap");
    childToParentMap.entrySet().forEach(entry -> {
      output.add(String.format("    %s => %s", entry.getKey(), entry.getValue()));
    });

    return output;
  }
}
