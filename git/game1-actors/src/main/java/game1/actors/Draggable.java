package game1.actors;

import game1.primitives.Shape;

/**
 * Draggable objects are able to be dragged with the mouse.
 */
public interface Draggable {

  /**
   * Gets the draggable area.
   *
   * @return the draggable area
   */
  public Shape getShape();

  /**
   * Gets the draggable's Z position.
   *
   * @return the draggable's Z position
   */
  public int getZ();

  /**
   * Called when dragging is in progress.
   *
   * @param windowX
   *          mouse X position
   * @param windowY
   *          mouse Y position
   */
  public void onDrag(float windowX, float windowY);

  /**
   * Called when dragging stops.
   *
   * @param windowX
   *          mouse X position
   * @param windowY
   *          mouse Y position
   */
  public void dragFinished(float windowX, float windowY);
}
