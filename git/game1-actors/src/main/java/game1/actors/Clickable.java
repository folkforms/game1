package game1.actors;

import game1.primitives.Shape;

/**
 * When a Clickable object is clicked using the mouse its {@link #onClick()} method is executed.
 */
public interface Clickable extends Actor, Active {

  /**
   * Gets the clickable area.
   *
   * @return the clickable area
   */
  public Shape<?> getShape();

  /**
   * Called when the user clicks on the clickable area.
   */
  public void onClick(int button, float windowX, float windowY);

  /**
   * Gets the clickable's Z position.
   *
   * @return the clickable's Z position
   */
  public int getZ();
}
