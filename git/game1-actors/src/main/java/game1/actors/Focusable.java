package game1.actors;

/**
 * Denotes an Actor that can receive focus, e.g. an item that can be selected from a list. Only 0-1
 * items can be in focus at a time.
 *
 * <p>
 * Note that simply implementing {@link Focusable} does not mean that an item will receive focus
 * when clicking on it. For mouse clicks you need to implement {@link Clickable} and add code such
 * as:
 * </p>
 *
 * <pre>
 * &#64;Override
 * public void onClick(int button) {
 *   FocusableActors.setSelected(this);
 * }
 * </pre>
 *
 * <p>
 * Similar code can be used inside a Command implementation to focus an item using the keyboard.
 * </p>
 */
public interface Focusable {

  /**
   * Called when the Actor gains focus.
   */
  void onFocus();

  /**
   * Called when the Actor loses focus.
   */
  void onBlur();
}
