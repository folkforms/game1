package game1.actors.actorproperties;

import java.util.HashMap;
import java.util.Map;

public class ActorProperties {
  private Map<ActorPropertiesEnum, Object> properties = new HashMap<>();

  @SuppressWarnings("unchecked")
  public <T> T get(ActorPropertiesEnum key) {
    Object object = properties.getOrDefault(key, key.getDefaultValue());
    return (T) object;
  }

  public void put(ActorPropertiesEnum key, Object value) {
    Class<?> type = key.getType();
    if (!type.equals(value.getClass())) {
      throw new RuntimeException(String.format(
          "Tried to set ActorPropertiesEnum.%s to '%s' (%s) but that property only allows objects of %s",
          key, value, value.getClass(), type));
    }
    properties.put(key, value);
  }

  @Override
  public String toString() {
    return String.format("%s[%s]", getClass().getSimpleName(), properties);
  }
}
