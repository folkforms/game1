package game1.actors.actorproperties;

import java.util.HashMap;
import java.util.Map;

import game1.actors.Actor;

public class ActorPropertiesRegistry {

  private static Map<Actor, ActorProperties> map = new HashMap<>();

  public static ActorProperties get(Actor actor) {
    return map.get(actor);
  }

  public static void put(Actor actor, ActorPropertiesEnum key, Object value) {
    ActorProperties properties = map.getOrDefault(actor, new ActorProperties());
    properties.put(key, value);
    map.put(actor, properties);
  }

  public static void clear() {
    map.clear();
  }
}
