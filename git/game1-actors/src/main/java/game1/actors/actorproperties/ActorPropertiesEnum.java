package game1.actors.actorproperties;

/**
 * 2024-12-29 I may go back to using ViewportAffected and ResolutionAffected interfaces in the long
 * run, but let's see how this pans out.
 */
public enum ActorPropertiesEnum {
  IS_RESOLUTION_AFFECTED(Boolean.class, true), //
  IS_VIEWPORT_AFFECTED(Boolean.class, true);

  private Class<?> type;
  private Object defaultValue;

  private ActorPropertiesEnum(Class<?> type, Object defaultValue) {
    this.type = type;
    this.defaultValue = defaultValue;
  }

  public Class<?> getType() {
    return type;
  }

  @SuppressWarnings("unchecked")
  public <T> T getDefaultValue() {
    return (T) defaultValue;
  }
}
