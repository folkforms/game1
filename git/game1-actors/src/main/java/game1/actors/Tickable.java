package game1.actors;

/**
 * Tickable objects do something every time a given number of ticks elapses.
 */
public interface Tickable {

  /**
   * Action a 'tick' for this object. Note that this method will only be called when the game is
   * unpaused.
   *
   * @param tick
   *          current tick number
   */
  public void onTick();
}
