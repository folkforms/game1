package game1.actors;

import org.joml.Vector3f;

/**
 * Moveable objects can be moved by some external system, for example by user input, gravity or a
 * cutscene player.
 */
public interface Moveable {

  Vector3f getPosition();

  void setPosition(float x, float y, float z);

  default public void setPosition(float x, float y) {
    setPosition(x, y, getPosition().z);
  }

  default public void setPosition(float z) {
    setPosition(getPosition().x, getPosition().y, z);
  }

  default void setPosition(Vector3f pos) {
    setPosition(pos.x, pos.y, pos.z);
  }
}
