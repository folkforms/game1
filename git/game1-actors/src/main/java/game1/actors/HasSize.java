package game1.actors;

public interface HasSize {

  float getWidth();

  float getHeight();
}
