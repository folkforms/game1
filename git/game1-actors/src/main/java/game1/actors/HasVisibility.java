package game1.actors;

public interface HasVisibility {

  boolean getVisible();

  void setVisible(boolean newValue);
}
