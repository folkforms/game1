package game1.actors;

import org.joml.Vector3f;

/**
 * Velocity objects can be moved by some external system, typically by user input or forces such as
 * gravity.
 */
public interface Velocity {

  Vector3f getSpeed();

  void setSpeed(float speedX, float speedY, float speedZ);

  default public void setSpeed(float speedX, float speedY) {
    setSpeed(speedX, speedY, getSpeed().z);
  }

  default void setSpeed(Vector3f speed) {
    setSpeed(speed.x, speed.y, speed.z);
  }
}
