package game1.actors;

import java.util.List;

/**
 * Actor is the superclass of everything that can be added to a UserState.
 */
public interface Actor {

  default void addChild(Actor child) {
    ParentChildRegistryFactory.getInstance().addParentChildRelationship(this, child);
  }

  /**
   * Convenience method to allow us to call `setVisible` on an Actor and have it applied to all of
   * its Drawable children.
   *
   * @param isVisible
   *          whether the Actor's Drawable children should be drawn or not
   */
  default void setVisible(boolean isVisible) {
    // FIXME ACTORS: Removing the validations for now as game1-actors has no concept of state. How
    // can we handle this? An event here and something in GameState handles it?
    // ActorValidations.validateActorAddedToState(this,
    // "Actor.setVisible failed as actor is not in the state");
    List<Actor> children = ParentChildRegistryFactory.getInstance().getChildren(this);
    for (int i = 0; i < children.size(); i++) {
      children.get(i).setVisible(isVisible);
    }
  }

  default Actor getClone() {
    throw new RuntimeException(String.format("Method getClone() has not been implemented for %s",
        this.getClass().getName()));
  }
}
