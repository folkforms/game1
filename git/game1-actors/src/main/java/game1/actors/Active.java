package game1.actors;

public interface Active {

  boolean isActive();

  void setActive(boolean b);
}
