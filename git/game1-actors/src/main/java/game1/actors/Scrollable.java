package game1.actors;

import game1.primitives.Shape;

/**
 * Scrollable objects respond to the mouse scroll wheel.
 */
public interface Scrollable extends Actor {

  /**
   * Gets the affected area of this actor.
   *
   * @return the affected area of this actor
   */
  public Shape getShape();

  /**
   * Called when the user mouse wheels up on this actor.
   */
  public void onMouseWheelUp();

  /**
   * Called when the user mouse wheels down on this actor.
   */
  public void onMouseWheelDown();

  /**
   * Gets the {@code z} value of this Scrollable.
   *
   * @return the {@code z} value of this Scrollable
   */
  public float getZ();
}
