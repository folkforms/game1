package game1.actors;

public interface HasScale {

  float getScale();
}
