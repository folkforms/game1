#!/usr/bin/ruby

@debug = false
@dry_run = false
if ARGV.include?("-d") || ARGV.include?("--debug") then @debug = true end
if ARGV.include?("-n") || ARGV.include?("--dry-run") then @dry_run = true end
puts "DEBUG: debug = #{@debug}" if @debug
puts "DEBUG: dry_run = #{@dry_run}" if @debug

mode = "patch"
if ARGV.include?("--patch") then
  mode = "patch"
elsif ARGV.include?("--minor") then
  mode = "minor"
elsif ARGV.include?("--major") then
  mode = "major"
elsif ARGV.include?("--hotfix") then
  mode = "hotfix"
end
puts "DEBUG: mode = #{mode}" if @debug

revision_line = IO.readlines("pom.xml").keep_if { |x| x.include? "<revision>" }[0]
version = revision_line.match(/<revision>(.*?)<\/revision>/).captures[0]
puts "DEBUG: current version = '#{version}'" if @debug

release_version = version.gsub(/-SNAPSHOT/, "")
puts "DEBUG: version_no_snapshot = '#{release_version}'" if @debug

def increment_version(version, mode)
  tokens = version.split(/\./)
  if mode == "major" then
    tokens[0] = tokens[0].to_i + 1
  elsif mode == "minor" then
    tokens[1] = tokens[1].to_i + 1
  elsif mode == "patch" then
    tokens[2] = tokens[2].to_i + 1
  elsif mode == "hotfix" then
    tokens[3] = tokens[3].to_i + 1
  end
  return mode != "hotfix" ? "#{tokens[0]}.#{tokens[1]}.#{tokens[2]}" : "#{tokens[0]}.#{tokens[1]}.#{tokens[2]}.#{tokens[3]}"
end

next_version = increment_version(release_version, mode) << "-SNAPSHOT"
puts "DEBUG: next_version = #{next_version}" if @debug

puts "current: #{version}"
puts "release: #{release_version}"
puts "next: #{next_version}"

def exec(cmd)
  if @dry_run then
    puts cmd
  else
    ok = system(cmd)
    if !ok then exit 1 end
  end
end

working_tree_clean = `git status --porcelain`.length ==0
if !working_tree_clean then
  puts "Error: Working tree not clean"
  exit 1
end

exec("mvn versions:set-property -Dproperty=revision -DnewVersion=#{release_version} -DgenerateBackupPoms=false")
exec("mvn clean install")
exec("git add -A .")
exec("git commit -m \"#{release_version}\"")
exec("git tag -a \"#{release_version}\" -m \"#{release_version}\"")

exec("mvn versions:set-property -Dproperty=revision -DnewVersion=#{next_version} -DgenerateBackupPoms=false")
exec("mvn clean install")
exec("git add -A .")
exec("git commit -m \"Update pom to snapshot version\"")
