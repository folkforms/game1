@echo off
cls

cd core
call mvn clean install -Dskip=true
if %ERRORLEVEL% neq 0 exit /b %ERRORLEVEL%
cd ..
