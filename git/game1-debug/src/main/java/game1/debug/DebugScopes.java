package game1.debug;

import folkforms.log.Log;

public class DebugScopes {
  public final static String INFRASTRUCTURE = "Infrastructure";
  public final static String EVENTS = "Events";
  public final static String ANNOTATIONS = "Annotations";
  public final static String STATE = "State";
  public final static String DATASETS = "Datasets";
  public final static String MAIN_THREAD_TICKETS = "MainThreadTickets";
  public final static String FONTS = "Fonts";
  public final static String PROPERTIES = "Properties";
  public final static String PATHS = "Paths";
  public final static String SPRITESHEETS = "Spritesheets";
  public final static String TEXTURE_LOADER = "TextureLoader";
  public final static String SOUND = "Sound";
  public final static String CUTSCENES = "Cutscenes";
  public final static String KEYBINDINGS = "Keybindings";
  public final static String FILE_SYSTEM = "FileSystem";
  public final static String MENUS = "Menus";
  public final static String GAME_CACHE = "GameCache";
  public final static String MOUSE = "Mouse";
  public final static String VIEWPORT = "Viewport";
  public final static String VARIABLES = "Variables";
  public final static String GAME = "Game";

  public static void init() {
    add(INFRASTRUCTURE);
    add(EVENTS);
    add(ANNOTATIONS);
    add(STATE);
    add(DATASETS);
    add(MAIN_THREAD_TICKETS);
    add(FONTS);
    add(PROPERTIES);
    add(PATHS);
    add(SPRITESHEETS);
    add(TEXTURE_LOADER);
    add(SOUND);
    add(CUTSCENES);
    add(KEYBINDINGS);
    add(FILE_SYSTEM);
    add(MENUS);
    add(GAME_CACHE);
    add(MOUSE);
    add(VIEWPORT);
    add(VARIABLES);
    add(GAME);
  }

  public static void add(String scopeName, boolean defaultValue) {
    internal_add(scopeName, defaultValue);
  }

  public static void add(String scopeName) {
    internal_add(scopeName, false);
  }

  private static void internal_add(String scopeName, boolean defaultValue) {
    Log.addScope(scopeName, defaultValue);
  }

  public static void addAndActivate(String scopeName) {
    internal_add(scopeName, true);
  }

  public static void activate(String debugStr) {
    String[] tokens = debugStr.split(",\\s*");
    for (String scopeName : tokens) {
      Log.setScopeEnabled(scopeName, true);
    }
  }
}
