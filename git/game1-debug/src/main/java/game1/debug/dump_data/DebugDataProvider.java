package game1.debug.dump_data;

import java.util.List;

public interface DebugDataProvider {

  String debug_getDumpFilename();

  List<String> debug_listData();
}
