package game1.debug.dump_data;

import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import folkforms.log.Log;
import folkforms.textio.TextIO;

public class DebugDataProviderRegistry {

  private static final String ROOT_OUTPUT_FOLDER = "_dumps";
  private static Map<Context, Set<DebugDataProvider>> contextToProviderMap = new HashMap<>();

  public static void register(DebugDataProvider dumpDataToFile, Context context) {
    Set<DebugDataProvider> set = contextToProviderMap.getOrDefault(context, new HashSet<>());
    set.add(dumpDataToFile);
    contextToProviderMap.put(context, set);
  }

  public static void dumpAllDataToFile() {
    Path outputFolder = Path.of(ROOT_OUTPUT_FOLDER, getLocalDateTime());
    outputFolder.toFile().mkdir();
    contextToProviderMap.get(Context.FILE).stream().forEach(d -> {
      debug_dumpDataToFile(outputFolder, d.debug_getDumpFilename(), d.debug_listData());
    });
  }

  private static String getLocalDateTime() {
    LocalDateTime d = LocalDateTime.now();
    return String.format("%04d-%02d-%02dT%02d-%02d-%02d.%03d", d.getYear(), d.getMonthValue(),
        d.getDayOfMonth(), d.getHour(), d.getMinute(), d.getSecond(), d.getNano() / 1_000_000);
  }

  public static void dumpAllDataToConsole() {
    Log.info("");
    contextToProviderMap.get(Context.CONSOLE).stream().forEach(d -> {
      Log.info("%s", d.debug_getDumpFilename());
      Log.info("");
      d.debug_listData().forEach(line -> Log.info(line));
      Log.info("");
      Log.info("----");
      Log.info("");
    });
  }

  public static void debug_dumpDataToFile(Path outputFolder, String filename, List<String> data) {
    filename = String.format("%s/%s", outputFolder.toString().replace('\\', '/'), filename);
    try {
      new TextIO().write(data, filename);
    } catch (IOException ex) {
      Log.error(ex);
    }
  }

  public enum Context {
    FILE, CONSOLE
  }
}
