package game1.filesystem;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Enumeration;
import java.util.List;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import folkforms.log.Log;
import folkforms.textio.TextIO;
import game1.debug.DebugScopes;

/**
 * File system access layer. Keeps file data in memory and discards it when it is no longer required
 * thus reducing the memory footprint.
 */
public class FileSystemImpl implements FileSystem {

  private Paths paths;
  private boolean isProduction;
  private boolean warnOnExternalPaths = true;
  private OpenFiles openFiles;

  public FileSystemImpl(Paths paths, boolean warnOnExternalPaths) throws IOException {
    this.paths = paths;
    this.isProduction = paths.isProduction();
    this.warnOnExternalPaths = warnOnExternalPaths;
    this.openFiles = new OpenFiles(paths.isProduction());
    Log.scoped(DebugScopes.FILE_SYSTEM, "user.dir: %s", System.getProperty("user.dir"));
  }

  // FIXME FILE_SYSTEM: Is this actually used?
  @Override
  public List<String> loadList(String resourcePath) throws IOException {
    return FileTransformer.toList(load(resourcePath), resourcePath);
  }

  @Override
  public byte[] load(String resourcePath) throws IOException {
    return load(resourcePath, true);
  }

  @Override
  public byte[] load(String resourcePath, boolean throwIfMissing) throws IOException {
    Log.scoped(DebugScopes.FILE_SYSTEM, "Loading '%s'", resourcePath);
    resourcePath = PathUtils.cleanPath(resourcePath);
    if (isProduction) {
      resourcePath = PathUtils.removeSrcMainResources(resourcePath);
    }
    Log.scoped(DebugScopes.FILE_SYSTEM, "  -> Cleaned up path to '%s'", resourcePath);

    String resourcePathModified = PathUtils.removeSrcMainResources(resourcePath);
    Optional<String> jarPathMaybe = findJarContainingFile(resourcePathModified);
    if (jarPathMaybe.isPresent()) {
      String jarPath = jarPathMaybe.get();
      Log.scoped(DebugScopes.FILE_SYSTEM, "  -> Found file in jar '%s'", jarPath);
      return openFiles.getOrOpen(jarPath, resourcePathModified);
    }

    File file = new File(isProduction ? "app/" + resourcePath : resourcePath);
    if (file.exists()) {
      Log.scoped(DebugScopes.FILE_SYSTEM, "  -> Found file on disk");
      if (!PathUtils.startsWithSrcMainResources(resourcePath) && !isProduction) {
        String message = String.format(
            "File '%s' should be placed in the src/main/resources folder otherwise it will not be included in the release",
            resourcePath);
        if (warnOnExternalPaths) {
          Log.warn(message);
        } else {
          throw new RuntimeException(message);
        }
      }
      return Files.readAllBytes(file.toPath());
    }

    if (throwIfMissing) {
      throw new IOException(String.format(
          "Failed to load resource '%s'. Could not find the file in any registered jar or on disk. (registeredJars = %s, user.dir = %s)",
          resourcePath, paths.listResourceJars(), System.getProperty("user.dir")));
    } else {
      return null;
    }
  }

  public boolean find(String resourcePath, boolean throwIfMissing) throws IOException {
    Log.scoped(DebugScopes.FILE_SYSTEM, "Searching for '%s'", resourcePath);
    resourcePath = PathUtils.cleanPath(resourcePath);
    if (isProduction) {
      resourcePath = PathUtils.removeSrcMainResources(resourcePath);
    }

    String resourcePathModified = PathUtils.removeSrcMainResources(resourcePath);
    Optional<String> jarPathMaybe = findJarContainingFile(resourcePathModified);
    if (jarPathMaybe.isPresent()) {
      return true;
    }

    File file = new File(isProduction ? "app/" + resourcePath : resourcePath);
    if (file.exists()) {
      return true;
    }

    if (throwIfMissing) {
      throw new IOException(String.format(
          "Failed to find resource '%s'. Could not find the file in any registered jar or on disk. (registeredJars = %s, user.dir = %s)",
          resourcePath, paths.listResourceJars(), System.getProperty("user.dir")));
    } else {
      return false;
    }
  }

  private Optional<String> findJarContainingFile(String resourcePath) throws IOException {
    Optional<String> jarPathMaybe = Optional.empty();
    List<String> resourceJars = paths.listResourceJars();
    for (int i = 0; i < resourceJars.size(); i++) {
      String jarPath = resourceJars.get(i);
      ZipFile zipFile = new ZipFile(jarPath);
      Enumeration<? extends ZipEntry> entries = zipFile.entries();
      while (entries.hasMoreElements()) {
        ZipEntry entry = entries.nextElement();
        if (entry.getName().contains(resourcePath)) {
          zipFile.close();
          return Optional.of(jarPath);
        }
      }
      zipFile.close();
    }
    return jarPathMaybe;
  }

  // FIXME FILE_SYSTEM: Not used, meaning nothing unloads files!
  // public void unload(String resourcePath) {
  // Log.scoped(DebugScopes.FILE_SYSTEM, "Unloading '%s'", resourcePath);
  // openFiles.unload(resourcePath);
  // }

  // FIXME FILE_SYSTEM: What is the difference between this and `exists`?
  public void validateFileExists(String resourcePath) {
    try {
      Log.scoped(DebugScopes.FILE_SYSTEM, "Validating file '%s' exists", resourcePath);
      resourcePath = PathUtils.cleanPath(resourcePath);
      if (isProduction) {
        resourcePath = PathUtils.removeSrcMainResources(resourcePath);
      }
      Log.scoped(DebugScopes.FILE_SYSTEM, "  -> Cleaned up path to '%s'", resourcePath);

      String resourcePathModified = PathUtils.removeSrcMainResources(resourcePath);
      Optional<String> jarPathMaybe = findJarContainingFile(resourcePathModified);
      if (jarPathMaybe.isPresent()) {
        String jarPath = jarPathMaybe.get();
        Log.scoped(DebugScopes.FILE_SYSTEM, "  -> Found loose file in jar '%s'", jarPath);
        return;
      }

      File file = new File(isProduction ? "app/" + resourcePath : resourcePath);
      if (file.exists()) {
        Log.scoped(DebugScopes.FILE_SYSTEM, "  -> Found file on disk");
        return;
      }
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }

    throw new RuntimeException(String.format(
        "Could not find file '%s' in any jar, or on disk. (resourceJars = %s, user.dir = %s)",
        resourcePath, paths.listResourceJars(), System.getProperty("user.dir")));
  }

  @Override
  public void write(List<String> contents, String path) throws IOException {
    new TextIO().write(contents, path);
  }

  public void debug_printData() {
    openFiles.debug_printData();
  }

  @Override
  public boolean exists(String resourcePath) throws IOException {
    return find(resourcePath, false);
  }
}
