package game1.filesystem;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import folkforms.log.Log;

/**
 * Transforms a resource from a <code>byte[]</code> into a <code>List&lt;String&gt;</code>.
 */
public class FileTransformer {

  /**
   * Converts a <code>byte[]</code> into a <code>List&lt;String&gt;</code>.
   *
   * @param bytes
   *          resource data
   * @param resourcePath
   *          used for debugging purposes
   * @return the resource data converted into a list
   */
  public static List<String> toList(byte[] bytes, String resourcePath) throws IOException {
    String regex = "\n";
    for (int i = 0; i < bytes.length; i++) {
      if (bytes[i] == '\r') {
        regex = "\r\n";
        Log.warn(
            "Resource '%s' has \\r\\n line endings. It should be converted to \\n line endings.",
            resourcePath);
        break;
      }
    }
    String s = new String(bytes);
    String[] lines = s.split(regex);
    return Arrays.stream(lines).collect(Collectors.toList());
  }
}
