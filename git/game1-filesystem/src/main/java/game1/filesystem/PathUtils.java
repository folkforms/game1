package game1.filesystem;

public class PathUtils {

  /**
   * Removes any "./" entries from the path in order to standardise paths when loading files in
   * memory.
   *
   * @param path
   *          path to clean
   * @return the path minus any "./" entries
   */
  public static String cleanPath(String path) {
    if (path.startsWith("./")) {
      path = path.substring(2);
    }
    while (path.contains("/./")) {
      path.replace("/./", "/");
    }
    return path;
  }

  public static String removeSrcMainResources(String path) {
    return path.replace("src/main/resources/", "");
  }

  public static boolean startsWithSrcMainResources(String path) {
    return path.startsWith("src/main/resources/");
  }
}
