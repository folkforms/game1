package game1.filesystem;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.StreamSupport;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import folkforms.log.Log;
import game1.debug.DebugScopes;

/**
 * Keeps track of files in memory and loads them if they are not already loaded. This way we can
 * cache the data rather than loading it a second time.
 */
public class OpenFiles {

  private boolean isProduction = true;
  private static Map<String, ZipFile> openJars = new HashMap<>();
  private static Map<String, String> reasons = new HashMap<>();

  OpenFiles(boolean isProduction) {
    this.isProduction = isProduction;
  }

  public byte[] getOrOpen(String jarPath, String resourcePath) throws IOException {
    Optional<ZipFile> zipFileMaybe;
    if (jarPath != null) {
      zipFileMaybe = Optional.of(getOrOpenJar(jarPath, resourcePath));
      byte[] bytes = loadFromJar(zipFileMaybe.get(), resourcePath);
      closeJar(jarPath);
      return bytes;
    } else {
      return loadFromDisk(resourcePath);
    }
  }

  private ZipFile getOrOpenJar(String jarPath, String reason) throws IOException {
    if (openJars.containsKey(jarPath)) {
      return openJars.get(jarPath);
    }

    ZipFile zipFile = new ZipFile(jarPath);
    openJars.put(jarPath, zipFile);
    reasons.put(jarPath, reason);
    return zipFile;
  }

  private void closeJar(String jarPath) throws IOException {
    Log.scoped(DebugScopes.FILE_SYSTEM, "  -> Closing jar '%s'", jarPath);
    ZipFile zipFile = openJars.get(jarPath);
    zipFile.close();
    openJars.remove(jarPath);
    reasons.remove(jarPath);
  }

  private byte[] loadFromJar(ZipFile zipFile, String path) throws IOException {
    Log.scoped(DebugScopes.FILE_SYSTEM, "  -> Loading bytes for '%s' from jar '%s'", path,
        zipFile.getName());
    Enumeration<? extends ZipEntry> entries = zipFile.entries();
    while (entries.hasMoreElements()) {
      ZipEntry entry = entries.nextElement();
      if (entry.getName().equals(path)) {
        InputStream stream = zipFile.getInputStream(entry);
        try (stream) {
          return stream.readAllBytes();
        }
      }
    }

    List<String> entriesList = StreamSupport.stream(
        Spliterators.spliteratorUnknownSize(zipFile.entries().asIterator(), Spliterator.ORDERED),
        false).map(ZipEntry::getName).toList();
    throw new IOException(
        String.format("zipFile '%s' did not contain an entry called '%s'. Entries: %s",
            zipFile.getName(), path, entriesList));
  }

  private byte[] loadFromDisk(String path) throws IOException {
    // FIXME FILE_SYSTEM: I suspect we will use the "app/" prefix when loading a game's blob data,
    // but I don't have an example right now
    if (isProduction) {
      Log.temp("#### Prefixing 'app/' to path '%s'", path);
      path = "app/" + path;
    }
    Log.scoped(DebugScopes.FILE_SYSTEM, "  -> Loading bytes for '%s' from disk", path);
    return Files.readAllBytes(new File(path).toPath());
  }

  public void unload(String resourcePath) {
    Log.scoped(DebugScopes.FILE_SYSTEM, "  -> Decrementing number of references to '%s'",
        resourcePath);
  }

  public void debug_printData() {
    Log.info("OpenFiles:");
    Log.info("  - Open jars: %s", openJars.keySet());
    Log.info("  - Reasons:");
    reasons.keySet().forEach(key -> Log.info("    - %s -> %s", key, reasons.get(key)));
  }
}
