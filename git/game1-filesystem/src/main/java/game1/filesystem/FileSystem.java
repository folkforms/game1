package game1.filesystem;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

// FIXME FILE_SYSTEM: This is doing too much. It's loading files but also unloading them.
public interface FileSystem {

  byte[] load(String resourcePath) throws IOException;

  byte[] load(String resourcePath, boolean throwIfMissing) throws IOException;

  List<String> loadList(String resourcePath) throws IOException;

  default String loadString(String resourcePath) throws IOException {
    return loadList(resourcePath).stream().collect(Collectors.joining("\n"));
  }

  void write(List<String> contents, String path) throws IOException;

  boolean exists(String resourcePath) throws IOException;
}
