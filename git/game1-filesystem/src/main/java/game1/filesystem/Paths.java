package game1.filesystem;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import folkforms.log.Log;
import folkforms.xpathutils.XPathUtils;
import game1.debug.DebugScopes;

/**
 * Manages paths based on the context we are running the game in.
 * <ul>
 * <li>If a <code>.production</code> file is found it is assumed we are running in production mode
 * and all paths will be prefixed with "app/" and Game1 files are assumed to be located in the
 * "app/game1" folder.</li>
 * <li>If a <code>.production</code> file is NOT found it is assumed we are running in development
 * mode and paths will be unchanged and the Game1 files are assumed to be located in the folder
 * specified by the {@code MAVEN_REPO} environment variable.</li>
 * </ul>
 */
public class Paths {

  private boolean isProduction;
  private String mavenRepo;
  private String gameShortName;
  private List<String> resourceJars = new ArrayList<>();

  public Paths() throws IOException {
    isProduction = new File("app/.production").exists();
    mavenRepo = isProduction ? "game1" : System.getenv("MAVEN_REPO");
    if (mavenRepo == null) {
      throw new RuntimeException(
          "MAVEN_REPO environment variable was not found. This variable must be set when we are "
              + "not in production. Use 'set MAVEN_REPO=C:\\Users\\<username>\\.m2\\repository'.");
    }

    String game1Version;
    String gameJarName;
    try {
      Document document = XPathUtils.getDocument(isProduction ? "app/pom.xml" : "pom.xml");
      game1Version = checkPom(document, "/pom:project/pom:properties/pom:game1.version/text()");
      gameShortName = checkPom(document, "/pom:project/pom:properties/pom:game.shortName/text()");
      String gameVersion = checkPom(document,
          "/pom:project/pom:properties/pom:game.version/text()");
      gameJarName = String.format("%s-%s.jar", gameShortName, gameVersion);
    } catch (ParserConfigurationException | SAXException ex) {
      throw new IOException(ex);
    }

    String game1CoreJarPath = isProduction
      ? String.format("app/game1/game1-core-%s.jar", game1Version) : String.format(
          "%s/game1/game1-core/%s/game1-core-%s.jar", mavenRepo, game1Version, game1Version);
    resourceJars.add(game1CoreJarPath);

    String game1ToolkitJarPath = isProduction
      ? String.format("app/game1/game1-toolkit-%s.jar", game1Version) : String.format(
          "%s/game1/game1-toolkit/%s/game1-toolkit-%s.jar", mavenRepo, game1Version, game1Version);
    resourceJars.add(game1ToolkitJarPath);

    String game1ToolkitCutscenesJarPath = isProduction
      ? String.format("app/game1/game1-toolkit-cutscenes-%s.jar", game1Version)
      : String.format("%s/game1/game1-toolkit-cutscenes/%s/game1-toolkit-cutscenes-%s.jar",
          mavenRepo, game1Version, game1Version);
    resourceJars.add(game1ToolkitCutscenesJarPath);

    if (isProduction) {
      Log.scoped(DebugScopes.FILE_SYSTEM, "Registering game jar: %s", gameJarName);
      resourceJars.add("app/" + gameJarName);
    }

    printDebugInfo();
  }

  private String checkPom(Document document, String xPathExpression) {
    try {
      return XPathUtils.getFirst(xPathExpression, document);
    } catch (Exception ex) {
      throw new RuntimeException(String.format("Could not find %s in pom.xml", xPathExpression),
          ex);
    }
  }

  public void printDebugInfo() {
    Log.scoped(DebugScopes.PATHS, "Paths:");
    Log.scoped(DebugScopes.PATHS, "    isProduction = %s", isProduction);
    Log.scoped(DebugScopes.PATHS, "    mavenRepo = %s", mavenRepo);
    Log.scoped(DebugScopes.PATHS, "    gameShortName = %s", gameShortName);
    Log.scoped(DebugScopes.PATHS, "    resourceJars = %s", resourceJars);
  }

  public boolean isProduction() {
    return isProduction;
  }

  public String getMavenRepo() {
    return mavenRepo;
  }

  public String getGameShortName() {
    return gameShortName;
  }

  public List<String> listResourceJars() {
    return resourceJars;
  }
}
