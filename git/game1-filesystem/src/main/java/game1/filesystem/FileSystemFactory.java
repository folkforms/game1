package game1.filesystem;

import java.io.IOException;

public class FileSystemFactory {

  private static FileSystem fileSystem = null;

  public static void init(Paths paths, boolean warnOnExternalPaths) throws IOException {
    fileSystem = new FileSystemImpl(paths, warnOnExternalPaths);
  }

  public static FileSystem getInstance() {
    return fileSystem;
  }
}
