package game1.filesystem2.datasets;

import java.util.ArrayList;
import java.util.List;

public class Dataset {

  public Dataset() {
  }

  private List<String> files = new ArrayList<>();

  public List<String> listFiles() {
    return files;
  }

  protected void add(String filename) {
    files.add(filename);
  }
}
