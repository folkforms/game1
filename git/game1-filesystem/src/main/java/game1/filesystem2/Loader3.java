package game1.filesystem2;

import java.io.IOException;
import java.util.Map;
import java.util.function.Supplier;

public abstract class Loader3 {

  public abstract Map<String, Supplier<?>> load(String filename) throws IOException;
}
