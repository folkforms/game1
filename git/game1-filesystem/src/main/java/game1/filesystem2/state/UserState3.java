package game1.filesystem2.state;

import java.util.List;

import game1.filesystem2.datasets.Dataset;

public abstract class UserState3 {

  public abstract List<Dataset> listDatasets();
}
