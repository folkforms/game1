package game1.filesystem2.state;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class GameDataSuppliers {

  private Map<String, Supplier<?>> suppliers = new HashMap<>();

  public void addAll(Map<String, Supplier<?>> map) {
    suppliers.putAll(map);
  }

  @SuppressWarnings("unchecked")
  public <T> T get(String name) {
    Supplier<?> supplier = suppliers.get(name);
    return (T) supplier.get();
  }
}
