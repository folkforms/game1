package game1.filesystem2.state;

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import game1.filesystem2.datasets.Dataset;

public class GameState2 {

  // private FilesInMemory filesInMemory;
  private GameDataSuppliers gameDataSuppliers;

  public GameState2(GameDataSuppliers gameDataSuppliers) {
    this.gameDataSuppliers = gameDataSuppliers;
  }

  public void moveToState(String userStateName) {
    loadFiles(userStateName);
  }

  private void loadFiles(String userStateName) {
    UserState3 userState = UserState3Registry.getUserState(userStateName);
    // filesInMemory.markAllFilesForUnloading();
    List<Dataset> datasets = userState.listDatasets();
    List<String> allFiles = datasets.stream().map(dataset -> dataset.listFiles())
        .flatMap(List::stream).toList();
    Map<String, Supplier<?>> allSuppliers = GameState2Utils.getAllSupplierMaps(allFiles);
    // gameDataSuppliers.markAllSuppliersForRemoval();
    gameDataSuppliers.addAll(allSuppliers);
    // gameDataSuppliers.removeAllMarkedSuppliers();
    // filesInMemory.unloadAllMarkedFiles();
  }
}
