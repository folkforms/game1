package game1.filesystem2.state;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import game1.filesystem2.Loader3;
import game1.filesystem2.Loader3Registry;

public class GameState2Utils {

  public static Map<String, Supplier<?>> getAllSupplierMaps(List<String> files) {
    List<Map<String, Supplier<?>>> supplierMapList = files.stream().map(filename -> {
      Loader3 loader = Loader3Registry.getLoaderForFilename(filename);
      try {
        return loader.load(filename);
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }).toList();

    Map<String, Supplier<?>> flattened = new HashMap<>();
    supplierMapList.forEach(flattened::putAll);
    return flattened;
  }
}
