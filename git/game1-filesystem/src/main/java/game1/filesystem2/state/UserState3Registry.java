package game1.filesystem2.state;

import java.util.HashMap;
import java.util.Map;

import game1.filesystem2.state.UserState3;

public class UserState3Registry {

  private static Map<String, UserState3> states = new HashMap<>();

  public static void add(String userStateName, UserState3 userState) {
    states.put(userStateName, userState);
  }

  public static UserState3 getUserState(String userStateName) {
    UserState3 userState = states.get(userStateName);
    if (userState == null) {
      throw new RuntimeException(String
          .format("Could not find a state named '%s'. Known states are: %s", userStateName, states));
    }
    return userState;
  }
}
