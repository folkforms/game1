package game1.filesystem2;

public class PathUtils2 {

  public static String normaliseSlashes(String pathStr) {
    return pathStr.replace('\\', '/');
  }
}
