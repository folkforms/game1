package game1.filesystem2;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.zip.ZipException;

/**
 * Loads files either from disk or from jars.
 */
public class FilesOnDisk {

  private List<Path> jars = new ArrayList<>();

  public byte[] get(Path filePath) throws IOException {
    Optional<byte[]> bytesMaybe = readFromAllSources(filePath);
    if (bytesMaybe.isPresent()) {
      return bytesMaybe.get();
    }
    throw new FileNotFoundException(
        String.format("Could not find file '%s' either on disk or in any jars", filePath));
  }

  private Optional<byte[]> readFromAllSources(Path filePath) throws IOException {
    if (filePath.toFile().exists()) {
      return Optional.of(Files.readAllBytes(filePath));
    } else {
      for (Path jarPath : jars) {
        Optional<byte[]> bytesMaybe = readFromJar(filePath, jarPath);
        if (bytesMaybe.isPresent()) {
          return Optional.of(bytesMaybe.get());
        }
      }
    }
    return Optional.empty();
  }

  public boolean exists(Path filePath) throws IOException {
    return readFromAllSources(filePath).isPresent();
  }

  private Optional<byte[]> readFromJar(Path filePath, Path jarPath)
      throws ZipException, IOException {
    return ZipFileUtils.loadFile(filePath, jarPath);
  }

  public void addZip(Path path) {
    jars.add(0, path);
  }
}
