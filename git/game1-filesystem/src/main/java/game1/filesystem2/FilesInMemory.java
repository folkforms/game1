package game1.filesystem2;

import java.util.HashMap;
import java.util.Map;

public class FilesInMemory {

  private Map<String, byte[]> data = new HashMap<>();

  public byte[] get(String filename) {
    return data.get(filename);
  }

  public void put(String filename, byte[] bytes) {
    data.put(filename, bytes);
  }
}
