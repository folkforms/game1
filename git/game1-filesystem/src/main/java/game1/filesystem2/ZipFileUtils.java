package game1.filesystem2;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

public class ZipFileUtils {

  public static Optional<byte[]> loadFile(Path filename, Path path)
      throws ZipException, IOException {

    ZipFile zipFile = new ZipFile(path.toFile());
    try {
      Enumeration<? extends ZipEntry> entries = zipFile.entries();
      while (entries.hasMoreElements()) {
        ZipEntry entry = entries.nextElement();
        if (entry.getName().equals(PathUtils2.normaliseSlashes(filename.toString()))) {
          InputStream stream = zipFile.getInputStream(entry);
          try (stream) {
            return Optional.of(stream.readAllBytes());
          }
        }
      }
      return Optional.empty();
    } finally {
      zipFile.close();
    }
  }
}
