package game1.filesystem2;

import java.util.HashMap;
import java.util.Map;

public class Loader3Registry {

  private static Map<String, Loader3> extensionToLoaderMap = new HashMap<>();

  public static void add(String extension, Loader3 loader) {
    if (extension.startsWith(".")) {
      extension = extension.substring(1);
    }
    extensionToLoaderMap.put(extension, loader);
  }

  public static Loader3 getLoaderForFilename(String filename) {
    String extension = filename.substring(filename.lastIndexOf('.') + 1);
    return getLoader(extension);
  }

  private static Loader3 getLoader(String extension) {
    Loader3 loader = extensionToLoaderMap.get(extension);
    if (loader == null) {
      throw new RuntimeException(
          String.format("Could not find a loader for extension '%s'. Known loaders are: %s",
              extension, extensionToLoaderMap));
    }
    return loader;
  }
}
