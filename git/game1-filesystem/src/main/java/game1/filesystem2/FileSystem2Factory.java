package game1.filesystem2;

public class FileSystem2Factory {

  private static FileSystem2 fileSystem;
  private static FilesInMemory filesInMemory;
  private static FilesOnDisk filesOnDisk;

  public static FileSystem2 getInstance() {
    if (fileSystem == null) {
      filesInMemory = new FilesInMemory();
      filesOnDisk = new FilesOnDisk();
      fileSystem = new FileSystem2(filesInMemory, filesOnDisk);
    }
    return fileSystem;
  }

  public static FileSystem2 newTestInstance() {
    return new FileSystem2(new FilesInMemory(), new FilesOnDisk());
  }
}
