package game1.filesystem2;

import java.io.IOException;
import java.nio.file.Path;

/**
 * Loads files either from memory or else from disk.
 */
public class FileSystem2 {

  private FilesInMemory filesInMemory;
  private FilesOnDisk filesOnDisk;

  public FileSystem2(FilesInMemory filesInMemory, FilesOnDisk filesOnDisk) {
    this.filesInMemory = filesInMemory;
    this.filesOnDisk = filesOnDisk;
  }

  public byte[] get(String filename) throws IOException {
    byte[] bytes = filesInMemory.get(filename);
    if (bytes != null) {
      return bytes;
    }
    return filesOnDisk.get(Path.of(filename));
  }

  public void addZip(String zipPath) {
    filesOnDisk.addZip(Path.of(zipPath));
  }
}
