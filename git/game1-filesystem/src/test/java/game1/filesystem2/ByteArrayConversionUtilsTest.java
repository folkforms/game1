package game1.filesystem2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

public class ByteArrayConversionUtilsTest {

  @Test
  public void itConvertsBytesToAList() {
    byte[] bytes = { 'a', 'b', 'c', '\n', 'd', 'e', 'f', '\n' };
    List<String> actual = ByteArrayConversionUtils.toList(bytes);
    assertEquals(List.of("abc", "def"), actual);
  }
}
