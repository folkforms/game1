package game1.filesystem2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import game1.filesystem2.testfixtures.DummyLoader1;
import game1.filesystem2.testfixtures.DummyLoader2;

public class Loader3RegistryTest {

  @Test
  public void itGetsTheLoaderBasedOnFileExtension() {
    Loader3 dummyLoader1 = new DummyLoader1();
    Loader3 dummyLoader2 = new DummyLoader2();
    Loader3Registry.add("aaa", dummyLoader1);
    Loader3Registry.add("bbb", dummyLoader2);
    assertEquals(dummyLoader1, Loader3Registry.getLoaderForFilename("foo.aaa"));
    assertEquals(dummyLoader2, Loader3Registry.getLoaderForFilename("foo.bbb"));
  }
}
