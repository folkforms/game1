package game1.filesystem2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import game1.filesystem2.testfixtures.ExampleLoader;

public class Loader3Test {

  @Test
  public void itLoadsASetOfFiles() throws IOException {
    FileSystem2 fileSystem = FileSystem2Factory.newTestInstance();
    ExampleLoader exampleLoader = new ExampleLoader(fileSystem);

    Map<String, Supplier<?>> actual = exampleLoader
        .load("src/test/resources/example-loader/example-loader-index.txt");

    // Call `get` on the suppliers so we can compare the output more easily
    Map<String, ?> actualAfterCallingGet = actual.entrySet().stream()
        .map(entry -> Map.entry(entry.getKey(), entry.getValue().get()))
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    assertEquals(Map.of("example-loader-1.txt", "aaaaaaaa", "example-loader-2.txt", "bbbbbbbb"),
        actualAfterCallingGet);
  }
}
