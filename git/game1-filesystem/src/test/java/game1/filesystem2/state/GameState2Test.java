package game1.filesystem2.state;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import game1.filesystem2.FileSystem2;
import game1.filesystem2.FileSystem2Factory;
import game1.filesystem2.Loader3Registry;
import game1.filesystem2.testfixtures.gamestatetest.DummyTextFileLoader;
import game1.filesystem2.testfixtures.gamestatetest.GS1UserState;

public class GameState2Test {

  @Test
  public void itLoadsRequiredDatasetsWhenMovingToANewState() {
    FileSystem2 fileSystem = FileSystem2Factory.newTestInstance();
    GameDataSuppliers gameDataSuppliers = new GameDataSuppliers();
    UserState3Registry.add("fooState", new GS1UserState());
    Loader3Registry.add("txt", new DummyTextFileLoader(fileSystem));
    GameState2 gameState = new GameState2(gameDataSuppliers);

    gameState.moveToState("fooState");

    List<String> foo = gameDataSuppliers.get("src/test/resources/gamestatetest/foo.txt");
    assertEquals(List.of("foo1", "foo2"), foo);
  }
}
