package game1.filesystem2.state;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import game1.filesystem2.testfixtures.DummyUserState1;
import game1.filesystem2.testfixtures.DummyUserState2;

public class UserState3RegistryTest {

  @Test
  public void itGetsTheStateByName() {
    UserState3 dummyUserState1 = new DummyUserState1();
    UserState3 dummyUserState2 = new DummyUserState2();
    UserState3Registry.add("fooState", dummyUserState1);
    UserState3Registry.add("barState", dummyUserState2);
    assertEquals(dummyUserState1, UserState3Registry.getUserState("fooState"));
    assertEquals(dummyUserState2, UserState3Registry.getUserState("barState"));
  }
}
