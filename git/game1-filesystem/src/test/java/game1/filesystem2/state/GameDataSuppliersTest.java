package game1.filesystem2.state;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Map;

import org.junit.jupiter.api.Test;

public class GameDataSuppliersTest {

  @Test
  public void itGetsSupplierByName() {
    GameDataSuppliers gds = new GameDataSuppliers();
    gds.addAll(Map.of("foo", () -> "foo data", "bar", () -> "bar data"));
    String actual1 = gds.get("foo");
    assertEquals("foo data", actual1);
    String actual2 = gds.get("bar");
    assertEquals("bar data", actual2);
  }
}
