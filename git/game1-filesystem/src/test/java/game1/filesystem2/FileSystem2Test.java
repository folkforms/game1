package game1.filesystem2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class FileSystem2Test {
  // - [ ] It gets a file from memory
  // - [ ] It gets a file from disk
  // - [ ] It allows registering of multiple jars

  private FilesInMemory filesInMemory;
  private FilesOnDisk filesOnDisk;

  @BeforeEach
  public void beforeEach() {
    filesInMemory = new FilesInMemory();
    filesOnDisk = new FilesOnDisk();
  }

  @Test
  public void itGetsAFileFromMemory() throws IOException {
    filesInMemory.put("aaa.txt", "aaa".getBytes());

    FileSystem2 fileSystem2 = new FileSystem2(filesInMemory, filesOnDisk);
    byte[] bytes = fileSystem2.get("aaa.txt");
    assertEquals("aaa", new String(bytes));
  }

  @Test
  public void itGetsAFileFromDiskIfItIsNotInMemory() throws IOException {
    FileSystem2 fileSystem2 = new FileSystem2(filesInMemory, filesOnDisk);
    byte[] bytes = fileSystem2.get("src/test/resources/foo.txt");
    assertEquals("foo\n", new String(bytes));
  }

  @Test
  public void itCanAddAdditionalZips() throws IOException {
    FileSystem2 fileSystem2 = new FileSystem2(filesInMemory, filesOnDisk);
    fileSystem2.addZip("src/test/resources/muk.zip");
    fileSystem2.addZip("src/test/resources/other.zip");
    fileSystem2.addZip("src/test/resources/aaa1.zip");
    fileSystem2.addZip("src/test/resources/aaa2.zip");
    byte[] bytes1 = fileSystem2.get("muk.txt");
    assertEquals("muk\n", new String(bytes1));
    byte[] bytes2 = fileSystem2.get("folder/bip.txt");
    assertEquals("bip\n", new String(bytes2));
    byte[] bytes3 = fileSystem2.get("aaa.txt");
    assertEquals("222\n", new String(bytes3));
  }
}
