package game1.filesystem2.datasets;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import game1.filesystem2.testfixtures.ExampleDataset;

public class DatasetTest {

  @Test
  public void itListsTheFilesToBeLoaded() {
    Dataset dataset = new ExampleDataset();
    List<String> files = dataset.listFiles();
    assertEquals(List.of("foo.txt", "bar.txt", "src/main/resources/folder/*"), files);
  }
}
