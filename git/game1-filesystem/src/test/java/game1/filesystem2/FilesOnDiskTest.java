package game1.filesystem2;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;

import org.junit.jupiter.api.Test;

public class FilesOnDiskTest {

  @Test
  public void itReadsDataFromAFile() throws IOException {
    FilesOnDisk f = new FilesOnDisk();
    byte[] bytes1 = f.get(Path.of("src/test/resources/foo.txt"));
    assertEquals("foo\n", new String(bytes1));
    byte[] bytes2 = f.get(Path.of("src/test/resources/bar.txt"));
    assertEquals("bar\n", new String(bytes2));
  }

  @Test
  public void itReadsDataFromAZipIfFileIsNotPresentOnDisk() throws IOException {
    FilesOnDisk f = new FilesOnDisk();
    f.addZip(Path.of("src/test/resources/muk.zip"));
    byte[] bytes1 = f.get(Path.of("muk.txt"));
    assertEquals("muk\n", new String(bytes1));
    byte[] bytes2 = f.get(Path.of("folder/bip.txt"));
    assertEquals("bip\n", new String(bytes2));
  }

  @Test
  public void itReadsMultipleZipsToFindTheFile() throws IOException {
    FilesOnDisk f = new FilesOnDisk();
    f.addZip(Path.of("src/test/resources/muk.zip"));
    f.addZip(Path.of("src/test/resources/other.zip"));
    byte[] bytes1 = f.get(Path.of("muk.txt"));
    assertEquals("muk\n", new String(bytes1));
    byte[] bytes2 = f.get(Path.of("folder/bip.txt"));
    assertEquals("bip\n", new String(bytes2));
  }

  @Test
  public void itGetsTheFileFromTheMostRecentlyAddedJar() throws IOException {
    FilesOnDisk f = new FilesOnDisk();
    f.addZip(Path.of("src/test/resources/aaa1.zip"));
    f.addZip(Path.of("src/test/resources/aaa2.zip"));
    byte[] bytes1 = f.get(Path.of("aaa.txt"));
    assertEquals("222\n", new String(bytes1));
  }

  @Test
  public void itThrowsIfTheFileIsNotFound() throws IOException {
    FilesOnDisk f = new FilesOnDisk();
    assertThrows(FileNotFoundException.class, () -> f.get(Path.of("does-not-exist.txt")), "hi");
  }

  @Test
  public void itCanValidateThatAFileExists() throws IOException {
    FilesOnDisk f = new FilesOnDisk();
    f.addZip(Path.of("src/test/resources/muk.zip"));
    assertTrue(f.exists(Path.of("src/test/resources/foo.txt")));
    assertTrue(f.exists(Path.of("muk.txt")));
    assertFalse(f.exists(Path.of("does-not-exist.txt")));
  }
}
