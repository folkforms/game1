package game1.filesystem2.testfixtures.gamestatetest;

import java.util.List;

import game1.filesystem2.datasets.Dataset;
import game1.filesystem2.state.UserState3;

public class GS1UserState extends UserState3 {

  @Override
  public List<Dataset> listDatasets() {
    return List.of(new GS1Dataset());
  }
}
