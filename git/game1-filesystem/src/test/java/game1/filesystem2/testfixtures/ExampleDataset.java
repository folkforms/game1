package game1.filesystem2.testfixtures;

import game1.filesystem2.datasets.Dataset;

public class ExampleDataset extends Dataset {
  public ExampleDataset() {
    add("foo.txt");
    add("bar.txt");
    add("src/main/resources/folder/*");
  }
}
