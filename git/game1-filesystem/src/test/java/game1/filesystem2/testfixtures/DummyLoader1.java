package game1.filesystem2.testfixtures;

import java.io.IOException;
import java.util.Map;
import java.util.function.Supplier;

import game1.filesystem2.Loader3;

public class DummyLoader1 extends Loader3 {

  public DummyLoader1() {
  }

  @Override
  public Map<String, Supplier<?>> load(String filename) throws IOException {
    return null;
  }
}
