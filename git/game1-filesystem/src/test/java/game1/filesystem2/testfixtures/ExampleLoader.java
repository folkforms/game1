package game1.filesystem2.testfixtures;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import game1.filesystem2.ByteArrayConversionUtils;
import game1.filesystem2.FileSystem2;
import game1.filesystem2.Loader3;

public class ExampleLoader extends Loader3 {

  private FileSystem2 fileSystem;

  public ExampleLoader(FileSystem2 fileSystem) {
    this.fileSystem = fileSystem;
  }

  @Override
  public Map<String, Supplier<?>> load(String filename) throws IOException {
    int lastSlash = filename.lastIndexOf('/');
    String folder = filename.substring(0, lastSlash);
    String indexFile = filename.substring(lastSlash + 1);
    byte[] indexFileBytes = fileSystem.get(String.format("%s/%s", folder, indexFile));
    List<String> otherFiles = ByteArrayConversionUtils.toList(indexFileBytes);

    Map<String, Supplier<?>> output = new HashMap<>();
    for (String otherFilename : otherFiles) {
      byte[] bytes = fileSystem.get(String.format("%s/%s", folder, otherFilename));
      String fileContents = ByteArrayConversionUtils.toList(bytes).get(0);
      output.put(otherFilename, () -> fileContents);
    }
    return output;
  }
}
