package game1.filesystem2.testfixtures.gamestatetest;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import game1.filesystem2.ByteArrayConversionUtils;
import game1.filesystem2.FileSystem2;
import game1.filesystem2.Loader3;

public class DummyTextFileLoader extends Loader3 {

  private FileSystem2 fileSystem;

  public DummyTextFileLoader(FileSystem2 fileSystem) {
    this.fileSystem = fileSystem;
  }

  @Override
  public Map<String, Supplier<?>> load(String filename) throws IOException {
    byte[] bytes = fileSystem.get(filename);
    List<String> list = ByteArrayConversionUtils.toList(bytes);
    return Map.of(filename, () -> list);
  }
}
