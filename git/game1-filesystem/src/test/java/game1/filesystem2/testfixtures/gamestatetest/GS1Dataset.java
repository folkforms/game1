package game1.filesystem2.testfixtures.gamestatetest;

import game1.filesystem2.datasets.Dataset;

public class GS1Dataset extends Dataset {

  public GS1Dataset() {
    add("src/test/resources/gamestatetest/foo.txt");
  }
}
