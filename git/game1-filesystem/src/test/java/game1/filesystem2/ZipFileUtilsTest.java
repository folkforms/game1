package game1.filesystem2;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;
import java.util.zip.ZipException;

import org.junit.jupiter.api.Test;

public class ZipFileUtilsTest {

  @Test
  public void itReadsAFileFromAZip() throws ZipException, IOException {
    Optional<byte[]> bytesMaybe = ZipFileUtils.loadFile(Path.of("muk.txt"),
        Path.of("src/test/resources/muk.zip"));
    assertTrue(bytesMaybe.isPresent());
    assertEquals("muk\n", new String(bytesMaybe.get()));
  }

  @Test
  public void itReadsAFileFromAFolderInAZip() throws ZipException, IOException {
    Optional<byte[]> bytesMaybe = ZipFileUtils.loadFile(Path.of("folder/bip.txt"),
        Path.of("src/test/resources/muk.zip"));
    assertTrue(bytesMaybe.isPresent());
    assertEquals("bip\n", new String(bytesMaybe.get()));
  }

  @Test
  public void itReturnsAnEmptyOptionalIfFileWasNotFound() throws ZipException, IOException {
    Optional<byte[]> bytesMaybe = ZipFileUtils.loadFile(Path.of("does-not-exist.txt"),
        Path.of("src/test/resources/muk.zip"));
    assertTrue(bytesMaybe.isEmpty());
  }
}
