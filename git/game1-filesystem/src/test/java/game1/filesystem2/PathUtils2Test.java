package game1.filesystem2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.nio.file.Path;

import org.junit.jupiter.api.Test;

public class PathUtils2Test {

  @Test
  public void itNormalisesSlashes() {
    String pathStr = "src/main/resources/foo.txt";
    Path path = Path.of(pathStr);
    assertEquals("src\\main\\resources\\foo.txt", path.toString());
    String actual = PathUtils2.normaliseSlashes(path.toString());
    assertEquals(pathStr, actual);
  }
}
