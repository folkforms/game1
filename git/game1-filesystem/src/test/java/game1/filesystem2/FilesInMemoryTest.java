package game1.filesystem2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class FilesInMemoryTest {

  @Test
  public void ifReturnsSomeDataBasedOnFilename() {
    FilesInMemory f = new FilesInMemory();
    f.put("aaa.txt", "aaa".getBytes());
    f.put("bbb.txt", "bbb".getBytes());
    byte[] bytes1 = f.get("aaa.txt");
    assertEquals("aaa", new String(bytes1));
    byte[] bytes2 = f.get("bbb.txt");
    assertEquals("bbb", new String(bytes2));
    byte[] bytes3 = f.get("does-not-exist.txt");
    assertEquals(null, bytes3);
  }
}
