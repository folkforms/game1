package game1.cutscenes.v9c.antlr.c9fileparser;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import folkforms.log.Log;
import game1.debug.DebugScopes;

class CutsceneUtilsTest {

  @BeforeAll
  public static void beforeAll() {
    DebugScopes.init();
    DebugScopes.activate("Cutscenes");
    Log.setLevel(Log.LEVEL_DEBUG);
  }

  @Test
  void itConvertsTagsToFunctionNames() {
    assertEquals("foo", CutsceneUtils.convertTagToFunctionName("#foo"));
    assertEquals("foo_bar", CutsceneUtils.convertTagToFunctionName("#foo bar"));
    assertEquals("dont_look_now", CutsceneUtils.convertTagToFunctionName("#don't look now"));
  }
}
