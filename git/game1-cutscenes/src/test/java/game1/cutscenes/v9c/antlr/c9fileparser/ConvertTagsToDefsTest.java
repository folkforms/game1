package game1.cutscenes.v9c.antlr.c9fileparser;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import folkforms.log.Log;
import game1.debug.DebugScopes;

class ConvertTagsToDefsTest {

  @BeforeAll
  public static void beforeAll() {
    DebugScopes.init();
    DebugScopes.activate("Cutscenes");
    Log.setLevel(Log.LEVEL_DEBUG);
  }

  @Test
  void itConvertsTagsToDefs() throws IOException {
    FileFragment ff1 = FileFragmentTestFixture.createTag("foo stuff");

    List<FileFragment> fileFragments = new ConvertTagsToDefs(List.of(ff1)).convertTagsToDefs();

    List<String> expected = Arrays.stream("""
        def foo_stuff() {
            bar();
        }
        """.split("\\n")).toList();
    assertEquals(expected, fileFragments.get(0).getLines());
  }
}
