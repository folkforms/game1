package game1.cutscenes.v9c.antlr.c9;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import folkforms.log.Log;
import folkforms.textio.TextIO;
import game1.cutscenes.v9c.antlr.statements.Statement;
import game1.debug.DebugScopes;

class C9Test {

  @BeforeAll
  public static void beforeAll() {
    DebugScopes.init();
    DebugScopes.activate("Cutscenes");
    Log.setLevel(Log.LEVEL_DEBUG);
  }

  @Test
  void itHandlesExpressionsInsideBrackets() throws IOException {
    List<String> expected = List.of( //
        // foo(2, a + b + c, x + 6);
        "_add(a, b) => _temp2", //
        "_add(_temp2, c) => _temp1", //
        "_add(x, 6) => _temp3", //
        "foo(2, _temp1, _temp3) => _temp0");
    test(expected, "src/test/resources/antlr/c9/c9-test-expressions-inside-brackets.txt");
  }

  @Test
  void itHandlesAssignment() throws IOException {
    List<String> expected = List.of( //
        // a = b + c + d;
        "_add(b, c) => _temp2", //
        "_add(_temp2, d) => _temp1", //
        "_assign(a, _temp1)", //
        // a = "foo";
        "_assign(a, \"foo\")", //
        // a = 5.0;
        "_assign(a, 5.0)", //
        // a = x + 4;
        "_add(x, 4) => _temp3", //
        "_assign(a, _temp3)", //
        // a = foo();
        "foo() => _temp4", //
        "_assign(a, _temp4)", //
        // a = bar(1, foo());
        "foo() => _temp6", //
        "bar(1, _temp6) => _temp5", //
        "_assign(a, _temp5)" //
    );
    test(expected, "src/test/resources/antlr/c9/c9-test-assign.txt");
  }

  @Test
  void itHandlesMultiplicationAndDivision() throws IOException {
    List<String> expected = List.of( //
        // e = 5 * 7 + 2;
        "_multiply(5, 7) => _temp2", //
        "_add(_temp2, 2) => _temp1", //
        "_assign(e, _temp1)", //
        // e = 2 + 5 * 7;
        "_multiply(5, 7) => _temp4", //
        "_add(2, _temp4) => _temp3", //
        "_assign(e, _temp3)", //
        // f = x / 7;
        "_divide(x, 7) => _temp5", //
        "_assign(f, _temp5)", //
        // g = 5 * (7 + 2)
        "_add(7, 2) => _temp7", //
        "_multiply(5, _temp7) => _temp6", //
        "_assign(g, _temp6)");
    test(expected, "src/test/resources/antlr/c9/c9-test-multiply-divide.txt");
  }

  @Test
  void itHandlesAdditionAndSubtraction() throws IOException {
    List<String> expected = List.of( //
        // a = b + c + d;
        "_add(b, c) => _temp2", //
        "_add(_temp2, d) => _temp1", //
        "_assign(a, _temp1)", //
        // a = x + 4;
        "_add(x, 4) => _temp3", //
        "_assign(a, _temp3)", //
        // b = c + foo(d);
        "foo(d) => _temp5", //
        "_add(c, _temp5) => _temp4", //
        "_assign(b, _temp4)", //
        // e = f - d;
        "_subtract(f, d) => _temp6", //
        "_assign(e, _temp6)" //
    );
    test(expected, "src/test/resources/antlr/c9/c9-test-add-subtract.txt");
  }

  @Test
  void itHandlesEquals() throws IOException {
    List<String> expected = List.of( //
        // 1 == 2;
        "_equals(1, 2) => _temp0", //
        // 4 == 4;
        "_equals(4, 4) => _temp1" //
    );
    test(expected, "src/test/resources/antlr/c9/c9-test-equals.txt");
  }

  @Test
  void itHandlesStringLiterals() throws IOException {
    List<String> expected = List.of( //
        // foo("hello");
        "foo(\"hello\") => _temp0" //
    );
    test(expected, "src/test/resources/antlr/c9/c9-test-string-literals.txt");
  }

  @Test
  void itHandlesFunctionCalls() throws IOException {
    List<String> expected = List.of( //
        // foo(x, y);
        "foo(x, y) => _temp0", //
        // muk(bar(3));
        "bar(3) => _temp2", //
        "muk(_temp2) => _temp1" //
    );
    test(expected, "src/test/resources/antlr/c9/c9-test-function-calls.txt");
  }

  @Test
  void itHandlesDotNotation() throws IOException {
    List<String> expected = List.of( //
        // x.foo(5, 6);
        "_invoke(x, \"foo\", 5, 6) => _temp0", //
        // a = b.c();
        "_invoke(b, \"c\") => _temp2", //
        "_assign(a, _temp2)", //
        // x = 5 * x.foo(\"stuff\");
        "_invoke(x, \"foo\", \"stuff\") => _temp4", //
        "_multiply(5, _temp4) => _temp3", //
        "_assign(x, _temp3)", //
        // foo(5, x.bar(1));
        "_invoke(x, \"bar\", 1) => _temp5", //
        "foo(5, _temp5) => _temp4" //
    );
    test(expected, "src/test/resources/antlr/c9/c9-test-dot-notation.txt");
  }

  @Test
  void itExpectsDotNotationToHaveBrackets() throws IOException {
    try {
      test(List.of(), "src/test/resources/antlr/c9/c9-test-dot-notation-2.txt");
    } catch (RuntimeException e) {
      assertEquals("Found 1 syntax error", e.getMessage());
    }
  }

  @Test
  void itHandlesLogicalAnd() throws IOException {
    List<String> expected = List.of( //
        "_logicalAnd(true, true) => _temp1", //
        "_assign(a, _temp1)", //
        "_logicalAnd(true, false) => _temp2", //
        "_assign(b, _temp2)", //
        "_logicalAnd(false, true) => _temp3", //
        "_assign(c, _temp3)", //
        "_logicalAnd(false, false) => _temp4", //
        "_assign(d, _temp4)" //
    );
    test(expected, "src/test/resources/antlr/c9/c9-test-logical-and.txt");
  }

  @Test
  void itHandlesLogicalOr() throws IOException {
    List<String> expected = List.of( //
        "_logicalOr(true, true) => _temp1", //
        "_assign(a, _temp1)", //
        "_logicalOr(true, false) => _temp2", //
        "_assign(b, _temp2)", //
        "_logicalOr(false, true) => _temp3", //
        "_assign(c, _temp3)", //
        "_logicalOr(false, false) => _temp4", //
        "_assign(d, _temp4)" //
    );
    test(expected, "src/test/resources/antlr/c9/c9-test-logical-or.txt");
  }

  @Test
  void itHandlesMultilineComments() throws IOException {
    List<String> expected = List.of( //
        "_assign(a, 1)" //
    );
    test(expected, "src/test/resources/antlr/c9/c9-test-multiline-comments.txt");
  }

  @Test
  void itHandlesSinglelineComments() throws IOException {
    List<String> expected = List.of( //
        "_assign(a, 2)" //
    );
    test(expected, "src/test/resources/antlr/c9/c9-test-single-line-comments.txt");
  }

  private void test(List<String> expected, String resourcePath) throws IOException {
    List<String> input = new TextIO().readAsList(resourcePath);
    C9LanguageFragment c = new C9LanguageFragment("foo", input);
    List<Statement> expressions = c.getStatements();
    List<String> actual = expressions.stream().map(Statement::toString).toList();
    assertEquals(expected, actual);
  }
}
