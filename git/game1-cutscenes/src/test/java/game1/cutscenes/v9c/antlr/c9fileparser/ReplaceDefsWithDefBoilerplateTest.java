package game1.cutscenes.v9c.antlr.c9fileparser;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import folkforms.log.Log;
import game1.debug.DebugScopes;

class ReplaceDefsWithDefBoilerplateTest {

  @BeforeAll
  public static void beforeAll() {
    DebugScopes.init();
    DebugScopes.activate("Cutscenes");
    Log.setLevel(Log.LEVEL_DEBUG);
  }

  @Test
  void itReplacesDefsWithDefBoilerplate() throws IOException {
    FileFragment ff1 = FileFragmentTestFixture.createDef("foo", "text", "character");

    List<FileFragment> fileFragments = new ReplaceDefsWithDefBoilerplate(List.of(ff1)).replace();

    List<String> expected = Arrays.stream("""
        // foo function
        _resolveLocalVariables(text, character);
          bar();
        """.split("\\n")).toList();
    assertEquals(expected, fileFragments.get(0).getLines());
  }

  @Test
  void itReplacesDefsWithDefBoilerplateNoArgsVersion() throws IOException {
    FileFragment ff1 = FileFragmentTestFixture.createDef("foo");

    List<FileFragment> fileFragments = new ReplaceDefsWithDefBoilerplate(List.of(ff1)).replace();

    List<String> expected = Arrays.stream("""
        // foo function
          bar();
        """.split("\\n")).toList();
    assertEquals(expected, fileFragments.get(0).getLines());
  }
}
