package game1.cutscenes.v9c.antlr.c9fileparser;

import java.util.Arrays;
import java.util.stream.Collectors;

import game1.cutscenes.v9c.antlr.c9fileparser.FileFragment;

public class FileFragmentTestFixture {

  public static FileFragment createTag(String tag) {
    FileFragment ff = new FileFragment();
    ff.add("#" + tag);
    ff.add("    bar();");
    return ff;
  }

  public static FileFragment createDef(String def, String... args) {
    FileFragment ff = new FileFragment();
    ff.add(
        String.format("def %s(%s) {", def, Arrays.stream(args).collect(Collectors.joining(", "))));
    ff.add("  bar();");
    ff.add("}");
    return ff;
  }
}
