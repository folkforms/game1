package game1.cutscenes.v9c.antlr.c9fileparser;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import folkforms.log.Log;
import game1.cutscenes.v9c.antlr.c9fileparser.FileFragment;
import game1.cutscenes.v9c.antlr.c9fileparser.FileFragmentConverter;
import game1.debug.DebugScopes;

class FileFragmentConverterTest {

  @BeforeAll
  public static void beforeAll() {
    DebugScopes.init();
    DebugScopes.activate("Cutscenes");
    Log.setLevel(Log.LEVEL_DEBUG);
  }

  @Test
  void itConvertsAFileIntoMultipleFileFragments() throws IOException {
    List<String> input = Arrays.stream("""
        def speak(String text, Var character) {
          displayText(text, character.x, character.y)
        }

        #say hello
            speak("hello there!", obi-wan-kenobi)
            wait(250)
        """.split("\\n")).toList();

    List<FileFragment> fileFragments = new FileFragmentConverter(input).createFileFragments();
    List<String> expected0 = Arrays.stream("""
        def speak(String text, Var character) {
          displayText(text, character.x, character.y)
        }
        """.split("\\n")).toList();
    List<String> expected1 = Arrays.stream("""
        #say hello
            speak("hello there!", obi-wan-kenobi)
            wait(250)
        """.split("\\n")).toList();
    assertEquals(expected0, fileFragments.get(0).getLines());
    assertEquals(expected1, fileFragments.get(1).getLines());
  }
}
