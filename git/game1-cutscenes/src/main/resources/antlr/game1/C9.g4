grammar C9;

prog: expression_list EOF;

terminator : terminator SEMICOLON
           | SEMICOLON
           ;

expression_list : MULTI_LINE_COMMENT
                | SINGLE_LINE_COMMENT
                | expression_list expression terminator
                | expression terminator
                | terminator
                ;

expression : version                                      # exprVersion
           | marker                                       # exprMarker
           | LEFT_RBRACKET expression RIGHT_RBRACKET      # exprInBrackets
           | dot_notation                                 # exprDotNotation
           | function_call                                # exprFunctionCall
           | expression EQUALS expression                 # exprEquals
           | expression op=(MULTIPLY | DIVIDE) expression # exprMultiplyDivide
           | expression op=(ADD | SUBTRACT) expression    # exprAddSubtract
           | expression op=LOGICAL_AND expression         # exprLogicalAnd
           | expression op=LOGICAL_OR expression          # exprLogicalOr
           | expression op=ASSIGN expression              # exprAssign
           | value                                        # exprValue
           ;

value : BOOLEAN_LITERAL  # valueBooleanLiteral
      | STRING_LITERAL   # valueStringLiteral
      | FLOAT_LITERAL    # valueFloatLiteral
      | id_              # valueId
      ;

dot_notation : member=id_ '.' name=id_ LEFT_RBRACKET params=param_list RIGHT_RBRACKET
             | member=id_ '.' name=id_ LEFT_RBRACKET RIGHT_RBRACKET
             ;

function_call : name=id_ LEFT_RBRACKET params=param_list RIGHT_RBRACKET
              | name=id_ LEFT_RBRACKET RIGHT_RBRACKET
              ;

param_list : param
           | param_list COMMA expression
           | param_list COMMA param
           ;

param : value         # simpleParam
      | dot_notation  # nestedDotNotation
      | function_call # nestedFunctionCall
      ;

// -------- Constants --------

WHITESPACE : ' ' -> skip;
NEWLINE : [\r]?[\n]+ -> skip;
COMMA : ',';
SEMICOLON : ';';
LEFT_RBRACKET : '(';
RIGHT_RBRACKET : ')';

MULTI_LINE_COMMENT  : '/*' .*? '*/' -> channel(HIDDEN);
SINGLE_LINE_COMMENT : '//' ~[\r\n]* -> channel(HIDDEN);

FLOAT_LITERAL : [0-9]+'.'?[0-9]*;
fragment ESCAPED_QUOTE : '\\"';
STRING_LITERAL : '"' ( ESCAPED_QUOTE | ~('\n'|'\r') )*? '"'
               | '\'' ( ESCAPED_QUOTE | ~('\n'|'\r') )*? '\'';
BOOLEAN_LITERAL: TRUE | FALSE;

EQUALS: '==';
ASSIGN: '=';
ADD: '+';
DIVIDE: '/';
MULTIPLY: '*';
SUBTRACT : '-';
LOGICAL_AND : '&&';
LOGICAL_OR : '||';

MARKER : '#' [a-zA-Z][a-zA-Z0-9_-]*;
marker : MARKER;

version: VERSION_PREFIX VERSION_NUMBER;
VERSION_PREFIX: '@version ';
VERSION_NUMBER: [0-9]+'.'[0-9]+'.'[0-9]+;

TRUE: 'true';
FALSE: 'false';

ID : '@' [a-zA-Z_][a-zA-Z0-9_]*
   | [a-zA-Z_][a-zA-Z0-9_]*;
id_ : ID;
