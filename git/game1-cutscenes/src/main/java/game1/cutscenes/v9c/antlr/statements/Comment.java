package game1.cutscenes.v9c.antlr.statements;

public class Comment implements Statement {

  private String text;

  public Comment(String text) {
    this.text = text;
  }

  @Override
  public String toString() {
    return String.format("%s", text);
  }
}
