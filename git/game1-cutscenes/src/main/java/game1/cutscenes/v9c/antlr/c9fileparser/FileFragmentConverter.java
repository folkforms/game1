package game1.cutscenes.v9c.antlr.c9fileparser;

import java.util.ArrayList;
import java.util.List;

class FileFragmentConverter {

  private List<String> input;

  public FileFragmentConverter(List<String> input) {
    this.input = input;
  }

  public List<FileFragment> createFileFragments() {
    List<FileFragment> output = new ArrayList<>();
    int i = 0;
    FileFragment current = null;
    while (i < input.size()) {
      String line = input.get(i);
      if (line.length() == 0) {
        i++;
        continue;
      }
      if (line.startsWith("#") || line.startsWith("def ")) {
        current = new FileFragment();
        output.add(current);
      }
      current.add(line);
      i++;
    }
    return output;
  }
}
