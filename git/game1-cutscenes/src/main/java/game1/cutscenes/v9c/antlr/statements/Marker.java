package game1.cutscenes.v9c.antlr.statements;

public class Marker implements Statement {

  private String marker;

  public Marker(String marker) {
    this.marker = marker;
  }

  @Override
  public String toString() {
    return String.format("%s", marker);
  }
}
