package game1.cutscenes.v9c.antlr.c9fileparser;

public class CutsceneUtils {

  public static String convertTagToFunctionName(String tag) {
    if (tag.startsWith("#")) {
      tag = tag.substring(1);
    }
    return tag.replaceAll("\\s+", "_").replaceAll("-", "_").replaceAll("[']", "");
  }
}
