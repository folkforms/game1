package game1.cutscenes.v9c.antlr.c9;

import java.util.List;
import java.util.stream.Collectors;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import antlr.game1.C9Lexer;
import antlr.game1.C9Parser;
import folkforms.log.Log;
import game1.cutscenes.v9c.antlr.statements.Statement;

public class C9LanguageFragment {

  private String functionName;
  private String inputText;
  private C9TreeListener treeListener;

  public C9LanguageFragment(String functionName, String inputText) {
    this.functionName = functionName;
    this.inputText = inputText;
    parse();
  }

  public C9LanguageFragment(String functionName, List<String> inputText) {
    this(functionName, inputText.stream().collect(Collectors.joining("\n")));
  }

  private void parse() {
    C9Lexer lexer = new C9Lexer(CharStreams.fromString(inputText));
    CommonTokenStream tokens = new CommonTokenStream(lexer);
    C9Parser parser = new C9Parser(tokens);
    ParseTree tree = parser.prog();
    throwIfSyntaxErrors(parser.getNumberOfSyntaxErrors());
    ParseTreeWalker walker = new ParseTreeWalker();
    treeListener = new C9TreeListener();
    walker.walk(treeListener, tree);
  }

  private void throwIfSyntaxErrors(int numErrors) {
    if (numErrors > 0) {
      throw new RuntimeException(
          String.format("Found %s syntax error%s", numErrors, numErrors < 2 ? "" : "s"));
    }
  }

  public String getFunctionName() {
    return functionName;
  }

  public List<Statement> getStatements() {
    return treeListener.getStatements();
  }

  public void debug_printData() {
    Log.info("function name: %s", functionName);
    treeListener.getStatements().forEach(s -> Log.info("    statement: %s", s));
  }
}
