package game1.cutscenes.v9c.antlr.c9fileparser;

import java.util.ArrayList;
import java.util.List;

class FileFragment {

  private List<String> lines = new ArrayList<>();

  public void add(String line) {
    lines.add(line);
  }

  public List<String> getLines() {
    return lines;
  }

  public String get(int index) {
    return lines.get(index);
  }

  public String set(int index, String replacement) {
    return lines.set(index, replacement);
  }

  public void add(int index, String replacement) {
    lines.add(index, replacement);
  }

  public String remove(int index) {
    return lines.remove(index);
  }

  public int size() {
    return lines.size();
  }

  @Override
  public String toString() {
    return String.format("FileFragment%s@%s", lines, hashCode());
  }
}
