# Cutscenes v9

## High-level overview

`game1-cutscenes` handles the loading and parsing of a `.c9` file via `C9FileParser`.

`game1-toolkit-cutscenes` handles the playing and interpreting of a cutscene, plus built-in functions like add/subtract, and Game1-specific functions like LogText.

Every statement in a cutscene is a function, e.g. `add(1,2)` or `displayText("hello there!")`.

## Loading .c9 files

Main:

    Engine.GAME_FILES.addMultiLoader("c9", C9FileMultiLoader.class);

Dataset:

    Engine.GAME_FILES.register("src/main/resources/foo.c9", TAG);

FIXME C9: I want some way to automatically register "c9" file loaders (and others like yaml) so there are default loaders for Game1 stuff. Obviously users will be able to override them if they really want to.

## How to play a cutscene

    var a = CutscenePlayerUtils.playSingleLine("@x = foo()", Engine.VARIABLES);
    var b = CutscenePlayerUtils.playTag("#foo", Engine.VARIABLES);
    var c = CutscenePlayerUtils.playStatement("@y = add(1, 2)", Engine.VARIABLES);

**NB:** The `@` symbol references a variable in `Engine.VARIABLES`.

FIXME C9: I'm still not sure how a cutscene player will live in the state. A global object or per-instance?

FIXME C9: I *think* I pass Engine.VARIABLES in order to allow test classes to not need to know about the Engine class. But I should either (a) have an initial setup like `CutscenePlayerUtils.setVariablesObject(Engine.VARIABLES)` or (b) use thin wrapper methods so `internal_playSingleLine(String, Variables)` does the heavy lifting and `playSingleLine(String)` just calls it with Engine.VARIABLES as a second parameter.

## Functions

You can define functions directly in C9 files:

    def addThreeNumbers(a, b, c) {
      return a + b + c;
    }

Alternatively, you can:

- Write your function as java code
- Register it using `CutscenePlayerUtils.registerCutsceneFunctionAsGlobalVariable(MyCutsceneFunction.class, Engine.VARIABLES);`
- It will now be available to cutscenes using its `getName` value

----

FIXME C9: I need a `return` function (and/or use Ruby's thing of last item is the return value.)

Note: The code above ("var a = ...") or indeed the use of "return" in "addThreeNumbesr" seems to imply I already have this?

----

Built-in functions

(Note: These are provided by game1-toolkit-cutscenes, not game1-cutscenes. So technically that should have another document. But really we just want one big document. So mayube put them here but with a note that they are actually defined in game1-toolkit-cutscenes.)

Assign ("=")
Add ("+")
Subtract ("-")
Divide ("/")
Multiply ("*")
Invoke (dot-notation)
LogicalAnd ("&&")
LogicalOr ("||")

FIXME Add examples of their use.

