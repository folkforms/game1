package game1.cutscenes.v9c.antlr.statements;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Function implements Statement {

  private String name;
  private List<Arg> args = new ArrayList<>();
  private Optional<String> assignmentVariable;

  public Function(String name, List<Arg> args, String assignmentVariable) {
    this.name = name;
    this.args = args;
    this.assignmentVariable = Optional.ofNullable(assignmentVariable);
  }

  public Function(String name, List<Arg> args) {
    this(name, args, null);
  }

  public String getName() {
    return name;
  }

  public List<Arg> getArgs() {
    return args;
  }

  public Optional<String> getAssignmentVariable() {
    return assignmentVariable;
  }

  @Override
  public String toString() {
    return String.format("%s(%s)%s", name,
        args.stream().map(Arg::getText).collect(Collectors.joining(", ")),
        assignmentVariable.isEmpty() ? "" : String.format(" => %s", assignmentVariable.get()));
  }
}
