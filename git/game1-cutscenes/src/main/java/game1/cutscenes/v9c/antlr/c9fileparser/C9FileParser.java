package game1.cutscenes.v9c.antlr.c9fileparser;

import java.util.Arrays;
import java.util.List;

import game1.cutscenes.v9c.antlr.c9.C9LanguageFragment;

public class C9FileParser {

  private List<String> input;

  public C9FileParser(List<String> input) {
    this.input = input;
  }

  public C9FileParser(String input) {
    this.input = Arrays.stream(input.split("\\n")).toList();
  }

  public List<C9LanguageFragment> parse() {
    // Phase 1: Convert file into file fragments
    List<FileFragment> fileFragments = new FileFragmentConverter(input).createFileFragments();
    // Phase 2: Convert tags to defs
    fileFragments = new ConvertTagsToDefs(fileFragments).convertTagsToDefs();
    // Phase 3: Replace defs with def boilerplate
    fileFragments = new ReplaceDefsWithDefBoilerplate(fileFragments).replace();
    // Phase 4: Convert code to C9LanguageFragment
    List<C9LanguageFragment> c9LanguageFragments = fileFragments.stream().map(ff -> {
      String name = extractFunctionName(ff.remove(0));
      return new C9LanguageFragment(name, ff.getLines());
    }).toList();
    return c9LanguageFragments;
  }

  private String extractFunctionName(String line) {
    return line.substring(3, line.lastIndexOf(" function"));
  }
}
