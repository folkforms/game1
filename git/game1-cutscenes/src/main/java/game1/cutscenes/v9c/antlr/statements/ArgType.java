package game1.cutscenes.v9c.antlr.statements;

public enum ArgType {
  VARIABLE, FLOAT_LITERAL, STRING_LITERAL, BOOLEAN_LITERAL
}
