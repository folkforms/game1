package game1.cutscenes.v9c.antlr.c9fileparser;

import java.util.List;

class ConvertTagsToDefs {

  private List<FileFragment> fileFragments;

  public ConvertTagsToDefs(List<FileFragment> fileFragments) {
    this.fileFragments = fileFragments;
  }

  public List<FileFragment> convertTagsToDefs() {
    return fileFragments.stream().map(this::convertTagToDef).toList();
  }

  private FileFragment convertTagToDef(FileFragment fileFragment) {
    boolean foundTag = false;
    List<String> lines = fileFragment.getLines();
    for (int i = 0; i < lines.size(); i++) {
      String line = lines.get(i);
      if (line.startsWith("#")) {
        foundTag = true;
        String tag = CutsceneUtils.convertTagToFunctionName(line);
        String newLine = String.format("def %s() {", tag);
        fileFragment.set(i, newLine);
      }
    }
    if (foundTag) {
      fileFragment.add("}");
    }
    return fileFragment;
  }
}
