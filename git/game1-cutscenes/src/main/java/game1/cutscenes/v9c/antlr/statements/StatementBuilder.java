package game1.cutscenes.v9c.antlr.statements;

import java.util.ArrayList;
import java.util.List;

public class StatementBuilder {

  private Class<? extends Statement> clazz;
  private String text;
  private String marker;
  private String function;
  private List<Arg> args = new ArrayList<>();
  private String assignmentVariable;

  public StatementBuilder(Class<? extends Statement> clazz) {
    this.clazz = clazz;
  }

  public StatementBuilder setMarker(String marker) {
    this.marker = marker;
    return this;
  }

  public StatementBuilder setFunctionName(String function) {
    this.function = function;
    return this;
  }

  public StatementBuilder addArg(Arg arg) {
    this.args.add(arg);
    return this;
  }

  public StatementBuilder setAssignmentVariable(String assignmentVariable) {
    this.assignmentVariable = assignmentVariable;
    return this;
  }

  public Statement build() {
    if (clazz == Function.class) {
      return new Function(function, args, assignmentVariable);
    } else if (clazz == Comment.class) {
      return new Comment(text);
    } else if (clazz == Marker.class) {
      return new Marker(marker);
    } else {
      throw new RuntimeException(
          String.format("StatementBuilder not configured for class %s", clazz));
    }
  }
}
