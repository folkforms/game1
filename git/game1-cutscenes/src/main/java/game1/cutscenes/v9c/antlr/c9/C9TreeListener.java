package game1.cutscenes.v9c.antlr.c9;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.antlr.v4.runtime.ParserRuleContext;

import antlr.game1.C9BaseListener;
import antlr.game1.C9Parser;
import folkforms.log.Log;
import game1.cutscenes.v9c.antlr.statements.Arg;
import game1.cutscenes.v9c.antlr.statements.ArgType;
import game1.cutscenes.v9c.antlr.statements.Function;
import game1.cutscenes.v9c.antlr.statements.Marker;
import game1.cutscenes.v9c.antlr.statements.Statement;
import game1.cutscenes.v9c.antlr.statements.StatementBuilder;
import game1.cutscenes.v9c.antlr.statements.TempIdGen;
import game1.debug.DebugScopes;

public class C9TreeListener extends C9BaseListener {

  private String version = "9.0.0";

  private List<Statement> finished = new ArrayList<>();
  private Stack<StatementBuilder> stack = new Stack<>();
  private StatementBuilder currentStatementBuilder = new StatementBuilder(null);
  private TempIdGen tempIdGen = new TempIdGen();
  private boolean removeUnnecessaryAssignments = true;
  private boolean inAssignmentOrNestedFunctionCall = false;

  @Override
  public void enterProg(C9Parser.ProgContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "enterProg");
  }

  @Override
  public void exitProg(C9Parser.ProgContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "exitProg");
    Log.scoped(DebugScopes.CUTSCENES, "---- Output ----");
    finished.stream().forEach(e -> Log.scoped(DebugScopes.CUTSCENES, "%s", e));
  }

  @Override
  public void enterExprVersion(C9Parser.ExprVersionContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "#### enterExprVersion: ctx = %s", ctx.getText());
    printChildren(ctx);
    String versionFromFile = ctx.version().VERSION_NUMBER().getText();
    if (!versionFromFile.equals(version)) {
      throw new RuntimeException(
          String.format("Mismatched versions: File is version %s but language is version %s",
              versionFromFile, version));
    }
  }

  @Override
  public void enterExprMarker(C9Parser.ExprMarkerContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "#### enterExprMarker: ctx = %s", ctx.getText());
    printChildren(ctx);
    startStatement(Marker.class);
    currentStatementBuilder.setMarker(ctx.getText());
  }

  @Override
  public void exitExprMarker(C9Parser.ExprMarkerContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "#### exitExprMarker: ctx = %s", ctx.getText());
    finishStatement();
  }

  @Override
  public void enterDot_notation(C9Parser.Dot_notationContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "#### enterDot_notation: ctx = %s", ctx.getText());
    printChildren(ctx);
    startStatement(Function.class, "_invoke");
    currentStatementBuilder.addArg(new Arg(ctx.member.getText(), ArgType.VARIABLE));
    currentStatementBuilder
        .addArg(new Arg(String.format("\"%s\"", ctx.name.getText()), ArgType.STRING_LITERAL));
  }

  @Override
  public void exitDot_notation(C9Parser.Dot_notationContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "#### exitDot_notation: ctx = %s", ctx.getText());
    finishStatement();
  }

  @Override
  public void enterFunction_call(C9Parser.Function_callContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "#### enterFunction_call: ctx = %s", ctx.getText());
    printChildren(ctx);
    startStatement(Function.class, ctx.id_().getText());
  }

  @Override
  public void exitFunction_call(C9Parser.Function_callContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "#### exitFunction_call: ctx = %s", ctx.getText());
    finishStatement();
  }

  @Override
  public void enterExprMultiplyDivide(C9Parser.ExprMultiplyDivideContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "######## enterExprMultiplyDivide: ctx = %s", ctx.getText());
    String functionName = ctx.op.getType() == C9Parser.MULTIPLY ? "_multiply" : "_divide";
    printChildren(ctx);
    startStatement(Function.class, functionName);
  }

  @Override
  public void exitExprMultiplyDivide(C9Parser.ExprMultiplyDivideContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "######## exitExprMultiplyDivide");
    finishStatement();
  }

  @Override
  public void enterExprAddSubtract(C9Parser.ExprAddSubtractContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "######## enterExprAddSubtract: ctx = %s", ctx.getText());
    String functionName = ctx.op.getType() == C9Parser.ADD ? "_add" : "_subtract";
    printChildren(ctx);
    startStatement(Function.class, functionName);
  }

  @Override
  public void exitExprAddSubtract(C9Parser.ExprAddSubtractContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "######## exitExprAddSubtract");
    finishStatement();
  }

  @Override
  public void enterExprLogicalAnd(C9Parser.ExprLogicalAndContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "######## enterExprLogicalAnd: ctx = %s", ctx.getText());
    startStatement(Function.class, "_logicalAnd");
  }

  @Override
  public void exitExprLogicalAnd(C9Parser.ExprLogicalAndContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "######## exitExprLogicalAnd");
    finishStatement();
  }

  @Override
  public void enterExprLogicalOr(C9Parser.ExprLogicalOrContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "######## enterExprLogicalOr: ctx = %s", ctx.getText());
    startStatement(Function.class, "_logicalOr");
  }

  @Override
  public void exitExprLogicalOr(C9Parser.ExprLogicalOrContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "######## exitExprLogicalOr");
    finishStatement();
  }

  @Override
  public void enterExprEquals(C9Parser.ExprEqualsContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "######## enterExprEquals: ctx = %s", ctx.getText());
    printChildren(ctx);
    startStatement(Function.class, "_equals");
  }

  @Override
  public void exitExprEquals(C9Parser.ExprEqualsContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "######## exitExprEquals");
    finishStatement();
  }

  @Override
  public void enterExprAssign(C9Parser.ExprAssignContext ctx) {
    inAssignmentOrNestedFunctionCall = true;
    Log.scoped(DebugScopes.CUTSCENES, "#### enterExprAssign: ctx = %s", ctx.getText());
    printChildren(ctx);
    startStatement(Function.class, "_assign");
  }

  @Override
  public void exitExprAssign(C9Parser.ExprAssignContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "######## exitExprAssign");
    if (removeUnnecessaryAssignments) {
      currentStatementBuilder.setAssignmentVariable(null);
      tempIdGen.decrement();
    }
    finishStatement();
    inAssignmentOrNestedFunctionCall = false;
  }

  @Override
  public void enterNestedFunctionCall(C9Parser.NestedFunctionCallContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "######## enterNestedFunctionCall");
    inAssignmentOrNestedFunctionCall = true;
  }

  @Override
  public void exitNestedFunctionCall(C9Parser.NestedFunctionCallContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "######## exitNestedFunctionCall");
    inAssignmentOrNestedFunctionCall = false;
  }

  @Override
  public void enterNestedDotNotation(C9Parser.NestedDotNotationContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "######## enterNestedDotNotation");
    inAssignmentOrNestedFunctionCall = true;
  }

  @Override
  public void exitNestedDotNotation(C9Parser.NestedDotNotationContext ctx) {
    Log.scoped(DebugScopes.CUTSCENES, "######## exitNestedDotNotation");
    inAssignmentOrNestedFunctionCall = false;
  }

  @Override
  public void enterValueId(C9Parser.ValueIdContext ctx) {
    currentStatementBuilder.addArg(new Arg(ctx.getText(), ArgType.VARIABLE));
  }

  @Override
  public void enterValueFloatLiteral(C9Parser.ValueFloatLiteralContext ctx) {
    currentStatementBuilder.addArg(new Arg(ctx.getText(), ArgType.FLOAT_LITERAL));
  }

  @Override
  public void enterValueStringLiteral(C9Parser.ValueStringLiteralContext ctx) {
    currentStatementBuilder.addArg(new Arg(ctx.getText(), ArgType.STRING_LITERAL));
  }

  @Override
  public void enterValueBooleanLiteral(C9Parser.ValueBooleanLiteralContext ctx) {
    currentStatementBuilder.addArg(new Arg(ctx.getText(), ArgType.BOOLEAN_LITERAL));
  }

  private void startStatement(Class<? extends Statement> statementType) {
    startStatement(statementType, null);
  }

  private void startStatement(Class<? extends Statement> statementType, String functionName) {
    String tempId = tempIdGen.get();
    currentStatementBuilder.addArg(new Arg(tempId, ArgType.VARIABLE));
    stack.push(currentStatementBuilder);
    currentStatementBuilder = new StatementBuilder(statementType);
    currentStatementBuilder.setFunctionName(functionName);
    // FIXME C9: This seems to work ok for now...
    // if (inAssignmentOrNestedFunctionCall) {
    currentStatementBuilder.setAssignmentVariable(tempId);
    // } else {
    // tempIdGen.decrement();
    // }
  }

  private void finishStatement() {
    finished.add(currentStatementBuilder.build());
    currentStatementBuilder = stack.pop();
  }

  private void printChildren(ParserRuleContext ctx) {
    for (int i = 0; i < ctx.getChildCount(); i++) {
      Log.scoped(DebugScopes.CUTSCENES, "# child = %s (%s children)", ctx.getChild(i).getText(),
          ctx.getChild(i).getChildCount());
    }
  }

  public List<Statement> getStatements() {
    return finished;
  }
}
