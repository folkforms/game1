package game1.cutscenes.v9c.antlr.statements;

public class TempIdGen {

  private int index = 0;

  public String get() {
    String id = String.format("_temp%s", index);
    index++;
    return id;
  }

  public void decrement() {
    index--;
  }
}
