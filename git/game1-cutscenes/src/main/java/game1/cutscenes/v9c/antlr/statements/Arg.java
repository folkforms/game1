package game1.cutscenes.v9c.antlr.statements;

public class Arg {

  private String text;
  private ArgType type;

  // FIXME C9: Should we just use objects and do away with ArgType?
  // FIXME C9: Why is this "text" and not "name"
  public Arg(String text, ArgType type) {
    this.text = text;
    this.type = type;
  }

  public String getText() {
    return text;
  }

  public ArgType getType() {
    return type;
  }

  public float asFloat() {
    if (type != ArgType.FLOAT_LITERAL) {
      throw new RuntimeException(
          String.format("Cannot convert arg '%s' to a float (type is %s)", text, type));
    } else {
      return Float.parseFloat(text);
    }
  }

  public String asString() {
    if (type == ArgType.STRING_LITERAL) {
      return text;
    }
    if (type == ArgType.VARIABLE) {
      return text;
    } else {
      throw new RuntimeException(
          String.format("Cannot convert arg '%s' to a string (type is %s)", text, type));
    }
  }

  public boolean asBoolean() {
    if (type != ArgType.BOOLEAN_LITERAL) {
      throw new RuntimeException(
          String.format("Cannot convert arg '%s' to a boolean (type is %s)", text, type));
    } else {
      return Boolean.parseBoolean(text);
    }
  }

  public Object asObject() {
    if (type == ArgType.FLOAT_LITERAL) {
      return asFloat();
    }
    if (type == ArgType.BOOLEAN_LITERAL) {
      return asBoolean();
    }
    return asString();
  }

  public static Arg resolveValue(Object obj) {
    String text = obj.toString();
    ArgType type = ArgType.STRING_LITERAL;
    if (obj.getClass() == Float.class || obj.getClass() == Integer.class
        || obj.getClass() == Double.class) {
      type = ArgType.FLOAT_LITERAL;
    } else if (obj.getClass() == Boolean.class) {
      type = ArgType.BOOLEAN_LITERAL;
    }
    return new Arg(text, type);
  }

  @Override
  public String toString() {
    return String.format("Arg[%s,%s]", text, type);
  }
}
