package game1.cutscenes.v9c.antlr.c9fileparser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

class ReplaceDefsWithDefBoilerplate {

  private List<FileFragment> fileFragments;

  public ReplaceDefsWithDefBoilerplate(List<FileFragment> fileFragments) {
    this.fileFragments = fileFragments;
  }

  public List<FileFragment> replace() {
    return fileFragments.stream().map(this::replace).toList();
  }

  private FileFragment replace(FileFragment fileFragment) {
    // Expect first line to be "def <name>(<args>) {"
    String firstLine = fileFragment.get(0);
    FunctionSignature functionSignature = parseFunctionSignature(firstLine);
    String name = functionSignature.getName();
    fileFragment.set(0, String.format("// %s function", name));

    // If args then add a call to _resolveLocalVariables
    if (functionSignature.getArgs().size() > 0) {
      String argsStr = functionSignature.getArgs().stream().collect(Collectors.joining(", "));
      String firstLineB = String.format("_resolveLocalVariables(%s);", argsStr);
      fileFragment.add(1, firstLineB);
    }

    // Replace closing bracket
    fileFragment.remove(fileFragment.size() - 1);

    return fileFragment;
  }

  private FunctionSignature parseFunctionSignature(String input) {
    Matcher matcher = Pattern.compile("def (.*?)\\((.*?)\\) \\{").matcher(input);
    boolean matchFound = matcher.find();
    if (matchFound) {
      FunctionSignature functionSignature = new FunctionSignature(matcher.group(1));
      if (matcher.groupCount() == 2) {
        if (matcher.group(2).length() > 0) {
          String[] split = matcher.group(2).split(",\\s*");
          Arrays.stream(split).forEach(item -> functionSignature.addArg(item));
        }
      }
      return functionSignature;
    }
    throw new RuntimeException(
        String.format("Could not extract function signature from '%s'", input));
  }

  private class FunctionSignature {
    private String name;
    private List<String> args = new ArrayList<>();

    FunctionSignature(String name) {
      this.name = name;
    }

    public void addArg(String arg) {
      args.add(arg);
    }

    public String getName() {
      return name;
    }

    public List<String> getArgs() {
      return args;
    }
  }
}
