# Game1

A 3D/2D game engine. Can be used for any and all types of games.

TO DO: Showcase animated gif

[Cutscenes reference](git/toolkit/src/main/java/game1/toolkit/cutscenes/cutscenes.md)

## Installation and/or upgrading Eclipse & Maven

1. Eclipse
    - Download eclipse from https://www.eclipse.org/downloads/ (current version is 2023-09)
    - Install the 2nd option (the cogwheel, J2EE devs) to d:\dev\tools\eclipse\jee-2023-09
2. Maven
    - Download maven from https://maven.apache.org/download.cgi
3. Env vars
    - Update ECLIPSE env var to the new Eclipse folder
    - Update PATH to new Maven `bin` folder
4. Launch with `eclipse.bat`
5. Import the projects: Import > Maven > Existing Maven projects > Enter "." and click Browse
    - Uncheck the top-level `/git/pom.xml`
    - Uncheck "Add project(s) to working set"
6. Import game1-utils and test-game separately
    - These are not part of the parent pom so they are not picked up by the above search
7. Build test-game spritesheets with `mvn exec:exec@spritesheets`
8. Run `i-gen.bat` to put a copy of the current code into the local Maven repo. This is so the `games` repo can access it.

## Eclipse setup

1. Enable Dark mode: Window > Preferences > General > Appearance > Set theme to "Dark" > Apply > Restart
2. Import the code formatter
    - Window > Preferences > Java > Code Style > Formatter > Import "D:\dev\projects\game1\misc\CodeFormatter100.xml"
    - Window > Preferences > Java > Editor > Save Actions > Check "Perform the selected actions on save" > Check "Format source code" (all lines), "Organize imports" and "Additional actions"
3. Package presentation: Click the kebab menu in the Project Explorer tab > Package presentation > Hierarchical
